#----------------Imports--------------------------
import unittest
import datetime
import logging
from collections import Counter

from lxml import etree
from pandas import ExcelFile
from pandas import ExcelWriter
import pytest
import pytest_html
import requests

from tkinter import messagebox

import json
from jsonpath_ng import jsonpath, parse


#from Python_Test1 import html_logger
import sys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.ui import Select

parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

jiraURL = 'https://oneamerica.atlassian.net/rest/api/2/issue/'
cookie1 = 'ajs_group_id=null; ajs_anonymous_id=%22b64f2425-4030-47ef-ae05-5ded888ecb3d%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_8468855bf729139e8663fdbf26bb2808727bd10e_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Byb2QtMTU5Mjg1ODM5NCIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sImNyZWF0ZWQiOjE2MDYyMjIwMjksInJlZnJlc2hUaW1lb3V0IjoxNjA2NzM0NzUxLCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiZDYyNDA5YTEtYjY1MC00ZGIzLTlmNWQtOGFjOWU4MTgyMDdiIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNjA2NzM0MTUxLCJleHAiOjE2MDkzMjYxNTEsImlhdCI6MTYwNjczNDE1MSwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiJkNjI0MDlhMS1iNjUwLTRkYjMtOWY1ZC04YWM5ZTgxODIwN2IifQ.Pv1SqnOWGZko_u4rwE6omhAH9h2Z71jMz0-wjE2vcQRhVoX2Sxa6NEr2RqmLNWZBvDHX-4aRhbFeKSlhGpBzqnXMq7IINc6T8kvPnnznzKunX5nbPP0CoI-jhzGbsht5Dlw2HEHDvrbFD4_OywlHDIU_4ATab1rwZgOdYVnO7iqsJTZIT3EIE6daZ7soreY-2HfKi4NMz79gLcssiXo-IsGxaNJ2MRewu1IK25yH-XEbuWMT2nEbL8X215nVdWZ41EnPlruu31XFj2Fy6gcm2LzpZIceV1bDUNKD1rwt1HXu-j9TgwpydJhUDZ91Amh9Jc4avm1DRRuCVG_KbD8Ukw'

import html_logger
from html_logger import setup, info

import pandas as pd

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


#----------------Imports--------------------------
#----------------Logging--------------------------
now = datetime.datetime.now()

#LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\OKRA-Test_"+ str(now.isoformat().replace(":","_")) + ".html"
LOG_FILENAME = ".\\Results\\OKRA-Test_"+ str(now.isoformat().replace(":","_")) + ".html"

#logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)
#setup("WebService_Test","1",LOG_FILENAME)
setup("OKRA Test","Executed on " + str(now),LOG_FILENAME)  #Zucchini Test
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')

#*********************************************************** 
#------------------Methods------------------------------------
#***********************************************************

#CompareXmlDb
    
def BrowserOpen(brow,url):
    """ if brow.strip() == 'chrome':
        driver = webdriver.Chrome('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\chromedriver.exe')
    elif brow.strip()== 'firefox':
        driver= webdriver.Firefox('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\geckodriver.exe')   
    """
   # driver = webdriver.Chrome('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\chromedriver.exe')
    if url != '':
        driver.get(url)   
        driver.implicitly_wait(5000)
        
def ElementClick(xpath): 
    if(driver.find_element_by_xpath(xpath).get_attribute('id') != ''):
        html_logger.dbg("Clicked " + driver.find_element_by_xpath(xpath).get_attribute('id'))
    else:
        html_logger.dbg("Clicked " + xpath)  
          
    driver.find_element_by_xpath(xpath).click()
    driver.implicitly_wait(1)
   

def ElementEnter(xpath,val):
    html_logger.dbg("Entered " + val  + " in " + xpath)
    driver.find_element_by_xpath(xpath).clear()
    driver.find_element_by_xpath(xpath).send_keys(val)
    driver.implicitly_wait(1)
    
def ElementSelect(xpath,val):
    html_logger.dbg("Selected " + val  + " in " + xpath)    
    selElem = Select(driver.find_element_by_xpath(xpath))
    selElem.select_by_visible_text(val)

def ElementCheckbox(xpath,val):
    if val.lower() == 'on':
        html_logger.dbg(xpath + " Check box is turned on")     
        if driver.find_element_by_xpath(xpath).is_selected():
            pass
        else:
            driver.find_element_by_xpath(xpath).click()
        
    elif val.lower() == 'off':
        html_logger.dbg(xpath + " Check box is turned off")
        if driver.find_element_by_xpath(xpath).is_selected():
            driver.find_element_by_xpath(xpath).click()  
        else:
            pass   

def VerifyTextContains(xpath,val):
    txt =  driver.find_element_by_xpath(xpath).get_attribute("innerHTML") 
    html_logger.dbg(txt + " present in " + xpath)
    if(val in txt):
         html_logger.dbg("Pass " + txt + " present in " + xpath)
    else:
         html_logger.err("Fail " + val + " not present in " + xpath + " but the value present was " + txt)     

def verifyElementPresent(xpath):
    if(len(driver.find_elements_by_xpath(xpath)) > 0):
        html_logger.dbg("Pass element " + xpath + " present as expected")
    else:
        html_logger.err("Fail element " + xpath + " not present as expected")
        
def verifyElementNotPresent(xpath):
    if(len(driver.find_elements_by_xpath(xpath)) == 0):
        html_logger.dbg("Pass element " + xpath + " not present as expected")
    else:
        html_logger.err("Fail element " + xpath + " present as expected")        

def waitUntilInput(msgcmt):
    #value = input("press any Key to Continue " + msgcmt)
    messagebox.showinfo("OKRA Test", msgcmt)
    
        
def Popup(val):
    if(val=='accept'):
        #driver.switch_to_alert().accept() 
        driver.switch_to.alert.accept()
        driver.implicitly_wait(1)
        html_logger.dbg("Pop up accepted")   
    else:
        #driver.switch_to_alert().dismiss()
        driver.switch_to.alert.dismiss()
        driver.implicitly_wait(1)
        html_logger.dbg("Pop up dismissed")

def callStory(us):
        html_logger.dbg("#####################################")
        html_logger.info("Start of User story " + us)
        html_logger.dbg("#####################################")
        #cookie1 = 'ajs_group_id=null; ajs_anonymous_id=%22b64f2425-4030-47ef-ae05-5ded888ecb3d%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_8468855bf729139e8663fdbf26bb2808727bd10e_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Byb2QtMTU5Mjg1ODM5NCIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sImNyZWF0ZWQiOjE2MDYyMjIwMjksInJlZnJlc2hUaW1lb3V0IjoxNjA2NzM0NzUxLCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiZDYyNDA5YTEtYjY1MC00ZGIzLTlmNWQtOGFjOWU4MTgyMDdiIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNjA2NzM0MTUxLCJleHAiOjE2MDkzMjYxNTEsImlhdCI6MTYwNjczNDE1MSwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiJkNjI0MDlhMS1iNjUwLTRkYjMtOWY1ZC04YWM5ZTgxODIwN2IifQ.Pv1SqnOWGZko_u4rwE6omhAH9h2Z71jMz0-wjE2vcQRhVoX2Sxa6NEr2RqmLNWZBvDHX-4aRhbFeKSlhGpBzqnXMq7IINc6T8kvPnnznzKunX5nbPP0CoI-jhzGbsht5Dlw2HEHDvrbFD4_OywlHDIU_4ATab1rwZgOdYVnO7iqsJTZIT3EIE6daZ7soreY-2HfKi4NMz79gLcssiXo-IsGxaNJ2MRewu1IK25yH-XEbuWMT2nEbL8X215nVdWZ41EnPlruu31XFj2Fy6gcm2LzpZIceV1bDUNKD1rwt1HXu-j9TgwpydJhUDZ91Amh9Jc4avm1DRRuCVG_KbD8Ukw'
        #'ajs_group_id=null; ajs_anonymous_id=%2294b03847-77a3-4883-9d64-fad8cd1677a8%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_981aa0ccfd9abbb1bce00285c0f1017f22a81aad_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Nlc3Npb24tc2VydmljZSIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sInJlZnJlc2hUaW1lb3V0IjoxNTgwNjAyNzE4LCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiMzkwMDgyMTgtZDVjNi00ZmIwLWFlMTgtYmUxMDM4ZjA5YTAyIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNTgwNjAyMTE4LCJleHAiOjE1ODMxOTQxMTgsImlhdCI6MTU4MDYwMjExOCwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiIzOTAwODIxOC1kNWM2LTRmYjAtYWUxOC1iZTEwMzhmMDlhMDIifQ.IQX3Qs_oEdyDfU5stIvNWg_24-I1bG9itzK4N0PKpLrcLffBTpQeLB0EjeyxkbO_342QaVWrRcUsoBeVy8kLWAvM_J9VYmlBUeNvSQBEYgajTAvLwfi3B19_sPfxGKltIxjTIOoCt46uA6BA_YbkyGVk_kUgt2TD6fpeVYItg0MhbBL2uYoyMbSQctuuW9X__yiqKeuWjeEDz0VuuRISRgEF9beSxQ8AsU9MxD0YylBOAUyGelv37ajpxSjUEswURdPicSDuULU5D7w7lQOrvVu-vVI_oX6i5zMf3JiF-uWEa5g6gcJuVhHB26JVXNMqz0oEce6tRFxi-14C45ZRzA'
        headers1 = 'headers = {''Cookie'':''' + cookie1 + "''}" 
        r = requests.get(url = jiraURL + us + '/comment',headers ={'Cookie': cookie1})   #EC-2584
        jsonval = r.json()
        
        '''try:
            driver.close()
        except:
            pass'''
        
        y = json.loads(json.dumps(jsonval))
        
        for i in y["comments"]:
            text1 = i["body"]
            print(text1)
            
            if '~code~' in text1:
                codetxt = text1
                arrcode = codetxt.split('\n')
                
                for linecode in arrcode:
                    if linecode != '':
                        arrLineElement = linecode.split("|")
                        #arrLineElement[1] = arrLineElement[1].lower()
                        
                        if arrLineElement[0] == '~code~':
                            pass
                        
                        elif '~Scenario' in arrLineElement[0]:
                            html_logger.info("Start of Scenario " + arrLineElement[0].replace("~Scenario",""))
                            
                        elif arrLineElement[1] == 'open':
                            BrowserOpen(arrLineElement[2],arrLineElement[3])
                            
                        elif arrLineElement[1] == 'click':
                                    ElementClick(arrLineElement[2].replace("\\",""))
                                   
                        elif arrLineElement[1] == 'select':
                                    ElementSelect(arrLineElement[2].replace("\\",""),arrLineElement[3]) 
                                    
                        elif arrLineElement[1] == 'checkbox':
                                    ElementCheckbox(arrLineElement[2].replace("\\",""),arrLineElement[3])                         
                                    
                        elif arrLineElement[1] == 'enter':
                                    ElementEnter(arrLineElement[2].replace("\\",""), arrLineElement[3]) 
                                    
                        elif arrLineElement[1] == 'newwindow':   
                                    driver.switch_to.window(driver.window_handles[1])  
            
                        elif arrLineElement[1] == 'popup':
                                    Popup(arrLineElement[2].replace("\\","")) 
                                    
                        elif arrLineElement[1] == 'story': 
                                     callStory(arrLineElement[2]) 
                                     
                        elif arrLineElement[1] == 'story_scenario': 
                                     callStory_Scenario(arrLineElement[2],arrLineElement[3])
                                     
                                     
                        elif arrLineElement[1] == 'verifyTextContains':
                                    VerifyTextContains(arrLineElement[2].replace("\\",""), arrLineElement[3])   
                        
                        elif arrLineElement[1] == 'verifyElementPresent':
                                    verifyElementPresent(arrLineElement[2].replace("\\",""))  
                                    
                        elif arrLineElement[1] == 'verifyElementNotPresent':
                                    verifyElementNotPresent(arrLineElement[2].replace("\\",""))              
                                    
                        elif arrLineElement[1] == 'waitUntilKeyPress':
                                    waitUntilInput(arrLineElement[2])                                



def callStory_Scenario(us,scenario):
        html_logger.dbg("#####################################")
        html_logger.info("Start of User story " + us)
        html_logger.dbg("#####################################")
        #cookie1 = 'ajs_group_id=null; ajs_anonymous_id=%22b64f2425-4030-47ef-ae05-5ded888ecb3d%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_8468855bf729139e8663fdbf26bb2808727bd10e_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Byb2QtMTU5Mjg1ODM5NCIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sImNyZWF0ZWQiOjE2MDYyMjIwMjksInJlZnJlc2hUaW1lb3V0IjoxNjA2NzM0NzUxLCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiZDYyNDA5YTEtYjY1MC00ZGIzLTlmNWQtOGFjOWU4MTgyMDdiIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNjA2NzM0MTUxLCJleHAiOjE2MDkzMjYxNTEsImlhdCI6MTYwNjczNDE1MSwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiJkNjI0MDlhMS1iNjUwLTRkYjMtOWY1ZC04YWM5ZTgxODIwN2IifQ.Pv1SqnOWGZko_u4rwE6omhAH9h2Z71jMz0-wjE2vcQRhVoX2Sxa6NEr2RqmLNWZBvDHX-4aRhbFeKSlhGpBzqnXMq7IINc6T8kvPnnznzKunX5nbPP0CoI-jhzGbsht5Dlw2HEHDvrbFD4_OywlHDIU_4ATab1rwZgOdYVnO7iqsJTZIT3EIE6daZ7soreY-2HfKi4NMz79gLcssiXo-IsGxaNJ2MRewu1IK25yH-XEbuWMT2nEbL8X215nVdWZ41EnPlruu31XFj2Fy6gcm2LzpZIceV1bDUNKD1rwt1HXu-j9TgwpydJhUDZ91Amh9Jc4avm1DRRuCVG_KbD8Ukw'
        #'ajs_group_id=null; ajs_anonymous_id=%2294b03847-77a3-4883-9d64-fad8cd1677a8%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_981aa0ccfd9abbb1bce00285c0f1017f22a81aad_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Nlc3Npb24tc2VydmljZSIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sInJlZnJlc2hUaW1lb3V0IjoxNTgwNjAyNzE4LCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiMzkwMDgyMTgtZDVjNi00ZmIwLWFlMTgtYmUxMDM4ZjA5YTAyIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNTgwNjAyMTE4LCJleHAiOjE1ODMxOTQxMTgsImlhdCI6MTU4MDYwMjExOCwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiIzOTAwODIxOC1kNWM2LTRmYjAtYWUxOC1iZTEwMzhmMDlhMDIifQ.IQX3Qs_oEdyDfU5stIvNWg_24-I1bG9itzK4N0PKpLrcLffBTpQeLB0EjeyxkbO_342QaVWrRcUsoBeVy8kLWAvM_J9VYmlBUeNvSQBEYgajTAvLwfi3B19_sPfxGKltIxjTIOoCt46uA6BA_YbkyGVk_kUgt2TD6fpeVYItg0MhbBL2uYoyMbSQctuuW9X__yiqKeuWjeEDz0VuuRISRgEF9beSxQ8AsU9MxD0YylBOAUyGelv37ajpxSjUEswURdPicSDuULU5D7w7lQOrvVu-vVI_oX6i5zMf3JiF-uWEa5g6gcJuVhHB26JVXNMqz0oEce6tRFxi-14C45ZRzA'
        headers1 = 'headers = {''Cookie'':''' + cookie1 + "''}" 
        r = requests.get(url = jiraURL + us + '/comment',headers ={'Cookie': cookie1})   #EC-2584
        jsonval = r.json()
        
        '''try:
            driver.close()
        except:
            pass'''
        
        y = json.loads(json.dumps(jsonval))
        skipcomments = 0 #'''logic to skip a comment of scenario doesn't match
        
        for i in y["comments"]:
            text1 = i["body"]
            #print(text1)
            skipcomments = 0
            
            if '~code~' in text1:
                codetxt = text1
                arrcode = codetxt.split('\n')
                
                for linecode in arrcode:
                    if skipcomments == -1:
                        continue
                   
                    if linecode != '':
                        arrLineElement = linecode.split("|")
                        #arrLineElement[1] = arrLineElement[1].lower()
                        
                        if arrLineElement[0] == '~code~':
                            pass
                        
                        elif '~Scenario' in arrLineElement[0]:
                            if (arrLineElement[0].replace("~Scenario","")).strip() != scenario:
                                skipcomments = -1
                                continue
                            else:
                                html_logger.info("Start of Scenario " + arrLineElement[0].replace("~Scenario",""))
                            
                        elif arrLineElement[1] == 'open':
                            BrowserOpen(arrLineElement[2],arrLineElement[3])
                            
                        elif arrLineElement[1] == 'click':
                                    ElementClick(arrLineElement[2].replace("\\",""))
                                   
                        elif arrLineElement[1] == 'select':
                                    ElementSelect(arrLineElement[2].replace("\\",""),arrLineElement[3]) 
                                    
                        elif arrLineElement[1] == 'checkbox':
                                    ElementCheckbox(arrLineElement[2].replace("\\",""),arrLineElement[3])                         
                                    
                        elif arrLineElement[1] == 'enter':
                                    ElementEnter(arrLineElement[2].replace("\\",""), arrLineElement[3]) 
                                    
                        elif arrLineElement[1] == 'newwindow':   
                                    driver.switch_to.window(driver.window_handles[1])  
            
                        elif arrLineElement[1] == 'popup':
                                    Popup(arrLineElement[2].replace("\\","")) 
                                    
                        elif arrLineElement[1] == 'story': 
                                     callStory(arrLineElement[2]) 
                                     
                        elif arrLineElement[1] == 'verifyTextContains':
                                    VerifyTextContains(arrLineElement[2].replace("\\",""), arrLineElement[3])   
                        
                        elif arrLineElement[1] == 'verifyElementPresent':
                                    verifyElementPresent(arrLineElement[2].replace("\\",""))  
                                    
                        elif arrLineElement[1] == 'verifyElementNotPresent':
                                    verifyElementNotPresent(arrLineElement[2].replace("\\",""))              
                                    
                        elif arrLineElement[1] == 'waitUntilKeyPress':
                                    waitUntilInput(arrLineElement[2])                                
                              




def callStory_backup(us):
        html_logger.dbg("#####################################")
        html_logger.info("Start of User story " + us)
        html_logger.dbg("#####################################")
        cookie1 = 'ajs_group_id=null; ajs_anonymous_id=%2294b03847-77a3-4883-9d64-fad8cd1677a8%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_981aa0ccfd9abbb1bce00285c0f1017f22a81aad_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Nlc3Npb24tc2VydmljZSIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sInJlZnJlc2hUaW1lb3V0IjoxNTgzMzU2MDk5LCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiMzkwMDgyMTgtZDVjNi00ZmIwLWFlMTgtYmUxMDM4ZjA5YTAyIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNTgzMzU1NDk5LCJleHAiOjE1ODU5NDc0OTksImlhdCI6MTU4MzM1NTQ5OSwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiIzOTAwODIxOC1kNWM2LTRmYjAtYWUxOC1iZTEwMzhmMDlhMDIifQ.tbgGsBawARFA1Ip2dv8nd6eESfmKmEUXBo9v54XqpJu8qhS-wRNK2b2E_tclUW9OD2rGa-t80HR4RQvmB_tUU3ckOQUEkEhd3sBvxrZQlPRkkbDv5m9RVTDFClnFDZkZHx-i5jy_pWlLR-EGckC9T9Q4y3zp4aziu1fiSzvQlSBf6w4H-X6OEoSOpMgxHIarfT12J9W9yQiXpwdRzjMFx5blb6ZEdtpB_tTguW6RhgkEcXY79VzLctAgmOVp7IhhsNfOKq5BqHv3iwW-nQT2ITaWezagclfSNMBjsbef_XHeg3uwYUS5O73uqm19Wh7WodcI54AbwmypOmOsLLIsgg'
        #'ajs_group_id=null; ajs_anonymous_id=%2294b03847-77a3-4883-9d64-fad8cd1677a8%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_981aa0ccfd9abbb1bce00285c0f1017f22a81aad_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Nlc3Npb24tc2VydmljZSIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sInJlZnJlc2hUaW1lb3V0IjoxNTgwNjAyNzE4LCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiMzkwMDgyMTgtZDVjNi00ZmIwLWFlMTgtYmUxMDM4ZjA5YTAyIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNTgwNjAyMTE4LCJleHAiOjE1ODMxOTQxMTgsImlhdCI6MTU4MDYwMjExOCwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiIzOTAwODIxOC1kNWM2LTRmYjAtYWUxOC1iZTEwMzhmMDlhMDIifQ.IQX3Qs_oEdyDfU5stIvNWg_24-I1bG9itzK4N0PKpLrcLffBTpQeLB0EjeyxkbO_342QaVWrRcUsoBeVy8kLWAvM_J9VYmlBUeNvSQBEYgajTAvLwfi3B19_sPfxGKltIxjTIOoCt46uA6BA_YbkyGVk_kUgt2TD6fpeVYItg0MhbBL2uYoyMbSQctuuW9X__yiqKeuWjeEDz0VuuRISRgEF9beSxQ8AsU9MxD0YylBOAUyGelv37ajpxSjUEswURdPicSDuULU5D7w7lQOrvVu-vVI_oX6i5zMf3JiF-uWEa5g6gcJuVhHB26JVXNMqz0oEce6tRFxi-14C45ZRzA'
        headers1 = 'headers = {''Cookie'':''' + cookie1 + "''}" 
        r = requests.get(url = jiraURL + us + '/comment',headers ={'Cookie': cookie1})   #EC-2584
        jsonval = r.json()
        
        '''try:
            driver.close()
        except:
            pass'''
        
        y = json.loads(json.dumps(jsonval))
        for i in y["comments"]:
            text1 = i["body"]
            if '~code~' in text1:
                codetxt = text1
                arrcode = codetxt.split('\n')
                for linecode in arrcode:
                    arrLineElement = linecode.split("|")
                    if arrLineElement[0] == 'open':
                        BrowserOpen(arrLineElement[1],arrLineElement[2])
                        
                    elif arrLineElement[0] == 'click':
                                ElementClick(arrLineElement[1].replace("\\",""))  
                                
                    elif arrLineElement[0] == 'enter':
                                ElementEnter(arrLineElement[1].replace("\\",""), arrLineElement[2]) 
                                
                    elif arrLineElement[0] == 'newwindow':   
                                driver.switch_to.window(driver.window_handles[1])  
        
                    elif arrLineElement[0] == 'popup':
                                Popup(arrLineElement[1].replace("\\","")) 
                                
                    elif arrLineElement[0] == 'story': 
                                 callStory(arrLineElement[1]) 

#*********************************************************** 
#------------------Methods------------------------------------
#***********************************************************

#*********************************************************** 
#------------------Test Call------------------------------------
#***********************************************************
                                      
from requests.auth import HTTPBasicAuth
import base64
import json

#driver.maximize_window()

USList = 'EC-3713' #'EC-3082' #'EC-2584'  #'EC-2584;EC-3333'
arrUSList = USList.split(';')
for us in arrUSList:
    driver = webdriver.Chrome('.\\Driver\\chromedriver.exe')
    driver.maximize_window()
    callStory(us)
    driver.close()
    
print("End of tests") 

#*********************************************************** 
#------------------Test Call------------------------------------
#***********************************************************  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')
#driver.get('https://oneamerica.atlassian.net/rest/api/2/issue/EC-2584/comment')

