import unittest
from test import support

import Payment_ReusableArtifacts
from Payment_ReusableArtifacts import Comparator,Parser,DFCompare

import html_logger
from html_logger import setup, info

import datetime
import logging
import sys
import time
import csv

import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET
       
now = datetime.datetime.now()
parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

#driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  

LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\GainLZ_DataMart_"+ str(now.isoformat().replace(":","_")) + ".html"
setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)




def compare_Worksite():
        #-------------------------------------------------------------------------------------------------------------
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.WRKSITE_OLS_COV_FACT vs GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp")   
        
        DMsql = '''select C.Ben_Plan_typ_cd,B.ADMIN_SYS_PARTY_KEY_TXT,A.LUMP_SUM_ELIM_PER_NBR,A.OLS_ELIM_PER_TYP_DESCR,A.OLS_QLFCTN_DISBLTY_TYP_CD,A.WRKSITE_DRUG_ALCOL_LIMIT_PER_CD,A.WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR
        ,A.WRKSITE_BEN_INJRY_ELIM_PER_CD,A.WRKSITE_BEN_INJRY_ELIM_PER_DESCR,A.WRKSITE_BEN_SICK_ELIM_PER_CD,A.WRKSITE_BEN_SICK_ELIM_PER_DESCR,
        
        A.WRKSITE_RTRN_TO_WRK_BEN_PER_CD,A.WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR,A.TOT_DISBLTY_DUR_TYP_CD,TOT_DISBLTY_DUR_TYP_DESCR,
        A.MNTL_ILL_BEN_DUR_TYP_CD,A.MNTL_ILL_BEN_DUR_TYP_DESCR,A.WRKSITE_DISBLTY_OPT_NBR,A.OTH_INCM_INTGRTN_CD,A.OTH_INCM_INTGRTN_DESCR,A.MAX_INJRY_BEN_DUR_CD,A.MAX_SICK_BEN_DUR_CD
        ,A.FULL_TM_STAT_QLFCTN_THRSHLD_NBR,A.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD,A.FULL_TM_STAT_AVG_PER_NBR,A.FULL_TM_STAT_AVG_PER_TYP_CD,A.FULL_TM_CNTRCT_IND,A.RTRN_TO_WRK_PER_NBR,
        A.PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR,A.POST_EFF_DT_TRTMT_FREE_PER_NBR,A.PRE_EXIT_COND_NOT_CVRD_PER_NBR,A.BEN_RDCTN_CD,BEN_RDCTN_DESCR,A.SLRY_CONT_TYP_CD,
        A.WRKSITE_DISBLTY_BSC_EARN_TYP_CD,A.OCPTN_DISBLTY_CD,A.OLS_DEATH_BEN_PCT,A.OLS_MNTL_ILL_BEN_LMT_PCT,A.OLS_SUBS_ABUSE_BEN_LMT_PCT,A.OLS_SPEC_COND_BEN_LMT_PCT,A.BSC_EARN_AVG_PER_NBR,A.PARTNERSHIP_INCM_TYP_CD,
        
        A.S_CORP_INCM_TYP_CD,SOLE_PROP_INCM_TYP_CD,A.SURV_BEN_PER_TYP_DESCR,A.BSC_EARN_AVG_PER_TYP_DESCR,A.BSC_EARN_AVG_PER_TYP_CD,A.SURV_BEN_PER_TYP_CD,A.SURV_BEN_PER_NBR,A.BSC_EARN_INCM_TYP_CD,A.TOTAL_DISBLTY_JOB_CLSFCTN_TYP_CD
        
        ,A.POST_EFF_DT_TRTMT_FREE_PER_TYP_CD,A.PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD,A.PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD,A.RTRN_TO_WRK_PER_TYP_CD,A.POST_EFF_DT_TRTMT_FREE_PER_DESCR,A.PRE_EXIT_COND_NOT_CVRD_PER_DESCR,
        A.PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR,A.RTRN_TO_WRK_PER_TYP_DESCR
        
         from dbo.WRKSITE_OLS_COV_FACT A
                   inner join dbo.Party_Dim B on (B.PARTY_DIM_GEN_ID= A.PARTY_DIM_GEN_ID)
                   inner join ben_plan_dim c on (A.BEN_PLAN_DIM_GEN_ID = C.BEN_PLAN_DIM_GEN_ID)'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #html_logger.info("End of 1st query")   
        
        
        #DMsql1df['WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR'] = DMsql1df['WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(["Engineering", "Distribution", "Finance"])]
        DMsql1df['TOT_DISBLTY_DUR_TYP_DESCR'] = DMsql1df['TOT_DISBLTY_DUR_TYP_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]                    
        DMsql1df['MNTL_ILL_BEN_DUR_TYP_DESCR'] = DMsql1df['MNTL_ILL_BEN_DUR_TYP_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['MAX_INJRY_BEN_DUR_CD'] = DMsql1df['MAX_INJRY_BEN_DUR_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['MAX_SICK_BEN_DUR_CD'] = DMsql1df['MAX_SICK_BEN_DUR_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_NBR'] = DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_NBR'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL','ODS'])]
        DMsql1df['WRKSITE_DISBLTY_BSC_EARN_TYP_CD'] = DMsql1df['WRKSITE_DISBLTY_BSC_EARN_TYP_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['BSC_EARN_AVG_PER_TYP_DESCR'] = DMsql1df['BSC_EARN_AVG_PER_TYP_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['SURV_BEN_PER_TYP_DESCR'] = DMsql1df['SURV_BEN_PER_TYP_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['TOTAL_DISBLTY_JOB_CLSFCTN_TYP_CD'] = DMsql1df['TOTAL_DISBLTY_JOB_CLSFCTN_TYP_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['BSC_EARN_AVG_PER_TYP_CD'] = DMsql1df['BSC_EARN_AVG_PER_TYP_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]
        DMsql1df['SURV_BEN_PER_TYP_CD'] = DMsql1df['SURV_BEN_PER_TYP_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['WDS','WDL'])]                    
        
        polnumber1 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]]
        strpolnumber1 =  ','.join(map(str, polnumber1))
        strpolnumber1 = strpolnumber1[:-1]
        strpolnumber1 = strpolnumber1.replace(',NON', '')
        
        polnumber2 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]]
        strpolnumber2 =  ','.join(map(str, polnumber2))
        strpolnumber2 = strpolnumber2[:-1]
        
        polnumber3 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]]
        strpolnumber3 =  ','.join(map(str, polnumber3))
        strpolnumber3 = strpolnumber3[:-1]
        
        #polnumber2 = ','.join('?' for i in range(len(DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12])))  # '?,?'
        #polnumber3 = ','.join('?' for i in range(len(DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15])))  # '?,?'
        
        '''
         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then
                             case when GNDRALDURT = 'D' then 'DAYS'
                             when GNDRALDURT = 'M' then 'MONTHS'
                             when GNDRALDURT = 'W' then 'WEEKS'
                             when GNDRALDURT = 'Y' then 'YEARS'
                             WHEN GNDRALDURT = ' ' then 'NONE'
                             End 
                         else
                             case when GNDRALDURT <> '' then 'NONE' end
                         End as GNDRALDURT
                         '''
        
                         
        LZsql = ('''select GNPOLNUM1,  
        
                        case when len(GNPOLNUM2) = 1 then concat('000',GNPOLNUM2)
                        when len(GNPOLNUM2) = 2 then concat('00',GNPOLNUM2)
                        when len(GNPOLNUM2) = 3 then concat('0',GNPOLNUM2)
                        end as GNPOLNUM2,
        
                        case when len(GNPOLNUM3) = 1 then concat('00',GNPOLNUM3)
                        when len(GNPOLNUM3) = 2 then concat('0',GNPOLNUM3)
                        end as GNPOLNUM3
                        
                        ,ELIMPERD,ELIMTYPE,GNDRALDUR,GNPLANTYP,GNEPINJDUR,GNEPSKDUR, GNEPSKDURT, RTNTOWRK,GNTOTDDD,
                         GNMDMIDUR,GNMDMIDURT,GNOPTIONNB,GNSSI,FTAVGPERD,PXPDPREFD,PXPDTFREE,PXPDCNDNC,GNSALCONT,
                         GNBEERNTYP,GNOCCINJSC,DTHPRSPCT,MNTILLPCT,DRGALCPCT,SPCNDLPCT,GNBEAVG,GNPARTINCT,
                         
                         GNSINCTYP,GNSPRINCT,GNSRVBEND,GNBEINCTYP,GNTODISDEF,
        
                         Rtrim(GNMXBNDURI) as GNMXBNDURI ,rtrim(GNMXBNDURS) as GNMXBNDURS,Rtrim(GNFTCONTHR) as GNFTCONTHR,
                         
                         
                         case when GNPLANTYP = 'ODS' then DEFOFDISB else Null End as DEFOFDISB,
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNDRALDURT else Null End as GNDRALDURT,
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNEPINJDRT else Null End as GNEPINJDRT,
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL'  then RTNTOWRKT else Null End as RTNTOWRKT,
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNTOTDDDT else Null End as GNTOTDDDT,
                         
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then FTAVGTYPE else Null End as FTAVGTYPE ,               
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then RTWDUR else null End as RTWDUR,
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then FTREQUIRE else Null End as FTREQUIRE ,    
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then FTREQTYPE else Null End as FTREQTYPE, 
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNSRVBENDT else Null End as GNSRVBENDT,
                         
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNBEAVGT else Null End as GNBEAVGT,
                         
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then PXPDTFREE else Null End as PXPDTFREE1 , 
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then PXPDCNDNC else Null End as PXPDCNDNC1 , 
                         case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' or GNPLANTYP = 'ODS' then PXPDPREFD else Null End as PXPDPREFD1,
                         
                         case when GNPLANTYP = 'OLS' then RTWTYP when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNRCDISDUT else NUll end as RTWTYP1                        
                         
                                       
                         from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp ''') #where GNPOLNUM1 in(''' + strpolnumber1 + ''') And GNPOLNUM2 in (''' +strpolnumber2 + ''') and GNPOLNUM3 in (''' + strpolnumber3 + ''')
                         
                         
                         #''')
          
          #TT_TO_FROM_VALUE,VOLRDCTBL,FTAUTIP_USER_TABLE_DESC,
          # 
          #
        #print(LZsql)       
             
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        #LZsql2df = LZsql2df.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
        
        LZsql2df['ELIMTYPE'] = LZsql2df['ELIMTYPE'].map({'D':'DAYS','M':'MONTHS',' ':'NONE'}) 
        array = ['WDS', 'WDL']
        LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'NONE'})
        LZsql2df['GNEPINJDRT'] = LZsql2df['GNEPINJDRT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'NONE'})
        LZsql2df['GNEPSKDURT'] = LZsql2df['GNEPSKDURT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'NONE'})
        LZsql2df['RTNTOWRKT'] = LZsql2df['RTNTOWRKT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'NONE'})
        LZsql2df['GNTOTDDDT'] = LZsql2df['GNTOTDDDT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'BLANK'})
        
        #LZsql2df['GNMDMIDURT'] = LZsql2df['GNMDMIDURT'].map({' ': ''})
        LZsql2df['GNMDMIDURT'] = LZsql2df['GNMDMIDURT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'DAYS'})
        LZsql2df['GNBEAVGT1'] = LZsql2df['GNBEAVGT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'DAYS'})
        
        #---4/3/2020 - LZsql2df['GNSRVBENDT'] = LZsql2df['GNSRVBENDT'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'NONE'})
        
        #LZsql2df['FTAVGTYPE1'] = LZsql2df['FTAVGTYPE1'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS',' ':'NONE'})
        
        #html_logger.info("End of 2nd query")   
        
        #LZsql2df = LZsql2df[LZsql2df.GNPOLNUM1.isin(DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]) & (LZsql2df.GNPOLNUM2.isin(DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12])) & (LZsql2df.GNPOLNUM3.isin(DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]))]
        
        cols = [['PARTNERSHIP_INCM_TYP_CD','GNPARTINCT','String'],['LUMP_SUM_ELIM_PER_NBR','ELIMPERD','String'],['OLS_ELIM_PER_TYP_DESCR','ELIMTYPE','String']
                ,['WRKSITE_DRUG_ALCOL_LIMIT_PER_CD','GNDRALDUR','String'],['WRKSITE_BEN_INJRY_ELIM_PER_CD','GNEPINJDUR','float']
                ,['WRKSITE_BEN_INJRY_ELIM_PER_DESCR','GNEPINJDRT','String'],['WRKSITE_BEN_SICK_ELIM_PER_CD','GNEPSKDUR','String'],['WRKSITE_BEN_SICK_ELIM_PER_DESCR','GNEPSKDURT','String']
                ,['WRKSITE_RTRN_TO_WRK_BEN_PER_CD','RTNTOWRK','float'],['TOT_DISBLTY_DUR_TYP_CD','GNTOTDDD','String']
                ,['TOT_DISBLTY_DUR_TYP_DESCR','GNTOTDDDT','String'],['MNTL_ILL_BEN_DUR_TYP_CD','GNMDMIDUR','String'],['MNTL_ILL_BEN_DUR_TYP_DESCR','GNMDMIDURT','String']
                ,['WRKSITE_DISBLTY_OPT_NBR','GNOPTIONNB','String'],['OTH_INCM_INTGRTN_CD','GNSSI','String'],['MAX_INJRY_BEN_DUR_CD','GNMXBNDURI','String']
                ,['MAX_SICK_BEN_DUR_CD','GNMXBNDURS','String'],['FULL_TM_STAT_AVG_PER_NBR','FTAVGPERD','float'],['FULL_TM_CNTRCT_IND','GNFTCONTHR','String']
                ,['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PXPDPREFD','float'],['POST_EFF_DT_TRTMT_FREE_PER_NBR','PXPDTFREE','float']
                ,['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PXPDCNDNC','float'],['SLRY_CONT_TYP_CD','GNSALCONT','String'],['WRKSITE_DISBLTY_BSC_EARN_TYP_CD','GNBEERNTYP','String'],
                ['OCPTN_DISBLTY_CD','GNOCCINJSC','String'],['OLS_DEATH_BEN_PCT','DTHPRSPCT','String'],['OLS_MNTL_ILL_BEN_LMT_PCT','MNTILLPCT','String'],
                ['OLS_SUBS_ABUSE_BEN_LMT_PCT','DRGALCPCT','String'],['OLS_SPEC_COND_BEN_LMT_PCT','SPCNDLPCT','String'],['BSC_EARN_AVG_PER_NBR','GNBEAVG','String'],['PARTNERSHIP_INCM_TYP_CD','GNPARTINCT','String'],
                
                
                ['S_CORP_INCM_TYP_CD','GNSINCTYP','String'],['SOLE_PROP_INCM_TYP_CD','GNSPRINCT','String'],
                ['BSC_EARN_AVG_PER_TYP_DESCR','GNBEAVGT1','String'],['BSC_EARN_AVG_PER_TYP_CD','GNBEAVGT','String'],
               ['SURV_BEN_PER_NBR','GNSRVBEND','String'],['BSC_EARN_INCM_TYP_CD','GNBEINCTYP','String'],['TOTAL_DISBLTY_JOB_CLSFCTN_TYP_CD','GNTODISDEF','String'],
                
                
                
                
                ] #['OTH_INCM_INTGRTN_DESCR','TT_TO_FROM_VALUE','String'],['BEN_RDCTN_CD','VOLRDCTBL','String'],['BEN_RDCTN_DESCR','FTAUTIP_USER_TABLE_DESC','String'],['SURV_BEN_PER_TYP_DESCR','GNSRVBENDT','String'],
        
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------------------------
        
        DMsql = '''select a.FULL_TM_STAT_QLFCTN_THRSHLD_NBR from dbo.WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('OLS','WDS','WDL') '''  #where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        #DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'] = DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        #DMsql1df['Ben_plan_nbr'] = DMsql1df['Ben_plan_nbr'].replace(0.0,np.nan).dropna()
        
        LZsql = ('''SELECT Case When FTHOURS <> 0 THEN FTHOURS When FTREQUIRE <> 0 THEN FTREQUIRE ELSE 0 END AS LZ_FULL_TM_STAT_QLFCTN_THRSHLD_TYP_NBR
                    FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP where GNPLANTYP in ('OLS', 'WDS' , 'WDL') AND (FTHOURS is not null or FTREQUIRE is not null) ''')  #where WF_EMAIL_ADDR <> '' 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['FULL_TM_STAT_QLFCTN_THRSHLD_NBR','LZ_FULL_TM_STAT_QLFCTN_THRSHLD_TYP_NBR','String']]   
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------------------------
        
        DMsql = '''select a.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD from dbo.WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('OLS','WDS','WDL') '''  #where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        #DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'] = DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        #DMsql1df['Ben_plan_nbr'] = DMsql1df['Ben_plan_nbr'].replace(0.0,np.nan).dropna()
        
        LZsql = ('''SELECT CASE when LTRIM(RTRIM(FTHRTYPE))='N' THEN 'N' When FTHOURS <> 0 THEN LTRIM(RTRIM(FTHRTYPE)) When FTREQUIRE<> 0 THEN CONCAT('W',COALESCE(LTRIM(RTRIM(FTREQTYPE)),''),'IY') END AS LZ_FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD
                    FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP where GNPLANTYP in ('OLS', 'WDS' , 'WDL') AND (FTHOURS is not null or FTREQUIRE is not null) ''')  #where WF_EMAIL_ADDR <> '' 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','LZ_FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','String']]   
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
         #----------------------------------------------------------------
        html_logger.info("Start of Comparing GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp vs dbo.WRKSITE_OLS_COV_FACT ")   
        
        DMsql = '''select a.SURV_BEN_PER_TYP_DESCR,a.RTRN_TO_WRK_PER_NBR,a.SURV_BEN_PER_TYP_CD from dbo.WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''SELECT CASE WHEN GNSRVBEND=0 THEN 'Days' WHEN GNSRVBENDT='D' THEN 'Days' WHEN GNSRVBENDT='M' THEN 'Months'
                    WHEN GNSRVBENDT='Q' THEN 'Quarters' WHEN GNSRVBENDT='W' THEN 'Weeks' WHEN GNSRVBENDT='Y' THEN 'Years'
                     END AS SURV_BEN_PER_TYP_DESCR, GNRCDISDUR,
                     CASE WHEN GNSRVBEND=0 THEN 'D' WHEN GNSRVBENDT='D' THEN 'D' WHEN GNSRVBENDT='M' THEN 'M'
                    WHEN GNSRVBENDT='Q' THEN 'Q' WHEN GNSRVBENDT='W' THEN 'W' WHEN GNSRVBENDT='Y' THEN 'Y'
                     END AS BSC_EARN_AVG_PER_TYP_CD                     
                      FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP
                    WHERE GNSRVBEND IS NOT NULL and GNPLANTYP in ('WDL','WDS')          
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        
        cols = [['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['RTRN_TO_WRK_PER_NBR','GNRCDISDUR','float'],['SURV_BEN_PER_TYP_CD','BSC_EARN_AVG_PER_TYP_CD','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
          #----------------------------------------------------------------
        html_logger.info("Start of Comparing GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp vs dbo.WRKSITE_OLS_COV_FACT ")   
        
        DMsql = '''select a.SURV_BEN_PER_TYP_DESCR,WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR,WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR from dbo.WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''SELECT CASE WHEN GNSRVBEND=0 THEN 'Days' WHEN GNSRVBENDT='D' THEN 'Days' WHEN GNSRVBENDT='M' THEN 'Months'
                    WHEN GNSRVBENDT='Q' THEN 'Quarters' WHEN GNSRVBENDT='W' THEN 'Weeks' WHEN GNSRVBENDT='Y' THEN 'Years'
                     END AS SURV_BEN_PER_TYP_DESCR,GNDRALDURT,RTNTOWRKT FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP
                    WHERE GNSRVBEND IS NOT NULL and GNPLANTYP in ('WDL','WDS')          
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({'':'DAYS','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        LZsql2df['RTNTOWRKT'] = LZsql2df['RTNTOWRKT'].map({'':'','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        cols = [['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR','GNDRALDURT','String'],['WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR','RTNTOWRKT','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #***********************************************************************************************************************
        
        DMsql = '''select a.OLS_QLFCTN_DISBLTY_TYP_CD from dbo.WRKSITE_OLS_COV_FACT a INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' select DEFOFDISB FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP WHERE  GNPLANTYP in ('OLS')         
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        #LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({' ':'DAYS','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        cols = [['OLS_QLFCTN_DISBLTY_TYP_CD','DEFOFDISB','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
         #***********************************************************************************************************************
        
        DMsql = '''select FULL_TM_STAT_AVG_PER_TYP_CD from dbo.WRKSITE_OLS_COV_FACT a INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL','OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT FTAVGTYPE FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP
                WHERE GNPLANTYP in ('WDL','WDS','OLS')        
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['FTAVGTYPE'] = LZsql2df['FTAVGTYPE'].map({' ':'D'})
        cols = [['FULL_TM_STAT_AVG_PER_TYP_CD','FTAVGTYPE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #-------------------------------------------------------------------------------------------------------------
        DMsql = '''select a.POST_EFF_DT_TRTMT_FREE_PER_TYP_CD,a.POST_EFF_DT_TRTMT_FREE_PER_DESCR from dbo.WRKSITE_OLS_COV_FACT a INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL','OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' select PXPDTTYPE from ( SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],concat([GNPHPEFCC],right([GNPHPEFMDY],2) ) as EFFYears ,
                CASE len([GNPHPEFMDY]) When 5 then concat('0',left([GNPHPEFMDY],3)) ELSE left([GNPHPEFMDY],4) END as Eff_MMDD ,PXPDTFREE as PER_NBR,case when PXPDTFREE=0 then 'D' ELSE PXPDTTYPE END AS PXPDTTYPE
                FROM (SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],LTRIM(RTRIM([GNPHPEFCC])) AS [GNPHPEFCC],LTRIM(RTRIM([GNPHPEFMDY])) AS [GNPHPEFMDY],PXPDTFREE
                ,PXPDTTYPE from [GAIN].[LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP] WHERE GNPLANTYP in ('WDS', 'WDL','OLS')) X) Q1       
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['PXPDTTYPE1'] = LZsql2df['PXPDTTYPE'].map({' ':'DAYS','M':'MONTHS','D':'DAYS','Y':'YEARS','W':'WEEKS'})
        cols = [['POST_EFF_DT_TRTMT_FREE_PER_TYP_CD','PXPDTTYPE','String'],['POST_EFF_DT_TRTMT_FREE_PER_DESCR','PXPDTTYPE1','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #-------------------------------------------------------------------------------------------------------------
        DMsql = '''select a.PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD,a.PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR from dbo.WRKSITE_OLS_COV_FACT a INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL','OLS') 
                 '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' select PXPDPREFT from (SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],
                concat([GNPHPEFCC],right([GNPHPEFMDY],2) ) as EFFYears ,CASE len([GNPHPEFMDY]) When 5 then concat('0',left([GNPHPEFMDY],3)) ELSE left([GNPHPEFMDY],4) END as Eff_MMDD
                ,PXPDPREFD as PER_NBR,case when PXPDPREFD=0 then 'D' ELSE PXPDPREFT END AS PXPDPREFT FROM(
                SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],LTRIM(RTRIM([GNPHPEFCC])) AS [GNPHPEFCC],LTRIM(RTRIM([GNPHPEFMDY])) AS [GNPHPEFMDY]
                ,PXPDPREFD,PXPDPREFT from [GAIN].[LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP] WHERE GNPLANTYP in ('WDS', 'WDL','OLS')) X) Q1       
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['PXPDPREFT1'] = LZsql2df['PXPDPREFT'].map({' ':'DAYS','M':'MONTHS','D':'DAYS','Y':'YEARS','W':'WEEKS'})
        
        cols = [['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PXPDPREFT','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PXPDPREFT1','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #-------------------------------------------------------------------------------------------------------------
        DMsql = '''select a.RTRN_TO_WRK_PER_TYP_CD,a.RTRN_TO_WRK_PER_TYP_DESCR from WRKSITE_OLS_COV_FACT a INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL','OLS')
                 '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' select PER_TYP_CD from(SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],concat([GNPHPEFCC],right([GNPHPEFMDY],2) ) as EFFYears ,
                CASE len([GNPHPEFMDY]) When 5 then concat('0',left([GNPHPEFMDY],3)) ELSE left([GNPHPEFMDY],4) END as Eff_MMDD
                ,PER_NBR ,case when PER_NBR=0 then 'D' ELSE PER_TYP_CD END AS PER_TYP_CD
                 FROM(SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],LTRIM(RTRIM([GNPHPEFCC])) AS [GNPHPEFCC]
                ,LTRIM(RTRIM([GNPHPEFMDY])) AS [GNPHPEFMDY],CASE WHEN GNPLANTYP='OLS' THEN RTWDUR WHEN GNPLANTYP IN ('WDL','WDS') THEN GNRCDISDUR
                END AS PER_NBR,CASE WHEN GNPLANTYP='OLS' THEN RTWTYP WHEN GNPLANTYP IN ('WDL','WDS') THEN GNRCDISDUT END AS PER_TYP_CD
                from [GAIN].[LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP]WHERE GNPLANTYP in ('WDS', 'WDL','OLS')) X) Q1       
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['PER_TYP_CD1'] = LZsql2df['PER_TYP_CD'].map({' ':'DAYS','M':'MONTHS','D':'DAYS','Y':'YEARS','W':'WEEKS'})
        
        cols = [['RTRN_TO_WRK_PER_TYP_CD','PER_TYP_CD','String'],['RTRN_TO_WRK_PER_TYP_DESCR','PER_TYP_CD1','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)


def compare_partyplanfact_GNBANSphp():
        #-------------------------------------------------------------------------------------------------------------
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.PARTY_PLAN_FACT vs GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp")   
        
        DMsql = '''select C.Ben_Plan_typ_cd,B.ADMIN_SYS_PARTY_KEY_TXT,A.DRUG_ALCOL_ABUSE_BEN_LIMIT_IND,A.FRST_DAY_HOSP_BEN_IND,A.FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND,
        A.MNTL_ILL_LIMIT_IND,A.ERISA_APPL_CD,A.PLCYHLDR_PLAN_CLS_DESCR,A.DISBLTY_ELIM_PER_QLFCTN_IND,A.SPL_DISBLTY_COND_LMT_IND,A.SPL_COND_BEN_DUR_TYP_CD,
        A.SPL_COND_BEN_DUR_TYP_DESCR,A.COST_OF_LVNG_BEN_ADJ_PER_CD,A.COST_OF_LVNG_BEN_ADJ_PER_DESCR,A.COST_OF_LVNG_BEN_ADJ_TYP_CD,A.DBL_ELIM_PER_IND,
        A.SPS_DISBLTY_BEN_ELIM_PER_CD,A.SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR,A.SPS_DISBLTY_MAX_MTHLY_BEN_AMT,A.SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD,
        A.SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR,A.PREM_WVR_CD
        
        from dbo.PARTY_PLAN_FACT A inner join dbo.Party_Dim B on (B.PARTY_DIM_GEN_ID= A.PARTY_DIM_GEN_ID)
        inner join ben_plan_dim c on (A.BEN_PLAN_DIM_GEN_ID = C.BEN_PLAN_DIM_GEN_ID) where A.PM_CUR_IND = 'Y' '''
        
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['COST_OF_LVNG_BEN_ADJ_PER_CD'] = DMsql1df['COST_OF_LVNG_BEN_ADJ_PER_CD'].replace('NONE','DAYS').replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'] = DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'].replace('NONE','DAYS').replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['PREM_WVR_CD'] = DMsql1df['PREM_WVR_CD'].replace('NO',np.nan).replace('',np.nan).dropna()
        DMsql1df['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR'] = DMsql1df['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR'].replace('NONE','DAYS').replace('NON',np.nan).replace('',np.nan).dropna()
        
        DMsql1df['SPL_COND_BEN_DUR_TYP_CD'] = DMsql1df['SPL_COND_BEN_DUR_TYP_CD'][DMsql1df.Ben_Plan_typ_cd.isin(['CLC', 'CLP', 'LTD', 'VLD', 'VMD', 'VSD', 'WDL', 'WDS'])]
        DMsql1df['SPL_COND_BEN_DUR_TYP_DESCR'] = DMsql1df['SPL_COND_BEN_DUR_TYP_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['CLC', 'CLP', 'LTD', 'VLD', 'VMD', 'VSD', 'WDL', 'WDS'])]
        DMsql1df['COST_OF_LVNG_BEN_ADJ_PER_DESCR'] = DMsql1df['COST_OF_LVNG_BEN_ADJ_PER_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['OLS']) == False]
        DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'] = DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['OLS']) == False]
        DMsql1df['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR'] = DMsql1df['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR'][DMsql1df.Ben_Plan_typ_cd.isin(['OLS']) == False]
        
        
        #html_logger.info("End of 1st query")   
       
        """ 
        polnumber1 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]]
        strpolnumber1 =  ','.join(map(str, polnumber1))
        strpolnumber1 = strpolnumber1[:-1]
        strpolnumber1 = strpolnumber1.replace(',NON', '')
        
        polnumber2 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]]
        strpolnumber2 =  ','.join(map(str, polnumber2))
        strpolnumber2 = strpolnumber2[:-1]
        
        polnumber3 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]]
        strpolnumber3 =  ','.join(map(str, polnumber3))
        strpolnumber3 = strpolnumber3[:-1]
        """
        strpolnumber1 =  DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]
        strpolnumber2 =  DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]
        strpolnumber3 =  DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]
        
        LZsql = ('''select GNPLANTYP,GNPOLNUM1,GNPOLNUM2,GNPOLNUM3,GNDRALINC,GNFDHOSP,GNFDHOSPOS,GNMDMIINC,PHPERISA,GNCLSDES1, GNCLSDES2, GNCLSDES3,GNRESIDBEN,GNSPCCNDL,GNSPCCNDUR,GNSPCCNDTY,
                    GNCOSTPER,GNCOSTYPE,GNNETGROSS,GN2XELIM,GNSELMPER,GNSELMTYP,GNSPDISMBN,GNSPLIFMAX,GNSPLIFTYP,GNWAIVER
        
                    from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp ''') 
                         
          
          #TT_TO_FROM_VALUE,VOLRDCTBL,FTAUTIP_USER_TABLE_DESC,case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNBEAVGT else Null End as GNBEAVGT
          # where GNPOLNUM1 in(''' + strpolnumber1 + ''') And GNPOLNUM2 in (''' +strpolnumber2 + ''') and GNPOLNUM3 in (''' + strpolnumber3 + ''')
          #
        #print(LZsql)       
             
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df = LZsql2df[(LZsql2df['GNPOLNUM1'].isin(strpolnumber1))]
        #LZsql2df['GNCLSDEScription'] = LZsql2df.apply(lambda x:'%s %s %s' % (x['GNCLSDES1'][0:49],x['GNCLSDES2'][0:49],x['GNCLSDES3'][0:49]),axis=1)
        LZsql2df['GNCLSDEScription'] = LZsql2df.apply(lambda x:'%s %s %s' % (x['GNCLSDES1'],x['GNCLSDES2'],x['GNCLSDES3']),axis=1)
        LZsql2df['GNCLSDEScription'] = LZsql2df['GNCLSDEScription'].str.strip()

        LZsql2df['GNSPCCNDUR'] = LZsql2df['GNSPCCNDUR'][LZsql2df.GNPLANTYP.isin(['CLC', 'CLP', 'LTD', 'VLD', 'VMD', 'VSD', 'WDL', 'WDS'])] 
        LZsql2df['GNSPCCNDTY'] = LZsql2df['GNSPCCNDTY'][LZsql2df.GNPLANTYP.isin(['CLC', 'CLP', 'LTD', 'VLD', 'VMD', 'VSD', 'WDL', 'WDS'])]
        
        LZsql2df['GNSPCCNDUR1'] = LZsql2df['GNSPCCNDUR'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNCOSTPER'] = LZsql2df['GNCOSTPER'].replace('NON',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNCOSTYPE'] = LZsql2df['GNCOSTYPE'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'DAYS'})
        LZsql2df['GNSELMTYP'] = LZsql2df['GNSELMTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'DAYS'})
        LZsql2df['GNSPLIFTYP'] = LZsql2df['GNSPLIFTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'DAYS'})
        
        LZsql2df['GNSPCCNDTY'] = LZsql2df['GNSPCCNDTY'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE','':np.nan})
        
        LZsql2df['GNWAIVER'] = LZsql2df['GNWAIVER'].replace('NO',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNSELMTYP'] = LZsql2df['GNSELMTYP'].replace('NO',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNSPCCNDTY'] = LZsql2df['GNSPCCNDTY'].replace('NO',np.nan).replace('',np.nan).dropna()
         
        
        cols = [['DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','GNDRALINC','String'],['FRST_DAY_HOSP_BEN_IND','GNFDHOSP','String'],['FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','GNFDHOSPOS','String'],
                ['MNTL_ILL_LIMIT_IND','GNMDMIINC','String'],['ERISA_APPL_CD','PHPERISA','String'],['PLCYHLDR_PLAN_CLS_DESCR','GNCLSDEScription','String'],
                ['DISBLTY_ELIM_PER_QLFCTN_IND','GNRESIDBEN','String'],['SPL_DISBLTY_COND_LMT_IND','GNSPCCNDL','String'],
                ['SPL_COND_BEN_DUR_TYP_DESCR','GNSPCCNDTY','String'],['COST_OF_LVNG_BEN_ADJ_PER_CD','GNCOSTPER','float'],['COST_OF_LVNG_BEN_ADJ_PER_DESCR','GNCOSTYPE','String'],
                ['COST_OF_LVNG_BEN_ADJ_TYP_CD','GNNETGROSS','string'],['DBL_ELIM_PER_IND','GN2XELIM','String'],['SPS_DISBLTY_BEN_ELIM_PER_CD','GNSELMPER','String'],
                ['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','GNSELMTYP','String'],['SPS_DISBLTY_MAX_MTHLY_BEN_AMT','GNSPDISMBN','String'],['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','GNSPLIFMAX','String'],
                ['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','GNSPLIFTYP','String'],['PREM_WVR_CD','GNWAIVER','float']
                ]
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        

        DMsql = '''select A.SPL_COND_BEN_DUR_TYP_CD from dbo.PARTY_PLAN_FACT A
        inner join ben_plan_dim c on (A.BEN_PLAN_DIM_GEN_ID = C.BEN_PLAN_DIM_GEN_ID)
        where C.Ben_Plan_typ_cd in ('CLC', 'CLP', 'LTD', 'VLD', 'VMD', 'VSD', 'WDL', 'WDS') and A.PM_CUR_IND = 'Y' ''' 
        
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        LZsql = ('''select GNSPCCNDUR from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp where GNPLANTYP in  ('CLC', 'CLP', 'LTD', 'VLD', 'VMD', 'VSD', 'WDL', 'WDS') ''') 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols=[['SPL_COND_BEN_DUR_TYP_CD','GNSPCCNDUR','float']]
        DFCompare.compare(DMsql1df,LZsql2df,cols)

def compare_partyplanfact_FTAPHAPL():
        #-------------------------------------------------------------------------------------------------------------
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.PARTY_PLAN_FACT vs GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp")   
        
        DMsql = '''select B.ADMIN_SYS_PARTY_KEY_TXT,A.DISBLTY_BEN_FREQ_TYP_DESCR,A.INIT_EMP_COV_QLFCTN_TYP_DESCR,A.NEW_EMP_COV_QLFCTN_TYP_DESCR,
        A.FIX_COV_BEN_AMT,A.MAX_COV_BEN_AMT,A.MIN_COV_BEN_AMT,A.COLA_CD,A.COLA_MAX_OCCUR_NBR,A.COLA_OCCUR_TYP,A.INSUR_INCR_UNIT_AMT,A.PLCYHLDR_PLAN_CLS_CD,
        A.DISBLTY_BEN_SLRY_PCT,A.DISBLTY_MIN_NET_BEN_PCT,A.MIN_NET_BEN_AMT,A.DISBLTY_BEN_FREQ_CD,A.DISBLTY_BEN_FREQ_TYP_CD,A.INIT_EMP_WAIT_PER_TYP_CD,
        A.INIT_EMP_WAIT_PER_TYP_DESCR,A.INIT_EMP_COV_QLFCTN_TYP_CD,A.NEW_EMP_WAIT_PER_TYP_CD,A.NEW_EMP_WAIT_PER_TYP_DESCR,A.NEW_EMP_COV_QLFCTN_TYP_CD,A.EMPLR_CONTRIB_PCT,A.COLA_OCCUR_TYP_DESCR
        
        from dbo.PARTY_PLAN_FACT A inner join dbo.Party_Dim B on (B.PARTY_DIM_GEN_ID= A.PARTY_DIM_GEN_ID)'''
        
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
       
        DMsql1df['COLA_CD'] = DMsql1df['COLA_CD'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['COLA_MAX_OCCUR_NBR'] = DMsql1df['COLA_MAX_OCCUR_NBR'].replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['INIT_EMP_COV_QLFCTN_TYP_DESCR'] = DMsql1df['INIT_EMP_COV_QLFCTN_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['DISBLTY_BEN_FREQ_TYP_DESCR'] = DMsql1df['DISBLTY_BEN_FREQ_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['NEW_EMP_COV_QLFCTN_TYP_DESCR'] = DMsql1df['NEW_EMP_COV_QLFCTN_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['NEW_EMP_WAIT_PER_TYP_DESCR'] = DMsql1df['NEW_EMP_WAIT_PER_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['COLA_OCCUR_TYP_DESCR'] = DMsql1df['COLA_OCCUR_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['INIT_EMP_WAIT_PER_TYP_DESCR'] = DMsql1df['INIT_EMP_WAIT_PER_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['COLA_OCCUR_TYP'] =  DMsql1df['COLA_OCCUR_TYP'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        DMsql1df['DISBLTY_BEN_FREQ_CD'] =  DMsql1df['DISBLTY_BEN_FREQ_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        DMsql1df['DISBLTY_BEN_FREQ_TYP_CD'] =  DMsql1df['DISBLTY_BEN_FREQ_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        
        
        TmpDMsql = '''select distinct B.ADMIN_SYS_PARTY_KEY_TXT        
        from dbo.PARTY_PLAN_FACT A inner join dbo.Party_Dim B on (B.PARTY_DIM_GEN_ID= A.PARTY_DIM_GEN_ID) where len(B.ADMIN_SYS_PARTY_KEY_TXT) = 15'''
        TmpDMsql1df = Parser.parseSQL(TmpDMsql,DMserver,DMdbname)
        
        '''
        DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'] = DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'].replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['PREM_WVR_CD'] = DMsql1df['PREM_WVR_CD'].replace('NO',np.nan).replace('',np.nan).dropna()
        '''
        #html_logger.info("End of 1st query")   
       
        """
        polnumber1 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]]
        strpolnumber1 =  ','.join(map(str, polnumber1))
        strpolnumber1 = strpolnumber1[:-1]
        strpolnumber1 = strpolnumber1.replace(',NON', '')
        
        polnumber2 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]]
        strpolnumber2 =  ','.join(map(str, polnumber2))
        strpolnumber2 = strpolnumber2[:-1]
        
        polnumber3 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]]
        strpolnumber3 =  ','.join(map(str, polnumber3))
        strpolnumber3 = strpolnumber3[:-1]
        """
        
        strpolnumber1 =  TmpDMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]
        strpolnumber2 =  TmpDMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]
        strpolnumber3 =  TmpDMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]
        
        listpolnum1 = strpolnumber1.values.tolist()
        listpolnum2 = strpolnumber2.values.tolist()
        listpolnum3 = strpolnumber3.values.tolist()
       
        
        iternum = 0
        for i in listpolnum1:
            polnum1 = i
            polnum2 = listpolnum2[iternum]
            polnum3 = listpolnum3[iternum]
            iternum = iternum + 1
            
            LZsql = ('''select GNPLANTYP,PHMAIN,VOLFLTAMT,VOLMXAMT,VOLMINAMT,DSCOLAPCT,DSCOLAMXDR,DSCOLADRTP,VOLINCAMT,CLSCD,DSSALPCT,DSMINPCT,
                        DSMINNTBFT,INITEWP,INITEWPTYP,NEWEWP,NEWEWPTYP,EMPRCTBPCT
                        
                        
                             
                        from GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL A
                      inner join GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B on(A.PHMAIN=B.GNPOLNUM1 and A.PHBRN = B.GNPOLNUM2 and A.PHDEPT = B.GNPOLNUM3) 
                      where GNPOLNUM1 = ''' + polnum1 + ''' And GNPOLNUM2 = ''' + polnum2 + ''' and GNPOLNUM3 = ''' + polnum3 )
                      
            tmpLZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname) 
            if(iternum==1):
                 LZsql2df =  tmpLZsql2df
            else:                
                LZsql2df = LZsql2df.append(tmpLZsql2df)
          #TT_TO_FROM_VALUE,VOLRDCTBL,FTAUTIP_USER_TABLE_DESC,case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNBEAVGT else Null End as GNBEAVGT
          # where GNPOLNUM1 in(''' + strpolnumber1 + ''') And GNPOLNUM2 in (''' +strpolnumber2 + ''') and GNPOLNUM3 in (''' + strpolnumber3 + ''')
          #case when GNPLANTYP <> 'OLS' then DSBFTFRQ else Null End as DSBFTFRQ,  
          #             case when GNPLANTYP <> 'OLS' then DSBFTFRQTP else Null End as DSBFTFRQTP,
          #              case when GNPLANTYP <> 'WDS' or GNPLANTYP <> 'WDL' then INITEWPEFF else Null End as INITEWPEFF,
          #              case when GNPLANTYP <> 'WDS' or GNPLANTYP <> 'WDL' then NEWEWPEFF else Null End as NEWEWPEFF  
        #print(LZsql)       
        #LZsql2df = LZsql2df[(LZsql2df['PHMAIN'].isin(strpolnumber1))]
        
 
        '''
        LZsql2df['GNCLSDEScription'] = LZsql2df.apply(lambda x:'%s %s %s' % (x['GNCLSDES1'],x['GNCLSDES2'],x['GNCLSDES3']),axis=1)
        
        LZsql2df['GNSPCCNDUR'] = LZsql2df['GNSPCCNDUR'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNCOSTPER'] = LZsql2df['GNCOSTPER'].replace('NON',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNCOSTYPE'] = LZsql2df['GNCOSTYPE'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNSELMTYP'] = LZsql2df['GNSELMTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNSPLIFTYP'] = LZsql2df['GNSPLIFTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        
        LZsql2df['GNSPCCNDTY'] = LZsql2df['GNSPCCNDTY'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE','':np.nan})
        
        LZsql2df['GNWAIVER'] = LZsql2df['GNWAIVER'].replace('NO',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNSELMTYP'] = LZsql2df['GNSELMTYP'].replace('NO',np.nan).replace('',np.nan).dropna()
        
         '''
        LZsql2df['DSBFTFRQTP'] = LZsql2df['DSBFTFRQTP'].map({'7':'7 DAY WORK WEEK','T':'THIRTY DAYS','5':'5 DAY WORK WEEK','':'NONE'})
        LZsql2df['INITEWPEFF'] = LZsql2df['INITEWPEFF'].map({'I':'IMMEDIATE','F':'FIRST OF THE MONTH','':'NONE'})
        LZsql2df['NEWEWPEFF'] = LZsql2df['NEWEWPEFF'].map({'I':'IMMEDIATE','F':'FIRST OF THE MONTH','':'NONE'})
        LZsql2df['INITEWPTYP'] = LZsql2df['INITEWPTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['NEWEWPTYP'] = LZsql2df['NEWEWPTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        #LZsql2df['DSCOLADRTP'] = LZsql2df['DSCOLADRTP'].map({'A':'ADJUSTMENTS','Y':'YEARS','':'NONE'})
        LZsql2df['DSCOLADRTP'] = LZsql2df['DSCOLADRTP'].replace('NONE',np.nan).replace('',np.nan).dropna()
        LZsql2df['DSCOLADRTP'] = LZsql2df['DSCOLAMXDR'].map({'':'N','5':'Y','10':'Y','65':'A','99':'A'})
        
        
        LZsql2df['DSCOLAPCT'] = LZsql2df['DSCOLAPCT'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        
        cols = [['DISBLTY_BEN_FREQ_TYP_DESCR','DSBFTFRQTP','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INITEWPEFF','String'],['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEWEWPEFF','String'],
                ['FIX_COV_BEN_AMT','VOLFLTAMT','String'],['MAX_COV_BEN_AMT','VOLMXAMT','String'],['MIN_COV_BEN_AMT','VOLMINAMT','String'],['COLA_CD','DSCOLAPCT','String'],
                ['COLA_MAX_OCCUR_NBR','DSCOLAMXDR','float'],['COLA_OCCUR_TYP','DSCOLADRTP','String'],['INSUR_INCR_UNIT_AMT','VOLINCAMT','String'],['PLCYHLDR_PLAN_CLS_CD','CLSCD','String'],
                ['DISBLTY_BEN_SLRY_PCT','DSSALPCT','String'],['DISBLTY_MIN_NET_BEN_PCT','DSMINPCT','String'],['MIN_NET_BEN_AMT','DSMINNTBFT','String'],['DISBLTY_BEN_FREQ_CD','DSBFTFRQ','String'],
                ['DISBLTY_BEN_FREQ_TYP_CD','DSBFTFRQTP','String'],['INIT_EMP_WAIT_PER_TYP_CD','INITEWP','float'],['INIT_EMP_WAIT_PER_TYP_DESCR','INITEWPTYP','String'],
                ['INIT_EMP_COV_QLFCTN_TYP_CD','INITEWPEFF','String'],['NEW_EMP_WAIT_PER_TYP_CD','NEWEWP','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEWEWPTYP','String'],
                ['NEW_EMP_COV_QLFCTN_TYP_CD','NEWEWPEFF','String'],['EMPLR_CONTRIB_PCT','EMPRCTBPCT','String'],['COLA_OCCUR_TYP_DESCR','DSCOLADRTP','String']
                ]
        DFCompare.compare(DMsql1df,LZsql2df,cols)


def compare_partyplanfact_FTAPHAPL_backup():
        #-------------------------------------------------------------------------------------------------------------
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.PARTY_PLAN_FACT vs GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp")   
        
        DMsql = '''select B.ADMIN_SYS_PARTY_KEY_TXT,A.DISBLTY_BEN_FREQ_TYP_DESCR,A.INIT_EMP_COV_QLFCTN_TYP_DESCR,A.NEW_EMP_COV_QLFCTN_TYP_DESCR,
        A.FIX_COV_BEN_AMT,A.MAX_COV_BEN_AMT,A.MIN_COV_BEN_AMT,A.COLA_CD,A.COLA_MAX_OCCUR_NBR,A.COLA_OCCUR_TYP,A.INSUR_INCR_UNIT_AMT,A.PLCYHLDR_PLAN_CLS_CD,
        A.DISBLTY_BEN_SLRY_PCT,A.DISBLTY_MIN_NET_BEN_PCT,A.MIN_NET_BEN_AMT,A.DISBLTY_BEN_FREQ_CD,A.DISBLTY_BEN_FREQ_TYP_CD,A.INIT_EMP_WAIT_PER_TYP_CD,
        A.INIT_EMP_WAIT_PER_TYP_DESCR,A.INIT_EMP_COV_QLFCTN_TYP_CD,A.NEW_EMP_WAIT_PER_TYP_CD,A.NEW_EMP_WAIT_PER_TYP_DESCR,A.NEW_EMP_COV_QLFCTN_TYP_CD,A.EMPLR_CONTRIB_PCT,A.COLA_OCCUR_TYP_DESCR
        
        from dbo.PARTY_PLAN_FACT A inner join dbo.Party_Dim B on (B.PARTY_DIM_GEN_ID= A.PARTY_DIM_GEN_ID)'''
        
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
       
        DMsql1df['COLA_CD'] = DMsql1df['COLA_CD'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['COLA_MAX_OCCUR_NBR'] = DMsql1df['COLA_MAX_OCCUR_NBR'].replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['INIT_EMP_COV_QLFCTN_TYP_DESCR'] = DMsql1df['INIT_EMP_COV_QLFCTN_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['DISBLTY_BEN_FREQ_TYP_DESCR'] = DMsql1df['DISBLTY_BEN_FREQ_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['NEW_EMP_COV_QLFCTN_TYP_DESCR'] = DMsql1df['NEW_EMP_COV_QLFCTN_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['NEW_EMP_WAIT_PER_TYP_DESCR'] = DMsql1df['NEW_EMP_WAIT_PER_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['COLA_OCCUR_TYP_DESCR'] = DMsql1df['COLA_OCCUR_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['INIT_EMP_WAIT_PER_TYP_DESCR'] = DMsql1df['INIT_EMP_WAIT_PER_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        DMsql1df['INIT_EMP_COV_QLFCTN_TYP_CD'] = DMsql1df['INIT_EMP_COV_QLFCTN_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        DMsql1df['DISBLTY_BEN_FREQ_TYP_CD'] = DMsql1df['DISBLTY_BEN_FREQ_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        DMsql1df['NEW_EMP_COV_QLFCTN_TYP_CD'] = DMsql1df['NEW_EMP_COV_QLFCTN_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        DMsql1df['PLCYHLDR_PLAN_CLS_CD'] = DMsql1df['PLCYHLDR_PLAN_CLS_CD'].replace('NON',np.nan).replace('',np.nan).dropna()
        
        DMsql1df['DISBLTY_BEN_FREQ_CD'] = DMsql1df['DISBLTY_BEN_FREQ_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        
        
        '''
        DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'] = DMsql1df['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR'].replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['PREM_WVR_CD'] = DMsql1df['PREM_WVR_CD'].replace('NO',np.nan).replace('',np.nan).dropna()
        '''
        #html_logger.info("End of 1st query")   
       
        """ 
        polnumber1 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]]
        strpolnumber1 =  ','.join(map(str, polnumber1))
        strpolnumber1 = strpolnumber1[:-1]
        strpolnumber1 = strpolnumber1.replace(',NON', '')
        
        polnumber2 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]]
        strpolnumber2 =  ','.join(map(str, polnumber2))
        strpolnumber2 = strpolnumber2[:-1]
        
        polnumber3 = [','.join(ele.split()) for ele in DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]]
        strpolnumber3 =  ','.join(map(str, polnumber3))
        strpolnumber3 = strpolnumber3[:-1]
        """
        strpolnumber1 =  DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[0:8]
        strpolnumber2 =  DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[8:12]
        strpolnumber3 =  DMsql1df['ADMIN_SYS_PARTY_KEY_TXT'].str[12:15]
        
        LZsql = ('''select A.PHMAIN,A.VOLFLTAMT,A.VOLMXAMT,A.VOLMINAMT,A.DSCOLAPCT,A.DSCOLAMXDR,A.DSCOLADRTP,A.VOLINCAMT,
                
                case when len(A.CLSCD) = 1 then concat('00',A.CLSCD)
                when len(A.CLSCD) = 2 then concat('0',A.CLSCD)
                else A.CLSCD end as CLSCD,
                
                A.DSSALPCT,A.DSMINPCT,
                    A.DSMINNTBFT,A.INITEWP,A.INITEWPTYP,A.NEWEWP,A.NEWEWPTYP,A.EMPRCTBPCT,A.DSBFTFRQ ,                   
                    A.DSBFTFRQTP,A.INITEWPEFF,A.NEWEWPEFF
                                    
                    from GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL A
                     
                ''') 
                         
          #
          #DSBFTFRQTP,INITEWPEFF,NEWEWPEFF,B.GNPLANTYP,  
                    
          #TT_TO_FROM_VALUE,VOLRDCTBL,FTAUTIP_USER_TABLE_DESC,case when GNPLANTYP = 'WDS' or GNPLANTYP = 'WDL' then GNBEAVGT else Null End as GNBEAVGT
          # inner join (select distinct GNPOLNUM1, GNPOLNUM2, GNPOLNUM3,GNPLANTYP from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP) B on(A.PHMAIN=B.GNPOLNUM1 and A.PHBRN = B.GNPOLNUM2 and A.PHDEPT = B.GNPOLNUM3)
          # where GNPOLNUM1 in(''' + strpolnumber1 + ''') And GNPOLNUM2 in (''' +strpolnumber2 + ''') and GNPOLNUM3 in (''' + strpolnumber3 + ''')
          #case when GNPLANTYP <> 'OLS' then DSBFTFRQ else Null End as DSBFTFRQ,  
                    #case when GNPLANTYP <> 'OLS' then DSBFTFRQTP else Null End as DSBFTFRQTP,
                    #case when GNPLANTYP <> 'WDS' or GNPLANTYP <> 'WDL' then INITEWPEFF else Null End as INITEWPEFF,
                    #case when GNPLANTYP <> 'WDS' or GNPLANTYP <> 'WDL' then NEWEWPEFF else Null End as NEWEWPEFF  
                    
                    
        #print(LZsql)       
             
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df = LZsql2df[(LZsql2df['PHMAIN'].isin(strpolnumber1))]
        
 
        '''
        LZsql2df['GNCLSDEScription'] = LZsql2df.apply(lambda x:'%s %s %s' % (x['GNCLSDES1'],x['GNCLSDES2'],x['GNCLSDES3']),axis=1)
        
        LZsql2df['GNSPCCNDUR'] = LZsql2df['GNSPCCNDUR'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNCOSTPER'] = LZsql2df['GNCOSTPER'].replace('NON',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNCOSTYPE'] = LZsql2df['GNCOSTYPE'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNSELMTYP'] = LZsql2df['GNSELMTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['GNSPLIFTYP'] = LZsql2df['GNSPLIFTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        
        LZsql2df['GNSPCCNDTY'] = LZsql2df['GNSPCCNDTY'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE','':np.nan})
        
        LZsql2df['GNWAIVER'] = LZsql2df['GNWAIVER'].replace('NO',np.nan).replace('',np.nan).dropna()
        LZsql2df['GNSELMTYP'] = LZsql2df['GNSELMTYP'].replace('NO',np.nan).replace('',np.nan).dropna()
        
         '''
        LZsql2df['DSBFTFRQTP'] = LZsql2df['DSBFTFRQTP'].replace('NON',np.nan).replace('',np.nan).dropna()
        LZsql2df['INITEWPEFF'] = LZsql2df['INITEWPEFF'].replace('NON',np.nan).replace('',np.nan).dropna()
        LZsql2df['NEWEWPEFF'] = LZsql2df['NEWEWPEFF'].replace('NON',np.nan).replace('',np.nan).dropna()
        
        LZsql2df['DSBFTFRQTPCD'] = LZsql2df['DSBFTFRQTP']
        LZsql2df['DSBFTFRQTP'] = LZsql2df['DSBFTFRQTP'].map({'7':'7 DAY WORK WEEK','T':'THIRTY DAYS','5':'5 DAY WORK WEEK','':'NONE'})
        
        LZsql2df['INITEWPEFFCD'] = LZsql2df['INITEWPEFF']
        LZsql2df['INITEWPEFF'] = LZsql2df['INITEWPEFF'].map({'I':'IMMEDIATE','F':'FIRST OF THE MONTH','':'NONE'})
        
        LZsql2df['NEWEWPEFFCD'] = LZsql2df['NEWEWPEFF']
        LZsql2df['NEWEWPEFF'] = LZsql2df['NEWEWPEFF'].map({'I':'IMMEDIATE','F':'FIRST OF THE MONTH','':'NONE'})
        
        LZsql2df['INITEWPTYP'] = LZsql2df['INITEWPTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['NEWEWPTYP'] = LZsql2df['NEWEWPTYP'].map({'D':'DAYS','M':'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        #LZsql2df['DSCOLADRTP'] = LZsql2df['DSCOLADRTP'].map({'A':'ADJUSTMENTS','Y':'YEARS','':'NONE'})
        
        LZsql2df['DSCOLADRTP'] = LZsql2df['DSCOLADRTP'].replace('NON',np.nan).replace('',np.nan).dropna()
        
        LZsql2df['DSCOLADRTP1'] = LZsql2df['DSCOLADRTP'].map({'Y':'YEARS','A':'ADJUSTMENTS'})
        LZsql2df['DSCOLADRTP2'] = LZsql2df['DSCOLADRTP'].map({'Y':'Y','A':'A'})
        LZsql2df['DSCOLADRTP'] = LZsql2df['DSCOLAMXDR'].map({'':'N','5':'Y','10':'Y','65':'A','99':'A'})
        
        
        LZsql2df['CLSCD'] = LZsql2df['CLSCD'].map({'.':'000','':'000'})
        
        LZsql2df['DSCOLAPCT'] = LZsql2df['DSCOLAPCT'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        
        cols = [['DISBLTY_BEN_FREQ_TYP_DESCR','DSBFTFRQTP','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INITEWPEFF','String'],['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEWEWPEFF','String'],
                ['FIX_COV_BEN_AMT','VOLFLTAMT','String'],['MAX_COV_BEN_AMT','VOLMXAMT','String'],['MIN_COV_BEN_AMT','VOLMINAMT','String'],['COLA_CD','DSCOLAPCT','String'],
                ['COLA_MAX_OCCUR_NBR','DSCOLAMXDR','float'],['COLA_OCCUR_TYP','DSCOLADRTP','String'],['INSUR_INCR_UNIT_AMT','VOLINCAMT','String'],['PLCYHLDR_PLAN_CLS_CD','CLSCD','String'],
                ['DISBLTY_BEN_SLRY_PCT','DSSALPCT','String'],['DISBLTY_MIN_NET_BEN_PCT','DSMINPCT','String'],['MIN_NET_BEN_AMT','DSMINNTBFT','String'],['DISBLTY_BEN_FREQ_CD','DSBFTFRQ','String'],
                ['DISBLTY_BEN_FREQ_TYP_CD','DSBFTFRQTPCD','String'],['INIT_EMP_WAIT_PER_TYP_CD','INITEWP','float'],['INIT_EMP_WAIT_PER_TYP_DESCR','INITEWPTYP','String'],
                ['INIT_EMP_COV_QLFCTN_TYP_CD','INITEWPEFFCD','String'],['NEW_EMP_WAIT_PER_TYP_CD','NEWEWP','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEWEWPTYP','String'],
                ['NEW_EMP_COV_QLFCTN_TYP_CD','NEWEWPEFFCD','String'],['EMPLR_CONTRIB_PCT','EMPRCTBPCT','String'],['COLA_OCCUR_TYP_DESCR','DSCOLADRTP1','String']
                ]
        DFCompare.compare(DMsql1df,LZsql2df,cols)







def compare_BenPlanDIM_GNBANSPLN():
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.BEN_PLAN_DIM vs GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN")   
        
        DMsql = '''select ben_plan_nbr,ben_plan_typ_cd,OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR,SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD,OTH_INCM_INTGRTN_MTHD_CD,IMMED_HOSP_BEN_IND,
        DISBLTY_BEN_CONT_PER_CD,FULL_TM_STAT_QLFCTN_THRSHLD_NBR,FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD,CVRD_EARN_TYP_CD,CVRD_EARN_TYP_DESCR,
        INDV_COV_EFF_TYP_CD,INDV_COV_EFF_TYP_DESCR  from dbo.BEN_PLAN_DIM b'''
        
        #OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD,
        
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        #DMsql1df['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR'] = DMsql1df['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR'][DMsql1df.ben_plan_typ_cd.isin(['OLS','STD','WDL','WDS']) == False]  #[DMsql1df.Ben_Plan_typ_cd.isin(['OLS']) == False]
        DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD'] = DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD'].replace('NONE',np.nan).replace('',np.nan).dropna()
        #DMsql1df['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD'] = DMsql1df['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD'].replace('NO',np.nan).replace('',np.nan).dropna()
        DMsql1df['CVRD_EARN_TYP_CD'] = DMsql1df['CVRD_EARN_TYP_CD'].replace('NO',np.nan).replace('',np.nan).dropna()
        DMsql1df['INDV_COV_EFF_TYP_DESCR'] = DMsql1df['INDV_COV_EFF_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        #DMsql1df['INDV_COV_EFF_TYP_CD'] = DMsql1df['INDV_COV_EFF_TYP_CD'].map({'N':'NAN'})
        DMsql1df['INDV_COV_EFF_TYP_CD'] = DMsql1df['INDV_COV_EFF_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
    
        LZsql = ('''select GNINTEG,GNINTEGME,GNRETHOSP,GNRETNWORK,GNFULLTIME,GNFTTYPE,
                 case when GNPLANTYP not in ('OLS','WDL','WDS') then GNINDEFF else Null End as GNINDEFF,
                 case when GNPLANTYP in('LTD', 'CLC', 'CLP', 'LDA', 'STD', 'CSC', 'CSP', 'VSD', 'VMD', 'VLD') then GNCOVEARN else Null End as GNCOVEARN,
                 case when GNPLANTYP not in ('OLS','WDL','WDS','STD') then GNDEFINDIS else Null End as GNDEFINDIS
                 
                 from GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN''') 
                
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['GNDEFINDIS1'] = LZsql2df['GNDEFINDIS']
        LZsql2df['GNINDEFF1'] = LZsql2df['GNINDEFF']
        
        LZsql2df['GNINDEFF1'] = LZsql2df['GNINDEFF1'].map({'A':'Anniversary','F' :'FIRST OF THE MONTH','S':'SCHEDULED ENROLLMENT','I':'IMMEDIATE','':'NONE'})
         
        cols = [['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','GNINTEG','String'],['OTH_INCM_INTGRTN_MTHD_CD','GNINTEGME','String'],
                ['IMMED_HOSP_BEN_IND','GNRETHOSP','String'],['DISBLTY_BEN_CONT_PER_CD','GNRETNWORK','String'],
                ['FULL_TM_STAT_QLFCTN_THRSHLD_NBR','GNFULLTIME','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','GNFTTYPE','String'],['CVRD_EARN_TYP_CD','GNCOVEARN','String'],
                ['CVRD_EARN_TYP_DESCR','GNCOVEARN','String'],['INDV_COV_EFF_TYP_CD','GNINDEFF','String'],['INDV_COV_EFF_TYP_DESCR','GNINDEFF1','String']
                ]
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        
        DMsql = ''' select OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR,OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD from ben_Plan_Dim
                    where BEN_PLAN_TYP_CD not in ('OLS','WDL','WDS','STD') and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
                    and OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR <> ''
                '''       
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        #DMsql1df['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD'] = DMsql1df['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD'].map({'9':'09'})
        
        LZsql = ('''select GNDEFINDIS from GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN
                 where GNPLANTYP not in ('OLS','WDL','WDS','STD') and GNDEFINDIS <> ''  ''')   #case when len(ltrim(GNDEFINDIS)) = 1 then concat('0',ltrim(GNDEFINDIS)) else GNDEFINDIS end as 
                
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        LZsql2df['GNDEFINDIS1'] = LZsql2df['GNDEFINDIS']
        #LZsql2df['GNINDEFF1'] = LZsql2df['GNINDEFF']
         
        LZsql2df['GNDEFINDIS'] = LZsql2df['GNDEFINDIS'].map({'AD':'Duration Activities of Daily Living (ADL)','A1':'1 Year Activities of Daily Living (ADL)','A1':'1 Year Activities of Daily Living (ADL)',
                                                              'A2':'2 Year Activities of Daily Living (ADL)','A3':'3 Year Activities of Daily Living (ADL)','A5':'5 Year Activities of Daily Living (ADL)',
                                                              'DA':'Duration Attorneys','DP':'Duration Physicians','DU':'Duration Regular Occupation','5':'5 Weeks','9':'9 Weeks','1A':'1 Year Attorney',
                                                              '1P':'1 Year Physician','1Y':'1 Year Regular Occupation','11':'11 Weeks','12':'12 Weeks','13':'13 Weeks','15':'15 Weeks','18':'18 Weeks',
                                                              '2A':'2 Year Attorney','2P':'2 Year Physician','2Y':'2 Year Regular Occupation','20':'20 Weeks','22':'22 Weeks','23':'23 Weeks','24':'24 Weeks',
                                                              '25':'25 Weeks','26':'26 Weeks','3A':'3 Year Attorney','3P':'3 Year Physician','3Y':'3 Year Regular Occupation','4A':'4 Year Attorney',
                                                              '4P':'4 Year Physician','4Y':'4 Year Regular Occupation','5A':'5 Year Attorney','5P':'5 Year Physician','5Y':'5 Year Regular Occupation'})
        
        cols = [['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','GNDEFINDIS','String'],['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','GNDEFINDIS1','String']]
        DFCompare.compare(DMsql1df,LZsql2df,cols)
                
def compare_BenPlanDIM_FTYPLAP():
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.BEN_PLAN_DIM vs GAIN.LZ_GAIN_Core_Plan_Disability_Options_FTYPLAP")   
        
        DMsql = '''select BEN_MAX_PER_NBR,BEN_MAX_PER_TYP_CD,BEN_MAX_PER_TYP_DESCR,RTRN_TO_WRK_PER_NBR,POST_EFF_DT_TRTMT_FREE_PER_NBR,PRE_EXIT_COND_NOT_CVRD_PER_NBR,
                    PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR,SURV_BEN_PER_NBR,SURV_BEN_PER_TYP_CD,SURV_BEN_PER_TYP_DESCR,BEN_PLAN_TYP_CD 
                    from dbo.BEN_PLAN_DIM b where b.BEN_PLAN_TYP_CD not in ('OLS','WDL','WDS')'''
        

        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['BEN_MAX_PER_TYP_CD'] = DMsql1df['BEN_MAX_PER_TYP_CD'][DMsql1df.BEN_PLAN_TYP_CD.isin(['OLS','WDL','WDS']) == False]
        DMsql1df['BEN_MAX_PER_TYP_CD'] = DMsql1df['BEN_MAX_PER_TYP_CD'].replace('NAN',np.nan).replace('',np.nan).dropna()
        
        
        DMsql1df['BEN_MAX_PER_TYP_DESCR'] = DMsql1df['BEN_MAX_PER_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        DMsql1df['SURV_BEN_PER_TYP_CD'] = DMsql1df['SURV_BEN_PER_TYP_CD'][DMsql1df.BEN_PLAN_TYP_CD.isin(['OLS','WDL','WDS']) == False]
    
        LZsql = ('''select FTYPLAP_MAX_BENEFIT_PERIODS,FTYPLAP_BEN_PERIOD_TYPE,FTYPLAP_RETURN_WRK_NUMBER,FTYPLAP_PR_TIME_TRMT_FREE,FTYPLAP_PR_OF_TIME_COND_N_C,FTYPLAP_PR_TIME_PR_EFF_DTE,
                    FTYPLAP_SVR_BEN_POTIME_PYBLE,
                    
                    case when FTYPLAP_PLAN_TYPE not in ('WDS','WDL','OLS') then FTYPLAP_SVR_BEN_POTIME_UNITS else Null End as FTYPLAP_SVR_BEN_POTIME_UNITS
                    from GAIN.LZ_GAIN_Core_Plan_Disability_Options_FTYPLAP''') 
                
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        #LZsql2df.loc[(LZsql2df.FTYPLAP_SVR_BEN_POTIME_PYBLE == '0'),'FTYPLAP_SVR_BEN_POTIME_UNITS']='D'
        LZsql2df['FTYPLAP_SVR_BEN_POTIME_UNITS'] = pd.Series(np.where((LZsql2df.FTYPLAP_SVR_BEN_POTIME_PYBLE == 0),'D',LZsql2df.FTYPLAP_SVR_BEN_POTIME_UNITS))

        
        LZsql2df['FTYPLAP_BEN_PERIOD_TYPE_CD'] =LZsql2df['FTYPLAP_BEN_PERIOD_TYPE']
        LZsql2df['FTYPLAP_BEN_PERIOD_TYPE'] = LZsql2df['FTYPLAP_BEN_PERIOD_TYPE'].map({'M':'MONTHS','A' :'AGE','Y':'YEARS','W':'WEEKS','':'NONE'})
        LZsql2df['FTYPLAP_SVR_BEN_POTIME_UNITS_desc'] =  LZsql2df['FTYPLAP_SVR_BEN_POTIME_UNITS']
        LZsql2df['FTYPLAP_SVR_BEN_POTIME_UNITS_desc'] = LZsql2df['FTYPLAP_SVR_BEN_POTIME_UNITS_desc'].map({'D':'DAYS','M' :'MONTHS','':'NONE'})
         
                
        cols = [['BEN_MAX_PER_NBR','FTYPLAP_MAX_BENEFIT_PERIODS','String'],['BEN_MAX_PER_TYP_CD','FTYPLAP_BEN_PERIOD_TYPE_CD','String'],['BEN_MAX_PER_TYP_DESCR','FTYPLAP_BEN_PERIOD_TYPE','String'],
                ['RTRN_TO_WRK_PER_NBR','FTYPLAP_RETURN_WRK_NUMBER','String'],['POST_EFF_DT_TRTMT_FREE_PER_NBR','FTYPLAP_PR_TIME_TRMT_FREE','String'],
                ['PRE_EXIT_COND_NOT_CVRD_PER_NBR','FTYPLAP_PR_OF_TIME_COND_N_C','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','FTYPLAP_PR_TIME_PR_EFF_DTE','String'],
                ['SURV_BEN_PER_NBR','FTYPLAP_SVR_BEN_POTIME_PYBLE','String'],['SURV_BEN_PER_TYP_CD','FTYPLAP_SVR_BEN_POTIME_UNITS','String'],['SURV_BEN_PER_TYP_DESCR','FTYPLAP_SVR_BEN_POTIME_UNITS_desc','String']
                ]
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)




def compare_BenPlanDIM_FTAPLIMA():
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.BEN_PLAN_DIM vs GAIN.LZ_GAIN_CORE_PLAN_MASTER_FTAPLIMA")   
        
        DMsql = '''select BEN_PLAN_NBR,BEN_PLAN_TYP_CD,PLAN_EFF_DT from dbo.BEN_PLAN_DIM b where PM_EFF_END_TS = '9999-12-31' '''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['BEN_PLAN_TYP_CD'] = DMsql1df['BEN_PLAN_TYP_CD'].replace('NON',np.nan).replace('',np.nan).dropna()
        
        #DMsql1df['BEN_PLAN_NBR'] = DMsql1df['BEN_PLAN_NBR'].map({'0.0':'NAN'})
        DMsql1df['BEN_PLAN_NBR'] = DMsql1df['BEN_PLAN_NBR'].replace('NON',np.nan).replace('0',np.nan).dropna()
         
        LZsql = ('''SELECT FLD0001,FLD0002,
                    CASE WHEN LEN(FLD0005)<=5 THEN
                            CONCAT(FLD0005C,SUBSTRING(CAST(FLD0005 AS VARCHAR(10)),4,2),'-','0',SUBSTRING(CAST(FLD0005 AS VARCHAR(10)),1,1),'-',SUBSTRING(CAST(FLD0005 AS VARCHAR(10)),2,2))
                            ELSE CONCAT(FLD0005C,SUBSTRING(CAST(FLD0005 AS VARCHAR(10)),5,2),'-',SUBSTRING(CAST(FLD0005 AS VARCHAR(10)),1,2),'-',SUBSTRING(CAST(FLD0005 AS VARCHAR(10)),3,2))
                    END AS ConcatDate
                    FROM GAIN.LZ_GAIN_CORE_PLAN_MASTER_FTAPLIMA''') 
                
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
                
        cols = [['BEN_PLAN_NBR','FLD0001','float'],['BEN_PLAN_TYP_CD','FLD0002','String'],['PLAN_EFF_DT','ConcatDate','String']
                ]
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        html_logger.info("Start of Comparing dbo.BEN_PLAN_DIM vs LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP")   
        DMsql = '''select
                case when ben_plan_typ_cd not in('WDL','WDS','OLS') then INDV_COV_EFF_TYP_CD else ' ' end as INDV_COV_EFF_TYP_CD,
                case when ben_plan_typ_cd not in('WDL','WDS','OLS') then INDV_COV_EFF_TYP_DESCR else ' ' end as INDV_COV_EFF_TYP_DESCR
                from ben_plan_dim where INDV_COV_EFF_TYP_CD <> ' ' '''
        

        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['INDV_COV_EFF_TYP_CD'] = DMsql1df['INDV_COV_EFF_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        DMsql1df['INDV_COV_EFF_TYP_DESCR'] = DMsql1df['INDV_COV_EFF_TYP_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        
        LZsql = ('''Select INDEFF from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP where INDEFF <> ' ' ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        LZsql2df['INDEFF1'] = LZsql2df['INDEFF'].map({'A':'Anniversary','F':'First of the month','I':'Immediate','S':'Scheduled Enrollment'})
        
        cols = [['INDV_COV_EFF_TYP_CD','INDEFF','String'],['INDV_COV_EFF_TYP_DESCR','INDEFF1','String']]
        DFCompare.compare(DMsql1df,LZsql2df,cols)  
 
 
 
 
 
def compare_PartyDIM_FTAPHAMA():
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
                
        html_logger.info("Start of Comparing dbo.PARTY_DIM vs GAIN.LZ_GAIN_Policyholder_Master_FTAPHAMA")   
        
        DMsql = '''select PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,CMPNY_PREF_NAME,RGNL_GRP_OFF_NBR,PLCYHLDR_STAT_CD,PLCYHLDR_TERM_DT,BILL_CNTCT_NAME,
        PLCYHLDR_REINSTMT_DT,COV_TYP_CD,PLCYHLDR_EFF_DT,PLCYHLDR_STAT_DESCR,PD_THRU_DT,EOB_MAIL_RCPNT_TYP_DESCR,EOB_MAIL_RCPNT_TYP_CD,PLCYHLDR_STD_INDY_CLSFCTN_CD from party_dim'''
    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['PLCYHLDR_MSTR_NBR'] = DMsql1df['PLCYHLDR_MSTR_NBR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['PLCYHLDR_BR_NBR'] = DMsql1df['PLCYHLDR_BR_NBR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['PLCYHLDR_DEPT_NBR'] = DMsql1df['PLCYHLDR_DEPT_NBR'].replace('NON',np.nan).replace('',np.nan).dropna()
        DMsql1df['PLCYHLDR_STAT_CD'] = DMsql1df['PLCYHLDR_STAT_CD'].replace('NONE',np.nan).replace('',np.nan).dropna() 
        DMsql1df['PLCYHLDR_STAT_DESCR'] = DMsql1df['PLCYHLDR_STAT_DESCR'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['COV_TYP_CD'] = DMsql1df['COV_TYP_CD'].replace('N',np.nan).replace('',np.nan).dropna()
        DMsql1df['CMPNY_PREF_NAME'] = DMsql1df['CMPNY_PREF_NAME'].replace('NONE',np.nan).replace('',np.nan).dropna()
         
        DMsql1df['RGNL_GRP_OFF_NBR'] = DMsql1df['RGNL_GRP_OFF_NBR'].replace('NON','').dropna()
         
        LZsql = ('''
        SELECT PHMAIN,PHBRN,PHDEPT,NAME,POLSYM,STS,CNTCNAME,RENSTCC,RENSTMDY,BUSTYP,EFFMDY,
        
        CASE WHEN LEN(PDTHRUYMD) = 3 THEN CONCAT(CAST(PDTHRUCC AS VARCHAR(10)),'00','-','0',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),1,1),'-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),2,3))
         WHEN LEN(PDTHRUYMD) = 4 THEN CONCAT(CAST(PDTHRUCC AS VARCHAR(10)),'00','-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),1,2),'-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),3,4))
         WHEN LEN(PDTHRUYMD) = 5 THEN CONCAT(CAST(PDTHRUCC AS VARCHAR(10)),'0',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),1,1),'-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),2,2),'-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),4,5))
         WHEN LEN(PDTHRUYMD) = 6 THEN CONCAT(CAST(PDTHRUCC AS VARCHAR(10)),SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),1,2),'-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),3,2),'-',SUBSTRING(CAST(PDTHRUYMD AS VARCHAR(10)),5,6))
         END AS ModifiedDate
        ,EOBMAILTO,SIC,
                    CASE WHEN LEN(TRMMDY)<=5 THEN
                            CONCAT(TRMCC,SUBSTRING(CAST(TRMMDY AS VARCHAR(10)),4,2),'-','0',SUBSTRING(CAST(TRMMDY AS VARCHAR(10)),1,1),'-',SUBSTRING(CAST(TRMMDY AS VARCHAR(10)),2,2))
                    when LEN(TRMMDY)= 6 THEN CONCAT(TRMCC,SUBSTRING(CAST(TRMMDY AS VARCHAR(10)),5,2),'-',SUBSTRING(CAST(TRMMDY AS VARCHAR(10)),1,2),'-',SUBSTRING(CAST(TRMMDY AS VARCHAR(10)),3,2))
                    else ''
                    END AS ConcatDate,

                    CASE when RENSTMDY = 0 or RENSTCC = 0 then null
                    WHEN LEN(RENSTMDY)<=5 THEN
                            CONCAT(RENSTCC,SUBSTRING(CAST(RENSTMDY AS VARCHAR(10)),4,2),'-','0',SUBSTRING(CAST(RENSTMDY AS VARCHAR(10)),1,1),'-',SUBSTRING(CAST(RENSTMDY AS VARCHAR(10)),2,2))
                    when LEN(TRMMDY)= 6 then CONCAT(RENSTCC,SUBSTRING(CAST(RENSTMDY AS VARCHAR(10)),5,2),'-',SUBSTRING(CAST(RENSTMDY AS VARCHAR(10)),1,2),'-',SUBSTRING(CAST(RENSTMDY AS VARCHAR(10)),3,2))
                    else ''
                    END AS ReinstConcatDate,

                     CASE when EFFMDY = 0 or EFFCC = 0 then null
                     WHEN LEN(EFFMDY)<=5 THEN
                            CONCAT(EFFCC,SUBSTRING(CAST(EFFMDY AS VARCHAR(10)),4,2),'-','0',SUBSTRING(CAST(EFFMDY AS VARCHAR(10)),1,1),'-',SUBSTRING(CAST(EFFMDY AS VARCHAR(10)),2,2))
                             when LEN(EFFMDY)= 6 then CONCAT(EFFCC,SUBSTRING(CAST(EFFMDY AS VARCHAR(10)),5,2),'-',SUBSTRING(CAST(EFFMDY AS VARCHAR(10)),1,2),'-',SUBSTRING(CAST(EFFMDY AS VARCHAR(10)),3,2))
                        else ''
                    END AS EffConcatDate

                    from GAIN.LZ_GAIN_Policyholder_Master_FTAPHAMA
                    
                ''') 
                
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['EOBMAILTO_Desc'] = LZsql2df['EOBMAILTO']
        LZsql2df['EOBMAILTO_Desc'] = LZsql2df['EOBMAILTO_Desc'].map({'E':'EMPLOYEE','A' :'ADMINISTRATOR','P':'POLICYHOLDER','B':'BOTH (EMPLOYEE AND POLICYHOLDER)'})
        
        LZsql2df['PHBRN'] = LZsql2df['PHBRN'].replace('NONE',np.nan).replace('',np.nan).dropna()
        LZsql2df['PHDEPT'] = LZsql2df['PHDEPT'].replace('NON',np.nan).replace('',np.nan).dropna()
        LZsql2df['NAME'] = LZsql2df['NAME'].str.replace('"','').replace('NONE',np.nan).dropna()
        
        
        LZsql2df['CNTCNAME'] = LZsql2df['CNTCNAME'].str.replace('"','').replace('NONE',np.nan).dropna()
        
        
        LZsql2df['STS1'] = LZsql2df['STS'].map({'T':'TERMINATED','A' :'ACTIVE','P':'PENDED'})
                
        cols = [['PLCYHLDR_MSTR_NBR','PHMAIN','String'],['PLCYHLDR_BR_NBR','PHBRN','float'],['PLCYHLDR_DEPT_NBR','PHDEPT','float'],['CMPNY_PREF_NAME','NAME','String'],
                ['RGNL_GRP_OFF_NBR','POLSYM','String'],['PLCYHLDR_STAT_CD','STS','String'],['PLCYHLDR_TERM_DT','ConcatDate','String'],['BILL_CNTCT_NAME','CNTCNAME','String'],
                ['PLCYHLDR_REINSTMT_DT','ReinstConcatDate','String'],['COV_TYP_CD','BUSTYP','String'],['PLCYHLDR_EFF_DT','EffConcatDate','String'],['PLCYHLDR_STAT_DESCR','STS1','String'],
                ['PD_THRU_DT','ModifiedDate','String'],['EOB_MAIL_RCPNT_TYP_DESCR','EOBMAILTO_Desc','String'],['EOB_MAIL_RCPNT_TYP_CD','EOBMAILTO','String'],['PLCYHLDR_STD_INDY_CLSFCTN_CD','SIC','String']
                ]
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #----------------------------------------------------------------------------------------------------------------------------------
        html_logger.info("Start of Comparing dbo.Party_DIM vs GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA") 
         
        DMsql = '''select PREM_PAY_GRACE_PER_CD,PLCYHLDR_BILL_SUBSYS_CD,ALLNC_CD from party_dim'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['PLCYHLDR_BILL_SUBSYS_CD'] = DMsql1df['PLCYHLDR_BILL_SUBSYS_CD'].replace('NO',np.nan).replace('',np.nan).dropna()
        DMsql1df['PREM_PAY_GRACE_PER_CD'] = DMsql1df['PREM_PAY_GRACE_PER_CD'].replace('NON',np.nan).replace('',np.nan).dropna()
        
        
        LZsql = ('''select WF_PH_CONTRACT_GRACE_DAYS,WF_SUBSYSTEM_CODE,concat(WF_ALLIANCE_NUM,'-',WF_ALLIANCE_SUF) as WF_ALLIANCE from GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['PREM_PAY_GRACE_PER_CD','WF_PH_CONTRACT_GRACE_DAYS','float'],['PLCYHLDR_BILL_SUBSYS_CD','WF_SUBSYSTEM_CODE','String'],['ALLNC_CD','WF_ALLIANCE','String']]  
        DFCompare.compare(DMsql1df,LZsql2df,cols)   
        #----------------------------------------------------------------------------------------------------------------------------------
        html_logger.info("Start of Comparing dbo.Party_DIM vs GAIN.LZ_GAIN_Policyholder_Master_Annex_GNBANSPOL") 
         
        DMsql = '''select PLCYHLDR_FRST_SUBSD_PREF_NAME,PLCYHLDR_SEC_SUBSD_PREF_NAME,PLCYHLDR_THIRD_SUBSD_PREF_NAME,PLCYHLDR_FOURTH_SUBSD_PREF_NAME from party_dim'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['PLCYHLDR_SEC_SUBSD_PREF_NAME'] = DMsql1df['PLCYHLDR_SEC_SUBSD_PREF_NAME'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['PLCYHLDR_THIRD_SUBSD_PREF_NAME'] = DMsql1df['PLCYHLDR_THIRD_SUBSD_PREF_NAME'].replace('NONE',np.nan).replace('',np.nan).dropna()
        DMsql1df['PLCYHLDR_FOURTH_SUBSD_PREF_NAME'] = DMsql1df['PLCYHLDR_FOURTH_SUBSD_PREF_NAME'].replace('NONE',np.nan).replace('',np.nan).dropna()
              
        LZsql = ('''select GNSUBSIN1, GNSUBSIN2,GNSUBSIN3,GNSUBSIN4 from GAIN.LZ_GAIN_Policyholder_Master_Annex_GNBANSPOL''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        LZsql2df['GNSUBSIN1'] = LZsql2df['GNSUBSIN1'].str.replace('"','').replace('NONE',np.nan).dropna()
        
        cols = [['PLCYHLDR_FRST_SUBSD_PREF_NAME','GNSUBSIN1','String'],['PLCYHLDR_SEC_SUBSD_PREF_NAME','GNSUBSIN2','String'],['PLCYHLDR_THIRD_SUBSD_PREF_NAME','GNSUBSIN3','String'],
                ['PLCYHLDR_FOURTH_SUBSD_PREF_NAME','GNSUBSIN4','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)   
        

def compare_CAUSERELBENLMTDIM_FTYPLCP():
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
            
        html_logger.info("Start of Comparing CAUSE_REL_BEN_LMT_DIM vs GAIN.LZ_GAIN_Core_Plan_Cause_Code_File_FTYPLCP")   
        
        DMsql = '''select B.Ben_plan_typ_cd, A.DISBLTY_MAX_BEN_PER_NBR,A.DISBLTY_MAX_BEN_PER_CD,A.CAUSE_ELIM_PER_NBR,A.CAUSE_ELIM_PER_TYP_CD,CAUSE_ELIM_PER_TYP_DESCR,DISBLTY_MAX_BEN_PER_DESCR from CAUSE_REL_BEN_LMT_DIM A
                    inner join ben_plan_dim B on (B.Ben_plan_dim_gen_id = A.Ben_plan_dim_gen_id) where B.Ben_plan_typ_cd not in ('OLS','WDS','WDL')'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['DISBLTY_MAX_BEN_PER_NBR'] = DMsql1df['DISBLTY_MAX_BEN_PER_NBR'].replace(0.0,np.nan).dropna()
        DMsql1df['DISBLTY_MAX_BEN_PER_CD'] = DMsql1df['DISBLTY_MAX_BEN_PER_CD'].replace('N',np.nan).dropna()
        DMsql1df['CAUSE_ELIM_PER_TYP_CD'] = DMsql1df['CAUSE_ELIM_PER_TYP_CD'].replace('N',np.nan).dropna()
        DMsql1df['CAUSE_ELIM_PER_TYP_DESCR'] = DMsql1df['CAUSE_ELIM_PER_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        
        LZsql = ('''select FTYPLCP_CAUSE_MAXIMUM,FTYPLCP_CAUSE_MAX_TYPE,FTYPLCP_CAUSE_ELIM_PERIOD,FTYPLCP_CAUSE_PERIOD_TIME from GAIN.LZ_GAIN_Core_Plan_Cause_Code_File_FTYPLCP''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['FTYPLCP_CAUSE_PERIOD_DESC'] = LZsql2df['FTYPLCP_CAUSE_PERIOD_TIME']
        LZsql2df['FTYPLCP_CAUSE_PERIOD_DESC'] = LZsql2df['FTYPLCP_CAUSE_PERIOD_DESC'].map({'D':'DAYS','M' :'MONTHS','W':'WEEKS','Y':'YEARS','':'NONE'})
        LZsql2df['FTYPLCP_CAUSE_MAX_TYPE_DESC'] = LZsql2df['FTYPLCP_CAUSE_MAX_TYPE']
        LZsql2df['FTYPLCP_CAUSE_MAX_TYPE_DESC'] = LZsql2df['FTYPLCP_CAUSE_MAX_TYPE_DESC'].map({'A':'AGE','B':'Bi-Weeks','M':'MONTHS','N':'Not Covered','S':'Semi-months','W':'Weeks','Y':'Years'})
        
        cols = [['DISBLTY_MAX_BEN_PER_NBR','FTYPLCP_CAUSE_MAXIMUM','float'],['DISBLTY_MAX_BEN_PER_CD','FTYPLCP_CAUSE_MAX_TYPE','String'],['CAUSE_ELIM_PER_NBR','FTYPLCP_CAUSE_ELIM_PERIOD','float'],
                ['CAUSE_ELIM_PER_TYP_CD','FTYPLCP_CAUSE_PERIOD_TIME','String'],['CAUSE_ELIM_PER_TYP_DESCR','FTYPLCP_CAUSE_PERIOD_DESC','String'],['DISBLTY_MAX_BEN_PER_DESCR','FTYPLCP_CAUSE_MAX_TYPE_DESC','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)   
        
        
        html_logger.info("Start of Comparing PARTY_DTL_EXT_DIM vs GAIN.LZ_GAIN_Policyholder_Master_FTAPHAMA")   
        
        DMsql = '''select PHONE_NBR,ADDR_SEC_LINE_TXT,ADDR_CITY_NAME,ADDR_ST_CD,ADDR_ZIP_CD,ADDR_FRST_LINE_TXT from PARTY_DTL_EXT_DIM where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000' '''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['ADDR_FRST_LINE_TXT'] = DMsql1df['ADDR_FRST_LINE_TXT'].str.replace(' ','').str.replace('# ','#')
        DMsql1df['ADDR_SEC_LINE_TXT'] = DMsql1df['ADDR_SEC_LINE_TXT'].str.replace(' ','').str.replace(' # ','#')
        DMsql1df['ADDR_ZIP_CD'] = DMsql1df['ADDR_ZIP_CD'].str.replace('-','').str.replace(' ','')
        
        LZsql = ('''select case when len(AREACD) = 3 then concat(AREACD,PHNPRF, PHNNUM) else null end as phonenum,ADR2,CITY,ST,ZIP,ADR1 from GAIN.LZ_GAIN_Policyholder_Master_FTAPHAMA''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['ADR2'] = LZsql2df['ADR2'].str.replace('"','').str.replace(' ','').dropna()
        LZsql2df['ZIP'] = LZsql2df['ZIP'].str.replace('-','').str.replace(' ','').dropna()
        LZsql2df['ADR1'] = LZsql2df['ADR1'].str.replace('"','').str.replace(' ','').dropna()
        
        cols = [['PHONE_NBR','phonenum','String'],['ADDR_SEC_LINE_TXT','ADR2','String'],['ADDR_CITY_NAME','CITY','String'],
                ['ADDR_ST_CD','ST','String'],['ADDR_ZIP_CD','ZIP','String'],['ADDR_FRST_LINE_TXT','ADR1','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)            
        
       
        """ 
        html_logger.info("Start of Comparing PARTY_DTL_EXT_DIM vs GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA")   
        
        DMsql = '''select EMAIL_ADDR_TXT from PARTY_DTL_EXT_DIM  '''  #where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        LZsql = ('''select WF_EMAIL_ADDR from GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA ''')  #where WF_EMAIL_ADDR <> '' 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)

        
        cols = [['EMAIL_ADDR_TXT','WF_EMAIL_ADDR','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols) 
        """

def compare_TTTOFROMVALUES():
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
            
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.OLS_QLFCTN_DISBLTY_TYP_DESCR")   
        
        DMsql = '''select distinct OLS_QLFCTN_DISBLTY_TYP_DESCR from WRKSITE_OLS_COV_FACT'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['OLS_QLFCTN_DISBLTY_TYP_DESCR'] = DMsql1df['OLS_QLFCTN_DISBLTY_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select  distinct TT_TABLE_ID,DEFOFDISB,TT_MAX_VALUE,TT_TO_FROM_VALUE from gain.LZ_GAIN_Conversion_Table_FTATTI A inner join gain.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP b
                on A.TT_MAX_VALUE=cast(B.DEFOFDISB as varchar) where TT_TABLE_ID='LUMP SUM DEF OF DIS' ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['OLS_QLFCTN_DISBLTY_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)   
        
        #--------------------------------------------------------------
        html_logger.info("Start of Comparing TTTOFROMVALUE vs ben_plan_dim.DISBLTY_BEN_CONT_PER_DESCR")   
        
        DMsql = '''select distinct DISBLTY_BEN_CONT_PER_DESCR from ben_plan_dim'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['DISBLTY_BEN_CONT_PER_DESCR'] = DMsql1df['DISBLTY_BEN_CONT_PER_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select distinct GNRETNWORK,TT_MAX_VALUE,TT_TABLE_ID,TT_TO_FROM_VALUE,len(TT_TO_FROM_VALUE)
                 from GAIN.LZ_GAIN_Conversion_Table_FTATTI
                A inner join [GAIN].LZ_GAIN_Core_Plan_Annex_GNBANSPLN B on A.TT_MAX_VALUE=CAST(B.GNRETNWORK AS VARCHAR) where TT_TABLE_ID ='RETURN TO WORK ' ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['DISBLTY_BEN_CONT_PER_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)   
        
       #--------------------------------------------------------------  
        html_logger.info("Start of Comparing TTTOFROMVALUE vs Party_dim.PLCYHLDR_BILL_SUBSYS_DESCR")   
        
        DMsql = '''select distinct PLCYHLDR_BILL_SUBSYS_DESCR from party_dim'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['PLCYHLDR_BILL_SUBSYS_DESCR'] = DMsql1df['PLCYHLDR_BILL_SUBSYS_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select distinct RTrim(substring(T2.TT_TO_FROM_VALUE,4,100)) as TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA T1
                left outer join GAIN.LZ_GAIN_Conversion_Table_FTATTI T2 ON T2.TT_MAX_VALUE = T1. WF_SUBSYSTEM_CODE AND T2.TT_TABLE_ID = 'BILL - SUB SYSTEM'
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['PLCYHLDR_BILL_SUBSYS_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs party_plan_fact.ERISA_APPL_DESCR")   
        
        DMsql = '''select distinct ERISA_APPL_DESCR from PARTY_PLAN_FACT'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['ERISA_APPL_DESCR'] = DMsql1df['ERISA_APPL_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select distinct FTATTI.TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP BANSPHP
                    inner join GAIN.LZ_GAIN_Conversion_Table_FTATTI FTATTI on (BANSPHP.PHPERISA = FTATTI.TT_MAX_VALUE)
                    where BANSPHP.PHPERISA != '' and FTATTI.TT_TABLE_ID ='ERISA CODES'
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['ERISA_APPL_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------  
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs party_dim.PREM_PAY_GRACE_PER_DESCR")   
        
        DMsql = '''select distinct PREM_PAY_GRACE_PER_DESCR from party_dim'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['PREM_PAY_GRACE_PER_DESCR'] = DMsql1df['PREM_PAY_GRACE_PER_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''SELECT DISTINCT SUBSTRING(A.TT_TO_FROM_VALUE,2,100) as TT_TO_FROM_VALUE from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                INNER JOIN GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA B ON (A.TT_MAX_VALUE = B.WF_PH_CONTRACT_GRACE_DAYS)
                where A.TT_TABLE_ID = 'CONTRACT GRACE DAYS'
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['PREM_PAY_GRACE_PER_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------  
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs party_dim.RGNL_GRP_OFF_NAME")   
        
        DMsql = '''select distinct RGNL_GRP_OFF_NAME from PARTY_DIM'''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['RGNL_GRP_OFF_NAME'] = DMsql1df['RGNL_GRP_OFF_NAME'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select distinct A.TT_TO_FROM_VALUE from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                INNER JOIN  GAIN.LZ_GAIN_Policyholder_Master_FTAPHAMA B ON (A.TT_MAX_VALUE = B.POLSYM)
                where A.TT_TABLE_ID = 'REGIONAL GROUP OFFICES'
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['RGNL_GRP_OFF_NAME','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------  

        html_logger.info("Start of Comparing TTTOFROMVALUE vs BEN_PLAN_DIM.OTH_INCM_INTGRTN_MTHD_DESCR")   
        
        DMsql = '''select OTH_INCM_INTGRTN_MTHD_DESCR from dbo.BEN_PLAN_DIM b where
                    ben_plan_typ_cd in ('CLC', 'CLP', 'LTD', 'VLD', 'VMD') and OTH_INCM_INTGRTN_MTHD_DESCR <> '' '''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select A.TT_TO_FROM_VALUE from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                    INNER JOIN  GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN B ON (cast(A.TT_MAX_VALUE as varchar(50)) = cast(B.GNINTEGME as varchar(50)))
                    where A.TT_TABLE_ID = 'CPL INTEGRATION METHOD' and GNPLANTYP in ('CLC', 'CLP', 'LTD', 'VLD', 'VMD')            
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        
        cols = [['OTH_INCM_INTGRTN_MTHD_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------  

        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR")   
        
        DMsql = '''select FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR from WRKSITE_OLS_COV_FACT '''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR'] = DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''SELECT CASE WHEN FTHRS.FTHOURS <> 0 THEN FTHRS.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR
     WHEN FTREQ.FTREQUIRE<> 0 THEN FTREQ.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR
     ELSE FTHRS.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR
     END AS FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR
FROM
( SELECT GNPOLNUM1 MasterNumber,GNPOLNUM2 BranchNumber,GNPOLNUM3 DeptNumber,GNPLANNUM PlanNumber,GNPLANTYP PlanType
              ,concat(GNPHPEFMDY,GNPHPEFCC) PlanEffDate,FTHRTYPE,FTHOURS
              ,CASE WHEN COALESCE(LTRIM(RTRIM(a.TT_TO_FROM_VALUE)),'')=''OR LTRIM(RTRIM(a.TT_TO_FROM_VALUE))='Blank' THEN LTRIM(RTRIM(a.TT_TO_FROM_VALUE))
                      WHEN FTHRTYPE='N' THEN TT_TO_FROM_VALUE
                      ELSE CONCAT('Number of work hours in a ',
                                    CASE FTHRTYPE WHEN 'A' THEN 'Year'
                                                              WHEN 'B' THEN 'Bi-Week'
                                                              WHEN 'M' THEN 'Month'
                                                              WHEN 'Q' THEN 'Quarter'
                                                              WHEN 'S' THEN 'Half-Year'
                                                              WHEN 'W' THEN 'Week'
                                                              ELSE TT_TO_FROM_VALUE
                                         END
                                    )
              END AS FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR
              FROM
              GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B
              INNER JOIN
              GAIN.LZ_GAIN_Conversion_Table_FTATTI A
              ON TT_MAX_VALUE= FTHRTYPE AND
              ((GNPLANTYP='OLS' AND TT_TABLE_ID = 'LUMP SUM FT HOURS TYPE') OR (GNPLANTYP IN ('WDL','WDS') AND TT_TABLE_ID = 'CPL FT HOURS TYPE')
              ) AND
              (FTHOURS is not null or FTREQUIRE is not null)
) FTHRS
FULL OUTER JOIN
(select GNPOLNUM1 MasterNumber,GNPOLNUM2 BranchNumber,GNPOLNUM3 DeptNumber,GNPLANNUM PlanNumber,GNPLANTYP PlanType ,concat(GNPHPEFMDY,GNPHPEFCC) PlanEffDate
              ,FTREQUIRE,FTREQTYPE,CASE WHEN COALESCE(LTRIM(RTRIM(TT_TO_FROM_VALUE)),'') IN ('Blank','') THEN NULL
ELSE CONCAT ('Number of work ',LTRIM(RTRIM(lower(TT_TO_FROM_VALUE))),' in a year')END AS FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR
              from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B
              INNER JOIN GAIN.LZ_GAIN_Conversion_Table_FTATTI A ON TT_MAX_VALUE= FTREQTYPE AND GNPLANTYP in ('OLS','WDL','WDS') AND
                 TT_TABLE_ID = 'LUMP SUM FT REQUIRE TYPE' AND(FTHOURS is not null or FTREQUIRE is not null))
     FTREQ ON FTHRS.MasterNumber=FTREQ.MasterNumber AND FTHRS.BranchNumber=FTREQ.BranchNumber AND FTHRS.DeptNumber=FTREQ.DeptNumber AND
     FTHRS.PlanNumber=FTREQ.PlanNumber AND FTHRS.PlanType=FTREQ.PlanType AND FTHRS.PlanEffDate=FTREQ.PlanEffDate           
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        
        cols = [['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #--------------------------------------------------------------   
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.FULL_TM_STAT_AVG_PER_TYP_DESCR - OLS")   
        
        DMsql = '''SELECT FULL_TM_STAT_AVG_PER_TYP_DESCR FROM WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD = 'OLS' '''
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select CASE WHEN FTAVGTYPE = '' THEN 'DAYS' else TT_TO_FROM_VALUE end as TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Conversion_Table_FTATTI A inner join GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B on
                    A.TT_MAX_VALUE=B.FTAVGTYPE where TT_TABLE_ID='LUMP SUM FT REQUIRE TYPE' and GNPLANTYP='OLS'            
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        
        cols = [['FULL_TM_STAT_AVG_PER_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #----------------------------------------------------------------
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.FULL_TM_STAT_AVG_PER_TYP_DESCR - WDS/WDL")   
        
        DMsql = '''SELECT FULL_TM_STAT_AVG_PER_TYP_DESCR FROM WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''select CASE WHEN FTAVGTYPE = '' THEN 'DAYS' else TT_TO_FROM_VALUE end as TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Conversion_Table_FTATTI A inner join GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B on
                    A.TT_MAX_VALUE=B.FTAVGTYPE where TT_TABLE_ID='WDIS DURATION TYPE' and GNPLANTYP in ('WDS','WDL')            
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        
        cols = [['FULL_TM_STAT_AVG_PER_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs BenPlanDim.FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR ")   
        
        DMsql = '''select FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR from ben_plan_dim '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR'] = DMsql1df['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''SELECT CASE TT_MAX_VALUE
                                WHEN 'A' THEN 'Number of work hours in a Year'
                                WHEN 'B' THEN 'Number of work hours in a Bi-Week'
                                WHEN 'M' THEN 'Number of work hours in a Month'
                                WHEN 'Q' THEN 'Number of work hours in a Quarter'
                                WHEN 'N' THEN TT_TO_FROM_VALUE
                                WHEN 'S' THEN 'Number of work hours in a Half-Year'
                                WHEN 'W' THEN 'Number of work hours in a Week'
                                ELSE TT_TO_FROM_VALUE
                                END as TT_TO_FROM_VALUE
                     from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                    INNER JOIN GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN B ON (A.TT_MAX_VALUE = B.GNFTTYPE)
                    where A.TT_TABLE_ID = 'CPL FT HOURS TYPE' ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        
        cols = [['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.SLRY_CONT_TYP_DESCR ")   
        
        DMsql = '''select SLRY_CONT_TYP_DESCR from WRKSITE_OLS_COV_FACT'''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['SLRY_CONT_TYP_DESCR'] = DMsql1df['SLRY_CONT_TYP_DESCR'].replace('',np.nan).dropna()
        DMsql1df['SLRY_CONT_TYP_DESCR'] = DMsql1df['SLRY_CONT_TYP_DESCR'].str.strip()
        
        LZsql = ('''select TT_TO_FROM_VALUE from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                inner join GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B on (A.TT_MAX_VALUE = B.GNSALCONT)
                where tt_table_id = 'WDIS SALARY CONTINUANCE' and tt_max_value <> '' ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].str.strip()
        
        cols = [['SLRY_CONT_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.WRKSITE_DISBLTY_BSC_EARN_TYP_DESCR ")   
        
        DMsql = '''select a.WRKSITE_DISBLTY_BSC_EARN_TYP_DESCR from WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL')'''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                    inner join GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B on (A.TT_MAX_VALUE = B.GNBEERNTYP)
                    where A.TT_TABLE_ID = 'BASIC EARNINGS TYPE' and B.GNPLANTYP in ( 'WDS', 'WDL' ) '''
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['WRKSITE_DISBLTY_BSC_EARN_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.OCPTN_DISBLTY_DESCR ")   
        
        DMsql = '''select a.OCPTN_DISBLTY_DESCR from WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL')  '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from [GAIN].[LZ_GAIN_Conversion_Table_FTATTI] A
                   inner join GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP B on (A.TT_MAX_VALUE = B.GNOCCINJSC)
                   AND TT_TABLE_ID = 'WDIS OCC INJURY/SICKNESS'  and B.GNPLANTYP in ('WDS','WDL') '''
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['OCPTN_DISBLTY_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.S_CORP_INCM_TYP_DESCR ")   
        
        DMsql = '''select a.S_CORP_INCM_TYP_DESCR from WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') and a.S_CORP_INCM_TYP_DESCR <> ''  '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP A
                inner join GAIN.LZ_GAIN_Conversion_Table_FTATTI B ON (B.TT_MAX_VALUE=A.GNSINCTYP AND
                B.TT_TABLE_ID = 'WDIS BASE EARN INCOME TYP') WHERE A.GNPLANTYP in ('WDL', 'WDS') AND GNBESCORPI <>'N'  '''
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['S_CORP_INCM_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------         
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.SOLE_PROP_INCM_TYP_DESCR ")   
        
        DMsql = '''select a.SOLE_PROP_INCM_TYP_DESCR from WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') and a.SOLE_PROP_INCM_TYP_DESCR <> ''  '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP A
                    inner join GAIN.LZ_GAIN_Conversion_Table_FTATTI B ON B.TT_MAX_VALUE=A.GNSPRINCT AND B.TT_TABLE_ID = 'WDIS BASE EARN INCOME TYP'
                    WHERE A.GNPLANTYP in ('WDL', 'WDS') AND GNBESLEPRI <>'N'  '''
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['SOLE_PROP_INCM_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------    
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.PARTNERSHIP_INCM_TYP_DESCR ")   
        
        DMsql = '''select a.PARTNERSHIP_INCM_TYP_DESCR from WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL')   '''  #and a.PARTNERSHIP_INCM_TYP_DESCR <> ''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP A
                    inner join GAIN.LZ_GAIN_Conversion_Table_FTATTI B ON B.TT_MAX_VALUE=A.GNPARTINCT AND B.TT_TABLE_ID = 'WDIS BASE EARN INCOME TYP'
                    WHERE A.GNPLANTYP in ('WDL', 'WDS')   '''  #AND GNBEPARTNI<>'N'
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['PARTNERSHIP_INCM_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------------------   
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.BSC_EARN_INCM_TYP_DESCR ")   
        
        DMsql = '''select a.BSC_EARN_INCM_TYP_DESCR from WRKSITE_OLS_COV_FACT a
                INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') '''  #and a.PARTNERSHIP_INCM_TYP_DESCR <> ''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP A
                inner join GAIN.LZ_GAIN_Conversion_Table_FTATTI B ON B.TT_MAX_VALUE=A.GNBEINCTYP AND B.TT_TABLE_ID = 'WDIS BASE EARN INCOME TYP'
                WHERE A.GNPLANTYP in ('WDL', 'WDS')  '''  #AND GNBEPARTNI<>'N'
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['BSC_EARN_INCM_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        
        html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.TOTAL_DISBLTY_JOB_CLSFCTN_TYP_DESCR ")   
        
        DMsql = '''select TOTAL_DISBLTY_JOB_CLSFCTN_TYP_DESCR from dbo.WRKSITE_OLS_COV_FACT A
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') '''  #and a.PARTNERSHIP_INCM_TYP_DESCR <> ''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''SELECT B.TT_TO_FROM_VALUE FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP A INNER JOIN GAIN.LZ_GAIN_Conversion_Table_FTATTI B
                ON B.TT_TABLE_ID = 'WDIS TOTAL DISABILITY DEF' AND A.GNTODISDEF = B.TT_MAX_VALUE where A.GNPLANTYP IN ('WDS','WDL') '''  #AND GNBEPARTNI<>'N'
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['TOTAL_DISBLTY_JOB_CLSFCTN_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------- 
        
        DMsql = '''select COLA_DESCR  from Party_plan_fact A
                inner join dbo.ben_plan_Dim B on (B.BEN_PLAN_DIM_GEN_ID= A.BEN_PLAN_DIM_GEN_ID) where b.BEN_PLAN_TYP_CD not in ('OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['COLA_DESCR'] = DMsql1df['COLA_DESCR'].replace('NONE',np.nan).dropna() 
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' select A.DSCOLAPCT FROM GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL A LEFT JOIN GAIN.LZ_GAIN_FTYPTMP B ON A.DSCOLAPCT=B.FTYPTMP_ID where A.PLnTyp not in('OLS') and DSCOLAPCT <> ''      
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['ELIMTYPE'] = LZsql2df['ELIMTYPE'].map({'D':'DAYS','M':'MONTHS',' ':'NONE'})
        
        LZsql2df['DSCOLAPCT'] = LZsql2df['DSCOLAPCT'].str.replace('04','COLA Benefits 4%').str.replace('01','COLA Benefits 1%').str.replace('02','COLA Benefits 2%').str.replace('03','COLA Benefits 3%').str.replace('05','COLA Benefits 5%').str.replace('06','COLA Benefits 6%').str.replace('08','COLA Benefits 8%')
                                
        #.map({1:'COLA Benefits 1%',2:'COLA Benefits 2%',3:'COLA Benefits 3%',4:'COLA Benefits 4%',5:'COLA Benefits 5%',6:'COLA Benefits 6%',8:'COLA Benefits 8%'})

        cols = [['COLA_DESCR','DSCOLAPCT','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #-------------------------------------------------------------------               
        DMsql = '''select a.CVRD_EARN_TYP_CD from Party_plan_fact A  --count(*)
                inner join dbo.ben_plan_Dim B on (B.BEN_PLAN_DIM_GEN_ID= A.BEN_PLAN_DIM_GEN_ID)
                where a.PM_CUR_IND = 'Y' and a.CVRD_EARN_TYP_CD <> '' '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['CVRD_EARN_TYP_CD'] = DMsql1df['CVRD_EARN_TYP_CD'].replace('NO',np.nan).dropna()
        DMsql1df['CVRD_EARN_TYP_DESCR'] = DMsql1df['CVRD_EARN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT GNCOVEARN FROM (SELECT X.GNPLANNUM ,X.GNPLANTYP,X.GNCOVEARN ,CAST(CONCAT(X.PLAN_EFFDT_YEAR,'/',X.PLAN_EFFDT_MM,'/',X.PLAN_EFFDT_DD) AS DATE) AS PLAN_EFF_DT
                   FROM (SELECT GNPLANNUM ,GNPLANTYP ,GNCOVEARN ,CONCAT(LTRIM(RTRIM(GNPLEFCC)), RIGHT(LTRIM(RTRIM(GNPLEFMDY)),2)) AS PLAN_EFFDT_YEAR
                 ,SUBSTRING(RIGHT(replicate('0',6) + LTRIM(RTRIM(GNPLEFMDY)), 6),1,2) AS PLAN_EFFDT_MM
                 ,SUBSTRING(RIGHT(replicate('0',6) + LTRIM(RTRIM(GNPLEFMDY)), 6),3,2) AS PLAN_EFFDT_DD FROM EB_LandingZone.GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN ) X) A
                INNER JOIN GAIN.LZ_GAIN_Core_Plan_Master_FTAPLIMA_Unique_VW UNQ ON A.GNPLANNUM=UNQ.PLAN_NUM AND A.GNPLANTYP=UNQ.PLAN_TYP AND A.PLAN_EFF_DT=UNQ.PLAN_EFF_DT
                INNER JOIN EB_LandingZone.GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL B ON A.GNPLANNUM=B.PLNNUM AND A.GNPLANTYP=B.PLNTYP WHERE COALESCE(LTRIM(RTRIM(GNCOVEARN)),'') <>''
                group by GNCOVEARN ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        #LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({' ':'DAYS','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        cols = [['CVRD_EARN_TYP_CD','GNCOVEARN','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #-------------------------------------------------------------------   
        DMsql = '''select POST_EFF_DT_TRTMT_FREE_TYP_CD,POST_EFF_DT_TRTMT_FREE_DESCR from ben_plan_dim
                    where BEN_PLAN_TYP_CD not in ('WDS', 'WDL','OLS') and PM_CUR_IND = 'Y' '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['POST_EFF_DT_TRTMT_FREE_TYP_CD'] = DMsql1df['POST_EFF_DT_TRTMT_FREE_TYP_CD'].replace('N',np.nan).dropna()
        DMsql1df['POST_EFF_DT_TRTMT_FREE_DESCR'] = DMsql1df['POST_EFF_DT_TRTMT_FREE_DESCR'].replace('NONE',np.nan).dropna()
        #DMsql1df['CVRD_EARN_TYP_DESCR'] = DMsql1df['CVRD_EARN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT CASE WHEN PER_NBR=0 THEN 'D' ELSE FTYPLAP_TREATMENT_FREE_UNITS END AS FTYPLAP_TREATMENT_FREE_UNITS FROM(
                SELECT FTYPLAP_Plan_number,FTYPLAP_Plan_type,LTRIM(RTRIM(FTYPLAP_eff_date_century)) AS CENTURY_YY,LTRIM(RTRIM(FTYPLAP_EFF_DATE_YYMMDD)) AS EFF_DT_YYMMDD
                ,FTYPLAP_PR_TIME_TRMT_FREE AS PER_NBR,FTYPLAP_TREATMENT_FREE_UNITS from GAIN.LZ_GAIN_Core_Plan_Disability_Options_FTYPLAP
                WHERE [FTYPLAP_PLAN_TYPE] not in ('WDS', 'WDL','OLS') ) X ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['FTYPLAP_TREATMENT_FREE_UNITS_desc'] = LZsql2df['FTYPLAP_TREATMENT_FREE_UNITS'].map({'M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        
        
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        #LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({' ':'DAYS','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        cols = [['POST_EFF_DT_TRTMT_FREE_TYP_CD','FTYPLAP_TREATMENT_FREE_UNITS','String'],['POST_EFF_DT_TRTMT_FREE_DESCR','FTYPLAP_TREATMENT_FREE_UNITS_desc','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------- 
        
        DMsql = '''select PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD,PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR  from ben_plan_dim
                where BEN_PLAN_TYP_CD not in ('WDS', 'WDL','OLS') and PM_CUR_IND = 'Y' and PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD <> '' '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD'] = DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD'].replace('N',np.nan).dropna()
        DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR'] = DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR'].replace('NONE',np.nan).dropna()
        #DMsql1df['CVRD_EARN_TYP_DESCR'] = DMsql1df['CVRD_EARN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT case when FTYPLAP_PRE_X_NOT_COVRD_UNITS = '' then 'D' else FTYPLAP_PRE_X_NOT_COVRD_UNITS end as FTYPLAP_PRE_X_NOT_COVRD_UNITS
                from GAIN.LZ_GAIN_Core_Plan_Disability_Options_FTYPLAP WHERE [FTYPLAP_PLAN_TYPE] not in ('WDS', 'WDL','OLS') ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['FTYPLAP_PRE_X_NOT_COVRD_UNITS_desc'] = LZsql2df['FTYPLAP_PRE_X_NOT_COVRD_UNITS'].map({'M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        
        
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        #LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({' ':'DAYS','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        cols = [['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','FTYPLAP_PRE_X_NOT_COVRD_UNITS','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','FTYPLAP_PRE_X_NOT_COVRD_UNITS_desc','String']]  
        
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------- 
        
        DMsql = '''select PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD,PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR from ben_plan_dim
                    where BEN_PLAN_TYP_CD not in ('WDS', 'WDL','OLS') and PM_CUR_IND = 'Y' and PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD <> '' '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD'] = DMsql1df['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD'].replace('N',np.nan).dropna()
        DMsql1df['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR'] = DMsql1df['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR'].replace('NONE',np.nan).dropna()
                
        LZsql = (''' SELECT case when FTYPLAP_PRIOR_EFF_DATE_UNITS = '' then 'D' else FTYPLAP_PRIOR_EFF_DATE_UNITS end as  FTYPLAP_PRIOR_EFF_DATE_UNITS from GAIN.LZ_GAIN_Core_Plan_Disability_Options_FTYPLAP
                    WHERE [FTYPLAP_PLAN_TYPE] not in ('WDS', 'WDL','OLS') ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['FTYPLAP_PRIOR_EFF_DATE_UNITS_desc'] = LZsql2df['FTYPLAP_PRIOR_EFF_DATE_UNITS'].map({'M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
 
        cols = [['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','FTYPLAP_PRIOR_EFF_DATE_UNITS','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','FTYPLAP_PRIOR_EFF_DATE_UNITS_desc','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------- 
        
        DMsql = '''select RTRN_TO_WRK_PER_TYP_CD,RTRN_TO_WRK_PER_DESCR from ben_plan_dim
                    where BEN_PLAN_TYP_CD not in ('WDS', 'WDL','OLS') and PM_CUR_IND = 'Y' and RTRN_TO_WRK_PER_TYP_CD <> '' '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['RTRN_TO_WRK_PER_TYP_CD'] = DMsql1df['RTRN_TO_WRK_PER_TYP_CD'].replace('N',np.nan).dropna()
        DMsql1df['RTRN_TO_WRK_PER_DESCR'] = DMsql1df['RTRN_TO_WRK_PER_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT FTYPLAP_RETURN_WRK_FREQ from GAIN.LZ_GAIN_Core_Plan_Disability_Options_FTYPLAP
                WHERE [FTYPLAP_PLAN_TYPE] not in ('WDS', 'WDL','OLS') and FTYPLAP_RETURN_WRK_FREQ <> '' ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['FTYPLAP_RETURN_WRK_FREQ_Desc'] = LZsql2df['FTYPLAP_RETURN_WRK_FREQ'].map({'M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})

        cols = [['RTRN_TO_WRK_PER_TYP_CD','FTYPLAP_RETURN_WRK_FREQ','String'],['RTRN_TO_WRK_PER_DESCR','FTYPLAP_RETURN_WRK_FREQ_Desc','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------- 
        
        DMsql = '''select a.PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD,a.PRE_EXIT_COND_NOT_CVRD_PER_DESCR from dbo.WRKSITE_OLS_COV_FACT a
                INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL','OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD'] = DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD'].replace('N',np.nan).dropna()
        DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_DESCR'] = DMsql1df['PRE_EXIT_COND_NOT_CVRD_PER_DESCR'].replace('NONE',np.nan).dropna()
                
        LZsql = (''' select case when PXPDCNDNC=0 then 'D' ELSE PXPDCNDTY END AS PXPDCNDTY
                 FROM (SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],LTRIM(RTRIM([GNPHPEFCC])) AS [GNPHPEFCC]
                ,LTRIM(RTRIM([GNPHPEFMDY])) AS [GNPHPEFMDY],PXPDCNDNC,PXPDCNDTY from [GAIN].[LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP]
                WHERE GNPLANTYP in ('WDS', 'WDL','OLS')) X ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df['PXPDCNDTY_DESC'] = LZsql2df['PXPDCNDTY'].map({'M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})

        cols = [['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PXPDCNDTY','String'],['PRE_EXIT_COND_NOT_CVRD_PER_DESCR','PXPDCNDTY_DESC','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #------------------------------------------------------------------- 
        
        DMsql = '''select BEN_LMT_TYP_DESCR from dbo.TRAD_VOL_COV_FACT A inner join dbo.ben_plan_Dim B on (B.BEN_PLAN_DIM_GEN_ID= A.BEN_PLAN_DIM_GEN_ID)
                    where B.BEN_PLAN_TYP_CD in ('LTD', 'CLC', 'CLP', 'VMD', 'VLD','VSD') and B.PM_CUR_IND = 'Y' '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['BEN_LMT_TYP_DESCR'] = DMsql1df['BEN_LMT_TYP_DESCR'].replace('',np.nan).dropna()
         
        LZsql = (''' select FTAUTIP_USER_TABLE_DESC from Gain.LZ_GAIN_Benefit_Change_Description_Table_FTAUTIP FTAU
                inner join Gain.LZ_GAIN_Policyholder_Plan_FTAPHAPL FTAP ON FTAP.PLNTYP in ('LTD', 'CLC', 'CLP', 'VMD', 'VLD','VSD') AND FTAUTIP_TABLE_FILE_ID = 'FTYBRAP' 
                AND FTAU.FTAUTIP_USER_TABLE_ID = FTAP.DSAGERDCTB  ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
 
        cols = [['BEN_LMT_TYP_DESCR','FTAUTIP_USER_TABLE_DESC','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------------
        
        DMsql = '''select distinct case when admin_SYS_name ='GAIN' and len(admin_Sys_Party_key_Txt) = 15 and isNumeric(admin_Sys_Party_key_Txt) = 1 and party_Role_Typ_Txt = 'Policyholder' then 'Policyholder-NA'
                 when (admin_SYS_name  <> 'GAIN' or len(admin_Sys_Party_key_Txt) <> 15 or isNumeric(admin_Sys_Party_key_Txt) <> 1) and party_Role_Typ_Txt = 'NA' then 'Policyholder-NA'
                 else 'Failure' end as Result from party_Dim '''
            
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        LZsql2df = pd.DataFrame(['Policyholder-NA']) 
        LZsql2df.columns = ['Result']         

        cols = [['Result','Result','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------------
        
        DMsql = '''select distinct b.plcyhldr_MSTR_NBR from PARTY_PLAN_FACT a
                    inner join party_dim b on (b.party_dim_gen_id = a.party_dim_gen_id)'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['plcyhldr_MSTR_NBR'] = DMsql1df['plcyhldr_MSTR_NBR'].replace('NONE',np.nan).dropna()
         
        LZsql = (''' select distinct PHMAIN from gain.LZ_GAIN_Policyholder_Plan_FTAPHAPL ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
 
        cols = [['plcyhldr_MSTR_NBR','PHMAIN','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #----------------------------------
        DMsql = '''select distinct b.plcyhldr_BR_NBR from PARTY_PLAN_FACT a
                    inner join party_dim b on (b.party_dim_gen_id = a.party_dim_gen_id)'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['plcyhldr_BR_NBR'] = DMsql1df['plcyhldr_BR_NBR'].replace('NONE',np.nan).dropna()
         
        LZsql = (''' select distinct PHBRN from gain.LZ_GAIN_Policyholder_Plan_FTAPHAPL ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)

        cols = [['plcyhldr_BR_NBR','PHBRN','float']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #---------------------------------------------------
        DMsql = '''select distinct b.plcyhldr_DEPT_NBR from PARTY_PLAN_FACT a
                    inner join party_dim b on (b.party_dim_gen_id = a.party_dim_gen_id)'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['plcyhldr_DEPT_NBR'] = DMsql1df['plcyhldr_DEPT_NBR'].replace('NON',np.nan).dropna()
          
        LZsql = (''' select distinct PHDEPT from gain.LZ_GAIN_Policyholder_Plan_FTAPHAPL ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)

        cols = [['plcyhldr_DEPT_NBR','PHDEPT','float']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #--------------------------------------------------------------------------------
        DMsql = '''select distinct DISBLTY_CAUSE_TYP_CD from CAUSE_LMT_DIM'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['DISBLTY_CAUSE_TYP_CD'] = DMsql1df['DISBLTY_CAUSE_TYP_CD'].replace('NON',np.nan).dropna()
         
        LZsql = (''' select distinct FTYPLCP_CAUSE_ID from GAIN.LZ_GAIN_Core_Plan_Cause_Code_File_FTYPLCP ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)

        cols = [['DISBLTY_CAUSE_TYP_CD','FTYPLCP_CAUSE_ID','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #-----------------------------------------------------------------------------------
        DMsql = '''select distinct DISBLTY_CAUSE_TYP_DESCR from CAUSE_LMT_DIM'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['DISBLTY_CAUSE_TYP_DESCR'] = DMsql1df['DISBLTY_CAUSE_TYP_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT distinct  REPLACE(FLD0003,' ','') AS LZ_DISBLTY_CAUSE_TYP_DESCR  FROM GAIN.LZ_GAIN_Core_Plan_Cause_Code_File_FTYPLCP a
                   JOIN GAIN.LZ_GAIN_Reference_Table_FTATBI b  ON a.fTYPLCP_CAUSE_ID=b.FLD0002 AND FLD0001 = 'DS' AND  FTYPLCP_CAUSE_ID IS NOT NULL ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)

        cols = [['DISBLTY_CAUSE_TYP_DESCR','LZ_DISBLTY_CAUSE_TYP_DESCR','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #------------------------------------------------------------------------------------
        DMsql = '''select BEN_RDCTN_CD from dbo.WRKSITE_OLS_COV_FACT A inner join dbo.ben_plan_Dim B on (B.BEN_PLAN_DIM_GEN_ID= A.BEN_PLAN_DIM_GEN_ID)
                    where B.BEN_PLAN_TYP_CD in ('OLS')'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
         
        LZsql = (''' select VOLRDCTBL from gain.LZ_GAIN_Policyholder_Plan_FTAPHAPL where plntyp in ('OLS') ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
       
        cols = [['BEN_RDCTN_CD','VOLRDCTBL','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #--------------------------------------------------
        DMsql = '''select BEN_RDCTN_DESCR from dbo.WRKSITE_OLS_COV_FACT A inner join dbo.ben_plan_Dim B on (B.BEN_PLAN_DIM_GEN_ID= A.BEN_PLAN_DIM_GEN_ID)
                    where B.BEN_PLAN_TYP_CD in ('OLS')'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        LZsql = (''' select FTAUTIP_USER_TABLE_DESC from gain.LZ_GAIN_Policyholder_Plan_FTAPHAPL FL inner join GAIN.LZ_GAIN_Benefit_Change_Description_Table_FTAUTIP FP ON
                    FP.FTAUTIP_TABLE_FILE_ID = 'FTAVARP' AND FP.FTAUTIP_USER_TABLE_ID = FL.VOLRDCTBL AND FL.PLNTYP = 'OLS' ''')
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['BEN_RDCTN_DESCR','FTAUTIP_USER_TABLE_DESC','String']]          
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #--------------------------------------------------------  
        DMsql = '''select distinct BEN_PLAN_TYP_CD,BEN_PLAN_TYP_DESCR from ben_Plan_dim'''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['BEN_PLAN_TYP_CD'] = DMsql1df['BEN_PLAN_TYP_CD'].replace('NON',np.nan).dropna()
        DMsql1df['BEN_PLAN_TYP_DESCR'] = DMsql1df['BEN_PLAN_TYP_DESCR'].replace('NONE',np.nan).dropna()

        
        LZsql = (''' select FLD0324,FLD0326 from GAIN.LZ_GAIN_Plan_Type_FTASCIPT''' )
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        LZsql2df = LZsql2df[LZsql2df['FLD0324'].str.strip().isin(DMsql1df['BEN_PLAN_TYP_CD'].str.strip())]

        cols = [['BEN_PLAN_TYP_DESCR','FLD0326','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #--------------------------------------------------------------------- 
        DMsql = '''select DISBLTY_MAX_BEN_AMT from party_plan_fact where PM_CUR_IND = 'Y' '''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        LZsql = (''' select DSMXAMT from GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL''' )
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)

        cols = [['DISBLTY_MAX_BEN_AMT','DSMXAMT','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        #------------------------------------------------------------------------------   
        DMsql = '''select b.BEN_PLAN_TYP_CD, b.Ben_plan_nbr from PARTY_PLAN_FACT a inner join ben_plan_dim b on (b.ben_plan_dim_gen_id = a.ben_plan_dim_gen_id) '''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['BEN_PLAN_TYP_CD'] = DMsql1df['BEN_PLAN_TYP_CD'].replace('NON',np.nan).dropna()
        DMsql1df['Ben_plan_nbr'] = DMsql1df['Ben_plan_nbr'].replace(0.0,np.nan).dropna()
        
        LZsql = (''' select PLNTYP,PLNNUM from GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL''' )
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['BEN_PLAN_TYP_CD','PLNTYP','String'],['Ben_plan_nbr','PLNNUM','float']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)

        #-------------------------------------------------------------------------------
        DMsql = '''select BEN_LMT_TYP_CD from TRAD_VOL_COV_FACT where BEN_LMT_TYP_CD <> '' '''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        DMsql1df['BEN_LMT_TYP_CD'] = DMsql1df['BEN_LMT_TYP_CD'].replace('NONE',np.nan).dropna()
        #DMsql1df['Ben_plan_nbr'] = DMsql1df['Ben_plan_nbr'].replace(0.0,np.nan).dropna()
        
        LZsql = (''' select DSAGERDCTB from GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL where DSAGERDCTB <> ''  and PLNTYP not in ('OLS','WDS' , 'WDL', 'STD', 'CSC', 'CSP', 'SDA') ''' )
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['BEN_LMT_TYP_CD','DSAGERDCTB','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------------------------------
        DMsql = '''select SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR from dbo.BEN_PLAN_DIM b where BEN_PLan_TYp_cd in ('STD', 'CSC', 'CSP' , 'SDA') and PM_CUR_IND = 'Y'  '''              
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'] = DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'].replace('NONE',np.nan).dropna()

        
        LZsql = (''' select substring(TT_TO_FROM_VALUE,6,100) as  TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Conversion_Table_FTATTI A inner join GAIN.LZ_GAIN_Core_Plan_Annex_GNBANSPLN B 
                ON cast(GNINTEG as varchar(100)) = TT_MAX_VALUE and TT_TABLE_ID = 'CPL INTEGRATION IND' where B.GNPLANTYP in ('STD', 'CSC', 'CSP' , 'SDA') ''' )
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)

        #-----------------------------------------------
        DMsql = '''select RTRN_TO_WRK_PER_NBR from WRKSITE_OLS_COV_FACT '''  #where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        LZsql = ('''select PER_NBR from ( SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],
                concat([GNPHPEFCC],right([GNPHPEFMDY],2) ) as EFFYears ,CASE len([GNPHPEFMDY]) When 5 then concat('0',left([GNPHPEFMDY],3)) ELSE left([GNPHPEFMDY],4) END as Eff_MMDD
                ,PER_NBR ,case when PER_NBR=0 then 'D' ELSE PER_TYP_CD END AS PER_TYP_CD  FROM (SELECT [GNPOLNUM1],[GNPOLNUM2],[GNPOLNUM3],[GNPLANNUM],[GNPLANTYP],LTRIM(RTRIM([GNPHPEFCC])) AS [GNPHPEFCC]
                ,LTRIM(RTRIM([GNPHPEFMDY])) AS [GNPHPEFMDY],CASE WHEN GNPLANTYP='OLS' THEN RTWDUR WHEN GNPLANTYP IN ('WDL','WDS') THEN GNRCDISDUR
                END AS PER_NBR,CASE WHEN GNPLANTYP='OLS' THEN RTWTYP WHEN GNPLANTYP IN ('WDL','WDS') THEN GNRCDISDUT END AS PER_TYP_CD from [GAIN].[LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP]
                WHERE GNPLANTYP in ('WDS', 'WDL','OLS')) X) Q1 ''')  #where WF_EMAIL_ADDR <> '' 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['RTRN_TO_WRK_PER_NBR','PER_NBR','float']]   
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #--------------------------------------------------
        DMsql = '''select ALLNC_CD, ALLNC_DESCR from party_dim '''  #where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        #DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'] = DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        #DMsql1df['Ben_plan_nbr'] = DMsql1df['Ben_plan_nbr'].replace(0.0,np.nan).dropna()
        
        LZsql = ('''select concat(WF_ALLIANCE_NUM,'-',WF_ALLIANCE_SUF) as LZ_ALLNC_CD,FLD0005 as LZ_ALLNC_DESCR from [GAIN].[LZ_GAIN_Provider_Table_FTCPVA] FTCPVA
                join GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA GNPHBIL on FTCPVA.FLD0001 = GNPHBIL.WF_ALLIANCE_NUM and FTCPVA. FLD0001A = GNPHBIL.WF_ALLIANCE_SUF ''')  #where WF_EMAIL_ADDR <> '' 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['ALLNC_DESCR','LZ_ALLNC_DESCR','String'],['ALLNC_CD','LZ_ALLNC_CD','String']]   
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #-------------------------------------------------------
        
        
def trial(): 
        
        DMserver = 'SQLD10785,4242'
        DMdbname = 'ENTPRS_CLAIMS_DM_UAT'
        
        LZserver = 'DEV04SQL2012,4242'
        LZdbname = 'EB_LandingZone'
               
        """html_logger.info("Start of Comparing TTTOFROMVALUE vs WRKSITE_OLS_COV_FACT.OTH_INCM_INTGRTN_DESCR ")   
        
        DMsql = '''select OTH_INCM_INTGRTN_DESCR from dbo.WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on (b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID) WHERE b.BEN_PLAN_TYP_CD in ('WDS','WDL') '''  #and a.PARTNERSHIP_INCM_TYP_DESCR <> ''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)

        
        LZsql = '''select TT_TO_FROM_VALUE from GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP A inner join Gain.LZ_GAIN_Conversion_Table_FTATTI B
                on cast(B.TT_MAX_VALUE as int)=A.GNSSI where TT_TABLE_ID = 'WDIS SOC SEC INTEGRATION'
                and GNPLANTYP in ('WDS','WDL') '''  #AND GNBEPARTNI<>'N'
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        cols = [['OTH_INCM_INTGRTN_DESCR','TT_TO_FROM_VALUE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
          #----------------------------------------------------------------
        html_logger.info("Start of Comparing GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSphp vs dbo.WRKSITE_OLS_COV_FACT ")   
        
        DMsql = '''select a.SURV_BEN_PER_TYP_DESCR,WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR,WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR from dbo.WRKSITE_OLS_COV_FACT a
                    INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = ('''SELECT CASE WHEN GNSRVBEND=0 THEN 'Days' WHEN GNSRVBENDT='D' THEN 'Days' WHEN GNSRVBENDT='M' THEN 'Months'
                    WHEN GNSRVBENDT='Q' THEN 'Quarters' WHEN GNSRVBENDT='W' THEN 'Weeks' WHEN GNSRVBENDT='Y' THEN 'Years'
                     END AS SURV_BEN_PER_TYP_DESCR,GNDRALDURT,RTNTOWRKT FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP
                    WHERE GNSRVBEND IS NOT NULL and GNPLANTYP in ('WDL','WDS')          
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['GNDRALDURT'] = LZsql2df['GNDRALDURT'].map({'':'DAYS','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        LZsql2df['RTNTOWRKT'] = LZsql2df['RTNTOWRKT'].map({'':'','M':'MONTHS','Y':'YEARS','W':'WEEKS','D':'DAYS'})
        cols = [['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR','GNDRALDURT','String'],['WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR','RTNTOWRKT','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        #***********************************************************************************************************************
       """     
        DMsql = '''select ALLNC_CD, ALLNC_DESCR from party_dim '''  #where Party_dtl_typ_cd = 'PRIMARY' and PM_EFF_END_TS = '9999-12-31 00:00:00.000'
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        
        #DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'] = DMsql1df['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR'].replace('NONE',np.nan).dropna()
        #DMsql1df['Ben_plan_nbr'] = DMsql1df['Ben_plan_nbr'].replace(0.0,np.nan).dropna()
        
        LZsql = ('''select concat(WF_ALLIANCE_NUM,'-',WF_ALLIANCE_SUF) as LZ_ALLNC_CD,FLD0005 as LZ_ALLNC_DESCR from [GAIN].[LZ_GAIN_Provider_Table_FTCPVA] FTCPVA
                join GAIN.LZ_GAIN_Policyholder_Billing_Admin_Code_GNPHBILDTA GNPHBIL on FTCPVA.FLD0001 = GNPHBIL.WF_ALLIANCE_NUM and FTCPVA. FLD0001A = GNPHBIL.WF_ALLIANCE_SUF ''')  #where WF_EMAIL_ADDR <> '' 
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        
        cols = [['ALLNC_DESCR','LZ_ALLNC_DESCR','String'],['ALLNC_CD','LZ_ALLNC_CD','String']]   
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)

         #***********************************************************************************************************************
        """   
        DMsql = '''select FULL_TM_STAT_AVG_PER_TYP_CD from dbo.WRKSITE_OLS_COV_FACT a INNER JOIN BEN_PLAN_DIM b on(b.BEN_PLAN_DIM_GEN_ID = a.BEN_PLAN_DIM_GEN_ID)
                    WHERE b.BEN_PLAN_TYP_CD in('WDS','WDL','OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' SELECT FTAVGTYPE FROM GAIN.LZ_GAIN_Policyholder_Plan_Annex_GNBANSPHP
                WHERE GNPLANTYP in ('WDL','WDS','OLS')        
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['TT_TO_FROM_VALUE'] = LZsql2df['TT_TO_FROM_VALUE'].replace('NONE',np.nan).dropna()
        LZsql2df['FTAVGTYPE'] = LZsql2df['FTAVGTYPE'].map({' ':'D'})
        cols = [['FULL_TM_STAT_AVG_PER_TYP_CD','FTAVGTYPE','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        DMsql = '''select COLA_DESCR  from Party_plan_fact A
                inner join dbo.ben_plan_Dim B on (B.BEN_PLAN_DIM_GEN_ID= A.BEN_PLAN_DIM_GEN_ID) where b.BEN_PLAN_TYP_CD not in ('OLS') '''
                    
        DMsql1df = Parser.parseSQL(DMsql,DMserver,DMdbname)
        DMsql1df['COLA_DESCR'] = DMsql1df['COLA_DESCR'].replace('NONE',np.nan).dropna() 
        #DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'] = DMsql1df['OTH_INCM_INTGRTN_MTHD_DESCR'].replace('NONE',np.nan).dropna()
        
        LZsql = (''' select A.DSCOLAPCT FROM GAIN.LZ_GAIN_Policyholder_Plan_FTAPHAPL A LEFT JOIN GAIN.LZ_GAIN_FTYPTMP B ON A.DSCOLAPCT=B.FTYPTMP_ID where A.PLnTyp not in('OLS') and DSCOLAPCT <> ''      
                 ''')
        
        LZsql2df = Parser.parseSQL(LZsql,LZserver,LZdbname)
        #LZsql2df['ELIMTYPE'] = LZsql2df['ELIMTYPE'].map({'D':'DAYS','M':'MONTHS',' ':'NONE'})
        
        LZsql2df['DSCOLAPCT'] = LZsql2df['DSCOLAPCT'].str.replace('04','COLA Benefits 4%').str.replace('01','COLA Benefits 1%').str.replace('02','COLA Benefits 2%').str.replace('03','COLA Benefits 3%').str.replace('05','COLA Benefits 5%').str.replace('06','COLA Benefits 6%').str.replace('08','COLA Benefits 8%')
                                
        #.map({1:'COLA Benefits 1%',2:'COLA Benefits 2%',3:'COLA Benefits 3%',4:'COLA Benefits 4%',5:'COLA Benefits 5%',6:'COLA Benefits 6%',8:'COLA Benefits 8%'})

        cols = [['COLA_DESCR','DSCOLAPCT','String']]  
        
        DFCompare.compare(DMsql1df,LZsql2df,cols)
        
        """
         
#------------Call  the methods created here---------------------------------------         
"""compare_Worksite()
compare_partyplanfact_GNBANSphp()
compare_partyplanfact_FTAPHAPL_backup()
compare_BenPlanDIM_GNBANSPLN()
compare_BenPlanDIM_FTYPLAP()
compare_BenPlanDIM_FTAPLIMA()
compare_PartyDIM_FTAPHAMA()
compare_CAUSERELBENLMTDIM_FTYPLCP()
compare_TTTOFROMVALUES()"""
#trial()
compare_partyplanfact_GNBANSphp()
#------------Call  the methods created here---------------------------------------   
try:
    driver.close()
except:
    pass  

print("Done")



