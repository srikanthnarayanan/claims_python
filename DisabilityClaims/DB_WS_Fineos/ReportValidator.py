import csv
import datetime
import logging
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
import sys
import time
import unittest
import re

from Payment_ReusableArtifacts import Comparator, Parser, DFCompare
import Payment_ReusableArtifacts
from html_logger import setup, info
import html_logger
import numpy as np 
import pandas as pd
import xml.etree.ElementTree as ET
from jinja2.ext import do
from datetime import datetime
from datetime import timedelta as td
import _struct
from collections import Counter
from bleach._vendor.html5lib._ihatexml import name
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#from datetime import date


#from test import support
now = datetime.now()
parentFolder = "G:\\Eclipise_workspace_10142020\\Python_Projects\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
#sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

#driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  

LOG_FILENAME = parentFolder + "DisabilityClaims\Claims_ReportValidation\Results\\ReportValidation_"+ str(now.isoformat().replace(":","_")) + ".html"
setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)

def report1():
    dbdata = pd.read_excel('C:\\Srikanth\\GL_ACCounting_detail_database_report.xls', sheet_name = 0)  #,encoding='cp437'  ,errors = 'ignore'
    print(dbdata.head(5))
    
    rptdata = pd.read_excel('C:\\Srikanth\\GL_ACCounting_detail_database_report.xls', sheet_name = 1)  #,encoding='cp437'  ,errors = 'ignore'
    print(rptdata.head(5))
    
    if(dbdata.equals(rptdata)):
        html_logger.dbg("ROw and column count matches")
    else:
        df_1notin2 = dbdata[~(dbdata['Group Main#'].isin(rptdata['Group Main#']) & dbdata['Account Number'].isin(rptdata['Account Number']))].reset_index(drop=True)
        html_logger.err("ROw and column count doesn't match. row count of dbdata is " + str(dbdata.shape[0]) + " but the row count of Report is " + str(rptdata.shape[0]))
        html_logger.err(df_1notin2.to_string())
        
    cols = [['Group Main#','Group Main#','String'],['Account Number','Account Number','String'],['JR#','JR#','String'],['GL Amonut','GL Amonut','float'],
            ['D/C','D/C','String'],['Cycle Date','Cycle Date','String'],['Member Name','Member Name','String'],['Branch','Branch','String'],['Dept','Dept','String'],
            ['Cert Number','Cert Number','String'],['State Code','State Code','String'],['Claim Office','Claim Office','String']]
    
    DFCompare.compare(dbdata,rptdata,cols)


def report2():
    sql1 = """SELECT distinct GRP_NBR AS 'Group Main',ACCT_NBR AS 'Account Number',JRNL_NBR AS 'JR_Num',PAY_AMT AS 'GL Amonut',DRCR_IND AS 'D_C',LAST_UPDT_TS AS 'Cycle Date'
        ,MBR_NAME AS 'Member Name',SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch',RIGHT([POLICY_NUMBER], 3) AS 'Dept',CERT_NBR AS 'Cert Number'
        ,ST_CD AS 'State Code',CLAIM_OFF_CD AS 'Claim Office' FROM [ENTPRS_CLAIMS_ODS].[FAI].[GL_ELMNT_DTL]"""
        
    sql11 = """SELECT GRP_NBR AS 'Group Main',ACCT_NBR AS 'Account Number',JRNL_NBR AS 'JR_Num',PAY_AMT AS 'GL Amount',DRCR_IND AS 'D_C'
        ,LAST_UPDT_TS AS 'Cycle Date',MBR_NAME AS 'Member Name',SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch',RIGHT([POLICY_NUMBER], 3) AS 'Dept'
        ,CERT_NBR AS 'Cert Number' ,ST_CD AS 'State Code',CLAIM_OFF_CD AS 'Claim Office' FROM [ENTPRS_CLAIMS_ODS].[FAI].[GL_ELMNT_DTL] order by GRP_NBR"""
        
    dbdata = Parser.parseSQL(sql1,"SQLD10746,4242","ENTPRS_CLAIMS_ODS")

    #dbdata = dbdata.sort_values(by=['Group Main', 'Account Number', 'State Code'])
    dbdata['combined'] = dbdata['Group Main'].astype(str) + ";" + dbdata['Account Number'].astype(str) + ";" + dbdata['JR_Num'].astype(str) + ";" +  dbdata['GL Amount'].astype(str) + ";" + dbdata['Member Name'].astype(str) + ";" + dbdata['State Code'].astype(str)
    dbdata = dbdata.reset_index(drop=True) 
    #print(dbdata.head(25))
    
    
    rptdata = pd.read_excel('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\DisabilityClaims\\Claims_ReportValidation\\Fineos Daily Accounting Details Report_0902.xls', sheet_name = 0, dtype=str)  #,encoding='cp437'  ,errors = 'ignore'  
    rptdata['Group Main#'] = rptdata['Group Main#'].fillna(method = 'ffill')
    rptdata = rptdata.dropna(axis=0, subset=['Account Number']).reset_index(drop=True)
    rptdata = rptdata.loc[:, ~rptdata.columns.str.contains('^Unnamed')]
    
    #rptdata = rptdata.sort_values(by=['Group Main#', 'Account Number', 'State Code'])
    rptdata = rptdata.reset_index(drop=True)
    rptdata1 = rptdata
    rptdata1['combined'] = rptdata1['Group Main#'].astype(str) + ";" + rptdata1['Account Number'].astype(str) + ";" + rptdata1['JR#'].astype(str) + ";" +  dbdata['GL Amount'].astype(str) + ";" + rptdata1['Member Name'].astype(str) + ";" + rptdata1['State Code'].astype(str) 
    #print(rptdata1.head(25))
    
    
    #------------------
    com_df = pd.concat([dbdata, rptdata1], ignore_index=True)
    
    
    #---------------------
    
    
    if(dbdata.reset_index(drop=True).equals(rptdata1.reset_index(drop=True))):
        html_logger.dbg("ROw and column count matches")
    else:
        dfdiff = rptdata1[~(rptdata1['combined'].isin(dbdata['combined']))]
        html_logger.err("ROw and column count doesn't match. row count of dbdata is " + str(dbdata.shape[0]) + "/"  + str(dbdata.shape[1]) + " but the row count of Report is " + str(rptdata1.shape[0]) + "/"  + str(rptdata1.shape[1]))
        print(dfdiff.head(25))
        
    cols = [['Group Main','Group Main#','float'],['Account Number','Account Number','float'],['JR_Num','JR#','string'],['GL Amount','GL Amount','float'],
            ['D_C','Debit(D) or Credit(C) Indicator','String'],['Cycle Date','Cycle Date','String'],['Member Name','Member Name','String'],['Branch','Branch','float'],['Dept','Dept','float'],
            ['Cert Number','Cert Number','float'],['State Code','State Code','float'],['Claim Office','Claim Office','float']]
    
    DFCompare.compare(dbdata,rptdata1,cols)
 
def  partyDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_DIM"  
    EBEN_SQL = "Select * from base.Party_Dim"
    
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyDim_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
  
    cols = [['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],['PARTY_TYP_CD','PARTY_TYP_CD','String'],['PARTY_TYP_DESCR','PARTY_TYP_DESCR','String'],
            ['PARTY_ROLE_TYP_TXT','PARTY_ROLE_TYP_TXT','String'],['PLCYHLDR_MSTR_NBR','PLCYHLDR_MSTR_NBR','String'],['PLCYHLDR_BR_NBR','PLCYHLDR_BR_NBR','String'],['PLCYHLDR_DEPT_NBR','PLCYHLDR_DEPT_NBR','String'],
            ['CMPNY_PREF_NAME','CMPNY_PREF_NAME','String'],['BILL_CNTCT_NAME','BILL_CNTCT_NAME','String'],['PLCYHLDR_BILL_SUBSYS_CD','PLCYHLDR_BILL_SUBSYS_CD','String'],['PLCYHLDR_BILL_SUBSYS_DESCR','PLCYHLDR_BILL_SUBSYS_DESCR','String'],
            ['RGNL_GRP_OFF_NAME','RGNL_GRP_OFF_NAME','String'],['RGNL_GRP_OFF_NBR','RGNL_GRP_OFF_NBR','String'],['PLCYHLDR_EFF_DT','PLCYHLDR_EFF_DT','String'],['PLCYHLDR_TERM_DT','PLCYHLDR_TERM_DT','String'],
            ['PLCYHLDR_REINSTMT_DT','PLCYHLDR_REINSTMT_DT','String'],['PLCYHLDR_STAT_CD','PLCYHLDR_STAT_CD','String'],['PLCYHLDR_STAT_DESCR','PLCYHLDR_STAT_DESCR','String'],['PREM_PAY_GRACE_PER_CD','PREM_PAY_GRACE_PER_CD','String'],
            ['PREM_PAY_GRACE_PER_DESCR','PREM_PAY_GRACE_PER_DESCR','String'],['PD_THRU_DT','PD_THRU_DT','String'],['PLCYHLDR_FRST_SUBSD_PREF_NAME','PLCYHLDR_FRST_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_SEC_SUBSD_PREF_NAME','PLCYHLDR_SEC_SUBSD_PREF_NAME','String'],['PLCYHLDR_THIRD_SUBSD_PREF_NAME','PLCYHLDR_THIRD_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_FOURTH_SUBSD_PREF_NAME','PLCYHLDR_FOURTH_SUBSD_PREF_NAME','String'],['PLCYHLDR_LEGAL_NAME','PLCYHLDR_LEGAL_NAME','String'],['PLCYHLDR_STD_INDY_CLSFCTN_CD','PLCYHLDR_STD_INDY_CLSFCTN_CD','String'],
            ['COV_TYP_CD','COV_TYP_CD','String'],['ALLNC_CD','ALLNC_CD','String'],['ALLNC_DESCR','ALLNC_DESCR','String'],['EOB_MAIL_RCPNT_TYP_CD','EOB_MAIL_RCPNT_TYP_CD','String'],['EOB_MAIL_RCPNT_TYP_DESCR','EOB_MAIL_RCPNT_TYP_DESCR','String'],
            ['PM_CUR_IND','PM_CUR_IND','String']
]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 
    
def benPlanDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):    
    ETPRS_SQL = "Select * from Ben_Plan_dim"  
    EBEN_SQL = "Select * from base.Ben_Plan_dim"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("benPlanDim_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],
            ['BEN_PLAN_NBR','BEN_PLAN_NBR','String'],['BEN_PLAN_TYP_CD','BEN_PLAN_TYP_CD','String'],['BEN_PLAN_TYP_DESCR','BEN_PLAN_TYP_DESCR','String'],['PLAN_EFF_DT','PLAN_EFF_DT','String'],
            ['BEN_MAX_PER_NBR','BEN_MAX_PER_NBR','String'],['BEN_MAX_PER_TYP_CD','BEN_MAX_PER_TYP_CD','String'],['BEN_MAX_PER_TYP_DESCR','BEN_MAX_PER_TYP_DESCR','String'],
            ['OTH_INCM_INTGRTN_MTHD_CD','OTH_INCM_INTGRTN_MTHD_CD','String'],['OTH_INCM_INTGRTN_MTHD_DESCR','OTH_INCM_INTGRTN_MTHD_DESCR','String'],['DISBLTY_BEN_CONT_PER_CD','DISBLTY_BEN_CONT_PER_CD','String'],
            ['DISBLTY_BEN_CONT_PER_DESCR','DISBLTY_BEN_CONT_PER_DESCR','String'],['IMMED_HOSP_BEN_IND','IMMED_HOSP_BEN_IND','String'],['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','String'],
            ['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','String'],['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','String'],
            ['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','String'],['RTRN_TO_WRK_PER_NBR','RTRN_TO_WRK_PER_NBR','String'],['RTRN_TO_WRK_PER_TYP_CD','RTRN_TO_WRK_PER_TYP_CD','String']
            ,['RTRN_TO_WRK_PER_DESCR','RTRN_TO_WRK_PER_DESCR','String'],['POST_EFF_DT_TRTMT_FREE_PER_NBR','POST_EFF_DT_TRTMT_FREE_PER_NBR','String'],['POST_EFF_DT_TRTMT_FREE_TYP_CD','POST_EFF_DT_TRTMT_FREE_TYP_CD','String'],
            ['POST_EFF_DT_TRTMT_FREE_DESCR','POST_EFF_DT_TRTMT_FREE_DESCR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','String'],
            ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','String'],
            ['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PRE_EXIT_COND_NOT_CVRD_PER_NBR','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','String'],
            ['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','String'],['CVRD_EARN_TYP_CD','CVRD_EARN_TYP_CD','String'],['CVRD_EARN_TYP_DESCR','CVRD_EARN_TYP_DESCR','String'],
            ['FULL_TM_STAT_QLFCTN_THRSHLD_NBR','FULL_TM_STAT_QLFCTN_THRSHLD_NBR','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','String'],
            ['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','String'],['INDV_COV_EFF_TYP_CD','INDV_COV_EFF_TYP_CD','String'],
            ['INDV_COV_EFF_TYP_DESCR','INDV_COV_EFF_TYP_DESCR','String'],['SURV_BEN_PER_NBR','SURV_BEN_PER_NBR','String'],['SURV_BEN_PER_TYP_CD','SURV_BEN_PER_TYP_CD','String'],
            ['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String'],
            ['PM_BATCH_ID','PM_BATCH_ID','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def partyPlanFact_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_PLAN_FACT"  
    EBEN_SQL = "Select * from base.PARTY_PLAN_FACT"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyPlanFact_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PARTY_PLAN_FACT_GEN_ID','PARTY_PLAN_FACT_GEN_ID','String'],['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],
            ['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['ERISA_APPL_CD','ERISA_APPL_CD','String'],
            ['ERISA_APPL_DESCR','ERISA_APPL_DESCR','String'],['PLCYHLDR_PLAN_CLS_CD','PLCYHLDR_PLAN_CLS_CD','String'],['PLCYHLDR_PLAN_CLS_DESCR','PLCYHLDR_PLAN_CLS_DESCR','String'],
            ['NEW_EMP_WAIT_PER_TYP_CD','NEW_EMP_WAIT_PER_TYP_CD','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEW_EMP_WAIT_PER_TYP_DESCR','String'],['NEW_EMP_COV_QLFCTN_TYP_CD','NEW_EMP_COV_QLFCTN_TYP_CD','String'],
            ['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEW_EMP_COV_QLFCTN_TYP_DESCR','String'],['INIT_EMP_WAIT_PER_TYP_CD','INIT_EMP_WAIT_PER_TYP_CD','String'],['INIT_EMP_WAIT_PER_TYP_DESCR','INIT_EMP_WAIT_PER_TYP_DESCR','String'],
            ['MNTL_ILL_LIMIT_IND','MNTL_ILL_LIMIT_IND','String'],['SPL_COND_BEN_DUR_TYP_CD','SPL_COND_BEN_DUR_TYP_CD','String'],['SPL_COND_BEN_DUR_TYP_DESCR','SPL_COND_BEN_DUR_TYP_DESCR','String'],
            ['SPL_DISBLTY_COND_LMT_IND','SPL_DISBLTY_COND_LMT_IND','String'],['SPS_DISBLTY_BEN_ELIM_PER_CD','SPS_DISBLTY_BEN_ELIM_PER_CD','String'],['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','String'],
            ['SPS_DISBLTY_MAX_MTHLY_BEN_AMT','SPS_DISBLTY_MAX_MTHLY_BEN_AMT','String'],['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','String'],['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','String'],
            ['DISBLTY_BEN_FREQ_CD','DISBLTY_BEN_FREQ_CD','String'],['DISBLTY_BEN_FREQ_TYP_CD','DISBLTY_BEN_FREQ_TYP_CD','String'],['DISBLTY_BEN_FREQ_TYP_DESCR','DISBLTY_BEN_FREQ_TYP_DESCR','String'],
            ['COST_OF_LVNG_BEN_ADJ_PER_CD','COST_OF_LVNG_BEN_ADJ_PER_CD','String'],['COST_OF_LVNG_BEN_ADJ_PER_DESCR','COST_OF_LVNG_BEN_ADJ_PER_DESCR','String'],['COST_OF_LVNG_BEN_ADJ_TYP_CD','COST_OF_LVNG_BEN_ADJ_TYP_CD','String'],
            ['DBL_ELIM_PER_IND','DBL_ELIM_PER_IND','String'],['DISBLTY_MAX_BEN_AMT','DISBLTY_MAX_BEN_AMT','String'],['DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','String'],
            ['MAX_COV_BEN_AMT','MAX_COV_BEN_AMT','String'],['DISBLTY_BEN_SLRY_PCT','DISBLTY_BEN_SLRY_PCT','String'],['DISBLTY_MIN_NET_BEN_PCT','DISBLTY_MIN_NET_BEN_PCT','String'],
            ['FRST_DAY_HOSP_BEN_IND','FRST_DAY_HOSP_BEN_IND','String'],['FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','String'],['EMPLR_CONTRIB_PCT','EMPLR_CONTRIB_PCT','String'],
            ['INIT_EMP_COV_QLFCTN_TYP_CD','INIT_EMP_COV_QLFCTN_TYP_CD','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INIT_EMP_COV_QLFCTN_TYP_DESCR','String'],['MIN_COV_BEN_AMT','MIN_COV_BEN_AMT','String'],
            ['MIN_NET_BEN_AMT','MIN_NET_BEN_AMT','String'],['FIX_COV_BEN_AMT','FIX_COV_BEN_AMT','String'],['INSUR_INCR_UNIT_AMT','INSUR_INCR_UNIT_AMT','String'],['DISBLTY_ELIM_PER_QLFCTN_IND','DISBLTY_ELIM_PER_QLFCTN_IND','String'],
            ['COLA_MAX_OCCUR_NBR','COLA_MAX_OCCUR_NBR','String'],['COLA_OCCUR_TYP','COLA_OCCUR_TYP','String'],['COLA_OCCUR_TYP_DESCR','COLA_OCCUR_TYP_DESCR','String'],['COLA_CD','COLA_CD','String'],
            ['COLA_DESCR','COLA_DESCR','String'],['CVRD_EARN_TYP_CD','CVRD_EARN_TYP_CD','String'],['CVRD_EARN_TYP_DESCR','CVRD_EARN_TYP_DESCR','String'],['PREM_WVR_CD','PREM_WVR_CD','String'],
            ['PREM_WVR_DESCR','PREM_WVR_DESCR','String'],['CMPNY_TAX_AGNT_CD','CMPNY_TAX_AGNT_CD','String'],['CMPNY_TAX_AGNT_DESCR','CMPNY_TAX_AGNT_DESCR','String'],['DISBLTY_FLAT_BEN_AMT','DISBLTY_FLAT_BEN_AMT','String'],
            ['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String'],['PM_BATCH_ID','PM_BATCH_ID','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def partyDtlExtDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_DTL_EXT_DIM"  
    EBEN_SQL = "Select * from base.PARTY_DTL_EXT_DIM"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyDtlExtDim_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PARTY_DTL_EXT_DIM_GEN_ID','PARTY_DTL_EXT_DIM_GEN_ID','String'],['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['PARTY_DTL_TYP_CD','PARTY_DTL_TYP_CD','String'],
            ['ADDR_FRST_LINE_TXT','ADDR_FRST_LINE_TXT','String'],['ADDR_SEC_LINE_TXT','ADDR_SEC_LINE_TXT','String'],['ADDR_THIRD_LINE_TXT','ADDR_THIRD_LINE_TXT','String'],
            ['ADDR_CITY_NAME','ADDR_CITY_NAME','String'],['ADDR_ZIP_CD','ADDR_ZIP_CD','String'],['ADDR_ST_CD','ADDR_ST_CD','String'],['ADDR_CNTRY_NAME','ADDR_CNTRY_NAME','String'],
            ['PHONE_NBR','PHONE_NBR','String'],['EMAIL_ADDR_TXT','EMAIL_ADDR_TXT','String'],['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],
            ['PM_EFF_END_TS','PM_EFF_END_TS','String'],['PM_BATCH_ID','PM_BATCH_ID','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 
    
def benPlanDimVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from Ben_Plan_dim_vw"  
    EBEN_SQL = "Select * from base.Ben_Plan_dim_vw"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("benPlanDimVW_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],['BEN_PLAN_NBR','BEN_PLAN_NBR','String']
            ,['BEN_PLAN_TYP_CD','BEN_PLAN_TYP_CD','String'],['BEN_PLAN_TYP_DESCR','BEN_PLAN_TYP_DESCR','String'],['PLAN_EFF_DT','PLAN_EFF_DT','String'],['BEN_MAX_PER_NBR','BEN_MAX_PER_NBR','String'],
            ['BEN_MAX_PER_TYP_DESCR','BEN_MAX_PER_TYP_DESCR','String'],['OTH_INCM_INTGRTN_MTHD_CD','OTH_INCM_INTGRTN_MTHD_CD','String'],['OTH_INCM_INTGRTN_MTHD_DESCR','OTH_INCM_INTGRTN_MTHD_DESCR','String'],
            ['DISBLTY_BEN_CONT_PER_CD','DISBLTY_BEN_CONT_PER_CD','String'],['DISBLTY_BEN_CONT_PER_DESCR','DISBLTY_BEN_CONT_PER_DESCR','String'],['IMMED_HOSP_BEN_IND','IMMED_HOSP_BEN_IND','String'],
            ['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','String'],['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','String'],
            ['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','String'],['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','String'],
            ['RTRN_TO_WRK_PER_NBR','RTRN_TO_WRK_PER_NBR','String'],['RTRN_TO_WRK_PER_TYP_CD','RTRN_TO_WRK_PER_TYP_CD','String'],['RTRN_TO_WRK_PER_DESCR','RTRN_TO_WRK_PER_DESCR','String'],
            ['POST_EFF_DT_TRTMT_FREE_PER_NBR','POST_EFF_DT_TRTMT_FREE_PER_NBR','String'],['POST_EFF_DT_TRTMT_FREE_TYP_CD','POST_EFF_DT_TRTMT_FREE_TYP_CD','String'],['POST_EFF_DT_TRTMT_FREE_DESCR','POST_EFF_DT_TRTMT_FREE_DESCR','String'],
            ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],
            ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','String'],['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PRE_EXIT_COND_NOT_CVRD_PER_NBR','String'],
            ['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','String'],
            ['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def partyPlanFactVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_PLAN_FACT_VW"  
    EBEN_SQL = "Select * from base.PARTY_PLAN_FACT_VW"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyPlanFactVW_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PARTY_PLAN_FACT_GEN_ID','PARTY_PLAN_FACT_GEN_ID','String'],['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],['PARTY_ADMIN_SYS_NAME','PARTY_ADMIN_SYS_NAME','String'],
            ['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],['PLAN_ADMIN_SYS_NAME','PLAN_ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],
            ['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['ERISA_APPL_CD','ERISA_APPL_CD','String'],['ERISA_APPL_DESCR','ERISA_APPL_DESCR','String'],['PLCYHLDR_PLAN_CLS_CD','PLCYHLDR_PLAN_CLS_CD','String'],
            ['PLCYHLDR_PLAN_CLS_DESCR','PLCYHLDR_PLAN_CLS_DESCR','String'],['NEW_EMP_WAIT_PER_TYP_CD','NEW_EMP_WAIT_PER_TYP_CD','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEW_EMP_WAIT_PER_TYP_DESCR','String'],
            ['NEW_EMP_COV_QLFCTN_TYP_CD','NEW_EMP_COV_QLFCTN_TYP_CD','String'],['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEW_EMP_COV_QLFCTN_TYP_DESCR','String'],['INIT_EMP_WAIT_PER_TYP_CD','INIT_EMP_WAIT_PER_TYP_CD','String'],
            ['INIT_EMP_WAIT_PER_TYP_DESCR','INIT_EMP_WAIT_PER_TYP_DESCR','String'],['MNTL_ILL_LIMIT_IND','MNTL_ILL_LIMIT_IND','String'],['SPL_COND_BEN_DUR_TYP_CD','SPL_COND_BEN_DUR_TYP_CD','String'],
            ['SPL_COND_BEN_DUR_TYP_DESCR','SPL_COND_BEN_DUR_TYP_DESCR','String'],['SPL_DISBLTY_COND_LMT_IND','SPL_DISBLTY_COND_LMT_IND','String'],['SPS_DISBLTY_BEN_ELIM_PER_CD','SPS_DISBLTY_BEN_ELIM_PER_CD','String'],
            ['DISBLTY_BEN_FREQ_CD','DISBLTY_BEN_FREQ_CD','String'],['DISBLTY_BEN_FREQ_TYP_CD','DISBLTY_BEN_FREQ_TYP_CD','String'],['DISBLTY_BEN_FREQ_TYP_DESCR','DISBLTY_BEN_FREQ_TYP_DESCR','String'],
            ['COST_OF_LVNG_BEN_ADJ_PER_CD','COST_OF_LVNG_BEN_ADJ_PER_CD','String'],['COST_OF_LVNG_BEN_ADJ_PER_DESCR','COST_OF_LVNG_BEN_ADJ_PER_DESCR','String'],['COST_OF_LVNG_BEN_ADJ_TYP_CD','COST_OF_LVNG_BEN_ADJ_TYP_CD','String'],
            ['DBL_ELIM_PER_IND','DBL_ELIM_PER_IND','String'],['DISBLTY_MAX_BEN_AMT','DISBLTY_MAX_BEN_AMT','String'],['DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','String'],['MAX_COV_BEN_AMT','MAX_COV_BEN_AMT','String'],
            ['DISBLTY_BEN_SLRY_PCT','DISBLTY_BEN_SLRY_PCT','String'],['DISBLTY_MIN_NET_BEN_PCT','DISBLTY_MIN_NET_BEN_PCT','String'],['FRST_DAY_HOSP_BEN_IND','FRST_DAY_HOSP_BEN_IND','String'],['FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','String'],
            ['EMPLR_CONTRIB_PCT','EMPLR_CONTRIB_PCT','String'],['INIT_EMP_COV_QLFCTN_TYP_CD','INIT_EMP_COV_QLFCTN_TYP_CD','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INIT_EMP_COV_QLFCTN_TYP_DESCR','String'],
            ['MIN_COV_BEN_AMT','MIN_COV_BEN_AMT','String'],['MIN_NET_BEN_AMT','MIN_NET_BEN_AMT','String'],['FIX_COV_BEN_AMT','FIX_COV_BEN_AMT','String'],['INSUR_INCR_UNIT_AMT','INSUR_INCR_UNIT_AMT','String'],
            ['DISBLTY_ELIM_PER_QLFCTN_IND','DISBLTY_ELIM_PER_QLFCTN_IND','String'],['COLA_MAX_OCCUR_NBR','COLA_MAX_OCCUR_NBR','String'],['COLA_OCCUR_TYP','COLA_OCCUR_TYP','String'],['COLA_CD','COLA_CD','String'],
            ['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 
    
def pmBatch_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PM_BATCH"  
    EBEN_SQL = "Select * from base.PM_BATCH"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("pmBatch_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PM_BATCH_ID','PM_BATCH_ID','String'],['PM_BATCH_STRT_TS','PM_BATCH_STRT_TS','String'],['PM_BATCH_END_TS','PM_BATCH_END_TS','String'],['PM_BATCH_STAT','PM_BATCH_STAT','String'],
            ['PM_BATCH_SCHEMA_NAME','PM_BATCH_SCHEMA_NAME','String'],['PM_BATCH_MODEL_TYP','PM_BATCH_MODEL_TYP','String'],['INS_TS','INS_TS','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def entprsPartyVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from ENTPRS_PARTY_VW"  
    EBEN_SQL = "Select * from base.ENTPRS_PARTY_VW"
    
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("entprsPartyVW_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
  
    cols = [['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],
            ['PARTY_TYP_CD','PARTY_TYP_CD','String'],['PARTY_TYP_DESCR','PARTY_TYP_DESCR','String'],['PARTY_ROLE_TYP_TXT','PARTY_ROLE_TYP_TXT','String'],
            ['PLCYHLDR_MSTR_NBR','PLCYHLDR_MSTR_NBR','String'],['PLCYHLDR_BR_NBR','PLCYHLDR_BR_NBR','String'],['PLCYHLDR_DEPT_NBR','PLCYHLDR_DEPT_NBR','String'],
            ['CMPNY_PREF_NAME','CMPNY_PREF_NAME','String'],['BILL_CNTCT_NAME','BILL_CNTCT_NAME','String'],['PLCYHLDR_BILL_SUBSYS_CD','PLCYHLDR_BILL_SUBSYS_CD','String'],
            ['PLCYHLDR_BILL_SUBSYS_DESCR','PLCYHLDR_BILL_SUBSYS_DESCR','String'],['RGNL_GRP_OFF_NAME','RGNL_GRP_OFF_NAME','String'],['RGNL_GRP_OFF_NBR','RGNL_GRP_OFF_NBR','String'],
            ['PLCYHLDR_EFF_DT','PLCYHLDR_EFF_DT','String'],['PLCYHLDR_TERM_DT','PLCYHLDR_TERM_DT','String'],['PLCYHLDR_REINSTMT_DT','PLCYHLDR_REINSTMT_DT','String'],
            ['PLCYHLDR_STAT_CD','PLCYHLDR_STAT_CD','String'],['PLCYHLDR_STAT_DESCR','PLCYHLDR_STAT_DESCR','String'],['PREM_PAY_GRACE_PER_CD','PREM_PAY_GRACE_PER_CD','String'],
            ['PREM_PAY_GRACE_PER_DESCR','PREM_PAY_GRACE_PER_DESCR','String'],['PD_THRU_DT','PD_THRU_DT','String'],['PLCYHLDR_FRST_SUBSD_PREF_NAME','PLCYHLDR_FRST_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_SEC_SUBSD_PREF_NAME','PLCYHLDR_SEC_SUBSD_PREF_NAME','String'],['PLCYHLDR_THIRD_SUBSD_PREF_NAME','PLCYHLDR_THIRD_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_FOURTH_SUBSD_PREF_NAME','PLCYHLDR_FOURTH_SUBSD_PREF_NAME','String'],['PLCYHLDR_LEGAL_NAME','PLCYHLDR_LEGAL_NAME','String'],
            ['COV_TYP_CD','COV_TYP_CD','String'],['PM_CUR_IND','PM_CUR_IND','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols)
    
def Eben_3717_1():
    print("Start of test")
    """
    #dev
    ETPRS_Server = "SQLD10747,4242" #"SQLD10785,4242"
    ETPRS_DB = "ENTPRS_CLAIMS_DM" 
        
    EBEN_Server = "SQLD10203,4242"
    EBEN_DB = "eBEN"
    """
    """
    #stg
    ETPRS_Server = "SQLT10747,5353" #"SQLD10785,4242"
    ETPRS_DB = "ENTPRS_CLAIMS_DM" 
        
    EBEN_Server = "SQLT10203,5353"
    EBEN_DB = "eBEN"
    """
    ETPRS_Server = "SQLP10747,2626" #"SQLD10785,4242"
    ETPRS_DB = "ENTPRS_CLAIMS_DM" 
        
    EBEN_Server = "SQLP10203,2626"
    EBEN_DB = "eBEN"
    
    partyDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    benPlanDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    partyPlanFact_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    partyDtlExtDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    benPlanDimVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    partyPlanFactVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    pmBatch_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    entprsPartyVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    
    
    print("End of test")   




def FineosDailyAccountingDetailsReport():
    html_logger.info("Start of Validation of Fineos Daily Accounting Details Report")
    sql1 = """SELECT   GRP_NBR AS 'Group Main'
        ,ACCT_NBR AS 'Account Number'
        ,JRNL_NBR AS 'JR_Num'
        ,g.PAY_AMT AS GL_Amonut
        ,DRCR_IND AS 'D_C'
        ,g.LAST_UPDT_TS AS 'Cycle Date'
        ,PAYEE_NAME AS 'Member Name'
        ,SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch'
        ,RIGHT([POLICY_NUMBER], 3) AS 'Dept'
        ,CERT_NBR AS 'Cert Number'
        ,claim_number
              ,ST_CD AS 'State Code'
        ,CLAIM_OFF_CD AS 'Claim Office'            
FROM [ENTPRS_CLAIMS_DM].dbo.[GL_ELMNT_DTL] g
JOIN [ENTPRS_CLAIMS_DM].dbo.[PAY_RQST_INFO] r ON r.PAY_RQST_NBR = g.PAY_RQST_NBR
"""

    sql1x = """SELECT GRP_NBR AS 'Group Main'
        ,ACCT_NBR AS 'Account Number'
        ,JRNL_NBR AS 'JR_Num'
        ,PAY_AMT AS 'GL Amonut'
        ,DRCR_IND AS 'D_C'
        ,LAST_UPDT_TS AS 'Cycle Date'
        ,MBR_NAME AS 'Member Name'
        ,SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch'
        ,RIGHT([POLICY_NUMBER], 3) AS 'Dept'
        ,CERT_NBR AS 'Cert Number'
        ,ST_CD AS 'State Code'
        ,CLAIM_OFF_CD AS 'Claim Office'
       FROM [ENTPRS_CLAIMS_ODS].[FAI].[GL_ELMNT_DTL]
"""
    """ ,CASE
                WHEN DRCR_IND = 'D' THEN PAY_AMT
        END as 'Debit Amount'
        ,CASE
                WHEN DRCR_IND = 'C' THEN PAY_AMT
        END as 'Credit Amount'"""
    
    """SELECT GRP_NBR AS 'Group Main',ACCT_NBR AS 'Account Number',JRNL_NBR AS 'JR_Num',PAY_AMT AS 'GL Amount',DRCR_IND AS 'D_C'
        ,LAST_UPDT_TS AS 'Cycle Date',MBR_NAME AS 'Member Name',SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch',RIGHT([POLICY_NUMBER], 3) AS 'Dept'
        ,CERT_NBR AS 'Cert Number' ,ST_CD AS 'State Code',CLAIM_OFF_CD AS 'Claim Office' FROM [ENTPRS_CLAIMS_ODS].[FAI].[GL_ELMNT_DTL] order by GRP_NBR"""
        
    dbdata = Parser.parseSQL(sql1,"SQLT10747,5353","ENTPRS_CLAIMS_DM")
    newdbdata = pd.DataFrame(columns = ['combined'])
    
    #---------- some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char
    #----------- some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
    
    i = 0
   
    
    for index,rows in dbdata.iterrows():  # Looping through Rows
        i = i + 1
        xx = 0
        for (columnName, columnData) in rows.items():  # Looping through Cols
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):  #checking if it's a number..if yes, convert that to float and convert back to string'
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                if columnName == 'Cycle Date' :   # date formats  - some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char  #some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
                    if len(str(rows['Cycle Date'])) == 26:
                        cycleDate1 = str(rows['Cycle Date'])[:-3]
                    elif len(str(rows['Cycle Date'])) == 19:
                        cycleDate1 = str(rows['Cycle Date']) + ".000"   
                    else:
                        cycleDate1 = str(rows['Cycle Date'])
                    StringTmp = cycleDate1       
                else:
                    StringTmp = columnData
                    
                if(str(StringTmp).lstrip("-").isdigit()):  # - is removed to remove negative amounts so that it can be considered as digit
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  
        
        newdbdata.loc[i] = String1
        
    #print(newdbdata.head(5))   
    #newdbdata.to_csv("c:\\Srikanth\\CSVDB.csv")
     
      

#-------------------- Read from Excel 
#-------------------- fill blank Group main with previous cell values - to fill out he merged cells
#--------------------- REmove first row of every group main the report, which has blank values
#--------------------- remove blank columns, SOrt & re index   & iterate to add all columns
  
    rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\Claims_ReportValidation\\FINEOS Daily Accounting Details Report_1104.xls', sheet_name = 0, dtype=str)  #,encoding='cp437'  ,errors = 'ignore'  
    rptdata['Group Main#'] = rptdata['Group Main#'].fillna(method = 'ffill')
    rptdata = rptdata.dropna(axis=0, subset=['Account Number']).reset_index(drop=True)
    rptdata = rptdata.loc[:, ~rptdata.columns.str.contains('^Unnamed')]
    
    #rptdata = rptdata.sort_values(by=['Group Main#', 'Account Number', 'JR#','GL Amount','Member Name','State Code'])
    rptdata = rptdata.reset_index(drop=True)
    rptdata['State Code'] = rptdata['State Code'].replace(np.nan,'None')
    rptdata1 = rptdata
    newrptdata1 = pd.DataFrame(columns = ['combined'])
    i = 0
    for index,rows in rptdata1.iterrows():  #Looping through Rows
        i = i + 1
        
        xx = 0
        for (columnName, columnData) in rows.items():    #Looping through Columns of a row & appending them 
            xx = xx + 1
            if xx == 1:
                String1 = str(columnData)
                if(String1.lstrip("-").isdigit()):
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                StringTmp = str(columnData)
                if(StringTmp.lstrip("-").isdigit()):
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  #Looping through Columns of a row & appending them 
        
        newrptdata1.loc[i] = String1
        
    #print(newrptdata1.head(5))   
    #newrptdata1.to_csv("c:\\Srikanth\\CSV1.csv") 
        
    cols = [['combined','combined','String']]
    DFCompare.compare_fullReport(newdbdata,newrptdata1,cols)
    html_logger.info("End of Validation of Fineos Daily Accounting Details Report")
    #----------------------------------------------------------------------------------------------------------------


def EC3802_PremiumWaiver(polnum, benType):
    html_logger.info("Start of Validation of Premium Waiver Report for Policy Number " + str(polnum) + " and Ben Type " + benType)
    #benType = 'STD'
    sql1 = """
        

select distinct case when g.ben_CASE_NBR is null then b.claim_case_nbr else g.ben_case_nbr end Claim_Number,
a.case_eff_strt_dt "Claimant Date of Disability" ,c.PRSN_BIRTH_DT "Claimant Date of Birth",
'xxx-xx-'+substring(c.prsn_natl_insur_nbr,6,4) "Claimant Taxpayer ID" ,c.PRSN_LAST_NM "Claimant Last Name ",c.PRSN_FRST_NM "Claimant First Name",
case
when g4.BEN_ELIG_WVR_CD=71455001 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Beginning of Elimination Period' then a.case_eff_strt_dt
when g4.BEN_ELIG_WVR_CD=71455002 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: After Elimination Period' then g.BEN_STRT_DT
when g4.BEN_ELIG_WVR_CD=71455003 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: First of Month After 30 Days' then
DATEADD(m,1,DATEADD(m,datediff(m,0,(dateadd(day,30,a.case_eff_strt_dt))),0))
when g4.BEN_ELIG_WVR_CD=71455004 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: First of Month After 52wks Coverage'  then
DATEADD(m,1,DATEADD(m,datediff(m,0,(dateadd(week,52,a.case_eff_strt_dt))),0))
when g4.BEN_ELIG_WVR_CD=71455005 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins Immed following 12W of Dis' then dateadd(day,85,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455006 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins First Day of Disability' then a.case_eff_strt_dt
when g4.BEN_ELIG_WVR_CD=71455007 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 91st Day of Disability' then dateadd(day,91,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455008 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 31st Day of Disability' then dateadd(day,31,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455009 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 46th Day of Disability' then dateadd(day,46,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455010 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 61st Day of Disability' then dateadd(day,61,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455011 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 76th Day of Disability' then dateadd(day,76,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455012 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 101st Day of Disability' then dateadd(day,101,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455013 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 121st Day of Disability' then dateadd(day,121,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455014 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: Begins 366th Day of Disability' then dateadd(day,366,a.case_eff_strt_dt)
when g4.BEN_ELIG_WVR_CD=71455015 and g4.BEN_ELIG_WVR_DESC='Waiver of Premium: No Waiver' then ''
else ''
end "Waiver Begin Date" ,
case when [BEN_TERM_DT] is null or [BEN_TERM_DT] > GETDATE() then ''
when [BEN_TERM_DT] is not null and [BEN_TERM_DT]<GETDATE()  and
g1.ben_per_typ_desc in ('Fully Certified','Partial Return to Work - ER','Partial Return to Work - other ER') and
g1.BEN_PER_BEN_STAT ='Approved' then BEN_PAY_OUT_EVNT_PER_END_DT
end "Waiver End Date" ,
g4.BEN_ELIG_WVR_DESC "Waiver Description",
i.PROC_STEP_NM "Claim Status",null "Class Description"

from    [ENTPRS_CLAIMS_DM].dbo.[CASE] a --(VBI_CASE)
join [ENTPRS_CLAIMS_DM].dbo.claim b on a.case_gen_id=b.case_gen_id --(VBI_claim in LZ)
--left join [ENTPRS_CLAIMS_DM].dbo.Document c1 on c1.CASE_CASE_NBR=a.case_case_nbr
left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.pcr_case_nbr=b.CLAIM_CASE_NBR
left join [ENTPRS_CLAIMS_DM].dbo.PERSON c on c.[PRSN_GEN_ID]=d.[PRSN_GEN_ID]
left join [ENTPRS_CLAIMS_DM].dbo.[PROCESS_INSTANCE] h on h.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP] i on i.PROC_STEP_GEN_ID=h.PROC_STEP_GEN_ID
left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = b.[CLAIM_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PERIOD] g1 on g1.[BEN_GEN_ID]=g.[BEN_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] g4 on g4.[BEN_GEN_ID]=g.[BEN_GEN_ID]
 -- select distinct BEN_ELIG_WVR_DESC from [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] where ben_elig_wvr_cd=71424002
left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_PAY_OUT_EVNT] g5 on g5.[BEN_GEN_ID]=g.[BEN_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[POLICY_SNAPSHOT] g2 on g2.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
left join [ENTPRS_CLAIMS_DM].[dbo].[CONTRACT_STUB] g3 on g2.[C_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_CLS_ID]
                AND g2.[I_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_INDX_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[COVERAGE] g6 ON g2.[PLCYSNAP_GEN_IND] = g6.[PLCYSNAP_GEN_IND]
left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_MAIN]  j on j.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] n on n.claim_GEN_ID=b.claim_GEN_ID
left join [ENTPRS_CLAIMS_DM].[dbo].[ORGANIZATION] n1 on g3.CNCTSTUB_REF_NBR=n1.[ORG_REF_NBR]
LEFT JOIN [ENTPRS_CLAIMS_DM].qa.[PARTY_DIM] o ON o.[ADMIN_SYS_PARTY_KEY_TXT] = n1.ORG_REF_NBR
left JOIN [ENTPRS_CLAIMS_DM].qa.[PARTY_PLAN_FACT] p ON p.PARTY_DIM_GEN_ID = o.PARTY_DIM_GEN_ID
left join [ENTPRS_CLAIMS_DM].qa.ben_plan_dim q on q.ben_plan_dim_gen_id=p.ben_plan_dim_gen_id


where  CNCTSTUB_REF_NBR in (""" + polnum + """)
and g6.CVRG_PROD_CTGY_CD = ISNULL(substring(g.BEN_CASE_NBR, [ENTPRS_CLAIMS_DM].DBO.INSTR(g.BEN_CASE_NBR, '-', 1, 2) + 1, 3), g6.CVRG_PROD_CTGY_CD)

 and a.pm_cur_ind ='Y' and B.pm_cur_ind ='Y'
 and g.ben_case_nbr is not null and g.ben_case_nbr like '%""" + benType + """%' """
        
    dbdata = Parser.parseSQL(sql1,"SQLD10746,4242","ENTPRS_CLAIMS_ODS")
    newdbdata = pd.DataFrame(columns = ['combined'])
    
    print("SQL is \n " + sql1 + "\n")
    
    dbdata = dbdata.replace({'NaT': ''})
    dbdata = dbdata.fillna('')
    
    #---------- some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char
    #----------- some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
    
    i = 0
   
    
    for index,rows in dbdata.iterrows():  # Looping through Rows
        i = i + 1
        xx = 0
        for (columnName, columnData) in rows.items():  # Looping through Cols
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):  #checking if it's a number..if yes, convert that to float and convert back to string'
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                if columnName == 'Waiver End Date' :   # date formats  - some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char  #some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
                    StringTmp = "" 
                elif columnName == 'Waiver Begin Date':
                    StringTmp =  str(rows['Waiver Begin Date'])[:10]
                    if StringTmp == '1900-01-01':
                        StringTmp = ""         
                else:
                    StringTmp = columnData
                    
                if(str(StringTmp).lstrip("-").isdigit()):  # - is removed to remove negative amounts so that it can be considered as digit
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp)
                
                if StringTmp == 'NONE':
                   StringTmp = '' 
                        
                String1 = String1 + "," +   StringTmp  
        
        newdbdata.loc[i] = String1
        
    #print(newdbdata.head(5))   
    #newdbdata.to_csv("c:\\Srikanth\\CSVDB.csv") 
     
    print(newdbdata) 

#-------------------- Read from Excel 
#-------------------- fill blank Group main with previous cell values - to fill out he merged cells
#--------------------- REmove first row of every group main the report, which has blank values
#--------------------- remove blank columns, SOrt & re index   & iterate to add all columns
  
    rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\Claims_ReportValidation\\Disability Premium Waiver_0805.xls', sheet_name = benType, dtype=str)  #,encoding='cp437'  ,errors = 'ignore'     
    rptdata = rptdata.replace({'NaT': ''})
    rptdata = rptdata.fillna('')
    
    #rptdata = rptdata.replace({np.NaN: ''}, inplace=True)
    
    rptdata = rptdata.reset_index(drop=True)
    rptdata1 = rptdata
    newrptdata1 = pd.DataFrame(columns = ['combined'])
    i = 0
    for index,rows in rptdata1.iterrows():  #Looping through Rows
        i = i + 1
        
        xx = 0
        for (columnName, columnData) in rows.items():    #Looping through Columns of a row & appending them 
            xx = xx + 1
            if xx == 1:
                String1 = str(columnData)
                if(String1.isdigit()):
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:  
                StringTmp = str(columnData)
                if(xx == 2 or xx == 3 or xx ==7):
                    #print(StringTmp)
                    if(xx==7 and StringTmp != ''):
                        StringTmp = datetime.datetime.strptime(StringTmp.strip("'"),'%m/%d/%Y').strftime('%Y-%m-%d')
                    StringTmp = StringTmp[:10]
                
                
                    
                if(StringTmp.isdigit()):
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  #Looping through Columns of a row & appending them 
        
        newrptdata1.loc[i] = String1
        
    #print(newrptdata1.head(5))   
    #newrptdata1.to_csv("c:\\Srikanth\\CSV1.csv") 
        
    cols = [['combined','combined','String']]
    DFCompare.compare_fullReport(newdbdata,newrptdata1,cols)
    html_logger.info("End of Validation of Premium Waiver Report for Policy Number " + str(polnum) + " and Ben Type " + benType)
 
 
def FINEOSCashDisbursementRequestDetailReport(src_cd):
    html_logger.info("Start of Validation of FINEOS Cash Disbursement Request Detail Report for Source Code " + str(src_cd))
    #sql1 = """SELECT distinct A.SRC_CD,A.PAY_RQST_NBR,A.PLAN_PLCY_NBR,CLAIM_NUMBER,A.PAYEE_NAME,A.PAY_AMT,B.GL_ACCT_NBR FROM ENTPRS_CLAIMS_ODS.FAI.PAY_RQST_INFO A INNER JOIN ENTPRS_CLAIMS_ODS.FAI.PAY_ACCT_DTL B ON A.PAY_RQST_INFO_GEN_ID = B.PAY_RQST_INFO_GEN_ID inner JOIN ENTPRS_CLAIMS_ODS.[FAI].[GL_ELMNT_DTL] d on d.PAY_RQST_NBR = a.PAY_RQST_NBR where SRC_CD ='""" + src_cd + """' """
    sql1 =  """SELECT distinct A.SRC_CD,A.PAY_RQST_NBR,A.PLAN_PLCY_NBR,CLAIM_NUMBER,A.PAYEE_NAME,A.PAY_AMT,B.GL_ACCT_NBR FROM ENTPRS_CLAIMS_dm.dbo.PAY_RQST_INFO A INNER JOIN ENTPRS_CLAIMS_Dm.dbo.PAY_ACCT_DTL B ON A.PAY_RQST_INFO_GEN_ID = B.PAY_RQST_INFO_GEN_ID inner JOIN ENTPRS_CLAIMS_DM.dbo.[GL_ELMNT_DTL] d on d.PAY_RQST_NBR = a.PAY_RQST_NBR WHERE SRC_CD ='""" + src_cd + "' "  #LEFT(a.SRC_CD,2) <> 'GL'
   
    #dbdata = Parser.parseSQL(sql1,"SQLD10746,4242","ENTPRS_CLAIMS_ODS")
    dbdata = Parser.parseSQL(sql1,"SQLT10747,5353","ENTPRS_CLAIMS_DM")
    dbdata = dbdata.drop('CLAIM_NUMBER',1)
    newdbdata = pd.DataFrame(columns = ['combined'])
    
    #---------- some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char
    #----------- some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
    
    i = 0
   
    
    for index,rows in dbdata.iterrows():  # Looping through Rows
        i = i + 1
        xx = 0
        for (columnName, columnData) in rows.items():  # Looping through Cols
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):  #checking if it's a number..if yes, convert that to float and convert back to string'
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                StringTmp = columnData  
                if(str(StringTmp).lstrip("-").isdigit()):  # - is removed to remove negative amounts so that it can be considered as digit
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  
        
        newdbdata.loc[i] = String1
        
    #print(newdbdata.head(5))   
    #newdbdata.to_csv("c:\\Srikanth\\CSVDB.csv") 
      

#-------------------- Read from Excel 
#-------------------- fill blank Group main with previous cell values - to fill out he merged cells
#--------------------- REmove first row of every group main the report, which has blank values
#--------------------- remove blank columns, SOrt & re index   & iterate to add all columns
  
    rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\Claims_ReportValidation\\FINEOS Cash Disbursement Request Detail Report_stg.xls', sheet_name = "Source Code " + src_cd, dtype=str)  #,encoding='cp437'  ,errors = 'ignore'  
    rptdata['Source Code'] = rptdata['Source Code'].fillna(method = 'ffill')
    #rptdata = rptdata.dropna(axis=0, subset=['Account Number']).reset_index(drop=True)
    rptdata = rptdata.loc[:, ~rptdata.columns.str.contains('^Unnamed')]
    rptdata = rptdata.drop('Claim\nNumber',1)
    
    #rptdata = rptdata.sort_values(by=['Group Main#', 'Account Number', 'JR#','GL Amount','Member Name','State Code'])
    rptdata = rptdata.reset_index(drop=True)
    #rptdata['Policy Holder\nNumber'] = rptdata['Policy Holder\nNumber'].replace('NAN', 'None')
    rptdata['Policy Holder\nNumber'] = rptdata['Policy Holder\nNumber'].replace(np.NaN,'NONE')
    
    rptdata1 = rptdata
    newrptdata1 = pd.DataFrame(columns = ['combined'])
    i = 0
    for index,rows in rptdata1.iterrows():  #Looping through Rows
        i = i + 1
        
        xx = 0
        for (columnName, columnData) in rows.items():    #Looping through Columns of a row & appending them 
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                if columnName == 'Check Amount' :
                   StringTmp = columnData[1:]
                else:    
                    StringTmp = columnData
                    
                if(str(StringTmp).isdigit()):
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  #Looping through Columns of a row & appending them 
        
        newrptdata1.loc[i] = String1
        
    #print(newrptdata1.head(5))   
    #newrptdata1.to_csv("c:\\Srikanth\\CSV1.csv") 
        
    cols = [['combined','combined','String']]
    DFCompare.compare_fullReport(newdbdata,newrptdata1,cols)
    html_logger.info("End of Validation of FINEOS Cash Disbursement Request Detail Report for Source Code " + str(src_cd))
    #----------------------------------------------------------------------------------------------------------------


def DisabilityClaimsStatusReport_AllClaims(polnum, benType):
    html_logger.info("Start of Disability Claims Status Report_All Claims " + str(polnum) + " and Ben Type " + benType)
    #benType = 'STD'
    sql1 = """
            declare @plcy_nbr varchar(50)
            declare @ben_typ varchar(50)
            set @plcy_nbr='000005550003000';
            set @ben_typ='LTD';
            
            with test_temp1 as (
            select * from (
            select distinct CNCTSTUB_REF_NBR,--CLAIM_TYP_DESC,CVRG_PROD_CTGY_CD,
             case when g.ben_CASE_NBR is null then b.claim_case_nbr else g.ben_case_nbr end Claim_Number,
            --ben_per_typ_desc,BEN_PER_BEN_STAT,
            -- initial decision date  ; test by checking from fineos.
            CASE WHEN [PROC_STEP_NM]  in ('Approved', 'Closed','Denied') THEN MIN(PROC_STEP_LAST_CHG_DT) end initial_decision_date
            ,MAX(BEN_PAY_OUT_EVNT_PER_END_DT) Date_Claim_Paid_To,
            max(PAY_DTL_PAY_END_DT) Last_Paid_Date,
            max(BEN_PER_TO_DT) Date_Claim_Authorized_To,
             CASE WHEN [PROC_STEP_NM]  in ('Closed','Closed-Approved', 'Denied') then max(i1.PROC_STEP_VIST_CHG_DT)  else '' end Date_Claim_Closed,
             CASE WHEN [PROC_STEP_NM]  in ('Closed','Closed-Approved', 'Denied') then max(PROC_STEP_VIST_CHG_RSN_DESC)  else '' end Closed_Reason,
            
            CASE WHEN [PROC_STEP_NM]  not in ('Closed','Closed-Approved', 'Denied') then ''
            WHEN [PROC_STEP_NM]  in ('Closed','Closed-Approved', 'Denied')
            AND ben_per_typ_desc in ('Fully Certified','Partial Return to Work - ER','Partial Return to Work - other ER') and BEN_PER_BEN_STAT ='Approved'
            THEN max(BEN_TERM_DT) --ben_claim_paid_to_dt
            ELSE '' END BENEFIT_TERMINATION_DATE,
            
            case when [PAY_LINE_TYP] ='Auto Gross Entitlement' then sum([PAY_LINE_AMT_MONAMT]) end Gross_Amount
            
            
            from    [ENTPRS_CLAIMS_DM].dbo.[CASE] a --(VBI_CASE)
            join [ENTPRS_CLAIMS_DM].dbo.claim b on a.case_gen_id=b.case_gen_id --(VBI_claim in LZ)
            left join (select distinct claim_case_nbr,case when count(CLAIM_TYP_DESC) >0 then 1 else 0 end STD_NO
            from [ENTPRS_CLAIMS_DM].[dbo].claim
            where CLAIM_TYP_DESC=@ben_typ
            group by claim_case_nbr)  b1 on b.claim_case_nbr=b1.claim_case_nbr
            
            left join [ENTPRS_CLAIMS_DM].dbo.Document c1 on c1.CASE_CASE_NBR=a.case_case_nbr
            left join [ENTPRS_CLAIMS_DM].dbo.PERSON c on c.[PRSN_GEN_ID]=c1.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.[PRSN_GEN_ID]=c.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.[PROCESS_INSTANCE] h on h.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP] i on i.[PROC_STEP_GEN_ID]=h.[PROC_STEP_GEN_ID]
            left JOIN [ENTPRS_CLAIMS_DM].dbo.[PROCESS_STEP_VISIT] i1 ON i1.[PROC_STEP_GEN_ID] = h.[PROC_STEP_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PERIOD] g1 on g1.[BEN_GEN_ID]=g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_PARAMETER] g5 ON g5.[BEN_GEN_ID] = g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_LINK] g6 ON g6.[BEN_PAY_LINK_GEN_ID] = g5.[BEN_PAY_LINK_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BEN_PAY_OUT_EVNT] g66 on g.[BEN_GEN_ID]=g66.[BEN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] g4 on g4.[BEN_GEN_ID]=g.[BEN_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[POLICY_SNAPSHOT] g2 on g2.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
            left join [ENTPRS_CLAIMS_DM].[dbo].[CONTRACT_STUB] g3 on g2.[C_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_CLS_ID]
                            AND g2.[I_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_INDX_ID]
                            LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[COVERAGE] g33 ON g33.[PLCYSNAP_GEN_IND] = g2.[PLCYSNAP_GEN_IND]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_MAIN]  j on j.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_INFO] k on k.[RQST_MAIN_GEN_ID]=j.[RQST_MAIN_GEN_ID]
            --left join [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] n on n.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
            --left JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = n.[OCPTN_GEN_ID]
            left join (select claim_gen_id,nc.OCPTN_GEN_ID,OCPTN_emp_loc_cd,OCPTN_emp_id from
            [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] nc
            JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = nc.[OCPTN_GEN_ID] ) n on n.claim_gen_id=b.claim_gen_id
            
                            left JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CONTRACT_ROLE_BRDG] g34 ON g3.[CNCTSTUB_GEN_ID] = g34.[CNCTSTUB_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[ORGANIZATION] n1 on g34.[ORG_GEN_ID] = n1.[ORG_GEN_ID] --j.rqst_main_plcy_nbr=n1.[ORG_REF_NBR]
            LEFT JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_DIM] o ON o.[ADMIN_SYS_PARTY_KEY_TXT] = n1.ORG_REF_NBR
                    left JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_PLAN_FACT] p ON p.PARTY_DIM_GEN_ID = o.PARTY_DIM_GEN_ID
                    left join [ENTPRS_CLAIMS_DM].[dbo].ben_plan_dim q on q.ben_plan_dim_gen_id=p.ben_plan_dim_gen_id
                    left join [ENTPRS_CLAIMS_DM].[dbo].party_address q1 on q1.[ORG_GEN_ID] = n1.[ORG_GEN_ID]
            
            
                    left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_LINE] l on l.[RQST_INFO_GEN_ID]=k.[RQST_INFO_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_DTL] m on m.[PAY_DTL_GEN_ID]=l.[PAY_DTL_GEN_ID]
            --where ben_per_typ_desc in ('Fully Certified','Partial Return to Work - ER','Partial Return to Work - other ER') and
            --BEN_PER_BEN_STAT ='Approved'
            
            and  c.pm_cur_ind ='Y' and c1.pm_cur_ind ='Y' and d.pm_cur_ind ='Y'
            and h.pm_cur_ind ='Y' and i.pm_cur_ind ='Y'  and i1.pm_cur_ind ='Y'
            and g1.pm_cur_ind ='Y' and g1.pm_cur_ind ='Y' and g5.pm_cur_ind ='Y' and g6.pm_cur_ind ='Y'
            and g4.pm_cur_ind ='Y' and g2.pm_cur_ind ='Y' and g3.pm_cur_ind ='Y' and k.pm_cur_ind ='Y'
            and j.pm_cur_ind ='Y' and n1.pm_cur_ind ='Y' and o.pm_cur_ind ='Y'
            and p.pm_cur_ind ='Y' and q.pm_cur_ind ='Y'  and g34.pm_cur_ind='Y' and q1.pm_cur_ind='Y'
            --and l.pm_cur_ind ='Y' and m1.pm_cur_ind ='Y' and m2.pm_cur_ind ='Y'
            where   CNCTSTUB_REF_NBR=@plcy_nbr --and g.ben_case_nbr='DI-848-STD-01'
            and (CASE
                                    WHEN g.[BEN_CASE_NBR] IS NOT NULL
                                            THEN CVRG_PROD_CTGY_CD
                                    ELSE CLAIM_TYP_DESC
                                    END) = @ben_typ
            and a.pm_cur_ind ='Y' and B.pm_cur_ind ='Y'
            
            group by b.claim_case_nbr,CNCTSTUB_REF_NBR,g.ben_case_nbr,[PROC_STEP_NM],ben_per_typ_desc,BEN_PER_BEN_STAT,CLAIM_TYP_DESC,CVRG_PROD_CTGY_CD,[PAY_LINE_TYP]
            
             )a where Gross_Amount is not null )
             -- ***********************
             , test_temp2 as (
             select distinct  CNCTSTUB_REF_NBR,--CLAIM_TYP_DESC,CVRG_PROD_CTGY_CD,
            case when g.ben_CASE_NBR is null then a.case_case_nbr else g.ben_case_nbr end Claim_Number,
            c.PRSN_LAST_NM Claimant_Last_Name,c.PRSN_FRST_NM Claimant_First_Name,'xxxxx'+substring(c.prsn_natl_insur_nbr,6,4) Claimant_Taxpayer_ID
            ,i.PROC_STEP_NM Claim_Status,a.case_eff_strt_dt Date_of_Disability ,g.BEN_STRT_DT Benefit_Start_Date,
            c.prsn_gndr_cd gender,c.prsn_birth_dt Date_of_Birth,
            --,a.CASE_CRTD_DT,b.CLAIM_CMPLT_DT,b.CLAIM_NOTIF_DT
            
            CASE
            WHEN g.BEN_CASE_NBR IS NULL     THEN b.Claim_notif_dt
            WHEN b.CLAIM_TYP_DESC = 'STD' THEN b.Claim_notif_dt
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=1 then b.Claim_notif_dt
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=0 then b.CLAIM_CMPLT_DT
            else b.CLAIM_CMPLT_DT
            END  Notification_Date,
            
            CASE
            WHEN g.BEN_CASE_NBR IS NULL     THEN a.CASE_CRTD_DT
            WHEN b.CLAIM_TYP_DESC = 'STD' THEN a.CASE_CRTD_DT
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=1 then a.CASE_CRTD_DT
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=0 then b.CLAIM_CMPLT_DT
            else b.CLAIM_CMPLT_DT
            END  Date_Claim_Opened ,
            ben_pay_link_intrnl_end_dt Benefit_End_Date,
            
            PAY_DTL_BUSN_NET_BEN_AMT_MONAMT Net_Benefit_Amount, -- wrong some calculations
            
            PAY_DTL_BUSN_NET_BEN_AMT_MONAMT Total_Benefits_Paid_To_Date,
            PLCYHLDR_PLAN_CLS_DESCR Class_Description,
            PRTYADDR_CLAIMNT_ST_CD Claimant_Residence_State,
            OCPTN_EMP_LOC_CD Claimant_Work_State,
            'why [PROC_INSTNC_OWN_USR_ID]' Claim_Examiner,
            ocptn_emp_id Employee_ID,
            
            
            CASE
                                    WHEN g.[BEN_CASE_NBR] IS NOT NULL
                                            THEN CVRG_PROD_CTGY_CD
                                    ELSE CLAIM_TYP_DESC
                                    END AS 'Claim Type'
            
            from    [ENTPRS_CLAIMS_DM].dbo.[CASE] a --(VBI_CASE)
            join [ENTPRS_CLAIMS_DM].dbo.claim b on a.case_gen_id=b.case_gen_id --(VBI_claim in LZ)
            left join (select distinct claim_case_nbr,case when count(CLAIM_TYP_DESC) >0 then 1 else 0 end STD_NO
            from [ENTPRS_CLAIMS_DM].[dbo].claim
            where CLAIM_TYP_DESC=@ben_typ
            group by claim_case_nbr)  b1 on b.claim_case_nbr=b1.claim_case_nbr
            
            left join [ENTPRS_CLAIMS_DM].dbo.Document c1 on c1.CASE_CASE_NBR=a.case_case_nbr
            left join [ENTPRS_CLAIMS_DM].dbo.PERSON c on c.[PRSN_GEN_ID]=c1.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.[PRSN_GEN_ID]=c.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.[PROCESS_INSTANCE] h on h.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP] i on i.[PROC_STEP_GEN_ID]=h.[PROC_STEP_GEN_ID]
            left JOIN [ENTPRS_CLAIMS_DM].dbo.[PROCESS_STEP_VISIT] i1 ON i1.[PROC_STEP_GEN_ID] = h.[PROC_STEP_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PERIOD] g1 on g1.[BEN_GEN_ID]=g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_PARAMETER] g5 ON g5.[BEN_GEN_ID] = g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_LINK] g6 ON g6.[BEN_PAY_LINK_GEN_ID] = g5.[BEN_PAY_LINK_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] g4 on g4.[BEN_GEN_ID]=g.[BEN_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[POLICY_SNAPSHOT] g2 on g2.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
            left join [ENTPRS_CLAIMS_DM].[dbo].[CONTRACT_STUB] g3 on g2.[C_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_CLS_ID]
                            AND g2.[I_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_INDX_ID]
                            LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[COVERAGE] g33 ON g33.[PLCYSNAP_GEN_IND] = g2.[PLCYSNAP_GEN_IND]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_MAIN]  j on j.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_INFO] k on k.[RQST_MAIN_GEN_ID]=j.[RQST_MAIN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] n on n.CLAIM_GEN_ID=b.CLAIM_GEN_ID
            left JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = n.[OCPTN_GEN_ID]
            --left join (select CLAIM_GEN_ID,nc.OCPTN_GEN_ID,OCPTN_emp_loc_cd,OCPTN_emp_id from
            --[ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] nc
            --JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = nc.[OCPTN_GEN_ID] ) n on n.CLAIM_GEN_ID=b.CLAIM_GEN_ID
            
                            left JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CONTRACT_ROLE_BRDG] g34 ON g3.[CNCTSTUB_GEN_ID] = g34.[CNCTSTUB_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[ORGANIZATION] n1 on g34.[ORG_GEN_ID] = n1.[ORG_GEN_ID] --j.rqst_main_plcy_nbr=n1.[ORG_REF_NBR]
            LEFT JOIN [ENTPRS_CLAIMS_DM].qa.[PARTY_DIM] o ON o.[ADMIN_SYS_PARTY_KEY_TXT] = n1.ORG_REF_NBR
                    left JOIN [ENTPRS_CLAIMS_DM].qa.[PARTY_PLAN_FACT] p ON p.PARTY_DIM_GEN_ID = o.PARTY_DIM_GEN_ID
                    left join [ENTPRS_CLAIMS_DM].qa.ben_plan_dim q on q.ben_plan_dim_gen_id=p.ben_plan_dim_gen_id
                    left join [ENTPRS_CLAIMS_DM].[dbo].party_address q1 on q1.[ORG_GEN_ID] = n1.[ORG_GEN_ID]
            
            
                    left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_LINE] l on l.[RQST_INFO_GEN_ID]=k.[RQST_INFO_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_DTL] m on m.[PAY_DTL_GEN_ID]=l.[PAY_DTL_GEN_ID]
            
            and  c.pm_cur_ind ='Y' and c1.pm_cur_ind ='Y' and d.pm_cur_ind ='Y'
            and h.pm_cur_ind ='Y' and i.pm_cur_ind ='Y'  and i1.pm_cur_ind ='Y'
            and g1.pm_cur_ind ='Y' and g1.pm_cur_ind ='Y' and g5.pm_cur_ind ='Y' and g6.pm_cur_ind ='Y'
            and g4.pm_cur_ind ='Y' and g2.pm_cur_ind ='Y' and g3.pm_cur_ind ='Y' and k.pm_cur_ind ='Y'
            and j.pm_cur_ind ='Y' and n.pm_cur_ind ='Y' and n1.pm_cur_ind ='Y' and o.pm_cur_ind ='Y'
            and p.pm_cur_ind ='Y' and q.pm_cur_ind ='Y'  and g34.pm_cur_ind='Y' and q1.pm_cur_ind='Y'
            --and l.pm_cur_ind ='Y' and m1.pm_cur_ind ='Y' and m2.pm_cur_ind ='Y'
            where   CNCTSTUB_REF_NBR=@plcy_nbr --and g.ben_case_nbr='DI-848-STD-01'
            --and PAY_DTL_PAY_STRT_DT>='2020-06-10 00:00:00.000'  AND PAY_DTL_PAY_END_DT<='2020-06-25'
            and a.pm_cur_ind ='Y' and B.pm_cur_ind ='Y'
            
            and (CASE
                                    WHEN g.[BEN_CASE_NBR] IS NOT NULL
                                            THEN CVRG_PROD_CTGY_CD
                                    ELSE CLAIM_TYP_DESC
                                    END) =@ben_typ
            
            --order by CNCTSTUB_REF_NBR Desc,PLCYHLDR_PLAN_CLS_DESCR desc
            )
            --**********************
            
            --select a.* ,b.*
            select distinct a.Claim_Number , b.Claimant_Last_Name,b.Claimant_First_Name,b.Claimant_Taxpayer_ID,b.Claim_Status,b.Date_of_Disability,b.Benefit_Start_Date,
            --b.gender,b.Date_of_Birth,
            b.Notification_Date,b.Date_Claim_Opened,a.Date_Claim_Paid_To,a.Date_Claim_Authorized_To,
            a.Date_Claim_Closed,a.Closed_Reason,a.BENEFIT_TERMINATION_DATE,b.Benefit_End_Date
            
            from test_temp1 a
            join test_temp2 b on a.CNCTSTUB_REF_NBR=b.CNCTSTUB_REF_NBR and a.Claim_Number=b.Claim_Number

         """
        
    dbdata = Parser.parseSQL(sql1,"SQLD10746,4242","ENTPRS_CLAIMS_ODS")
    newdbdata = pd.DataFrame(columns = ['combined'])
    
    dbdata = dbdata.replace({'NaT': ''})
    dbdata = dbdata.fillna('')
    
    dbdata = dbdata.drop('Benefit_Start_Date',1)
    dbdata = dbdata.drop('Notification_Date',1)
    dbdata = dbdata.drop('Date_Claim_Paid_To',1)
    dbdata = dbdata.drop('Date_Claim_Closed',1)
    dbdata = dbdata.drop('BENEFIT_TERMINATION_DATE',1)
    dbdata = dbdata.drop('Benefit_End_Date',1)
      
    
    #---------- some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char
    #----------- some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
    
    i = 0
   
    
    for index,rows in dbdata.iterrows():  # Looping through Rows
        i = i + 1
        xx = 0
        for (columnName, columnData) in rows.items():  # Looping through Cols
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):  #checking if it's a number..if yes, convert that to float and convert back to string'
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                """
                if xx== 6 or xx==7 or xx==9 or xx==12 or xx==13 or xx==15 or xx==17:
                    StringTmp = str(String1)[:10]
                if columnName == 'Waiver End Date' :   # date formats  - some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char  #some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
                    StringTmp = "" 
                #elif columnName == 'Waiver Begin Date':
                #    StringTmp =  str(rows['Waiver Begin Date'])[:10]           
                else:
                    StringTmp = columnData
                """ 
                StringTmp = str(columnData)   
                if(str(StringTmp).lstrip("-").isdigit()):  # - is removed to remove negative amounts so that it can be considered as digit
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp)
                
                if StringTmp == 'NONE':
                   StringTmp = '' 
                        
                String1 = String1 + "," +   StringTmp  
        
        newdbdata.loc[i] = String1
        
    #print(newdbdata.head(5))   
    #newdbdata.to_csv("c:\\Srikanth\\CSVDB.csv") 
      

#-------------------- Read from Excel 
#-------------------- fill blank Group main with previous cell values - to fill out he merged cells
#--------------------- REmove first row of every group main the report, which has blank values
#--------------------- remove blank columns, SOrt & re index   & iterate to add all columns
  
    rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\Claims_ReportValidation\\Disability Claim Status Report - All Claims.xls', sheet_name = "ALL LTD CLAIMS", dtype=str)  #,encoding='cp437'  ,errors = 'ignore'     
    rptdata = rptdata.replace({'NaT': ''})
    rptdata = rptdata.fillna('')
    
    rptdata = rptdata.drop('Benefit Start Date',1)
    rptdata = rptdata.drop('Notification Date',1)
    rptdata = rptdata.drop('Date Claim Paid To',1)
    rptdata = rptdata.drop('Date Claim Closed',1)
    rptdata = rptdata.drop('Benefit Termination Date',1)
    rptdata = rptdata.drop('Benefit End Date',1)
    
    rptdata['Claimant \nTaxpayer ID'] = rptdata['Claimant \nTaxpayer ID'].str.replace("-","").str.replace(" ","")
    
    #rptdata = rptdata.replace({np.NaN: ''}, inplace=True)
    
    rptdata = rptdata.reset_index(drop=True)
    rptdata1 = rptdata
    newrptdata1 = pd.DataFrame(columns = ['combined'])
    i = 0
    for index,rows in rptdata1.iterrows():  #Looping through Rows
        i = i + 1
        
        xx = 0
        for (columnName, columnData) in rows.items():    #Looping through Columns of a row & appending them 
            xx = xx + 1
            if xx == 1:
                String1 = str(columnData)
                if(String1.isdigit()):
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:  
                StringTmp = str(columnData)
                if xx== 6 or xx==7 or xx==9 or xx==12 or xx==13 or xx==15 or xx==17:
                    StringTmp = str(StringTmp)[:10]
                #if(xx == 2 or xx == 3 or xx ==7):
                #    #print(StringTmp)
                #    if(xx==7 and StringTmp != ''):
                #        StringTmp = datetime.datetime.strptime(StringTmp.strip("'"),'%m/%d/%Y').strftime('%Y-%m-%d')
                #    StringTmp = StringTmp[:10]
                
                
                    
                if(StringTmp.isdigit()):
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  #Looping through Columns of a row & appending them 
        
        newrptdata1.loc[i] = String1
        
    #print(newrptdata1.head(5))   
    #newrptdata1.to_csv("c:\\Srikanth\\CSV1.csv") 
        
    cols = [['combined','combined','String']]
    DFCompare.compare_fullReport(newdbdata,newrptdata1,cols)
    html_logger.info("End of Disability Claims Detail Report_All Claims  " + str(polnum) + " and Ben Type " + benType)
 

def Misc1099Reports():
    html_logger.info("Start of Validation of 1099 Report")
    sql1 = """with payment as (SELECT distinct plcy_snp.ben_case_nbr,RQST_INFO_PLCYHLDR_NBR,RQST_INFO_EMP_CONTR_PCT,RQST_INFO_GROS_BEN_AMT_MONAMT,PAY_LINE_AMT_MONAMT,PAY_LINE_TYP,PAY_DTL_PAY_STRT_DT
                        ,PAY_DTL_BUSN_NET_BEN_AMT_MONAMT,PAY_DTL_PAY_END_DT,RQST_MAIN_CLS_ID,RQST_MAIN_INDX_ID,rm.RQST_MAIN_ROLE_NM,RQST_MAIN_ORG_NM,RQST_MAIN_PLCY_NBR
                        ,rm.org_gen_id,CLAIM_DTL_ADDR_1,CLAIM_DTL_ADDR_2,CLAIM_DTL_CITY ,CLAIM_DTL_ST,CLAIM_DTL_PSTL_CD,PAY_LINE_TAX_INCM,ri.RQST_INFO_PAY_MTHD,CLAIM_DTL_SSN
                        ,RQST_MAIN_ORG_ZIP_CD,RQST_MAIN_ORG_ADDR1,RQST_MAIN_ORG_ST_CD,RQST_INFO_PAYEE_ADDR,RQST_INFO_PAYEE_ADDR1_TXT,RQST_INFO_PAYEE_ADDR2_TXT
                        ,RQST_INFO_PAYEE_CITY_NAME,RQST_INFO_PAYEE_FULL_NAME,RQST_INFO_PAYEE_SSN,RQST_INFO_PAYEE_ST_CD,RQST_INFO_PAYEE_ZIP_CD,RQST_INFO_PAY_TYP_CD
                        ,RQST_MAIN_ORG_CITY     ,plcy_snp.CLAIM_CASE_NBR,ISNULL(plcy_snp.CLAIM_GEN_ID, rm.CLAIM_GEN_ID) CLAIM_GEN_ID,plcy_snp.CASE_GEN_ID
                        ,plcy_snp.CVRG_PROD_CTGY_CD,PAY_DTL_DESCR,RQST_INFO_CLS_ID,RQST_INFO_INDX_ID,ri.RQST_INFO_CMBN_PAY_DESCR,ri.RQST_INFO_PAY_EVNT_TYPE_DESCR,prsn_cust_nbr,
                        ri.rqst_info_gen_id
       FROM [dbo].[RQST_INFO] ri
              LEFT OUTER JOIN [dbo].[PAY_LINE] pl ON pl.[RQST_INFO_GEN_ID] = ri.[RQST_INFO_GEN_ID]
       LEFT OUTER JOIN [dbo].[PAY_DTL] pd ON pd.PAY_DTL_GEN_ID = pl.PAY_DTL_GEN_ID
       LEFT OUTER JOIN [dbo].[CLAIM_DTL] cd ON cd.[RQST_INFO_GEN_ID] = ri.[RQST_INFO_GEN_ID]
              AND cd.[CLAIM_DTL_GEN_ID] = pd.[CLAIM_DTL_GEN_ID]
       LEFT OUTER JOIN [dbo].[RQST_MAIN] rm ON rm.[RQST_INFO_JOIN_INDX_ID] = ri.[RQST_INFO_INDX_ID]
                                           AND rm.[RQST_MAIN_GEN_ID] = ri.[RQST_MAIN_GEN_ID]

       FULL OUTER JOIN (SELECT PSnap.CLAIM_CASE_NBR
                ,c.CLAIM_GEN_ID
                ,c.CASE_GEN_ID
                ,CVRG_PROD_CTGY_CD,cc.prsn_cust_nbr,g.ben_case_nbr
        FROM [dbo].[POLICY_SNAPSHOT] PSnap
        LEFT OUTER MERGE JOIN [dbo].[CLAIM] C ON C.[CLAIM_CASE_NBR] = PSnap.[CLAIM_CASE_NBR]
        left join dbo.[case] c1 on c1.case_gen_id=c.case_gen_id
        left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.pcr_case_nbr=c1.case_case_nbr
left join [ENTPRS_CLAIMS_DM].dbo.PERSON cc on cc.[PRSN_GEN_ID]=d.[PRSN_GEN_ID]   --and cc.prsn_cust_nbr=356
left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = c.[CLAIM_GEN_ID]
        LEFT OUTER MERGE JOIN [dbo].[CONTRACT_STUB] CS ON PSnap.[CNCTSTUB_GEN_ID]=cs.[CNCTSTUB_GEN_ID]
        LEFT OUTER MERGE JOIN [dbo].[COVERAGE] Cov ON Cov.[PLCYSNAP_GEN_IND] = Psnap.[PLCYSNAP_GEN_IND]
                AND PSnap.pm_cur_ind = 'Y'
                AND C.pm_cur_ind = 'Y'
                AND CS.pm_cur_ind = 'Y'
                AND Cov.pm_cur_ind = 'Y') plcy_snp ON rm.CLAIM_GEN_ID = plcy_snp.CLAIM_GEN_ID and rm.ben_case_nbr=plcy_snp.ben_case_nbr
                where  CVRG_PROD_CTGY_CD = substring(plcy_snp.BEN_CASE_NBR, DBO.INSTR(plcy_snp.BEN_CASE_NBR, '-', 1, 2) + 1, 3))

, all_fields as (select  distinct TRANS_STAT_DT,TAX_AGNT_CD,
case when [PAY_DTL_DESCR]='Benefit Payment' and PAY_LINE_TYP='Survivor Benefit' then 'SSN'
when [PAY_DTL_DESCR]='Benefit Payment' and PAY_LINE_TYP='Settlement' then 'TaxID'
else '' end EIN_SSN_Type ,b.claim_case_nbr,RQST_INFO_PAYEE_FULL_NAME,c.PRSN_FRST_NM,c.PRSN_LAST_NM,
CLAIM_DTL_ADDR_1,CLAIM_DTL_ADDR_2,CLAIM_DTL_CITY,CLAIM_DTL_ST,CLAIM_DTL_PSTL_CD,
 PAY_LINE_TYP,PAY_LINE_AMT_MONAMT,c.PRSN_CUST_NBR,RQST_MAIN_ROLE_NM,PAY_DTL_PAY_END_DT,PAY_DTL_PAY_STRT_DT,g.ben_case_nbr

from    [ENTPRS_CLAIMS_DM].dbo.[CASE] a --(VBI_CASE)
join [ENTPRS_CLAIMS_DM].dbo.claim b on a.CASE_PRNT_CASE_NBR=b.CLAIM_CASE_NBR --(VBI_claim in LZ)
left join [ENTPRS_CLAIMS_DM].dbo.[PROCESS_INSTANCE] h on h.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP] i on i.[PROC_STEP_GEN_ID]=h.[PROC_STEP_GEN_ID]
LEFT JOIN [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP_VISIT] i1 ON i1.[PROC_STEP_GEN_ID]=h.[PROC_STEP_GEN_ID]
left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = b.[CLAIM_GEN_ID]
left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.pcr_case_nbr=a.case_case_nbr
left join [ENTPRS_CLAIMS_DM].dbo.PERSON c on c.[PRSN_GEN_ID]=d.[PRSN_GEN_ID]
--left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] g4 on g4.[BEN_GEN_ID]=g.[BEN_GEN_ID]

left join        payment ppp on ppp.claim_gen_id=b.claim_gen_id and ppp.prsn_cust_nbr=c.prsn_cust_nbr --where b.claim_case_nbr='DI-513'
--and ppp.RQST_INFO_PAYEE_SSN=c.PRSN_NATL_INSUR_NBR
left join (SELECT DISTINCT
                           PAY_LINE_PRCHS_DTL_RQST.PLCY_NBR
                          ,PAY_LINE_PRCHS_DTL_RQST.CLAIM_CASE_NBR
                          ,PAY_LINE_PRCHS_DTL_RQST.BEN_CASE_NBR
                          ,PAY_LINE_PRCHS_DTL_RQST.BEN_PLAN_NBR
                          ,PAY_LINE_PRCHS_DTL_RQST.BEN_PLAN_TYP_CD
                          ,PAY_LINE_PRCHS_DTL_RQST.PLCYHLDR_BEN_PLAN_EFF_DT
                          ,PAY_LINE_PRCHS_DTL_RQST.EOB_MAIL_RCPNT_TYP_CD
                          ,PAY_LINE_PRCHS_DTL_RQST.RAIL_RD_IND
                          ,PAY_LINE_PRCHS_DTL_RQST.TAX_AGNT_CD
                          ,PAY_LINE_PRCHS_DTL_RQST.PLCYHLDR_CNTCT_NAME
                FROM [dbo].[PAY_LINE_PRCHS_DTL_RQST]
       WHERE pm_cur_ind = 'Y') pl on pl.PLCY_NBR=RQST_INFO_PLCYHLDR_NBR and pl.CLAIM_CASE_NBR=b.CLAIM_CASE_NBR and pl.BEN_CASE_NBR=g.BEN_CASE_NBR
                        LEFT OUTER JOIN [ENTPRS_CLAIMS_ODS].[FAI].[PAY_RQST_INFO] pri ON pri.[CLS_ID] = ppp.RQST_INFO_CLS_ID
                                                                                                                                 AND pri.[INDX_ID]= ppp.RQST_INFO_INDX_ID
        LEFT OUTER JOIN [ENTPRS_CLAIMS_ODS].[FAI].[PAY_STAT_WRITEBACK] psw ON psw.[CLS_ID] = ppp.RQST_INFO_CLS_ID
                                                                                                                         AND psw.[INDX_ID]= ppp.RQST_INFO_INDX_ID

where   a.pm_cur_ind ='Y' and B.pm_cur_ind ='Y'  -- and TRANS_STAT_DT is not null

and PAY_LINE_TYP in ('FIT Amount','Survivor Benefit','Settlement')
and prsn_frst_nm is not null --and prsn_frst_nm like 'Charles%'
and year(TRANS_STAT_DT)=2020

and CVRG_PROD_CTGY_CD = ISNULL(substring(ppp.BEN_CASE_NBR, DBO.INSTR(ppp.BEN_CASE_NBR, '-', 1, 2) + 1, 3), CVRG_PROD_CTGY_CD)
and (PAY_LINE_AMT_MONAMT IS NOT NULL
and RQST_INFO_GROS_BEN_AMT_MONAMT is not null
and PAY_DTL_PAY_STRT_DT is not null
        and PAY_LINE_TYP IS  NOT NULL)
        --and b.claim_case_nbr='DI-952'
-- and g.ben_Case_nbr='DI-466-LTD-01'
)

select distinct  a.*,b.survivor_benefit,b.fit_amount,b.Settlement from
(select TAX_AGNT_CD Tax_Agent_code,claim_case_nbr, EIN_SSN_Type,RQST_INFO_PAYEE_FULL_NAME,CLAIM_DTL_ADDR_1,CLAIM_DTL_ADDR_2,CLAIM_DTL_CITY,CLAIM_DTL_ST,CLAIM_DTL_PSTL_CD
from all_fields
where RQST_MAIN_ROLE_NM = 'Claimant'
--and RQST_INFO_PAYEE_FULL_NAME<>'FIT Amount Payee'
) A

join (select claim_case_nbr, RQST_INFO_PAYEE_FULL_NAME,
case when PAY_LINE_TYP='Survivor Benefit' then sum(PAY_LINE_AMT_MONAMT)  end survivor_benefit,
case when PAY_LINE_TYP='FIT Amount' then sum(PAY_LINE_AMT_MONAMT)  end FIT_Amount,
case when PAY_LINE_TYP='Settlement' then sum(PAY_LINE_AMT_MONAMT)  end Settlement
from all_fields
--where RQST_MAIN_ROLE_NM = 'Claimant'
--where RQST_INFO_PAYEE_FULL_NAME<>'FIT Amount Payee'
group by claim_case_nbr,RQST_INFO_PAYEE_FULL_NAME,PAY_LINE_TYP,ben_case_nbr ) b  on a.claim_case_nbr=b.claim_case_nbr and a.RQST_INFO_PAYEE_FULL_NAME=b.RQST_INFO_PAYEE_FULL_NAME
order by TAX_AGENT_CoDe,RQST_INFO_PAYEE_FULL_NAME
"""
        
    dbdata = Parser.parseSQL(sql1,"SQLD10747,4242","ENTPRS_CLAIMS_DM")
    dbdata = dbdata.drop('claim_case_nbr', axis=1)
    dbdata = dbdata.fillna(value = np.nan)
    dbdata = dbdata.replace('',np.nan)
    dbdata = dbdata.replace('TaxID','Tax ID')
    
    
    dbdata = dbdata.sort_values(by = ['RQST_INFO_PAYEE_FULL_NAME'])
    print(dbdata.head(5))
    
    newdbdata = pd.DataFrame(columns = ['combined'])
    
    #---------- some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char
    #----------- some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
    """    
    i = 0
   
    
    for index,rows in dbdata.iterrows():  # Looping through Rows
        i = i + 1
        xx = 0
        for (columnName, columnData) in rows.items():  # Looping through Cols
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):  #checking if it's a number..if yes, convert that to float and convert back to string'
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                if columnName == 'Cycle Date' :   # date formats  - some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char  #some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
                    if len(str(rows['Cycle Date'])) == 26:
                        cycleDate1 = str(rows['Cycle Date'])[:-3]
                    elif len(str(rows['Cycle Date'])) == 19:
                        cycleDate1 = str(rows['Cycle Date']) + ".000"   
                    else:
                        cycleDate1 = str(rows['Cycle Date'])
                    StringTmp = cycleDate1       
                else:
                    StringTmp = columnData
                    
                if(str(StringTmp).lstrip("-").isdigit()):  # - is removed to remove negative amounts so that it can be considered as digit
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  
        
        newdbdata.loc[i] = String1

    """  
    rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\Claims_ReportValidation\\FINEOS 1099 Misc Report.xls', sheet_name = "FINEOS 1099 Misc Report", dtype=str)  #,encoding='cp437'  ,errors = 'ignore'     
    
    #rptdata['Box 4 FIT'] = rptdata['Box 4 FIT'].str.replace("(","").str.replace(")","")
    
    
    #rptdata = rptdata.reset_index(drop=True)
    rptdata = rptdata.sort_values(by=['Name'])
    rptdata = rptdata.fillna(value = np.nan)
    
    print(rptdata.head(5))
    rptdata1 = rptdata
    newrptdata1 = pd.DataFrame(columns = ['combined'])
    
    
    """
    i = 0
    for index,rows in rptdata1.iterrows():  #Looping through Rows
        i = i + 1
        
        xx = 0
        for (columnName, columnData) in rows.items():    #Looping through Columns of a row & appending them 
            xx = xx + 1
            if xx == 1:
                String1 = str(columnData)
                if(String1.isdigit()):
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:  
                StringTmp = str(columnData)
                #if xx== 6 or xx==7 or xx==9 or xx==12 or xx==13 or xx==15 or xx==17:
                #    StringTmp = str(StringTmp)[:10]
                #if(xx == 2 or xx == 3 or xx ==7):
                #    #print(StringTmp)
                #    if(xx==7 and StringTmp != ''):
                #        StringTmp = datetime.datetime.strptime(StringTmp.strip("'"),'%m/%d/%Y').strftime('%Y-%m-%d')
                #    StringTmp = StringTmp[:10]
                
                
                    
                if(StringTmp.isdigit()):
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  #Looping through Columns of a row & appending them 
        
        newrptdata1.loc[i] = String1
        
    #print(newrptdata1.head(5))   
    #newrptdata1.to_csv("c:\\Srikanth\\CSV1.csv") 
    """
        
    cols = [['Tax Agent Code','Tax_Agent_code','String'],['EIN/SSN Type','EIN_SSN_Type','String'],['Name','RQST_INFO_PAYEE_FULL_NAME','String'],['Address Line One','CLAIM_DTL_ADDR_1','String'],['Address Line Two','CLAIM_DTL_ADDR_2','String'],
            ['City','CLAIM_DTL_CITY','String'],['State','CLAIM_DTL_ST','String'],['Zip Code','CLAIM_DTL_PSTL_CD','float'],['Box 3','survivor_benefit','float'],['Box 4 FIT','fit_amount','float'],['Box 10','Settlement','float']]
    DFCompare.compare_fullReport(rptdata,dbdata,cols)
    #html_logger.info("End of Disability Claims Detail Report_All Claims  " + str(polnum) + " and Ben Type " + benType)
        
#---------------------------------------------------------


def DisabilityClaimsDetailReport_AllClaims(polnum, benType):
    html_logger.info("Start of Disability Claims Detail Report_All Claims " + str(polnum) + " and Ben Type " + benType)
    #benType = 'STD'
    sql1 = """
            declare @plcy_nbr varchar(50)
            set @plcy_nbr='000005550003000';
            
            with test_temp1 as (
            select * from (
            select distinct CNCTSTUB_REF_NBR,--CLAIM_TYP_DESC,CVRG_PROD_CTGY_CD,
             case when g.ben_CASE_NBR is null then b.claim_case_nbr else g.ben_case_nbr end Claim_Number,
            --ben_per_typ_desc,BEN_PER_BEN_STAT,
            -- initial decision date  ; test by checking from fineos.
            CASE WHEN [PROC_STEP_NM]  in ('Approved', 'Closed','Denied') THEN MIN(PROC_STEP_LAST_CHG_DT) end initial_decision_date
            ,MAX(BEN_PAY_OUT_EVNT_PER_END_DT) Date_Claim_Paid_To,
            max(PAY_DTL_PAY_END_DT) Last_Paid_Date,
            max(BEN_PER_TO_DT) Date_Claim_Authorized_To,
             CASE WHEN [PROC_STEP_NM]  in ('Closed','Closed-Approved', 'Denied') then max(i1.PROC_STEP_VIST_CHG_DT)  else '' end Date_Claim_Closed,
             CASE WHEN [PROC_STEP_NM]  in ('Closed','Closed-Approved', 'Denied') then max(PROC_STEP_VIST_CHG_RSN_DESC)  else '' end Closed_Reason,
            
            CASE WHEN [PROC_STEP_NM]  not in ('Closed','Closed-Approved', 'Denied') then ''
            WHEN [PROC_STEP_NM]  in ('Closed','Closed-Approved', 'Denied') 
            AND ben_per_typ_desc in ('Fully Certified','Partial Return to Work - ER','Partial Return to Work - other ER') and BEN_PER_BEN_STAT ='Approved' 
            THEN max(BEN_TERM_DT) --ben_claim_paid_to_dt
            ELSE '' END BENEFIT_TERMINATION_DATE,
            
            case when [PAY_LINE_TYP] ='Auto Gross Entitlement' then sum([PAY_LINE_AMT_MONAMT]) end Gross_Amount
            
            
            from    [ENTPRS_CLAIMS_DM].dbo.[CASE] a --(VBI_CASE)
            join [ENTPRS_CLAIMS_DM].dbo.claim b on a.case_gen_id=b.case_gen_id --(VBI_claim in LZ)
            left join (select distinct claim_case_nbr,case when count(CLAIM_TYP_DESC) >0 then 1 else 0 end STD_NO 
            from [ENTPRS_CLAIMS_DM].[dbo].claim 
            where CLAIM_TYP_DESC='STD' 
            group by claim_case_nbr)  b1 on b.claim_case_nbr=b1.claim_case_nbr
            
            left join [ENTPRS_CLAIMS_DM].dbo.Document c1 on c1.CASE_CASE_NBR=a.case_case_nbr
            left join [ENTPRS_CLAIMS_DM].dbo.PERSON c on c.[PRSN_GEN_ID]=c1.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.[PRSN_GEN_ID]=c.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.[PROCESS_INSTANCE] h on h.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP] i on i.[PROC_STEP_GEN_ID]=h.[PROC_STEP_GEN_ID]
            left JOIN [ENTPRS_CLAIMS_DM].dbo.[PROCESS_STEP_VISIT] i1 ON i1.[PROC_STEP_GEN_ID] = h.[PROC_STEP_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PERIOD] g1 on g1.[BEN_GEN_ID]=g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_PARAMETER] g5 ON g5.[BEN_GEN_ID] = g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_LINK] g6 ON g6.[BEN_PAY_LINK_GEN_ID] = g5.[BEN_PAY_LINK_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BEN_PAY_OUT_EVNT] g66 on g.[BEN_GEN_ID]=g66.[BEN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] g4 on g4.[BEN_GEN_ID]=g.[BEN_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[POLICY_SNAPSHOT] g2 on g2.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
            left join [ENTPRS_CLAIMS_DM].[dbo].[CONTRACT_STUB] g3 on g2.[C_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_CLS_ID]
                            AND g2.[I_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_INDX_ID]
                            LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[COVERAGE] g33 ON g33.[PLCYSNAP_GEN_IND] = g2.[PLCYSNAP_GEN_IND]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_MAIN]  j on j.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_INFO] k on k.[RQST_MAIN_GEN_ID]=j.[RQST_MAIN_GEN_ID]
            --left join [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] n on n.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
            --left JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = n.[OCPTN_GEN_ID] 
            left join (select claim_gen_id,nc.OCPTN_GEN_ID,OCPTN_emp_loc_cd,OCPTN_emp_id from
            [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] nc
            JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = nc.[OCPTN_GEN_ID] ) n on n.claim_gen_id=b.claim_gen_id
            
                            left JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CONTRACT_ROLE_BRDG] g34 ON g3.[CNCTSTUB_GEN_ID] = g34.[CNCTSTUB_GEN_ID]
                            
            left join [ENTPRS_CLAIMS_DM].[dbo].[ORGANIZATION] n1 on g34.[ORG_GEN_ID] = n1.[ORG_GEN_ID] --j.rqst_main_plcy_nbr=n1.[ORG_REF_NBR] 
            LEFT JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_DIM] o ON o.[ADMIN_SYS_PARTY_KEY_TXT] = n1.ORG_REF_NBR
                    left JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_PLAN_FACT] p ON p.PARTY_DIM_GEN_ID = o.PARTY_DIM_GEN_ID
                    left join [ENTPRS_CLAIMS_DM].[dbo].ben_plan_dim q on q.ben_plan_dim_gen_id=p.ben_plan_dim_gen_id
                    left join [ENTPRS_CLAIMS_DM].[dbo].party_address q1 on q1.[ORG_GEN_ID] = n1.[ORG_GEN_ID]
            
            
                    left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_LINE] l on l.[RQST_INFO_GEN_ID]=k.[RQST_INFO_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_DTL] m on m.[PAY_DTL_GEN_ID]=l.[PAY_DTL_GEN_ID]
            --where ben_per_typ_desc in ('Fully Certified','Partial Return to Work - ER','Partial Return to Work - other ER') and
            --BEN_PER_BEN_STAT ='Approved'
            
            and  c.pm_cur_ind ='Y' and c1.pm_cur_ind ='Y' and d.pm_cur_ind ='Y'
            and h.pm_cur_ind ='Y' and i.pm_cur_ind ='Y'  and i1.pm_cur_ind ='Y'
            and g1.pm_cur_ind ='Y' and g1.pm_cur_ind ='Y' and g5.pm_cur_ind ='Y' and g6.pm_cur_ind ='Y'
            and g4.pm_cur_ind ='Y' and g2.pm_cur_ind ='Y' and g3.pm_cur_ind ='Y' and k.pm_cur_ind ='Y'
            and j.pm_cur_ind ='Y' and n1.pm_cur_ind ='Y' and o.pm_cur_ind ='Y' 
            and p.pm_cur_ind ='Y' and q.pm_cur_ind ='Y'  and g34.pm_cur_ind='Y' and q1.pm_cur_ind='Y'
            --and l.pm_cur_ind ='Y' and m1.pm_cur_ind ='Y' and m2.pm_cur_ind ='Y' 
            where   CNCTSTUB_REF_NBR=@plcy_nbr --and g.ben_case_nbr='DI-848-STD-01'
            and (CASE 
                                    WHEN g.[BEN_CASE_NBR] IS NOT NULL
                                            THEN CVRG_PROD_CTGY_CD
                                    ELSE CLAIM_TYP_DESC
                                    END) ='STD'
            and a.pm_cur_ind ='Y' and B.pm_cur_ind ='Y'
            
            group by b.claim_case_nbr,CNCTSTUB_REF_NBR,g.ben_case_nbr,[PROC_STEP_NM],ben_per_typ_desc,BEN_PER_BEN_STAT,CLAIM_TYP_DESC,CVRG_PROD_CTGY_CD,[PAY_LINE_TYP]
            
             )a where Gross_Amount is not null )
             -- ***********************
             , test_temp2 as (
             select distinct  CNCTSTUB_REF_NBR,--CLAIM_TYP_DESC,CVRG_PROD_CTGY_CD,
            case when g.ben_CASE_NBR is null then a.case_case_nbr else g.ben_case_nbr end Claim_Number,
            c.PRSN_LAST_NM Claimant_Last_Name,c.PRSN_FRST_NM Claimant_First_Name,'xxxxx'+substring(c.prsn_natl_insur_nbr,6,4) Claimant_Taxpayer_ID 
            ,i.PROC_STEP_NM Claim_Status,a.case_eff_strt_dt Date_of_Disability ,g.BEN_STRT_DT Benefit_Start_Date,
            c.prsn_gndr_cd gender,c.prsn_birth_dt Date_of_Birth,
            --,a.CASE_CRTD_DT,b.CLAIM_CMPLT_DT,b.CLAIM_NOTIF_DT
            
            CASE 
            WHEN g.BEN_CASE_NBR IS NULL     THEN b.Claim_notif_dt
            WHEN b.CLAIM_TYP_DESC = 'STD' THEN b.Claim_notif_dt
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=1 then b.Claim_notif_dt
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=0 then b.CLAIM_CMPLT_DT
            else b.CLAIM_CMPLT_DT
            END  Notification_Date,
            
            CASE 
            WHEN g.BEN_CASE_NBR IS NULL     THEN a.CASE_CRTD_DT
            WHEN b.CLAIM_TYP_DESC = 'STD' THEN a.CASE_CRTD_DT
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=1 then a.CASE_CRTD_DT
            WHEN b.CLAIM_TYP_DESC = 'LTD' and b1.STD_NO=0 then b.CLAIM_CMPLT_DT
            else b.CLAIM_CMPLT_DT
            END  Date_Claim_Opened ,
            ben_pay_link_intrnl_end_dt Benefit_End_Date,
            
            PAY_DTL_BUSN_NET_BEN_AMT_MONAMT Net_Benefit_Amount, -- wrong some calculations
            
            PAY_DTL_BUSN_NET_BEN_AMT_MONAMT Total_Benefits_Paid_To_Date,
            PLCYHLDR_PLAN_CLS_DESCR Class_Description,
            PRTYADDR_CLAIMNT_ST_CD Claimant_Residence_State,
            OCPTN_EMP_LOC_CD Claimant_Work_State,
            'why [PROC_INSTNC_OWN_USR_ID]' Claim_Examiner,
            ocptn_emp_id Employee_ID,
            
            
            CASE 
                                    WHEN g.[BEN_CASE_NBR] IS NOT NULL
                                            THEN CVRG_PROD_CTGY_CD
                                    ELSE CLAIM_TYP_DESC
                                    END AS 'Claim Type'
            
            from    [ENTPRS_CLAIMS_DM].dbo.[CASE] a --(VBI_CASE)
            join [ENTPRS_CLAIMS_DM].dbo.claim b on a.case_gen_id=b.case_gen_id --(VBI_claim in LZ)
            left join (select distinct claim_case_nbr,case when count(CLAIM_TYP_DESC) >0 then 1 else 0 end STD_NO 
            from [ENTPRS_CLAIMS_DM].[dbo].claim 
            where CLAIM_TYP_DESC='STD' 
            group by claim_case_nbr)  b1 on b.claim_case_nbr=b1.claim_case_nbr
            
            left join [ENTPRS_CLAIMS_DM].dbo.Document c1 on c1.CASE_CASE_NBR=a.case_case_nbr
            left join [ENTPRS_CLAIMS_DM].dbo.PERSON c on c.[PRSN_GEN_ID]=c1.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CASE_ROLE_BRDG] d on d.[PRSN_GEN_ID]=c.[PRSN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.[PROCESS_INSTANCE] h on h.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PROCESS_STEP] i on i.[PROC_STEP_GEN_ID]=h.[PROC_STEP_GEN_ID]
            left JOIN [ENTPRS_CLAIMS_DM].dbo.[PROCESS_STEP_VISIT] i1 ON i1.[PROC_STEP_GEN_ID] = h.[PROC_STEP_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].dbo.benefit g on g.[CLAIM_GEN_ID] = b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PERIOD] g1 on g1.[BEN_GEN_ID]=g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_PARAMETER] g5 ON g5.[BEN_GEN_ID] = g.[BEN_GEN_ID]
                    LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[BENEFIT_PAYMENT_LINK] g6 ON g6.[BEN_PAY_LINK_GEN_ID] = g5.[BEN_PAY_LINK_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[BEN_ELIG_LMTS] g4 on g4.[BEN_GEN_ID]=g.[BEN_GEN_ID]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[POLICY_SNAPSHOT] g2 on g2.[CLAIM_CASE_NBR]=b.[CLAIM_CASE_NBR]
            left join [ENTPRS_CLAIMS_DM].[dbo].[CONTRACT_STUB] g3 on g2.[C_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_CLS_ID]
                            AND g2.[I_OCCTRSTB_CHANGECONTRAC] = g3.[CNCTSTUB_INDX_ID]
                            LEFT OUTER JOIN [ENTPRS_CLAIMS_DM].[dbo].[COVERAGE] g33 ON g33.[PLCYSNAP_GEN_IND] = g2.[PLCYSNAP_GEN_IND]
            
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_MAIN]  j on j.[CLAIM_GEN_ID]=b.[CLAIM_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[RQST_INFO] k on k.[RQST_MAIN_GEN_ID]=j.[RQST_MAIN_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] n on n.CLAIM_GEN_ID=b.CLAIM_GEN_ID
            left JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = n.[OCPTN_GEN_ID] 
            --left join (select CLAIM_GEN_ID,nc.OCPTN_GEN_ID,OCPTN_emp_loc_cd,OCPTN_emp_id from
            --[ENTPRS_CLAIMS_DM].[dbo].[OCCUPATION_CLAIM] nc
            --JOIN [ENTPRS_CLAIMS_DM].[dbo].[EARN] ne ON ne.[OCPTN_GEN_ID] = nc.[OCPTN_GEN_ID] ) n on n.CLAIM_GEN_ID=b.CLAIM_GEN_ID
            
                            left JOIN [ENTPRS_CLAIMS_DM].[dbo].[PARTY_CONTRACT_ROLE_BRDG] g34 ON g3.[CNCTSTUB_GEN_ID] = g34.[CNCTSTUB_GEN_ID]
                            
            left join [ENTPRS_CLAIMS_DM].[dbo].[ORGANIZATION] n1 on g34.[ORG_GEN_ID] = n1.[ORG_GEN_ID] --j.rqst_main_plcy_nbr=n1.[ORG_REF_NBR] 
            LEFT JOIN [ENTPRS_CLAIMS_DM].qa.[PARTY_DIM] o ON o.[ADMIN_SYS_PARTY_KEY_TXT] = n1.ORG_REF_NBR
                    left JOIN [ENTPRS_CLAIMS_DM].qa.[PARTY_PLAN_FACT] p ON p.PARTY_DIM_GEN_ID = o.PARTY_DIM_GEN_ID
                    left join [ENTPRS_CLAIMS_DM].qa.ben_plan_dim q on q.ben_plan_dim_gen_id=p.ben_plan_dim_gen_id
                    left join [ENTPRS_CLAIMS_DM].[dbo].party_address q1 on q1.[ORG_GEN_ID] = n1.[ORG_GEN_ID]
            
            
                    left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_LINE] l on l.[RQST_INFO_GEN_ID]=k.[RQST_INFO_GEN_ID]
            left join [ENTPRS_CLAIMS_DM].[dbo].[PAY_DTL] m on m.[PAY_DTL_GEN_ID]=l.[PAY_DTL_GEN_ID]
            
            and  c.pm_cur_ind ='Y' and c1.pm_cur_ind ='Y' and d.pm_cur_ind ='Y'
            and h.pm_cur_ind ='Y' and i.pm_cur_ind ='Y'  and i1.pm_cur_ind ='Y'
            and g1.pm_cur_ind ='Y' and g1.pm_cur_ind ='Y' and g5.pm_cur_ind ='Y' and g6.pm_cur_ind ='Y'
            and g4.pm_cur_ind ='Y' and g2.pm_cur_ind ='Y' and g3.pm_cur_ind ='Y' and k.pm_cur_ind ='Y'
            and j.pm_cur_ind ='Y' and n.pm_cur_ind ='Y' and n1.pm_cur_ind ='Y' and o.pm_cur_ind ='Y'
            and p.pm_cur_ind ='Y' and q.pm_cur_ind ='Y'  and g34.pm_cur_ind='Y' and q1.pm_cur_ind='Y'
            --and l.pm_cur_ind ='Y' and m1.pm_cur_ind ='Y' and m2.pm_cur_ind ='Y' 
            where   CNCTSTUB_REF_NBR=@plcy_nbr --and g.ben_case_nbr='DI-848-STD-01'
            --and PAY_DTL_PAY_STRT_DT>='2020-06-10 00:00:00.000'  AND PAY_DTL_PAY_END_DT<='2020-06-25'
            and a.pm_cur_ind ='Y' and B.pm_cur_ind ='Y' 
            
            and (CASE 
                                    WHEN g.[BEN_CASE_NBR] IS NOT NULL
                                            THEN CVRG_PROD_CTGY_CD
                                    ELSE CLAIM_TYP_DESC
                                    END) ='STD'
            
            --order by CNCTSTUB_REF_NBR Desc,PLCYHLDR_PLAN_CLS_DESCR desc
            )
            --**********************
            
            --select a.* ,b.*
            select a.Claim_Number , b.Claimant_Last_Name,b.Claimant_First_Name,b.Claimant_Taxpayer_ID,b.Claim_Status,b.Date_of_Disability,b.Benefit_Start_Date,
            b.gender,b.Date_of_Birth,b.Notification_Date,b.Date_Claim_Opened,a.initial_decision_date,a.Date_Claim_Paid_To,a.Last_Paid_Date,a.Date_Claim_Authorized_To,
            a.Date_Claim_Closed,a.Closed_Reason,a.BENEFIT_TERMINATION_DATE,b.Benefit_End_Date,a.Gross_Amount,b.Net_Benefit_Amount,b.Total_Benefits_Paid_To_Date,
            b.Class_Description,b.Claimant_Residence_State,b.Claimant_Work_State,b.Claim_Examiner,b.Employee_ID
            
            from test_temp1 a
            join test_temp2 b on a.CNCTSTUB_REF_NBR=b.CNCTSTUB_REF_NBR and a.Claim_Number=b.Claim_Number """
        
    dbdata = Parser.parseSQL(sql1,"SQLD10746,4242","ENTPRS_CLAIMS_ODS")
    newdbdata = pd.DataFrame(columns = ['combined'])
    
    dbdata = dbdata.replace({'NaT': ''})
    dbdata = dbdata.fillna('')
    
    dbdata = dbdata.drop('Date_Claim_Paid_To',1)
    dbdata = dbdata.drop('Date_Claim_Authorized_To',1)
    dbdata = dbdata.drop('Date_Claim_Closed',1)
    dbdata = dbdata.drop('BENEFIT_TERMINATION_DATE',1)
    dbdata = dbdata.drop('Gross_Amount',1)
    dbdata = dbdata.drop('Net_Benefit_Amount',1)
    dbdata = dbdata.drop('Total_Benefits_Paid_To_Date',1)
    dbdata = dbdata.drop('Claim_Examiner',1)
    dbdata = dbdata.drop('Employee_ID',1)
    
    
    
    #---------- some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char
    #----------- some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
    
    i = 0
   
    
    for index,rows in dbdata.iterrows():  # Looping through Rows
        i = i + 1
        xx = 0
        for (columnName, columnData) in rows.items():  # Looping through Cols
            xx = xx + 1
            if xx == 1:
                String1 = columnData
                if(String1.lstrip("-").isdigit()):  #checking if it's a number..if yes, convert that to float and convert back to string'
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:
                if xx== 6 or xx==7 or xx==9 or xx==12 or xx==13 or xx==15 or xx==17:
                    StringTmp = str(String1)[:10]
                if columnName == 'Waiver End Date' :   # date formats  - some dates have dd-mm-yyyy 12:00:00.123000 - (26) so have to remove last 3 char  #some have dd-mm-yyyy 12:00:00 (19) so have to add .000  else send as it is.
                    StringTmp = "" 
                #elif columnName == 'Waiver Begin Date':
                #    StringTmp =  str(rows['Waiver Begin Date'])[:10]           
                else:
                    StringTmp = columnData
                    
                if(str(StringTmp).lstrip("-").isdigit()):  # - is removed to remove negative amounts so that it can be considered as digit
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp)
                
                if StringTmp == 'NONE':
                   StringTmp = '' 
                        
                String1 = String1 + "," +   StringTmp  
        
        newdbdata.loc[i] = String1
        
    #print(newdbdata.head(5))   
    #newdbdata.to_csv("c:\\Srikanth\\CSVDB.csv") 
      

#-------------------- Read from Excel 
#-------------------- fill blank Group main with previous cell values - to fill out he merged cells
#--------------------- REmove first row of every group main the report, which has blank values
#--------------------- remove blank columns, SOrt & re index   & iterate to add all columns
  
    rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\Claims_ReportValidation\\Disability Claim Detail Report - All Claims_1.xls', sheet_name = "ALL STD", dtype=str)  #,encoding='cp437'  ,errors = 'ignore'     
    rptdata = rptdata.replace({'NaT': ''})
    rptdata = rptdata.fillna('')
    
    rptdata = rptdata.drop('Date Claim Paid To',1)
    rptdata = rptdata.drop('Date Claim Authorized To',1)
    rptdata = rptdata.drop('Date Claim Closed',1)
    rptdata = rptdata.drop('Benefit Termination Date',1)
    rptdata = rptdata.drop('Gross Benefit Amount',1)
    rptdata = rptdata.drop('Net Benefit Amount (before taxes) ',1)
    rptdata = rptdata.drop('Total Benefits Paid To Date',1)
    rptdata = rptdata.drop('Claim Examiner',1)
    
    #rptdata = rptdata.replace({np.NaN: ''}, inplace=True)
    
    rptdata = rptdata.reset_index(drop=True)
    rptdata1 = rptdata
    newrptdata1 = pd.DataFrame(columns = ['combined'])
    i = 0
    for index,rows in rptdata1.iterrows():  #Looping through Rows
        i = i + 1
        
        xx = 0
        for (columnName, columnData) in rows.items():    #Looping through Columns of a row & appending them 
            xx = xx + 1
            if xx == 1:
                String1 = str(columnData)
                if(String1.isdigit()):
                    String1 = str(float(String1))
                else:
                    String1 = str(String1)   
            else:  
                StringTmp = str(columnData)
                if xx== 6 or xx==7 or xx==9 or xx==12 or xx==13 or xx==15 or xx==17:
                    StringTmp = str(StringTmp)[:10]
                #if(xx == 2 or xx == 3 or xx ==7):
                #    #print(StringTmp)
                #    if(xx==7 and StringTmp != ''):
                #        StringTmp = datetime.datetime.strptime(StringTmp.strip("'"),'%m/%d/%Y').strftime('%Y-%m-%d')
                #    StringTmp = StringTmp[:10]
                
                
                    
                if(StringTmp.isdigit()):
                    StringTmp = str(float(StringTmp))
                else:
                    StringTmp = str(StringTmp) 
                String1 = String1 + "," +   StringTmp  #Looping through Columns of a row & appending them 
        
        newrptdata1.loc[i] = String1
        
    #print(newrptdata1.head(5))   
    #newrptdata1.to_csv("c:\\Srikanth\\CSV1.csv") 
        
    cols = [['combined','combined','String']]
    DFCompare.compare_fullReport(newdbdata,newrptdata1,cols)
    html_logger.info("End of Disability Claims Detail Report_All Claims  " + str(polnum) + " and Ben Type " + benType)
 



def compare_String(xlval,finval,fieldval):
    if(str(xlval).strip() == 'nan'):
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))

def compare_float(xlval,finval,fieldval):
    if(str(xlval).strip() == 'nan'):
        xlval = 0
        
    if(float(str(xlval).strip())== float(str(finval).strip())):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))




def compare_ClassDesc(xlval,finval,waiverDescList,fieldval):
    if(str(xlval).strip() == 'nan'):
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        if(str(xlval) in waiverDescList):
            html_logger.dbg(fieldval + " validation is pass. The value is " + str(xlval) + " and is present in the List of Class Description")
        else:     
            html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))


def compare_Date(xlval,finval,fieldval):
    try:
        if(str(xlval).strip() == ''):
            xlval = ''
        elif(str(xlval).strip() == 'NaT'):
            xlval = ''        
        else:    
            datetimeobject = datetime.strptime(str(xlval),'%Y-%m-%d %H:%M:%S')
            xlval = datetimeobject.strftime('%m/%d/%Y')
    except:
        xlval = ''
    
    if(str(finval).strip() == ''):
            finval = ''
    finval = finval.replace("-","")            
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))

def compare_Date1(xlval,finval,fieldval):
    try:
        if(str(xlval).strip() == ''):
            xlval = ''
        elif(xlval =='No Waiver'):
            xlval = 'No Waiver'
        elif(str(xlval).strip() == 'NaT'):
            xlval = ''        
        else:    
            datetimeobject = datetime.strptime(str(xlval),'%m/%d/%Y')
            xlval = datetimeobject.strftime('%m/%d/%Y')
    except:
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))

#####################################################################                     
def waiverReportValidation_Fineos(caseno,xldrow):
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
        time.sleep(3)    
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        time.sleep(3)    
        
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
        if len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel DataLabel']"))>0:
            benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel DataLabel']").text
        elif len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel']"))>0:
            benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel']").text
        else:
            benStartDate = ''
         
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        time.sleep(3)           
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        #for i on range(rowCnt):
        waiverDescription = driver.find_element_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr["+ str(rowCnt) + "]/td").text
        try:
            disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text
        except:
            disDate = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(3)   
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(3)
        try:
            waiverEndDate = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[1]/td[5]").text
        except:
            waiverEndDate= ''
            
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        if disDate == '-'  or len(disDate)<10:
            disDate = claimDisDate
        else:
            disDate =  disDate
               
        idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        a = idNumber.split(" ")[0]
        idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
       
        #disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
        
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
        time.sleep(9)
        
    
        classDesc = '' 
        classID = '' 
        classid = ''
        row = 1
        x1 = 0
        rowcnts = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
        #print(rowcnt)
        while x1 == 0:
            for i in range(1,rowcnt+1):
                classID = driver.find_element_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr[" + str(i) + "]/td[3]").text
                if type in shortTerm:
                    if classID.split("-")[1] in shortTerm:
                        classid = classID
                        x1 =1
                        break
                        
                    else:
                        try:
                            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(row+1) + "]/td[1]").click()
                        except:
                             x1=1   
                        time.sleep(3)
                        
                elif type in longTerm:
                    if classID.split("-")[1] in longTerm:
                        classid = classID
                        x1 =1
                        break
                    else:
                        try:
                            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(row+1) + "]/td[1]").click()
                        except:
                             x1=1   
                        time.sleep(3)
                        
        classDescList = []                                                  
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr"))
        for i in range(1,rowcnt+1):
            classID = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[1]").text
            classDescList += [driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text]
            if classID==classid:
                classDesc = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text
                #break
        
        if len(disDate) == 10 :#benStatus == 'Approved'or 1==1 :
            if waiverDescription == "Waiver of Premium: No Waiver":
                waiverbeginDate = 'No Waiver'
                waiverendDate = 'No Waiver'
                
            elif waiverDescription == 'Waiver of Premium: Beginning of Elimination Period':
                waiverbeginDate = disDate
                waiverendDate = '' #need to pick from recurring payment
                
            elif waiverDescription == 'Waiver of Premium: After Elimination Period':
                waiverbeginDate = benStartDate
                waiverendDate = waiverEndDate #need to pick from recurring payment  
                
            elif waiverDescription == 'Waiver of Premium: First of Month After 30 Days':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=30)
                waiverbeginDate = (waiverbeginDate.replace(day=1) + td(days=32)).replace(day=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment     
                
            elif waiverDescription == 'Waiver of Premium: First of Month After 52wks Coverage':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(weeks=52)
                waiverbeginDate = (waiverbeginDate.replace(day=1) + td(days=32)).replace(day=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment   
             
            elif waiverDescription == 'Waiver of Premium: Begins Immed following 12W of Dis':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(weeks=12) + td(days=1)
                #waiverbeginDate = datetime.strptime(waiverbeginDate, "%m/%d/%Y") + td(days=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment   
                
            elif waiverDescription == 'Waiver of Premium: Begins First Day of Disability':
                waiverbeginDate = disDate
                waiverendDate = waiverEndDate #need to pick from recurring payment 
            
            elif waiverDescription == 'Waiver of Premium: Begins 91st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=91)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment    
            
            elif waiverDescription == 'Waiver of Premium: Begins 31st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=31)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 46th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=46)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 61st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=61)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 76th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=76)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 101st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=101)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 121st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=121)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 366th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=366)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
                                     
            else:        
                waiverbeginDate = disDate
                waiverendDate = ''
        else:
            waiverbeginDate = ''
            waiverendDate = ''    
                
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        
        xldisDate = xldrow[1]
        xlDOB = xldrow[2]
        xlidNumber = xldrow[3]
        xllName = xldrow[4]
        xlfname = xldrow[5]
        xlwaiverbeginDate = str(xldrow[6]).replace('nan','')
        xlwaiverendDate = str(xldrow[7]).replace('nan','')
        xlwaiverDescription = xldrow[8]
        xlbenStatus = xldrow[9]
        xlclassDesc = xldrow[10]
        
        compare_Date(xldisDate,disDate,'Disability Date')
        compare_Date(xlDOB,DOB,'Date of Birth')
        compare_String(xlidNumber,idNumber,'IDNumber')
        compare_String(xllName,lname,'Last Name')
        compare_String(xlfname,fname,'First Name')
        
        #compare_Date1(xlwaiverbeginDate,waiverbeginDate,'WaiverBeginDate')
        
        if(xlwaiverbeginDate==''):
            compare_Date1(xlwaiverbeginDate,'','WaiverBeginDate')
        elif(xlwaiverbeginDate=='No Waiver'):
            compare_Date1(xlwaiverbeginDate,'No Waiver','WaiverBeginDate')
        else:    
            compare_Date1(xlwaiverbeginDate,waiverbeginDate,'WaiverBeginDate')
            
        if(xlwaiverendDate==''):
            compare_Date1(xlwaiverendDate,'','WaiverEndDate')
        elif(xlwaiverendDate=='No Waiver'):
            compare_Date1(xlwaiverendDate,'No Waiver','WaiverEndDate')
        else:    
            compare_Date1(xlwaiverendDate,waiverendDate,'WaiverEndDate')
            
        compare_String(xlwaiverDescription,waiverDescription,'waiverDescription')
        compare_String(xlbenStatus,benStatus,'benStatus')
        compare_ClassDesc(xlclassDesc,classDesc,classDescList,'classDesc')
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        disDate = claimDisDate
               
        idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        a = idNumber.split(" ")[0]
        idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
       
        #disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
       
        
        xldisDate = xldrow[1]
        xlDOB = xldrow[2]
        xlidNumber = xldrow[3]
        xllName = xldrow[4]
        xlfname = xldrow[5]
        xlwaiverbeginDate = str(xldrow[6]).replace('nan','')
        xlwaiverendDate = str(xldrow[7]).replace('nan','')
        xlwaiverDescription = xldrow[8]
        xlbenStatus = xldrow[9]
        xlclassDesc = xldrow[10]
        
        compare_Date(xldisDate,disDate,'Disability Date')
        compare_Date(xlDOB,DOB,'Date of Birth')
        compare_String(xlidNumber,idNumber,'IDNumber')
        compare_String(xllName,lname,'Last Name')
        compare_String(xlfname,fname,'First Name')
        compare_String(xlwaiverbeginDate,'','Waiver begin Date')
        compare_String(xlwaiverendDate,'','Waiver End Date')
        compare_String(xlwaiverDescription,'','Description')        
        compare_String(xlbenStatus,benStatus,'Description')   
       
        #html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------

def waiverValidation():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of Disability Waiver Report Validation with Fineos")    
    
    """
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "STD Claims" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    waiverReportValidation_Fineos(caseno,xldf.iloc[i])
            #print(xldf.iloc[i,j])
    """
    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "Disability Waiver Report" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    waiverReportValidation_Fineos(caseno,xldf.iloc[i])
 
##################################################################### 
def waiverPremiumValidation_Fineos(caseno,xldrow):
    
    xlClaimType = xldrow[0]
    xlPolicyholderName = xldrow[1]
    xlPolicyNumber = xldrow[2]
    xllName = xldrow[3]
    xlfname = xldrow[4]
    xlDOB = xldrow[5]
    xlArenaClaimNo = xldrow[6]
    xlPlanType = xldrow[8]
    xlwaiverbeginDate = str(xldrow[10]).replace('nan','')
    xlwaiverendDate = str(xldrow[11]).replace('nan','')
    xlwaiverDescription = xldrow[13]
    xlbenStatus = xldrow[14]
    xlbenStartDt = xldrow[16]
    xlbenEndDt = xldrow[17]
    xlDtofDis = xldrow[21]
    xlElimPeriod = xldrow[22]
    xlBillingCodes = xldrow[23]
                                
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    #time.sleep(5) 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.CONTROL + "a");
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.DELETE)
    
    time.sleep(2) 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        try:
            if type in shortTerm:
                driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
            elif type in longTerm:
                driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
            else:
                 driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
        except:
             return
                
        time.sleep(3)    
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        time.sleep(3)    
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
        
        if type != '':     #don't perfirm these actions if there is no benefits
            if type in shortTerm:
                ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
            elif type in longTerm:   
                ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
                time.sleep(3)
            
            if type in shortTerm:
                polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
                claimType =driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage'][1]/span").get_attribute('textContent')
                planType = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code'][1]/span").get_attribute('textContent')
            elif type in longTerm:   
                polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
                claimType =driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage'][1]/span").get_attribute('textContent')    
                planType = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code'][1]/span").get_attribute('textContent')       
        
            planType = planType.split(" ")[1]   
        
        if len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel DataLabel']"))>0:
            benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel DataLabel']").text
        elif len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel']"))>0:
            benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel']").text
        else:
            benStartDate = ''
         
        if len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_End_Date')][@class='DataLabel DataLabel']"))>0:
            benEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')][@class='DataLabel DataLabel']").text
        elif len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_End_Date')][@class='DataLabel']"))>0:
            benEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')][@class='DataLabel']").text
        else:
            benEndDate = ''
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        try:
            driver.find_element_by_xpath("//div[contains(text(),'Contacts')][@class='TabOff']").click()
            time.sleep(3)   
        except:
            pass         
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(10)           
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        #for i on range(rowCnt):
        waiverDescription = driver.find_element_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr["+ str(rowCnt) + "]/td").text
        try:
            disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text
        except:
            disDate = ''
         
        driver.find_element_by_xpath("//div[contains(text(),'Benefit Parameters')][@class='TabOff']").click()
        time.sleep(3)
        SickElimPeriod = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriod')]").text
        AccElimPeriod = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriodAccident')]").text
        
            
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(3)   
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(3)
        try:
            waiverEndDate = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[1]/td[5]").text
        except:
            waiverEndDate= ''
         
         #benStatus
        driver.find_element_by_xpath("//div[contains(text(),'Case History')][@class='TabOff']").click()
        time.sleep(3)
        time.sleep(3)
        driver.find_element_by_xpath("//a[contains(@id,'_DROPDOWN.WidgetListContentSource.NewsFeedForCaseWidgetListlink')]").click()
        driver.find_element_by_xpath("//input[contains(@id,'NewsFeedForCaseWidgetList_All_Allcheck_CHECKBOX')]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//input[contains(@id,'Process_Changes_Process_Changescheck_CHECKBOX')]").click()
        driver.find_element_by_xpath("//input[contains(@id,'NewsFeedForCaseWidgetList_applyChanges_applyChangesbutton')]").click()
        
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        time.sleep(2)
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2019")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break  
        
        StatusDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
        StatusDate = StatusDate.split(", ")[1]
        datetimeobject = datetime.strptime(str(StatusDate),'%b %d %Y')
        StatusDate = datetimeobject.strftime('%m/%d/%Y')
        
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied':
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
            #closedReason = driver.find_element_by_xpath("//span[contains(@id,'_reason')][@class='DataLabel']").text
            cnt1 = len(driver.find_elements_by_xpath("//span[contains(text(),'Managing Process Stage changed to " + benStatus + "')]"))
            closedReason = driver.find_element_by_xpath("(//span[contains(text(),'Managing Process Stage changed to " + benStatus + "')])["+ str(cnt1) +"]/following::span[@title='The reason for changing the stage of the activity, as input by the user']").text
            
        else:
            closedDate = ''
            closedReason = ''
            
                
         
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        time.sleep(3)
            
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        time.sleep(3)
        if len(driver.find_elements_by_xpath("//a[@id='btn_close_popup_msg']")) > 0:
            driver.find_element_by_xpath("//a[@id='btn_close_popup_msg']").click()
        
        
        if (len(driver.find_elements_by_xpath("//div[contains(text(),'Close')]"))> 0):
            driver.find_element_by_xpath("//div[contains(text(),'Close')]").click()
            
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        if disDate == '-'  or len(disDate)<10:
            disDate = claimDisDate
        else:
            disDate =  disDate
               
        idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        a = idNumber.split(" ")[0]
        idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
       
        #disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
        sickAcc = driver.find_element_by_xpath("//span[contains(@id,'_SicknessAccident')]").text
        if sickAcc == 'Sickness':
            finElimPeriod = SickElimPeriod
        else:
            finElimPeriod = AccElimPeriod
        
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        time.sleep(5)
        if len(driver.find_elements_by_xpath("//a[@id='btn_close_popup_msg']")) > 0:
            driver.find_element_by_xpath("//a[@id='btn_close_popup_msg']").click()
        
        driver.find_element_by_xpath("//td[contains(@id,'_CaseTabControlBean_More_Tabs_cell')]").click()
        driver.find_element_by_xpath("//a[contains(text(),'Case Alias')]").click()
        time.sleep(5)
        
        finArenaNumber  =''
        rowcntx = len(driver.find_elements_by_xpath("//table[contains(@id,'_caseAlias')]/tbody/tr"))
        if rowcntx > 1:
            for yy in range(1,rowcntx+1):
                if driver.find_element_by_xpath("//table[contains(@id,'_caseAlias')]/tbody/tr[" + str(yy) + "]/td[1]").text == 'Arena Claim Number':
                    finArenaNumber =  driver.find_element_by_xpath("//table[contains(@id,'_caseAlias')]/tbody/tr[" + str(yy) + "]/td[1]").text   
                    break
        
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'General Claim')]").click()
        time.sleep(3)
        comClaimDt = ''
        if type in shortTerm:
            comClaimDt = driver.find_element_by_xpath("//span[contains(@id,'_employeeStatReceivedDateBean')]").text
        elif type in longTerm:
            comClaimDt = driver.find_element_by_xpath("//span[contains(@id,'_employerStatReceivedDateBean')]").text
        
        if comClaimDt == '-':
            comClaimDt =''
        
               
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
        time.sleep(3)
        
        rowcntxx = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
        for x in range(1, rowcntxx+1):
            if float(driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" +str(x) + "]/td[1]" ).text) == float(xlPolicyNumber):
                driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" +str(x) + "]/td[1]" ).click()
                break
        time.sleep(5)
                
        driver.find_element_by_xpath("//input[contains(@id,'_CoveragesLVW_cmdEdit')]").click()
        time.sleep(3)
        billingMthd = driver.find_element_by_xpath("//span[contains(@id,'_billingMethod')]").text
        if billingMthd == 'Standard (Std) Summary':
            finBillingMthd = '02-Std Summary'
        elif billingMthd == 'Standard (Std) List':
            finBillingMthd = '01-Std List'
        elif billingMthd == 'ePay List':
            finBillingMthd = '03'    
        elif billingMthd == 'eBill':
            finBillingMthd = '04'   
        elif billingMthd == 'AUL Desktop':
            finBillingMthd = '05'
        elif billingMthd == 'iBill':
            finBillingMthd = '06-iBill'       
        elif billingMthd == 'Special Summary':
            finBillingMthd = '07'    
        elif billingMthd == 'Standard (Std) List':
            finBillingMthd = '01'
        
        driver.find_element_by_xpath("//input[contains(@id,'editPageCancel_cloned')]").click()
        time.sleep(3)
                
        driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
        time.sleep(9)
        
        rowcntxx = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
        for x in range(1, rowcntxx+1):
            if float(driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" +str(x) + "]/td[1]" ).text) == float(xlPolicyNumber):
                driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" +str(x) + "]/td[1]" ).click()
                break
        """
        classDesc = '' 
        classID = '' 
        classid = ''
        row = 1
        x1 = 0
        rowcnts = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
        #print(rowcnt)
        while x1 == 0:
            for i in range(1,rowcnt+1):
                classID = driver.find_element_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr[" + str(i) + "]/td[3]").text
                if type in shortTerm:
                    if classID.split("-")[1] in shortTerm:
                        classid = classID
                        x1 =1
                        break
                        
                    else:
                        try:
                            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(row+1) + "]/td[1]").click()
                            time.sleep(3)
                            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
                            i = 1
                        except:
                             x1=1   
                        time.sleep(3)
                        
                elif type in longTerm:
                    if classID.split("-")[1] in longTerm:
                        classid = classID
                        x1 =1
                        break
                    else:
                        try:
                            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(row+1) + "]/td[1]").click()
                            time.sleep(3)
                            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
                            i = 1
                        except:
                             x1=1   
                        time.sleep(3)
                        
        classDescList = []                                                  
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr"))
        for i in range(1,rowcnt+1):
            classID = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[1]").text
            classDescList += [driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text]
            if classID==classid:
                classDesc = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text
         """       #break
        finPolicyHolderName = driver.find_element_by_xpath("//table[contains(@id,'MemberDetailsDivisionClassLinkListviewWidget')]/tbody/tr[1]/td[2]").text
        
        if len(disDate) == 10 :#benStatus == 'Approved'or 1==1 :
            if waiverDescription == "Waiver of Premium: No Waiver":
                waiverbeginDate = 'No Waiver'
                waiverendDate = 'No Waiver'
                
            elif waiverDescription == 'Waiver of Premium: Beginning of Elimination Period':
                waiverbeginDate = disDate
                waiverendDate = waiverEndDate #need to pick from recurring payment
                
            elif waiverDescription == 'Waiver of Premium: After Elimination Period':
                waiverbeginDate = benStartDate
                waiverendDate = waiverEndDate #need to pick from recurring payment  
                
            elif waiverDescription == 'Waiver of Premium: First of Month After 30 Days':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=30)
                waiverbeginDate = (waiverbeginDate.replace(day=1) + td(days=32)).replace(day=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment     
                
            elif waiverDescription == 'Waiver of Premium: First of Month After 52wks Coverage':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(weeks=52)
                waiverbeginDate = (waiverbeginDate.replace(day=1) + td(days=32)).replace(day=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment   
             
            elif waiverDescription == 'Waiver of Premium: Begins Immed following 12W of Dis':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(weeks=12) + td(days=1)
                #waiverbeginDate = datetime.strptime(waiverbeginDate, "%m/%d/%Y") + td(days=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment   
                
            elif waiverDescription == 'Waiver of Premium: Begins First Day of Disability':
                waiverbeginDate = disDate
                waiverendDate = waiverEndDate #need to pick from recurring payment 
            
            elif waiverDescription == 'Waiver of Premium: Begins 91st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=91)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment    
            
            elif waiverDescription == 'Waiver of Premium: Begins 31st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=31)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 46th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=46)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 61st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=61)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 76th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=76)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 101st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=101)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 121st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=121)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 366th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=366)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
                                     
            else:        
                waiverbeginDate = disDate
                waiverendDate = ''
        else:
            waiverbeginDate = ''
            waiverendDate = ''    
                
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        
        xlClaimType = xldrow[0]
        xlPolicyholderName = xldrow[1]
        xlPolicyNumber = xldrow[2]
        xllName = xldrow[3]
        xlfname = xldrow[4]
        xlDOB = xldrow[5]
        xlArenaClaimNo = xldrow[6]
        xlPlanType = xldrow[8]
        xlwaiverbeginDate = str(xldrow[10]).replace('nan','')
        xlwaiverendDate = str(xldrow[11]).replace('nan','')
        xlwaiverDescription = xldrow[13]
        xlbenStatus = xldrow[14]
        xlbenStartDt = xldrow[16]
        xlbenEndDt = xldrow[17]
        xlDtofDis = xldrow[21]
        xlElimPeriod = xldrow[22]
        xlBillingCodes = xldrow[23]
        
        xlStatusDate = xldrow[15]
        xlclosedDate = xldrow[18]
        xlclosedReason = xldrow[19]
        xlcomClaimDt = xldrow[20]



        
        compare_String(xlClaimType,claimType,'Claim Type')
        compare_String(xlPolicyholderName,finPolicyHolderName,'PolicyholderName')
        compare_float(xlPolicyNumber,polNo,'Policy Number')
        compare_String(xllName,lname,'Last Name')
        compare_String(xlfname,fname,'First Name')
        compare_Date(xlDOB,DOB,'DOB')
        compare_String(xlArenaClaimNo,finArenaNumber,'ArenaNumber')
        compare_String(xlPlanType,planType,'planType')
        #compare_Date(xlwaiverbeginDate,waiverbeginDate,'waiverbeginDate')
        #compare_Date(xlwaiverbeginDate,waiverbeginDate,'waiverbeginDate')
        #compare_Date(xlwaiverendDate,waiverendDate,'waiverendDate')
        compare_String(xlwaiverDescription,waiverDescription,'Waiver Description')
        compare_String(xlbenStatus,benStatus,'Ben Status')
        compare_Date(xlbenStartDt,benStartDate, "Ben Start Date")
        compare_Date(xlbenEndDt,benEndDate, "Ben End Date")
        compare_Date(xlDtofDis,disDate,"Date of Disability")
        compare_String(xlElimPeriod,finElimPeriod,'ElimPeriod')
        compare_String(xlBillingCodes,finBillingMthd,"Billing Method")
        
        compare_Date(xlStatusDate,StatusDate,'StatusDate')
        compare_Date(xlclosedDate,closedDate,'closedDate')
        compare_String(xlclosedReason,closedReason,'closedReason')
        compare_Date(xlcomClaimDt,comClaimDt,'CompleteClaimDt')
        #compare_Date1(xlwaiverbeginDate,waiverbeginDate,'WaiverBeginDate')
        
        if(xlwaiverbeginDate==''):
            compare_Date1(xlwaiverbeginDate,'','WaiverBeginDate')
        elif(xlwaiverbeginDate=='No Waiver'):
            compare_Date1(xlwaiverbeginDate,'No Waiver','WaiverBeginDate')
        else:    
            compare_Date1(xlwaiverbeginDate,waiverbeginDate,'WaiverBeginDate')
            
        if(xlwaiverendDate==''):
            compare_Date1(xlwaiverendDate,'','WaiverEndDate')
        elif(xlwaiverendDate=='No Waiver'):
            compare_Date1(xlwaiverendDate,'No Waiver','WaiverEndDate')
        else:    
            compare_Date1(xlwaiverendDate,waiverendDate,'WaiverEndDate')

    '''else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        disDate = claimDisDate
               
        idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        a = idNumber.split(" ")[0]
        idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
       
        #disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
       
        
        xldisDate = xldrow[1]
        xlDOB = xldrow[2]
        xlidNumber = xldrow[3]
        xllName = xldrow[4]
        xlfname = xldrow[5]
        xlwaiverbeginDate = str(xldrow[6]).replace('nan','')
        xlwaiverendDate = str(xldrow[7]).replace('nan','')
        xlwaiverDescription = xldrow[8]
        xlbenStatus = xldrow[9]
        xlclassDesc = xldrow[10]
        
        compare_Date(xldisDate,disDate,'Disability Date')
        compare_Date(xlDOB,DOB,'Date of Birth')
        compare_String(xlidNumber,idNumber,'IDNumber')
        compare_String(xllName,lname,'Last Name')
        compare_String(xlfname,fname,'First Name')
        compare_String(xlwaiverbeginDate,'','Waiver begin Date')
        compare_String(xlwaiverendDate,'','Waiver End Date')
        compare_String(xlwaiverDescription,'','Description')        
        compare_String(xlbenStatus,benStatus,'Description')   
     '''

def waiverPremiumValidation():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of Disability Premium Report Validation with Fineos")    
       
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "DisabilityPremiumReport" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,7])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==7):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    waiverPremiumValidation_Fineos(caseno,xldf.iloc[i])
 

#######################################################################
 
def disClaimStatusReport_Validation_Fineos(caseno,xldrow, sheetType):
      
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) >= 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5)    
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    
    if type in shortTerm:
        driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
    elif type in longTerm:
        driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
    else:
        driver.find_element_by_xpath("//div[text() = 'Group Disability Claim - ']").click()
        
    if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
        driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
    
    cnt = 0
    
    if type != '':     #don't perfirm these actions if there is no benefits
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')]").text
        try:
            benEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')]").text
        except:
            benEndDate = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)        
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        
        driver.find_element_by_xpath("//div[contains(text(),'Case History')][@class='TabOff']").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break    
       
       
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied':
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
            #closedReason = driver.find_element_by_xpath("//span[contains(@id,'_reason')][@class='DataLabel']").text
            cnt1 = len(driver.find_elements_by_xpath("//span[contains(text(),'Managing Process Stage changed to " + benStatus + "')]"))
            closedReason = driver.find_element_by_xpath("(//span[contains(text(),'Managing Process Stage changed to " + benStatus + "')])["+ str(cnt1) +"]/following::span[@title='The reason for changing the stage of the activity, as input by the user']").text
            
        else:
            closedDate = ''
            closedReason = ''
        
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(5)
        dateAuthThru = ''
        
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr"))
        if(rowcnt>0):
            for i in range(1,rowcnt+1):
                if(driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[" + str(i) + "]/td[2]").text == 'Approved'):
                    dateAuthThru = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[" + str(i) + "]/td[5]").text
                    break
        else:
            dateAuthThru = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
                
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        try:
            driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
            time.sleep(3)
            #driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
            #time.sleep(3)
            driver.find_element_by_xpath("//a[@title='Sort by: Period End Date']").click()
            time.sleep(3)
            driver.find_element_by_xpath("//a[@title='Sort by: Period End Date']").click()
            time.sleep(3)
            
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            if(rowcnt>0):
                datePaidThru = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[1]/td[3]").text
            else:
                datePaidThru = ''
        except:
                datePaidThru = ''       
         
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
    else:
         benStatus = ''
         benStartDate = ''
         benEndDate = notDate = datePaidThru = dateAuthThru = closedDate = closedReason =''
         
         #startDate
            
    if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
    
    if(benStatus == ''):
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
    name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
    name = name.replace('Miss ','').replace('Mr ','').replace('Ms ','')
    length = len(name.split(" "))  
    lname = name.split(" ")[-1]
    fname = name.split(" ")[0]
    
    DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
    
    idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
    a = idNumber.split(" ")[0]
    idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
   
    disDate = driver.find_element_by_xpath("//span[contains(@id,'IncurredDate')][contains(@class,'DataLabel')]").text 
    disDate = disDate.strip().replace("-","")
    
    
    notDate = driver.find_element_by_xpath("//span[contains(@id,'_notificationDate')][@class='DataLabel']").text
    
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'General Claim')]").click()
    time.sleep(3)
    time.sleep(3)
    if type in shortTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - STD']/ancestor::td[1]/following::td/span/span").text
    elif type in longTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - LTD']/ancestor::td[1]/following::td/span/span").text
    else:
         startDate = '-'   
         
    if len(startDate) < 10:
         startDate = ''
    '''
    #claim start date and CLaim end date
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Case History')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
    driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
    
    time.sleep(5)
    while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break    
   
    cnt = len(driver.find_elements_by_xpath("//div[contains(@class,'dateDivider')]/span"))
    if(cnt>0):
        startDate = driver.find_element_by_xpath("(//*[contains(text() ,'Intake Document - GDI')]|//span[contains(text(),'New Claim Submitted')])/preceding::div[@class='dateDivider'][1]/span").text   
        startDate = startDate.split(", ")[1]
        datetimeobject = datetime.strptime(str(startDate),'%b %d %Y')
        startDate = datetimeobject.strftime('%m/%d/%Y')  
    else:
        startDate = '' 
    
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  
    '''
          
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
    time.sleep(3)
    
    driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
    time.sleep(5)
           
    xllName = xldrow[1]
    xlfname = xldrow[2] 
    xlidNumber = xldrow[3]
    xlbenStatus = xldrow[4]
    xldisDate = xldrow[5]
    xlbenStartDate = xldrow[6]
    xlnotDate = xldrow[7]
    xlstartDate = xldrow[8]
    xldatePaidThru = xldrow[9]
    xldateAuthThru = xldrow[10]
    if sheetType != 'Open':
        xlclosedDate = xldrow[11] 
        xlclosedReason = xldrow[12]
        xlbenEndDate = xldrow[13]
    else:
        xlbenEndDate = xldrow[11]
        
                               
    compare_String(xllName, lname, "LastName") 
    compare_String(xlfname, fname, "FirstName") 
    compare_String(xlidNumber.lower(), idNumber.lower(), "idNumber") 
    compare_String(xlbenStatus, benStatus, "benStatus") 
    compare_Date(xldisDate, disDate, "disDate") 
    compare_Date(xlbenStartDate, benStartDate, "benStartDate") 
    
    compare_Date(xlnotDate, notDate, "notification Date")
    compare_Date(xlstartDate, startDate, "Claim Complete Date")
    
    if str(xldatePaidThru).strip() == '' or str(xldatePaidThru) == 'nan' :
        compare_Date(xldatePaidThru, '', "Date Claim Paid Thru")
    else:
        compare_Date(xldatePaidThru, datePaidThru, "Date Claim Paid Thru")
    
    if str(xldateAuthThru).strip() == '' or str(xldateAuthThru) == 'nan' :
        compare_Date(xldateAuthThru, '', "Date Claim Authorized Thru")
    else:
        compare_Date(xldateAuthThru, dateAuthThru, "Date Claim Authorized Thru")

    if str(xlbenEndDate).strip() == '' or str(xlbenEndDate) == 'nan' :
        compare_Date(xlbenEndDate, '', "Benefit End Date")
    else:
        compare_Date(xlbenEndDate, benEndDate, "Benefit End Date")    
    
    if sheetType != 'Open':
        compare_String(xlclosedReason, closedReason, "Closed Reason")
        
        if str(xlclosedDate).strip() == '' or str(xlclosedDate) == 'nan' :
            compare_Date(xlclosedDate, '', "Date Claim Closed")
        else:
            compare_Date(xlclosedDate, closedDate, "Date Claim Closed")


def disClaimStatusReport():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
    html_logger.info("Start of Disability Claim Status Report Validation with Fineos")  
      
    #xlpath = "C:\\Users\\T003320\\Downloads\\Disability Claim Status Report - All Claims.xlsx" #"C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    #sheetname = "ALL STD OPEN CLAIMS"#"STD Claims" #"LTD Claims" #"DisabilityPremiumWaiver"   
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "disClaimStatusReport"
    
    sheetType = 'All' #  'Open' #
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
    
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    disClaimStatusReport_Validation_Fineos(caseno,xldf.iloc[i],sheetType)
            
#####################################################################             

def disClaimDetailReport_Validation_Fineos(caseno,xldrow, sheetType):
      
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) >= 3:
        type = caseno.split("-")[2]
    else:
        type = ''
    classDesc = ''       
    time.sleep(5)    
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    
    if type in shortTerm:
        driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
    elif type in longTerm:
        driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
    else:
        driver.find_element_by_xpath("//div[text() = 'Group Disability Claim - ']").click()
        
    if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
        driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
    
    cnt = 0
    
  
    
    
    if type != '':     #don't perfirm these actions if there is no benefits
        if type in shortTerm:
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
        elif type in longTerm:   
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
            time.sleep(3)
        
        if type in shortTerm:
            polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
        elif type in longTerm:   
            polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
                       
        time.sleep(5)
        ClaimExaminer = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
        ClaimExaminer = ClaimExaminer.split("(")[0]
        ClaimExaminer = ClaimExaminer.strip()
    
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')]").text
        if len(benStartDate)<10:
            benStartDate = ''
            
        try:
            benEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')]").text
        except:
            benEndDate = ''
        
        ActionChains(driver).move_to_element(driver.find_element_by_xpath("//div[contains(text(),'Documents')][@class='TabOff']")).perform()    
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)        
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        #for i on range(rowCnt):
        #waiverDescription = driver.find_element_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr["+ str(rowCnt) + "]/td").text
        claimWorkState = ''
        
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        try:
            driver.find_element_by_xpath("//div[contains(text(),'Occupation')][@class='TabOff']").click()
            time.sleep(3)
            driver.find_element_by_xpath("//input[contains(@id,'_OccupationList_cmdView')]").click()
            time.sleep(3)
            claimWorkState = driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
            
            driver.find_element_by_xpath("//input[contains(@Id,'_cmdPageBack')]").click()
        except:
            pass
       
        driver.find_element_by_xpath("//div[contains(text(),'Case History')][@class='TabOff']").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
                try:
                    driver.find_element_by_xpath("//a[text()='View Older']").click()
                    time.sleep(5)
                except:
                    break 
            
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied' or benStatus == 'Approved':
            if benStatus == 'Closed - Approved':
                benStatus1 = 'Approved'
            else:
                benStatus1 = benStatus
                        
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
            closedReason = driver.find_element_by_xpath("//span[contains(@id,'_reason')][@class='DataLabel']").text
            time.sleep(3)
            
            #cnt = len(driver.find_elements_by_xpath("//span[text()='Managing Process Stage changed to " + benStatus1 + "']/ancestor::div[@class='WidgetListWidget'][1]/preceding-sibling::div[@class='dateDivider']/span"))
            cnt = len(driver.find_elements_by_xpath("//span[text()='Managing Process Stage changed to " + benStatus1 + "']"))
            if(cnt>0):
                InitialDecisionDt = driver.find_element_by_xpath("(//span[text()='Managing Process Stage changed to " + benStatus1 + "'])["+ str(cnt) + "]/ancestor::div[@class='WidgetListWidget'][1]/preceding-sibling::div[@class='dateDivider'][1]/span").text
                #InitialDecisionDt = driver.find_element_by_xpath("(//span[text()='Managing Process Stage changed to " + benStatus1 + "']/ancestor::div[@class='WidgetListWidget'][1]/preceding-sibling::div[@class='dateDivider'][1]/span)[" + str(cnt) + "]").text
                InitialDecisionDt = InitialDecisionDt.split(", ")[1]
                datetimeobject = datetime.strptime(str(InitialDecisionDt),'%b %d %Y')
                InitialDecisionDt = datetimeobject.strftime('%m/%d/%Y')
            else:
                InitialDecisionDt = ''   

        else:
            closedDate = ''
            closedReason = InitialDecisionDt = ''

        time.sleep(5)
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(5)
        
        if(len(driver.find_elements_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr"))>0):
            dateAuthThru = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[1]/td[5]").text
        else:
            dateAuthThru = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        
        #-------last payment date-------------------------
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        try:
            #driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
            time.sleep(3)
            driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
            time.sleep(3)
            
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            if rowcnt > 0:
                for i in range(1,rowcnt+1):
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    LastPaidDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')]").text
                    checkno = driver.find_element_by_xpath("//span[contains(@id,'_TransactionNumber')]").text
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    if checkno != '-' or checkno != '':
                        break
            else:
                 LastPaidDate = ''       
        except:
            LastPaidDate = ''
        #-------last payment date-------------------------
        
        #-------------Total benefits paid to date-----------------------------------
        try:
            NetAmount = 0
            if len(driver.find_elements_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']"))==0:
                NetAmount = 0
            else:   
                while(1==1):
                    driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
                    time.sleep(3)
                    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
                    if rowcnt > 0:
                        for i in range(1,rowcnt+1):
                            driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                            time.sleep(3)
                            driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                            time.sleep(3)
                            NetAmount = NetAmount + float(driver.find_element_by_xpath("//span[contains(@id,'_netBenefitAmountBean')]").text.replace(',',''))
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                        if(i==rowcnt):
                            if(len(driver.find_elements_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]"))>0):
                                driver.find_element_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]").click() 
                                time.sleep(3)
                            else:
                                break
                    else:
                            NetAmount = 0
                            break               
                print(NetAmount)
        except:
            NetAmount = 0    
        #---------------Total benefits paid to date-----------------------------------
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        try:
            #driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            if(rowcnt>0):
                datePaidThru = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[1]/td[3]").text
            else:
                datePaidThru = ''
        except:
                datePaidThru = ''       
        
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
    else:
         benStatus = ''
         benStartDate = ''
         benEndDate = notDate = datePaidThru = dateAuthThru = closedDate = closedReason = claimWorkState = InitialDecisionDt = dateAuthThru = LastPaidDate = datePaidThru = ''
         NetAmount = 0
         time.sleep(5)
         ClaimExaminer = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
         ClaimExaminer = ClaimExaminer.split("(")[0]
         ClaimExaminer = ClaimExaminer.strip()
         #startDate
            
    if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
    
    name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
    name = name.replace('Miss ','').replace('Mr ','').replace('Ms ','')
    
    length = len(name.split(" "))  
    lname = name.split(" ")[-1]
    fname = name.split(" ")[0]
    
    if benStatus == '':
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
    DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
    
    idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
    a = idNumber.split(" ")[0]
    idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
    try:
        disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
    except:
        disDate = ''
        
    notDate = driver.find_element_by_xpath("//span[contains(@id,'_notificationDate')][@class='DataLabel']").text
    
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'General Claim')]").click()
    time.sleep(3)
    if type in shortTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - STD']/ancestor::td[1]/following::td/span/span").text
    elif type in longTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - LTD']/ancestor::td[1]/following::td/span/span").text
    else:
         startDate = '-'   
         
    if len(startDate) < 10:
         startDate = ''
         
    if claimWorkState == '':
        #driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        #time.sleep(3)
        driver.find_element_by_xpath("//div[contains(text(),'Occupation')][@class='TabOff']").click()
        time.sleep(3)
        try:
            driver.find_element_by_xpath("//input[contains(@id,'_OccupationList_cmdView')]").click()
            time.sleep(3)
            claimWorkState = driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
            driver.find_element_by_xpath("//input[contains(@name,'_cmdPageBack')]").click()
        except:
            claimWorkState = ''
            
    '''
    #claim start date and CLaim end date
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Case History')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
    driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
    
    time.sleep(5)
    while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break    
   
    cnt = len(driver.find_elements_by_xpath("//div[contains(@class,'dateDivider')]/span"))
    if(cnt>0):
        startDate = driver.find_element_by_xpath("(//div[contains(@class,'dateDivider')]/span)[" + str(cnt) + "]").text   
        startDate = startDate.split(", ")[1]
        datetimeobject = datetime.strptime(str(startDate),'%b %d %Y')
        startDate = datetimeobject.strftime('%m/%d/%Y')  
    else:
        startDate = ''  
        
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  
    '''
    classid = ''          
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
    time.sleep(3)

    
    """
    driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
    time.sleep(9)
    
    if polNo != '':
        rwct1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
        for s in range(1,rwct1+1):
            if driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(s) + "]/td[1]").text == polNo :
                driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(s) + "]/td[1]").click()
                break
    
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
        #print(rowcnt)
        for i in range(1,rowcnt+1):
            classID = driver.find_element_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr[" + str(i) + "]/td[3]").text
            if type in shortTerm:
                if classID.split("-")[1] in shortTerm:
                    classid = classID
                    break
            elif type in longTerm:
                if classID.split("-")[1] in longTerm:
                    classid = classID
                    break
            else:
                    classid = ''    
        if(classid != ''):            
            classDescList = []                                                  
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr"))
            for i in range(1,rowcnt+1):
                classID = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[1]").text
                classDescList += [driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text]
                if classID==classid:
                    classDesc = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text
        else: 
            classDesc = ''
            classDescList = []
            
        driver.find_element_by_xpath("//input[contains(@name,'_editPageCancel')]").click()
        time.sleep(3)
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END)
    else:
        classDesc = ''
        classDescList = []
    """
    classDesc = ''
    classDescList = []
    #----------Start of claimant deails------------
    time.sleep(5)
    driver.find_element_by_xpath("//dt[contains(text(),'Claimant')]/following::dd/a").click()
    time.sleep(5)
    try:
        driver.find_element_by_xpath("//dt[contains(text(),'Claimant')]/following::dd/a").click()
    except:
        pass   
    
    if(len(driver.find_elements_by_xpath("//span[contains(@id,'gender')]"))>0):
        Gender = driver.find_element_by_xpath("//span[contains(@id,'gender')]").text
    else:
        Gender = ''
    if(len(driver.find_elements_by_xpath("//span[text()='HOME']/ancestor::div[1]/following::div[1]/descendant::span[contains(@name,'_address')]")) > 0):
        addr = driver.find_element_by_xpath("//span[text()='HOME']/ancestor::div[1]/following::div[1]/descendant::span[contains(@name,'_address')]").text   #    //span[text()='HOME']/ancestor::div[1]/following::div[1]/div/span
        claimResState = addr.split('\n')[1].split(",")[1].strip()
        print(claimResState)
    else:    
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
            claimResState = addr.split('\n')[1].split(",")[1].strip()
            print(claimResState)
        else:
            claimResState = ''
    
    xllName = xldrow[1]
    xlfname = xldrow[2] 
    xlidNumber = xldrow[3]
    xlbenStatus = xldrow[4]
    xldisDate = xldrow[5]
    xlbenStartDate = xldrow[6]
    xlGender = xldrow[7]
    xlDOB = xldrow[8]
    xlnotDate = xldrow[9]
    xlstartDate = xldrow[10]
    xlInitialDecisionDt = xldrow[11]
    xldatePaidThru = xldrow[12]
    xlLastPaidDate = xldrow[13]
    xldateAuthThru = xldrow[14]
    if sheetType != 'Open':
        xlclosedDate = xldrow[15]
        xlclosedReason = xldrow[16]
        xlbenEndDate = xldrow[17]
        xltotBenPaidDate = xldrow[18]
        xlclassDesc = xldrow[19]
        xlclaimResState = xldrow[20]
        xlclaimWorkState = xldrow[21]
        xlClaimExaminer = xldrow[22] 
    else:
        xlbenEndDate = xldrow[15]
        xltotBenPaidDate = xldrow[16]
        xlclassDesc = xldrow[17]
        xlclaimResState = xldrow[18]
        xlclaimWorkState = xldrow[19]
        xlClaimExaminer = xldrow[20]    
    #------------------------------------------------------------                
    if xlclaimWorkState == 'Unknown':
            xlclaimWorkState =''
    if claimWorkState == 'Unknown':
         claimWorkState =''
                
    compare_String(xllName, lname, "LastName") 
    compare_String(xlfname, fname, "FirstName") 
    compare_String(xlidNumber.lower(), idNumber.lower(), "idNumber") 
    compare_String(xlbenStatus, benStatus, "benStatus") 
    compare_Date(xldisDate, disDate, "disDate") 
    compare_Date(xlbenStartDate, benStartDate, "benStartDate") 
    compare_Date(xlDOB, DOB, "Date Of Birth")
    compare_String(xlGender, Gender, "Gender")
    compare_Date(xlnotDate, notDate, "notification Date")
    compare_Date(xlstartDate, startDate, "Date Claim Opened")
    compare_Date(xlInitialDecisionDt, InitialDecisionDt, "Initial Decision Date")
    #compare_Date(xlInitialDecisionDt, InitialDecisionDt1, "Initial Decision Date")
    
    if str(xldatePaidThru).strip() == '' or str(xldatePaidThru) == 'nan' :
        compare_Date(xldatePaidThru, '', "Date Claim Paid Thru")
    else:
        compare_Date(xldatePaidThru, datePaidThru, "Date Claim Paid Thru")
    
    
    compare_Date(xlLastPaidDate, LastPaidDate, "Last Paid Date") 
    
    if str(xldateAuthThru).strip() == '' or str(xldateAuthThru) == 'nan' :
        compare_Date(xldateAuthThru, '', "Date Claim Authorized Thru")
    else:
        compare_Date(xldateAuthThru, dateAuthThru, "Date Claim Authorized Thru")

    if str(xlbenEndDate).strip() == '' or str(xlbenEndDate) == 'nan' :
        compare_Date(xlbenEndDate, '', "Benefit End Date")
    else:
        compare_Date(xlbenEndDate, benEndDate, "Benefit End Date")
    
    
    compare_String(str(xltotBenPaidDate).replace('.0',''), str(NetAmount).replace('.0',''), "Total Benefits Paid To Date")
    compare_ClassDesc(xlclassDesc,classDesc,classDescList,'classDesc') 
    compare_String(xlclaimResState, claimResState, "Claimant Resident State")
    compare_String(xlclaimWorkState, claimWorkState, "Claimant Work State")
    compare_String(xlClaimExaminer, ClaimExaminer, "Claim Examiner")
        
    if sheetType != 'Open':
        if len(closedReason.strip()) ==1:
            closedReason = closedReason.replace('-','')
            compare_String(xlclosedReason, closedReason, "Closed Reason")
            
        if str(xlclosedDate).strip() == '' or str(xlclosedDate) == 'nan' :
            compare_Date(xlclosedDate, '', "Date Claim Closed")
        else:
            compare_Date(xlclosedDate, closedDate, "Date Claim Closed")
            
    

def disClaimDetailReport():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
    html_logger.info("Start of Disability Claim Detail Report Validation with Fineos")  
      
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\\Users\\T003320\\Downloads\\Disability Claim Status Report - All Claims.xlsx" ##"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "DisClaimDtlRpt" #"ALL STD OPEN CLAIMS"#"STD Claims" #"LTD Claims" #"DisabilityPremiumWaiver"   
    sheetType =    'All' #'Open'  # 
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
    
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    disClaimDetailReport_Validation_Fineos(caseno,xldf.iloc[i],sheetType)
  
##################################################################### 
def FICAWithholdingMatch_Fineos(caseno,xldrow,strtDate):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        time.sleep(5)
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").send_keys(strtDate)
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        listTotamt = []
        listmedwithld = []
        listsswithld = []
        listmedTotamt = []
        listssTotamt = []
        listmedmatch = []
        listssmatch=[]
        if rowcnt > 0:

            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listmedwithld.append(str('0')) 
                    listsswithld.append(str('0')) 
                    listmedTotamt.append(str('0')) 
                    listssTotamt.append(str('0')) 
                    listmedmatch.append(str('0'))
                    listssmatch.append(str('0'))
                    
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        if adjName == 'FICA Medicare':
                            medwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            medwithld  = '%.2f' %(float(medwithld))
                            #print(str(medwithld) + " as medwithld amount")
                            listmedwithld.append(str('%.2f' %(float(medwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            medTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            medTotamt = medTotamt.replace(",","")
                            
                            if '.00' in medTotamt:
                                listmedTotamt.append(str(medTotamt.replace('.00','')))
                            else:
                                listmedTotamt.append(str('%.2f' %(float(medTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            if xldrow['W2 Prepared by OneAmerica'].iloc[0] == 'Y' or xldrow['W2 Prepared by OneAmerica'].iloc[0] == 'B' :
                                medmatch = float(medwithld)
                            else:
                                medmatch = 0
                            listmedmatch.append(str('%.2f' %(abs(float(medmatch)))))
                                    
                        elif adjName == 'FICA Social Security':
                            sswithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            sswithld  = '%.2f' %(float(sswithld))
                            #print(str(sswithld) + " as sswithld amount")
                            listsswithld.append(str('%.2f' %(float(sswithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            ssTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            ssTotamt = ssTotamt.replace(",","")
                            
                            if '.00' in ssTotamt:
                                listssTotamt.append(str(ssTotamt.replace('.00','')))
                            else:
                                listssTotamt.append(str('%.2f' %(float(ssTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            if xldrow['W2 Prepared by OneAmerica'].iloc[0] == 'Y' or xldrow['W2 Prepared by OneAmerica'].iloc[0] == 'B' :
                                ssmatch = float(sswithld)
                            else:
                                ssmatch = 0
                            listssmatch.append(str('%.2f' %(abs(float(ssmatch)))))
                            
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        else:
            listmedwithld.append(str('0')) 
            listsswithld.append(str('0')) 
            listmedTotamt.append(str('0')) 
            listssTotamt.append(str('0')) 
            listmedmatch.append(str('0'))
            listssmatch.append(str('0'))
                    
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        
        if rowcnt > 0:
            for i in range(1,rowcnt+1):
                listName.append(name.strip())
                listcustId.append(cusid)
                listssn.append(ssn)
                listcaseno.append(caseno)
                listempMatch.append("0")
        else:
                listName.append(name.strip())
                listcustId.append(cusid)
                listssn.append(ssn)
                listcaseno.append(caseno)
                listempMatch.append("0")
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Claimant Name': listName, 'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD Medicare Wages': listmedTotamt,'QTD Medicare Withheld': listmedwithld, 'QTD Employer Medicare Match': listmedmatch,'QTD Soc Sec Wages': listssTotamt, 'QTD Soc Sec Withheld': listsswithld,'QTD Employer Match Soc Sec': listssmatch} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,2:]
        
        fin_df['QTD Medicare Wages']  =  fin_df['QTD Medicare Wages'].astype(float)  
        xldrow['QTD Medicare Wages']  =  xldrow['QTD Medicare Wages'].astype(float)
        
        fin_df['QTD Soc Sec Wages']  =  fin_df['QTD Soc Sec Wages'].astype(float)  
        xldrow['QTD Soc Sec Wages']  =  xldrow['QTD Soc Sec Wages'].astype(float)
        
        fin_df['QTD Medicare Withheld']  =  fin_df['QTD Medicare Withheld'].astype(float)  
        xldrow['QTD Medicare Withheld']  =  xldrow['QTD Medicare Withheld'].astype(float)
        
        fin_df['QTD Soc Sec Withheld']  =  fin_df['QTD Soc Sec Withheld'].astype(float)  
        xldrow['QTD Soc Sec Withheld']  =  xldrow['QTD Soc Sec Withheld'].astype(float)
        
        fin_df['QTD Employer Medicare Match']  =  fin_df['QTD Employer Medicare Match'].astype(float)  
        xldrow['QTD Employer Medicare Match']  =  xldrow['QTD Employer Medicare Match'].astype(float)
        
        fin_df['QTD Employer Match Soc Sec']  =  fin_df['QTD Employer Match Soc Sec'].astype(float)  
        xldrow['QTD Employer Match Soc Sec']  =  xldrow['QTD Employer Match Soc Sec'].astype(float)
        
        fin_df['QTD Medicare Wages'].iloc[0] = str('%.2f' %(float(fin_df['QTD Medicare Wages'].sum())))
        fin_df['QTD Soc Sec Wages'].iloc[0] = str('%.2f' %(float(fin_df['QTD Soc Sec Wages'].sum())))
        fin_df['QTD Medicare Withheld'].iloc[0] = str('%.2f' %(abs(float( fin_df['QTD Medicare Withheld'].sum()))))
        fin_df['QTD Soc Sec Withheld'].iloc[0] =str('%.2f' %(abs(float(fin_df['QTD Soc Sec Withheld'].sum()))))
        fin_df['QTD Employer Medicare Match'].iloc[0] = str('%.2f' %(float( fin_df['QTD Employer Medicare Match'].sum())))
        fin_df['QTD Employer Match Soc Sec'].iloc[0] = str('%.2f' %(float( fin_df['QTD Employer Match Soc Sec'].sum())))
        
        xldrow['QTD Medicare Wages'] = str('%.2f' %(float(xldrow['QTD Medicare Wages'])))
        xldrow['QTD Soc Sec Wages'] = str('%.2f' %(float(xldrow['QTD Soc Sec Wages'])))
        xldrow['QTD Medicare Withheld'] = str('%.2f' %(abs(float( xldrow['QTD Medicare Withheld']))))
        xldrow['QTD Soc Sec Withheld'] =str('%.2f' %(abs(float(xldrow['QTD Soc Sec Withheld']))))
        xldrow['QTD Employer Medicare Match'] = str('%.2f' %(float( xldrow['QTD Employer Medicare Match'])))
        xldrow['QTD Employer Match Soc Sec'] = str('%.2f' %(float( xldrow['QTD Employer Match Soc Sec'])))
        
          
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[0:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def FICAWithholdingMatch():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 941 FICA Withhold and Match Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "FICAWithHolding_Match" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,5].unique()
    print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            FICAWithholdingMatch_Fineos(caseno,xldf,"01/01/2021")
            
            
            
##################################################################### 
def FedWageTax_Fineos(caseno,xldrow,strtDate):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    xlclaimname = xldrow['Claimant Name'].to_string(index=False)
    xlclaimname = xlclaimname.strip()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + xlclaimname + "']").click()   #name
        time.sleep(3)   
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").send_keys(strtDate)
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listFitWithhld = []
            listFitWages = []
           
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listFitWithhld.append(str('0')) 
                    listFitWages.append(str('0')) 
                  
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    fitPresent = 0
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        
                        if adjName == 'FIT Amount':
                            fitPresent = 1
                            fitwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            fitwithld = fitwithld.replace(",","")
                            fitwithld  = '%.2f' %(abs(float(fitwithld)))
                            #print(str(medwithld) + " as medwithld amount")
                            listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            if '.00' in fitwages:
                                listFitWages.append(str(fitwages.replace('.00','')))
                            else:
                                listFitWages.append(str('%.2f' %(float(fitwages)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                    if fitPresent == 0:
                        #driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        if adjName != 'Alimony' and adjName != 'Child Support':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(1) + "]/td[1]").click()
                            time.sleep(3)
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text

                            fitwages = fitwages.replace(",","")
                            fitwages = str(abs(float(fitwages)))
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)   
                        
                        else:
                            fitwages = '0'
                            
                        if '.00' in fitwages:
                            listFitWages.append(str(fitwages.replace('.00','')))
                        else:
                            listFitWages.append(str('%.2f' %(abs(float(fitwages))))) 
                        
                        fitwithld = '0'
                        listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                        
                         
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        name = name.replace("  "," ")
        
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listempMatch.append("0")
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Claimant Name': listName, 'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD FIT Withheld': listFitWithhld,'QTD FIT Wages': listFitWages} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,1:]
        
        xldrow['Claimant Name'] = xldrow['Claimant Name'].str.replace("  "," ")
        fin_df['QTD FIT Withheld']  =  fin_df['QTD FIT Withheld'].astype(float)  
        xldrow['QTD FIT Withheld']  =  xldrow['QTD FIT Withheld'].astype(float)
        
        fin_df['QTD FIT Wages']  =  fin_df['QTD FIT Wages'].astype(float)  
        xldrow['QTD FIT Wages']  =  xldrow['QTD FIT Wages'].astype(float)
        
        fin_df['QTD FIT Withheld'].iloc[0] =   fin_df['QTD FIT Withheld'].sum()
        fin_df['QTD FIT Wages'].iloc[0] =   fin_df['QTD FIT Wages'].sum()
           
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def FedWageTax():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 941 Federal wages and Tax Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "FedWageTax" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,4].unique()
    print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            FedWageTax_Fineos(caseno,xldf,"01/01/2021")
            

##################################################################### 
def StateWageTax_Fineos(caseno,xldrow, strtDate):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    xldrow['State'] = xldrow['State'].apply(lambda x: '{0:0>2}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").send_keys(strtDate)
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listFitWithhld = []
            listFitWages = []
           
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listFitWithhld.append(str('0')) 
                    listFitWages.append(str('0')) 
                  
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    fitPresent = 0
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        
                        if adjName == 'State Income Tax':
                            fitPresent = 1
                            fitwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            fitwithld = fitwithld.replace(",","")
                            fitwithld  = '%.2f' %(abs(float(fitwithld)))
                            #print(str(medwithld) + " as medwithld amount")
                            listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            if '.00' in fitwages:
                                listFitWages.append(str(fitwages.replace('.00','')))
                            else:
                                listFitWages.append(str('%.2f' %(float(fitwages)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                    if fitPresent == 0:
                        #driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        if adjName != 'Alimony' and adjName != 'Child Support':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(1) + "]/td[1]").click()
                            time.sleep(3)
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)   
                        
                        else:
                            fitwages = '0'
                            
                        if '.00' in fitwages:
                            listFitWages.append(str(fitwages.replace('.00','')))
                        else:
                            listFitWages.append(str('%.2f' %(float(fitwages)))) 
                        
                        fitwithld = '0'
                        listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                        
                         
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()

        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        name = name.replace("  "," ")
        
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
            claimResCountry = (addr.replace('\n','')).split(",")[-1].strip()
            if('USA' in claimResCountry):
                 claimResState = (addr.replace('\n','')).split(",")[-3].strip()   
            #print(claimResState)
            else:
                claimResState = '70'
        
        
        state = claimResState #driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
        #state = strip(str(state.split("-"))[0])
        
        sql11 = "select ST_GID_CD from [ref].[ST_CD_REF] where st_cd= '" + state + "'"
    
        dbdata = Parser.parseSQL(sql11,"SQLD10747,4242","ENTPRS_CLAIMS_DM")
        st_cd = dbdata['ST_GID_CD'][0]
            
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        liststcd = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listempMatch.append("0")
            liststcd.append(st_cd.strip())
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Claimant Name': listName, 'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD SIT Withheld': listFitWithhld,'QTD SIT Wages': listFitWages, 'State' : liststcd} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,2:]
        
        xldrow['Claimant Name'] = xldrow['Claimant Name'].str.replace("  "," ")
        
        fin_df['QTD SIT Withheld']  =  fin_df['QTD SIT Withheld'].astype(float)  
        xldrow['QTD SIT Withheld']  =  xldrow['QTD SIT Withheld'].astype(float)
        
        fin_df['QTD SIT Wages']  =  fin_df['QTD SIT Wages'].astype(float)  
        xldrow['QTD SIT Wages']  =  xldrow['QTD SIT Wages'].astype(float)
        xldrow['QTD SIT Wages'].iloc[0]  = '%.2f' %(float( xldrow['QTD SIT Wages'].iloc[0]))
        
        fin_df['QTD SIT Withheld'].iloc[0] = fin_df['QTD SIT Withheld'].sum()
        fin_df['QTD SIT Wages'].iloc[0] = fin_df['QTD SIT Wages'].sum()
        fin_df['QTD SIT Wages'].iloc[0]  = '%.2f' %(float( fin_df['QTD SIT Wages'].iloc[0]))
         
          
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[0:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def StateWageTax():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of State wages and Tax Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "StateWageTax" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            StateWageTax_Fineos(caseno,xldf,"01/01/2021")
            
##################################################################### 


##################################################################### 
def MiscReport1099_Fineos(caseno,name,xldrow):
    listFitWithhld = []
    listFitWages = []
    liststmltamt = []
    listsurBenamt = []
    listssnein = []
            
    xldrow = xldrow[(xldrow['Claim Number']==caseno) & (xldrow['Name'] == name)]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    #xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    xldrow['Zip Code'] = xldrow['Zip Code'].apply(lambda x: '{0:0>5}'.format(x))
    xldrow['Box 3'] = xldrow['Box 3'].sum()
    xldrow['Box 4 FIT'] = xldrow['Box 4 FIT'].sum()
    xldrow['Box 10'] = xldrow['Box 10'].sum()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
    ssnein = "0.0"
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = xldrow['Name'].iloc[0] #driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        #driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listFitWithhld = []
            listFitWages = []
            liststmltamt = []
            listsurBenamt = []
            listssnein = []
            totalfitwithld = 0
            totalstmltamt = 0
            totalsurBenamt = 0
            
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listFitWithhld.append(str('0')) 
                    listFitWages.append(str('0')) 
                    liststmltamt.append(str('0'))
                    listsurBenamt.append(str('0'))
                    
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    fitPresent = 0
                    
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        if adjName == 'Settlement':
                            stlmtamt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            stlmtamt = float(stlmtamt.replace(',',''))
                            totalstmltamt = totalstmltamt + stlmtamt
                            ssnein = 'TaxID'
                        
                        elif adjName == 'Survivor Benefit':
                            surBenamt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            surBenamt = float(surBenamt.replace(',',''))
                            totalsurBenamt = totalsurBenamt + surBenamt
                            ssnein = 'SSN'
                    
                                                    
                    
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                for j in range(1,rowcnt2+1):
                    adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                    if adjName == 'FIT Amount':
                        fitPresent = 1
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        fitwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                        fitwithld = fitwithld.replace(",","")
                        fitwithld  = '%.2f' %(float(fitwithld))
                        print(fitwithld)
                        totalfitwithld = float(fitwithld) + totalfitwithld
                        #print(str(medwithld) + " as medwithld amount")
                        #listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                        """
                        driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                        time.sleep(3)
                        fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                        fitwages = fitwages.replace(",","")
                        
                        if '.00' in fitwages:
                            listFitWages.append(str(fitwages.replace('.00','')))
                        else:
                            listFitWages.append(str('%.2f' %(float(fitwages)))) 
                        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                        """
                        
                """if fitPresent == 0:
                    #driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                    adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                    if adjName != 'Alimony' and adjName != 'Child Support':
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(1) + "]/td[1]").click()
                        time.sleep(3)
                        driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                        time.sleep(3)
                        fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                        fitwages = fitwages.replace(",","")
                        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)   
                    
                    else:
                        fitwages = '0'
                        
                    if '.00' in fitwages:
                        listFitWages.append(str(fitwages.replace('.00','')))
                    else:
                        listFitWages.append(str('%.2f' %(float(fitwages)))) 
                    
                    fitwithld = '0'
                    listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                    
                     
                    #else:
                    #     listmedwithld.append(str('0')) 
                    #     listsswithld.append(str('0'))  
                    #     listmedTotamt.append(str('0')) 
                    #     listssTotamt.append(str('0')) 
      
                """
                time.sleep(3)        
                driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                time.sleep(3)
                    
            totalsurBenamt = totalsurBenamt * -1    
            totalstmltamt = totalstmltamt * -1    
            listFitWithhld.append(str('%.2f' %(float(totalfitwithld)))) 
            listsurBenamt.append(str('%.2f' %(float(totalsurBenamt))))
            liststmltamt.append(str('%.2f' %(float(totalstmltamt)))) 
            listssnein.append(str(ssnein))      
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()

        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        
        name1 = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        if name1==name:
            driver.find_element_by_xpath("//a[contains(@name,'_KeyInfoBarLink_0')]").click()
        else:
            driver.find_element_by_xpath("//button[@class='us-button']").click()
            driver.find_element_by_xpath("//li[contains(text(),'Person')]").click()
                
            driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(name)
            time.sleep(5)
            if len(driver.find_elements_by_xpath("//i[@class='icon-person' or @class='icon-organisation']/following::strong[text()='" + name + "']")) > 0:
                driver.find_element_by_xpath("//i[@class='icon-person' or @class='icon-organisation']/following::strong[text()='" + name + "']").click()
            else:
                driver.find_element_by_xpath("//button[@class='us-button']").click()
                driver.find_element_by_xpath("//li[contains(text(),'Organization')]").click()
                #driver.find_element_by_xpath("//input[@id='universal-search']").clear()    
                #driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(name)
                time.sleep(5)
                driver.find_element_by_xpath("//i[@class='icon-person' or @class='icon-organisation']/following::strong[text()='" + name + "']").click() 
            time.sleep(5)
        
        
        #cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        #cusid = cusid.strip()
        
        #ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        #ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        #ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        #ssn = ssn1+ssn2+ssn3
        
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
        elif(len(driver.find_elements_by_xpath("//span[contains(@id,'_Address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_Address')]").text  
              
        claimResCountry = (addr.replace('\n','')).split(",")[-1].strip()
        if('USA' in claimResCountry):
             #claimResState = (addr.replace('\n','')).split(",")[-3].strip()
             claimAdd1 =  (addr.replace('\n','')).split(",")[0].strip()
             
             #print((str2.replace('\n','')).split(",")[1].strip())
             claimCity =  (addr.replace('\n','')).split(",")[-4].strip()
             claimState =  (addr.replace('\n','')).split(",")[-3].strip()
             claimZip =  (addr.replace('\n','')).split(",")[-2].strip()
             #print((addr.replace('\n','')).split(",")[-1].strip())

             #claimAdd2 =   
        #print(claimResState)
        else:
            claimResState = '70'
        
        
        #state = claimResState #driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
        #state = strip(str(state.split("-"))[0])
                
        listName = []
        listclaimAdd1 = []
        listclaimAdd2 = []
        listclaimCity = []
        listclaimState = []
        listclaimZip = []
        
        
        #for i in range(1,rowcnt+1):
        listName.append(name.strip())
        listclaimAdd1.append(claimAdd1)
        listclaimAdd2.append('0.0')
        listclaimCity.append(claimCity)
        listclaimState.append(claimState)
        listclaimZip.append(claimZip)
            
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'EIN/SSN Type' :listssnein ,'Claimant Name': listName, 'Address Line One': listclaimAdd1,'Address Line Two': listclaimAdd2, 'City': listclaimCity, 'State': listclaimState, 'Zip Code': listclaimZip,'Box 3': listsurBenamt, 'Box 4 FIT' : listFitWithhld, 'Box 10' : liststmltamt } 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,1:-1]
        
        fin_df['Box 3']  =  fin_df['Box 3'].astype(float)  
        xldrow['Box 3']  =  xldrow['Box 3'].astype(float)
        
        fin_df['Box 4 FIT']  =  fin_df['Box 4 FIT'].astype(float)  
        xldrow['Box 4 FIT']  =  xldrow['Box 4 FIT'].astype(float)
        
        fin_df['Box 10']  =  fin_df['Box 10'].astype(float)  
        xldrow['Box 10']  =  xldrow['Box 10'].astype(float)
           
        fin_df = fin_df.applymap(str)
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def MiscReport1099():
    html_logger.info("Start of 1099 Misc Report with Fineos")    
   
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "1099Misc" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)a
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        #print(str(xldf.iloc[i,-1])[:3])
        if(str(xldf.iloc[i,-1])[:3] == 'DI-'):
            caseno = xldf.iloc[i,-1]
            name = xldf.iloc[i,2]
            html_logger.info("Start of Case Number" + str(caseno) + " and the name " + str(name)) 
            MiscReport1099_Fineos(caseno,name,xldf)
            
##################################################################### 

##################################################################### 
def ASOSimpleReport1099_Fineos(caseno,PaymentBeginDate,PaymentEndDate,xldrow1):
    
    PaymentBeginDate = datetime.strptime(str(PaymentBeginDate),'%m/%d/%Y')
    PaymentEndDate = datetime.strptime(str(PaymentEndDate),'%m/%d/%Y')
    
    xldrow = xldrow1[(xldrow1['FINEOS Claim Nbr']==caseno) & (xldrow1['Payment Begin Date'] == PaymentBeginDate.strftime('%m/%d/%Y'))]
    xldrow = xldrow.reset_index(drop=True)
    #xldrow = xldrow.iloc[:,2:]
    #xldrow = xldrow.drop('Policyholder Master Name',1)
    print(xldrow['Employer FICA Amt'][0])
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0)
    
    xldrow['Payment Begin Date'] = pd.to_datetime(xldrow['Payment Begin Date'])
    xldrow['Payment Begin Date'] = xldrow['Payment Begin Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment Date'] = pd.to_datetime(xldrow['Payment Date'])
    xldrow['Payment Date'] = xldrow['Payment Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment End Date'] = pd.to_datetime(xldrow['Payment End Date'])
    xldrow['Payment End Date'] = xldrow['Payment End Date'].dt.strftime('%m/%d/%Y')
    
    #xldrow['Zip Code'] = xldrow['Zip Code'].apply(lambda x: '{0:0>5}'.format(x))
    #xldrow['Box 3'] = xldrow['Box 3'].sum()
    #xldrow['Box 4 FIT'] = xldrow['Box 4 FIT'].sum()
    #xldrow['Box 10'] = xldrow['Box 10'].sum()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
    ssnein = "0.0"
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        time.sleep(3)
        if type in shortTerm:
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
        elif type in longTerm:   
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
        time.sleep(3)
        
        if type in shortTerm:
            polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
        elif type in longTerm:   
            polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
            
        #polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit']/following::strong[text()='Policy Number']/span").text 
        polNo1 = polNo   
        polNo = str(polNo[:8]) + '-' + str(polNo[8:12]) + '-' + str(polNo[12:])
        listPolNo = []
        listPolNo.append(polNo)
        
        #planType = driver.find_element_by_xpath("//strong[text()='Coverage Code']/span").get_attribute('textContent')
        try:
            planType = planType.split(" ")[1]
        except:
            planType = ''
            
        listPlanType = []
        listPlanType.append(planType)
        
        driver.find_element_by_xpath("//button[contains(text(),'All')]").click()
        time.sleep(1)
        driver.find_element_by_xpath("//li[contains(text(),'Organization')]").click()
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(polNo1)
        time.sleep(3)
        PHMstrName = driver.find_element_by_xpath("//strong[text()='" + polNo1 + "']/preceding::div[1]").text
        listPHMstrName = []
        listPHMstrName.append(PHMstrName)
        
        if type in shortTerm:
            driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']").click()
        elif type in longTerm:   
            driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']").click()
        time.sleep(3)
        """ 
       #-----------------------------------
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Payment Plan')][@class='TabOff']").click()
        time.sleep(3)
        
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
        for i in range(rowcnt):
            daterng = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + i + "]/td[2]").text
            if daterng ==PaymentBeginDate.strftime('%m/%d/%Y'):
                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + i + "]/td[1]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@id,'_ViewButton')]").click()
                time.sleep(3)
                payStartDt = driver.find_element_by_xpath("//span[contains(@id,'_periodStartDateBean')]").text
                payEndDt = driver.find_element_by_xpath("//span[contains(@id,'_periodEndDateBean')]").text
                chkEFT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                chkEFT =chkEFT.replace(',','')   
                
                driver.find_element_by_xpath("//div[contains(text(),'Alternate Payee Dues')][@class='TabOff']").click()
                time.sleep(3)
                rowcnt1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[1]/td[6]"))
                if(rowcnt1 > 0):
                    for j in range(rowcnt1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[" + j + "]/td[4]")
                        if adjName == 'Garnishment' or adjName == 'Alimony'  or adjName == 'Child Support' or adjName == 'Tax Levy' or adjName == 'Health & Welfare Premium Deduction' or adjName == 'Pension - 401K Contribution' :
                                driver.find_element_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[" + j + "]/td[4]").click()
                                chkGarnishment = driver.find_element_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[" + j + "]/td[4]").text
                                chkGarnishment = chkGarnishment.replace(",","")
                                chkGarnishment  = '%.2f' %(float(chkGarnishment))
                                #print(chkGarnishment)
                                totalchkGarnishment = float(chkGarnishment) + totalchkGarnishment
                                #print(str(medwithld) + " as medwithld amount")
                        elif adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld' or adjName == 'FIT Amount' or adjName == 'FIT Refund' or adjName == 'Mandatory FIT' or adjName == 'State Income Tax' or adjName == 'SIT Refund':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                            EmployeeTaxAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            EmployeeTaxAmt = EmployeeTaxAmt.replace(",","")
                            EmployeeTaxAmt  = '%.2f' %(abs(float(EmployeeTaxAmt)))
                            #print(chkGarnishment)
                            totalEmployeeTaxAmt = float(EmployeeTaxAmt) + totalEmployeeTaxAmt 
                           
                            if adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld': 
                                FICAamt = FICAamt + float(EmployeeTaxAmt)
                        
                        
                        elif adjName == 'Employer FICA Medicare' or adjName == 'Employer FICA Medicare Refund' or adjName == 'Employer FICA Medicare Underwithheld' or adjName == 'Employer FICA Social Security' or adjName == 'Employer FICA Social Security Refund' or adjName == 'Employer FICA Social Security Withheld':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                            EmployeeFICAAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            EmployeeFICAAmt = EmployeeFICAAmt.replace(",","")
                            EmployeeFICAAmt  = '%.2f' %(float(EmployeeFICAAmt))
                            #print(chkGarnishment)
                            totalEmployeeFICAAmt = float(EmployeeFICAAmt) + totalEmployeeFICAAmt   
                        
                        if totalEmployeeFICAAmt == 0:
                            totalEmployeeFICAAmt = totalEmployeeFICAAmt + FICAamt
                            
                        totalLiabAmt = float(totalchkEFT) +  float(totalchkGarnishment) + float(totalEmployeeTaxAmt) +  float(totalEmployeeFICAAmt)      
                        time.sleep(3)        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
        #------------------------------------
                        """
        
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text  #xldrow['Name'].iloc[0] #
        fname = name.split(' ')[0]
        lname = name.replace(fname,"").strip()#name.split(' ')[-1]
        listFName = []
        listLName = []
        listFName.append(str(fname))
        listLName.append(str(lname))
        

        listCaseNo = []
        listCaseNo.append(str(caseno))
        
        
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        
        totalchkGarnishment = 0   
        totalEmployeeTaxAmt = 0  
        FICAamt = 0
        totalEmployeeFICAAmt = 0 
        chkEFT = 0
        listchkEFT = []        
        listchkGarnishment = [] 
        listEmployeeTaxAmt = []
        listEmployeeFICAAmt = []
        listLiabAmt = []
        listpayStartDt = []
        listpayEndDt = []
        listtransDate = []
        
        totalchkGarnishment = 0   
        totalEmployeeTaxAmt = 0  
        totalEmployeeFICAAmt = 0
        totalLiabAmt = 0  
        totalchkEFT = 0              
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            rowAvlbl = 0
            
            for i in range(1,rowcnt+1):
                time.sleep(5)
                strtdate1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                #enddate1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                #d1 = date(date1)
                #d2 = date(PaymentBeginDate)
                #d3 = date(PaymentEndDate)
                
                date_format = "%Y-%m-%d %H:%M:%S"
                date_format1 = "%m/%d/%Y"
                """
                a = datetime.strptime(str(PaymentBeginDate), date_format)
                b = datetime.strptime(strtdate1, date_format1) # Date to be checked
                c = datetime.strptime(str(PaymentEndDate), date_format)
                d = datetime.strptime(str(PaymentBeginDate), date_format)  #Date entered here should always be the same as 'a'
                delta1 = b - a
                delta2 = c - b
                delta3 = d - a
                """
                strtDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                endDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                
                a = datetime.strptime(str(strtDt), date_format1)
                b = datetime.strptime(str(PaymentBeginDate), date_format) # Date to be checked
                c = datetime.strptime(str(endDt), date_format1)
                d = datetime.strptime(str(strtDt), date_format1)  #Date entered here should always be the same as 'a'
                delta1 = b - a
                delta2 = c - b
                delta3 = d - a
                
                
                if((driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text == PaymentBeginDate.strftime('%m/%d/%Y')) and (driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text == PaymentEndDate.strftime('%m/%d/%Y'))) :
                    rowAvlbl = 1
                    payStartDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                    payEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                    
                    time.sleep(3)
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    #--------------------
                    transDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')]").text
                    chkEFT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                    chkEFT =chkEFT.replace(',','')
                    #------------------
                    driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                    time.sleep(3) 
                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                    if(rowcnt2 == 0):
                        chkEFT = 0 
                        totalchkEFT = 0                       
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                    else:  
                         totalchkEFT = totalchkEFT + float(chkEFT)                    
                        #for j in range(1,rowcnt2+1):
                        #    adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        #   driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        #    #if(adjName == 'Auto Gross Entitlement' or adjName == 'Main Payment Line'):
                        #    chkEFT = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                        #   chkEFT = float(chkEFT.replace(',',''))
                        #   totalchkEFT = totalchkEFT + chkEFT
                                                   
                    totalchkGarnishment = 0
                    totalEmployeeTaxAmt = 0
                    totalEmployeeFICAAmt = 0
                    FICAamt = 0
                    
                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                    if rowcnt2 == 0:
                        #chkGarnishment = 0 
                        totalchkGarnishment = 0   
                        totalEmployeeTaxAmt = 0  
                        FICAamt = 0
                        totalEmployeeFICAAmt = 0                  
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3) 
                    else:    
                        for j in range(1,rowcnt2+1):
                            adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                            if adjName == 'Garnishment' or adjName == 'Alimony'  or adjName == 'Child Support' or adjName == 'Tax Levy' or adjName == 'Health & Welfare Premium Deduction' or adjName == 'Pension - 401K Contribution' :
                                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                chkGarnishment = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                chkGarnishment = chkGarnishment.replace(",","")
                                chkGarnishment  = '%.2f' %(float(chkGarnishment))
                                #print(chkGarnishment)
                                totalchkGarnishment = float(chkGarnishment) + totalchkGarnishment
                                #print(str(medwithld) + " as medwithld amount")
                            elif adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld' or adjName == 'FIT Amount' or adjName == 'FIT Refund' or adjName == 'Mandatory FIT' or adjName == 'State Income Tax' or adjName == 'SIT Refund':
                                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                EmployeeTaxAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                EmployeeTaxAmt = EmployeeTaxAmt.replace(",","")
                                EmployeeTaxAmt  = '%.2f' %(abs(float(EmployeeTaxAmt)))
                                #print(chkGarnishment)
                                totalEmployeeTaxAmt = float(EmployeeTaxAmt) + totalEmployeeTaxAmt 
                               
                                if adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld': 
                                    FICAamt = FICAamt + float(EmployeeTaxAmt)
                            
                            
                            elif adjName == 'Employer FICA Medicare' or adjName == 'Employer FICA Medicare Refund' or adjName == 'Employer FICA Medicare Underwithheld' or adjName == 'Employer FICA Social Security' or adjName == 'Employer FICA Social Security Refund' or adjName == 'Employer FICA Social Security Withheld':
                                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                EmployeeFICAAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                EmployeeFICAAmt = EmployeeFICAAmt.replace(",","")
                                EmployeeFICAAmt  = '%.2f' %(float(EmployeeFICAAmt))
                                #print(chkGarnishment)
                                totalEmployeeFICAAmt = float(EmployeeFICAAmt) + totalEmployeeFICAAmt   
                        
                         
                        if totalEmployeeFICAAmt == 0:
                            if  xldrow['Employer FICA Amt'][0] == 0:
                                totalEmployeeFICAAmt = 0
                            else:    
                                totalEmployeeFICAAmt = totalEmployeeFICAAmt + FICAamt
                            
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)  
                    totalchkGarnishment = abs(totalchkGarnishment)      
                    
                    totalLiabAmt = float(totalchkEFT) +  float(totalchkGarnishment) + float(totalEmployeeTaxAmt) +  float(totalEmployeeFICAAmt)      
                    time.sleep(3)        
                    
                            
                    listchkEFT.append(str('%.2f' %(float(totalchkEFT))))         
                    listchkGarnishment.append(str('%.2f' %(float(totalchkGarnishment)))) 
                    listEmployeeTaxAmt.append(str('%.2f' %(float(totalEmployeeTaxAmt))))
                    listEmployeeFICAAmt.append(str('%.2f' %(float(totalEmployeeFICAAmt)))) 
                    listLiabAmt.append(str('%.2f' %(float(totalLiabAmt))))
                    
                    listpayStartDt.append(str(payStartDt))
                    listpayEndDt.append(str(payEndDt))
                    listtransDate.append(str(transDate))
                    
                    xldrow['Garnishment Amt'] = str('%.2f' %(float( xldrow['Garnishment Amt'])))
                    xldrow['Employee Tax Amt'] = str('%.2f' %(float( xldrow['Employee Tax Amt']))) 
                    xldrow['Employer FICA Amt'] = str('%.2f' %(float( xldrow['Employer FICA Amt']))) 
                    xldrow['Liability Amt'] = str('%.2f' %(float( xldrow['Liability Amt']))) 
                    xldrow['Check EFT Amt'] = str('%.2f' %(float( xldrow['Check EFT Amt'])))  
        #except:
        #   pass
        
                    dict = {'Policyholder Nbr':listPolNo, 'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo,'Last Name':listLName, 'First Name':listFName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Liability Amt':listLiabAmt,'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt} #'Policyholder Master Name':listPHMstrName,
                    fin_df = pd.DataFrame(dict)                  
                    fin_df = fin_df.applymap(str)
                    #xldrow = xldrow.iloc[:,:-1]
                    xldrow = xldrow.applymap(str)
                    
                    if xldrow.equals(fin_df):
                        html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
                    elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
                        html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                    elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
                        html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                    else:
                        html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                                               
                elif((delta1.days >= delta3.days) and (delta2.days >= delta3.days)):                   
                    rowAvlbl = 1
                    
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    
                    #payStartDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                   #payEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                    driver.find_element_by_xpath("//div[contains(text(),'Payment Allocations')][@class='TabOff']").click()
                    time.sleep(3)
                    itmfnd = 0
                    try:
                        rowcntpg = len(driver.find_elements_by_xpath("//span[contains(@id,'_AllocatedDuesListView_blockNumber_')]"))
                    except:
                        rowcntpg = 1
                    if rowcntpg == 0:
                        rowcntpg =1
                                
                    for xx in range(1,rowcntpg+1):
                        rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr"))
                        k = 1
                        for k in range(1,rowcnt2+1):
                            
                            if((driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[1]").text == PaymentBeginDate.strftime('%m/%d/%Y')) and (driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[2]").text == PaymentEndDate.strftime('%m/%d/%Y'))) :
                                payStartDt = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[1]").text
                                payEndDt = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[2]").text
                                 
                                driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[1]").click()
                                time.sleep(3)
                                driver.find_element_by_xpath("//input[contains(@name,'cmdView')]").click()
                                time.sleep(3)
                                chkEFT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                                chkEFT =chkEFT.replace(',','')
                                totalchkEFT = chkEFT
                                
                                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr"))
                                if rowcnt2 == 0:
                                    #chkGarnishment = 0 
                                    totalchkGarnishment = 0   
                                    totalEmployeeTaxAmt = 0  
                                    FICAamt = 0
                                    totalEmployeeFICAAmt = 0                  
                                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                                    time.sleep(3) 
                                else:    
                                    for j in range(1,rowcnt2+1):
                                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                                        if adjName == 'Garnishment' or adjName == 'Alimony'  or adjName == 'Child Support' or adjName == 'Tax Levy' or adjName == 'Health & Welfare Premium Deduction' or adjName == 'Pension - 401K Contribution' :
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            chkGarnishment = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            chkGarnishment = chkGarnishment.replace(",","")
                                            chkGarnishment  = '%.2f' %(float(chkGarnishment))
                                            totalchkGarnishment = float(chkGarnishment) + totalchkGarnishment
                                            #print(str(medwithld) + " as medwithld amount")
                                        elif adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld' or adjName == 'FIT Amount' or adjName == 'FIT Refund' or adjName == 'Mandatory FIT' or adjName == 'State Income Tax' or adjName == 'SIT Refund':
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            EmployeeTaxAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            EmployeeTaxAmt = EmployeeTaxAmt.replace(",","")
                                            EmployeeTaxAmt  = '%.2f' %(abs(float(EmployeeTaxAmt)))
                                            #print(chkGarnishment)
                                            totalEmployeeTaxAmt = float(EmployeeTaxAmt) + totalEmployeeTaxAmt 
                                           
                                            if adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld': 
                                                FICAamt = FICAamt + float(EmployeeTaxAmt)
                                        
                                        
                                        elif adjName == 'Employer FICA Medicare' or adjName == 'Employer FICA Medicare Refund' or adjName == 'Employer FICA Medicare Underwithheld' or adjName == 'Employer FICA Social Security' or adjName == 'Employer FICA Social Security Refund' or adjName == 'Employer FICA Social Security Withheld':
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            EmployeeFICAAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            EmployeeFICAAmt = EmployeeFICAAmt.replace(",","")
                                            EmployeeFICAAmt  = '%.2f' %(float(EmployeeFICAAmt))
                                            #print(chkGarnishment)
                                            totalEmployeeFICAAmt = float(EmployeeFICAAmt) + totalEmployeeFICAAmt   
                                    
                                    if totalEmployeeFICAAmt == 0:
                                        if  xldrow['Employer FICA Amt'][0] == 0:
                                            totalEmployeeFICAAmt = 0
                                        else:    
                                            totalEmployeeFICAAmt = totalEmployeeFICAAmt + FICAamt
                                    
                                    totalchkGarnishment1 = abs(totalchkGarnishment) 
                                    totalLiabAmt = float(totalchkEFT) +  float(totalchkGarnishment1) + float(totalEmployeeTaxAmt) +  float(totalEmployeeFICAAmt)      
                                    time.sleep(3)
                                    itmfnd  =1  
                                    break
                        if itmfnd == 0:
                             try:
                                 driver.find_element_by_xpath("//a[contains(@name,'AllocatedDuesListView_cmdNext')]").click()
                             except:
                                 html_logger.err("No dates found")           
                     #page for    
                    try:         
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                         
                        driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                        time.sleep(3) 
                        transDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')]").text
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                         
                        listchkEFT.append(str('%.2f' %(float(totalchkEFT))))         
                        listchkGarnishment.append(str('%.2f' %(float(totalchkGarnishment1)))) 
                        listEmployeeTaxAmt.append(str('%.2f' %(float(totalEmployeeTaxAmt))))
                        listEmployeeFICAAmt.append(str('%.2f' %(float(totalEmployeeFICAAmt)))) 
                        listLiabAmt.append(str('%.2f' %(float(totalLiabAmt))))
                        listpayStartDt.append(str(payStartDt))
                        listpayEndDt.append(str(payEndDt))
                        listtransDate.append(str(transDate))
                        
                        xldrow['Garnishment Amt'] = str('%.2f' %(float( xldrow['Garnishment Amt'])))
                        xldrow['Employee Tax Amt'] = str('%.2f' %(float( xldrow['Employee Tax Amt']))) 
                        xldrow['Employer FICA Amt'] = str('%.2f' %(float( xldrow['Employer FICA Amt']))) 
                        xldrow['Liability Amt'] = str('%.2f' %(float( xldrow['Liability Amt']))) 
                        xldrow['Check EFT Amt'] = str('%.2f' %(float( xldrow['Check EFT Amt'])))  
             #excep:
             #   pass
             
                        dict = {'Policyholder Nbr':listPolNo, 'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo,'Last Name':listLName, 'First Name':listFName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Liability Amt':listLiabAmt,'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt} #'Policyholder Master Name':listPHMstrName,
                        fin_df = pd.DataFrame(dict)                  
                        fin_df = fin_df.applymap(str)
                         #xldrow = xldrow.iloc[:,:-1]
                        xldrow = xldrow.applymap(str)
                         
                        if xldrow.equals(fin_df):
                            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
                        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
                            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
                            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                         
                        else:
                             html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())

                    except:
                         pass       
            """listchkEFT.append(str('%.2f' %(float(totalchkEFT))))         
                listchkGarnishment.append(str('%.2f' %(float(totalchkGarnishment)))) 
                listEmployeeTaxAmt.append(str('%.2f' %(float(totalEmployeeTaxAmt))))
                listEmployeeFICAAmt.append(str('%.2f' %(float(totalEmployeeFICAAmt)))) 
                listLiabAmt.append(str('%.2f' %(float(totalLiabAmt))))
                listpayStartDt.append(str(payStartDt))
                listpayEndDt.append(str(payEndDt))
                listtransDate.append(str(transDate))
                
                xldrow['Garnishment Amt'] = str('%.2f' %(float( xldrow['Garnishment Amt'])))
                xldrow['Employee Tax Amt'] = str('%.2f' %(float( xldrow['Employee Tax Amt']))) 
                xldrow['Employer FICA Amt'] = str('%.2f' %(float( xldrow['Employer FICA Amt']))) 
                xldrow['Liability Amt'] = str('%.2f' %(float( xldrow['Liability Amt']))) 
                xldrow['Check EFT Amt'] = str('%.2f' %(float( xldrow['Check EFT Amt'])))  
    #except:
    #   pass
    
                dict = {'Policyholder Nbr':listPolNo, 'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo,'Last Name':listLName, 'First Name':listFName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Liability Amt':listLiabAmt,'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt} #'Policyholder Master Name':listPHMstrName,
                fin_df = pd.DataFrame(dict)                  
                fin_df = fin_df.applymap(str)
                #xldrow = xldrow.iloc[:,:-1]
                xldrow = xldrow.applymap(str)
                
                if xldrow.equals(fin_df):
                    html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
                elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
                    html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
                    html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                
                else:
                    html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
            """  
            if rowAvlbl == 0:
                html_logger.err("Payment record  with that date range is not available")
                    
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def ASOSimpleReport1099():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of ASO SimpleTable Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ASOSimple" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
   #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)a
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        #print(str(xldf.iloc[i,-1])[:3])
        if(str(xldf.iloc[i,2])[:3] == 'DI-'):
            caseno = xldf.iloc[i,2]
            PaymentBeginDate = xldf.iloc[i,6]
            PaymentEndDate = xldf.iloc[i,7]
            html_logger.info("Start of Case Number" + str(caseno) + " and the PaymentBeginDate " + str(PaymentBeginDate) + " and the PaymentEndDate " + str(PaymentEndDate)) 
            ASOSimpleReport1099_Fineos(caseno,PaymentBeginDate,PaymentEndDate,xldf)
            
##################################################################### 

##################################################################### 
def ReconcileEmpTax_NoW2_8922_Fineos(caseno,xldrow):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listTotamt = []
            listmedwithld = []
            listsswithld = []
            listmedTotamt = []
            listssTotamt = []
            listmedmatch = []
            listssmatch=[]
            listRRT1Withheld = []
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listmedwithld.append(str('0')) 
                    listsswithld.append(str('0')) 
                    listmedTotamt.append(str('0')) 
                    listssTotamt.append(str('0')) 
                    listmedmatch.append(str('0'))
                    listssmatch.append(str('0'))
                    listRRT1Withheld.append(str('0'))
                    
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        if adjName == 'FICA Medicare':
                            medwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            medwithld  = '%.2f' %(float(medwithld))
                            medwithld = abs(float(medwithld))
                            #print(str(medwithld) + " as medwithld amount")
                            listmedwithld.append(str('%.2f' %(float(medwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            medTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            medTotamt = medTotamt.replace(",","")
                            
                            if '.00' in medTotamt:
                                listmedTotamt.append(str(medTotamt.replace('.00','')))
                            else:
                                listmedTotamt.append(str('%.2f' %(float(medTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            """if xldrow['FICA Match Exists'].iloc[0] == 'Y':
                                medmatch = float(medwithld)
                            else:
                                medmatch = 0
                            listmedmatch.append(str('%.2f' %(float(medmatch))))
                            """
                                    
                        elif adjName == 'FICA Social Security':
                            sswithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            sswithld  = '%.2f' %(float(sswithld))
                            sswithld = abs(float(sswithld))
                            #print(str(sswithld) + " as sswithld amount")
                            listsswithld.append(str('%.2f' %(float(sswithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            ssTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            ssTotamt = ssTotamt.replace(",","")
                            
                            if '.00' in ssTotamt:
                                listssTotamt.append(str(ssTotamt.replace('.00','')))
                            else:
                                listssTotamt.append(str('%.2f' %(float(ssTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            '''if xldrow['FICA Match Exists'].iloc[0] == 'Y':
                                ssmatch = float(sswithld)
                            else:
                                ssmatch = 0
                            listssmatch.append(str('%.2f' %(float(ssmatch))))'''
                            
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listRRT1Withheld.append("0")
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD Medicare Wages': listmedTotamt,'QTD Medicare Withheld': listmedwithld, 'QTD Soc Sec Wages': listssTotamt, 'QTD Soc Sec Withheld': listsswithld,'FIT Wages': listssTotamt,'RRT1 Withheld':listRRT1Withheld} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,2:]
        
        fin_df['QTD Medicare Wages']  =  fin_df['QTD Medicare Wages'].astype(float)  
        xldrow['QTD Medicare Wages']  =  xldrow['QTD Medicare Wages'].astype(float)
        
        fin_df['QTD Soc Sec Wages']  =  fin_df['QTD Soc Sec Wages'].astype(float)  
        xldrow['QTD Soc Sec Wages']  =  xldrow['QTD Soc Sec Wages'].astype(float)
        
        fin_df['QTD Medicare Withheld']  =  fin_df['QTD Medicare Withheld'].astype(float)  
        xldrow['QTD Medicare Withheld']  =  xldrow['QTD Medicare Withheld'].astype(float)
        
        fin_df['QTD Soc Sec Withheld']  =  fin_df['QTD Soc Sec Withheld'].astype(float)  
        xldrow['QTD Soc Sec Withheld']  =  xldrow['QTD Soc Sec Withheld'].astype(float)
        
        xldrow['FIT Wages']  =  xldrow['FIT Wages'].astype(float)
        fin_df['FIT Wages']  =  fin_df['FIT Wages'].astype(float)
        
        xldrow['RRT1 Withheld'] = xldrow['RRT1 Withheld'].astype(float)
        fin_df['RRT1 Withheld'] = fin_df['RRT1 Withheld'].astype(float)
        
        #fin_df = fin_df.iloc[:2,:]
             
        fin_df['QTD Medicare Wages'].iloc[0] = fin_df['QTD Medicare Wages'].sum()
        fin_df['QTD Soc Sec Wages'].iloc[0] = fin_df['QTD Soc Sec Wages'].sum()
        fin_df['QTD Medicare Withheld'].iloc[0] = fin_df['QTD Medicare Withheld'].sum()
        fin_df['QTD Soc Sec Withheld'].iloc[0] = fin_df['QTD Soc Sec Withheld'].sum()
        fin_df['FIT Wages'].iloc[0] = fin_df['FIT Wages'].sum()
        fin_df['RRT1 Withheld'].iloc[0] = fin_df['RRT1 Withheld'].sum()
        
       
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[0:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def ReconcileEmpTax_NoW2_8922():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 8922 ReconcileEmpTax_NoW2 Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ReconcileEmpTax_NoW2" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,4].unique()
    print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            ReconcileEmpTax_NoW2_8922_Fineos(caseno,xldf)
            
#############################################################
##################################################################### 
def ClaimantEOB_Fineos(caseno,xldrow, reportx):
    fullPayment = ''
    fullPayment1 = ''
    itmfnd = 0
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow.reset_index(drop = True)
   
    #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
    assignedTo = ""
    date = xldrow.iat[0,9]
    name = xldrow.iat[0,3].split("\n")[0].strip()   #xldrow.iat[0,0].strip() #   for AlttEOB
    #name = xldrow.iat[0,0].strip()
    name = name.replace("  "," ")
    print(name)
    
    #xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        
        #name1 = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        #driver.find_element_by_xpath("//span[contains(@id,'_PaymentDateFrom')]").click()
        #time.sleep(3)
        #driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").clear()
        #driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").send_keys("01/01/2021")
        try:
            driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[contains(text(),'" + name + "')]").click()
        except:
             pass   
        time.sleep(3)  
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        
        date = xldrow.iloc[0][9]
        startdate = date.split("-")[0].strip()
        enddate = date.split("-")[1].strip()
                
        driver.find_element_by_xpath("//a[contains(@title,'Filter by:Period Start Date')]").click()
        time.sleep(2)
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodStartDa_startDateFilter')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodStartDa_startDateFilter')]").send_keys(startdate)
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodStartDa_startDateFilter')]/following::input[contains(@id,'_PaymentHistoryDetailsListview_cmdFilter')][1]").click()
        time.sleep(5)
        
        driver.find_element_by_xpath("//a[contains(@title,'Filter by:Period End Date')]").click()
        time.sleep(2)
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodEndDate_endDateFilter')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodEndDate_endDateFilter')]").send_keys(enddate)
        driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodEndDate_endDateFilter')]/following::input[contains(@id,'_PaymentHistoryDetailsListview_cmdFilter')][1]").click()
        time.sleep(5)
        
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listTotamt = []
            listmedwithld = []
            listsswithld = []
            listmedTotamt = []
            listssTotamt = []
            listmedmatch = []
            listssmatch=[]
            listRRT1Withheld = []
            for i in range(1,rowcnt+1):
                date = xldrow.iloc[0][9]
                startdate = date.split("-")[0].strip()
                enddate = date.split("-")[1].strip()
                
                finStrtDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[2]").text
                finEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[3]").text
                if finEndDt == '-':
                    finEndDt = '01/01/1900'
                if finStrtDt == '-':
                    finStrtDt = '01/01/1900'    
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                
                amount = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").text
                xlAmount = xldrow.iat[0,8].split("//")[1].strip()
#except:        
                amount  = amount.replace(",","")
                amount = str('%.2f' %(float(amount)))
                xlAmount  = xlAmount.replace(",","")
                            
                if(datetime.strptime(startdate,'%m/%d/%Y') == datetime.strptime(finStrtDt,'%m/%d/%Y') and datetime.strptime(enddate,'%m/%d/%Y') == datetime.strptime(finEndDt,'%m/%d/%Y')  and reportx == 'EOB'):     
                    time.sleep(3)
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(5)
                    try:
                            if(len(driver.find_elements_by_xpath("//span[contains(@name,'_payeeAddress')]"))> 0):
                                address = driver.find_element_by_xpath("//span[contains(@name,'_payeeAddress')]").text
                            elif(len(driver.find_elements_by_xpath("//span[contains(@id,'_payeeAddress')]"))> 0):  
                                address = driver.find_element_by_xpath("//span[contains(@id,'_payeeAddress')]").text 
                            else:
                                 address = ''   
                    except:
                        address = ''
                    assignedTo1 = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
                    assignedTo =  assignedTo1.split("(")[0]
                    chkDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')][@class='DataLabel']").text
                    payMtd = driver.find_element_by_xpath("//span[contains(@id,'_paymentMethodDropDown')][@class='DataLabel']").text
                    name = driver.find_element_by_xpath("//span[contains(@id,'_payeeName')]").text
                    name = name.replace("Mr ","")
                    nomPayee = driver.find_element_by_xpath("//span[contains(@Id,'_nominatedPayee')]").text
                    if nomPayee == '-':
                        name = name
                    else:
                        name = nomPayee
                                        
                    TotPayment = driver.find_element_by_xpath("//span[contains(@id,'_NetPaymentAmount')]").text
                    TotPayment = TotPayment.replace(",","")
                                                            
                    driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                    time.sleep(3) 
                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                    if(rowcnt2 == 0):                       
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                    else:
                        fullPayment = ""
                        fullPayment1 = ""
                        for j in range(1,rowcnt2+1):
                            adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                            if adjName == 'Auto Gross Entitlement':
                                adjName = 'Gross Benefit'
                                adjName1 = adjName 
                            elif adjName == 'COLA Summary Auto Generated Adjustment Type':
                                adjName = 'COLA Summary Adjustment Type' 
                                adjName1 = 'COLA  Adjustment' 
                            #elif adjName == 'COLA Summary Adjustment Type':  DI-1131 fails if we have this condition
                                #adjName = 'COLA  Adjustment'      
                            elif adjName == 'FIT Amount':
                                adjName = 'Federal Income Tax'  
                                adjName1 = adjName 
                            else:
                                 adjName1 = adjName   
                                 
                            amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            amount  = amount.replace(",","")
                            amount = str('%.2f' %(float(amount)))
                            
                            payment = adjName + "//" + str(amount)  
                            payment1 = adjName1 + "//" + str(amount)  
                            
                            if j ==1:
                                fullPayment = payment.replace(",","")
                                fullPayment1 = payment1.replace(",","")
                            else:
                                fullPayment = fullPayment + ";;" + payment.replace(",","")
                                fullPayment1 = fullPayment1 + ";;" + payment1.replace(",","")
                                    
                        rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
                        if rowcnt2 > 0:
                               for j in range(1,rowcnt2+1):
                                   adjName = driver.find_element_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + str(j) + "]/td[1]").text
                                   if adjName == 'FIT Amount':
                                       adjName = 'Federal Income Tax' 
                                       adjName1 = adjName
                                   elif adjName == 'FICA Medicare':
                                       adjName1 = adjName #'RRTM'
                                   elif adjName == 'FICA Social Security':
                                       adjName1 = adjName #'RRT1'      
                                   amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                   amount  = amount.replace(",","")
                                   amount = str('%.2f' %(float(amount)))       
                                   payment = adjName + "//" + str(amount)    
                                   payment1 = adjName1 + "//" + str(amount)  
                                   
                                   fullPayment = fullPayment + ";;" + payment.replace(",","")
                                   fullPayment1 = fullPayment1 + ";;" + payment1.replace(",","")
                        time.sleep(3)        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                        break
                elif reportx != 'EOB':
                    #fullPayment = ''
                    rowcnt1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr"))
                    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
                    if rowcnt>0:
                        for x in range(1, rowcnt+1):
                            #finStrtDt1 = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(x) + "]/td[1]").text
                            #finendDt1 = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(x) + "]/td[2]").text
                            
                            finStrtDt1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(x) + "]/td[2]").text
                            finendDt1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(x) + "]/td[3]").text
                            type1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(x) + "]/td[4]").text
                            name1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(x) + "]/td[8]").text
                            
                            if finendDt1 == '-':
                                finendDt1 = '01/01/1900'
                            if finStrtDt1 == '-':
                                finStrtDt1 = '01/01/1900'
                                
                            #amount = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(x) + "]/td[4]").text
                            #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
        #except:        
                            #amount  = amount.replace(",","")
                            #amount = str('%.2f' %(float(amount)))
                            #xlAmount  = xlAmount.replace(",","")
                            #xlAmount = str('%.2f' %(float(xlAmount)))
                                            
                            #if datetime.strptime(startdate,'%m/%d/%Y') == datetime.strptime(finStrtDt1,'%m/%d/%Y') and datetime.strptime(enddate,'%m/%d/%Y') == datetime.strptime(finendDt1,'%m/%d/%Y') : #and xlAmount == amount  :
                            if type1 == 'ScheduledAlternate'  and name1.upper() == name.upper():     
                                    time.sleep(3)
                                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(x) +"]").click()
                                    time.sleep(3)
                                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                                    time.sleep(5)
                                    try:
                                        address = driver.find_element_by_xpath("//span[contains(@name,'_payeeAddress')]").text
                                    except:
                                        address = ''
                                        
                                    if len(driver.find_elements_by_xpath("//img[contains(@name,'CaseOwnershipSummary')][contains(@src,'plus.png')]"))>0:
                                        driver.find_element_by_xpath("//img[contains(@name,'CaseOwnershipSummary')][contains(@src,'plus.png')]").click()

                                    time.sleep(3)
                                    assignedTo1 = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
                                    assignedTo =  assignedTo1.split("(")[0]
                                    chkDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')][@class='DataLabel']").text
                                    payMtd = driver.find_element_by_xpath("//span[contains(@id,'_paymentMethodDropDown')][@class='DataLabel']").text
                                    name = driver.find_element_by_xpath("//span[contains(@id,'_payeeName')]").text
                                    nomPayee = driver.find_element_by_xpath("//span[contains(@Id,'_nominatedPayee')]").text
                                    if nomPayee == '-':
                                        name = name
                                    else:
                                        name = nomPayee    
                                        
                                    TotPayment = driver.find_element_by_xpath("//span[contains(@id,'_NetPaymentAmount')]").text
                                    TotPayment = TotPayment.replace(",","")
                                    #address = driver.find_element_by_xpath("//span[contains(@id,'_payeeName')]").text
                                                                            
                                    driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                                    time.sleep(3) 
                                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                                    if(rowcnt2 == 0):                       
                                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                                        time.sleep(3)
                                    else:
                                        fullPayment = ""
                                        for j in range(1,rowcnt2+1):
                                            adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                                            amount  = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            if adjName == 'Auto Gross Entitlement':
                                                adjName = 'Gross Benefit'
                                            elif adjName == 'COLA Summary Auto Generated Adjustment Type':
                                                adjName = 'COLA Summary Adjustment Type' 
                                            elif adjName == 'FIT Amount':
                                                adjName = 'Federal Income Tax' 
                                            #elif adjName == 'COLA Summary Adjustment Type':   DI-1131 fails if we have this condition
                                             #   adjName = 'COLA  Adjustment'     
                                            #amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            
                                            
                                            payment = adjName + "//" + str(amount)    
                                            if j ==1:
                                                fullPayment = payment.replace(",","")
                                            else:
                                                fullPayment = fullPayment + ";;" + payment.replace(",","")
                                                    
                                        rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
                                        if rowcnt2 > 0:
                                               for j in range(1,rowcnt2+1):
                                                   adjName = driver.find_element_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + str(j) + "]/td[1]").text
                                                   if adjName == 'FIT Amount':
                                                       adjName = 'Federal Income Tax'
                                                
                                                   amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                                   amount  = amount.replace(",","")
                                                   amount = str('%.2f' %(float(amount)))       
                                                   payment = adjName + "//" + str(amount)    
                                                   fullPayment = fullPayment + ";;" + payment.replace(",","")
                                                
                                        time.sleep(3)        
                                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                                        time.sleep(3)
                                        itmfnd = 1
                                        break
        #   pass                    break
                    if itmfnd == 1:
                        itmfnd = 0
                        break
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
        time.sleep(9)
        try:
            PolicyHolder = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyDivisionsListview')]/tbody/tr[1]/td[2]").text
        except:
            PolicyHolder =  ''
                
        driver.find_element_by_xpath("//input[contains(@name,'editPageCancel')]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        time.sleep(3)
        
        insname = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        insname = insname.replace("Mr ","")
        #insname = insname.split(" ")[0] + " " + insname.split(" ")[-1]
        insname = insname.strip()
        try:
            address = address.replace("\\n","\n")
        except:
             address = ''   
        #address = name.replace("Mr","") + "\n" + address
        address = name + "\n" + address
        address = address.replace(" ","").replace("\n","").replace(" ","").upper()
        
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listinsName = []
        listName = []
        listaddress = []
        listChkDt = []
        listcaseno = []
        listProcessor = []
        listChkDt = []
        listpayMtd = []
        listPolicyHolder = []
        listPayment = []
        listFinPayment = []
        
        for i in range(1,rowcnt+1):
            listinsName.append(insname.strip())
            listName.append(name.strip())
            listcaseno.append(caseno)
            listaddress.append(address.replace("\\n",""))
            listProcessor.append(assignedTo.strip())
            listChkDt.append(chkDate)
            listpayMtd.append(payMtd)
            listPolicyHolder.append(PolicyHolder)
            listPayment.append(fullPayment)
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Insured Name': listinsName, 'Claim Number': caseno, 'Policy Holder': listPolicyHolder, 'address': listaddress,'Processor': listProcessor, 'Check Date': listChkDt, 'Source Code': listpayMtd}  #, 'Payment': listPayment 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        
        #fullPayment = str('%.2f' %(float(fullPayment)))
        #xldrow['Payment'] = xldrow['Payment'].astype(float)
        try:
            xldrow['Check Date'] = xldrow['Check Date'].dt.strftime('%m/%d/%Y')
        except:
            xldrow['Check Date'] = ''
        listFinPayment = fullPayment.split(";;") 
        listFinPayment1 = fullPayment1.split(";;") 
        listXLPayment = xldrow['Payment'].str.split(";;")
        

        
        if (Counter(listXLPayment.tolist()[0]) == Counter(listFinPayment)) or (Counter(listXLPayment.tolist()[0]) == Counter(listFinPayment1)):
            html_logger.dbg("Payment match - The payment values are " + fullPayment)
        elif (all(x in listFinPayment for x in listXLPayment.tolist()[0])) or (all(x in listFinPayment1 for x in listXLPayment.tolist()[0])):    
            html_logger.dbg("Payment subset match - The payment values are " + str(listFinPayment) + " and XL values are " + str(listXLPayment.tolist()[0]))
        else:
            html_logger.err("Payment doesn't match - Extract has \n" + xldrow['Payment'].to_string() + "\n but Fin has \n" + fullPayment)    
        
        if float(TotPayment) == float(xldrow.iat[0,-1]):
            html_logger.dbg("Total Payment match - The payment values are " + TotPayment)
        else:
            html_logger.err("Total Payment doesn't match - Extract has \n" + str(xldrow.iat[0,-1]) + "\n but Fin has \n" + str(TotPayment))   
            
        xldrow = xldrow.drop(['Reference ID'],axis=1)
        xldrow = xldrow.drop(['Dates'],axis=1)
        xldrow = xldrow.drop(['Payment'],axis=1)
        xldrow = xldrow.drop(['PaymentTotal'],axis=1)
        
        xldrow['address'] = xldrow['address'].str.replace(" ","").str.replace("\\n","").str.replace(" ","").str.upper()
        xldrow.reset_index(drop = True)

        
        #xldrow = xldrow.iloc[2:,:]
        
        #xldrow['Check Date'] = xldrow['Check Date'].str.split(" ").str[0]
        
        #fin_df = fin_df.iloc[:2,:]
        xldrow = xldrow.applymap(str)
        fin_df = fin_df.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
def ClaimantEOB():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of ClaimantEOB  Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ClaimEOB" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname, skiprows=[0])
    xldf1 = xldf.iloc[:,1].unique()
    print(xldf1)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            ClaimantEOB_Fineos(caseno,xldf,'AltEOB')     #allowed values =  AltEOB, EOB
###################################################################################
def VendorEOB_Fineos(name,xldrow):
    xldrow = xldrow[xldrow['Name']==name]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow.reset_index(drop = True)
   
    #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
   
    date = xldrow.iat[0,3]
    name = xldrow.iat[0,0].strip()
    name = name.replace("  "," ")
    #print(name)
    
    driver.find_element_by_xpath("//a[contains(@name,'_MENUITEM.SearchPartieslink')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//input[contains(@id,'Organisation_GROUP')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//input[contains(@id,'_Name')]").send_keys(name)
    driver.find_element_by_xpath("//input[contains(@id,'_searchButton')]").click()
    time.sleep(10)
    
    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PartySearchResultListviewWidget')]/tbody/tr"))
    if rowcnt > 0 :
        driver.find_element_by_xpath("//input[contains(@id,'_searchPageOk_cloned')]").click()
        time.sleep(5)
        try:
            address = driver.find_element_by_xpath("//span[contains(@name,'_Address')]").text
            address = address.rsplit("\n",1)[0]
            address = address.replace('\n',"").replace(",","").replace(" ","")
            if address[0,2].upper() == 'MR':
               address = address.replace("Mr","",1)  
        except:
            address = ""
        #print(address)
        
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(3)
        allAmts = ''
        rowcntx = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        for i in range(1,rowcntx+1):
            if float(driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").text) == float(xldrow.iat[0,2]):
                totalamt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").text
                #print(totalamt)
                
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").click()
                time.sleep(5)
                rowcnty = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr"))
                for j in range(1,rowcnty+1):
                    if j == 1:
                        casenos = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[1]" ).text
                        amounts = str(float(driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[5]" ).text))
                    else:    
                        casenos = casenos + ";;" + driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[1]" ).text
                        amounts = str(amounts) + ";;" +str(float(driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[5]" ).text))
                        
                driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
                arrcasenos = casenos.split(";;")
                arramounts = amounts.split(";;")
                
                longTerm = ['LTD','VLD','CLC','WDL']
                shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
                
                
                cnt = 0
                for caseno in arrcasenos:
                    if len(caseno.split("-")) > 3:
                        type = caseno.split("-")[2]
                    else:
                        type = ''
                        
                    time.sleep(5) 
                    cnt  =cnt + 1
                    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
                    time.sleep(5)
                    if type!= '':
                        if type in shortTerm:
                            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
                        elif type in longTerm:
                            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
                        else:
                             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
                            
                            
                        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
                            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
                        time.sleep(5)
                        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
                            driver.find_element_by_xpath("//div[text()='Close']").click()
                        
                        driver.find_element_by_xpath("//div[contains(text(),'Expenses')][@class='TabOff']").click()
                        time.sleep(3) 
                        amt1 = arramounts[cnt-1]
                        rowcntz = len(driver.find_elements_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr"))
                        for x1 in range(1, rowcntz+1):
                            if float(driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(x1) + "]/td[6]").text) == float(amt1):
                                invval = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(x1) + "]/td[1]").text
                                date1 = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(x1) + "]/td[2]").text
                                if invval == '-':
                                    invval = ''
                                if date1 == '-':
                                    date1 = ''    
                                Chkamt = str('%.2f' %(float(amt1))) + ";;" + str(invval)+ ";;" + str(date1)
                                break
                        if allAmts == '':
                            allAmts = Chkamt
                        else:     
                            allAmts = allAmts + "//" + Chkamt 
            break           
        #print(allAmts)               
        compare_String(xldrow.iat[0,0].strip(), name, "Name")
        compare_String(xldrow.iat[0,1].strip(), address.upper(), "Address")   
        compare_String(str('%.2f' %(float(xldrow.iat[0,2]))), str('%.2f' %(float(totalamt))), "TotalAmt")  
         
        compare_String(xldrow.iat[0,3].strip(), allAmts, "All Amounts") 
        listFinPayment = allAmts.split("//")
        listXLPayment = xldrow.iat[0,3].split("//")
        
        if (Counter(listXLPayment) == Counter(listFinPayment)):
            html_logger.dbg("Payment match - The payment values are " + allAmts)
        else:
            html_logger.err("Payment doesn't match - Extract has \n" + allAmts + "\n but Fin has \n" + xldrow.iat[0,3])  
              
                       
    else:
        html_logger.err("Records not found, hence exiting")    
     
    #xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
##################################################
def VendorEOB():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of VendorEOB  Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "VendorEOB" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname, skiprows=[0])
    xldf1 = xldf.iloc[:,0].unique()
    print(xldf1)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i]) != ''):
            name = xldf1[i]
            html_logger.info("Start of Vendor " + str(name)) 
            VendorEOB_Fineos(name,xldf)     #allowed values =  AltEOB, EOB


       
       

################################################################################
def DisClaimPayment_Fineos(caseno,BenfromDt,BenEndDt,xldrow):
    xldrow = xldrow[(xldrow['Claim Number']==caseno) & (xldrow['Benefit Paid From Date']==BenfromDt) & (xldrow['Benefit Paid Thru Date']==BenEndDt)]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow.reset_index(drop = True)
   
    #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
   
    date = xldrow.iat[0,9]
    name = xldrow.iat[0,3].split("\n")[0].strip()
    print(name)
    #xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        benStartDt = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][contains(@class,'DataLabel')]").text
        claimStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        driver.find_element_by_xpath("//div[contains(text(),'Calculation Parameters')][@class='TabOff']").click()
        time.sleep(3)
        earnings = driver.find_element_by_xpath("//label[text()='Earnings at Initial Payment Frequency']/ancestor::td[1]/following::td/span").text    
        benDisDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following::dd").text   
        benDisDate = benDisDate.strip() 
        
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        wrkComp = 0
        fitAmt = 0
        ficaAmt = 0
        sitAmt = 0
        
        socSecOfst = 0
        GrpDisSTD = 0
        partialDis = 0
        OvrPymt = 0
        sickPymt = 0
        OthrAmt = 0
        stDisIncome = 0
        salCont = 0
        
        i = True
        while i == True:
            #len(driver.find_elements_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]")) > 0
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            for i in range(1, rowcnt+1):
                 strdt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[2]").text
                 enddt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[3]").text
                 
                 if datetime.strptime(str(strdt),'%m/%d/%Y') == datetime.strptime(str(BenfromDt),'%Y-%m-%d %H:%M:%S') and datetime.strptime(str(enddt),'%m/%d/%Y') == datetime.strptime(str(BenEndDt),'%Y-%m-%d %H:%M:%S') :
                     driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" +str(i) + "]").click()
                     time.sleep(3)
                     driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                     time.sleep(5)
                     netbfrTax = driver.find_element_by_xpath("//span[contains(@id,'_netBenefitAmountBean')]").text
                     netbfrTax = netbfrTax.replace(",","")
                     netaftrTax = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                     netaftrTax = netaftrTax.replace(",","")
                     grosBenAmt = driver.find_element_by_xpath("//span[contains(@id,'_basicAmount')]").text
                     grosBenAmt = grosBenAmt.replace(",","")
                     
                     
                     driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                     time.sleep(3)
                     rowcnt1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr"))
                     for x in range(1,rowcnt1+1): 
                         benName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[1]").text
                         if benName == "FIT Amount":
                                fitAmt = float(fitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "FICA" in benName:
                                ficaAmt = float(ficaAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "Social Security" in benName:
                                socSecOfst = float(socSecOfst) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Group Disability Insurance":
                                GrpDisSTD = float(GrpDisSTD) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Partial Return To Work Offset":
                                partialDis = float(partialDis) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))              
                         elif benName == "State Income Tax":
                                sitAmt = float(sitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Offset Recovery":
                                OvrPymt = float(OvrPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Disability - State":
                                stDisIncome  = float(stDisIncome) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))     
                         elif benName =="Sick Leave":
                                sickPymt = float(sickPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))       
                         elif benName == "Workers' Compensation":
                                wrkComp = float(wrkComp) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Salary Continuance":
                                salCont = float(salCont) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         else:
                                OthrAmt = float(OthrAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                                        
                     rowcnt2 =  len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                     for x in range(1,rowcnt2+1): 
                         benName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[1]").text
                         if benName == "FIT Amount":
                                fitAmt = float(fitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "FICA" in benName:
                                ficaAmt = float(ficaAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "Social Security" in benName:
                                socSecOfst = float(socSecOfst) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Group Disability Insurance":
                                GrpDisSTD = float(GrpDisSTD) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Partial Return To Work Offset":
                                partialDis = float(partialDis) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))              
                         elif benName == "State Income Tax":
                                sitAmt = float(sitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Offset Recovery":
                                OvrPymt = float(OvrPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Disability - State":
                                stDisIncome  = float(stDisIncome) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))     
                         elif benName =="Sick Leave":
                                sickPymt = float(sickPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))       
                         elif benName == "Workers' Compensation":
                                wrkComp = float(wrkComp) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Salary Continuance":
                                salCont = float(salCont) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         else:
                                OthrAmt = float(OthrAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                                           
                     driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                     time.sleep(3)
                     break       
                 else:
                     pass               
                 
                                                                        
            if len(driver.find_elements_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]")) > 0:
                 driver.find_element_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]").click()
                 pass
            else: 
                 i = False
         #end of while loop# 
         
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        SSN = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        SSN = SSN.split("(")[0]
        SSN = str(SSN[0:3]) .upper()+ "-" + str(SSN[3:5]).upper() + "-" + str(SSN[5:9])
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        if benDisDate == '-'  or len(benDisDate)<10:
            benDisDate = claimDisDate
        else:
            benDisDate =  benDisDate
     
        
         
         #-------comparing stars----------------
        listClaimNo = []
        listLastName = []
        listFirstName = []
        listSSN = []
        listClaimStatus = []
        listDOD = []
        listBenStrtDt = []
        listBenPdfrm = []
        listBenPdto = []
        listEarnings = []
        listGrossBen = []
        
        listsocSecOfst = []
        listGrpDisSTD= []
        listpartialDis = []
        listOvrPymt = []
        listsickPymt = []
        liststDisIncome = []
        listsalCont = []
         
        listworkComp = []
        listNetBenbfrTax = []
        listFitAmt = []
        listSitAmt = []
        listFicaAmt = []
        listNetAftrbfrTax = []
        listothrAmt = []
         
         
        listClaimNo.append(str(caseno).strip())
        listLastName.append(str(lname).strip())
        listFirstName.append(str(fname).strip())
        listSSN.append(str(SSN).strip())
        listClaimStatus.append(str(claimStatus).strip())
        listDOD.append(str(datetime.strftime(datetime.strptime(benDisDate,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())    #datetime.date.strftime(benDisDate, "%Y-%m-%d %H:%M:%S")
        listBenStrtDt.append(str(datetime.strftime(datetime.strptime(benStartDt,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())
        listBenPdfrm.append(str(datetime.strftime(datetime.strptime(strdt,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())
        listBenPdto.append(str(datetime.strftime(datetime.strptime(enddt,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())
        listEarnings.append(str('%.2f' %(float(earnings))).strip())  
        listGrossBen.append(str('%.2f' %(float(grosBenAmt))).strip())
        #---7 more required
        
        listsocSecOfst.append(str('%.2f' %(float(socSecOfst))).strip())
        listGrpDisSTD.append(str('%.2f' %(float(GrpDisSTD))).strip())
        listpartialDis.append(str('%.2f' %(float(partialDis))).strip())
        listOvrPymt.append(str('%.2f' %(float(OvrPymt))).strip())
        listsickPymt.append(str('%.2f' %(float(sickPymt))).strip())
        liststDisIncome.append(str('%.2f' %(float(stDisIncome))).strip())
        listsalCont.append(str('%.2f' %(float(salCont))).strip())         
        
        listworkComp.append(str('%.2f' %(float(wrkComp))).strip())
        if float(grosBenAmt) -float(netbfrTax) == 0:
            listothrAmt.append(str('0.00'))
        else:
            OthrAmt = '0.00' # remove this once the defect is fixed.    
            listothrAmt.append(str('%.2f' %(float(OthrAmt))).strip())
                               
        listNetBenbfrTax.append(str('%.2f' %(float(netbfrTax))).strip())
    
        listFitAmt.append(str('%.2f' %(float(fitAmt))).strip())
        listSitAmt.append(str('%.2f' %(float(sitAmt))).strip())
        listFicaAmt.append(str('%.2f' %(float(ficaAmt))).strip())
        listNetAftrbfrTax.append(str('%.2f' %(float(netaftrTax))).strip())
        
        
        dict = {'Claim Number' :listClaimNo ,'Claimant Last Name' :listLastName ,'Claimant First Name' :listFirstName ,'Claimant Social Security Number' :listSSN ,'Claim Status' :listClaimStatus ,'Date of Disability' :listDOD ,'Benefit Start Date' :listBenStrtDt ,'Benefit Paid From Date' :listBenPdfrm ,'Benefit Paid Thru Date' :listBenPdto ,'Earnings' :listEarnings ,'Gross Benefit ' :listGrossBen, 
                   'Social Security Offset': listsocSecOfst, 'Group Disability (STD)':listGrpDisSTD,'Overpayment':listOvrPymt, 'Partial\nDisability':listpartialDis, 'State Disability Income': liststDisIncome, 'Salary Continuance':listsalCont,'Sick Leave':listsickPymt                 
                   ,'Workers Compensation' :listworkComp ,'Other ' :listothrAmt ,'Net Benefit (before taxes)' :listNetBenbfrTax ,'FIT ' :listFitAmt ,'SIT' :listSitAmt ,'FICA ' :listFicaAmt ,'Net Payment (after taxes)' :listNetAftrbfrTax}

        fin_df = pd.DataFrame(dict)         
        
        xldrow['Net Benefit (before taxes)']  =  xldrow['Net Benefit (before taxes)'].astype(float)
        fin_df['Net Benefit (before taxes)']  =  fin_df['Net Benefit (before taxes)'].astype(float)
        
        xldrow['Gross Benefit ']  =  xldrow['Gross Benefit '].astype(float)
        fin_df['Gross Benefit ']  =  fin_df['Gross Benefit '].astype(float)
        
        xldrow['Net Payment (after taxes)']  =  xldrow['Net Payment (after taxes)'].astype(float)
        fin_df['Net Payment (after taxes)']  =  fin_df['Net Payment (after taxes)'].astype(float)
        
        xldrow['Earnings']  =  xldrow['Earnings'].astype(float)
        fin_df['Earnings']  =  fin_df['Earnings'].astype(float)
        
        xldrow['Social Security Offset ']  =  xldrow['Social Security Offset '].astype(float).map('{:,.2f}'.format)
        xldrow['Group Disability (STD)']  =  xldrow['Group Disability (STD)'].astype(float).map('{:,.2f}'.format)
        xldrow['Overpayment ']  =  xldrow['Overpayment '].astype(float).map('{:,.2f}'.format)
        xldrow['Partial\nDisability']  =  xldrow['Partial\nDisability'].astype(float).map('{:,.2f}'.format)
        xldrow['State Disability Income']  =  xldrow['State Disability Income'].astype(float).map('{:,.2f}'.format)
        xldrow['Salary Continuance']  =  xldrow['Salary Continuance'].astype(float).map('{:,.2f}'.format)
        xldrow['Sick Leave']  =  xldrow['Sick Leave'].astype(float).map('{:,.2f}'.format)
        xldrow['Workers Compensation']  =  xldrow['Workers Compensation'].astype(float).map('{:,.2f}'.format)
        xldrow['Other ']  =  xldrow['Other '].astype(float).map('{:,.2f}'.format)
        xldrow['FIT ']  =  xldrow['FIT '].astype(float).map('{:,.2f}'.format)
        xldrow['SIT']  =  xldrow['SIT'].astype(float).map('{:,.2f}'.format)
        xldrow['FICA ']  =  xldrow['FICA '].astype(float).map('{:,.2f}'.format)
         
        xldrow = xldrow.applymap(str)
        fin_df = fin_df.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
 
 
        
                
def DisClaimPayment():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of DisClaimPayment  Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "DisClaimPayment" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,1].unique()
    #print(xldf1)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iat[i,0])[:3] == 'DI-'):
            caseno = xldf.iat[i,0]
            BenFromDt = xldf.iat[i,7]
            BenToDt = xldf.iat[i,8]
            
            #xldf = xldf.iloc[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            DisClaimPayment_Fineos(caseno,BenFromDt,BenToDt,xldf)     #allowed values =  AltEOB, EOB
       

##################################################################### 
def ClaimsFeedReport_Fineos(caseno,PaymentBeginDate,PaymentAmount,xldrow):
    
    xldrow = xldrow[(xldrow['BEN_CASE_NBR']==caseno) & (xldrow['PAY_SRS_BEG_1_DT'] == PaymentBeginDate.strftime('%m/%d/%Y')) & (xldrow['GROS_BEN_AMT'] == PaymentAmount)]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    
    """ 
    xldrow['Payment Begin Date'] = pd.to_datetime(xldrow['Payment Begin Date'])
    xldrow['Payment Begin Date'] = xldrow['Payment Begin Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment Date'] = pd.to_datetime(xldrow['Payment Date'])
    xldrow['Payment Date'] = xldrow['Payment Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment End Date'] = pd.to_datetime(xldrow['Payment End Date'])
    xldrow['Payment End Date'] = xldrow['Payment End Date'].dt.strftime('%m/%d/%Y')
    """
    
    #xldrow['Zip Code'] = xldrow['Zip Code'].apply(lambda x: '{0:0>5}'.format(x))
    #xldrow['Box 3'] = xldrow['Box 3'].sum()
    #xldrow['Box 4 FIT'] = xldrow['Box 4 FIT'].sum()
    #xldrow['Box 10'] = xldrow['Box 10'].sum()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
    
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
        
           
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        time.sleep(3)
        ##################
        BenStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        arr_Claim_case = Split(caseno,"-")
        Claim_case = arr_Claim_case[0] + "-" + arr_Claim_case[1]
       ###################
       
        time.sleep(3)
        if type in shortTerm:
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
        elif type in longTerm:   
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
        time.sleep(3)
        
        if type in shortTerm:
            polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
        elif type in longTerm:   
            polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
            
        #polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit']/following::strong[text()='Policy Number']/span").text 
        ##############
        polNo1 = polNo
        ######
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Case History')]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
                try:
                    driver.find_element_by_xpath("//a[text()='View Older']").click()
                    time.sleep(5)
                except:
                    break    
       
        cnt = len(driver.find_elements_by_xpath("//div[contains(@class,'dateDivider')]/span"))
        if(cnt>0):
            startDate = driver.find_element_by_xpath("(//*[contains(text() ,'Intake Document - GDI')]|//span[contains(text(),'New Claim Submitted')])/preceding::div[@class='dateDivider'][1]/span").text   
            startDate = startDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(startDate),'%b %d %Y')
            startDate = datetimeobject.strftime('%m/%d/%Y')  
        else:
            startDate = '' 
        
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied' or benStatus == 'Approved':
            if benStatus == 'Closed - Approved':
                benStatus1 = 'Approved'
            else:
                benStatus1 = benStatus
                        
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
        
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  
    
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
             for i in range(1,rowcnt+1):
                time.sleep(3)
                fin_payBeginDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[1]").text
                fin_payamount = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[7]").text
                fin_payamount = replace(str(fin_payamount),"," )
                periodStrtDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                periodEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                
                if(fin_payBeginDt == PaymentBeginDate and float(PaymentAmount) == float(fin_payamount)):
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    claim_own = driver.find_element_by_xpath("//label[text()='Added By']/parent::span//parent::td/following::td/span").text
                    issueDate = driver.find_element_by_xpath("//label[text()='Issue Date']/parent::span//parent::td/following::td/span/span").text
                    transdate = driver.find_element_by_xpath("//label[text()='Transaction Status Date']/parent::span//parent::td/following::td/span").text 
                    fin_periodStrtDt = periodStrtDt
                    fin_periodEndDt = periodEndDt
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
                    driver.find_element_by_xpath("//div[contains(text(),'Case Details')][@class='TabOff']").click()
                    time.sleep(5)
                    BenStDt = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][contains(@class,'DataLabel')]").text
                    BenEndDt = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')][contains(@class,'DataLabel')]").text
                    
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()

        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        ###########################
        disabilityDate = driver.find_element_by_xpath("//label[text()='Disability Date']/parent::div/following::span/span").text
        
        name1 = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        if name1==name:
            driver.find_element_by_xpath("//a[contains(@name,'_KeyInfoBarLink_0')]").click()
        else:    
            driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(name)
            time.sleep(5)
            driver.find_element_by_xpath("//i[@class='icon-person' or @class='icon-organisation']/following::strong[text()='" + name + "']").click() 
            time.sleep(5)
        
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
        elif(len(driver.find_elements_by_xpath("//span[contains(@id,'_Address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_Address')]").text  
              
        claimResCountry = (addr.replace('\n','')).split(",")[-1].strip()
        if('USA' in claimResCountry):
             #claimResState = (addr.replace('\n','')).split(",")[-3].strip()
             claimState =  (addr.replace('\n','')).split(",")[-3].strip()
        else:
            claimState = ''
            
        #cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        #cusid = cusid.strip()
        
        #ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        #ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        #ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        #ssn = ssn1+ssn2+ssn3
        #except:
        #   pass
        
        dict = {'Policyholder Nbr':listPolNo, 'Policyholder Master Name':listPHMstrName,'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo, 'First Name':listFName,'Last Name':listLName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt, 'Liability Amt':listLiabAmt }
        fin_df = pd.DataFrame(dict)                  
        fin_df = fin_df.applymap(str)
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
        if rowAvlbl == 0:
            html_logger.err("Payment record  with that date range is not available")
                    
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def ClaimsFeedReport():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of Claims Feed Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ClaimFeedReport" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)a
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        #print(str(xldf.iloc[i,-1])[:3])
        if(str(xldf['BEN_CASE_NBR'].iloc[i])[:3] == 'DI-'):
            caseno = str(xldf['BEN_CASE_NBR'].iloc[i])
            PaymentBeginDate = xldf['PAY_SRS_BEG_1_DT'].iloc[i]
            PaymentAmount = xldf['GROS_BEN_AMT'].iloc[i]
            
            html_logger.info("Start of Case Number" + str(caseno) + " and the PaymentBeginDate " + str(PaymentBeginDate)) 
            ClaimsFeedReport_Fineos(caseno,PaymentBeginDate,PaymentAmount,xldf)
            
##################################################################### 


            #print(xldf.iloc[i,j])

                                                  
#waiverReportValidation_Fineos("DI-482-LTD-01")  #"DI-484-STD-02"

#DisabilityClaimsDetailReport_AllClaims("006067360000000","STD") -- need to work on this  after data changes



#FineosDailyAccountingDetailsReport()
#FINEOSCashDisbursementRequestDetailReport('8DCLC')
#FINEOSCashDisbursementRequestDetailReport('8DCLE')
#FineosDailyAccountingDetailsReport()


"""    
EC3802_PremiumWaiver("006067360000000","STD")
EC3802_PremiumWaiver("006067360000000","LTD")



FineosDailyAccountingDetailsReport()
FINEOSCashDisbursementRequestDetailReport('8DCLC')
FINEOSCashDisbursementRequestDetailReport('8DAPC')
FINEOSCashDisbursementRequestDetailReport('8DCLE')


FINEOSCashDisbursementRequestDetailReport('8DAPC')
FINEOSCashDisbursementRequestDetailReport('8DAPE')
FINEOSCashDisbursementRequestDetailReport('8DCLC')
FINEOSCashDisbursementRequestDetailReport('8DCLE')
"""

#-------Eben-----------------------------------
#Eben_3717_1()  # Eben Db comparison
#-------Eben-----------------------------------




#options = webdriver.ChromeOptions()
#driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)

options = webdriver.ChromeOptions()
capabilities = DesiredCapabilities.CHROME.copy()
capabilities['platform'] = 'WINDOWS'
capabilities['version'] = "ANY"
driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
#driver = webdriver.Remote(command_executor=f"http://10.32.232.129:4444/wd/hub",desired_capabilities=capabilities,options = options)


disClaimDetailReport()  #PHReport Disability Claim Detail Report fin vs Excel Report
#waiverValidation()  # Report comparison for fineos vs Excel report - Swetha
#disClaimStatusReport()# Report comparison for fineos vs Excel report - Swetha

#FICAWithholdingMatch() # Report comparison for fineos vs Excel report - Swetha  # Fixed Sum
#FedWageTax() # fixed sum
#StateWageTax() # Fixed Sum
#MiscReport1099()
#ASOSimpleReport1099()
#ReconcileEmpTax_NoW2_8922()  # fixed sum
#print(("WILL BYERS").capitalize())
#ClaimantEOB()
#VendorEOB()
#waiverPremiumValidation()
#DisClaimPayment()   #same script fro DIsclaimStatusReport

#ASODB validation is in C:\Srikanth\Srikanth\Claims_ReportValidation\ASODB_Report.xlsm

#ClaimsFeedReport()

'''
str1 = """123 QA Avenue,
        Vancouver,
        BC,
        V5J3Z7,
        Canada."""
        
str2 = """street 1,
washington, WA, 98052,
USA."""
claimResState = (str2.replace('\n','')).split(",")[-3].strip()
print(claimResState)
print((str2.replace('\n','')).split(",")[0].strip())
#print((str2.replace('\n','')).split(",")[1].strip())
print((str2.replace('\n','')).split(",")[-4].strip())
print((str2.replace('\n','')).split(",")[-3].strip())
print((str2.replace('\n','')).split(",")[-2].strip())
print((str2.replace('\n','')).split(",")[-1].strip())
claimResState = (str1.replace('\n','')).split(",")[-3].strip()
print(claimResState)
'''


#DisabilityClaimsDetailReport_AllClaims("006067360000000","STD") #-- need to work on this  after data changes
#DisabilityClaimsStatusReport_AllClaims("006067360000000","STD")


#EC3802_PremiumWaiver("'000005550003000','006170980000000','006999970000000','006191570000000','006171190000000','000020765001000','000075440002000','006018800000000','000020765001000','116000010002000'","LTD")
#EC3802_PremiumWaiver("'000005550003000','006170980000000','006999970000000','006191570000000','006171190000000','000020765001000','000075440002000','006018800000000','000020765001000','116000010002000'","STD")

#Misc1099Reports()
#FineosDailyAccountingDetailsReport()