# -*- coding: utf-8 -*-
#----------------Imports--------------------------
#import unittest
import datetime
import time
import logging
from collections import Counter

from lxml import etree
from pandas import ExcelFile
from pandas import ExcelWriter
import pytest
import pytest_html
import requests
import pyautogui

#import splinter
#from splinter import Browser

from Screenshot import Screenshot_Clipping
from pynput.keyboard import Key, Controller
import difflib

import json
import keyboard

from jsonpath_ng import jsonpath, parse


#from Python_Test1 import html_logger
import sys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

import html_logger
from html_logger import setup, info
import pandas as pd

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

#----------------Imports--------------------------
#----------------Logging--------------------------
now = datetime.datetime.now()

LOG_FILENAME = parentFolder + "DisabilityClaims\\LetterValidation\\Letters_"+ str(now.isoformat().replace(":","_")) + ".html"
#logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)
#setup("WebService_Test","1",LOG_FILENAME)

setup("Letters Validation","Executed on " + str(now),LOG_FILENAME)
html_logger.info("Start of driver")
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')


 
 
 
 
 
 
 
 
 

#----------------Logging--------------------------


'''----------------------------------------DBCall---------------------------------------------------------------------------
gets Pararmeter as dbServer, dbName and SQLQuery

ex: dbResult = dbCall(dbServer='SQLD10747,4242', dbName='ENTPRS_CLAIMS_DM', sqlQuery="select ADMIN_SYS_PARTY_KEY_TXT from dbo.ENTPRS_PARTY_VW p where ADMIN_SYS_PARTY_KEY_TXT like '0000004%' and 
                (cmpny_pref_name like 'test sub%' or PLCYHLDR_FRST_SUBSD_PREF_NAME like 'test sub%' or PLCYHLDR_SEC_SUBSD_PREF_NAME like 'test sub%' or PLCYHLDR_THIRD_SUBSD_PREF_NAME like 'test sub%' 
                or PLCYHLDR_FOURTH_SUBSD_PREF_NAME like 'test sub%')")
-------------------------------------------------------------------------------------------------------------------'''

def dbCall(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    list_result = list()

    for row in cursor:
        list_result.append(str(row[0]).strip())
        
      
    return ';'.join([str(elem) for elem in list_result]) 



def dbCall1(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    list_result = list()

    for row in cursor:
        list_result.append(str(row[0]).strip())
        
      
    return list_result
#----------------------------------------End of DBCall---------------------------------------------------------------------------



def dbCall_multi(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    #list_result = list()
    return cursor.fetchall()
 
    #return list_result



def dbretrieveData(recordset,columnname):
    row = ""
    list_result = list()
    for row in recordset:
        colval = eval("row." + columnname)
        if(colval != None):
            list_result.append(colval)      
    return list_result          


def getEnum(enum_id):
        try:
                wsQuery = "Select distinct Fineos_enum_instnc_name from qa.fineos_enum_xref where fineos_enum_id  in(" +  enum_id + ") and PM_CUR_IND = 'Y'"
                recordset1 = dbCall(dbServer='SQLD10747,4242', dbName='ENTPRS_CLAIMS_DM', sqlQuery=wsQuery)
                returnval = recordset1
        except:
                returnval = ''       
        return returnval
    
def changeFormat(date1):
        if(date1 != ''):
            return datetime.datetime.strptime(date1[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d/%Y')
        else:
            return ''
        
def changeFormatmmdd(date1):
        if(date1 != ''):
            return datetime.datetime.strptime(date1[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d')
        else:
            return ''

def compareString(FN_Value, WS_Value, FieldName):
    try:
        FN_Value = FN_Value.replace("'","")
        WS_Value = WS_Value.replace("'","")
        WS_Value = WS_Value.replace('"',"")
        WS_Value = WS_Value.replace(", ",";")
        WS_Value = WS_Value.replace(",",";")
        
        if(';' in WS_Value):
            WS_list = WS_Value.split(";")
            WS_list.sort()
            WS_Value = ';'.join(WS_list)
        
        if(';' in FN_Value):
            FN_list = FN_Value.split(";")
            FN_list.sort()
            FN_Value = ';'.join(FN_list)
            
                
        if(FN_Value == '-'):
            FN_Value = ''
        
        if(FN_Value == WS_Value):
            html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
        else:
            try:
                if(float(FN_Value.replace(",","")) == float(WS_Value.replace(",",""))):
                    html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
                else:
                    html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
            except:            
                html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
    except:
            html_logger.dbg("Error message when comparing " + FieldName + " Please check manually")   



def compareString_1(FN_Value, WS_Value, FieldName):
    try:
        FN_Value = FN_Value.replace("'","")
        WS_Value = WS_Value.replace("'","")
        WS_Value = WS_Value.replace('"',"")
        WS_Value = WS_Value.replace("  "," ")
        WS_Value = WS_Value.replace("  "," ") #re-remove the spaces
        if(';' in WS_Value):
            WS_list = WS_Value.split(";")
            WS_list.sort()
            WS_Value = ';'.join(WS_list)
        
        if(';' in FN_Value):
            FN_list = FN_Value.split(";")
            FN_list.sort()
            FN_Value = ';'.join(FN_list)
            
                
        if(FN_Value == '-'):
            FN_Value = ''
        
        if(FN_Value == WS_Value):
            html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
        else:
            try:
                if(float(FN_Value.replace(",","")) == float(WS_Value.replace(",",""))):
                    html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
                else:
                    html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
            except:            
                html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
    except:
            html_logger.dbg("Error message when comparing " + FieldName + " Please check manually")   




            
            
def compareStringContains(FN_Value, WS_Value, FieldName):             
    try:
        FN_Value = FN_Value.replace("'","")
        WS_Value = WS_Value.replace("'","")
        WS_Value = WS_Value.replace(", ",";")
        WS_Value = WS_Value.replace(",",";")
        
        if(';' in WS_Value):
            WS_list = WS_Value.split(";")
            WS_list.sort()
            WS_Value = ';'.join(WS_list)
        
        if(';' in FN_Value):
            FN_list = FN_Value.split(";")
            FN_list.sort()
            FN_Value = ';'.join(FN_list)
            
                
        if(FN_Value == '-'):
            FN_Value = ''
        
        if(WS_Value in FN_Value):
            html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
        else:     
            html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
    except:
            html_logger.dbg("Error message when comparing " + FieldName + " Please check manually")  
            
                
'''
recordset1 = dbCall_multi("SQLD10747,4242","ENTPRS_CLAIMS_DM", "Select ADMIN_SYS_NAME, PARTY_TYP_CD,PARTY_ROLE_TYP_TXT,ADMIN_SYS_PARTY_KEY_TXT,CMPNY_PREF_NAME,BILL_CNTCT_NAME,PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,RGNL_GRP_OFF_NBR from dbo.ENTPRS_PARTY_VW e where ADMIN_SYS_PARTY_KEY_TXT like '00000021%'")  
AdminSysName = dbretrieveData(recordset1,"ADMIN_SYS_NAME")
cmpnyPrefname = dbretrieveData(recordset1,"CMPNY_PREF_NAME")
BILL_CNTCT_NAME = dbretrieveData(recordset1,"BILL_CNTCT_NAME")
print(str(AdminSysName))
print(str(cmpnyPrefname))
print(str(BILL_CNTCT_NAME))
'''

# "Select ADMIN_SYS_NAME, PARTY_TYP_CD,PARTY_ROLE_TYP_TXT,ADMIN_SYS_PARTY_KEY_TXT,CMPNY_PREF_NAME,BILL_CNTCT_NAME,PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,RGNL_GRP_OFF_NBR from dbo.ENTPRS_PARTY_VW e where ADMIN_SYS_PARTY_KEY_TXT like '00000006%'"

'''----------------------------------------SOAPPost---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)

ex: dbResult = wsResult = SOAPPost(url=url1,reqBody=body1,reqHeader=headers1,reqPath=reqPath1)
-------------------------------------------------------------------------------------------------------------------'''

def SOAPPost(url,reqBody,reqHeader,reqPath):
    response = requests.post(url,data=reqBody,headers=reqHeader)
    rsp = response.content
    tree = etree.fromstring(rsp)
    doc = etree.ElementTree(tree)

    find_val1 = etree.XPath(reqPath)
    return find_val1(tree)
    
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------



def SOAPPost_Response(url,reqBody,reqHeader):
    response = requests.post(url,data=reqBody,headers=reqHeader)
    rsp = response.content
    tree = etree.fromstring(rsp)
    doc = etree.ElementTree(tree)
    return tree



def SOAPPost_getNodeValue(tree,reqPath):
    find_val1 = etree.XPath(reqPath)
    
    return find_val1(tree)


'''----------------------------------------JSONGet---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)

ex: dbResult = wsResult = JSONGet(url=url1,reqHeader=headers1,reqPath=reqPath1)
-------------------------------------------------------------------------------------------------------------------'''

def JSONGet(url,reqHeader,reqPath):
    response = requests.get(url,headers=reqHeader)
    rsp = response.content
    
    json_data = json.loads(rsp)
    jsonpath_expression = parse(reqPath)
    match = jsonpath_expression.find(json_data)
    return {match[0].value}
    
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------

def wait_Sec(secs):
    time.sleep(secs)

def Sync(xpth):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))



def Click(xpth):
    try:
        wait = WebDriverWait(driver,10)
        wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
        driver.find_element_by_xpath(xpth).click()
    except:
        Click(xpth)    
    html_logger.dbg("Clicked " + xpth + " element successfully")


def Set(xpth,val1):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).send_keys(str(val1))
    html_logger.dbg("Set the value of " + val1 + " to the " + xpth + " element successfully")
    
def GetText(xpth): 
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    return str(driver.find_element_by_xpath(xpth).text)
       
'''----------------------------------------Fineos Login---------------------------------------------------------------------------
gets Pararmeter as Url and Search Type

ex: Login(url=url1,SearchType)
-------------------------------------------------------------------------------------------------------------------'''

def Login(url,searchType):
    
    driver.get(url)
    driver.maximize_window()
    if(searchType == 'OrgSearch'):
        xpth_srchPartyLink = "//a[contains(@id,'_MENUITEM.SearchPartieslink')]"
        Click(xpth_srchPartyLink)
        #driver.implicitly_wait(5)
    
        xpth_rdoOrganization = "//input[contains(@id,'_Organisation_GROUP')]"
        #Sync(xpth_rdoOrganization)
        Click(xpth_rdoOrganization)



def Logout():
    xpth_Useroption = "//a[contains(@id,'UserOptionslink')]"
    Click(xpth_Useroption) 
    
    xpth_Logout = "//a[contains(@id,'MENUITEM.Logoutlink')]"
    Click(xpth_Logout)   
    
    xpth_Confirm = "//input[contains(@id,'_Logout_yes')]"
    Click(xpth_Confirm)
    
    driver.close()


def screenshot(path):
    screenshot = pyautogui.screenshot()
    screenshot.save(path)
    html_logger.dbg('Screenshot saved in ' + path)



def screenshot1(path):
    
    ob = Screenshot_Clipping.Screenshot()
    img_url=ob.full_Screenshot(driver, save_path= path)
    html_logger.dbg('Screenshot saved in ' + img_url)


                             
'''
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical error message')
'''




















































def letters(LeterType,BenType,Lettername,expText,claimNo):
    
    url = "https:CONTENT:password1@idt-claims-webapp.oneamerica.fineos.com"
    searchType = "OrgSearch"
    Login(url,searchType)
    Set("//input[@id='universal-search']",claimNo)
    wait_Sec(5)
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.DOWN)
    wait_Sec(2)
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.DOWN)
    wait_Sec(2)
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)
    
    wait_Sec(2)
    driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
    wait_Sec(2)
    
    driver.find_element_by_xpath("//span[text() = 'Correspondence']").click()
    driver.find_element_by_xpath("//span[contains(text(),'Other Document')]").click()
    wait_Sec(2)
    driver.find_element_by_xpath("//span[text()='Letters']//parent::span//preceding-sibling::span").click()
    wait_Sec(2)
    #driver.find_element_by_xpath("//span[contains(text(),'Denial and Closure')]//parent::span//preceding-sibling::span").click()
    driver.find_element_by_xpath("//span[contains(text(),'" + LeterType + "')]//parent::span//preceding-sibling::span").click()
    wait_Sec(2)
    #driver.find_element_by_xpath("//span[contains(text(),'Denial and Closure')]//parent::span//following-sibling::span[contains(@id,'treenode')]//descendant::span[text()='LTD']").click()
    driver.find_element_by_xpath("//span[contains(text(),'" + LeterType + "')]//parent::span//following-sibling::span[contains(@id,'treenode')]//descendant::span[text()='" + BenType + "']").click()
    wait_Sec(5)
    #driver.find_element_by_xpath("//td[text()='Closure- Survivor and Benefit']").click()  
    driver.find_element_by_xpath("//td[text()='" + Lettername + "']").click() 
    wait_Sec(2)
    driver.find_element_by_xpath("//input[contains(@id,'_searchPageOk_cloned')]").click()
    wait_Sec(2)
    
    date1 = datetime.datetime.now().strftime("%m/%d/%Y")
    lnkName = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
    disdate = driver.find_element_by_xpath("//dl[@class='info']//dt[contains(text(),'Disability Date')]//following-sibling::dd").text
    
    salutation = driver.find_element_by_xpath("//select[contains(@id,'_SalutationDropDown_')]//option[@selected]").text
    
    #if(len(driver.find_elements_by_xpath("//dl[@class='info']/dd[2]")) > 0):
    #    disdate = driver.find_element_by_xpath("//dl[@class='info']/dd[2]").text
    #else:
    #    disdate = ""
        
    if(Lettername == 'LTD Claim Withdrawal' or Lettername =='Agreement Concerning Benefits' or Lettername == 'Fax Cover Sheet' or Lettername == 'STD Withdrawal Closure'):
        address = driver.find_element_by_xpath("//span[contains(@id,'RecipientWidget') and contains(@id,'_Address') and @class='DataLabel']").text
    else:   
        address = driver.find_element_by_xpath("//td[contains(@id,'_listviewAddress0')]").get_attribute('title')
        
    if(Lettername == 'LTD Claim Withdrawal' or Lettername =='Agreement Concerning Benefits' or Lettername == 'Fax Cover Sheet'):
        arrStr1 = address.split(",")
        x = 0
        for i in arrStr1:
            if(x < len(arrStr1)):
                if(x==0):
                    addr1 = i.strip() + "  " 
                elif(x==1):
                    addr1 = addr1 + "\n" +  i.strip()
                elif(x==2):
                    addr1 = addr1 + " " +  i.strip()
                elif(x==3):
                    addr1 = addr1 + " " +  i.replace(".","").strip()    
                x = x + 1   
    elif(Lettername == 'STD Withdrawal Closure'):
        arrStr1 = address.split(",")
        x = 0
        for i in arrStr1:
            if(x < len(arrStr1)):
                if(x==0):
                    addr1 = i.strip()
                elif(x==1):
                    addr1 = addr1 + " " + i.strip() + " "
                elif(x==2):
                    addr1 = addr1 + "\n" +  i.strip()
                elif(x==3):
                    addr1 = addr1 + " " +  i.strip()
                elif(x==4):
                    addr1 = addr1 + " " +  i.replace(".","").strip()    
                x = x + 1               
    else:
        arrStr1 = address.split("\n")
        x = 0
        for i in arrStr1:
            if(x < len(arrStr1)-1):
                if(x==0):
                    if(Lettername == 'Closure- Survivor and Benefit' or Lettername == 'LTD Attending Physician Occupation Narrative' or Lettername == 'Attorney Request for MVA or WC Info' or Lettername == 'LTD Initial Liability (IL) 10 Day' or Lettername == 'LTD Telephone Call Contact w/in 15 days' or Lettername == 'OLS Blank BA' or Lettername == 'ATP Advance Pay and Close (APC), Privacy Notice' 
                       or Lettername == 'LTD Closure Not Disabled Any Occ' or Lettername == 'LTD Closure Elimination Period (EP) Not Met' or Lettername == 'LTD Closure Failure to Provide Proof' or Lettername == 'LTD Closure No Coverage' or Lettername == 'LTD Closure RTW Own Occ' or Lettername == 'LTD Closure Survivor and Benefit'
                       or Lettername == 'LTD Approval Limited Conditions' or Lettername == 'LTD Approval Ongoing') or Lettername == 'LTD Approval Pregnancy w Pending Delivery' or Lettername == 'LTD Approval w Estimated RTW' or Lettername == 'LTD 15 Day Request for Info' or Lettername == 'LTD 15 Day Final Request for Info': 
                        addr1 = i.replace(",","").strip() + " "
                    elif(Lettername == 'STD Blank Examiner'):
                        addr1 = i.replace(",","").strip() + ""    
                    elif(Lettername == 'LTD Acknowledgement' or Lettername == 'Appeal Acknowledgement'): 
                        addr1 = i.replace(",","").strip() + "  "
                    else: 
                        addr1 = i.replace(",","").strip() + "  "       
                else:
                    addr1 = addr1 + "\n" +  i.replace(",","")
                    addr1 = addr1.strip()
                x = x + 1  

  
    policyno = driver.find_element_by_xpath("//*[contains(@id,'_Contracts')]/tbody/tr[1]/td[2]/a").text
    phNumber = driver.find_element_by_xpath("//td[contains(@id,'_PartiesOnContractName0')]").text
    
    driver.find_element_by_xpath("//i[@class='fa fa-chevron-left']").click()
    businessName = driver.find_element_by_xpath("//label[text()='Employer']//ancestor::tr//following-sibling::tr/td[1]/descendant::a").text
    
    namex = lnkName.split(" ")
    if len(namex) ==3:
        name = namex[1] + " " + namex[2]
    else:
        name = lnkName    
    
    ucaseName = lnkName.upper()

    if("<<<policyno>>>" in expText):
        expText = expText.replace("<<<policyno>>>",policyno)
        
    if("<<<businessName>>>" in expText):
        expText = expText.replace("<<<businessName>>>",businessName) 
        
    if("<<<disdate>>>" in expText):
        expText = expText.replace("<<<disdate>>>",disdate)        
    
    if("<<<name>>>" in expText):
        expText = expText.replace("<<<name>>>",name)
        
    if("<<<ucaseName>>>" in expText):
        expText = expText.replace("<<<ucaseName>>>",ucaseName)    
        
    if("<<<disdate>>>" in expText):
        expText = expText.replace("<<<disdate>>>",disdate)
              
    if("<<<PHName>>>" in expText):
        expText = expText.replace("<<<PHName>>>",phNumber)
            
    if("<<<days>>>" in expText):
        expText = expText.replace("<<<days>>>","30")
     
    if("<<<date>>>" in expText):
        expText = expText.replace("<<<date>>>",date1)   
    
    if("<<<Name>>>" in expText):
        expText = expText.replace("<<<Name>>>",lnkName)     
    
    if("<<<ADDR>>>" in expText):
        expText = expText.replace("<<<ADDR>>>",addr1.strip())        
    
    if("<<<claimNo>>>" in expText):
        expText = expText.replace("<<<claimNo>>>",claimNo)
    
    if("<<<benType>>>" in expText):
        expText = expText.replace("<<<benType>>>",BenType)    
            
    if(Lettername == 'Closure- Survivor and Benefit'):
        header = date1 + "\n\n" + lnkName + "\n" + addr1 + "\n\n" + "Re:   Long-Term Disability Claim No.: DI-612" + "\n" + "        Group: " + policyno + "\n" + "        Policyholder: " + phNumber + "\n\n" + "Dear " + lnkName + ":\n\n"
    elif(Lettername == 'LTD Acknowledgement'):
        header = date1 + "\n\n\n" + lnkName + "\n" + addr1 + "\n\n" + "Re:   Long-Term Disability Claim No.: DI-612" + "\n" + "        Group: " + policyno + "\n" + "        Policyholder: " + phNumber + "\n\n" + "Dear " + lnkName + ":\n"
    elif(Lettername == 'Appeal Acknowledgement'):
        header = date1 + "\n\n\n" + lnkName + "\n" + addr1 + "\n\n" + "Re:\tDisability Claim No.: DI-612" + "\n" + "Group: " + policyno + "\n" + "Policyholder: " + phNumber + "\n\n" + "Dear " + lnkName + ":\n\n"
    elif(Lettername == 'LTD Claim Withdrawal'):
        header = date1 + "\n\n\n" + lnkName + "\n" + addr1 + "\n\n" + "Re:   Long-Term Disability Claim No.: DI-612" + "\n" + "        Group: " + policyno + "\n" + "        Policyholder: " + phNumber + "\n\n" + "Dear " + lnkName + "\n"
    
                 
    #if(Lettername == 'Closure- Survivor and Benefit'):
    #    expText = header + expText
    #elif(Lettername == 'New Claims' or Lettername == 'Appeal Acknowledgement'): 
    #    expText = header + expText  
    #else:
    #    #expText = header + expText
    #    pass
    
    #print("***********expText is ************* \n" + expText)
    #print(lnkName + " -- "  +salutation + " -- "  +address + " -- "  +policyno + " -- "  +phNumber )
    driver.find_element_by_xpath("//input[contains(@id,'_next_cloned')]").click()
    wait_Sec(5)
    

    
    keyboard = Controller()
    keyboard.press(Key.left)
    keyboard.release(Key.left)

    wait_Sec(2)
    
    keyboard.press(Key.enter)
    keyboard.release(Key.enter)
    
    
    #driver.switch_to_alert().accept()
    wait_Sec(30)
    keyboard.press(Key.ctrl)
    keyboard.press('a')
    wait_Sec(2)
    keyboard.release('a')
    keyboard.release(Key.ctrl)
    
    keyboard.press(Key.ctrl)
    keyboard.press('c')
    wait_Sec(2)
    keyboard.release('c')
    keyboard.release(Key.ctrl)
    
    keyboard.press(Key.alt)
    keyboard.press('f')
    wait_Sec(2)
    keyboard.release('f')
    keyboard.release(Key.alt)
    wait_Sec(2)
    keyboard.press('c')
    keyboard.release('c')
    
    keyboard.press(Key.alt)
    keyboard.press(Key.f4)
    wait_Sec(1)
    keyboard.release(Key.alt)
    keyboard.release(Key.f4)
    
    
    
    import pyperclip
    actText = pyperclip.paste()
    
    import re
    
    actText = actText.replace("\r","").replace(",","")
    expText = expText.replace("\r","").replace(",","")
    
    if expText in actText:
            #print("pass")
            expText = re.sub(r'[^\x00-\x7f]',r'', expText)
            html_logger.dbg(Lettername + " Validation Passed. The Values Match as expected.\n The Values are \n" + expText)
    else:
            #print("fail") 
            #print(actText)
            #print("-----------------------")
            #print(expText)
            expText = re.sub(r'[^\x00-\x7f]',r'', expText)
            actText = re.sub(r'[^\x00-\x7f]',r'', actText)
            
            html_logger.err(Lettername + " Validation Failed. The Values doesn't Match as expected.\n The Expected Values are \n" + expText + "\n but the actual Values are \n" + actText)
    
    #print("***********actText is ************* \n" + actText)
    
    #output_list = [li for li in list(difflib.ndiff(expText,actText)) if li[0] != ' ']
    #print(output_list)
    
    #print(actText)
    #print("-----------------------")
    #print(expText)
      
    #driver.find_element_by_xpath("//body[@class='PageBody']").send_keys(Keys.CONTROL + 'a')
    #driver.find_element_by_xpath("//body[@class='PageBody']").send_keys(Keys.CONTROL + 'c')
    #driver.find_element_by_xpath("//body[@class='PageBody']").send_keys(Keys.ALT + 'f' +'c') 
    
    
options = webdriver.ChromeOptions()
driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)

##################################################################
import pandas as pd
rptdata = pd.read_excel(parentFolder + 'DisabilityClaims\\LetterValidation\\TestData.xls', sheet_name = "TestData", dtype=str)
for ind,row in rptdata.iterrows():
    if(row['Execute'] == 'Y'):
         html_logger.info("Start of " + rptdata['ID'][ind]) 
         LeterType = row['LeterType']
         BenType = row['BenType']
         Lettername = row['Lettername']
         expText = row['expText']
         claimNo = row['case_id']
         letters(LeterType,BenType,Lettername,expText,claimNo)
         html_logger.info("End of " + rptdata['ID'][ind])

try:
    driver.close()
except:
    pass          
##################################################################

































































############################################################################################################################
print("---------------------------Start of Closure- Survivor and Benefit Letter------------------------------------------") 
LeterType = 'Denial and Closure'
BenType = 'LTD'
Lettername = 'Closure- Survivor and Benefit'
expText = """American United Life Insurance Company® (AUL), a OneAmerica® company, is the long-term disability (LTD) insurance carrier for <<<PHName>>>. We wish to extend our condolences on behalf of your recent loss of your RELATIONSHIP TO CLAIMANT, Ms Albert Adam.
We have completed our calculation to determine the final benefits payable. You will be receiving a check under separate cover in the amount of $. Enclosed is a copy of our calculation for your review.
Add policy provision and explanation if policy allows for recovery if the LTD benefits were overpaid.
You may request a review of this determination by submitting your request in writing to:
OneAmerica Disability Claims Department                                                                                                  PO Box 7003                                                                                                                                          Indianapolis, Indiana 46207
This written request for review must be submitted within 180 days of your receipt of this letter. Your request should state any reasons why you feel this determination is incorrect, and should include any written comments, documents (i.e. any medical records that have not yet been submitted from a physician who is certifying your disability), records, or other information relating to your claim for benefits. Only one review will be allowed, and your request must be submitted within 180 days of your receipt of this letter to be considered.
Under normal circumstances, you will be notified in writing of the final determination within 45 days of the date we receive your request for review. If we determine that special circumstances require an extension of time for processing, you will ordinarily be notified of the decision no later than 90 days from the date we receive your request for review.
We will, upon specific request and free of charge, provide copies of all documents, records, and/or other information relevant to your claim for benefits. We will also, upon specific request and free of charge, provide copies of any internal rule, guideline, protocol or other similar criterion (if any) relied upon in making this determination. Other than the policy, we did not rely on any internal rules, guidelines, protocols, standards or other similar criteria in making this claim determination.
If your claim is subject to the Employee Retirement Income Security Act of 1974 ("the Act"), you have the right to bring a civil action under section 502(a) of the Act following an adverse benefit determination on review. Your failure to request a review within 180 days of your receipt of this letter may constitute a failure to exhaust the administrative remedies available under the Act, and may affect your ability to bring a civil action under the Act.
Nothing in this letter should be construed as a waiver of any of AUL’s rights and defenses under the above policy, and all these rights and defenses are reserved to the company, whether or not specifically mentioned herein.
STATE LANGUAGE, if applicable
Should you have any questions regarding this letter or your LTD claim please contact our call center toll free at 1-855-517-6365, Monday through Friday between the hours of 8:00 a.m. and 6:00 p.m., ET.
Sincerely,
System ID
System
Phone: 1-855-517-6365
Fax: 1-844-287-9499
If you need assistance in a language other than English, please contact OneAmerica at 1-855-517-6365.
Si necesita asistencia en un idioma que no sea inglés, póngase en contacto con OneAmerica llamando al 1-855-517-6365.
如果您需要英语以外其他语言的帮助，请通过 1-855-517-6365 联系 OneAmerica.
Ła’ da shíká adoolwoł nínízingo shá ata’ hodoolnih, kojį hodíílnih t’áá shǫǫdí OneAmerica at 1-855-517-6365.
Kung kailangan mo ng tulong sa isang wikang hindi Ingles, mangyaring makipag-ugnayan sa OneAmerica sa 1-855-517-6365.

Enclosures: [ ________________]
"""
exp1Text = """American United Life Insurance Company® (AUL), a OneAmerica® company, is the long-term disability (LTD) insurance carrier for West Side Unlimited. We wish to extend our condolences on behalf of your recent loss of your RELATIONSHIP TO CLAIMANT, Ms Albert Adam.
We have completed our calculation to determine the final benefits payable. You will be receiving a check under separate cover in the amount of $. Enclosed is a copy of our calculation for your review.
Add policy provision and explanation if policy allows for recovery if the LTD benefits were overpaid.
You may request a review of this determination by submitting your request in writing to:
OneAmerica Disability Claims Department                                                                                                  PO Box 7003                                                                                                                                          Indianapolis, Indiana 46207
This written request for review must be submitted within 180 days of your receipt of this letter. Your request should state any reasons why you feel this determination is incorrect, and should include any written comments, documents (i.e. any medical records that have not yet been submitted from a physician who is certifying your disability), records, or other information relating to your claim for benefits. Only one review will be allowed, and your request must be submitted within 180 days of your receipt of this letter to be considered.
Under normal circumstances, you will be notified in writing of the final determination within 45 days of the date we receive your request for review. If we determine that special circumstances require an extension of time for processing, you will ordinarily be notified of the decision no later than 90 days from the date we receive your request for review.
We will, upon specific request and free of charge, provide copies of all documents, records, and/or other information relevant to your claim for benefits. We will also, upon specific request and free of charge, provide copies of any internal rule, guideline, protocol or other similar criterion (if any) relied upon in making this determination. Other than the policy, we did not rely on any internal rules, guidelines, protocols, standards or other similar criteria in making this claim determination.
If your claim is subject to the Employee Retirement Income Security Act of 1974 ("the Act"), you have the right to bring a civil action under section 502(a) of the Act following an adverse benefit determination on review. Your failure to request a review within 180 days of your receipt of this letter may constitute a failure to exhaust the administrative remedies available under the Act, and may affect your ability to bring a civil action under the Act.
Nothing in this letter should be construed as a waiver of any of AUL’s rights and defenses under the above policy, and all these rights and defenses are reserved to the company, whether or not specifically mentioned herein.
STATE LANGUAGE, if applicable
Should you have any questions regarding this letter or your LTD claim please contact our call center toll free at 1-855-517-6365, Monday through Friday between the hours of 8:00 a.m. and 6:00 p.m., ET.
Sincerely,
System ID
System
Phone: 1-855-517-6365
Fax: 1-844-287-9499
If you need assistance in a language other than English, please contact OneAmerica at 1-855-517-6365.
Si necesita asistencia en un idioma que no sea inglés, póngase en contacto con OneAmerica llamando al 1-855-517-6365.
如果您需要英语以外其他语言的帮助，请通过 1-855-517-6365 联系 OneAmerica.
Ła’ da shíká adoolwoł nínízingo shá ata’ hodoolnih, kojį hodíílnih t’áá shǫǫdí OneAmerica at 1-855-517-6365.
Kung kailangan mo ng tulong sa isang wikang hindi Ingles, mangyaring makipag-ugnayan sa OneAmerica sa 1-855-517-6365.

Enclosures: [ ________________]
"""
#letters(LeterType,BenType,Lettername,expText)   
print("---------------------------End of Closure- Survivor and Benefit Letter------------------------------------------") 
############################################################################################################################

############################################################################################################################
print("---------------------------Start of LTD Acknowledgement Letter------------------------------------------") 
LeterType = 'New Claims'
BenType = 'LTD'
Lettername = 'LTD Acknowledgement'
expText = """I am writing to advise that American United Life Insurance Company® (AUL), a OneAmerica® company, the Long-Term Disability (LTD) carrier for your employer, has received your long-term Disability (LTD) claim forms.
For us to determine your eligibility for benefits, we are in need of additional information. A Claims Examiner will be attempting to reach you within the next 10 business days to complete a telephonic interview. As this interview is required to reach a determination on your claim, please make every effort to complete this call.
Additionally, please complete and return the following, enclosed form within <<<days>>> days of the date of this letter or by DATE:
•         Authorization for Release of Medical and Other Information
If this claim is determined to be payable, LTD benefits are scheduled to begin following the completion of the elimination period. 
If you have any questions about your claim, you may write directly to the address provided, or call our toll-free number 1-855-517-6365 between the hours of 8:00 a.m. and 6:00 p.m. ET, Monday through Friday.
Sincerely,
System ID
System
Phone: 1-855-517-6365
Fax: 1-844-287-9499


Enclosures: [ ________________]
"""
#letters(LeterType,BenType,Lettername,expText)   
print("---------------------------End of LTD Acknowledgement Letter------------------------------------------") 
############################################################################################################################

def Test():
    expText = """07/23/2020


Ms Albert Adam
2 shady ln  
Carmel IN 46032

Re:   Long-Term Disability Claim No.: DI-612
        Group: 006140170000000
        Policyholder: West Side Unlimited

Dear Ms Albert Adam:
I am writing to advise that American United Life Insurance Company® (AUL), a OneAmerica® company, the Long-Term Disability (LTD) carrier for your employer, has received your long-term Disability (LTD) claim forms.
For us to determine your eligibility for benefits, we are in need of additional information. A Claims Examiner will be attempting to reach you within the next 10 business days to complete a telephonic interview. As this interview is required to reach a determination on your claim, please make every effort to complete this call.
Additionally, please complete and return the following, enclosed form within 30 days of the date of this letter or by DATE:
•         Authorization for Release of Medical and Other Information
If this claim is determined to be payable, LTD benefits are scheduled to begin following the completion of the elimination period. 
If you have any questions about your claim, you may write directly to the address provided, or call our toll-free number 1-855-517-6365 between the hours of 8:00 a.m. and 6:00 p.m. ET, Monday through Friday.
Sincerely,
System ID
System
Phone: 1-855-517-6365
Fax: 1-844-287-9499


Enclosures: [ ________________]

"""
    
    actText = """07/23/2020


Ms Albert Adam
2 shady ln  
Carmel IN 46032

Re:   Long-Term Disability Claim No.: DI-612
        Group: 006140170000000
        Policyholder: West Side Unlimited

Dear Ms Albert Adam:
I am writing to advise that American United Life Insurance Company® (AUL), a OneAmerica® company, the Long-Term Disability (LTD) carrier for your employer, has received your long-term Disability (LTD) claim forms.
For us to determine your eligibility for benefits, we are in need of additional information. A Claims Examiner will be attempting to reach you within the next 10 business days to complete a telephonic interview. As this interview is required to reach a determination on your claim, please make every effort to complete this call.
Additionally, please complete and return the following, enclosed form within 30 days of the date of this letter or by DATE:
•         Authorization for Release of Medical and Other Information
If this claim is determined to be payable, LTD benefits are scheduled to begin following the completion of the elimination period. 
If you have any questions about your claim, you may write directly to the address provided, or call our toll-free number 1-855-517-6365 between the hours of 8:00 a.m. and 6:00 p.m. ET, Monday through Friday.
Sincerely,
System ID
System
Phone: 1-855-517-6365
Fax: 1-844-287-9499


Enclosures: [ ________________]



"""

    if expText.replace("\n","") in actText.replace("\n",""):
            print("pass")
            
    else:
            print("fail")

    output_list = [li for li in list(difflib.ndiff(expText,actText)) if li[0] != ' ']
    print(output_list)

#Test()    

       