# -*- coding: utf-8 -*-
#----------------Imports--------------------------
#import unittest
import datetime
import time
import logging
from collections import Counter

from lxml import etree
from pandas import ExcelFile
from pandas import ExcelWriter
import pytest
import pytest_html
import requests
import pyautogui

#import splinter
#from splinter import Browser

from Screenshot import Screenshot_Clipping
from pynput.keyboard import Key, Controller
import difflib

import json
import keyboard

from jsonpath_ng import jsonpath, parse


#from Python_Test1 import html_logger
import sys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


parentFolder = "C:\\Srikanth\\Eclipise_workspace_10142020\\Python_Projects\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

import html_logger
from html_logger import setup, info
import pandas as pd

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

#----------------Imports--------------------------
#----------------Logging--------------------------
now = datetime.datetime.now()

LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\CovLoader_FINEOSWEBSERVICES_"+ str(now.isoformat().replace(":","_")) + ".html"
#logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)
#setup("WebService_Test","1",LOG_FILENAME)

setup("WebService_Test","Executed on " + str(now),LOG_FILENAME)
html_logger.info("Start of driver")
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')


 
 
 
 
 
 
 
 
 

#----------------Logging--------------------------


'''----------------------------------------DBCall---------------------------------------------------------------------------
gets Pararmeter as dbServer, dbName and SQLQuery

ex: dbResult = dbCall(dbServer='SQLD10747,4242', dbName='ENTPRS_CLAIMS_DM', sqlQuery="select ADMIN_SYS_PARTY_KEY_TXT from dbo.ENTPRS_PARTY_VW p where ADMIN_SYS_PARTY_KEY_TXT like '0000004%' and 
                (cmpny_pref_name like 'test sub%' or PLCYHLDR_FRST_SUBSD_PREF_NAME like 'test sub%' or PLCYHLDR_SEC_SUBSD_PREF_NAME like 'test sub%' or PLCYHLDR_THIRD_SUBSD_PREF_NAME like 'test sub%' 
                or PLCYHLDR_FOURTH_SUBSD_PREF_NAME like 'test sub%')")
-------------------------------------------------------------------------------------------------------------------'''

def dbCall(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    list_result = list()

    for row in cursor:
        list_result.append(str(row[0]).strip())
        
      
    return ';'.join([str(elem) for elem in list_result]) 



def dbCall1(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    list_result = list()

    for row in cursor:
        list_result.append(str(row[0]).strip())
        
      
    return list_result
#----------------------------------------End of DBCall---------------------------------------------------------------------------



def dbCall_multi(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    #list_result = list()
    return cursor.fetchall()
 
    #return list_result



def dbretrieveData(recordset,columnname):
    row = ""
    list_result = list()
    for row in recordset:
        colval = eval("row." + columnname)
        if(colval != None):
            list_result.append(colval)      
    return list_result          


def getEnum(enum_id):
        try:
                wsQuery = "Select distinct Fineos_enum_instnc_name from qa.fineos_enum_xref where fineos_enum_id  in(" +  enum_id + ") and PM_CUR_IND = 'Y'"
                recordset1 = dbCall(dbServer='SQLD10747,4242', dbName='ENTPRS_CLAIMS_DM', sqlQuery=wsQuery)
                returnval = recordset1
        except:
                returnval = ''       
        return returnval
    
def changeFormat(date1):
        if(date1 != ''):
            return datetime.datetime.strptime(date1[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d/%Y')
        else:
            return ''
        
def changeFormatmmdd(date1):
        if(date1 != ''):
            return datetime.datetime.strptime(date1[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d')
        else:
            return ''

def compareString(FN_Value, WS_Value, FieldName):
    try:
        FN_Value = FN_Value.replace("'","")
        WS_Value = WS_Value.replace("'","")
        WS_Value = WS_Value.replace('"',"")
        WS_Value = WS_Value.replace(", ",";")
        WS_Value = WS_Value.replace(",",";")
        
        if(';' in WS_Value):
            WS_list = WS_Value.split(";")
            WS_list.sort()
            WS_Value = ';'.join(WS_list)
        
        if(';' in FN_Value):
            FN_list = FN_Value.split(";")
            FN_list.sort()
            FN_Value = ';'.join(FN_list)
            
                
        if(FN_Value == '-'):
            FN_Value = ''
        
        if(FN_Value == WS_Value):
            html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
        else:
            try:
                if(float(FN_Value.replace(",","")) == float(WS_Value.replace(",",""))):
                    html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
                else:
                    html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
            except:            
                html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
    except:
            html_logger.dbg("Error message when comparing " + FieldName + " Please check manually")   



def compareString_1(FN_Value, WS_Value, FieldName):
    try:
        FN_Value = FN_Value.replace("'","")
        WS_Value = WS_Value.replace("'","")
        WS_Value = WS_Value.replace('"',"")
        WS_Value = WS_Value.replace("  "," ")
        WS_Value = WS_Value.replace("  "," ") #re-remove the spaces
        if(';' in WS_Value):
            WS_list = WS_Value.split(";")
            WS_list.sort()
            WS_Value = ';'.join(WS_list)
        
        if(';' in FN_Value):
            FN_list = FN_Value.split(";")
            FN_list.sort()
            FN_Value = ';'.join(FN_list)
            
                
        if(FN_Value == '-'):
            FN_Value = ''
        
        if(FN_Value == WS_Value):
            html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
        else:
            try:
                if(float(FN_Value.replace(",","")) == float(WS_Value.replace(",",""))):
                    html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
                else:
                    html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
            except:            
                html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
    except:
            html_logger.dbg("Error message when comparing " + FieldName + " Please check manually")   




            
            
def compareStringContains(FN_Value, WS_Value, FieldName):             
    try:
        FN_Value = FN_Value.replace("'","")
        WS_Value = WS_Value.replace("'","")
        WS_Value = WS_Value.replace(", ",";")
        WS_Value = WS_Value.replace(",",";")
        
        if(';' in WS_Value):
            WS_list = WS_Value.split(";")
            WS_list.sort()
            WS_Value = ';'.join(WS_list)
        
        if(';' in FN_Value):
            FN_list = FN_Value.split(";")
            FN_list.sort()
            FN_Value = ';'.join(FN_list)
            
                
        if(FN_Value == '-'):
            FN_Value = ''
        
        if(WS_Value in FN_Value):
            html_logger.dbg(FieldName + " Validation Passed. Fineos Value matches with WebService value. The value is " + FN_Value)
        else:     
            html_logger.err(FieldName + " Validation Failed. Fineos Value doesn't matches with WebService value. The Fin value is " + FN_Value + " but the WebService value is " + WS_Value)
    except:
            html_logger.dbg("Error message when comparing " + FieldName + " Please check manually")  
            
                
'''
recordset1 = dbCall_multi("SQLD10747,4242","ENTPRS_CLAIMS_DM", "Select ADMIN_SYS_NAME, PARTY_TYP_CD,PARTY_ROLE_TYP_TXT,ADMIN_SYS_PARTY_KEY_TXT,CMPNY_PREF_NAME,BILL_CNTCT_NAME,PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,RGNL_GRP_OFF_NBR from dbo.ENTPRS_PARTY_VW e where ADMIN_SYS_PARTY_KEY_TXT like '00000021%'")  
AdminSysName = dbretrieveData(recordset1,"ADMIN_SYS_NAME")
cmpnyPrefname = dbretrieveData(recordset1,"CMPNY_PREF_NAME")
BILL_CNTCT_NAME = dbretrieveData(recordset1,"BILL_CNTCT_NAME")
print(str(AdminSysName))
print(str(cmpnyPrefname))
print(str(BILL_CNTCT_NAME))
'''

# "Select ADMIN_SYS_NAME, PARTY_TYP_CD,PARTY_ROLE_TYP_TXT,ADMIN_SYS_PARTY_KEY_TXT,CMPNY_PREF_NAME,BILL_CNTCT_NAME,PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,RGNL_GRP_OFF_NBR from dbo.ENTPRS_PARTY_VW e where ADMIN_SYS_PARTY_KEY_TXT like '00000006%'"

'''----------------------------------------SOAPPost---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)

ex: dbResult = wsResult = SOAPPost(url=url1,reqBody=body1,reqHeader=headers1,reqPath=reqPath1)
-------------------------------------------------------------------------------------------------------------------'''

def SOAPPost(url,reqBody,reqHeader,reqPath):
    response = requests.post(url,data=reqBody,headers=reqHeader)
    rsp = response.content
    tree = etree.fromstring(rsp)
    doc = etree.ElementTree(tree)

    find_val1 = etree.XPath(reqPath)
    return find_val1(tree)
    
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------



def SOAPPost_Response(url,reqBody,reqHeader):
    response = requests.post(url,data=reqBody,headers=reqHeader)
    rsp = response.content
    tree = etree.fromstring(rsp)
    doc = etree.ElementTree(tree)
    return tree



def SOAPPost_getNodeValue(tree,reqPath):
    find_val1 = etree.XPath(reqPath)
    
    return find_val1(tree)


'''----------------------------------------JSONGet---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)

ex: dbResult = wsResult = JSONGet(url=url1,reqHeader=headers1,reqPath=reqPath1)
-------------------------------------------------------------------------------------------------------------------'''

def JSONGet(url,reqHeader,reqPath):
    response = requests.get(url,headers=reqHeader)
    rsp = response.content
    
    json_data = json.loads(rsp)
    jsonpath_expression = parse(reqPath)
    match = jsonpath_expression.find(json_data)
    return {match[0].value}
    
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------

def wait_Sec(secs):
    time.sleep(secs)

def Sync(xpth):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))



def Click(xpth):
    try:
        wait = WebDriverWait(driver,10)
        wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
        driver.find_element_by_xpath(xpth).click()
    except:
        Click(xpth)    
    html_logger.dbg("Clicked " + xpth + " element successfully")


def Set(xpth,val1):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).send_keys(str(val1))
    html_logger.dbg("Set the value of " + val1 + " to the " + xpth + " element successfully")
    
def GetText(xpth): 
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    return str(driver.find_element_by_xpath(xpth).text)
       
'''----------------------------------------Fineos Login---------------------------------------------------------------------------
gets Pararmeter as Url and Search Type

ex: Login(url=url1,SearchType)
-------------------------------------------------------------------------------------------------------------------'''

def Login(url,searchType):
    
    driver.get(url)
    driver.maximize_window()
    if(searchType == 'OrgSearch'):
        xpth_srchPartyLink = "//a[contains(@id,'_MENUITEM.SearchPartieslink')]"
        Click(xpth_srchPartyLink)
        #driver.implicitly_wait(5)
    
        xpth_rdoOrganization = "//input[contains(@id,'_Organisation_GROUP')]"
        #Sync(xpth_rdoOrganization)
        Click(xpth_rdoOrganization)



def Logout():
    xpth_Useroption = "//a[contains(@id,'UserOptionslink')]"
    Click(xpth_Useroption) 
    
    xpth_Logout = "//a[contains(@id,'MENUITEM.Logoutlink')]"
    Click(xpth_Logout)   
    
    xpth_Confirm = "//input[contains(@id,'_Logout_yes')]"
    Click(xpth_Confirm)
    
    driver.close()


def screenshot(path):
    screenshot = pyautogui.screenshot()
    screenshot.save(path)
    html_logger.dbg('Screenshot saved in ' + path)



def screenshot1(path):
    
    ob = Screenshot_Clipping.Screenshot()
    img_url=ob.full_Screenshot(driver, save_path= path)
    html_logger.dbg('Screenshot saved in ' + img_url)


                             
'''
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical error message')
'''






























































def validate_covLoader(policyno,classDiv):
    url = "https:CONTENT:password1@idt-claims-webapp.oneamerica.fineos.com"
    WSURL = "https://internal-apigw-nonprod.oneamerica.com/claims-dev/coverage/disability/search"
    caseno = "DI-671"
    
    #url = "https://uat-claims-webapp.oneamerica.fineos.com"
    #WSURL = "https://internal-apigw-nonprod.oneamerica.com/claims-stg/coverage/disability/search"
    #caseno = "DI-62"
    
    searchType = "OrgSearch"
    
    
    #policyNo = "006038230000000"        #"006036490001000" # "006181030000000" #     
    #classDiv = "3175-VLD-04/01/2001"   #"35-CSP-05/01/2001" #  "100-WDS-01/01/2019" # 
    incDate = "2020-07-01T00:00:00"
    
   
    WSBody = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <soapenv:Header/> 
                <soapenv:Body>
                <p:GroupDICoverageLoaderServiceRequest xmlns:p="http://www.fineos.com/wscomposer/GroupDICoverageLoaderService">
                <groupPolicyReferenceNumber>""" + policyNo + """</groupPolicyReferenceNumber>
                <memberID></memberID>
                <policyReference3></policyReference3>
                <policyReference4></policyReference4>
                <policyReference5></policyReference5>
                <incurredDate>""" + incDate + """</incurredDate>
                <classesAndDivisionsList>""" + classDiv + """</classesAndDivisionsList>
                <additional-data-set/>
                </p:GroupDICoverageLoaderServiceRequest>
                </soapenv:Body>
                </soapenv:Envelope>  
            """
    WSHeader = {'content-type': 'text/xml'}        
    respText = SOAPPost_Response(WSURL,WSBody,WSHeader)
    #tree1 = SOAPPost_Response(url1,reqBody1,reqHeader1)
    #-----------------------------------------------------------------------
    WSURL1 = "https://internal-apigw-nonprod.oneamerica.com/claims-dev/policies/search"
    WSBody1 = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:org="http://www.fineos.com/wscomposer/OrganisationSearchIntegration">
                 <soapenv:Header/>  <soapenv:Body>
                <p:PolicySearchIntegrationRequest xmlns:p='http://www.fineos.com/wscomposer/PolicySearchIntegration'>
                <partyReferenceNo></partyReferenceNo>
                <partySourceSystem>External</partySourceSystem>
                <policyNumber1>""" + policyNo + """</policyNumber1>
                <policyNumber2></policyNumber2>
                <policyNumber3></policyNumber3>
                <policyNumber4></policyNumber4>
                <policyNumber5></policyNumber5>
                <policyAdminSystem></policyAdminSystem>
                <divisionId></divisionId>
                <divisionName></divisionName>
                <classId></classId>
                <className></className>
                <employerName></employerName>
                <employerCustomerNumber></employerCustomerNumber>
                <employerRegistrationNumber></employerRegistrationNumber>
                </p:PolicySearchIntegrationRequest>
                </soapenv:Body>
                </soapenv:Envelope> 
            """
    WSHeader1 = {'content-type': 'text/xml'}        
    respText1 = SOAPPost_Response(WSURL1,WSBody1,WSHeader1)
    #---------------------------------------------------------------------------
    reqNode1 = "//OCContractStub/AdditionalDetails/text()"
    WS_AdditionalDetails =  str(SOAPPost_getNodeValue(respText1,reqNode1)).strip('[]')   
              
    reqNode = "//OLContract/EffectiveDate/text()"
    WS_StartDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    #WS_StartDate = datetime.datetime.strptime(WS_StartDate[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d/%Y')
    WS_StartDate = changeFormat(WS_StartDate)
    
    reqNode = "//OLContract/EndDate/text()"
    WS_EndDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    #WS_EndDate = datetime.datetime.strptime(WS_EndDate[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d/%Y')
    WS_EndDate = changeFormat(WS_EndDate)
    
    reqNode = "//OLContract/TerminationDate/text()"
    WS_TermDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    #WS_TermDate = datetime.datetime.strptime(WS_TermDate[:-16].strip("'"),'%Y-%m-%d').strftime('%m/%d/%Y')
    WS_TermDate = changeFormat(WS_TermDate)
    
    reqNode = "//OLContract/Coverage/FullId/text()"
    WS_Coverage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Coverage_Enum = getEnum(str(WS_Coverage).strip('[]'))
    
    reqNode = "//OLContract/CoverageCode/FullId/text()"
    WS_CoverageCode = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CoverageCode_Enum = getEnum(str(WS_CoverageCode).strip('[]'))
    
    reqNode = "//OLContract/Source/FullId/text()"
    WS_Source = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Source_Enum = getEnum(str(WS_Source).strip('[]'))
    
    reqNode = "//OLContract/ContractStatus/FullId/text()"
    WS_ContractStatus = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_ContractStatus_Enum = getEnum(str(WS_ContractStatus).strip('[]'))
    
    reqNode = "//OLContract/ERISA/text()"
    WS_ERISA = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/GracePeriod/text()"
    WS_GracePeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/SpecialHandling/text()"
    WS_SpecialHandling = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")    
    
    reqNode = "//OLContract/LastPolicyReviewDate/text()"
    WS_LastPolicyReviewDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_LastPolicyReviewDate = changeFormat(WS_LastPolicyReviewDate)
    
    reqNode = "//OLContract/LastRefreshDate/text()"
    WS_LastRefreshDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_LastRefreshDate = changeFormat(WS_LastRefreshDate)
    
    reqNode = "//OLContract/LatestReinstatementDate/text()"
    WS_LatestReinstatementDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_LatestReinstatementDate = changeFormat(WS_LatestReinstatementDate)
    
    reqNode = "//OLContract/PremPaidToDate/text()"
    WS_PremPaidToDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_PremPaidToDate = changeFormat(WS_PremPaidToDate)
    
    #--------------------------------------------------------------------------------------------------------------------------------
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/EffectiveDate/text()"
    WS_EffectiveDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_EffectiveDate = changeFormat(WS_EffectiveDate)

    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/EndDate/text()"
    WS_CapAssu_EndDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_EndDate = changeFormat(WS_CapAssu_EndDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/Coverage/FullId/text()"
    WS_CapAssu_Coverage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Coverage_Enum = getEnum(str(WS_CapAssu_Coverage).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/CoverageCode/FullId/text()"
    WS_CapAssu_CoverageCode = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_CoverageCode_Enum = getEnum(str(WS_CapAssu_CoverageCode).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/Source/FullId/text()"
    WS_CapAssu_Source = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Source_Enum = getEnum(str(WS_CapAssu_Source).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/TerminationDate/text()"
    WS_CapAssu_TerminationDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_TerminationDate = changeFormat(WS_CapAssu_TerminationDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/Status/FullId/text()"
    WS_CapAssu_Status = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Status_Enum = getEnum(str(WS_CapAssu_Status).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/EDOCCalculationType/FullId/text()"
    WS_CapAssu_EDOCCalculationType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_EDOCCalculationType_Enum = getEnum(str(WS_CapAssu_EDOCCalculationType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/MinimumHoursForEligibility/text()"
    WS_MinimumHoursForEligibility = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/SupportedBillingMethod/FullId/text()"
    WS_CapAssu_SupportedBillingMethod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_SupportedBillingMethod_Enum = getEnum(str(WS_CapAssu_SupportedBillingMethod).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/coverageDivisionClass/OLCoverageDivisionClass/DivisionName/text()"
    WS_DivisionName = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/coverageDivisionClass/OLCoverageDivisionClass/ClassName/text()"
    WS_ClassName = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")

    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/coverageDivisionClass/OLCoverageDivisionClass/ClassDescription/text()"
    WS_ClassDescription = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'").strip('"')
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/coverageDivisionClass/OLCoverageDivisionClass/DivisionID/text()"
    WS_DivisionID = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")


    #------------------------------------------------------------------
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/AutoAcceptanceLimit/Amount/text()"
    WS_AutoAcceptanceLimit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'").strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/LateEnrollmentPeriodBasis/FullId/text()"
    WS_CapAssu_LateEnrollmentPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_LateEnrollmentPeriodBasis_Enum = getEnum(str(WS_CapAssu_LateEnrollmentPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MinimumQualifyPeriodBasis/FullId/text()"
    WS_CapAssu_MinimumQualifyPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_MinimumQualifyPeriodBasis_Enum = getEnum(str(WS_CapAssu_MinimumQualifyPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/WaitingPeriodBasis/FullId/text()"
    WS_CapAssu_WaitingPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_WaitingPeriodBasis_Enum = getEnum(str(WS_CapAssu_WaitingPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/Type/FullId/text()"
    WS_CapAssu_Type = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Type_Enum = getEnum(str(WS_CapAssu_Type).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/ContestablePeriodApplicable/text()"
    WS_ContestablePeriodApplicable = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/ContestableReviewMonths/text()"
    WS_ContestableReviewMonths = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/EffectiveDateOfCoverage/text()"
    WS_CapAssu_EffectiveDateOfCoverage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_EffectiveDateOfCoverage = changeFormat(WS_CapAssu_EffectiveDateOfCoverage)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/EmpContStatus/FullId/text()"
    WS_CapAssu_EmpContStatus = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_EmpContStatus_Enum = getEnum(str(WS_CapAssu_EmpContStatus).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/EmployeeContributionPercentage/text()"
    WS_EmployeeContributionPercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/EndDateCalculationType/FullId/text()"
    WS_CapAssu_EndDateCalculationType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_EndDateCalculationType_Enum = getEnum(str(WS_CapAssu_EndDateCalculationType).strip('[]'))
    #--------------------------------
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/LastRefreshDate/text()"
    WS_CapAssu_LastRefreshDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_LastRefreshDate = changeFormat(WS_CapAssu_LastRefreshDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/LateEnrollmentPeriod/text()"
    WS_LateEnrollmentPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/LatestDateforClaim/text()"
    WS_CapAssu_LatestDateforClaim = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_LatestDateforClaim = changeFormat(WS_CapAssu_LatestDateforClaim)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/LimitAdjustmentTypes/text()"
    WS_LimitAdjustmentTypes = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MinimumQualifyPeriod/text()"
    WS_MinimumQualifyPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/Reinsured/text()"
    WS_Reinsured = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/Source/FullId/text()"
    WS_CapAssu_Benefit_Source = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Benefit_Source_Enum = getEnum(str(WS_CapAssu_Benefit_Source).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/SourceOfEDOC/FullId/text()"
    WS_CapAssu_Benefit_SourceOfEDOC = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Benefit_SourceOfEDOC_Enum = getEnum(str(WS_CapAssu_Benefit_SourceOfEDOC).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/StartDateCalculationType/FullId/text()"
    WS_CapAssu_Benefit_StartDateCalculationType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_Benefit_StartDateCalculationType_Enum = getEnum(str(WS_CapAssu_Benefit_StartDateCalculationType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/Underwritten/text()"
    WS_Underwritten= str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/WaitingPeriod/text()"
    WS_WaitingPeriod= str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    #------------------------------------------------------------------------------
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/AccidentEliminationPeriod/text()"
    WS_AccidentEliminationPeriod= str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/AccumulateEliminationPeriods/text()"
    WS_AccumulateEliminationPeriods= str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/AccidentEliminationPeriodBasis/FullId/text()"
    WS_CapAssu_ELimDet_AccidentEliminationPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_ELimDet_AccidentEliminationPeriodBasis_Enum = getEnum(str(WS_CapAssu_ELimDet_AccidentEliminationPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/SicknessEliminationPeriodBasis/FullId/text()"
    WS_CapAssu_ELimDet_SicknessEliminationPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_ELimDet_SicknessEliminationPeriodBasis_Enum = getEnum(str(WS_CapAssu_ELimDet_SicknessEliminationPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/EliminationDays/text()"
    WS_EliminationDays = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/SicknessEliminationPeriod/text()"
    WS_SicknessEliminationPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/EliminationAmount/Amount/text()"
    WS_Sickness_Amount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/HospitalEliminationPeriodBasis/FullId/text()"
    WS_CapAssu_ELimDet_HospitalEliminationPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_ELimDet_HospitalEliminationPeriodBasis_Enum = getEnum(str(WS_CapAssu_ELimDet_HospitalEliminationPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/HospitalEliminationPeriod/text()"
    WS_Sickness_HospitalEliminationPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eliminationPeriods/OLEliminationDetails/HospitalizationClauseApplies/text()"
    WS_Sickness_HospitalizationClauseApplies = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    #------------------------------------------------------------------------
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eligibilityLimits/OLEligibilityLimits/EligibilityLimitsType/FullId/text()"
    WS_CapAssu_EligibilityLimitsType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_CapAssu_EligibilityLimitsType_Enum = getEnum(str(WS_CapAssu_EligibilityLimitsType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/eligibilityLimits/OLEligibilityLimits/MaximumAgeLimit/text()"
    WS_Elig_MaximumAgeLimit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/AveragingPeriod/text()"
    WS_Salary_AveragingPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/ChangeOfEarningsAssessmentDate/FullId/text()"
    WS_Salary_ChangeOfEarningsAssessmentDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_ChangeOfEarningsAssessmentDate_Enum = getEnum(str(WS_Salary_ChangeOfEarningsAssessmentDate).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/EarningsBasis/FullId/text()"
    WS_Salary_EarningsBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_EarningsBasis_Enum = getEnum(str(WS_Salary_EarningsBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/EarningsDefinition/FullId/text()"
    WS_Salary_EarningsDefinition = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_EarningsDefinition_Enum = getEnum(str(WS_Salary_EarningsDefinition).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/EarningsEffectiveDate/text()"
    WS_Salary_EarningsEffectiveDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_EarningsEffectiveDate = changeFormat(WS_Salary_EarningsEffectiveDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/PreIncidentGrossEarningsAtPaymentFrequency/Amount/text()"
    WS_Salary_PayFreqAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/MaximumGrossEarnings/Amount/text()"
    WS_Salary_MaximumGrossEarnings = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/MaximumNetEarnings/Amount/text()"
    WS_Salary_MaximumNetEarnings = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/PreIncidentNetEarningsAtPaymentFrequency/Amount/text()"
    WS_Salary_PreIncidentNetEarningsAtPaymentFrequency = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/PeriodBasis/FullId/text()"
    WS_Salary_PeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_PeriodBasis_Enum = getEnum(str(WS_Salary_PeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/RoundingPrecision/FullId/text()"
    WS_Salary_RoundingPrecision = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_RoundingPrecision_Enum = getEnum(str(WS_Salary_RoundingPrecision).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/RoundingRule/FullId/text()"
    WS_Salary_RoundingRule = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Salary_RoundingRule_Enum = getEnum(str(WS_Salary_RoundingRule).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/salaryParameters/OLSalaryParameters/UseGrossSalaryForGBA/text()"
    WS_Salary_UseGrossSalaryForGBA = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    #-------------------------------------------------
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/conditions/OLCondition/Duration/text()"
    WS_Condition_Duration = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/conditions/OLCondition/DurationUnit/FullId/text()"
    WS_Condition_DurationUnit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Condition_DurationUnit_Enum = getEnum(str(WS_Condition_DurationUnit).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/conditions/OLCondition/Status/FullId/text()"
    WS_Condition_Status = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Condition_Status_Enum = getEnum(str(WS_Condition_Status).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/conditions/OLCondition/Type/FullId/text()"
    WS_Condition_Type = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Condition_Type_Enum = getEnum(str(WS_Condition_Type).strip('[]'))
       
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/preExistCondition/OLPreExistConditionDetails/TimeToClaimMonths/text()"
    WS_PreExCondition_TimeToClaimMonths = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/preExistCondition/OLPreExistConditionDetails/ReviewPeriodPriorToCoverMonths/text()"
    WS_PreExCondition_ReviewPeriodPriorToCoverMonths = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/preExistCondition/OLPreExistConditionDetails/TreatmentFreePeriodMonths/text()"
    WS_PreExCondition_TreatmentFreePeriodMonths = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    #--------------------------------------------------------------------------
     
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/AgeLimit/text()"
    WS_amt_ageLimit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/PeriodBasis/FullId/text()"
    WS_amt_PeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_amt_PeriodBasis_Enum = getEnum(str(WS_amt_PeriodBasis).strip('[]'))
     
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/PeriodEnd/text()"
    WS_amt_PeriodEnd = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/PeriodStart/text()"
    WS_amt_PeriodStart = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/tiers/OLTier/EndAmount/Amount/text()"
    WS_amt_EndAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/tiers/OLTier/FixedAmountPerFrequency/Amount/text()"
    WS_amt_FixedAmountPerFrequency = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/tiers/OLTier/StartAmount/Amount/text()"
    WS_amt_StartAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/amountDefinitionIntervals/OLAmountDefinitionInterval/tiers/OLTier/Percentage/text()"
    WS_amt_Percentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    #-----------------------------------------------------------------------
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/EffectiveDate/text()"
    WS_RegPreComp_EffectiveDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_EffectiveDate = changeFormat(WS_RegPreComp_EffectiveDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/EndDate/text()"
    WS_RegPreComp_EndDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_EndDate = changeFormat(WS_RegPreComp_EndDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/Coverage/FullId/text()"
    WS_RegPreComp_Coverage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_Coverage_Enum = getEnum(str(WS_RegPreComp_Coverage).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/CoverageCode/FullId/text()"
    WS_RegPreComp_CoverageCode = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_CoverageCode_Enum = getEnum(str(WS_RegPreComp_Coverage).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/Source/FullId/text()"
    WS_RegPreComp_Source = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_Source_Enum = getEnum(str(WS_RegPreComp_Source).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/TerminationDate/text()"
    WS_RegPreComp_TerminationDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_TerminationDate = changeFormat(WS_RegPreComp_TerminationDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/AGERoundingPrecision/FullId/text()"
    WS_RegPreComp_AGERoundingPrecision = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_AGERoundingPrecision_Enum = getEnum(str(WS_RegPreComp_AGERoundingPrecision).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/AGERoundingRule/FullId/text()"
    WS_RegPreComp_AGERoundingRule = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_AGERoundingRule_Enum = getEnum(str(WS_RegPreComp_AGERoundingRule).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/DuesCreatedInArrears/text()"
    WS_RegPreComp_DuesCreatedInArrears = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'") 
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/FrequencyAmount/Amount/text()"
    WS_RegPreComp_FrequencyAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MaximumAggregateAmount/Amount/text()"
    WS_RegPreComp_MaximumAggregateAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
     
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MaximumAmount/Amount/text()"
    WS_RegPreComp_MaximumAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MaximumCalcType/FullId/text()"
    WS_RegPreComp_MaximumCalcType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_MaximumCalcType_Enum = getEnum(str(WS_RegPreComp_MaximumCalcType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MaximumPercentage/text()"
    WS_RegPreComp_MaximumPercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MinimumAmount/Amount/text()"
    WS_RegPreComp_MinimumAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MinimumCalcType/FullId/text()"
    WS_RegPreComp_MinimumCalcType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_MinimumCalcType_Enum = getEnum(str(WS_RegPreComp_MinimumCalcType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/MinimumPercentage/text()"
    WS_RegPreComp_MinimumPercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    #-----------------------------------------------------------------------
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/Amount/Amount/text()"
    WS_RegPreComp_LocAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/BalancingAmount/Amount/text()"
    WS_RegPreComp_LocBalancingAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/BusinessNBACalcOverride/text()"
    WS_RegPreComp_BusinessNBACalcOverride = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/BusinessNBAChangeReason/FullId/text()"
    WS_RegPreComp_BusinessNBAChangeReason = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/BusinessNBAEffectiveDate/text()"
    WS_RegPreComp_BusinessNBAEffectiveDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_BusinessNBAEffectiveDate = changeFormat(WS_RegPreComp_BusinessNBAEffectiveDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/BusinessNetBenefitAmount/Amount/text()"
    WS_RegPreComp_LocAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/GrossBenAmtCalcOverride/text()"
    WS_RegPreComp_LocAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/GrossBenAmtChangeReason/FullId/text()"
    WS_RegPreComp_GrossBenAmtChangeReason = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_GrossBenAmtChangeReason_Enum = getEnum(str(WS_RegPreComp_GrossBenAmtChangeReason).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/GrossBenAmtEffectiveDate/text()"
    WS_RegPreComp_GrossBenAmtEffectiveDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_GrossBenAmtEffectiveDate = changeFormat(WS_RegPreComp_BusinessNBAEffectiveDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/InitialAmount/Amount/text()"
    WS_RegPreComp_InitialAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/InterestRate/text()"
    WS_RegPreComp_InterestRate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/IntrstOvrride/text()"
    WS_RegPreComp_IntrstOvrride = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/NetPaymentAmount/Amount/text()"
    WS_RegPreComp_NetPaymentAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/SuppressTaxCall/text()"
    WS_RegPreComp_SuppressTaxCall = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/TaxOverride/FullId/text()"
    WS_RegPreComp_TaxOverride = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_TaxOverride_Enum = getEnum(str(WS_RegPreComp_TaxOverride).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/TaxWageAmount/Amount/text()"
    WS_RegPreComp_TaxWageAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/Anniversary/text()"
    WS_RegPreComp_Anniversary= str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_Anniversary = changeFormatmmdd(WS_RegPreComp_Anniversary)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/EliminationPeriodBasis/FullId/text()"
    WS_RegPreComp_EliminationPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_RegPreComp_EliminationPeriodBasis_Enum = getEnum(str(WS_RegPreComp_EliminationPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/EliminationPeriod/text()"
    WS_RegPreComp_EliminationPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/AdjustBenefit/text()"
    WS_RegPreComp_AdjustBenefit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/IndexationApplies/text()"
    WS_RegPreComp_IndexationApplies = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/MaxCumulativeRate/text()"
    WS_RegPreComp_MaxCumulativeRate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/MaximumRate/text()"
    WS_RegPreComp_MaximumRate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/MaxNumberCOLAs/text()"
    WS_RegPreComp_MaxNumberCOLAs = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/realAmount/OLLocalMoneyAmount/indexationGenerator/OLIndexationGenerator/MinimumRate/text()"
    WS_RegPreComp_MinimumRate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    #------------------------------------------------------------------------------
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/CalculationPeriodProRatingRule/FullId/text()"
    WS_paymentFrequencyHistories_CalculationPeriodProRatingRule = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_paymentFrequencyHistories_CalculationPeriodProRatingRule_Enum = getEnum(str(WS_paymentFrequencyHistories_CalculationPeriodProRatingRule).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/EffectiveDate/text()"
    WS_paymentFrequencyHistories_EffectiveDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_paymentFrequencyHistories_EffectiveDate = changeFormat(WS_paymentFrequencyHistories_EffectiveDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/PaymentCycleStartDate/text()"
    WS_paymentFrequencyHistories_PaymentCycleStartDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_paymentFrequencyHistories_PaymentCycleStartDate = changeFormat(WS_paymentFrequencyHistories_PaymentCycleStartDate)
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/PaymentDueDay/FullId/text()"
    WS_pmentFreqHist_PaymentDueDay = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_pmentFreqHist_PaymentDueDay_Enum = getEnum(str(WS_pmentFreqHist_PaymentDueDay).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/PremiumPaymentFrequency/FullId/text()"
    WS_pmentFreqHist_PremiumPaymentFrequency = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_pmentFreqHist_PremiumPaymentFrequency_Enum = getEnum(str(WS_pmentFreqHist_PremiumPaymentFrequency).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/RenewalDay/text()"
    WS_RegPreComp_RenewalDay = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
   
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/paymentDetails/OLRegularPremiumComponent/paymentFrequencyHistories/OLPaymentFrequencyHistory/RenewalDayBasis/FullId/text()"
    WS_pmentFreqHist_RenewalDayBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_pmentFreqHist_RenewalDayBasis_Enum = getEnum(str(WS_pmentFreqHist_RenewalDayBasis).strip('[]'))
    #---------------------------------------------------------------------------------------------------------------
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MaximumBenefitPeriodAccidentBasis/FullId/text()"
    WS_BenRit_MaximumBenefitPeriodAccidentBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_MaximumBenefitPeriodAccidentBasis_Enum = getEnum(str(WS_BenRit_MaximumBenefitPeriodAccidentBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MaximumBenefitPeriodAccident/text()"
    WS_RegPreComp_MaximumBenefitPeriodAccident = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/AdviceToPay/text()"
    WS_RegPreComp_AdviceToPay = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/AdviceToPayOverride/FullId/text()"
    WS_BenRit_AdviceToPayOverride = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_AdviceToPayOverride_Enum = getEnum(str(WS_BenRit_AdviceToPayOverride).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MaximumBenefitPeriodBasis/FullId/text()"
    WS_BenRit_MaximumBenefitPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_MaximumBenefitPeriodBasis_Enum = getEnum(str(WS_BenRit_MaximumBenefitPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MinimumBenefitPeriodBasis/FullId/text()"
    WS_BenRit_MinimumBenefitPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_MinimumBenefitPeriodBasis_Enum = getEnum(str(WS_BenRit_MinimumBenefitPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/CheckCutting/FullId/text()"
    WS_BenRit_CheckCutting = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_CheckCutting_Enum = getEnum(str(WS_BenRit_CheckCutting).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MaximumBenefitPeriodHospitalBasis/FullId/text()"
    WS_BenRit_MaximumBenefitPeriodHospitalBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_MaximumBenefitPeriodHospitalBasis_Enum = getEnum(str(WS_BenRit_MaximumBenefitPeriodHospitalBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MaximumBenefitPeriodHospital/text()"
    WS_RegPreComp_MaximumBenefitPeriodHospital = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MaximumBenefitPeriod/text()"
    WS_RegPreComp_MaximumBenefitPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/MinimumBenefitPeriod/text()"
    WS_RegPreComp_MinimumBenefitPeriod= str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/ReinsurerLiabilityApprovedToDate/text()"
    WS_paymentFrequencyHistories_ReinsurerLiabilityApprovedToDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_paymentFrequencyHistories_ReinsurerLiabilityApprovedToDate = changeFormat(WS_paymentFrequencyHistories_ReinsurerLiabilityApprovedToDate)
    #-------------------------------------------------------------------
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/BenefitQualificationStartDate/FullId/text()"
    WS_BenRit_BenefitQualificationStartDate = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_BenefitQualificationStartDate_Enum = getEnum(str(WS_BenRit_BenefitQualificationStartDate).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/BenQualificationReqType/FullId/text()"
    WS_BenRit_BenQualificationReqType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_BenQualificationReqType_Enum = getEnum(str(WS_BenRit_BenQualificationReqType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/CODPeriod/text()"
    WS_BenRit_CODPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/CODPeriodBasis/FullId/text()"
    WS_BenRit_CODPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_CODPeriodBasis_Enum = getEnum(str(WS_BenRit_CODPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/OverridePercentage/text()"
    WS_BenRit_OverridePercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/BasisForPercentageAmount/FullId/text()"
    WS_BenRit_BasisForPercentageAmount = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_BasisForPercentageAmount_Enum = getEnum(str(WS_BenRit_BasisForPercentageAmount).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/BasisForUnit/FullId/text()"
    WS_BenRit_BasisForUnit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_BasisForUnit_Enum = getEnum(str(WS_BenRit_BasisForUnit).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/RecurrentBenefitPeriodBasis/FullId/text()"
    WS_BenRit_RecurrentBenefitPeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_RecurrentBenefitPeriodBasis_Enum = getEnum(str(WS_BenRit_RecurrentBenefitPeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/CalculationInputs/text()"
    WS_BenRit_CalculationInputs = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/ExtendPeriodForPartial/text()"
    WS_BenRit_ExtendPeriodForPartial = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/BenefitCalculationType/FullId/text()"
    WS_BenRit_BenefitCalculationType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_BenefitCalculationType_Enum = getEnum(str(WS_BenRit_BenefitCalculationType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/MinimumHoursWorkRequired/text()"
    WS_BenRit_MinimumHoursWorkRequired = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/NumberOfUnits/text()"
    WS_BenRit_NumberOfUnits = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/Percentage/text()"
    WS_BenRit_Percentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/PresumptiveDisabilityApplies/text()"
    WS_BenRit_PresumptiveDisabilityApplies = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/RecurrentBenefitPeriod/text()"
    WS_BenRit_RecurrentBenefitPeriod = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/ReturnToWorkCalculationType/FullId/text()"
    WS_BenRit_ReturnToWorkCalculationType = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_ReturnToWorkCalculationType_Enum = getEnum(str(WS_BenRit_ReturnToWorkCalculationType).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/SuccessiveDisabilityDaysLimit/text()"
    WS_BenRit_SuccessiveDisabilityDaysLimit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/TotalDisabilityDefinition/text()"
    WS_BenRit_TotalDisabilityDefinition = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/ValueForPercentage/Amount/text()"
    WS_BenRit_ValueForPercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/ValueOfUnit/Amount/text()"
    WS_BenRit_ValueOfUnit = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/DisabilityEarningsReductionPercentage/text()"
    WS_BenRit_DisabilityEarningsReductionPercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/MaximumEarningsThresholdPercentage/text()"
    WS_BenRit_MaximumEarningsThresholdPercentage = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/Period/text()"
    WS_BenRit_Period = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]').strip("'")
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/PeriodBasis/FullId/text()"
    WS_BenRit_PeriodBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_PeriodBasis_Enum = getEnum(str(WS_BenRit_PeriodBasis).strip('[]'))
    
    reqNode = "//OLContract/assuranceComponents/OLCapitalAssuranceComponent/benefitRight/OLRegularPaymentBenefitRight/incomeReplacement/OLIncomeReplacementDetails/StartDateBasis/FullId/text()"
    WS_BenRit_StartDateBasis = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_BenRit_StartDateBasis_Enum = getEnum(str(WS_BenRit_StartDateBasis).strip('[]'))
    
    #-----------------------------------------------------------------------------------------------------------------------------------------------
    
    #wsQuery = "Select Fineos_enum_instnc_name from qa.fineos_enum_xref where fineos_enum_id  = " +  str(WS_Coverage).strip('[]') 
    #recordset1 = dbCall(dbServer='SQLD10747,4242', dbName='ENTPRS_CLAIMS_DM', sqlQuery=wsQuery)
    #dbResult = recordset1 #dbretrieveData(recordset1,"Fineos_enum_instnc_name")

    
    
    Login(url,searchType)
    Set("//input[@id='universal-search']",caseno)
    wait_Sec(5)
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)
    
    Click("//td[contains(@id,'CoveragesTab_cell')]//*[@class='TabOff']")
    Click("//input[contains(@id,'ChangeContracts')]")
    if(len(driver.find_elements_by_xpath("//input[contains(@id,'RemoveContract') and @disabled]")) == 0):
        Click("//input[contains(@id,'RemoveContract')]")
        Click("//input[contains(@id,'ConfirmRemove_yes')]")
    Click("//input[contains(@id,'AddContract')]")
    
    wait_Sec(2)
    Click("//td[contains(@id,'contractSearch_cell')]//*[@class='TabOff']")
    Set("//input[contains(@id,'ReferenceNo')]",policyNo)
    Click("//input[contains(@id,'searchButton')]")
    
    if(len(driver.find_elements_by_xpath("//*[@id='PageMessage1']")) == 0):
        Click("//input[contains(@id,'searchPageOk')]")    
        if(len(driver.find_elements_by_xpath("//*[@id='PageMessage1']")) > 0):
            wait_Sec(2)
            Click("//a[@id='btn_close_popup_msg']")
            Click("//input[contains(@id,'searchPageOk')]")
            Click("//a[@id='btn_close_popup_msg']")
            html_logger.err("cannot use same policy numner - known issue in Fineos")
        else:
            try:
                #Click("//td[text()='" + classDiv + "']")
                FN_ClassDiv = ""
                FN_DivId = ""
                wait_Sec(5)
                Rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'GroupPolicyClassesListview')]/tbody/tr"))
                for i in range(Rowcnt):
                    if(GetText("//table[contains(@id,'GroupPolicyClassesListview')]/tbody/tr[" + str(i+1) + "]/td[1]") == classDiv):
                        FN_ClassDiv = GetText("//table[contains(@id,'GroupPolicyClassesListview')]/tbody/tr[" + str(i+1) + "]/td[3]")
                        FN_DivId = GetText("//table[contains(@id,'GroupPolicyDivisionsListview')]/tbody/tr[1]/td[1]")
                Click("//td[text()='" + classDiv + "']")         
            except:
                html_logger.err("Classes mentioned not found, hence exiting")
                Click("//input[contains(@id,'editPageCancel_cloned')]")  
                
            Click("//input[contains(@id,'linkButtonBean')]")
            Click("//input[contains(@id,'editPageSave_cloned')]")
            wait_Sec(2)
            Sync("//input[contains(@id,'updateCoverages')]")
            Click("//input[contains(@id,'updateCoverages')]")
            
            # get Policy values
            wait_Sec(5) 
            Sync("//input[contains(@id,'ChangeContracts')]") 
            Click("//input[contains(@id,'ChangeContracts')]")
            Sync("//*[contains(@id,'StartDate')]")
            html_logger.dbg("*********************Policy Validation Starts*******************************")

            FN_StartDate = GetText("//*[contains(@id,'StartDate')]")
            compareString(FN_StartDate,WS_StartDate,"StartDate")
            
            FN_AdditionalDetails = GetText("//*[contains(@id,'AdditionalDetails')]")
            if(FN_AdditionalDetails.strip() == "-"):
               FN_AdditionalDetails = "" 
            compareString(FN_AdditionalDetails,WS_AdditionalDetails,"AdditionalDetails - From PolicySearch")
    
            FN_EndDate = GetText("//*[contains(@id,'EndDate')]")
            compareString(FN_EndDate,WS_EndDate,"EndDate")
            
            select = Select(driver.find_element_by_xpath("//select[contains(@id,'_Product')]"))
            FN_Coverage = select.first_selected_option.text 
            compareString(FN_Coverage,WS_Coverage_Enum,"Coverage") 
            
            select = Select(driver.find_element_by_xpath("//select[contains(@id,'_Source')]"))
            FN_Source = select.first_selected_option.text
            compareString(FN_Source,WS_Source_Enum,"Source") 
            
            FN_TerminationDate = driver.find_element_by_xpath("//input[contains(@id,'_TerminationDate')]").get_attribute('value')  # and @type='hidden'
            compareString(FN_TerminationDate,WS_TermDate,'Termination Date') 
            
            select = Select(driver.find_element_by_xpath("//select[contains(@id,'_ContractStatus')]"))
            FN_Status = select.first_selected_option.text 
            compareString(FN_Status,WS_ContractStatus_Enum,'ContractStatus') 
            
            FN_ERISA = driver.find_element_by_xpath("//input[contains(@id,'_ErisaFlag') and @type = 'hidden']").get_attribute('value')
            compareString(FN_ERISA, WS_ERISA,'ERISA')
            
            FN_GracePeriod = driver.find_element_by_xpath("//input[contains(@id,'GracePeriod')]").get_attribute('value')
            compareString(FN_GracePeriod, WS_GracePeriod,'GracePeriod')
            
            FN_SpecialHandling = driver.find_element_by_xpath("//input[contains(@id,'_SpecialHandling') and @type = 'hidden' ]").get_attribute('value')
            compareString(FN_SpecialHandling, WS_SpecialHandling,'SpecialHandling')
            
            FN_LastPolicyReviewDate = driver.find_element_by_xpath("//input[contains(@id,'_lastPolicyDateTimeBean')]").get_attribute('value')
            compareString(FN_LastPolicyReviewDate,WS_LastPolicyReviewDate,'LastPolicyReviewDate') 
            
            FN_LatestReinstDate = driver.find_element_by_xpath("//input[contains(@id,'LatestReinstDate')]").get_attribute('value')
            compareString(FN_LatestReinstDate,WS_LatestReinstatementDate,'LatestReinstatementDate') 
            
            FN_PremiumPaidToDate = driver.find_element_by_xpath("//input[contains(@id,'PremiumPaidToDate')]").get_attribute('value')
            compareString(FN_PremiumPaidToDate,WS_PremPaidToDate,'PremiumPaidToDate') 
            
            FN_RefreshDate = GetText("//*[contains(@id,'Refresh_Date')]")
            compareString(FN_RefreshDate, WS_LastRefreshDate,'LastRefreshDate')
            
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "PolicyDetail_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            Click("//input[contains(@id,'editPageCancel_cloned')]")
            html_logger.dbg("*********************Policy Validation Ends*******************************")
            #-------------------------------------------------------------------------------
            
            Click("//input[contains(@id,'_CoveragesLVW_cmdEdit')]")
            wait_Sec(5)
            html_logger.dbg("*********************Coverage Validation Starts*******************************")
            FN_Coverage = GetText("//*[contains(@id,'_ProductCategory')]")
            compareString(FN_Coverage, WS_CapAssu_Coverage_Enum, "EditCoverage_Coverage")
            
            FN_CoverageCode = GetText("//*[contains(@id,'_ProductCode')]")
            compareString(FN_CoverageCode, WS_CapAssu_CoverageCode_Enum, "EditCoverage_CoverageCode")
            
            FN_Source = GetText("//*[contains(@id,'_Source')]")
            compareString(FN_Source, WS_CapAssu_Source_Enum, "EditCoverage_Source")
            
            FN_EndDate = GetText("//*[contains(@id,'EndDate')]")
            compareString(FN_EndDate, WS_CapAssu_EndDate, "EditCoverage_EndDate")
            
            FN_Status = GetText("//*[contains(@id,'_Status')]")
            compareString(FN_Status, WS_CapAssu_Status_Enum, "EditCoverage_Status")
            
            FN_EffectiveDate = GetText("//*[contains(@id,'_EffectiveDate')]")
            compareString(FN_EffectiveDate, WS_EffectiveDate, "EditCoverage_EffectiveDate")
            
            FN_CalculationType = GetText("//*[contains(@id,'_CalculationType')]")
            compareString(FN_CalculationType, WS_CapAssu_EDOCCalculationType_Enum, "EditCoverage_CalculationType")
            
            FN_MinHoursForEligibility = GetText("//*[contains(@id,'_MinHoursForEligibility')]")
            compareString(FN_MinHoursForEligibility, WS_MinimumHoursForEligibility, "EditCoverage_MinimumHoursForEligibility")
            
            FN_BillingMethod = GetText("//*[contains(@id,'_billingMethod')]")
            compareString(FN_BillingMethod, WS_CapAssu_SupportedBillingMethod_Enum, "EditCoverage__billingMethod")
            
            select = Select(driver.find_element_by_xpath("//select[contains(@id,'_groupPolicyDivision')]"))
            try:
                FN_groupPolicyDivision = select.first_selected_option.text 
                compareString(FN_groupPolicyDivision,WS_DivisionName,'Divisionname')
            except:
                html_logger.err("POlicyDIvision is not displayed")
                
            select = Select(driver.find_element_by_xpath("//select[contains(@id,'_groupPolicyClass')]"))
            try:
                FN_groupPolicyClass = select.first_selected_option.text 
                compareString(FN_groupPolicyClass,WS_ClassName,'ClassName')
            except:
                html_logger.err("ClassName is not displayed")
                
            compareString_1(FN_ClassDiv,WS_ClassDescription,'ClassDescription')
             
            compareString(FN_DivId,WS_DivisionID,'DivisionID')
            
            
            
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "CoverageDetail_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            Click("//input[contains(@id,'editPageCancel_cloned')]")
            html_logger.dbg("*********************Coverage Validation Ends*******************************")
            #------------------------------------------------------------------------------------------------------------------------------------------- 
            
            html_logger.dbg("*********************Benefit Rights Validation Starts*******************************")
            Click("//input[contains(@id,'BenefitRights_cmdEdit')]")
            wait_Sec(5)
            
            FN_BenRigh_Type = GetText("//*[contains(@id,'_type')]")
            compareString(FN_BenRigh_Type, WS_CapAssu_Type_Enum, "BenRight_Type")
            
            FN_BenRigh_Source = GetText("//*[contains(@id,'_source')]")
            compareString(FN_BenRigh_Source, WS_CapAssu_Benefit_Source_Enum, "BenRight_Source")
            
            FN_BenRigh_earliestDateForClaim = GetText("//*[contains(@id,'_earliestDateForClaim')]")
            compareString(FN_BenRigh_earliestDateForClaim, WS_CapAssu_EffectiveDateOfCoverage, "BenRight_earliestDateForClaim")
            
            FN_BenRigh_latestDateForClaim = GetText("//*[contains(@id,'_latestDateForClaim')]")
            compareString(FN_BenRigh_latestDateForClaim, WS_CapAssu_LatestDateforClaim, "BenRight_latestDateForClaim")
            
            FN_BenRigh_SourceOfEDOC = GetText("//*[contains(@id,'_SourceOfEDOC')]")
            compareString(FN_BenRigh_SourceOfEDOC, WS_CapAssu_Benefit_SourceOfEDOC_Enum, "BenRight_SourceOfEDOC")
            
            FN_BenRigh_minQualifyPeriod = GetText("//*[contains(@id,'_minQualifyPeriod')]")
            compareString(FN_BenRigh_minQualifyPeriod, WS_MinimumQualifyPeriod, "BenRight_minQualifyPeriod")
            
            FN_BenRigh_minQualifyPeriodbasis = GetText("//*[contains(@id,'_BasisOfMinimumQualifyPeriod')]")
            compareString(FN_BenRigh_minQualifyPeriodbasis, WS_CapAssu_MinimumQualifyPeriodBasis_Enum , "MinimumQualifyPeriodBasis")
            
            FN_BenRigh_contestableReview = driver.find_element_by_xpath("//img[contains(@id,'_contestableReview')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_contestableReview:
                FN_BenRigh_contestableReview = 'false'
            else:
                FN_BenRigh_contestableReview = 'true'
                    
            compareString(FN_BenRigh_contestableReview, WS_ContestablePeriodApplicable, "ContestablePeriodApplicable")
            
            FN_BenRigh_contestableReviewMonths = GetText("//*[contains(@id,'_contestableReviewMonths')]")
            compareString(FN_BenRigh_contestableReviewMonths, WS_ContestableReviewMonths, "ContestableReviewMonths")
            
            FN_BenRigh_LateEnrollmentPeriod = GetText("//*[contains(@id,'_LateEnrollmentPeriod')]")
            compareString(FN_BenRigh_LateEnrollmentPeriod, WS_LateEnrollmentPeriod, "LateEnrollmentPeriod")
            
            FN_BenRigh_BasisOfLateEnrollmentPeriod = GetText("//*[contains(@id,'_BasisOfLateEnrollmentPeriod')]")
            compareString(FN_BenRigh_BasisOfLateEnrollmentPeriod, WS_CapAssu_LateEnrollmentPeriodBasis_Enum, "BasisOfLateEnrollmentPeriod")
             
            FN_BenRigh_waitingPeriod = GetText("//*[contains(@id,'_waitingPeriod')]")
            compareString(FN_BenRigh_waitingPeriod, WS_WaitingPeriod, "waitingPeriod")
             
            FN_BenRigh_waitingPeriodBasis = GetText("//*[contains(@id,'_waitingPeriodBasis')]")
            compareString(FN_BenRigh_waitingPeriodBasis, WS_CapAssu_WaitingPeriodBasis_Enum , "waitingPeriodBasis")
            
            FN_BenRigh_reinsuredFlag = driver.find_element_by_xpath("//img[contains(@id,'_reinsuredFlag')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_reinsuredFlag:
                FN_BenRigh_reinsuredFlag = 'false'
            else:
                FN_BenRigh_reinsuredFlag = 'true'
                
            compareString(FN_BenRigh_reinsuredFlag, WS_Reinsured, "reinsuredFlag")
            
            FN_BenRigh_lastRefreshDateValue = GetText("//label[contains(@id,'_lastRefreshDateValue_')]")
            compareString(FN_BenRigh_lastRefreshDateValue, WS_CapAssu_LastRefreshDate, "LastRefreshDate")
            
            FN_BenRigh_adviceToPay = driver.find_element_by_xpath("//img[contains(@id,'_adviceToPay')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_adviceToPay:
                FN_BenRigh_adviceToPay = 'false'
            else:
                FN_BenRigh_adviceToPay = 'true'
                
            compareString(FN_BenRigh_adviceToPay, WS_RegPreComp_AdviceToPay, "AdviceToPay")
             
            FN_BenRigh_checkCutting = GetText("//*[contains(@id,'_checkCutting')]")
            compareString(FN_BenRigh_checkCutting, WS_BenRit_CheckCutting_Enum,"checkCutting")
            
            cnt = len(driver.find_elements_by_xpath("//table[contains(@id,'EligibilityLimitsListviewWidget')]/tbody/tr"))
            for i in range(cnt):
                if i == 0:
                    FN_elgType = driver.find_element_by_xpath("//table[contains(@id,'EligibilityLimitsListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[1]").text
                else:                                               
                    FN_elgType = FN_elgType + ";"  + driver.find_element_by_xpath("//table[contains(@id,'EligibilityLimitsListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[1]").text
                                                               
            compareString(FN_elgType, WS_CapAssu_EligibilityLimitsType_Enum ,"EligibilityLimits") 
             
            
            cnt = len(driver.find_elements_by_xpath("//table[contains(@id,'EligibilityLimitsListviewWidget')]/tbody/tr"))
            for i in range(cnt):
                if i == 0:
                    FN_elgLimit = driver.find_element_by_xpath("//table[contains(@id,'EligibilityLimitsListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[2]").text
                else:                                               
                    FN_elgLimit = FN_elgLimit + ";"  + driver.find_element_by_xpath("//table[contains(@id,'EligibilityLimitsListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[2]").text
                                                               
            compareString(FN_elgLimit, WS_Elig_MaximumAgeLimit,"EligibilityMaxLimits")
             
                                                               
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "GeneralDetail_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            
            html_logger.dbg("*********************General Tab Validation Ends*******************************")
            #---------------------------------------------------------------------------------------------------------------------------
            
            html_logger.dbg("*********************Benefit Parameter Validation Begins*******************************")
            
            Click("//td[contains(@id,'_Eligibility_cell')]")
            
            FN_Benparam_benefitStartDateCalculationType = GetText("//*[contains(@id,'_benefitStartDateCalculationTypeDropDownBean')]")
            compareString(FN_Benparam_benefitStartDateCalculationType, WS_CapAssu_Benefit_StartDateCalculationType_Enum , "StartDateCalculationType")
            
            FN_Benparam_benefitEndDateCalculationType = GetText("//*[contains(@id,'_benefitEndDateCalculationTypeDropDownBean')]")
            compareString(FN_Benparam_benefitEndDateCalculationType, WS_CapAssu_EndDateCalculationType_Enum  , "EndDateCalculationType")
            
            #Need mapping
            #FN_Benparam_EligibilityLimit = GetText("//*[contains(@id,'_EligibilityLimit')]")
            #compareString(FN_Benparam_EligibilityLimit, WS_CapAssu_EndDateCalculationType_Enum  , "EligibilityLimit")  
            
            FN_Benparam_minBenefitPeriod = GetText("//*[contains(@id,'_minBenefitPeriod')]")
            compareString(FN_Benparam_minBenefitPeriod, WS_RegPreComp_MinimumBenefitPeriod, "MinimumBenefitPeriod")
            
            FN_Benparam_minBenefitPeriodBasis = GetText("//*[contains(@id,'_minBenefitPeriodBasis')]")
            compareString(FN_Benparam_minBenefitPeriodBasis, WS_BenRit_MinimumBenefitPeriodBasis_Enum , "minBenefitPeriodBasis")
            
            FN_Benparam_maxBenefitPeriod = GetText("//*[contains(@id,'maxBenefitPeriod')]")
            compareString(FN_Benparam_maxBenefitPeriod, WS_RegPreComp_MaximumBenefitPeriod  , "maxBenefitPeriod")
            
            FN_Benparam_maxBenefitPeriodBasis = GetText("//*[contains(@id,'maxBenefitPeriodBasis')]")
            compareString(FN_Benparam_maxBenefitPeriodBasis, WS_BenRit_MaximumBenefitPeriodBasis_Enum , "maxBenefitPeriodBasis")
            
            FN_Benparam_accidentMaxBenefitPeriod = GetText("//*[contains(@id,'accidentMaxBenefitPeriod')]")
            compareString(FN_Benparam_accidentMaxBenefitPeriod, WS_RegPreComp_MaximumBenefitPeriodAccident , "accidentMaxBenefitPeriod")
            
            FN_Benparam_accidentMaxBenefitPeriodBasis = GetText("//*[contains(@id,'accidentMaxBenefitPeriodBasis')]")
            compareString(FN_Benparam_accidentMaxBenefitPeriodBasis, WS_BenRit_MaximumBenefitPeriodAccidentBasis_Enum, "accidentMaxBenefitPeriodBasis")
            
            FN_Benparam_hospitalMaxBenefitPeriod = GetText("//*[contains(@id,'hospitalMaxBenefitPeriod')]")
            compareString(FN_Benparam_hospitalMaxBenefitPeriod, WS_RegPreComp_MaximumBenefitPeriodHospital, "hospitalMaxBenefitPeriod")
            
            FN_Benparam_hospitalMaxBenefitPeriodBasis = GetText("//*[contains(@id,'hospitalMaxBenefitPeriodBasis')]")
            compareString(FN_Benparam_hospitalMaxBenefitPeriodBasis, WS_BenRit_MaximumBenefitPeriodHospitalBasis_Enum , "hospitalMaxBenefitPeriodBasis")
            
            #---------------------------------------------------
            FN_Benparam_successiveDisabilityDaysLimit = GetText("//*[contains(@id,'successiveDisabilityDaysLimit')]")
            compareString(FN_Benparam_successiveDisabilityDaysLimit, WS_BenRit_SuccessiveDisabilityDaysLimit  , "successiveDisabilityDaysLimit")
            
            FN_BenRigh_presumptiveDisabilityApplies = driver.find_element_by_xpath("//img[contains(@id,'presumptiveDisabilityApplies')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_presumptiveDisabilityApplies:
                FN_BenRigh_presumptiveDisabilityApplies = 'false'
            else:
                FN_BenRigh_presumptiveDisabilityApplies = 'true'
                
            compareString(FN_BenRigh_presumptiveDisabilityApplies, WS_BenRit_PresumptiveDisabilityApplies , "presumptiveDisabilityApplies")
            
            FN_BenRigh_extendPeriodPartial = driver.find_element_by_xpath("//img[contains(@id,'_extendPeriodPartial')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_extendPeriodPartial:
                FN_BenRigh_extendPeriodPartial = 'false'
            else:
                FN_BenRigh_extendPeriodPartial = 'true'
                
            compareString(FN_BenRigh_extendPeriodPartial, WS_BenRit_ExtendPeriodForPartial , "ExtendPeriodPartial")
            
            FN_Benparam_totalDisDefinition = GetText("//*[contains(@id,'totalDisDefinition')]")
            compareString(FN_Benparam_totalDisDefinition, WS_BenRit_TotalDisabilityDefinition , "totalDisDefinition")

            FN_Benparam_benefitQualificationStartDate = GetText("//*[contains(@id,'benefitQualificationStartDate')]")
            compareString(FN_Benparam_benefitQualificationStartDate, WS_BenRit_BenefitQualificationStartDate_Enum , "benefitQualificationStartDate")
            
            FN_Benparam_benefitQualReqType = GetText("//*[contains(@id,'benefitQualReqType')]")
            compareString(FN_Benparam_benefitQualReqType, WS_BenRit_BenQualificationReqType_Enum , "benefitQualReqType")
            
            FN_Benparam_minHoursWorkRequired = GetText("//*[contains(@id,'minHoursWorkRequired')]")
            compareString(FN_Benparam_minHoursWorkRequired, WS_BenRit_MinimumHoursWorkRequired , "minHoursWorkRequired")
            
            FN_Benparam_benQualPeriod = GetText("//*[contains(@id,'_benQualPeriod')]")
            compareString(FN_Benparam_benQualPeriod, WS_BenRit_CODPeriod  , "CODPeriod")
            
            FN_Benparam_benQualPeriodBasis = GetText("//*[contains(@id,'benQualPeriodBasis')]")
            compareString(FN_Benparam_benQualPeriodBasis, WS_BenRit_CODPeriodBasis_Enum  , "CODPeriodBasis")
            
            FN_Benparam_recurrentBenefitPeriod = GetText("//*[contains(@id,'recurrentBenefitPeriod')]")
            compareString(FN_Benparam_recurrentBenefitPeriod, WS_BenRit_RecurrentBenefitPeriod  , "recurrentBenefitPeriod")
            
            FN_Benparam_recurrentBenefitPeriodbasis = GetText("//*[contains(@id,'individualPeriodBasis')]")
            compareString(FN_Benparam_recurrentBenefitPeriodbasis, WS_BenRit_RecurrentBenefitPeriodBasis_Enum , "recurrentBenefitPeriodbasis")
            
            FN_Benparam_EarningsDefinitionBean = GetText("//*[contains(@id,'EarningsDefinitionBean')]")
            compareString(FN_Benparam_EarningsDefinitionBean, WS_Salary_EarningsDefinition_Enum , "EarningsDefinitionBean")
            
            FN_Benparam_EarningsChangeEffectiveDateBean = GetText("//*[contains(@id,'EarningsChangeEffectiveDateBean')]")
            compareString(FN_Benparam_EarningsChangeEffectiveDateBean, WS_Salary_ChangeOfEarningsAssessmentDate_Enum,"EarningsChangeEffectiveDateBean")
            
            FN_Benparam_AveragingPeriodBean = GetText("//*[contains(@id,'AveragingPeriodBean')]")
            compareString(FN_Benparam_AveragingPeriodBean, WS_Salary_AveragingPeriod  , "AveragingPeriodBean")
            
            FN_Benparam_AveragingPeriodBasisBean = GetText("//*[contains(@id,'AveragingPeriodBasisBean')]")
            compareString(FN_Benparam_AveragingPeriodBasisBean, WS_Salary_PeriodBasis_Enum  , "AveragingPeriodBasisBean")
         #----------------------------------------------------------------------------------------   
            FN_Benparam_EliminationPeriod = GetText("//*[contains(@id,'EliminationPeriod')]")
            compareString(FN_Benparam_EliminationPeriod, WS_SicknessEliminationPeriod  , "SicknessEliminationPeriod")
            
            FN_Benparam_EliminationPeriodBasis = GetText("//*[contains(@id,'EliminationPeriodBasis')]")
            compareString(FN_Benparam_EliminationPeriodBasis, WS_CapAssu_ELimDet_SicknessEliminationPeriodBasis_Enum  , "SicknessEliminationPeriodBasis")
            
            FN_Benparam_EliminationPeriodAccident = GetText("//*[contains(@id,'EliminationPeriodAccident')]")
            compareString(FN_Benparam_EliminationPeriodAccident, WS_AccidentEliminationPeriod  , "EliminationPeriodAccident")
            
            FN_Benparam_EliminationAccidentPeriodBasis = GetText("//*[contains(@id,'EliminationAccidentPeriodBasis')]")
            compareString(FN_Benparam_EliminationAccidentPeriodBasis, WS_CapAssu_ELimDet_AccidentEliminationPeriodBasis_Enum  , "EliminationAccidentPeriodBasis")
            
            FN_Benparam_EliminationPeriodHospital = GetText("//*[contains(@id,'EliminationPeriodHospital')]")
            compareString(FN_Benparam_EliminationPeriodHospital, WS_Sickness_HospitalEliminationPeriod  , "EliminationPeriodHospital")
            
            FN_Benparam_EliminationHospitalPeriodBasis = GetText("//*[contains(@id,'EliminationHospitalPeriodBasis')]")
            compareString(FN_Benparam_EliminationHospitalPeriodBasis, WS_CapAssu_ELimDet_HospitalEliminationPeriodBasis_Enum  , "EliminationHospitalPeriodBasis")
            
            FN_Benparam_EliminationAmount = GetText("//*[contains(@id,'_EliminationAmount')]")
            compareString(FN_Benparam_EliminationAmount, WS_Sickness_Amount  , "EliminationAmount")
            
            FN_BenRigh_AccumulateEliminationPeriodFlag = driver.find_element_by_xpath("//img[contains(@id,'_AccumulateEliminationPeriodFlag')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_AccumulateEliminationPeriodFlag:
                FN_BenRigh_AccumulateEliminationPeriodFlag = 'false'
            else:
                FN_BenRigh_AccumulateEliminationPeriodFlag = 'true'
                
            compareString(FN_BenRigh_AccumulateEliminationPeriodFlag, WS_AccumulateEliminationPeriods , "AccumulateEliminationPeriodFlag")
            
            FN_BenRigh__hospitalizationClauseApplies = driver.find_element_by_xpath("//img[contains(@id,'_hospitalizationClauseAppliesCheckBoxBean')]").get_attribute('src')
            if 'unticked' in FN_BenRigh__hospitalizationClauseApplies:
                FN_BenRigh__hospitalizationClauseApplies = 'false'
            else:
                FN_BenRigh__hospitalizationClauseApplies = 'true'
                
            compareString(FN_BenRigh__hospitalizationClauseApplies, WS_Sickness_HospitalizationClauseApplies , "hospitalizationClauseApplies")
            
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "BenefitParam_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            html_logger.dbg("*********************Benefit Parameter Validation Ends*******************************")
            
            #-------------------------------------------------------------------------------------------------------------------------------
            html_logger.dbg("*********************Conditions Validation Begins*******************************")
            
            Click("//td[contains(@id,'Disability_Claim_Group_Conditions')]")
            wait_Sec(5)
            #if condition is not null - shouod be added here
            cnt = len(driver.find_elements_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr"))
            if(cnt >0):
                for i in range(cnt):
                    if i == 0:
                        FN_CondType = driver.find_element_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr[" + str(i+1) + "]/td[1]").text
                        FN_CondDurUnit = driver.find_element_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr[" + str(i+1) + "]/td[2]").text
                        FN_CondDur = driver.find_element_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr[" + str(i+1) + "]/td[3]").text
                    else:                                               
                        FN_CondType = FN_CondType + ";"  + driver.find_element_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr[" + str(i+1) + "]/td[1]").text
                        FN_CondDurUnit = FN_CondDurUnit + ";"  + driver.find_element_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr[" + str(i+1) + "]/td[2]").text 
                        FN_CondDur = FN_CondDur +  ";"  + driver.find_element_by_xpath("//table[contains(@id,'BenefitRightConditionsListView')]/tbody/tr[" + str(i+1) + "]/td[3]").text
                #if condition is not null - shouod be added here
                                                                  
                compareString(FN_CondType, WS_Condition_Type_Enum  ,"Condition Type") 
                compareStringContains(FN_CondDurUnit, WS_Condition_DurationUnit_Enum  ,"Condition Duration Unit") 
                compareString(FN_CondDur, WS_Condition_Duration  ,"Condition Duration") 
            
            FN_Cond_ReviewPeriodPriortoCover = GetText("//*[contains(@id,'Review_Period_Prior_to_Cover')]")
            compareString(FN_Cond_ReviewPeriodPriortoCover, WS_PreExCondition_ReviewPeriodPriorToCoverMonths  , "ReviewPeriodPriortoCover")
            
            FN_Benparam_Time_To_Claim = GetText("//*[contains(@id,'Time_To_Claim')]")
            compareString(FN_Benparam_Time_To_Claim, WS_PreExCondition_TimeToClaimMonths  , "Time_To_Claim")
            
            FN_Benparam_Treatment_Free_Period = GetText("//*[contains(@id,'Treatment_Free_Period')]")
            compareString(FN_Benparam_Treatment_Free_Period, WS_PreExCondition_TreatmentFreePeriodMonths,"Treatment_Free_Period")
            
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "Conditions_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            html_logger.dbg("*********************Conditions Validation Ends*******************************")
            #-----------------------------------------------------------------------------------------------------------
            
            html_logger.dbg("*********************Validation of Calc Param Begins*******************************")
            
            Click("//td[contains(@id,'Claim_Group_CalculationParameters_cell')]")
            wait_Sec(5)
            FN_Calcparam_benefitCalculationType = GetText("//*[contains(@id,'_benefitCalculationType')]")
            compareString(FN_Calcparam_benefitCalculationType, WS_BenRit_BenefitCalculationType_Enum  , "benefitCalculationType")
            
            FN_Calcparam_overridePercentage = str(float(GetText("//*[contains(@id,'_overridePercentage')]"))/100)
            compareString(FN_Calcparam_overridePercentage, WS_BenRit_OverridePercentage , "OverridePercentage")
            
            FN_BenRigh_useGrossSalaryForGBACheckBox = driver.find_element_by_xpath("//img[contains(@id,'_useGrossSalaryForGBACheckBox')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_useGrossSalaryForGBACheckBox:
                FN_BenRigh_useGrossSalaryForGBACheckBox = 'false'
            else:
                FN_BenRigh_useGrossSalaryForGBACheckBox = 'true'
                
            compareString(FN_BenRigh_useGrossSalaryForGBACheckBox, WS_Salary_UseGrossSalaryForGBA , "useGrossSalaryForGBACheckBox")
            
            
            cnt = len(driver.find_elements_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr"))
            if(cnt >0):
                for i in range(cnt):
                    if i == 0:
                        FN_Percentage = str(float(driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[1]").text)/100)
                        FN_Amount = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[2]").text
                        FN_StartAmount = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[3]").text
                        FN_EndAmount = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[4]").text
                        FN_PeriodStart = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[5]").text
                        FN_PeriodEnd = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[6]").text
                        FN_Periodbasis = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[7]").text
                        FN_Age = driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[8]").text
                       
                    else:                                               
                        FN_Percentage = FN_Percentage + ";" + str(float(driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[1]").text)/100)
                        FN_Amount = FN_Amount + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[2]").text 
                        FN_StartAmount = FN_StartAmount + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[3]").text
                        FN_EndAmount = FN_EndAmount + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[4]").text
                        FN_PeriodStart = FN_PeriodStart + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[5]").text
                        FN_PeriodEnd = FN_PeriodEnd + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[6]").text
                        FN_Periodbasis = FN_Periodbasis + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[7]").text
                        FN_Age = FN_Age + ";" + driver.find_element_by_xpath("//table[contains(@id,'PaymentTiersListviewWidget')]/tbody/tr[" + str(i+1) + "]/td[8]").text
                        
                #if condition is not null - shouod be added here
                                                                  
                compareString(FN_Percentage, WS_amt_Percentage  ,"Percentage") 
                compareString(FN_Amount, WS_amt_FixedAmountPerFrequency  ,"FixedAmountPerFrequency") 
                compareString(FN_StartAmount, WS_amt_StartAmount  ,"Start Amount") 
                compareString(FN_EndAmount, WS_amt_EndAmount  ,"End Amount") 
                compareString(FN_PeriodStart, WS_amt_PeriodStart  ,"Period Start") 
                compareString(FN_PeriodEnd, WS_amt_PeriodEnd  ,"Period End") 
                compareString(FN_Periodbasis, WS_amt_PeriodBasis_Enum  ,"period basis") 
                compareString(FN_Age, WS_amt_ageLimit  ,"Age") 
            
            FN_Calcparam_AGERoundingRule = GetText("//*[contains(@id,'AGERoundingRule')]")
            compareString(FN_Calcparam_AGERoundingRule, WS_RegPreComp_AGERoundingRule_Enum , "AGERoundingRule")
            
            FN_Calcparam_AGERoundingPrecision = GetText("//*[contains(@id,'AGERoundingPrecision')]")
            compareString(FN_Calcparam_AGERoundingPrecision, WS_RegPreComp_AGERoundingPrecision_Enum , "AGERoundingPrecision")
           
           # need data 
           # FN_Calcparam_RoundUnit = GetText("//*[contains(@id,'RoundUnit')]")
           # compareString(FN_Calcparam_RoundUnit, xxxx , "RoundUnit")
           # 
           # FN_Calcparam_RoundInstruction = GetText("//*[contains(@id,'RoundInstruction')]")
           # compareString(FN_Calcparam_RoundInstruction, xxxx , "RoundInstruction")
            
            
            
            FN_Calcparam_MaximumAggregateAmount = GetText("//*[contains(@id,'MaximumAggregateAmount')]")
            compareString(FN_Calcparam_MaximumAggregateAmount, WS_RegPreComp_MaximumAggregateAmount  , "MaximumAggregateAmount")
            
            FN_Calcparam_MinimumAmount = GetText("//*[contains(@id,'_MinimumAmount')]")
            compareString(FN_Calcparam_MinimumAmount, WS_RegPreComp_MinimumAmount  , "MinimumAmount")
            
            FN_Calcparam_MaximumAmount = GetText("//*[contains(@id,'_MaximumAmount')]")
            compareString(FN_Calcparam_MaximumAmount, WS_RegPreComp_MaximumAmount  , "MaximumAmount")
            
            FN_Calcparam_MinimumPercentage = str(float(GetText("//*[contains(@id,'MinimumPercentage')]"))/100.00)
            compareString(FN_Calcparam_MinimumPercentage, WS_RegPreComp_MinimumPercentage  , "MinimumPercentage")
            
            FN_Calcparam_maxPercentage = str(float(GetText("//*[contains(@id,'_maxPercentage')]"))/100.00)
            compareString(FN_Calcparam_maxPercentage, WS_RegPreComp_MaximumPercentage  , "MaxPercentage")
            
            FN_Calcparam_MinimumCalcType = GetText("//*[contains(@id,'MinimumCalcType')]")
            compareString(FN_Calcparam_MinimumCalcType, WS_RegPreComp_MinimumCalcType_Enum  , "MinimumCalcType")
            
            FN_Calcparam_MaximumCalcType = GetText("//*[contains(@id,'MaximumCalcType')]")
            compareString(FN_Calcparam_MaximumCalcType, WS_RegPreComp_MaximumCalcType_Enum  , "MaximumCalcType")
            
            FN_Calcparam_AutoAcceptanceLimit = GetText("//*[contains(@id,'AutoAcceptanceLimit')]")
            compareString(FN_Calcparam_AutoAcceptanceLimit, WS_AutoAcceptanceLimit  , "AutoAcceptanceLimit")
            
            FN_BenRigh_Underwritten = driver.find_element_by_xpath("//img[contains(@id,'Underwritten')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_Underwritten:
                FN_BenRigh_Underwritten = 'false'
            else:
                FN_BenRigh_Underwritten = 'true'
                
            compareString(FN_BenRigh_Underwritten, WS_Underwritten, "Underwritten")
            
            
            FN_BenRigh_DuesCreatedInArrears = driver.find_element_by_xpath("//img[contains(@id,'DuesCreatedInArrears')]").get_attribute('src')
            if 'unticked' in FN_BenRigh_DuesCreatedInArrears:
                FN_BenRigh_DuesCreatedInArrears = 'false'
            else:
                FN_BenRigh_DuesCreatedInArrears = 'true'
                
            compareString(FN_BenRigh_DuesCreatedInArrears, WS_RegPreComp_DuesCreatedInArrears , "DuesCreatedInArrears")
            
            #--------------------------
            
            FN_Calcparam_Payment_Frequency = GetText("//*[contains(@id,'Payment_Frequency')]")
            compareString(FN_Calcparam_Payment_Frequency, WS_pmentFreqHist_PremiumPaymentFrequency_Enum  , "Payment_Frequency")
            
            FN_Calcparam_calcPeriodProRatingRuleBean = GetText("//*[contains(@id,'calcPeriodProRatingRuleBean')]")
            compareString(FN_Calcparam_calcPeriodProRatingRuleBean, WS_paymentFrequencyHistories_CalculationPeriodProRatingRule_Enum  , "calcPeriodProRatingRuleBean")
            
            FN_Calcparam_Renewal_Day_Basis = GetText("//*[contains(@id,'Renewal_Day_Basis')]")
            compareString(FN_Calcparam_Renewal_Day_Basis, WS_pmentFreqHist_RenewalDayBasis_Enum  , "Renewal_Day_Basis")
            
            FN_Calcparam_contributoryStatus = GetText("//*[contains(@id,'contributoryStatus')]")
            compareString(FN_Calcparam_contributoryStatus, WS_CapAssu_EmpContStatus_Enum  , "contributoryStatus")
            
            FN_Calcparam_employeeContributionPercentage = GetText("//*[contains(@id,'employeeContributionPercentage')]")
            compareString(FN_Calcparam_employeeContributionPercentage, WS_EmployeeContributionPercentage  , "employeeContributionPercentage")


            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "CalcParam_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            html_logger.dbg("*********************Calculation Parameter Validation Ends*******************************")
            
            #------------------------------------------------------------------------------------------------------------
            html_logger.dbg("*********************Return to Work Begins*******************************")
            
            Click("//td[contains(@id,'Claim_Group_ReturnToWork_cell')]")
            wait_Sec(5)
            
            
            FN_RTW_ReturnToWorkCalcType = GetText("//*[contains(@id,'ReturnToWorkCalcType')]")
            compareString(FN_RTW_ReturnToWorkCalcType, WS_BenRit_ReturnToWorkCalculationType_Enum  , "ReturnToWorkCalcType")
            
            FN_RTW_WIBPeriodstartDate = GetText("//*[contains(@id,'WIBPeriodstartDate')]")
            compareString(FN_RTW_WIBPeriodstartDate, WS_BenRit_StartDateBasis_Enum  , "WIBPeriodstartDate")
            
            FN_RTW_WIBPeriod = GetText("//*[@id='WIBPeriodDetailsWidget']//*[@class='WidgetPanel']/tbody/tr[5]/td[2]/span")
            compareString(FN_RTW_WIBPeriod, WS_BenRit_Period , "WIBPeriod")
            
            FN_RTW_WIBPeriodBasis = GetText("//*[contains(@id,'WIBPeriodBasis')]")
            compareString(FN_RTW_WIBPeriodBasis, WS_BenRit_PeriodBasis_Enum , "WIBPeriodBasis")
            
            FN_Maximum_Earnings_Threshold_Percentage = GetText("//*[contains(@id,'Maximum_Earnings_Threshold_Percentage_(WIB)')]")
            compareString(FN_Maximum_Earnings_Threshold_Percentage, WS_BenRit_MaximumEarningsThresholdPercentage  , "Maximum_Earnings_Threshold_Percentage_(WIB)")
            
            FN_RTW_Disability_Earnings_Reduction_Percentage = GetText("//*[contains(@id,'Disability_Earnings_Reduction_Percentage_(WIB)')]")
            compareString(FN_RTW_Disability_Earnings_Reduction_Percentage, WS_BenRit_DisabilityEarningsReductionPercentage  , "Disability_Earnings_Reduction_Percentage_(WIB)")
            
            #need mapping------------------------
            #FN_RTW_Maximum_Earnings_Threshold_Percentage_Post = GetText("//*[contains(@id,'Maximum_Earnings_Threshold_Percentage_(Post-WIB)')]")
            #compareString(FN_RTW_Maximum_Earnings_Threshold_Percentage_Post, xxxx , "Maximum_Earnings_Threshold_Percentage_(Post-WIB)")
            
            #FN_RTW_Disability_Earnings_Reduction_Percentage_Post = GetText("//*[contains(@id,'Disability_Earnings_Reduction_Percentage_(Post-WIB)')]")
            #compareString(FN_RTW_Disability_Earnings_Reduction_Percentage_Post, xxxx , "Disability_Earnings_Reduction_Percentage_(Post-WIB)")
            
            
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "ReturntoWork_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            html_logger.dbg("*********************Return to Work Validation Ends*******************************")
            
            
             #------------------------------------------------------------------------------------------------------------
            html_logger.dbg("*********************COLA Begins*******************************")
            
            Click("//td[contains(@id,'Group_PaymentIndexation_cell')]")
            wait_Sec(5)
            
            if(driver.find_element_by_xpath("//input[@type='submit' and contains(@value,'COLA')]").get_attribute("value") == 'Apply COLA'):
                    FN_RegPreComp_Anniversary = ""
                    FN_RegPreComp_EliminationPeriodBasis_Enum = "Unknown"
                    FN_RegPreComp_EliminationPeriod = "0" 
                    FN_RegPreComp_AdjustBenefit = "true"   # previously this was false and due to EC-5563 it's chnaged to true
                    FN_RegPreComp_IndexationApplies = "false"
                    FN_RegPreComp_MaxCumulativeRate = "0.00"
                    FN_RegPreComp_MaximumRate = "0.00"
                    FN_RegPreComp_MaxNumberCOLAs = "0"
                    FN_RegPreComp_MinimumRate = "0.00"            
            else:
                    FN_RegPreComp_Anniversary = GetText("//*[contains(@id,'_Anniversary')]")
                    FN_RegPreComp_EliminationPeriodBasis_Enum = GetText("//*[contains(@id,'_Elimination_Period_Basis')]")
                    FN_RegPreComp_EliminationPeriod =  GetText("//*[contains(@id,'_Elimination_Period_Units')]")
                    
                    FN_RegPreComp_AdjustBenefit = driver.find_element_by_xpath("//img[contains(@id,'_Create_Real_Adjustments')]").get_attribute('src')
                    if 'unticked' in FN_RegPreComp_AdjustBenefit:
                        FN_RegPreComp_AdjustBenefit = 'false'
                    else:
                        FN_RegPreComp_AdjustBenefit = 'true'
                        
                    FN_RegPreComp_IndexationApplies  = 'true'
                        
                    
                    FN_RegPreComp_MaxCumulativeRate = GetText("//*[contains(@id,'Max_Cumulative_Rate')]")[:-2]    
                    FN_RegPreComp_MaximumRate = GetText("//*[contains(@id,'Maximum_Rate')]")[:-2]
                    FN_RegPreComp_MaxNumberCOLAs = GetText("//*[contains(@id,'MaxNumber_Indexations')]") 
                    FN_RegPreComp_MinimumRate = GetText("//*[contains(@id,'Minimum_Rate')]")[:-2] 
                      
            compareString(FN_RegPreComp_Anniversary,WS_RegPreComp_Anniversary,"RegPreComp_Anniversary")
            compareString(FN_RegPreComp_EliminationPeriodBasis_Enum , WS_RegPreComp_EliminationPeriodBasis_Enum   , "RegPreComp_EliminationPeriodBasis")
            compareString(FN_RegPreComp_EliminationPeriod , WS_RegPreComp_EliminationPeriod   , "FN_RegPreComp_EliminationPeriod")
            compareString(FN_RegPreComp_AdjustBenefit, WS_RegPreComp_AdjustBenefit  , "RegPreComp_AdjustBenefit")
            compareString(FN_RegPreComp_IndexationApplies, WS_RegPreComp_IndexationApplies,"RegPreComp_IndexationApplies")
            compareString(FN_RegPreComp_MaxCumulativeRate, WS_RegPreComp_MaxCumulativeRate  , "RegPreComp_MaxCumulativeRate")
            compareString(FN_RegPreComp_MaximumRate, WS_RegPreComp_MaximumRate  , "RegPreComp_MaximumRate")
            compareString(FN_RegPreComp_MaxNumberCOLAs, WS_RegPreComp_MaxNumberCOLAs  , "RegPreComp_MaxNumberCOLAs")
            compareString(FN_RegPreComp_MinimumRate, WS_RegPreComp_MinimumRate  , "RegPreComp_MinimumRate")
            
            screenshot(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\" + "COLA_" + policyNo + "_" + str(datetime.datetime.now().isoformat().replace(":","_")) + ".png")
            html_logger.dbg("*********************Return to Work Validation Ends*******************************") 
                
            Click("//input[contains(@id,'cmdPageBack_cloned')]")
            #------------------------------------------------------------------------------------------------------------
    else:
        Click("//a[@id='btn_close_popup_msg']")
        Click("//input[contains(@id,'searchPageCancel_cloned')]")
        Click("//input[contains(@id,'editPageCancel_cloned')]")
        html_logger.err("policy number doesn't exist")


#------------------------Tests starts Here----------------------------------------

options = webdriver.ChromeOptions()


polnos = "006144010000000,300-WDS-01/01/2017" #"006163290001000,100-WDS-01/01/2018"#"006143200000000,106-WDS-09/01/2015"#"006105320000000,100-WDS-12/01/2018" #"006198590000000,100-WDS-02/01/2020" #"006163290001000,100-WDS-01/01/2018" #"006121060002000,100-WDS-05/01/2020" #"006134780000000,100-WDS-07/01/2020"  #"006121060002000,100-WDS-05/01/2020" #
#";;;;;006144010000000,300-WDS-01/01/2017" 
#"006142960000000,100-WDL-08/01/2015;006142969999001,100-WDL-01/01/2019"
#"006143280000000,51739-LTD-04/01/2016;006142960000000,101-WDS-08/01/2015;006143280000000,51739-LTD-04/01/2016;006142920000000,51693-LTD-04/01/2016;006142960000000,100-WDL-08/01/2015;006142969999001,100-WDL-01/01/2019;006142960000000,100-WDS-08/01/2015;006142969999002,100-WDL-01/01/2019;006143140000000,23230-STD-09/01/2018;006143280000000,100-WDS-04/01/2016;006143450000000,51688-LTD-07/01/2019;006143140000000,23233-STD-01/01/2016;006143450000000,51689-LTD-07/01/2019;006142920000000,51694-LTD-04/01/2016;006142960000000,100-WDS-08/01/2015;006143140000000,51667-LTD-08/01/2015;006143280000000,51739-LTD-04/01/2016"
#"006142060000000,10-OLS-10/01/2019;006142190000000,100-WDL-08/01/2015;006142190000000,23240-STD-08/01/2015;006142360000000,100-WDL-11/01/2015;006142190000000,23241-STD-08/01/2015;006142360001000,100-WDS-11/01/2015;006145010001000,100-WDL-03/01/2018;006142770000000,51709-LTD-10/01/2015;006145010001000,101-WDL-03/01/2018;006142060000000,100-WDL-10/01/2019;006142340000000,51581-LTD-07/01/2015;006142190000000,101-WDL-08/01/2015;006145010001000,100-WDS-03/01/2018;006142360000000,100-WDS-11/01/2015;006145010001000,100-WDS-03/01/2018;006142360001000,101-WDS-11/01/2015"
#"006143200000000,100-WDS-09/01/2015;006143060000000,51660-LTD-08/01/2015;006144010000000,301-WDS-01/01/2017;006144320000000,100-WDL-12/01/2018;006144460000000,100-WDL-12/01/2018;006142700000000,100-WDS-08/01/2015;006144320000000,101-WDS-02/01/2018;006142830001000,100-WDS-03/01/2018;006142840000000,100-WDS-03/01/2019;006143040000000,36290-LTD-09/01/2015"
#'006106060000000,10990-VLD-04/01/2017;000000240003000,101-WDS-01/01/2019;006140000000000,11-OLS-11/01/2015'  #000000240002000,51452-LTD-01/01/2019;
#006144010000000,300-WDS-01/01/2017;006144320000000,100-WDS-02/01/2018;006144340000000,51796-LTD-01/01/2016;006144460000000,100-WDS-12/01/2015;006144620000000,51820-LTD-01/01/2016;006144710005000,100-WDS-01/01/2018;006142700000000,100-WDL-08/01/2015;006142710000000,51661-LTD-08/01/2015;006142720000000,51606-LTD-08/01/2015;006144320000000,101-WDL-02/01/2018;006142830001000,100-WDL-03/01/2018;006142840000000,100-WDL-03/01/2019;006142830001000,101-WDL-03/01/2018;006142850000000,51604-LTD-08/01/2015;006142830001000,101-WDS-03/01/2018;006142850000000,51605-LTD-08/01/2015;006143040000000,100-WDS-09/01/2015;006143060000000,51659-LTD-08/01/2015;
arrPolsnos = polnos.split(";")

for arrpols in arrPolsnos:
    
    driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
    arrpol = arrpols.split(",")
    policyNo = arrpol[0]#"006155400000000"        #"006036490001000" # "006181030000000" #     
    classDiv = arrpol[1]#"100-WDL-03/01/2017"
    html_logger.info('Start of Iterations with Policy No ' + str(policyNo) + ' and classDiv  ' + classDiv)
    validate_covLoader(policyNo,classDiv)
    #driver.get("https://www.linkedin.com/")
    #screenshot1("C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\DisabilityClaims\\DB_WS_Fineos\\Results\Testxx.png")
    
    try:
        driver.close()
    except:
        pass  





def retrieve_OrgSearech(polno1):
    url = "https://internal-apigw-nonprod.oneamerica.com/claims-dev/organization/search"
    searchType = "OrgSearch"
    caseno = "DI-611"
    #policyNo = "006038230000000"        #"006036490001000" # "006181030000000" #     
    #classDiv = "3175-VLD-04/01/2001"   #"35-CSP-05/01/2001" #  "100-WDS-01/01/2019" # 
    polno = polno1 #"000005650129000"
    
    WSURL = "https://internal-apigw-nonprod.oneamerica.com/claims-dev/party/search"
    WSBody = """<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope'> 
                <soapenv:Header> 
                </soapenv:Header> 
                <soapenv:Body>
                <p:PartyRetrievalDetailsRequest xmlns:p='http://www.fineos.com/wscomposer/PartyRetrievalDetails'> 
                <sourceSystem>External</sourceSystem>
                <referenceNo>""" + polno + """</referenceNo>
                </p:PartyRetrievalDetailsRequest>
                </soapenv:Body>
                </soapenv:Envelope>"""
    WSHeader = {'content-type': 'text/xml'}        
    respText = SOAPPost_Response(WSURL,WSBody,WSHeader)
    
    
    reqNode = "//OCOrganisation/correspondenceAddress/OCPartyAddress/versions/OCPartyAddressVersion/postalAddress/OCPostalAddress/Address1/text()"
    WS_Add1 = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    reqNode = "//OCOrganisation/correspondenceAddress/OCPartyAddress/versions/OCPartyAddressVersion/postalAddress/OCPostalAddress/Country/FullId/text()"
    WS_Country = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    WS_Country_Enum = getEnum(str(WS_Country).strip('[]'))
    
    reqNode = "//OCOrganisation/correspondenceAddress/OCPartyAddress/versions/OCPartyAddressVersion/postalAddress/OCPostalAddress/PostCode/text()"
    WS_PostCode = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    reqNode = "//OCOrganisation/correspondenceAddress/OCPartyAddress/versions/OCPartyAddressVersion/postalAddress/OCPostalAddress/PremiseNo/text()"
    WS_PremiseNo = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    reqNode = "//OCOrganisation/correspondenceAddress/OCPartyAddress/versions/OCPartyAddressVersion/contactMediums/OCPhone/AreaCode/text()"
    WS_AreaCode = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    reqNode = "//OCOrganisation/correspondenceAddress/OCPartyAddress/versions/OCPartyAddressVersion/contactMediums/OCPhone/TelephoneNo/text()"
    WS_TelephoneNo = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    reqNode = "//OCOrganisation/Name/text()"
    WS_Name = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    
    WSURL = "https://internal-apigw-nonprod.oneamerica.com/claims-dev/coverage/disability/classesdivisions"
    WSBody = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <soapenv:Header/> 
                <soapenv:Body>
                <p:GetClassesAndDivisionsRequest
                xmlns:p="http://www.fineos.com/classesanddivisions/operationtypes">
                <policyReference>""" + polno + """</policyReference> <incurredDate>2020-04-28T00:00:00</incurredDate>
                </p:GetClassesAndDivisionsRequest> </soapenv:Body> </soapenv:Envelope>"""
                
    WSHeader = {'content-type': 'text/xml'}        
    respText = SOAPPost_Response(WSURL,WSBody,WSHeader)
    
    reqNode = "//classesAndDivisions/classes/description/text()"
    WS_ClsDesc = str(SOAPPost_getNodeValue(respText,reqNode)).strip('[]')
    
    
    
    print(polno + ";" + WS_Name + ";" + WS_Add1 + ";" + WS_Country_Enum + ";" + WS_PostCode + ";" + WS_PremiseNo + ";" + WS_AreaCode + WS_TelephoneNo + ";" + WS_ClsDesc )
 
"""

polnos = '000000240003000,103-WDS-01/01/2019'
arrPolsnos = polnos.split(";")

for arrpols in arrPolsnos:
    retrieve_OrgSearech(arrpols)

"""

