#----------------Imports--------------------------
import unittest
import datetime
import logging
from collections import Counter

from lxml import etree
from pandas import ExcelFile
from pandas import ExcelWriter
import pytest
import pytest_html
import requests

import json
from jsonpath_ng import jsonpath, parse


#from Python_Test1 import html_logger
import sys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.chrome.webdriver import WebDriver
parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

import html_logger
from html_logger import setup, info
import pandas as pd

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


#----------------Imports--------------------------
#----------------Logging--------------------------
now = datetime.datetime.now()

LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\WebService_DB_"+ str(now.isoformat().replace(":","_")) + ".html"
#logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)
#setup("WebService_Test","1",LOG_FILENAME)
setup("OKRA Test","Executed on " + str(now),LOG_FILENAME)  #Zucchini Test
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')
 

#----------------Logging--------------------------


'''----------------------------------------DBCall---------------------------------------------------------------------------
gets Pararmeter as dbServer, dbName and SQLQuery

ex: dbResult = dbCall(dbServer='SQLD10747,4242', dbName='ENTPRS_CLAIMS_DM', sqlQuery="select ADMIN_SYS_PARTY_KEY_TXT from dbo.ENTPRS_PARTY_VW p where ADMIN_SYS_PARTY_KEY_TXT like '0000004%' and 
                (cmpny_pref_name like 'test sub%' or PLCYHLDR_FRST_SUBSD_PREF_NAME like 'test sub%' or PLCYHLDR_SEC_SUBSD_PREF_NAME like 'test sub%' or PLCYHLDR_THIRD_SUBSD_PREF_NAME like 'test sub%' 
                or PLCYHLDR_FOURTH_SUBSD_PREF_NAME like 'test sub%')")
-------------------------------------------------------------------------------------------------------------------'''

def dbCall(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    list_result = list()

    for row in cursor:
        list_result.append(str(row[0]).strip())
        
      
    return ';'.join([str(elem) for elem in list_result]) 



def dbCall1(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    list_result = list()

    for row in cursor:
        list_result.append(str(row[0]).strip())
        
      
    return list_result
#----------------------------------------End of DBCall---------------------------------------------------------------------------



def dbCall_multi(dbServer,dbName,sqlQuery):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + dbServer +';'
                      'Database=' + dbName + ';'
                      'Trusted_Connection=yes;')

    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    #list_result = list()
    return cursor.fetchall()
 
    #return list_result



def dbretrieveData(recordset,columnname):
    row = ""
    list_result = list()
    for row in recordset:
        colval = eval("row." + columnname)
        if(colval != None):
            list_result.append(colval)      
    return list_result          

'''
recordset1 = dbCall_multi("SQLD10747,4242","ENTPRS_CLAIMS_DM", "Select ADMIN_SYS_NAME, PARTY_TYP_CD,PARTY_ROLE_TYP_TXT,ADMIN_SYS_PARTY_KEY_TXT,CMPNY_PREF_NAME,BILL_CNTCT_NAME,PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,RGNL_GRP_OFF_NBR from dbo.ENTPRS_PARTY_VW e where ADMIN_SYS_PARTY_KEY_TXT like '00000021%'")  
AdminSysName = dbretrieveData(recordset1,"ADMIN_SYS_NAME")
cmpnyPrefname = dbretrieveData(recordset1,"CMPNY_PREF_NAME")
BILL_CNTCT_NAME = dbretrieveData(recordset1,"BILL_CNTCT_NAME")
print(str(AdminSysName))
print(str(cmpnyPrefname))
print(str(BILL_CNTCT_NAME))
'''

# "Select ADMIN_SYS_NAME, PARTY_TYP_CD,PARTY_ROLE_TYP_TXT,ADMIN_SYS_PARTY_KEY_TXT,CMPNY_PREF_NAME,BILL_CNTCT_NAME,PLCYHLDR_MSTR_NBR,PLCYHLDR_BR_NBR,PLCYHLDR_DEPT_NBR,RGNL_GRP_OFF_NBR from dbo.ENTPRS_PARTY_VW e where ADMIN_SYS_PARTY_KEY_TXT like '00000006%'"

'''----------------------------------------SOAPPost---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)

ex: dbResult = wsResult = SOAPPost(url=url1,reqBody=body1,reqHeader=headers1,reqPath=reqPath1)
-------------------------------------------------------------------------------------------------------------------'''

def SOAPPost(url,reqBody,reqHeader,reqPath):
    response = requests.post(url,data=reqBody,headers=reqHeader)
    rsp = response.content
    tree = etree.fromstring(rsp)
    doc = etree.ElementTree(tree)

    find_val1 = etree.XPath(reqPath)
    return find_val1(tree)
    
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------



def SOAPPost_Response(url,reqBody,reqHeader):
    response = requests.post(url,data=reqBody,headers=reqHeader)
    rsp = response.content
    tree = etree.fromstring(rsp)
    doc = etree.ElementTree(tree)
    return tree



def SOAPPost_getNodeValue(tree,reqPath):
    find_val1 = etree.XPath(reqPath)
    return find_val1(tree)

'''
url = "https://internal-apigw-nonprod.oneamerica.com/claims-dev/organization/search"
reqBody1 = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:org="http://www.fineos.com/wscomposer/OrganisationSearchIntegration">
   <soapenv:Header/>
   <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" soapenv:mustUnderstand="1">
    <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-969899140">
        <wsse:Username>CONTENT</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.#PasswordText">CONTENT</wsse:Password>
    </wsse:UsernameToken>
   </wsse:Security> 
   <soapenv:Body> 
<p:OrganisationSearchIntegrationRequest xmlns:p="http://www.fineos.com/wscomposer/OrganisationSearchIntegration"> 
<name>test</name>
<customerNumber/> 
<registrationNumber/> 
<telNo/> 
<premisesNumber/> 
<addressLine/> 
<postCode/> 
<country/> 
<policyNo></policyNo> 
<additional-data-set/> 
</p:OrganisationSearchIntegrationRequest> 
</soapenv:Body>
</soapenv:Envelope>"""
reqHeader = {'content-type': 'text/xml'}
reqPath1 = "//OCOrganisation/Name/text()"
reqPath2 = "//OCOrganisation/ReferenceNo/text()"
reqPath3 = "//OCOrganisation/SourceSystem/FullId/text()"

tree1 = SOAPPost_Response(url,reqBody1,reqHeader)
print(SOAPPost_getNodeValue(tree1,reqPath1))
print(SOAPPost_getNodeValue(tree1,reqPath2))
print(SOAPPost_getNodeValue(tree1,reqPath3))

'''

'''----------------------------------------JSONGet---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)

ex: dbResult = wsResult = JSONGet(url=url1,reqHeader=headers1,reqPath=reqPath1)
-------------------------------------------------------------------------------------------------------------------'''

def JSONGet(url,reqHeader,reqPath):
    response = requests.get(url,headers=reqHeader)
    rsp = response.content
    
    json_data = json.loads(rsp)
    jsonpath_expression = parse(reqPath)
    match = jsonpath_expression.find(json_data)
    return {match[0].value}
    
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------



def Sync(xpth):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))



def Click(xpth):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).click()



def Set(xpth,val1):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).send_keys(str(val1))
    
'''----------------------------------------Fineos Login---------------------------------------------------------------------------
gets Pararmeter as Url and Search Type

ex: Login(url=url1,SearchType)
-------------------------------------------------------------------------------------------------------------------'''

def Login(url,searchType):
    
    driver.get(url)
    driver.maximize_window()
    if(searchType == 'OrgSearch'):
        xpth_srchPartyLink = "//a[contains(@id,'_MENUITEM.SearchPartieslink')]"
        Click(xpth_srchPartyLink)
        #driver.implicitly_wait(5)
    
        xpth_rdoOrganization = "//input[contains(@id,'_Organisation_GROUP')]"
        #Sync(xpth_rdoOrganization)
        Click(xpth_rdoOrganization)



def Logout():
    xpth_Useroption = "//a[contains(@id,'UserOptionslink')]"
    Click(xpth_Useroption) 
    
    xpth_Logout = "//a[contains(@id,'MENUITEM.Logoutlink')]"
    Click(xpth_Logout)   
    
    xpth_Confirm = "//input[contains(@id,'_Logout_yes')]"
    Click(xpth_Confirm)
    
    driver.close()



def ReturnTableValue(xpth,enum="null",dbServer ="SQLD10747,4242",dbName ="ENTPRS_CLAIMS_DM" ):
    name = ""
    i = 0               
    if(enum == "null"):
        if(len(driver.find_elements_by_xpath("//a[contains(@id,'cmdNext')]")) > 0):
            while(len(driver.find_elements_by_xpath("//a[contains(@id,'cmdNext')]")) > 0):
                if(i != 0):
                     driver.find_element_by_xpath("//a[contains(@id,'cmdNext')]").click()
                for j in driver.find_elements_by_xpath(xpth):
                        if(name ==""):
                            name = j.text
                        else:    
                            name = name + ";" + j.text
                i = i + 1
            if(len(driver.find_elements_by_xpath("//a[contains(@id,'cmdPrev')]")) > 0):    
                driver.find_element_by_xpath("//a[contains(@id,'cmdPrev')]").click()
        else:
             for j in driver.find_elements_by_xpath(xpth):
                        if(name ==""):
                            name = j.text
                        else:    
                            name = name + ";" + j.text
                            
    else:
         if(len(driver.find_elements_by_xpath("//a[contains(@id,'cmdNext')]")) > 0):
            while(len(driver.find_elements_by_xpath("//a[contains(@id,'cmdNext')]")) > 0):
                if(i != 0):
                     driver.find_element_by_xpath("//a[contains(@id,'cmdNext')]").click()
                for j in driver.find_elements_by_xpath(xpth):
                    if(enum == "PartyType"):
                        sqlqry1 = "select fineos_enum_id from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_INSTNC_NAME = '" + j.text + "' and fineos_dom_name = 'Party Type'"
                    elif(enum == "PartySourceSystem"):
                        sqlqry1 = "select fineos_enum_id from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_INSTNC_NAME = '" + j.text + "' and fineos_dom_name = 'PartySourceSystem'"  
                          
                    if(name == ""):
                        name = ' '.join([str(elem) for elem in dbCall1(dbServer,dbName,sqlqry1)])
                    else:    
                        name = name + ";" + ' '.join([str(elem) for elem in dbCall1(dbServer,dbName,sqlqry1)])
                i = i + 1
            if(len(driver.find_elements_by_xpath("//a[contains(@id,'cmdPrev')]")) > 0):    
                driver.find_element_by_xpath("//a[contains(@id,'cmdPrev')]").click()
         else:
             for j in driver.find_elements_by_xpath(xpth):
                 if(enum == "PartyType"):
                        sqlqry1 = "select fineos_enum_id from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_INSTNC_NAME = '" + j.text + "' and fineos_dom_name = 'Party Type'"
                 elif(enum == "PartySourceSystem"):
                        sqlqry1 = "select fineos_enum_id from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_INSTNC_NAME = '" + j.text + "' and fineos_dom_name = 'PartySourceSystem'" 
                 if(name == ""):
                        name = ' '.join([str(elem) for elem in dbCall1(dbServer,dbName,sqlqry1)])
                 else:    
                        name = name + ";" + ' '.join([str(elem) for elem in dbCall1(dbServer,dbName,sqlqry1)])   
                            
    return name    
 
#----------------------------------------End of SOAPPost---------------------------------------------------------------------------


'''----------------------------------------ComparePosts---------------------------------------------------------------------------
gets Pararmeter as Url, reqBody, reqHeaders and ReqPath(xpath of the node to be retrieved)
ex: ComparePost(wsValue=val1,dbValue=val2)
-------------------------------------------------------------------------------------------------------------------'''
def ComparePost(wsValue,dbValue):
    try:
        rtnVal = 1
        wsList = wsValue.split(";")
        dbList = dbValue.split(";")
        if(len(wsList) == len(dbList)):
            if(Counter(wsList) == Counter(dbList)):
                result = 1
                rtnVal = 1
                #print("Pass")
                #logging.info("Pass - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                html_logger.dbg("Pass - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n")
            else:
                result = 1
                if(len(dbValue)>1):
                    dbValue = dbValue.lower()
                    wsValue = wsValue.lower()
                    for i in range(len(dbValue.split(";"))):
                        if(dbValue.split(";")[i] in wsValue.split(";")[i]):
                           result = result * 1
                        else:
                            result = result * 0   
                        
                    if(result == 1):
                        #print("Pass")
                        #logging.info("Pass - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                        html_logger.dbg("Pass - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n")
                        rtnVal = 1
                    else:
                        #print("Fail") 
                        #logging.error("Fail - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                        html_logger.err("Fail - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n")
                        rtnVal = 0
                else:
                    if(dbValue in wsValue):
                        #print("Pass")
                        #logging.info("Pass - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                        html_logger.dbg("Pass - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n")
                        rtnVal = 1
                    else:
                        #print("Fail") 
                        #logging.error("Fail - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n") 
                        html_logger.err("Fail - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n")
                        rtnVal = 0  
        else:
             html_logger.err("Fail - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n")                    
        #-----------------------------------------------------------------------------------------------------------------------------
        return rtnVal
    except:
        html_logger.err("Fail -Error Occured hence exiting"  + "\n")
        return 0



def CompareallPost(wsValue,dbValue,finValue):
    try:
        rtnVal = 1
        wsList = wsValue.split(";")
        dbList = dbValue.split(";")
        finList = finValue.split(";")
        
        if(len(wsList) == len(dbList) == len(finList)):
            if(Counter(wsList) == Counter(dbList) == Counter(finList)):
                result = 1
                rtnVal = 1
                #print("Pass")
                #logging.info("Pass - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                html_logger.dbg("Pass - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n and FinValue is " + str(finValue))
            else:
                result = 1
                if(len(dbValue)>1):
                    for i in range(len(dbValue.split(";"))):
                        if(dbValue.split(";")[i] in wsValue.split(";")[i]):
                           result = result * 1
                        else:
                            result = result * 0  
                             
                        if(dbValue.split(";")[i] in finValue.split(";")[i]):
                           result = result * 1
                        else:
                            result = result * 0
                            
                    if(result == 1):
                        #print("Pass")
                        #logging.info("Pass - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                        html_logger.dbg("Pass - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n  and FinValue is " + str(finValue))
                        rtnVal = 1
                    else:
                        #print("Fail") 
                        #logging.error("Fail - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                        html_logger.err("Fail - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n  and FinValue is " + str(finValue))
                        rtnVal = 0
                else:
                    if((dbValue in wsValue) and (dbValue in finValue)):
                        #print("Pass")
                        #logging.info("Pass - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n")
                        html_logger.dbg("Pass - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n and FinValue is " + str(finValue))
                        rtnVal = 1
                    else:
                        #print("Fail") 
                        #logging.error("Fail - wsValue is " + str(wsValue) + " dbValue is " + str(dbValue) + "\n") 
                        html_logger.err("Fail - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n and FinValue is " + str(finValue))
                        rtnVal = 0  
        else:
             html_logger.err("Fail - wsValue is " + str(wsValue) + "\n dbValue is " + str(dbValue) + "\n and FinValue is " + str(finValue))                    
        #-----------------------------------------------------------------------------------------------------------------------------
        return rtnVal
    except:
        html_logger.err("Fail -Error Occured hence exiting"  + "\n")
        return 0


       
def CompareXmlDb():
    print("Process Started ...")
    df = pd.read_excel(parentFolder + "TestData\\WebService.xlsx", sheet_name="Sheet1")
    
    for j in df.index:  
        html_logger.info("\n##############################################################################################")
        html_logger.info("---------------------------Start of Iteration    " + str(j+1) + "----------------------------")
        html_logger.info("-----------------------" + df['Description'][j] + "----------------------------------------------")
        html_logger.info("##############################################################################################\n")
        endResult = 1
        wsResult = ""
        dbResult = ""  
        for i in df.columns:
            if('Service' in i):
                url1 = df['URL'][j]
                reqBody1 = df['Request'][j]
                reqHeader1 = {'content-type': 'text/xml'} # df['Headers'][j]
                reqNode = df[i][j]   
                     
                wsResult = SOAPPost(url=url1,reqBody=reqBody1,reqHeader=reqHeader1,reqPath=reqNode)   #SOAPPost(url=url1,reqBody=body1,reqHeader=headers1,reqPath=reqPath1)  #
                html_logger.info("WebService for " + reqNode + " is " + str(wsResult))
                #print(str(i) + "------" + str(df[i][j]))
            elif('Database' in i):
                dbServer1 = df['DBServer'][j]
                dbName1 = df['DBName'][j]
                sqlQuery2 = df[i][j]
                
                dbResult = dbCall(dbServer=dbServer1,dbName=dbName1,sqlQuery=sqlQuery2)   
               # html_logger.info("DB value for the query " + sqlQuery2 + " is " + str(dbResult))
                html_logger.info("DB value for the query is " + str(dbResult))
                    
            if(wsResult != "" and dbResult !=""):     
                rtnVal1 = ComparePost(wsValue = wsResult, dbValue =dbResult)
                endResult = endResult * rtnVal1
                wsResult = ""
                dbResult = ""        
        if(endResult ==1):
            html_logger.info("-----The Test is a Pass------------------") 
        else:
            html_logger.info("-----The Test is a Fail.. please check the log for details------------")            
    print("Process Completed")   
    
    
   
def CompareXMLFineos():  
    print("Process Started ...")
    df = pd.read_excel(parentFolder + "TestData\\WebService.xlsx", sheet_name="Sheet2", dtype=str)
    df.fillna('', inplace=True)
    
    for j in df.index:  
        html_logger.info("\n##############################################################################################")
        html_logger.info("---------------------------Start of Iteration    " + str(j+1) + "----------------------------")
        html_logger.info("-----------------------" + df['Description'][j] + "----------------------------------------------")
        html_logger.info("##############################################################################################\n")
        endResult = 1
        wsResult = ""
        FinResult = ""
        findone = False;
        serdone = False;  
        for i in df.columns:
             if('Service' in i):
                serdone = True 
                url1 = df['URL'][j]
                reqBody1 = df['Request'][j]
                reqHeader1 = {'content-type': 'text/xml'} # df['Headers'][j]
                reqNode = df[i][j]   
                if(reqNode != ''):     
                    wsResult = SOAPPost(url=url1,reqBody=reqBody1,reqHeader=reqHeader1,reqPath=reqNode)   #SOAPPost(url=url1,reqBody=body1,reqHeader=headers1,reqPath=reqPath1)  #
                    wsResult = ';'.join([str(elem) for elem in wsResult])
                    html_logger.info("WebService for " + reqNode + " is " + str(wsResult))
                    #print(str(i) + "------" + str(df[i][j]))
             
             elif('Fineos' in i):
                findone = True 
                if(df['SrchType'][j] == 'OrgSearch'): 
                    dbServer1 = df['DBServer'][j]
                    dbName1 = df['DBName'][j]
                    Finval = df[i][j]
                    if(Finval != ''):
                        orgName = df['OrgName'][j]
                        polNumber = df['PolicyNumber'][j]
                        
                        xpth_orgName = "//input[contains(@id,'Name')][contains(@id,'PersonOrganisationSearchSupportWidget')]"
                        driver.find_element_by_xpath(xpth_orgName).clear()
                        Set(xpth_orgName,orgName)
                        
                        xpth_polNum = "//input[contains(@id,'Policy_Reference')][contains(@id,'PolicySearchSupportWidget')]"
                        driver.find_element_by_xpath(xpth_polNum).clear()
                        Set(xpth_polNum,polNumber)
                                           
                        xpth_SearchBtn = "//input[contains(@id,'_searchButton')]"
                        Click(xpth_SearchBtn)
            
                        if(len(driver.find_elements_by_xpath("//*[@id='PageMessage1']"))> 0 and len(driver.find_elements_by_xpath("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[5]")) == 0):
                            print("no result found")
                            Click("//a[contains(@id,'btn_close_popup_msg')]")
                            FinResult = ""
                        else:
                            if(len(driver.find_elements_by_xpath("//*[@id='PageMessage1']"))> 0):
                                Click("//a[contains(@id,'btn_close_popup_msg')]")
                            if(Finval == "FinName"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[5]")  
                            elif(Finval == "FinPolicyNo"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[6]")
                            elif(Finval == "FinSource"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[8]",'PartySourceSystem',dbServer1,dbName1)
                            elif(Finval == "FinType"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[4]",'PartyType',dbServer1,dbName1)    
                           # html_logger.info("DB value for the query " + sqlQuery2 + " is " + str(dbResult))
                            html_logger.info("Fineos value for the query is " + str(FinResult))  
            
             #if(wsResult != "" and FinResult !=""):
             if(findone and serdone):
                findone = False
                serdone = False 
                if(wsResult != "" and FinResult !=""):      
                    rtnVal1 = ComparePost(wsValue = wsResult, dbValue =FinResult)
                    endResult = endResult * rtnVal1
                    wsResult = ""
                    FinResult = ""
                elif(wsResult == "" and FinResult ==""):
                    html_logger.dbg("Pass - wsValue is " + str(wsResult) + "\n and Fineos Value is " + str(FinResult) + "\n")    
                else:
                    html_logger.err("Fail - wsValue is " + str(wsResult) + "\n but Fineos Value is " + str(FinResult) + "\n")
        if(endResult ==1):
            html_logger.info("-----The Test is a Pass------------------") 
        else:
            html_logger.info("-----The Test is a Fail.. please check the log for details------------")            
    print("Process Completed") 
   # Logout()  
                               
'''
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical error message')
'''

def CompareXML_DB_Fineos():  
    print("Process Started ...")
    df = pd.read_excel(parentFolder + "TestData\\WebService.xlsx", sheet_name="WSTesting", dtype=str)
    df.fillna('', inplace=True)
    
    for j in df.index:  
        recordset1 = ""
        # --New -   Get New recordset
        
        html_logger.info("\n##############################################################################################")
        html_logger.info("---------------------------Start of Iteration    " + str(j+1) + "----------------------------")
        html_logger.info("-----------------------" + df['Description'][j] + "----------------------------------------------")
        html_logger.info("##############################################################################################\n")
        endResult = 1
        wsResult = ""
        dbResult = ""
        FinResult = ""
        findone = False;
        serdone = False;
        dbdone = False;  
        for i in df.columns:
             if('FetchDB' in i):
                 dbServer1 = df['DBServer'][j]
                 dbName1 = df['DBName'][j]
                 sqlQuery2 = df[i][j]
                 recordset1 = dbCall_multi(dbServer1,dbName1,sqlQuery2)
             
             elif('WSRequest' in i):
                url1 = df['URL'][j]
                reqBody1 = df['WSRequest'][j]
                reqHeader1 = {'content-type': 'text/xml'} 
                tree1 = SOAPPost_Response(url1,reqBody1,reqHeader1)
                
             elif('startFineos' in i):
                if(df['startFineos'][j] == 'Y'): 
                    url = df['Fin_URL'][j] 
                    searchType = df['SrchType'][j] 
                    Login(url,searchType)
                    
             elif('Service' in i):
                serdone = True 
                reqNode = df[i][j]   
                if(reqNode != ''):     
                    wsResult = SOAPPost_getNodeValue(tree1,reqNode)
                    wsResult = ';'.join([str(elem) for elem in wsResult])
                    html_logger.info("WebService for " + reqNode + " is " + str(wsResult))

             elif('Database' in i):
                dbdone = True; 
                SqlColName = df[i][j]
                # --New -  query just colum names & store in list
                if(SqlColName != ''):
                    dbResult = dbretrieveData(recordset1,SqlColName)
                    dbResult = ';'.join([str(elem) for elem in dbResult]) 
                    html_logger.info("DB value for the query is " + str(dbResult))
                       
             elif('Fineos' in i):
                findone = True 
                if(df['SrchType'][j] == 'OrgSearch'): 
                    dbServer1 = df['DBServer'][j]
                    dbName1 = df['DBName'][j]
                    Finval = df[i][j]
                    if(Finval != ''):
                        orgName = df['OrgName'][j]
                        polNumber = df['PolicyNumber'][j]
                        
                        xpth_orgName = "//input[contains(@id,'Name')][contains(@id,'PersonOrganisationSearchSupportWidget')]"
                        driver.find_element_by_xpath(xpth_orgName).clear()
                        Set(xpth_orgName,orgName)
                        
                        xpth_polNum = "//input[contains(@id,'Policy_Reference')][contains(@id,'PolicySearchSupportWidget')]"
                        driver.find_element_by_xpath(xpth_polNum).clear()
                        Set(xpth_polNum,polNumber)
                                           
                        xpth_SearchBtn = "//input[contains(@id,'_searchButton')]"
                        Click(xpth_SearchBtn)
            
                        if(len(driver.find_elements_by_xpath("//*[@id='PageMessage1']"))> 0 and len(driver.find_elements_by_xpath("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[5]")) == 0):
                            print("no result found")
                            Click("//a[contains(@id,'btn_close_popup_msg')]")
                            FinResult = ""
                        else:
                            if(len(driver.find_elements_by_xpath("//*[@id='PageMessage1']"))> 0):
                                Click("//a[contains(@id,'btn_close_popup_msg')]")
                            if(Finval == "FinName"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[5]")  
                            elif(Finval == "FinPolicyNo"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[6]")
                            elif(Finval == "FinSource"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[8]",'PartySourceSystem',dbServer1,dbName1)
                            elif(Finval == "FinType"):
                                FinResult = ReturnTableValue("//*[contains(@id,'PartySearchResultListviewWidget')]/tbody/tr/td[4]",'PartyType',dbServer1,dbName1)  
                              
                           # html_logger.info("DB value for the query " + sqlQuery2 + " is " + str(dbResult))
                            html_logger.info("Fineos value for the query is " + str(FinResult)) 
                            
                           # tags = {'name': 'TagName','policyno':'PolicyNo','source':'Source','partyType':'PartyType'}
                           # ReturnallTableValue(tags) 
                    #else:
                         #FinResult = "empty"
                                
             #if(wsResult != "" and FinResult !=""):
             if(findone and serdone and dbdone):
                findone = False
                serdone = False
                dbdone = False; 
                if(wsResult != "" and dbResult !="" and FinResult != ""):      
                    rtnVal1 = CompareallPost(wsValue = wsResult, dbValue = dbResult, finValue = FinResult)
                    endResult = endResult * rtnVal1
                    wsResult = ""
                    FinResult = ""
                    dbResult = ""
                elif(wsResult != "" and dbResult !="" and FinResult == ""):      
                    rtnVal1 = ComparePost(wsValue = wsResult, dbValue =dbResult)
                    endResult = endResult * rtnVal1
                    wsResult = ""
                    FinResult = "" 
                    dbResult = ""
                elif(wsResult == "" and FinResult == "" and dbResult == ""):
                    #html_logger.dbg("Pass - wsValue is " + str(wsResult) + "\n and Fineos Value is " + str(FinResult) + "\n and database value is " + str(dbResult))
                    pass    
                else:
                    html_logger.err("Fail - wsValue is " + str(wsResult) + "\n but Fineos Value is " + str(FinResult) + "\n and database value is " + str(dbResult))

        if(endResult ==1):
            html_logger.info("-----The Test is a Pass------------------") 
        else:
            html_logger.info("-----The Test is a Fail.. please check the log for details------------")
                     
    print("Process Completed")
    #Logout()  
    
#------------------------Tests starts Here----------------------------------------
#CompareXmlDb
    
def BrowserOpen(brow,url):
    """ if brow.strip() == 'chrome':
        driver = webdriver.Chrome('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\chromedriver.exe')
    elif brow.strip()== 'firefox':
        driver= webdriver.Firefox('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\geckodriver.exe')   
    """
   # driver = webdriver.Chrome('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\chromedriver.exe')
    if url != '':
        driver.get(url)   
        driver.implicitly_wait(5000)
        
def ElementClick(xpath): 
    if(driver.find_element_by_xpath(xpath).get_attribute('id') != ''):
        html_logger.dbg("Clicked " + driver.find_element_by_xpath(xpath).get_attribute('id'))
    else:
        html_logger.dbg("Clicked " + xpath)  
          
    driver.find_element_by_xpath(xpath).click()
    driver.implicitly_wait(1)
   

def ElementEnter(xpath,val):
    html_logger.dbg("Entered " + val  + " in " + xpath)
    driver.find_element_by_xpath(xpath).clear()
    driver.find_element_by_xpath(xpath).send_keys(val)
    driver.implicitly_wait(1)

def VerifyTextContains(xpath,val):
    txt =  driver.find_element_by_xpath(xpath).get_attribute("innerHTML") 
    html_logger.dbg(txt + " present in " + xpath)
    if(val in txt):
         html_logger.dbg("Pass " + txt + " present in " + xpath)
    else:
         html_logger.err("Fail " + val + " not present in " + xpath + " but the value present was " + txt)     

def verifyElementPresent(xpath):
    if(len(driver.find_elements_by_xpath(xpath)) > 0):
        html_logger.dbg("Pass element " + xpath + " present as expected")
    else:
        html_logger.err("Fail element " + xpath + " not present as expected")

        
def Popup(val):
    if(val=='accept'):
        #driver.switch_to_alert().accept() 
        driver.switch_to.alert.accept()
        driver.implicitly_wait(1)
        html_logger.dbg("Pop up accepted")   
    else:
        #driver.switch_to_alert().dismiss()
        driver.switch_to.alert.dismiss()
        driver.implicitly_wait(1)
        html_logger.dbg("Pop up dismissed")

def callStory(us):
        html_logger.dbg("#####################################")
        html_logger.info("Start of User story " + us)
        html_logger.dbg("#####################################")
        cookie1 = 'ajs_group_id=null; ajs_anonymous_id=%2294b03847-77a3-4883-9d64-fad8cd1677a8%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_981aa0ccfd9abbb1bce00285c0f1017f22a81aad_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Nlc3Npb24tc2VydmljZSIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sInJlZnJlc2hUaW1lb3V0IjoxNTgwNjAyNzE4LCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiMzkwMDgyMTgtZDVjNi00ZmIwLWFlMTgtYmUxMDM4ZjA5YTAyIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNTgwNjAyMTE4LCJleHAiOjE1ODMxOTQxMTgsImlhdCI6MTU4MDYwMjExOCwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiIzOTAwODIxOC1kNWM2LTRmYjAtYWUxOC1iZTEwMzhmMDlhMDIifQ.IQX3Qs_oEdyDfU5stIvNWg_24-I1bG9itzK4N0PKpLrcLffBTpQeLB0EjeyxkbO_342QaVWrRcUsoBeVy8kLWAvM_J9VYmlBUeNvSQBEYgajTAvLwfi3B19_sPfxGKltIxjTIOoCt46uA6BA_YbkyGVk_kUgt2TD6fpeVYItg0MhbBL2uYoyMbSQctuuW9X__yiqKeuWjeEDz0VuuRISRgEF9beSxQ8AsU9MxD0YylBOAUyGelv37ajpxSjUEswURdPicSDuULU5D7w7lQOrvVu-vVI_oX6i5zMf3JiF-uWEa5g6gcJuVhHB26JVXNMqz0oEce6tRFxi-14C45ZRzA'
        headers1 = 'headers = {''Cookie'':''' + cookie1 + "''}" 
        r = requests.get(url = 'https://oneamerica.atlassian.net/rest/api/2/issue/' + us + '/comment',headers ={'Cookie': cookie1})   #EC-2584
        jsonval = r.json()
        
        '''try:
            driver.close()
        except:
            pass'''
        
        y = json.loads(json.dumps(jsonval))
        for i in y["comments"]:
            text1 = i["body"]
            if '~code~' in text1:
                codetxt = text1
                arrcode = codetxt.split('\n')
                
                for linecode in arrcode:
                    if linecode != '':
                        arrLineElement = linecode.split("|")
                        if arrLineElement[0] == '~code~':
                            pass
                        
                        elif '~Scenario' in arrLineElement[0]:
                            html_logger.info("Start of Scenario " + arrLineElement[0].replace("~Scenario",""))
                            
                        elif arrLineElement[1] == 'open':
                            BrowserOpen(arrLineElement[2],arrLineElement[3])
                            
                        elif arrLineElement[1] == 'click':
                                    ElementClick(arrLineElement[2].replace("\\",""))  
                                    
                        elif arrLineElement[1] == 'enter':
                                    ElementEnter(arrLineElement[2].replace("\\",""), arrLineElement[3]) 
                                    
                        elif arrLineElement[1] == 'newwindow':   
                                    driver.switch_to.window(driver.window_handles[1])  
            
                        elif arrLineElement[1] == 'popup':
                                    Popup(arrLineElement[2].replace("\\","")) 
                                    
                        elif arrLineElement[1] == 'story': 
                                     callStory(arrLineElement[2]) 
                                     
                        elif arrLineElement[1] == 'verifyTextContains':
                                    VerifyTextContains(arrLineElement[2].replace("\\",""), arrLineElement[3])   
                        
                        elif arrLineElement[1] == 'verifyElementPresent':
                                    verifyElementPresent(arrLineElement[2].replace("\\",""))                      

def callStory_backup(us):
        html_logger.dbg("#####################################")
        html_logger.info("Start of User story " + us)
        html_logger.dbg("#####################################")
        cookie1 = 'ajs_group_id=null; ajs_anonymous_id=%2294b03847-77a3-4883-9d64-fad8cd1677a8%22; atlassian.xsrf.token=BT13-7JEV-YVD3-MGC3_981aa0ccfd9abbb1bce00285c0f1017f22a81aad_lin; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Nlc3Npb24tc2VydmljZSIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YTI1YWIzNDliMGU3ZjM4NmQ4NDlmOTIiLCJlbWFpbERvbWFpbiI6Im9uZWFtZXJpY2EuY29tIiwiaW1wZXJzb25hdGlvbiI6W10sInJlZnJlc2hUaW1lb3V0IjoxNTgwNjAyNzE4LCJ2ZXJpZmllZCI6dHJ1ZSwiaXNzIjoic2Vzc2lvbi1zZXJ2aWNlIiwic2Vzc2lvbklkIjoiMzkwMDgyMTgtZDVjNi00ZmIwLWFlMTgtYmUxMDM4ZjA5YTAyIiwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNTgwNjAyMTE4LCJleHAiOjE1ODMxOTQxMTgsImlhdCI6MTU4MDYwMjExOCwiZW1haWwiOiJzcmlrYW50aC5uYXJheWFuYW5Ab25lYW1lcmljYS5jb20iLCJqdGkiOiIzOTAwODIxOC1kNWM2LTRmYjAtYWUxOC1iZTEwMzhmMDlhMDIifQ.IQX3Qs_oEdyDfU5stIvNWg_24-I1bG9itzK4N0PKpLrcLffBTpQeLB0EjeyxkbO_342QaVWrRcUsoBeVy8kLWAvM_J9VYmlBUeNvSQBEYgajTAvLwfi3B19_sPfxGKltIxjTIOoCt46uA6BA_YbkyGVk_kUgt2TD6fpeVYItg0MhbBL2uYoyMbSQctuuW9X__yiqKeuWjeEDz0VuuRISRgEF9beSxQ8AsU9MxD0YylBOAUyGelv37ajpxSjUEswURdPicSDuULU5D7w7lQOrvVu-vVI_oX6i5zMf3JiF-uWEa5g6gcJuVhHB26JVXNMqz0oEce6tRFxi-14C45ZRzA'
        headers1 = 'headers = {''Cookie'':''' + cookie1 + "''}" 
        r = requests.get(url = 'https://oneamerica.atlassian.net/rest/api/2/issue/' + us + '/comment',headers ={'Cookie': cookie1})   #EC-2584
        jsonval = r.json()
        
        '''try:
            driver.close()
        except:
            pass'''
        
        y = json.loads(json.dumps(jsonval))
        for i in y["comments"]:
            text1 = i["body"]
            if '~code~' in text1:
                codetxt = text1
                arrcode = codetxt.split('\n')
                for linecode in arrcode:
                    arrLineElement = linecode.split("|")
                    if arrLineElement[0] == 'open':
                        BrowserOpen(arrLineElement[1],arrLineElement[2])
                        
                    elif arrLineElement[0] == 'click':
                                ElementClick(arrLineElement[1].replace("\\",""))  
                                
                    elif arrLineElement[0] == 'enter':
                                ElementEnter(arrLineElement[1].replace("\\",""), arrLineElement[2]) 
                                
                    elif arrLineElement[0] == 'newwindow':   
                                driver.switch_to.window(driver.window_handles[1])  
        
                    elif arrLineElement[0] == 'popup':
                                Popup(arrLineElement[1].replace("\\","")) 
                                
                    elif arrLineElement[0] == 'story': 
                                 callStory(arrLineElement[1]) 

'''                                      
from requests.auth import HTTPBasicAuth
import base64
import json

#driver.maximize_window()

USList = 'EC-2584'  #'EC-2584;EC-3333'
arrUSList = USList.split(';')
for us in arrUSList:
    driver = webdriver.Chrome('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\Driver\\chromedriver.exe')
    driver.maximize_window()
    callStory(us)
    driver.close()
    
print("End of tests")   
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')
#driver.get('https://oneamerica.atlassian.net/rest/api/2/issue/EC-2584/comment')

'''
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage as ndi
from skimage import io

from skimage import feature
from skimage import data
from skimage import filters

# Generate noisy image of a square

#im = np.zeros((128, 128))
im = io.imread('C:\\Users\\T003320\\Pictures\\Test1.JPG')
print(im)
#im1 = filters.gaussian(im,10)
im1 = filters.hessian(im,sigmas=range(1,30,5))
io.imsave('C:\\Users\\T003320\\Pictures\\ProfilePic1.JPG', im1)
      
""" 
import numpy as np
import matplotlib.pyplot as plt

#define the vertical filter
vertical_filter = [[-1,-1,-1], [0,0,0], [1,2,1]]

#define the horizontal filter
horizontal_filter = [[-1,-1,1], [-2,0,2], [-1,0,1]]

#read in the pinwheel image
img = plt.imread('C:\\Users\\T003320\\Pictures\\ProfilePic.JPG')

#get the dimensions of the image
n,m,d = img.shape

#initialize the edges image
edges_img = img.copy()


#loop over all pixels in the image
for row in range(3, n-2):
    for col in range(3, m-2):
        
        #create little local 3x3 box
        local_pixels = img[row-1:row+2, col-1:col+2, 0]
        
        #apply the vertical filter
        vertical_transformed_pixels = vertical_filter*local_pixels
        #remap the vertical score
        vertical_score = vertical_transformed_pixels.sum()/4
        
        #apply the horizontal filter
        horizontal_transformed_pixels = horizontal_filter*local_pixels
        #remap the horizontal score
        horizontal_score = horizontal_transformed_pixels.sum()/4
        
        #combine the horizontal and vertical scores into a total edge score
        edge_score = (vertical_score**2 + horizontal_score**2)**.5
        
        #insert this edge score into the edges image
        edges_img[row, col] = [edge_score]*3

#remap the values in the 0-1 range in case they went out of bounds
edges_img = edges_img/edges_img.max()

plt.imsave("C:\\Users\\T003320\\Pictures\\ProfilePic1.JPG",edges_img)
"""