#----------------Imports--------------------------
'''
import pandas as pd
from pandas import ExcelFile
from pandas import ExcelWriter

import datetime, logging, sys

import html_logger
from html_logger import setup, info

parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

#----------------Imports--------------------------
#----------------Logging--------------------------
now = datetime.datetime.now()

LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\WebService_DB_"+ str(now.isoformat().replace(":","_")) + ".html"
setup("WebService_Test","Executed on " + str(now),LOG_FILENAME)

df1 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv")

df1['ADDRESSLINE'] = df1[['PAYMENTADD1', 'PAYMENTADD4']].astype(str).apply(lambda x: ''.join(x), axis=1)
print(df1['ADDRESSLINE'])

df1['ADDRESSLINEx'] = df1['PAYMENTADD1'] + "\n" + df1['PAYMENTADD2']
print(df1['ADDRESSLINEx'])
df1.to_csv("C:\\Users\\T003320\\Desktop\\test1.csv")

df2 = df1[df1.PAYMENTMETHOD.isin(['Check','Elec Funds Transfer'])]


df2.to_csv("C:\\Users\\T003320\\Desktop\\test2.csv")

'''
"""
#--------------Step 1----------------------------------------
#--------------Extract DataFrame from CSV/XML/DB-------------
import Payment_ReusableArtifacts
from Payment_ReusableArtifacts import Comparator,Parser,DFCompare
import pandas as pd

xmldf = sqldf = pd.DataFrame
#----------------Parse XML to Dataframe------------------------
xmlpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\OneAmerica_Disbursement.xml"
xmldfcols = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']
xmldf = Parser.parseXML(xmlpath,xmldfcols)    
#print(xmldf)
#----------------Parse SQL to Dataframe------------------------
date1 = '2020-01-14'
dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'
sql1 = '''select PAY_RQST_NBR,PAY_ADMIN_SYS,CMPNY_CD,SRC_CD,LGCY_SRC_CD,PAY_TYP,PAY_AMT,PAYEE_NAME,CHK_ADDR_LINE_1,CHK_ADDR_LINE_2,CHK_ADDR_LINE_3,CHK_ADDR_CITY,CHK_ADDR_ST,CHK_ADDR_ZIP_CD,MAIL_SORT_CD,ENT_OPER_USR_ID,RDFI_ROUT_NBR,RDFI_ACCT_NBR,RDFI_ACCT_TYP
            from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\')'''
sqldf = Parser.parseSQL(sql1,dbserver,dbname)
#print(sqldf)
#----------------Compare two Dataframes------------------------
cols = [['Pay_Rqst_Nbr','PAY_RQST_NBR','String'],['Pay_Admin_Sys','PAY_ADMIN_SYS','String']]
DFCompare.compare1(xmldf,sqldf,cols)
print("---------------------------------------------------------")

#----- merging columns to create new columns------------

xmldf['newCol'] = xmldf['Pay_Admin_Sys'] + " " + xmldf['Cmpny_Cd']
sqldf['newCol'] = sqldf['PAY_ADMIN_SYS'] + " " + sqldf['CMPNY_CD']

cols = [['Pay_Rqst_Nbr','PAY_RQST_NBR','String'],['Pay_Admin_Sys','PAY_ADMIN_SYS','String'],['newCol','newCol','String']]
DFCompare.compare1(xmldf,sqldf,cols)
print("---------------------------------------------------------")

#------------Scenario 1--------------------------
#################################################################################################

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv"
csvdf = Parser.parseCSV(csvpath)

#csvdf['newC'] = csvdf['C'].astype(str).str[0:2] -  SubString
#csvdf['newC'] = csvdf['AMALGAMATIONC'].astype(str).str.split(' ').str[0] - split text
#print(csvdf['newC'])
#print(csvdf)

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI1.csv"
csv1df = Parser.parseCSV(csvpath)
#print(csv1df)

cols = [['LASTUPDATEDATE','LASTUPDATEDATE','Date'],['C','C','float'],['TRANSSTATUSDA','TRANSSTATUSDA','Date'],['PAYEESOCNUMBE','PAYEESOCNUMBE','strLen-9'],
        ['CONTRACTREF','CONTRACTREF','string']]
DFCompare.compare1(csvdf,csv1df,cols)
print("---------------------------------------------------------")

#------------Scenario 2--------------------------
#################################################################################################
"""

from time import sleep
from picamera import PiCamera

camera = PiCamera()
camera.resolution = (1024, 768)
camera.start_preview()
# Camera warm-up time
sleep(2)
camera.capture('C:\\Users\\T003320\\Desktop\\foo.jpg')
print("end")