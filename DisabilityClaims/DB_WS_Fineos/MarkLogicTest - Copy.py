import pandas as pd
import numpy as np
import pyodbc 
from Payment_ReusableArtifacts import Comparator, Parser, DFCompare
import Payment_ReusableArtifacts
from html_logger import setup, info
import html_logger
from datetime import datetime
from sklearn.covariance.shrunk_covariance_ import OAS
from builtins import str
import xlsxwriter

def Policyholder():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    tabVal = Parser.parseTSV("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\load_ready_files\Policyholder.tsv")
    tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
    print (tabVal.head(5))
    
    sql1 = "Select * from Policyholder.Policyholder" # where POlicyholderMainNumber = 45462625"   
    df1 = Parser.parseMLSQL(sql1,"6NLOJLMOE-lb.z.marklogicsvc.com","data-hub-FINAL","eu-qa-sn","hNFV0$zp5")
    df1 = df1.sort_values('PolicyholderNumber').reset_index(drop = True)
    print(df1.head(5))
    
    cols = [['PolicyholderNumber','PolicyholderNumber','float'],['PolicyholderMainNumber','PolicyholderMainNumber','float'],
            ['PolicyholderBranchNumber','PolicyholderBranchNumber','float'],['PolicyholderDepartmentNumber','PolicyholderDepartmentNumber','float'],
            ['SpecialPolicyholderIdentifierSuffix','SpecialPolicyholderIdentifierSuffix','float'],['CorporationNumber','CorporationNumber','float'],
            ['CorporationSuffix','CorporationSuffix','String'],['PolicyholderName','PolicyholderName','String'],['PolicyholderEffectiveDate','PolicyholderEffectiveDate','String'],
            ['PolicyholderStatus','PolicyholderStatus','String'],['GroupSalesRepresentativeName','GroupSalesRepresentativeName','String'],
            ['GroupSalesRepresentativeNumber','GroupSalesRepresentativeNumber','float'],['RegionalSalesOfficeCode','RegionalSalesOfficeCode','String'],
            ['RegionalSalesOfficeName','RegionalSalesOfficeName','String'],['Address.AddressType','AddressType','String'],['Address.AddressLine1','AddressLine1','String'],
            ['Address.AddressLine2','AddressLine2','String'],['Address.AddressLine3','AddressLine3','String'],['Address.AddressLine4','AddressLine4','String'],
            ['Address.City','City','String'],['Address.State','State','String'],['Address.ZipCode','ZipCode','float'],['Address.County','County','String']]
    
    if(tabVal.shape[0] == df1.shape[0]):
        html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
    else:
        html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
    
    DFCompare.compare(tabVal,df1,cols)


def InvoiceHistory():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    tabVal = Parser.parseTSV("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\load_ready_files\InvoiceHistory.tsv")
    tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
    print (tabVal.head(5))
    
    sql1 = "select * from InvoiceHistory.InvoiceHistory" # where POlicyholderMainNumber = 45462625"   
    df1 = Parser.parseMLSQL(sql1,"6NLOJLMOE-lb.z.marklogicsvc.com","data-hub-FINAL","eu-qa-sn","hNFV0$zp5")
    df1 = df1.sort_values('PolicyholderNumber').reset_index(drop = True)
    print(df1.head(5))
    
    cols = [['PolicyholderNumber','PolicyholderNumber','float'],['PlanNumber','PlanNumber','float'],['PlanType','PlanType','String'],['DueDate','DueDate','String'],
            ['TransactionDate','TransactionDate','String'],['TransactionType','TransactionType','String'],['AccountsReceivableType','AccountsReceivableType','float'],
            ['AccountsReceivableSubType','AccountsReceivableSubType','float'],['CertificateNumber','CertificateNumber','float'],['CertificateTieBreaker','CertificateTieBreaker','float'],
            ['DocumentNumber','DocumentNumber','float'],['InvoiceNumber','InvoiceNumber','float'],['DocumentAmount','DocumentAmount','float'],
            ['DocumentAmountRenewed','DocumentAmountRenewed','float'],['NewDocumentAmount','NewDocumentAmount','float'],
            ['VolumeAmount','VolumeAmount','float'],['RenewedVolumeAmount','RenewedVolumeAmount','float'],['NewVolumeAmount','NewVolumeAmount','float'],
            ['EmployeeCount','EmployeeCount','float'],['RenewedEmployeeCount','RenewedEmployeeCount','float'],['NewEmployeeCount','NewEmployeeCount','float'],
            ['RateEffectiveDate','RateEffectiveDate','String'],['TransactionPostIdentifier','TransactionPostIdentifier','String'],['PolicyholderEffectiveDate','PolicyholderEffectiveDate','String'],
            ['PlanEffectiveDate','PlanEffectiveDate','String']]
    
    if(tabVal.shape[0] == df1.shape[0]):
        html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
    else:
        html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
    
    DFCompare.compare(tabVal,df1,cols)

def PolicyHolderPlan():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    tabVal = Parser.parseTSV("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\load_ready_files\PolicyholderPlan.tsv")
    tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
    print (tabVal.head(5))
    
    sql1 = "select * from PolicyholderPlan.PolicyholderPlan" # where POlicyholderMainNumber = 45462625"   
    df1 = Parser.parseMLSQL(sql1,"6NLOJLMOE-lb.z.marklogicsvc.com","data-hub-FINAL","eu-qa-sn","hNFV0$zp5")
    df1 = df1.sort_values('PolicyholderNumber').reset_index(drop = True)
    print(df1.head(5))
    
    cols = [['PolicyholderNumber','PolicyholderNumber','float'],['PlanNumber','PlanNumber','float'],['PlanType','PlanType','String'],
            ['PolicyholderPlanEffectiveDate','PolicyholderPlanEffectiveDate','String'],['PolicyholderPlanTerminationDate','PolicyholderPlanTerminationDate','String'],
            ['CommissionPayIndicator','CommissionPayIndicator','String'],['BillingPlanIndicator','BillingPlanIndicator','String'],['EverEffectiveIndicator','EverEffectiveIndicator','String'],
            ['RateNumber','RateNumber','String'],['PlanOriginalEffectiveDate','PlanOriginalEffectiveDate','String'],
            ['RenewedCommissionsPremiumIndicator','RenewedCommissionsPremiumIndicator','String']]
    
    if(tabVal.shape[0] == df1.shape[0]):
        html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
    else:
        html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
    
    DFCompare.compare(tabVal,df1,cols)


#Policyholder
#InvoiceHistory()
#PolicyHolderPlan()

def PolicyHolderPlan_EBDM():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    tabVal = Parser.parseTSV("K:\\IT WORK\\PROJ00014201 - Data Delivery Modernization\\Testing\\Pilot 2\\Sprint 13\\Load Ready Files\\20210608\PolicyholderPlan.TSV") #("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\LRF_version2\PolicyholderPlan.tsv")
    #tabVal = tabVal.groupby(['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate']).first()
    tabVal = tabVal.drop_duplicates(subset = ['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate','DocumentInfo.systemStart'],keep = 'last').reset_index(drop = True)
    #tabVal = tabVal[tabVal['DocumentInfo.systemEnd'].notnull()]
    tabVal['BillingPlanIndicator'] = tabVal["BillingPlanIndicator"].replace('','N').fillna('N')
    #tabVal['RenewedCommissionsPremiumIndicator'] = tabVal["RenewedCommissionsPremiumIndicator"].replace('','N').fillna('N')
    tabVal['RateNumber'] = tabVal["RateNumber"].replace('','0').fillna('NULL')
    #tabVal['DocumentInfo.systemStart'] = datetime.strptime(tabVal['DocumentInfo.systemStart'],'%Y-%m-%dT%H:%M:%S.%f')
    #tabVal['DocumentInfo.systemStart']=  tabVal['DocumentInfo.systemStart'].dt.strftime('%Y-%m-%dT%H:%M:%S.%f')
    tabVal['DocumentInfo.systemStart'] = pd.to_datetime(tabVal['DocumentInfo.systemStart'])
    tabVal['DocumentInfo.systemStart'] = tabVal['DocumentInfo.systemStart'].astype(str).map(lambda v: v.split('.')[0])
    tabVal['DocumentInfo.isLatest'][tabVal['DocumentInfo.isLatest'] == 'FALSE'] = 'N'
    tabVal['DocumentInfo.isLatest'][tabVal['DocumentInfo.isLatest'] == 'TRUE'] = 'Y' 
    
    #tabVal['DocumentInfo.systemStart_1'] = pd.to_datetime(tabVal['DocumentInfo.systemStart'], format='%Y-%m-%dT%H:%M:%S.%fZ')
    #tabVal['DocumentInfo.systemStart_1'] = tabVal['DocumentInfo.systemStart_1'].dt.strftime('%Y-%m-%d')
    
    #tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
    print(tabVal.head(5))
    
    sql1 = "select * from rpt.PLCYHLDR_PLAN where LOAD_BATCH_ID = 30"   #"select * from rpt.PLCYHLDR_PLAN p where PROC_MANAGEMENT_CURR_IND = 'Y'" # where POlicyholderMainNumber = 45462625"   
    df1 = Parser.parseSQL(sql1, "SQLD10130,4242", "EB_DM")
    #df1 = Parser.parseMLSQL(sql1,"6NLOJLMOE-lb.z.marklogicsvc.com","data-hub-FINAL","eu-qa-sn","hNFV0$zp5")
    df1 = df1.sort_values('PLCYHLDR_NBR').reset_index(drop = True)
    #df1['ODH_EFF_DT'] = datetime.strptime(df1['ODH_EFF_DT'],'%Y-%m-%d %H:%M:%S')
    #df1['ODH_EFF_DT'] =  df1['ODH_EFF_DT'].dt.strftime('%Y-%m-%d %H:%M:%S')
    df1['ODH_EFF_DT'] = pd.to_datetime(df1['ODH_EFF_DT'])
    #df1['ODH_EFF_DT_1']  = pd.to_datetime(df1['ODH_EFF_DT'] , format='%Y-%m-%d %H:%M:%S.%f')
    #df1['ODH_EFF_DT_1'] = df1['ODH_EFF_DT_1'].dt.strftime('%Y-%m-%d')
    #df1['ODH_EFF_DT'] = df1['ODH_EFF_DT'].astype(str).map(lambda v: v.split(' ')[0])
    print(df1.head(5))
    
    cols = [['PolicyholderNumber','PLCYHLDR_NBR','String'],['PlanNumber','PLAN_NBR','float'],['PlanType','PLAN_TYP','String'],
            ['PolicyholderPlanEffectiveDate','PLCYHLDR_PLAN_EFF_DT','String'],['PolicyholderPlanTerminationDate','PLCYHLDR_PLAN_TERM_DT','String'],
            ['CommissionPayIndicator','COMM_PAY_IND','String'],['BillingPlanIndicator','BILL_PLAN_IND','String'],['EverEffectiveIndicator','EVER_EFF_IND','String'],
            ['RateNumber','RT_NBR','String'],['PlanOriginalEffectiveDate','PLAN_ORIG_EFF_DT','String']
            ,['RenewedCommissionsPremiumIndicator','RENEWED_COMMISSIONS_PREM_IND','String'],['RatingProcessFileIndicator','RT_PROC_FILE_IND','String']
            ,['DocumentInfo.systemStart','ODH_EFF_DT','String'],['DocumentInfo.isLatest','PROC_MANAGEMENT_CURR_IND','String']]

    
    if(tabVal.shape[0] == df1.shape[0]):
        html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
    else:
        html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
    
    #DFCompare.compare(tabVal,df1,cols)
    #tabVal = tabVal.sort_values(by=['PolicyholderNumber','PlanNumber','DocumentInfo.systemStart'])
    tabVal['unique'] = tabVal["PolicyholderNumber"].astype(str) +" "+ tabVal["DocumentInfo.systemStart"].astype(str)   #tabVal['PolicyholderNumber'] + tabVal['PlanNumber'] + tabVal['DocumentInfo.systemStart']
    
    #df1 = df1.sort_values(by=['PLCYHLDR_NBR','PLAN_NBR','ODH_EFF_DT'])
    df1['unique'] =df1["PLCYHLDR_NBR"].astype(str) +" "+ df1["ODH_EFF_DT"].astype(str)
    
    tabVal = tabVal.astype(str)
    tabVal['EverEffectiveIndicator'] = tabVal['EverEffectiveIndicator'].str.upper()
    tabVal['PlanType'] = tabVal['PlanType'].str.upper()
    tabVal['PlanOriginalEffectiveDate'] = tabVal['PlanOriginalEffectiveDate'].fillna('None').replace('nan','None')
    tabVal['PolicyholderPlanTerminationDate'] = tabVal['PolicyholderPlanTerminationDate'].fillna('None').replace('nan','None')
    tabVal['RenewedCommissionsPremiumIndicator'] = tabVal['RenewedCommissionsPremiumIndicator'].fillna('None').replace('nan','None')
    
    df1 = df1.astype(str)
            
    result = DFCompare.newCompare(tabVal,df1,cols,'unique')
    print(parentFolder + "ReportValidation_"+ timenow + ".csv")
    result.to_csv(parentFolder + "ReportValidation_"+ timenow + ".csv")
    
    rowcnt = result.shape[0]
    
    writer = pd.ExcelWriter(parentFolder + "PolicyHolderPlan_EBDM_"+ timenow + ".xlsx", engine='xlsxwriter')
    result.to_excel(writer,sheet_name = 'Result')
    
    workbook = writer.book
    worksheet = writer.sheets['Result']
    
    # Add a format. Light red fill with dark red text.
    format1 = workbook.add_format({'bg_color': '#FFC7CE','font_color': '#9C0006'})
    
    # Add a format. Green fill with dark green text.
    format2 = workbook.add_format({'bg_color': '#C6EFCE','font_color': '#006100'})
    
    worksheet.conditional_format('A1:O'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    '|',
                                       'format':   format1})
    worksheet.conditional_format('A1:O'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    '|',
                                       'format':   format2})
    
    for i in range(2,rowcnt+2):
        worksheet.write_formula('P'+str(i), '=COUNTIF(A'+str(i)+':O'+str(i)+',"*|*")')
        worksheet.write_formula('Q'+str(i), '=IF(P'+str(i) +'=0,"Pass","Fail")')
     
    worksheet.conditional_format('P2:P'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'equal to',
                                    'value':    0,
                                    'format':   format2})
    worksheet.conditional_format('P2:P'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'greater than',
                                    'value':    0,
                                    'format':   format1})
    
    worksheet.conditional_format('Q2:Q'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    'Fail',
                                       'format':   format1})
    worksheet.conditional_format('Q2:Q'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    'Pass',
                                       'format':   format2})
       
    writer.save()
    print("################################################################")
    print("Remeber the output will be in Source(TSV) Data /Target(DB_ Data Format if there are differences")
    print("################################################################")
    
#Policyholder
#InvoiceHistory()
#PolicyHolderPlan()


def InvHistory__EBDM():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    tabVal = Parser.parseTSV("K:\\IT WORK\\PROJ00014201 - Data Delivery Modernization\\Testing\\Pilot 2\\Sprint 13\\Load Ready Files\\20210608\\InvoiceHistory.tsv")
    #tabVal = tabVal.groupby(['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate']).first()
    tabVal = tabVal.drop_duplicates(subset = ['PolicyholderNumber', 'PlanNumber','PlanType','DueDate','TransactionDate','AccountsReceivableType','AccountsReceivableSubType','DocumentNumber','DocumentInfo.systemStart'],keep = 'last').reset_index(drop = True)
    tabVal = tabVal[tabVal['DocumentInfo.systemEnd'].notnull()]
    #tabVal['EmployeeCount'] = tabVal['EmployeeCount'].astype(float)
    #tabVal['InvoiceNumber'] = tabVal['InvoiceNumber'].astype(float)
    #tabVal['NewEmployeeCount'] = tabVal['NewEmployeeCount'].astype(float)
    #tabVal['NewVolumeAmount'] = tabVal['NewVolumeAmount'].astype(float)
    
    tabVal = tabVal.apply(lambda x: pd.to_numeric(x, errors='ignore', downcast='float'))
    
    tabVal['DocumentAmount'] = tabVal["DocumentAmount"].replace('','0.0').fillna('0.0')
    tabVal['DocumentAmountRenewed'] = tabVal["DocumentAmountRenewed"].replace('','0.0').fillna('0.0')
    tabVal['NewDocumentAmount'] = tabVal["NewDocumentAmount"].replace('','0.0').fillna('0.0')
    tabVal['VolumeAmount'] = tabVal["VolumeAmount"].replace('','0.0').fillna('0.0')
    tabVal['RenewedVolumeAmount'] = tabVal["RenewedVolumeAmount"].replace('','0.0').fillna('0.0')
    
    tabVal['NewVolumeAmount'] = tabVal["NewVolumeAmount"].replace('','0.0').fillna('0.0')
    tabVal['EmployeeCount'] = tabVal["EmployeeCount"].replace('','0.0').fillna('0.0')
    tabVal['RenewedEmployeeCount'] = tabVal["RenewedEmployeeCount"].replace('','0.0').fillna('0.0')
    tabVal['NewEmployeeCount'] = tabVal["NewEmployeeCount"].replace('','0.0').fillna('0.0')
    
    #tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
    tabVal['AccountsReceivableSubType'] = tabVal['AccountsReceivableSubType'].replace("",'0').fillna('0')
    tabVal['DocumentInfo.systemStart'] = pd.to_datetime(tabVal['DocumentInfo.systemStart'])
    tabVal['DocumentInfo.systemStart'] = tabVal['DocumentInfo.systemStart'].astype(str).map(lambda v: v.split('.')[0])
    tabVal['DocumentInfo.isLatest'][tabVal['DocumentInfo.isLatest'] == 'FALSE'] = 'N'
    tabVal['DocumentInfo.isLatest'][tabVal['DocumentInfo.isLatest'] == 'TRUE'] = 'Y' 
    
    print(tabVal.head(5))
    
    sql1 = "select * from rpt.INV_HIST p where LOAD_BATCH_ID = 32" # where POlicyholderMainNumber = 45462625"   
    df1 = Parser.parseSQL(sql1, "SQLD10130,4242", "EB_DM")
    #df1 = Parser.parseMLSQL(sql1,"6NLOJLMOE-lb.z.marklogicsvc.com","data-hub-FINAL","eu-qa-sn","hNFV0$zp5")
    df1['ACCT_RECEIVABLE_SUBTYP'] = df1['ACCT_RECEIVABLE_SUBTYP'].replace("null",'0').fillna('0')
    df1 = df1.sort_values('PLCYHLDR_NBR').reset_index(drop = True)
    df1['ODH_EFF_DT'] = pd.to_datetime(df1['ODH_EFF_DT'])
    
    
    print(df1.head(5))
    
    cols = [['PolicyholderNumber','PLCYHLDR_NBR','String'],['PlanNumber','PLAN_NBR','float'],['PlanType','PLAN_TYP','String'],['DueDate','DUE_DT','String'],
            ['TransactionDate','TRANS_DT','String'],['TransactionType','TRANS_TYP_DESCR','String'],['AccountsReceivableType','ACCT_RECEIVABLE_TYP','String']
            ,['AccountsReceivableSubType','ACCT_RECEIVABLE_SUBTYP','String'],['CertificateNumber','CERT_NBR','float'],
            ['CertificateTieBreaker','CERT_TIE_BREAKER','float'],['DocumentNumber','DOC_NBR','float'],['InvoiceNumber','INV_NBR','String'],
            ['DocumentAmount','DOC_AMT','float'],['DocumentAmountRenewed','DOC_AMT_RENEWED','float'],['NewDocumentAmount','NEW_DOC_AMT','float'],
            ['VolumeAmount','VOL_AMT','float'],['RenewedVolumeAmount','RENEWED_VOL_AMT','float'],['NewVolumeAmount','NEW_VOL_AMT','float'],
            ['EmployeeCount','EMP_CNT','float'],['RenewedEmployeeCount','RENEWED_EMP_CNT','float'],['NewEmployeeCount','NEW_EMP_CNT','float'],
            ['RateEffectiveDate','RT_EFF_DT','String'],['TransactionPostIdentifier','TRANS_POST_ID_DESCR','String'],['PolicyholderEffectiveDate','PLCYHLDR_EFF_DT','String'],
            ['PlanEffectiveDate','PLAN_EFF_DT','String'],['DocumentInfo.systemStart','ODH_EFF_DT','String'],['DocumentInfo.isLatest','PROC_MANAGEMENT_CURR_IND','String']]

    tabVal['unique'] = tabVal["PolicyholderNumber"].astype(str) +" "+ tabVal["DocumentInfo.systemStart"].astype(str)   #tabVal['PolicyholderNumber'] + tabVal['PlanNumber'] + tabVal['DocumentInfo.systemStart']
    df1['unique'] =df1["PLCYHLDR_NBR"].astype(str) +" "+ df1["ODH_EFF_DT"].astype(str)
    df1["ODH_EFF_DT"] = df1["ODH_EFF_DT"].astype(str)
    df1 = df1.apply(lambda x: pd.to_numeric(x, errors='ignore', downcast='float'))
    df1['ODH_EFF_DT'] = pd.to_datetime(df1['ODH_EFF_DT'], format='%Y-%m-%d') 
    #df1['ODH_EFF_DT'] = df1['ODH_EFF_DT'].str.split(' ').str[0]
    
    tabVal['DocumentInfo.systemStart'] = pd.to_datetime(tabVal['DocumentInfo.systemStart'], format='%Y-%m-%d')
    #tabVal['DocumentInfo.systemStart']= tabVal['DocumentInfo.systemStart'].str.split(' ').str[0]
    
    if(tabVal.shape[0] == df1.shape[0]):
        html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
    else:
        html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
    
    DFCompare.compare(tabVal,df1,cols)
    tabVal = tabVal.astype(str)
    df1 = df1.astype(str)
    
    result = DFCompare.newCompare(tabVal,df1,cols,'unique')
    print(parentFolder + "ReportValidation_"+ timenow + ".csv")
    #result.to_csv(parentFolder + "ReportValidation_"+ timenow + ".csv")
    rowcnt = result.shape[0]
    
    writer = pd.ExcelWriter(parentFolder + "InvoiceHistory_EBDM_"+ timenow + ".xlsx", engine='xlsxwriter')
    result.to_excel(writer,sheet_name = 'Result')
    
    workbook = writer.book
    worksheet = writer.sheets['Result']
    
    # Add a format. Light red fill with dark red text.
    format1 = workbook.add_format({'bg_color': '#FFC7CE','font_color': '#9C0006'})
    
    # Add a format. Green fill with dark green text.
    format2 = workbook.add_format({'bg_color': '#C6EFCE','font_color': '#006100'})
    
    worksheet.conditional_format('A1:AB'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    '|',
                                       'format':   format1})
    worksheet.conditional_format('A1:AB'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    '|',
                                       'format':   format2})
    
    for i in range(2,rowcnt+2):
        worksheet.write_formula('AD'+str(i), '=COUNTIF(A'+str(i)+':AB'+str(i)+',"*|*")')
        worksheet.write_formula('AE'+str(i), '=IF(AD'+str(i) +'=0,"Pass","Fail")')
     
    worksheet.conditional_format('AD2:AD'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'equal to',
                                    'value':    0,
                                    'format':   format2})
    worksheet.conditional_format('AD2:AD'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'greater than',
                                    'value':    0,
                                    'format':   format1})
    
    worksheet.conditional_format('AE2:AE'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    'Fail',
                                       'format':   format1})
    worksheet.conditional_format('AE2:AE'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    'Pass',
                                       'format':   format2})
       
    writer.save()
    print("################################################################")
    print("Remeber the output will be in Source(TSV) Data /Target(DB_ Data Format if there are differences")
    print("################################################################")
    



#InvHistory()


def Policyholder_EBDM():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    tabVal = Parser.parseTSV("K:\\IT WORK\\PROJ00014201 - Data Delivery Modernization\\Testing\\Pilot 2\\Sprint 13\\Load Ready Files\\20210602\\Policyholder.tsv")  #("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\LRF_version2\Policyholder.tsv")
    #tabVal = tabVal.groupby(['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate']).first()
    tabVal = tabVal.drop_duplicates(subset = ['PolicyholderNumber','DocumentInfo.systemStart'],keep = 'last').reset_index(drop = True)
    tabVal = tabVal[tabVal['DocumentInfo.systemEnd'].notnull()]
    tabVal['PaidThruDate'] = tabVal['PaidThruDate'].replace('', '9999-01-01').replace(np.nan,'9999-01-01')
    tabVal['Address.AddressLine3'] = tabVal['Address.AddressLine3'].fillna('null')
    tabVal['Address.AddressLine4'] = tabVal['Address.AddressLine4'].fillna('null')
    tabVal['PaymentMethod'] = tabVal['PaymentMethod'].fillna('null')
    tabVal['BillingCycle'] = tabVal['BillingCycle'].fillna('null')
    tabVal['BillingLevel'] = tabVal['BillingLevel'].fillna('null')
    tabVal['AdminByCode'] = tabVal['AdminByCode'].fillna('null')
    tabVal['AdminByCodeDescription'] = tabVal['AdminByCodeDescription'].fillna('null')
    tabVal['AdminByNumber'] = tabVal['AdminByNumber'].fillna('null')
    tabVal['AdminNumberDescription'] = tabVal['AdminNumberDescription'].fillna('null')
    
    tabVal['AdminByCode_1'] = tabVal['AdminByCode'].astype(str).str[0:2]
    tabVal['AdminByNumber'][tabVal['AdminByNumber']  !='null'] = tabVal['AdminByNumber'].astype(str).str[0:-1]
    #tabVal['AdminByNumber'] = tabVal['AdminByNumber'].astype(str).str[0:-1]
    
    tabVal['DocumentInfo.systemStart_1'] = pd.to_datetime(tabVal['DocumentInfo.systemStart'], format='%Y-%m-%dT%H:%M:%S.%fZ')
    tabVal['DocumentInfo.systemStart_1'] = tabVal['DocumentInfo.systemStart_1'].dt.strftime('%Y-%m-%d')
    
    print(tabVal.head(5))
    
    sql1 = "select * from rpt.PLCYHLDR p where PROC_MANAGEMENT_CURR_IND = 'Y'" # where POlicyholderMainNumber = 45462625"   
    sql1 = "select * from rpt.PLCYHLDR where LOAD_BATCH_ID = '27'"
    
    df1 = Parser.parseSQL(sql1, "SQLD10130,4242", "EB_DM")
    #df1 = Parser.parseMLSQL(sql1,"6NLOJLMOE-lb.z.marklogicsvc.com","data-hub-FINAL","eu-qa-sn","hNFV0$zp5")
    df1['ODH_EFF_DT_1']  = pd.to_datetime(df1['ODH_EFF_DT'] , format='%Y-%m-%d %H:%M:%S.%f')
    df1['ODH_EFF_DT_1'] = df1['ODH_EFF_DT_1'].dt.strftime('%Y-%m-%d')
    
    '''df1['PAY_MTHD_DESCR'] = df1['PAY_MTHD_DESCR'].replace('NULL','')
    df1['BILL_CYC_DESCR'] = df1['BILL_CYC_DESCR'].replace('NULL','')
    df1['BILL_LVL_DESCR'] = df1['BILL_LVL_DESCR'].replace('NULL','')
    df1['ADDR_LINE_3'] = df1['ADDR_LINE_3'].replace('NULL','')
    df1['ADDR_LINE_4'] = df1['ADDR_LINE_4'].replace('NULL','')'''
    
    df1 = df1.sort_values('PLCYHLDR_NBR').reset_index(drop = True)
  
    print(df1.head(5))
    
    cols = [['PolicyholderNumber','PLCYHLDR_NBR','String'],['PolicyholderMainNumber','PLCYHLDR_MAIN_NBR','float'],['PolicyholderBranchNumber','PLCYHLDR_BR_NBR','String'],
            ['PolicyholderDepartmentNumber','PLCYHLDR_DEPT_NBR','String'],['SpecialPolicyholderIdentifierSuffix','SPEC_PLCYHLDR_ID_SFX','String'],['CorporationNumber','CORPORATION_NBR','String'],
            ['CorporationSuffix','CORPORATION_SFX','String'],['PolicyholderName','PLCYHLDR_NAME','String'],['PolicyholderEffectiveDate','PLCYHLDR_EFF_DT','String'],['PolicyholderStatus','PLCYHLDR_STAT_DESCR','String'],
            ['GroupSalesRepresentativeName','GRP_SLS_REP_NAME','String'],['GroupSalesRepresentativeNumber','GRP_SLS_REP_NBR','String'],['RegionalSalesOfficeCode','RGNL_SLS_OFF_CD','String'],
            ['RegionalSalesOfficeName','RGNL_SLS_OFF_NAME','String'],['PaidThruDate','PD_THRU_DT','String'],['PolicyholderStatusReasonCode','PLCYHLDR_STAT_RSN_DESCR','String'],
            ['AgentofRecord','AGENT_OF_REC_DESCR','String'],['PaymentMethod','PAY_MTHD_DESCR','String'],['BillingCycle','BILL_CYC_DESCR','String'],['BillingLevel','BILL_LVL_DESCR','String']
            ,['BillingMonth','BILL_MTH_NAME','String'],['BillingFrequency','BILL_FREQ_DESCR','String'],['BillingSystemCode','BILL_SYS_CD','String'],['BillingSystemCodeDescription','BILL_SYS_CD_DESCR','String'],
            ['BillingSubsystemCode','BILL_SUBSYSTEM_CD','String'],['BillingSubsystemCodeDescription','BILL_SUBSYSTEM_CD_DESCR','String'],['ServicingProducer','SVC_PRDCR_DESCR','String'],
            ['PolicyBillAlignmentDate','PLCY_BILL_ALIGNMENT_DT','String'],['BillingTypeIndicator','BILL_TYP_IND','String'],['Address.AddressType','ADDR_TYP','String'],
            ['Address.AddressLine1','ADDR_LINE_1','String'],['Address.AddressLine2','ADDR_LINE_2','String'],['Address.AddressLine3','ADDR_LINE_3','String'],['Address.AddressLine4','ADDR_LINE_4','String'],
            ['Address.City','CITY_NAME','String'],['Address.State','ST_NAME','String'],['Address.County','CNTY_CD','String'],['Address.ZipCode','ZIP_CD','String'],
            ['AdminByCode_1','ADMIN_BY_CD','String'],['AdminByCodeDescription','ADMIN_BY_CD_DESCR','String'],['AdminByNumber','ADMIN_BY_NBR','String'],['AdminNumberDescription','ADMIN_NBR_DESCR','String'],
            ['DocumentInfo.systemStart_1','ODH_EFF_DT_1','String']]
    
    
    df1['ODH_EFF_DT'] = pd.to_datetime(df1['ODH_EFF_DT'], format='%Y-%m-%d') 
    tabVal['DocumentInfo.systemStart'] = pd.to_datetime(tabVal['DocumentInfo.systemStart'], format='%Y-%m-%d')
    df1['ODH_EFF_DT']=  df1['ODH_EFF_DT'].astype(str).map(lambda v: v.split('.')[0])
    tabVal['DocumentInfo.systemStart']=  tabVal['DocumentInfo.systemStart'].astype(str).map(lambda v: v.split('.')[0])
    
     
    tabVal['unique'] = tabVal["PolicyholderNumber"].astype(str) +" "+ tabVal["DocumentInfo.systemStart"].astype(str)   #tabVal['PolicyholderNumber'] + tabVal['PlanNumber'] + tabVal['DocumentInfo.systemStart']
    df1['unique'] =df1["PLCYHLDR_NBR"].astype(str) +" "+ df1["ODH_EFF_DT"].astype(str)
    df1["ODH_EFF_DT"] = df1["ODH_EFF_DT"].astype(str)
    df1 = df1.apply(lambda x: pd.to_numeric(x, errors='ignore', downcast='float'))
    tabVal = tabVal.apply(lambda x: pd.to_numeric(x, errors='ignore', downcast='float'))
    
    #df1['ODH_EFF_DT'] = df1['ODH_EFF_DT'].str.split(' ').str[0]
    
    
    
    if(tabVal.shape[0] == df1.shape[0]):
        html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
    else:
        html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
    
    #DFCompare.compare(tabVal,df1,cols)
    
    tabVal = tabVal.astype(str)
    df1 = df1.astype(str)
    
    result = DFCompare.newCompare(tabVal,df1,cols,'unique')
    print(parentFolder + "ReportValidation_"+ timenow + ".csv")
    #result.to_csv(parentFolder + "ReportValidation_"+ timenow + ".csv")
    
    rowcnt = result.shape[0]
    
    writer = pd.ExcelWriter(parentFolder + "PolicyHolder_EBDM_"+ timenow + ".xlsx", engine='xlsxwriter')
    result.to_excel(writer,sheet_name = 'Result')
    
    workbook = writer.book
    worksheet = writer.sheets['Result']
    
    # Add a format. Light red fill with dark red text.
    format1 = workbook.add_format({'bg_color': '#FFC7CE','font_color': '#9C0006'})
    
    # Add a format. Green fill with dark green text.
    format2 = workbook.add_format({'bg_color': '#C6EFCE','font_color': '#006100'})
    
    worksheet.conditional_format('A1:AR'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    '|',
                                       'format':   format1})
    worksheet.conditional_format('A1:AR'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    '|',
                                       'format':   format2})
    
    for i in range(2,rowcnt+2):
        worksheet.write_formula('AS'+str(i), '=COUNTIF(A'+str(i)+':AR'+str(i)+',"*|*")')
        worksheet.write_formula('AT'+str(i), '=IF(AS'+str(i) +'=0,"Pass","Fail")')
     
    worksheet.conditional_format('AS2:AS'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'equal to',
                                    'value':    0,
                                    'format':   format2})
    worksheet.conditional_format('AS2:AS'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'greater than',
                                    'value':    0,
                                    'format':   format1})
    
    worksheet.conditional_format('AT2:AT'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    'Fail',
                                       'format':   format1})
    worksheet.conditional_format('AT2:AT'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    'Pass',
                                       'format':   format2})
       
    writer.save()
    print("################################################################")
    print("Remeber the output will be in Source(TSV) Data /Target(DB_ Data Format if there are differences")
    print("################################################################")
    
    

def report_diff(x):
    return x[0] if x[0] == x[1] else '{} | {}'.format(*x)
    
def Validation14Point_Trial():
    now = datetime.now()
    parentFolder = "C:\\Srikanth\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    #tabVal = Parser.parseTSV("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\LRF_version2\PolicyholderPlan.tsv")
    CDSData = Parser.parseExcel("C:\\Srikanth\\Srikanth\\Projects\\Claims\\14PointReport Data\\CDS_Data_20210518.xlsx", "Sheet1")
    CDSData = CDSData.drop(['BenAdjCount'],axis=1)
    CDSData = CDSData.sort_values('ClaimNumber').reset_index(drop = True)
    CDSData = CDSData.drop(columns =['BenAdj1Code','BenAdj2Code','BenAdj3Code','BenAdj4Code','BenAdj5Code','BenAdj6Code','BenAdj7Code','BenAdj8Code','BenAdj9Code','BenAdj10Code','BenAdj11Code','BenAdj12Code','BenAdj13Code','BenAdj14Code','BenAdj15Code','BenAdj16Code','BenAdj17Code','BenAdj18Code','BenAdj19Code','BenAdj20Code','BenAdj21Code','BenAdj22Code','BenAdj23Code','OpenDiaries','ClaimNbr','Diary1Code','Diary1Desc','Diary1AssignedTo','Diary1TargetDate','Diary2Code','Diary2Desc','Diary2AssignedTo','Diary2TargetDate','Diary3Code','Diary3Desc','Diary3AssignedTo','Diary3TargetDate','Diary4Code','Diary4Desc','Diary4AssignedTo','Diary4TargetDate','Diary5Code','Diary5Desc','Diary5AssignedTo','Diary5TargetDate','Diary6Code','Diary6Desc','Diary6AssignedTo','Diary6TargetDate','Diary7Code','Diary7Desc','Diary7AssignedTo','Diary7TargetDate','Diary8Code','Diary8Desc','Diary8AssignedTo','Diary8TargetDate','Diary9Code','Diary9Desc','Diary9AssignedTo','Diary9TargetDate','Diary10Code','Diary10Desc','Diary10AssignedTo','Diary10TargetDate','Diary11Code','Diary11Desc','Diary11AssignedTo','Diary11TargetDate','Diary12Code','Diary12Desc','Diary12AssignedTo','Diary12TargetDate','Diary13Code','Diary13Desc','Diary13AssignedTo','Diary13TargetDate'],axis=1)
    CDSData = CDSData.replace('NULL',0).replace('nan',0).replace('NaT',0).replace('',0).replace(' ',0)
    CDSData = CDSData.replace({pd.NaT: 0, np.NaN: 0})
    CDSData['BenAdj23ToDate'] = CDSData['BenAdj23ToDate'] .astype(int)
    
    OAStagingData = Parser.parseCSV("C:\\Srikanth\\Srikanth\\Projects\\Claims\\14PointReport Data\\export_0527.csv")
    OAStagingData = OAStagingData.sort_values('ClaimNumber').reset_index(drop = True)
    OAStagingData = OAStagingData.fillna(0) 
    
    
    OAStagingData = OAStagingData.drop(['Row #'],axis=1)
    OAStagingData['BenefitBeginDate'] = pd.to_datetime(OAStagingData['BenefitBeginDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['MaxBenefitDate'] = pd.to_datetime(OAStagingData['MaxBenefitDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenefitTermDate'] = pd.to_datetime(OAStagingData['BenefitTermDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['MEDDate'] = pd.to_datetime(OAStagingData['MEDDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['ChangeOfDefinitionDate'] = pd.to_datetime(OAStagingData['ChangeOfDefinitionDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['LastPaymentFromDate'] = pd.to_datetime(OAStagingData['LastPaymentFromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    
    OAStagingData['LastPaymentToDate'] = pd.to_datetime(OAStagingData['LastPaymentToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj1FromDate'] = pd.to_datetime(OAStagingData['BenAdj1FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj1ToDate'] = pd.to_datetime(OAStagingData['BenAdj1ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj2FromDate'] = pd.to_datetime(OAStagingData['BenAdj2FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj2ToDate'] = pd.to_datetime(OAStagingData['BenAdj2ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj3FromDate'] = pd.to_datetime(OAStagingData['BenAdj3FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj3ToDate'] = pd.to_datetime(OAStagingData['BenAdj3ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj4FromDate'] = pd.to_datetime(OAStagingData['BenAdj4FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj4ToDate'] = pd.to_datetime(OAStagingData['BenAdj4ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj5FromDate'] = pd.to_datetime(OAStagingData['BenAdj5FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj5ToDate'] = pd.to_datetime(OAStagingData['BenAdj5ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj6FromDate'] = pd.to_datetime(OAStagingData['BenAdj6FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj6ToDate'] = pd.to_datetime(OAStagingData['BenAdj6ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    
    OAStagingData['GrossBenefitAmount'] = OAStagingData['GrossBenefitAmount'].astype(float)
    OAStagingData['TotalPaymentGrossAmts'] = OAStagingData['TotalPaymentGrossAmts'].astype(float)
    OAStagingData['BenefitTaxablePct'] = OAStagingData['BenefitTaxablePct'].astype(float)
    OAStagingData['NetBenefitAmount'] = OAStagingData['NetBenefitAmount'].astype(float)
    OAStagingData['OverPymtOrigAmt'] = OAStagingData['OverPymtOrigAmt'].astype(float)
    OAStagingData['OverPymtPaidAmt'] = OAStagingData['OverPymtPaidAmt'].astype(float)
    OAStagingData['OverPymtBalAmt'] = OAStagingData['OverPymtBalAmt'].astype(float)
    OAStagingData['BenAdj1Amt'] = OAStagingData['BenAdj1Amt'].astype(float)
    OAStagingData['BenAdj2Amt'] = OAStagingData['BenAdj2Amt'].astype(float)
    OAStagingData['BenAdj3Amt'] = OAStagingData['BenAdj3Amt'].astype(float)
    OAStagingData['BenAdj4Amt'] = OAStagingData['BenAdj4Amt'].astype(float)
    OAStagingData['BenAdj5Amt'] = OAStagingData['BenAdj5Amt'].astype(float)
    OAStagingData['BenAdj6Amt'] = OAStagingData['BenAdj6Amt'].astype(float)
    OAStagingData['BenAdj7Amt'] = OAStagingData['BenAdj7Amt'].astype(float)
    OAStagingData['BenAdj8Amt'] = OAStagingData['BenAdj8Amt'].astype(float)
    OAStagingData['BenAdj9Amt'] = OAStagingData['BenAdj9Amt'].astype(float)
    OAStagingData['BenAdj10Amt'] = OAStagingData['BenAdj10Amt'].astype(float)
    OAStagingData['BenAdj11Amt'] = OAStagingData['BenAdj11Amt'].astype(float)
    OAStagingData['BenAdj12Amt'] = OAStagingData['BenAdj12Amt'].astype(float)
    OAStagingData['BenAdj13Amt'] = OAStagingData['BenAdj13Amt'].astype(float)
    OAStagingData['BenAdj14Amt'] = OAStagingData['BenAdj14Amt'].astype(float)
    OAStagingData['BenAdj15Amt'] = OAStagingData['BenAdj15Amt'].astype(float)
    OAStagingData['BenAdj16Amt'] = OAStagingData['BenAdj16Amt'].astype(float)
    OAStagingData['BenAdj17Amt'] = OAStagingData['BenAdj17Amt'].astype(float)
    OAStagingData['BenAdj18Amt'] = OAStagingData['BenAdj18Amt'].astype(float)
    OAStagingData['BenAdj19Amt'] = OAStagingData['BenAdj19Amt'].astype(float)
    OAStagingData['BenAdj20Amt'] = OAStagingData['BenAdj20Amt'].astype(float)
    OAStagingData['BenAdj21Amt'] = OAStagingData['BenAdj21Amt'].astype(float)
    OAStagingData['BenAdj22Amt'] = OAStagingData['BenAdj22Amt'].astype(float)
    OAStagingData['BenAdj23Amt'] = OAStagingData['BenAdj23Amt'].astype(float)
    
    CDSData['GrossBenefitAmount'] = CDSData['GrossBenefitAmount'].astype(float)
    CDSData['TotalPaymentGrossAmts'] = CDSData['TotalPaymentGrossAmts'].astype(float)
    CDSData['BenefitTaxablePct'] = CDSData['BenefitTaxablePct'].astype(float)
    CDSData['NetBenefitAmount'] = CDSData['NetBenefitAmount'].astype(float)
    CDSData['OverPymtOrigAmt'] = CDSData['OverPymtOrigAmt'].astype(float)
    CDSData['OverPymtPaidAmt'] = CDSData['OverPymtPaidAmt'].astype(float)
    CDSData['OverPymtBalAmt'] = CDSData['OverPymtBalAmt'].astype(float)
    CDSData['BenAdj1Amt'] = CDSData['BenAdj1Amt'].astype(float)
    CDSData['BenAdj2Amt'] = CDSData['BenAdj2Amt'].astype(float)
    CDSData['BenAdj3Amt'] = CDSData['BenAdj3Amt'].astype(float)
    CDSData['BenAdj4Amt'] = CDSData['BenAdj4Amt'].astype(float)
    CDSData['BenAdj5Amt'] = CDSData['BenAdj5Amt'].astype(float)
    CDSData['BenAdj6Amt'] = CDSData['BenAdj6Amt'].astype(float)
    CDSData['BenAdj7Amt'] = CDSData['BenAdj7Amt'].astype(float)
    CDSData['BenAdj8Amt'] = CDSData['BenAdj8Amt'].astype(float)
    CDSData['BenAdj9Amt'] = CDSData['BenAdj9Amt'].astype(float)
    CDSData['BenAdj10Amt'] = CDSData['BenAdj10Amt'].astype(float)
    CDSData['BenAdj11Amt'] = CDSData['BenAdj11Amt'].astype(float)
    CDSData['BenAdj12Amt'] = CDSData['BenAdj12Amt'].astype(float)
    CDSData['BenAdj13Amt'] = CDSData['BenAdj13Amt'].astype(float)
    CDSData['BenAdj14Amt'] = CDSData['BenAdj14Amt'].astype(float)
    CDSData['BenAdj15Amt'] = CDSData['BenAdj15Amt'].astype(float)
    CDSData['BenAdj16Amt'] = CDSData['BenAdj16Amt'].astype(float)
    CDSData['BenAdj17Amt'] = CDSData['BenAdj17Amt'].astype(float)
    CDSData['BenAdj18Amt'] = CDSData['BenAdj18Amt'].astype(float)
    CDSData['BenAdj19Amt'] = CDSData['BenAdj19Amt'].astype(float)
    CDSData['BenAdj20Amt'] = CDSData['BenAdj20Amt'].astype(float)
    CDSData['BenAdj21Amt'] = CDSData['BenAdj21Amt'].astype(float)
    CDSData['BenAdj22Amt'] = CDSData['BenAdj22Amt'].astype(float)
    CDSData['BenAdj23Amt'] = CDSData['BenAdj23Amt'].astype(float)
    
    OAStagingData = OAStagingData.replace('1970-01-01 00:00:00','0')
    OAStagingData = OAStagingData.astype(str)
    CDSData = CDSData.astype(str)
    OAStagingData = OAStagingData.sort_values('ClaimNumber').reset_index(drop = True)
    CDSData = CDSData.sort_values('ClaimNumber').reset_index(drop = True)
    
   
    """
    dictionary = {"CDSData":CDSData,"OAStagingData":OAStagingData}
    df=pd.concat(dictionary)
    ##df.drop_duplicates(keep=False)
    
    
    txt = df.drop_duplicates(keep=False).to_string()
    if 'Empty DataFrame' in txt:
        html_logger.dbg("DB matches as expected. No differences found")
    else:  
        arrTxt = txt.split('OAStagingData')
        html_logger.dbg(arrTxt[0].replace('CDSData',''))
        html_logger.dbg('-----------------------------')
        html_logger.dbg(arrTxt[1])
        df.to_csv("C:/Srikanth/text1.csv")
    """
    #df = pd.concat([CDSData,OAStagingData]) 
    CDSData['ClaimNumber'] = CDSData['ClaimNumber'].astype(float)
    CDSData.sort_values('ClaimNumber').reset_index(drop = True)
    #CDSData.set_index(['ClaimNumber'], inplace=True)
    OAStagingData['ClaimNumber'] = OAStagingData['ClaimNumber'].astype(float)
    OAStagingData.sort_values('ClaimNumber').reset_index(drop = True)
    
    
    df_all = pd.concat([CDSData.set_index('ClaimNumber'), OAStagingData.set_index('ClaimNumber')],axis='columns',keys=['CDSData', 'OAStagingData'], join='outer')
    df_all = df_all.swaplevel(axis='columns')[CDSData.columns[1:]]
    changes = df_all.groupby(level=0, axis=1).apply(lambda frame: frame.apply(report_diff, axis=1))
    changes = changes[['ClaimStatus','BenefitBeginDate','MaxBenefitDate','GrossBenefitAmount','NetBenefitAmount','BenefitTermDate','MEDDate','BenefitTaxablePct','ChangeOfDefinitionDate','LastPaymentFromDate','LastPaymentToDate','TotalPaymentGrossAmts','PaymentPreference','OverPymtOrigAmt','OverPymtPaidAmt','OverPymtBalAmt','BenAdj1Desc','BenAdj1Amt','BenAdj1FromDate','BenAdj1ToDate','BenAdj2Desc','BenAdj2Amt','BenAdj2FromDate','BenAdj2ToDate','BenAdj3Desc','BenAdj3Amt','BenAdj3FromDate','BenAdj3ToDate','BenAdj4Desc','BenAdj4Amt','BenAdj4FromDate','BenAdj4ToDate','BenAdj5Desc','BenAdj5Amt','BenAdj5FromDate','BenAdj5ToDate','BenAdj6Desc','BenAdj6Amt','BenAdj6FromDate','BenAdj6ToDate','BenAdj7Desc','BenAdj7Amt','BenAdj7FromDate','BenAdj7ToDate','BenAdj8Desc','BenAdj8Amt','BenAdj8FromDate','BenAdj8ToDate','BenAdj9Desc','BenAdj9Amt','BenAdj9FromDate','BenAdj9ToDate','BenAdj10Desc','BenAdj10Amt','BenAdj10FromDate','BenAdj10ToDate','BenAdj11Desc','BenAdj11Amt','BenAdj11FromDate','BenAdj11ToDate','BenAdj12Desc','BenAdj12Amt','BenAdj12FromDate','BenAdj12ToDate','BenAdj13Desc','BenAdj13Amt','BenAdj13FromDate','BenAdj13ToDate','BenAdj14Desc','BenAdj14Amt','BenAdj14FromDate','BenAdj14ToDate','BenAdj15Desc','BenAdj15Amt','BenAdj15FromDate','BenAdj15ToDate','BenAdj16Desc','BenAdj16Amt','BenAdj16FromDate','BenAdj16ToDate','BenAdj17Desc','BenAdj17Amt','BenAdj17FromDate','BenAdj17ToDate','BenAdj18Desc','BenAdj18Amt','BenAdj18FromDate','BenAdj18ToDate','BenAdj19Desc','BenAdj19Amt','BenAdj19FromDate','BenAdj19ToDate','BenAdj20Desc','BenAdj20Amt','BenAdj20FromDate','BenAdj20ToDate','BenAdj21Desc','BenAdj21Amt','BenAdj21FromDate','BenAdj21ToDate','BenAdj22Desc','BenAdj22Amt','BenAdj22FromDate','BenAdj22ToDate','BenAdj23Desc','BenAdj23Amt','BenAdj23FromDate','BenAdj23ToDate']]
    print(changes)
    changes.to_csv(parentFolder + "ReportValidation_"+ timenow + ".csv")
    #print(list(CDSData.dtypes))
    #print(list(OAStagingData.dtypes))
    #df = diff_pd(CDSData, OAStagingData)
    #df.to_csv("C:/Srikanth/text1.csv")
    """    
    for x in range(0,tabVal.shape[0]):
        polNUm = tabVal['PolicyholderNumber'].iloc[x]
        fltTab = tabVal[tabVal['PolicyholderNumber'] == polNUm]
        
        #tabVal = tabVal.groupby(['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate']).first()
        fltTab = fltTab.drop_duplicates(subset = ['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate'],keep = 'last').reset_index(drop = True)
        fltTab = fltTab[fltTab['DocumentInfo.systemEnd'].notnull()]
        fltTab['BillingPlanIndicator'] = fltTab["BillingPlanIndicator"].replace('','N').fillna('N')
        #tabVal['RenewedCommissionsPremiumIndicator'] = tabVal["RenewedCommissionsPremiumIndicator"].replace('','N').fillna('N')
        fltTab['RateNumber'] = fltTab["RateNumber"].replace('','0').fillna('NULL')
        #tabVal['DocumentInfo.systemStart'] = datetime.strptime(tabVal['DocumentInfo.systemStart'],'%Y-%m-%dT%H:%M:%S.%f')
        #tabVal['DocumentInfo.systemStart']=  tabVal['DocumentInfo.systemStart'].dt.strftime('%Y-%m-%dT%H:%M:%S.%f')
        fltTab['DocumentInfo.systemStart'] = pd.to_datetime(fltTab['DocumentInfo.systemStart'])
        fltTab['DocumentInfo.systemStart'] = fltTab['DocumentInfo.systemStart'].astype(str).map(lambda v: v.split('.')[0])
        #tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
        #print(fltTab.head(5))
    
        fltdf1 = df1[df1['PLCYHLDR_NBR'] == str(polNUm)]
        #df1['ODH_EFF_DT'] = datetime.strptime(df1['ODH_EFF_DT'],'%Y-%m-%d %H:%M:%S')
        #df1['ODH_EFF_DT'] =  df1['ODH_EFF_DT'].dt.strftime('%Y-%m-%d %H:%M:%S')
        
        #df1['ODH_EFF_DT'] = df1['ODH_EFF_DT'].astype(str).map(lambda v: v.split(' ')[0])
        #print(df1.head(5))
        
        cols = [['PolicyholderNumber','PLCYHLDR_NBR','float'],['PlanNumber','PLAN_NBR','float'],['PlanType','PLAN_TYP','String'],
                ['PolicyholderPlanEffectiveDate','PLCYHLDR_PLAN_EFF_DT','String'],['PolicyholderPlanTerminationDate','PLCYHLDR_PLAN_TERM_DT','String'],
                ['CommissionPayIndicator','COMM_PAY_IND','String'],['BillingPlanIndicator','BILL_PLAN_IND','String'],['EverEffectiveIndicator','EVER_EFF_IND','String'],
                ['RateNumber','RT_NBR','String'],['PlanOriginalEffectiveDate','PLAN_ORIG_EFF_DT','String']
                ,['RenewedCommissionsPremiumIndicator','RENEWED_COMMISSIONS_PREM_IND','String'],['RatingProcessFileIndicator','RT_PROC_FILE_IND','String']
                ,['DocumentInfo.systemStart','ODH_EFF_DT','String']]
    
        
        if(tabVal.shape[0] == df1.shape[0]):
            html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
        else:
            html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
        html_logger.info("Validation of Policy number " + str(polNUm))
        DFCompare.compare(fltTab,fltdf1,cols)
    """


def Validation14Point():
    print("Start of Validation")

    now = datetime.now()
    parentFolder = "C:\\Srikanth\\Srikanth\\Projects\\Claims\\14PointReport Data\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    #sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
    #driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
    timenow = str(now.isoformat().replace(":","_"))
    LOG_FILENAME = parentFolder + "ReportValidation_"+ timenow + ".html"
    setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
    
    OAStagingData = Parser.parseCSV("C:\\Srikanth\\Srikanth\\Projects\\Claims\\14PointReport Data\\export_0527_1.csv")
    OAStagingData = OAStagingData.sort_values('ClaimNumber').reset_index(drop = True)
    OAStagingData = OAStagingData.fillna(0) 
    
    #tabVal = Parser.parseTSV("K:\IT WORK\PROJ00014201 - Data Delivery Modernization\Testing\LRF_version2\PolicyholderPlan.tsv")
    CDSData1 = Parser.parseExcel("C:\\Srikanth\\Srikanth\\Projects\\Claims\\14PointReport Data\\CDS_Data_20210518.xlsx", "Sheet1")
    CDSData = CDSData1[CDSData1['ClaimNumber'].isin(list(OAStagingData['ClaimNumber'].astype(float)))]
    CDSData = CDSData.drop(['BenAdjCount'],axis=1)
    CDSData = CDSData.sort_values('ClaimNumber').reset_index(drop = True)
    CDSData = CDSData.drop(columns =['BenAdj1Code','BenAdj2Code','BenAdj3Code','BenAdj4Code','BenAdj5Code','BenAdj6Code','BenAdj7Code','BenAdj8Code','BenAdj9Code','BenAdj10Code','BenAdj11Code','BenAdj12Code','BenAdj13Code','BenAdj14Code','BenAdj15Code','BenAdj16Code','BenAdj17Code','BenAdj18Code','BenAdj19Code','BenAdj20Code','BenAdj21Code','BenAdj22Code','BenAdj23Code','OpenDiaries','ClaimNbr','Diary1Code','Diary1Desc','Diary1AssignedTo','Diary1TargetDate','Diary2Code','Diary2Desc','Diary2AssignedTo','Diary2TargetDate','Diary3Code','Diary3Desc','Diary3AssignedTo','Diary3TargetDate','Diary4Code','Diary4Desc','Diary4AssignedTo','Diary4TargetDate','Diary5Code','Diary5Desc','Diary5AssignedTo','Diary5TargetDate','Diary6Code','Diary6Desc','Diary6AssignedTo','Diary6TargetDate','Diary7Code','Diary7Desc','Diary7AssignedTo','Diary7TargetDate','Diary8Code','Diary8Desc','Diary8AssignedTo','Diary8TargetDate','Diary9Code','Diary9Desc','Diary9AssignedTo','Diary9TargetDate','Diary10Code','Diary10Desc','Diary10AssignedTo','Diary10TargetDate','Diary11Code','Diary11Desc','Diary11AssignedTo','Diary11TargetDate','Diary12Code','Diary12Desc','Diary12AssignedTo','Diary12TargetDate','Diary13Code','Diary13Desc','Diary13AssignedTo','Diary13TargetDate'],axis=1)
    CDSData = CDSData.replace('NULL',0).replace('nan',0).replace('NaT',0).replace('',0).replace(' ',0)
    CDSData = CDSData.replace({pd.NaT: 0, np.NaN: 0})
    CDSData['BenAdj23ToDate'] = CDSData['BenAdj23ToDate'] .astype(int)
    
    
    
    
    OAStagingData = OAStagingData.drop(['Row #'],axis=1)
    OAStagingData['BenefitBeginDate'] = pd.to_datetime(OAStagingData['BenefitBeginDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d')
    OAStagingData['MaxBenefitDate'] = pd.to_datetime(OAStagingData['MaxBenefitDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d')
    OAStagingData['BenefitTermDate'] = pd.to_datetime(OAStagingData['BenefitTermDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d')
    OAStagingData['MEDDate'] = pd.to_datetime(OAStagingData['MEDDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['ChangeOfDefinitionDate'] = pd.to_datetime(OAStagingData['ChangeOfDefinitionDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['LastPaymentFromDate'] = pd.to_datetime(OAStagingData['LastPaymentFromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    
    OAStagingData['LastPaymentToDate'] = pd.to_datetime(OAStagingData['LastPaymentToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj1FromDate'] = pd.to_datetime(OAStagingData['BenAdj1FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj1ToDate'] = pd.to_datetime(OAStagingData['BenAdj1ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj2FromDate'] = pd.to_datetime(OAStagingData['BenAdj2FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj2ToDate'] = pd.to_datetime(OAStagingData['BenAdj2ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj3FromDate'] = pd.to_datetime(OAStagingData['BenAdj3FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj3ToDate'] = pd.to_datetime(OAStagingData['BenAdj3ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj4FromDate'] = pd.to_datetime(OAStagingData['BenAdj4FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj4ToDate'] = pd.to_datetime(OAStagingData['BenAdj4ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj5FromDate'] = pd.to_datetime(OAStagingData['BenAdj5FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj5ToDate'] = pd.to_datetime(OAStagingData['BenAdj5ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj6FromDate'] = pd.to_datetime(OAStagingData['BenAdj6FromDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    OAStagingData['BenAdj6ToDate'] = pd.to_datetime(OAStagingData['BenAdj6ToDate'], format='%Y-%m-%d %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    
    OAStagingData['GrossBenefitAmount'] = OAStagingData['GrossBenefitAmount'].astype(float)
    OAStagingData['TotalPaymentGrossAmts'] = OAStagingData['TotalPaymentGrossAmts'].astype(float)
    OAStagingData['BenefitTaxablePct'] = OAStagingData['BenefitTaxablePct'].astype(float)
    OAStagingData['NetBenefitAmount'] = OAStagingData['NetBenefitAmount'].astype(float)
    OAStagingData['OverPymtOrigAmt'] = OAStagingData['OverPymtOrigAmt'].astype(float)
    OAStagingData['OverPymtPaidAmt'] = OAStagingData['OverPymtPaidAmt'].astype(float)
    OAStagingData['OverPymtBalAmt'] = OAStagingData['OverPymtBalAmt'].astype(float)
    OAStagingData['BenAdj1Amt'] = OAStagingData['BenAdj1Amt'].astype(float)
    OAStagingData['BenAdj2Amt'] = OAStagingData['BenAdj2Amt'].astype(float)
    OAStagingData['BenAdj3Amt'] = OAStagingData['BenAdj3Amt'].astype(float)
    OAStagingData['BenAdj4Amt'] = OAStagingData['BenAdj4Amt'].astype(float)
    OAStagingData['BenAdj5Amt'] = OAStagingData['BenAdj5Amt'].astype(float)
    OAStagingData['BenAdj6Amt'] = OAStagingData['BenAdj6Amt'].astype(float)
    OAStagingData['BenAdj7Amt'] = OAStagingData['BenAdj7Amt'].astype(float)
    OAStagingData['BenAdj8Amt'] = OAStagingData['BenAdj8Amt'].astype(float)
    OAStagingData['BenAdj9Amt'] = OAStagingData['BenAdj9Amt'].astype(float)
    OAStagingData['BenAdj10Amt'] = OAStagingData['BenAdj10Amt'].astype(float)
    OAStagingData['BenAdj11Amt'] = OAStagingData['BenAdj11Amt'].astype(float)
    OAStagingData['BenAdj12Amt'] = OAStagingData['BenAdj12Amt'].astype(float)
    OAStagingData['BenAdj13Amt'] = OAStagingData['BenAdj13Amt'].astype(float)
    OAStagingData['BenAdj14Amt'] = OAStagingData['BenAdj14Amt'].astype(float)
    OAStagingData['BenAdj15Amt'] = OAStagingData['BenAdj15Amt'].astype(float)
    OAStagingData['BenAdj16Amt'] = OAStagingData['BenAdj16Amt'].astype(float)
    OAStagingData['BenAdj17Amt'] = OAStagingData['BenAdj17Amt'].astype(float)
    OAStagingData['BenAdj18Amt'] = OAStagingData['BenAdj18Amt'].astype(float)
    OAStagingData['BenAdj19Amt'] = OAStagingData['BenAdj19Amt'].astype(float)
    OAStagingData['BenAdj20Amt'] = OAStagingData['BenAdj20Amt'].astype(float)
    OAStagingData['BenAdj21Amt'] = OAStagingData['BenAdj21Amt'].astype(float)
    OAStagingData['BenAdj22Amt'] = OAStagingData['BenAdj22Amt'].astype(float)
    OAStagingData['BenAdj23Amt'] = OAStagingData['BenAdj23Amt'].astype(float)
    
    CDSData['GrossBenefitAmount'] = CDSData['GrossBenefitAmount'].astype(float)
    CDSData['TotalPaymentGrossAmts'] = CDSData['TotalPaymentGrossAmts'].astype(float)
    CDSData['BenefitTaxablePct'] = CDSData['BenefitTaxablePct'].astype(float)
    CDSData['NetBenefitAmount'] = CDSData['NetBenefitAmount'].astype(float)
    CDSData['OverPymtOrigAmt'] = CDSData['OverPymtOrigAmt'].astype(float)
    CDSData['OverPymtPaidAmt'] = CDSData['OverPymtPaidAmt'].astype(float)
    CDSData['OverPymtBalAmt'] = CDSData['OverPymtBalAmt'].astype(float)
    CDSData['BenAdj1Amt'] = CDSData['BenAdj1Amt'].astype(float)
    CDSData['BenAdj2Amt'] = CDSData['BenAdj2Amt'].astype(float)
    CDSData['BenAdj3Amt'] = CDSData['BenAdj3Amt'].astype(float)
    CDSData['BenAdj4Amt'] = CDSData['BenAdj4Amt'].astype(float)
    CDSData['BenAdj5Amt'] = CDSData['BenAdj5Amt'].astype(float)
    CDSData['BenAdj6Amt'] = CDSData['BenAdj6Amt'].astype(float)
    CDSData['BenAdj7Amt'] = CDSData['BenAdj7Amt'].astype(float)
    CDSData['BenAdj8Amt'] = CDSData['BenAdj8Amt'].astype(float)
    CDSData['BenAdj9Amt'] = CDSData['BenAdj9Amt'].astype(float)
    CDSData['BenAdj10Amt'] = CDSData['BenAdj10Amt'].astype(float)
    CDSData['BenAdj11Amt'] = CDSData['BenAdj11Amt'].astype(float)
    CDSData['BenAdj12Amt'] = CDSData['BenAdj12Amt'].astype(float)
    CDSData['BenAdj13Amt'] = CDSData['BenAdj13Amt'].astype(float)
    CDSData['BenAdj14Amt'] = CDSData['BenAdj14Amt'].astype(float)
    CDSData['BenAdj15Amt'] = CDSData['BenAdj15Amt'].astype(float)
    CDSData['BenAdj16Amt'] = CDSData['BenAdj16Amt'].astype(float)
    CDSData['BenAdj17Amt'] = CDSData['BenAdj17Amt'].astype(float)
    CDSData['BenAdj18Amt'] = CDSData['BenAdj18Amt'].astype(float)
    CDSData['BenAdj19Amt'] = CDSData['BenAdj19Amt'].astype(float)
    CDSData['BenAdj20Amt'] = CDSData['BenAdj20Amt'].astype(float)
    CDSData['BenAdj21Amt'] = CDSData['BenAdj21Amt'].astype(float)
    CDSData['BenAdj22Amt'] = CDSData['BenAdj22Amt'].astype(float)
    CDSData['BenAdj23Amt'] = CDSData['BenAdj23Amt'].astype(float)
    
    OAStagingData = OAStagingData.replace('1970-01-01 00:00:00','0')
    OAStagingData = OAStagingData.astype(str)
    CDSData = CDSData.astype(str)
    OAStagingData = OAStagingData.sort_values('ClaimNumber').reset_index(drop = True)
    CDSData = CDSData.sort_values('ClaimNumber').reset_index(drop = True)
    
   
    """
    dictionary = {"CDSData":CDSData,"OAStagingData":OAStagingData}
    df=pd.concat(dictionary)
    ##df.drop_duplicates(keep=False)
    
    
    txt = df.drop_duplicates(keep=False).to_string()
    if 'Empty DataFrame' in txt:
        html_logger.dbg("DB matches as expected. No differences found")
    else:  
        arrTxt = txt.split('OAStagingData')
        html_logger.dbg(arrTxt[0].replace('CDSData',''))
        html_logger.dbg('-----------------------------')
        html_logger.dbg(arrTxt[1])
        df.to_csv("C:/Srikanth/text1.csv")
    """
    #df = pd.concat([CDSData,OAStagingData]) 
    CDSData['ClaimNumber'] = CDSData['ClaimNumber'].astype(float)
    CDSData.sort_values('ClaimNumber').reset_index(drop = True)
    #CDSData.set_index(['ClaimNumber'], inplace=True)
    OAStagingData['ClaimNumber'] = OAStagingData['ClaimNumber'].astype(float)
    OAStagingData.sort_values('ClaimNumber').reset_index(drop = True)
    
    
    df_all = pd.concat([CDSData.set_index('ClaimNumber'), OAStagingData.set_index('ClaimNumber')],axis='columns',keys=['CDSData', 'OAStagingData'], join='outer')
    df_all = df_all.swaplevel(axis='columns')[CDSData.columns[1:]]
    changes = df_all.groupby(level=0, axis=1).apply(lambda frame: frame.apply(report_diff, axis=1))
    changes = changes[['ClaimStatus','BenefitBeginDate','MaxBenefitDate','GrossBenefitAmount','NetBenefitAmount','BenefitTermDate','MEDDate','BenefitTaxablePct','ChangeOfDefinitionDate','LastPaymentFromDate','LastPaymentToDate','TotalPaymentGrossAmts','PaymentPreference','OverPymtOrigAmt','OverPymtPaidAmt','OverPymtBalAmt','BenAdj1Desc','BenAdj1Amt','BenAdj1FromDate','BenAdj1ToDate','BenAdj2Desc','BenAdj2Amt','BenAdj2FromDate','BenAdj2ToDate','BenAdj3Desc','BenAdj3Amt','BenAdj3FromDate','BenAdj3ToDate','BenAdj4Desc','BenAdj4Amt','BenAdj4FromDate','BenAdj4ToDate','BenAdj5Desc','BenAdj5Amt','BenAdj5FromDate','BenAdj5ToDate','BenAdj6Desc','BenAdj6Amt','BenAdj6FromDate','BenAdj6ToDate','BenAdj7Desc','BenAdj7Amt','BenAdj7FromDate','BenAdj7ToDate','BenAdj8Desc','BenAdj8Amt','BenAdj8FromDate','BenAdj8ToDate','BenAdj9Desc','BenAdj9Amt','BenAdj9FromDate','BenAdj9ToDate','BenAdj10Desc','BenAdj10Amt','BenAdj10FromDate','BenAdj10ToDate','BenAdj11Desc','BenAdj11Amt','BenAdj11FromDate','BenAdj11ToDate','BenAdj12Desc','BenAdj12Amt','BenAdj12FromDate','BenAdj12ToDate','BenAdj13Desc','BenAdj13Amt','BenAdj13FromDate','BenAdj13ToDate','BenAdj14Desc','BenAdj14Amt','BenAdj14FromDate','BenAdj14ToDate','BenAdj15Desc','BenAdj15Amt','BenAdj15FromDate','BenAdj15ToDate','BenAdj16Desc','BenAdj16Amt','BenAdj16FromDate','BenAdj16ToDate','BenAdj17Desc','BenAdj17Amt','BenAdj17FromDate','BenAdj17ToDate','BenAdj18Desc','BenAdj18Amt','BenAdj18FromDate','BenAdj18ToDate','BenAdj19Desc','BenAdj19Amt','BenAdj19FromDate','BenAdj19ToDate','BenAdj20Desc','BenAdj20Amt','BenAdj20FromDate','BenAdj20ToDate','BenAdj21Desc','BenAdj21Amt','BenAdj21FromDate','BenAdj21ToDate','BenAdj22Desc','BenAdj22Amt','BenAdj22FromDate','BenAdj22ToDate','BenAdj23Desc','BenAdj23Amt','BenAdj23FromDate','BenAdj23ToDate']]
    print(changes)
    #changes.to_csv(parentFolder + "ReportValidation_"+ timenow + ".csv")
    
    
    rowcnt = changes.shape[0]
    
    writer = pd.ExcelWriter(parentFolder + "ReportValidation_"+ timenow + ".xlsx", engine='xlsxwriter')
    changes.to_excel(writer,sheet_name = 'Result')
    
    workbook = writer.book
    worksheet = writer.sheets['Result']
    
    # Add a format. Light red fill with dark red text.
    format1 = workbook.add_format({'bg_color': '#FFC7CE','font_color': '#9C0006'})
    
    # Add a format. Green fill with dark green text.
    format2 = workbook.add_format({'bg_color': '#C6EFCE','font_color': '#006100'})
    
    worksheet.conditional_format('A1:DE'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    '|',
                                       'format':   format1})
    worksheet.conditional_format('A1:DE'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    '|',
                                       'format':   format2})
    
    for i in range(2,rowcnt+2):
        worksheet.write_formula('DF'+str(i), '=COUNTIF(A'+str(i)+':DE'+str(i)+',"*|*")')
        worksheet.write_formula('DG'+str(i), '=IF(DF'+str(i) +'=0,"Pass","Fail")')
     
    worksheet.conditional_format('DF2:DF'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'equal to',
                                    'value':    0,
                                    'format':   format2})
    worksheet.conditional_format('DF2:DF'+str(rowcnt+1), {'type':     'cell',
                                    'criteria': 'greater than',
                                    'value':    0,
                                    'format':   format1})
    
    worksheet.conditional_format('DG2:DG'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'containing',
                                       'value':    'Fail',
                                       'format':   format1})
    worksheet.conditional_format('DG2:DG'+str(rowcnt+1), {'type':     'text',
                                       'criteria': 'not containing',
                                       'value':    'Pass',
                                       'format':   format2})
       
    writer.save()
    print("################################################################")
    print("Remeber the output will be in CDS Data | OA Staging Data Format if there are differences")
    print("################################################################")
    print("End of Validation")
    #print(list(CDSData.dtypes))
    #print(list(OAStagingData.dtypes))
    #df = diff_pd(CDSData, OAStagingData)
    #df.to_csv("C:/Srikanth/text1.csv")
    """    
    for x in range(0,tabVal.shape[0]):
        polNUm = tabVal['PolicyholderNumber'].iloc[x]
        fltTab = tabVal[tabVal['PolicyholderNumber'] == polNUm]
        
        #tabVal = tabVal.groupby(['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate']).first()
        fltTab = fltTab.drop_duplicates(subset = ['PolicyholderNumber', 'PlanNumber','PlanType','PolicyholderPlanEffectiveDate'],keep = 'last').reset_index(drop = True)
        fltTab = fltTab[fltTab['DocumentInfo.systemEnd'].notnull()]
        fltTab['BillingPlanIndicator'] = fltTab["BillingPlanIndicator"].replace('','N').fillna('N')
        #tabVal['RenewedCommissionsPremiumIndicator'] = tabVal["RenewedCommissionsPremiumIndicator"].replace('','N').fillna('N')
        fltTab['RateNumber'] = fltTab["RateNumber"].replace('','0').fillna('NULL')
        #tabVal['DocumentInfo.systemStart'] = datetime.strptime(tabVal['DocumentInfo.systemStart'],'%Y-%m-%dT%H:%M:%S.%f')
        #tabVal['DocumentInfo.systemStart']=  tabVal['DocumentInfo.systemStart'].dt.strftime('%Y-%m-%dT%H:%M:%S.%f')
        fltTab['DocumentInfo.systemStart'] = pd.to_datetime(fltTab['DocumentInfo.systemStart'])
        fltTab['DocumentInfo.systemStart'] = fltTab['DocumentInfo.systemStart'].astype(str).map(lambda v: v.split('.')[0])
        #tabVal = tabVal.sort_values('PolicyholderNumber').reset_index(drop = True)
        #print(fltTab.head(5))
    
        fltdf1 = df1[df1['PLCYHLDR_NBR'] == str(polNUm)]
        #df1['ODH_EFF_DT'] = datetime.strptime(df1['ODH_EFF_DT'],'%Y-%m-%d %H:%M:%S')
        #df1['ODH_EFF_DT'] =  df1['ODH_EFF_DT'].dt.strftime('%Y-%m-%d %H:%M:%S')
        
        #df1['ODH_EFF_DT'] = df1['ODH_EFF_DT'].astype(str).map(lambda v: v.split(' ')[0])
        #print(df1.head(5))
        
        cols = [['PolicyholderNumber','PLCYHLDR_NBR','float'],['PlanNumber','PLAN_NBR','float'],['PlanType','PLAN_TYP','String'],
                ['PolicyholderPlanEffectiveDate','PLCYHLDR_PLAN_EFF_DT','String'],['PolicyholderPlanTerminationDate','PLCYHLDR_PLAN_TERM_DT','String'],
                ['CommissionPayIndicator','COMM_PAY_IND','String'],['BillingPlanIndicator','BILL_PLAN_IND','String'],['EverEffectiveIndicator','EVER_EFF_IND','String'],
                ['RateNumber','RT_NBR','String'],['PlanOriginalEffectiveDate','PLAN_ORIG_EFF_DT','String']
                ,['RenewedCommissionsPremiumIndicator','RENEWED_COMMISSIONS_PREM_IND','String'],['RatingProcessFileIndicator','RT_PROC_FILE_IND','String']
                ,['DocumentInfo.systemStart','ODH_EFF_DT','String']]
    
        
        if(tabVal.shape[0] == df1.shape[0]):
            html_logger.dbg("Count of the Records matches as expected. The Number of rows " +  str(tabVal.shape[0]))
        else:
            html_logger.err("Count of the records doesn't Match, TSV file count is " + str(tabVal.shape[0]) + " but the DB count is " + str(df1.shape[0]))
        html_logger.info("Validation of Policy number " + str(polNUm))
        DFCompare.compare(fltTab,fltdf1,cols)
    """



Validation14Point()
#PolicyHolderPlan_EBDM()
#Policyholder_EBDM()
#InvHistory__EBDM()
#Validation14Point_Trial()
#InvHistory__EBDM()
#PolicyHolderPlan_EBDM()
#Policyholder_EBDM()
#PolicyHolderPlan()
#Policyholder()
#InvoiceHistory()
#Validation14Point_Trial()

"""
conn = pyodbc.connect('Driver={MarkLogic SQL};Server=6NLOJLMOE-lb.z.marklogicsvc.com;Database=data-hub-FINAL;uid=eu-qa-sn;pwd=hNFV0$zp5')  #;Trusted_Connection=yes;
cursor = conn.cursor()
    
sqldf = pd.read_sql_query(sql1,conn)
sqldf = sqldf.sort_values('PolicyholderNumber')
print (sqldf.head(5))
"""
