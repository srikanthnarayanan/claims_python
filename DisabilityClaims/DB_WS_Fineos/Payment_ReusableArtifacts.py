import html_logger
from html_logger import setup, info
import datetime
import logging
import sys
import time
import csv
import numpy as np

import pandas as pd
import xml.etree.ElementTree as ET
    

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait 

import html_logger
from html_logger import setup, info
#from test.support import testresult
    
#now = datetime.datetime.now()
#parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
#sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

#driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  

#LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\PaymentPEIValidation_"+ str(now.isoformat().replace(":","_")) + ".html"
#setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)

def getvalueofnode(node):
    """ return node text or None """
    return node.text if node is not None else 'None'

######################################################################################
# Class XMLParser
# contains methods to read XML data and Return a DataFrame
# Methods:
#    getXML(xmlfile path,cols)
#        Parses XMl file and Returns a DataList - as of now it's only one method
#        Eg:xmlpath = "C:\\Users\\CurrentPEI\\OneAmerica_Disbursement.xml"
#           dfcols = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name'] 
#           XMLParser.getXML(xmlpath,dfcols)
#
######################################################################################
class Parser:

    def parseXML(xmlfile,cols):
        parsed_xml = ET.parse(xmlfile)
        df_xml = pd.DataFrame(columns=cols)
    
        for node in parsed_xml.getroot():
            i = 0
            dfseries = ""
            dflists = list()
            for col in cols:
                i=i+1
                if(i==1):
                    dfseries = getvalueofnode(node.find(col))
                else:
                    dfseries = dfseries + "~~" + getvalueofnode(node.find(col))
            
            dflists = dfseries.split("~~")                           
            df_xml = df_xml.append(pd.Series(dflists,index=cols),ignore_index=True)
        
        return df_xml 
        
########################End of method parseXML########################################
######################################################################################

# contains methods to read SQL from DB and Return a DataFrame
# Methods:
#    getSQL(Sqlquery, DbServerName, DBName)
#        Parses XMl file and Returns a DataList - as of now it's only one method
#        Eg:sql1 = '''select A.PAYMENTMETHOD,A.GROSSPAYMENTA_MONAMT,A.payeefullname,A.PAYMENTADD1,A.PAYMENTADD2,A.PAYMENTADD3,A.PAYMENTPOSTCO,A.PAYMENTADD4,A.PAYMENTADD6,A.PAYEEBANKSORT,A.PAYEEACCOUNTN,A.PAYEEACCOUNTT,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID
#                from LZ.PEI_RQST_INFO A where PEI_I in (select Index_id from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))
#                and PEI_C in (select class_ID from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))'''
#           DbServerName = 'SQLD10746,4242'
#           DBName = 'ENTPRS_CLAIMS_ODS' 
#           SQLParser.getSQL(sql1, DbServerName, DBName)
#
######################################################################################
  
    def parseSQL(sql1,servername,databasename):
        
        import pyodbc 
        conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=' + servername +';'
                          'Database=' + databasename + ';'
                          'Trusted_Connection=yes;')
        
        sqldf = pd.read_sql_query(sql1,conn)
        return sqldf


    def parseMLSQL(sql1,servername,databasename, uid, pwd):
        
        import pyodbc 
        conn = pyodbc.connect('Driver={MarkLogic SQL};'
                          'Server=' + servername +';'
                          'Database=' + databasename + ';'
                          'uid=' + uid + ';' 
                          'pwd=' + pwd+';'
                          'Trusted_Connection=yes;')
        
        sqldf = pd.read_sql_query(sql1,conn)
        return sqldf
    
########################End of Method ParseSQL########################################
######################################################################################

# contains methods to read SQL from DB and Return a DataFrame
# Methods:
#    getSQL(Sqlquery, DbServerName, DBName)
#        Parses XMl file and Returns a DataList - as of now it's only one method
#        Eg:sql1 = '''select A.PAYMENTMETHOD,A.GROSSPAYMENTA_MONAMT,A.payeefullname,A.PAYMENTADD1,A.PAYMENTADD2,A.PAYMENTADD3,A.PAYMENTPOSTCO,A.PAYMENTADD4,A.PAYMENTADD6,A.PAYEEBANKSORT,A.PAYEEACCOUNTN,A.PAYEEACCOUNTT,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID
#                from LZ.PEI_RQST_INFO A where PEI_I in (select Index_id from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))
#                and PEI_C in (select class_ID from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))'''
#           DbServerName = 'SQLD10746,4242'
#           DBName = 'ENTPRS_CLAIMS_ODS' 
#           SQLParser.getSQL(sql1, DbServerName, DBName)
#
######################################################################################

    def parseCSV(csvfilename):
        dfPEICSV = pd.read_csv(csvfilename,encoding='utf-8', dtype = str)
        return dfPEICSV
    
    def parseTSV(tsvfilename):
       dfPEITSV = pd.read_csv(tsvfilename,sep='\t',dtype = str)  #encoding='utf-8'
       return dfPEITSV
   
    def parseExcel(excelPath, Sheetname):
        dfparseExcel = pd.read_excel(excelPath,sheet_name=Sheetname)
        return dfparseExcel
############################################
def diff_df(df1, df2, how="left"):
    """
      Find Difference of rows for given two dataframes
      this function is not symmetric, means
            diff(x, y) != diff(y, x)
      however
            diff(x, y, how='left') == diff(y, x, how='right')

      Ref: https://stackoverflow.com/questions/18180763/set-difference-for-pandas/40209800#40209800
    """
    '''
    if (df1.columns != df2.columns).any():
        raise ValueError("Two dataframe columns must match")
    
    if df1.equals(df2):
        return 'Success - two dataframes matches as expected'
    elif how == 'right':
        return pd.concat([df2, df1, df1]).drop_duplicates(keep=False)
    elif how == 'left':
        return pd.concat([df1, df2, df2]).drop_duplicates(keep=False)
    else:
        raise ValueError('how parameter supports only "left" or "right keywords"')

    '''
    
    
##############################################
class DFCompare:
    
    
    def diffDataFrame(df1,df2,colname1,colname2):
        result= 1
        if(len(colname1) == len(colname2)):
            for i,j in zip(colname1,colname2):
                ds1 = df1[i]
                ds2 = df2[j]
                if(set(ds1.replace('None',np.nan).replace('',np.nan).replace('false',False).replace('true',True).dropna().astype(str).str.replace('\\n',' ')) == set(ds2.replace('None',np.nan).replace('',np.nan).replace('false',False).replace('true',True).replace("\\n",' ').dropna().astype(str).str.replace('\\n',' '))):
                    #print(colname + " result is True")
                    html_logger.dbg("Pass - " + i + " result is True")
                    
                elif(ds1.dtype == 'datetime64[ns]' or ds2.dtype == 'datetime64[ns]'):
                    ds11 = ds1.astype('datetime64[ns]')
                    ds21 = ds2.astype('datetime64[ns]')
                    if(set(ds11.fillna('')) == set(ds21.fillna(''))):
                        html_logger.dbg("Pass - " + i + " result is True")
                    else:
                        html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds11).difference(set(ds21))) + "  and  " + str(set(ds21).difference(set(ds11))))
                        result = result * 0
                        
                elif(ds1.dtype == float or ds2.dtype == float):
                    ds11 = ds1.astype(float)
                    ds21 = ds2.astype(float)
                    if(set(ds11.fillna('')) == set(ds21.fillna(''))):
                        html_logger.dbg("Pass - " + i + " result is True")
                    else:
                        html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds11).difference(set(ds2))) + "  and  " + str(set(ds21).difference(set(ds1))))
                        result = result * 0   
                                  
                else:
                    ds11 = ds1.astype(str)
                    ds21 = ds2.astype(str)    
                    #print(colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))))
                    html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))       
        else:
            html_logger.err("Fail - Length of the columns doesn't match as expected")       
            result = result * 0
            
        if(result ==1):
            html_logger.info("Test Result is PASS")
        else:
            html_logger.info("Test Result is FAIL")
    
    
    def diffDataFrame_x(df1,df2,colname1,colname2):
        result= 1
        if(len(colname1) == len(colname2)):
            for i,j in zip(colname1,colname2):
                ds1 = df1[i]
                ds2 = df2[j]
                if(set(ds1.replace('None',np.nan).dropna()) == set(ds2.replace('',np.nan).dropna())):
                    html_logger.dbg("Pass - " + i + " result is True")
                else:#
                 if(ds1.dtype =='datetime64[ns]'):
                    try:
                        ds1dtype = ds1.dtype
                        ds2dtype = ds2.dtype
                        if(ds1dtype != 'bool' and ds2dtype != 'bool'):
                            ds1 = ds1.astype('datetime64[ns]')
                            ds2 = ds2.astype('datetime64[ns]')
                            if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                                html_logger.dbg("Pass - " + i + " result is True")
                            #elif(ds2.fillna('').isin(ds1.fillna('')).all()): 
                            #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2))))
                            #elif(ds1.fillna('').isin(ds2.fillna('')).all()):
                            #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds2).difference(set(ds1))))     
                            else:
                                html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                                result = result * 0   
                        else:
                            raise ValueError('oops!')          
                    except:                     
                        try:
                            if(ds1dtype != 'bool' and ds2dtype != 'bool'):
                                ds1 = ds1.astype(float)
                                ds2 = ds2.astype(float)
                                if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                                    html_logger.dbg("Pass - " + i + " result is True")
                                #elif(ds2.fillna('').isin(ds1.fillna('')).all()): 
                                #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2))))
                                #elif(ds1.fillna('').isin(ds2.fillna('')).all()):
                                #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds2).difference(set(ds1))))    
                                else:
                                    html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                                    result = result * 0 
                            else:
                                raise ValueError('oops!')              
                        except:
                            try:
                                if(set(ds1.astype(str).str.replace('\\n',' ')) == set(ds2.astype(str).str.replace('\\n',' '))):
                                    html_logger.dbg("Pass - " + i + " result is True")
                                elif(set(ds1.astype(str).str.upper()) == set(ds2.astype(str).str.upper())):
                                    html_logger.dbg("Pass - " + i + " result is True")    
                                #elif(ds2.astype(str).str.replace('None',np.nan).replace('\\n',' ').dropna().isin(ds1.astype(str).str.replace('None',np.nan).replace('\\n',' ').dropna()).all()):
                                #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2))))
                                #elif(ds2.astype(str).str.replace('None',np.nan).replace('\\n',' ').dropna().isin(ds1.astype(str).str.replace('None',np.nan).replace('\\n',' ').dropna()).all()):
                                #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2))))
                                #elif(ds2.astype(str).str.upper.replace('None',np.nan).replace('\\n',' ').dropna().isin(ds1.astype(str).str.upper.replace('None',np.nan).replace('\\n',' ').dropna()).all()):
                                #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present. Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2))))
                                #elif(ds2.astype(str).str.upper.replace('None',np.nan).replace('\\n',' ').dropna().isin(ds1.astype(str).str.upper.replace('None',np.nan).replace('\\n',' ').dropna()).all()):
                                #    html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present. Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2)))) 
                                                 
                                else:    
                                    html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                                    result = result * 0     
                            except:             
                                html_logger.err("Fail - " + i + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                                result = result * 0      
                #elif(ds2.replace('None',np.nan).dropna().isin(ds1.replace('None',np.nan).dropna()).all()):
                 #   html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds1).difference(set(ds2))))
                #elif(ds1.replace('None',np.nan).dropna().isin(ds2.replace('None',np.nan).dropna()).all()):
                 #   html_logger.dbg("Pass - " + i + " result is True - not exact match but subset is present.  Matching element is " + str(set(ds1).intersection(set(ds2)))  + " Missing elements are " + str(set(ds2).difference(set(ds1))))            
                   
        else:
            html_logger.err("Fail - Length of the columns doesn't match as expected")       
            result = result * 0
        if(result ==1):
            html_logger.info("Test Result is PASS")
        else:
            html_logger.info("Test Result is FAIL")        
        
    def diffDataFrameString(ds1,consValue,colname):
        if((ds1 == consValue).all()):    #str((ds1] == consValue).all())
            #print(colname + " result is True")
            html_logger.dbg("Pass - " + colname + " result is True")
        else:    
            #print(colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))))
            html_logger.err("Fail - " + colname + " result is False. The difference is " + ds1.to_string(index=False))

    
    def diffDataFrame_allCols(df1,df2):
        i = -1;
        for df1col in df1.columns:
            i = i + 1;
            DFCompare.compare_noResult(df1,df2,[[df1.columns[i],df2.columns[i],'string']])
        
    
    def compare(df1,df2,cols):
        res = 1
        for col in cols:
            ds1 = df1[col[0]]
            ds2 = df2[col[1]]
            type = col[2]
            if(type.lower() == 'string'):
                try:
                    ds1 = ds1.replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper().str.strip()  #replace('None',np.nan).replace('',np.nan).dropna().
                    ds2 = ds2.replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper().str.strip()  #replace('None',np.nan).replace('',np.nan).dropna()
                except:
                    ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().str.strip().replace('\\n',' ')
                    ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().str.strip().replace('\\n',' ')

                if(set(ds1.replace('',np.nan).dropna()) == set(ds2.replace('',np.nan).dropna())):
                    try:
                        html_logger.dbg("Pass - " + col[0] + "//" + col[1] + " result is True. The values are " + str(set(ds1.astype(str)))[0:1000])
                    except:
                        html_logger.dbg("Pass - " + col[0] + "//" + col[1] + " result is True. The values are " + str(str(str(set(ds1.astype(str)))[0:1000]).encode('utf8')))    
                else:
                    try:
                        html_logger.err("Fail - " + col[0] + "//" + col[1]+ " result is False. The difference is " + str(set(ds1).difference(set(ds2)))[0:1000] + "  and  \n" + str(set(ds2).difference(set(ds1)))[0:1000])   #[0:1000]
                    except:
                        html_logger.err("Fail - " + col[0] + "//" + col[1]+ " result is False. The difference cannot be displayed due to encodig issues in string. regret the inconvenience")    
                    res = 0

                        
            elif(type.lower() == 'float'):
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).replace('null',np.nan).dropna().astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).replace('null',np.nan).dropna().astype(float)
                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0]+ "//" + col[1] + " result is True. The values are " + str(set(ds1.astype(str)))[0:1000])
                else:
                    html_logger.err("Fail - " + col[0]+ "//" + col[1] + " result is False. The difference is " + str(set(ds1).difference(set(ds2)))[0:1000] + "  and  \n" + str(set(ds2).difference(set(ds1)))[0:1000])
                    res = 0
                    
            elif('floatLen' in (type)):
                len1 = int(type.split("-")[1])
                padchar = type.split("-")[2]
                #side = type.split("-")[3]
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0 
                
                              
            elif(type.lower() == 'date'):
                ds1 = ds1.astype('datetime64[ns]')
                ds2 = ds2.astype('datetime64[ns]')
                if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1)))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                    res = 0
    
    def report_diff(x):
        return x[0] if x[0] == x[1] else '{} | {}'.format(*x)

    def newCompare(df1,df2,cols,forKey):
        res = 1
        newdf1 = pd.DataFrame()
        newdf2 = pd.DataFrame()  
        for col in cols:
            newdf1[col[0]] =df1[col[0]] 
            newdf2[col[0]] =df2[col[1]]                       
        
        newdf1[forKey] = df1[forKey]
        newdf2[forKey] = df2[forKey]
        
        print(newdf1.head())
        print(newdf2.head())
        
        
        df_all = pd.concat([newdf1.set_index(forKey), newdf2.set_index(forKey)],axis='columns',keys=['newdf1', 'newdf2'], join='outer')
        #df_all = pd.concat([newdf1,newdf2],axis='columns',keys=['newdf1', 'newdf2'], join='outer')
        df_all = df_all.swaplevel(axis='columns')[newdf1.columns[:-1]]
        changes = df_all.groupby(level=0, axis=1).apply(lambda frame: frame.apply(DFCompare.report_diff, axis=1))
        return changes
        #if(res==1):
            #html_logger.statusPass("The Status of Test is Pass")
        #else:
            #html_logger.statusFail("The Status of Test is Fail")     
            
    def compare_fullReport(df1,df2,cols):
        html_logger.dbg("No of DB rows are " + str(df1.shape[0]))
        html_logger.dbg("No of Report rows are " + str(df2.shape[0]))
        html_logger.dbg("----------------------------------")
        res = 1
        for col in cols:
            ds1 = df1[col[0]]
            ds2 = df2[col[1]]
            type = col[2]
            if(type.lower() == 'string'):
                try:
                    ds1 = ds1.replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper().str.strip()  #replace('None',np.nan).replace('',np.nan).dropna().
                    ds2 = ds2.replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper().str.strip()  #replace('None',np.nan).replace('',np.nan).dropna()
                except:
                    ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().str.strip().replace('\\n',' ')
                    ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().str.strip().replace('\\n',' ')

                if(set(ds1.replace('',np.nan).dropna()) == set(ds2.replace('',np.nan).dropna())):
                    html_logger.dbg("Pass - " + col[0] + "//" + col[1] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + "//" + col[1]+ " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n\n\n" + str(set(ds2).difference(set(ds1))))   #[0:1000]
                    res = 0

                        
            elif(type.lower() == 'float'):
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(float)
                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0]+ "//" + col[1] + " result is True. The values are " + str(set(ds1.astype(str)))[0:1000])
                else:
                    html_logger.err("Fail - " + col[0]+ "//" + col[1] + " result is False. The difference is " + str(set(ds1).difference(set(ds2)))[0:1000] + "  and  \n\n\n" + str(set(ds2).difference(set(ds1)))[0:1000])
                    res = 0
                    
            elif('floatLen' in (type)):
                len1 = int(type.split("-")[1])
                padchar = type.split("-")[2]
                #side = type.split("-")[3]
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0 
                
                              
            elif(type.lower() == 'date'):
                ds1 = ds1.astype('datetime64[ns]')
                ds2 = ds2.astype('datetime64[ns]')
                if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1)))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                    res = 0
                        
        #if(res==1):
            #html_logger.statusPass("The Status of Test is Pass")
        #else:
            #html_logger.statusFail("The Status of Test is Fail")    
                                   
    
    def compare1(df1,df2,cols):
        res = 1
        for col in cols:
            ds1 = df1[col[0]]
            ds2 = df2[col[1]]
            type = col[2]
            if(type.lower() == 'string'):
                try:
                    ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper()
                    ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper()
                except:
                    ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' ')
                    ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' ')

                if(set(ds1) == set(ds2)):
                    print("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    print("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0

                        
            elif(type.lower() == 'float'):
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(float)
                if(set(ds1) == set(ds2)):
                    print("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    print("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0
                    
            elif('floatLen' in (type)):
                len1 = int(type.split("-")[1])
                padchar = type.split("-")[2]
                #side = type.split("-")[3]
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                if(set(ds1) == set(ds2)):
                    print("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    print("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0 
                    
            elif('strLen' in (type)):
                len1 = int(type.split("-")[1])
                #padchar = type.split("-")[2]
                #side = type.split("-")[3]
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(str)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(str)
                if(set(ds1) == set(ds2)):
                    print("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    print("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0     
                              
            elif(type.lower() == 'date'):
                ds1 = ds1.astype('datetime64[ns]')
                ds2 = ds2.astype('datetime64[ns]')
                if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                    print("Pass - " + col[0] + " result is True. The values are " + str(set(ds1)))
                else:
                    print("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                    res = 0
                        
        if(res==1):
            print("The Status of Test is Pass")
        else:
            print("The Status of Test is Fail")                
     
     
    def compare_noResult(df1,df2,cols):
        res = 1
        for col in cols:
            ds1 = df1[col[0]]
            ds2 = df2[col[1]]
            type = col[2]
            if(type.lower() == 'string'):
                try:
                    ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper()
                    ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper()
                except:
                    ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' ')
                    ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' ')

                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0] + " result is True") #. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0

                        
            elif(type.lower() == 'float'):
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(float)
                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0] + " result is True") #. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0
                    
            elif('floatLen' in (type)):
                len1 = int(type.split("-")[1])
                padchar = type.split("-")[2]
                #side = type.split("-")[3]
                ds1 = ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                ds2 = ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)
                if(set(ds1) == set(ds2)):
                    html_logger.dbg("Pass - " + col[0] + " result is True")  #. The values are " + str(set(ds1.astype(str)))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
                    res = 0 
                
                              
            elif(type.lower() == 'date'):
                ds1 = ds1.astype('datetime64[ns]')
                ds2 = ds2.astype('datetime64[ns]')
                if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                    html_logger.dbg("Pass - " + col[0] + " result is True.")   #The values are " + str(set(ds1))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                    res = 0

                
def compare_baselined(df1,df2,cols):
        res = 1
        for col in cols:
            ds1 = df1[col[0]]
            ds2 = df2[col[1]]
            type = col[2]
            if(type.lower() == 'string'):
                try:
                    if(set(ds1.replace('None',np.nan).replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper()) == set(ds2.replace('None',np.nan).replace('',np.nan).str.replace('\\n',' ').dropna().astype(str).str.upper())):
                        html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                    else:
                        html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1.replace('None',np.nan).replace('',np.nan).dropna().str.replace('\\n',' ').astype(str).str.upper())) + "  and  \n" + str(set(ds2.replace('None',np.nan).replace('',np.nan).str.replace('\\n',' ').dropna().astype(str).str.upper())))
                        res = 0
                except:
                    if(set(ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' ')) == set(ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' '))):
                        html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                    else:
                        html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' '))) + "  and  \n" + str(set(ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.upper().replace('\\n',' '))))
                        res = 0
                        
            elif(type.lower() == 'float'):
                if(set(ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(float)) == set(ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(float))):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(float))) + "  and  \n" + str(set(ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(float))))
                    res = 0
                    
            elif('floatLen' in (type)):
                len1 = int(type.split("-")[1])
                padchar = type.split("-")[2]
                #side = type.split("-")[3]
                
                if(set((ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float))) == set((ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1)).astype(float))):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1.astype(str))))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set((ds1.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)))) + "  and  \n" + str((set(ds2.replace('None',np.nan).replace('',np.nan).dropna().astype(str).str.zfill(len1).astype(float)))))
                    res = 0 
                
                              
            elif(type.lower() == 'date'):
                ds1 = ds1.astype('datetime64[ns]')
                ds2 = ds2.astype('datetime64[ns]')
                if(set(ds1.fillna('')) == set(ds2.fillna(''))):
                    html_logger.dbg("Pass - " + col[0] + " result is True. The values are " + str(set(ds1)))
                else:
                    html_logger.err("Fail - " + col[0] + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  " + str(set(ds2).difference(set(ds1))))
                    res = 0
                        
        if(res==1):
            html_logger.statusPass("The Status of Test is Pass")
        else:
            html_logger.statusFail("The Status of Test is Fail")                


class Comparator:

    def Compare_XML_DB(xmlpath,xmldfcols,sql1,dbserver,dbname,xmlcols,sqlcols):
      
        xmldf = Parser.parseXML(xmlpath,xmldfcols)
        sqldf = Parser.parseSQL(sql1, dbserver,dbname)
        tstresult = DFCompare.diffDataFrame(xmldf, sqldf, xmlcols, sqlcols)
        

    def Compare_CSV_DB(csvpath,sql1,dbserver,dbname,csvcols,sqlcols):
        csvdf = Parser.parseCSV(csvfilename)
        sqldf = Parser.parseSQL(sql1, dbserver,dbname)
        DFCompare.diffDataFrame(csvdf, sqldf, csvcols, sqlcols)
    
          
          
          