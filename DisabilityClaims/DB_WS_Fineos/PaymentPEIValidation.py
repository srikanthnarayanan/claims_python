import html_logger
from html_logger import setup, info
import datetime
import logging
import sys
import time
import csv

import pandas as pd
import xml.etree.ElementTree as ET
    

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait 

import html_logger
from html_logger import setup, info
    
now = datetime.datetime.now()
parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  

LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\PaymentPEIValidation_"+ str(now.isoformat().replace(":","_")) + ".html"
setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
   
def ValidatePaymentPEI(filename2,claimNos):
    now = datetime.datetime.now()
    parentFolder = "C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
    sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")
    LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\PaymentPEIExtractValidation_"+ str(now.isoformat().replace(":","_")) + ".html"
    setup("Payment vs PEIExtract Validation","Executed on " + str(now),LOG_FILENAME)
    
    
    #claimNos = ['DI-453']   #['DI-327','DI-416']               #['DI-416'] #['DI-383','DI-382', 'DI-384'] #
    import pandas as pd
    for claimNo in claimNos:
        html_logger.dbg("Working on Claim Number " + str(claimNo))
        
        #-------------------------------------------------------------------------------------------------------------
        PEclass = ""
        PEindex = ""
        df1 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIClaimDetails.csv")
        df2 = df1[df1['CLAIMNUMBER']==claimNo]
        PEclass = df2['PECLASSID'].to_string(index = False)
        PEindex = df2['PEINDEXID'].to_string(index = False)
        
        if("\n' in PEclass"):
            arrPEclass = PEclass.split("\n")
            arrPEindex = PEindex.split("\n")
        else:
            arrPEclass = PEclass
            arrPEindex = PEindex
        i = 0 
        PElistAmount = [] #list1 = []
        list3 = []
        PEIlistLineType = []
        listLineType = []
        
        #-------------------------------------------------------------------------------------------------------------
        for PEclass1 in arrPEclass:     
            PEindex1 = arrPEindex[i]
            i = i + 1 
            df3 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentLine.csv")
            try:
                df4 = df3[df3['C_PYMNTEIF_PAYMENTLINES'] == int(PEclass1)]
                df4 = df4[df4['I_PYMNTEIF_PAYMENTLINES'] == int(PEindex1)]
                
                df5 = df4['AMOUNT_MONAMT']
                #df6 = df4['PAYEEFULLNAME']
                df7 = df4['LINETYPE']
                
                PElistAmount = PElistAmount + (df5.values).tolist()
                PEIlistLineType = PEIlistLineType + (df7.values).tolist()
            except:
                html_logger.err("no records found for " + str(claimNo) + " in the PEI Extract")
        #-------------------------------------------------------------------------------------------------------------
        
        #exp_df = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\extract.csv")
        exp_df = pd.read_csv(filename2)             
        exp_df = exp_df[exp_df['CLAIMNUMBER']==claimNo]
        exp_df1 = exp_df['AMOUNT_MONAMT']
        exp_name = exp_df['PAYEEFULLNAME'].dropna()
        exp_paymethod = exp_df['PAYMENTMETHOD'].dropna()
        exp_lineType = exp_df['LINETYPE'].dropna()
        lineStatusind = 1
        
        ExpAMOUNT = exp_df1.values.tolist()
        listName = exp_name.values.tolist()
        listpaymentMethod = exp_paymethod.values.tolist()
        listLineType = exp_lineType.values.tolist()
        
        totamt = 0
        for amnt in ExpAMOUNT:
            totamt = totamt + amnt
        html_logger.info("---Validating from PEIPaymentLine Extract---")
        
        try:
            if(sorted(PElistAmount)==sorted(ExpAMOUNT)):
                html_logger.dbg("Breakdown of the amount matches. The values are " + str(sorted(PElistAmount)))
            else:
                html_logger.err("Breakdown of the amount doesn't match. Expected is " + str(sorted(ExpAMOUNT)) + " but the extract has " + str(sorted(PElistAmount))) 
                lineStatusind = 0   
        except:
            html_logger.err("Error in parsing data")
        
        if(sorted(listLineType) == sorted(PEIlistLineType)):
            html_logger.dbg("Payment LineType matches. Values are " + str(listLineType))
        else:
            html_logger.err("Payment LineType doesn't match.PEI extract has " + str(PEIlistLineType) + " but the payment made for " + str(listLineType))
            lineStatusind = 0    
        #except:
            #print("False")
        
        peinamelist = []
        peipaymentmethod = []
        #-------------------------------------------------------------------------------------------------------------        
        acttotamt = 0    
        df31 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv")
        i = 0
        for PEclass1 in arrPEclass:     
            PEindex1 = arrPEindex[i]
            i = i + 1 
            #try:
            df41 = df31[df31['C'] == int(PEclass1)]
            df41 = df41[df41['I'] == int(PEindex1)]
            df51 = df41['AMOUNT_MONAMT']
            df52 = df41['PAYMENTMETHOD']
            df53 = df41['PAYEEFULLNAME']
            df54 = df41['PAYMENTMETHOD']
            
            #print(str(df53.to_string(index = False)))
            peinamelist.append(df53.to_string(index = False).strip())
            peipaymentmethod.append(df54.to_string(index = False).strip())
            
            #html_logger.dbg("Individual amount of the payment is " + str(round(float(df51),2)))
            #html_logger.dbg("Payment method is " + df52.to_string(index = False))
            #print("Payee FullName is " + df53.to_string(index = False))
            acttotamt = acttotamt + round(float(df51),2)
        
        html_logger.info("---Validating from PEI Extract---")        
        if(round(float(acttotamt),2)==round(float(totamt),2)):
            html_logger.dbg("amount matches. Values are " + str(round(float(totamt),2)))
        else:
            html_logger.err("amount doesn't match. Expected value is " + str(round(float(totamt),2)) + " but the actual is " + str(round(float(acttotamt),2)) + " and the payment method is " + df52.to_string(index = False))
            lineStatusind = 0    
        #except:
            #print("False")
    
        if(sorted(listName) == sorted(peinamelist)):
            html_logger.dbg("Name matches. Values are " + str(listName))
        else:
            html_logger.err("Name doesn't match.PEI extract has " + str(peinamelist) + " but the payment made for " + str(listName)) 
            lineStatusind = 0   
        #except:
            #print("False")  
        
        if(sorted(listpaymentMethod) == sorted(peipaymentmethod)):
            html_logger.dbg("Payment Method matches. Values are " + str(listpaymentMethod))
        else:
            html_logger.err("Payment Method doesn't match.PEI extract has " + str(peipaymentmethod) + " but the payment made for " + str(listpaymentMethod))   
            lineStatusind = 0 
        #except:
            #print("False")

        html_logger.dbg("*************************************************************")
        print("end")

def extractDetails(casenos):
    
    now = datetime.datetime.now()
    #casenos = ['DI-327','DI-383']           #["DI-453"] #["DI-416","DI-327"]
    outString = "CLAIMNUMBER,AMOUNT_MONAMT,PAYEEFULLNAME,PAYMENTMETHOD,LINETYPE,STATUS"
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe') 
    driver.get("https:CONTENT:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
        
    for caseno in casenos:
        
       
        xpth_srchCase = "//a[contains(@id,'MENUITEM.SearchCaseslink')]"
        Click(xpth_srchCase)
        #driver.implicitly_wait(5)
    
        xpth_Case = "//div[@class='TabOff'][contains(text(),'Case')]"
        #Sync(xpth_rdoOrganization)
        Click(xpth_Case)   
        
        xpth_CaseNo = "//input[contains(@id,'_caseNumber')]"
        Set(xpth_CaseNo,caseno)
        Click("//input[contains(@id,'searchButton')]")  # search button    
        
        try:
            if(len(driver.find_element_by_xpath("//*[@id='PageMessage1']")) == 0):
                print("Case number not found, hence exiting")
        except:
                pass 
        
        benefitrowno = len(driver.find_elements_by_xpath("//div[@id='recurringBenefitWidgetMultipaint']/div"))
        for ii  in range(benefitrowno):   
            syncwait(3)
            Click("//div[@id='recurringBenefitWidgetMultipaint']/div[" + str(ii+1) + "]/div/div/span/span[2]/a")
            syncwait(3)
            #advicetopay = driver.find_element_by_xpath("//img[contains(@id,'_adviceToPay')]").get_attribute("checked")
            #checkcutt = driver.find_element_by_xpath("//*[contains(@id,'_checkCutting')]").text 
            #Click("//a[text()='Group Disability Claim']")
             
            Click("//div[@class='TabOff'][contains(.,'Payment History')]") 
            syncwait(3)
        
            rowno = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr")) #.size()
            for i in range(rowno):
               syncwait(3)
               if(driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i+1) + "]/td[6]").text == "Active"):
                  Click("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i+1) + "]/td[1]") 
                 # syncwait(3)
                  TotalAmt =  driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i+1) + "]/td[7]").text
                  #print(TotalAmt)
                  name1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i+1) + "]/td[8]").text
                  addpaymethod = 1
                  try:
                      if(len(driver.find_elements_by_xpath("//span[contains(@id,'_AllocatedDuesListView_blockNumber_1')]")) > 0):
                          Click("//span[contains(@id,'_AllocatedDuesListView_blockNumber_1')]")
                  except:
                      pass
                          
                  while True:
                          payAllRowno = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr")) #.size()
                          for j in range(1,payAllRowno+1):
                              Click("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) + "]/td[1]")
                              #syncwait(5)
                              #driver.find_element_by_xpath(xpath)
                              Click("//input[@title='View payment instruction']")
                              
                              if(addpaymethod==1):
                                  syncwait(5) 
                                  paymthd = driver.find_element_by_xpath("//span[contains(@id,'_paymentMethodDropDown')]").text
                                  addpaymethod =0
                              else:
                                  paymthd = ""
                                  
                              balancepayeeadjus = len(driver.find_elements_by_xpath("//table[contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr")) #.size()
                              for k in range(1,balancepayeeadjus+1):
                                name =  driver.find_element_by_xpath("//table[contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" + str(k) + "]/td[1]").text
                                amount = driver.find_element_by_xpath("//table[contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" + str(k) + "]/td[2]").text
                                amount = (str(amount)).replace(",","")      
                                outString = outString + "\n" + caseno + "," + amount + "," + name1 + "," + paymthd + "," + name         #+ "," + advicetopay + "," + checkcutt
                                name1 = ""
                                paymthd = ""
                                advicetopay = ""
                                checkcutt = ""
                                
                              benefitamt = len(driver.find_elements_by_xpath("//table[contains(@id,'OffsetsListview_')]/tbody/tr")) #.size()
                              for k in range(1,benefitamt+1):
                                 name = driver.find_element_by_xpath("//table[contains(@id,'OffsetsListview_')]/tbody/tr[" + str(k) + "]/td[1]").text
                                 amount = driver.find_element_by_xpath("//table[contains(@id,'OffsetsListview_')]/tbody/tr[" + str(k) + "]/td[2]").text 
                                 amount = (str(amount)).replace(",","")
                                 outString = outString + "\n" + caseno + "," + amount + "," + name1 + "," + paymthd + "," + name        #+ "," + advicetopay + "," + checkcutt
                                 name1 = ""
                                 paymthd = ""
                                 advicetopay = ""
                                 checkcutt = ""
              
                              Click("//input[contains(@id,'_cmdPageBack_cloned')]")
                          if len(driver.find_elements_by_xpath("//a[contains(@name,'_AllocatedDuesListView_cmdNext')]")) == 0:
                              break
                              
                          if len(driver.find_elements_by_xpath("//a[contains(@name,'_AllocatedDuesListView_cmdNext')]")) > 0:      
                              Click("//a[contains(@name,'_AllocatedDuesListView_cmdNext')]")
                              
                          
            Click("//a[text()='Group Disability Claim']")   
            syncwait(3)
    filename1 = 'C:\\Users\\T003320\\Desktop\\New_PEI\\Extract' + str(now.isoformat().replace(":","_")) + '.csv'              
    with open(filename1,'w') as outfile:
        outfile.write(outString)
    driver.close()   
    ValidatePaymentPEI(filename1,casenos)    
    print("end")

def getvalueofnode(node):
    """ return node text or None """
    return node.text if node is not None else None




#https://gokhanatil.com/2017/11/python-for-data-science-importing-xml-to-pandas-dataframe.html
#------------------------------------------XML to SQL Validation--------------------------------------------------------------------
'''------------------------------------- 
 ---------------XML to SQL Validation--------------------------'''

def dataframe_difference1(df1, df2, which=None):
    """Find rows which are different between two DataFrames."""
    comparison_df = df1.merge(df2,
                              indicator=True,
                              how='outer')
    if which is None:
        diff_df = comparison_df[comparison_df['_merge'] != 'both']
    else:
        diff_df = comparison_df[comparison_df['_merge'] == which]
    #diff_df.to_csv('C:\\Srikanth\\123456.csv')
    return diff_df


def dataframe_difference(df1, df2, which=None):
    return df1.to_string(index=False) == df2.to_string(index=False)


def diffDataFrame(ds1,ds2,colname):
    
    if(set(ds1.dropna()) == set(ds2.dropna())):
        #print(colname + " result is True")
        html_logger.dbg("Pass - " + colname + " result is True")
        return True
    else:    
        #print(colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))))
        html_logger.err("Fail - " + colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))) + "  and  \n" + str(set(ds2).difference(set(ds1))))
        return False


def diffDataFrame_isin(ds1,ds2,colname):
    if(ds1.dropna().isin(ds2.dropna()).all()):
        #print(colname + " result is True")
        html_logger.dbg("Pass - " + colname + " result is True")
    else:    
        #print(colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))))
        html_logger.err("Fail - " + colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))))


def diffDataFrameString(ds1,consValue,colname):
    if((ds1 == consValue).all()):    #str((ds1] == consValue).all())
        #print(colname + " result is True")
        html_logger.dbg("Pass - " + colname + " result is True")
    else:    
        #print(colname + " result is False. The difference is " + str(set(ds1).difference(set(ds2))))
        html_logger.err("Fail - " + colname + " result is False. The difference is " + ds1.to_string(index=False))



 #-----------------------------------------------------------------------------


 #------------------------------------------------------------------   

def getXML():
    #print(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\OneAmerica_Disbursement.xml")
    parsed_xml = ET.parse("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\OneAmerica_Disbursement.xml")
    dfcols = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']
    df_xml = pd.DataFrame(columns=dfcols)
    
    for node in parsed_xml.getroot():
        Pay_Rqst_Nbr = node.find('Pay_Rqst_Nbr')
        Pay_Admin_Sys = node.find('Pay_Admin_Sys')
        Cmpny_Cd = node.find('Cmpny_Cd')
        Src_Cd = node.find('Src_Cd')       
        Lgcy_Src_Cd = node.find('Lgcy_Src_Cd')
        Pay_Typ = node.find('Pay_Typ')
        Pay_Amt = node.find('Pay_Amt')
        Payee_Name = node.find('Payee_Name')
        Chk_Addr_Line_1 = node.find('Chk_Addr_Line_1')
        Chk_Addr_Line_2 = node.find('Chk_Addr_Line_2')
        Chk_Addr_Line_3 = node.find('Chk_Addr_Line_3')
        Chk_Addr_City = node.find('Chk_Addr_City')
        Chk_Addr_St = node.find('Chk_Addr_St')
        Chk_Addr_Zip_Cd = node.find('Chk_Addr_Zip_Cd')
        Mail_Sort_Cd = node.find('Mail_Sort_Cd')
        Ent_Oper_Usr_Id = node.find('Ent_Oper_Usr_Id')
        Rdfi_Rout_Nbr = node.find('Rdfi_Rout_Nbr')
        Rdfi_Acct_Nbr = node.find('Rdfi_Acct_Nbr')
        Rdfi_Acct_Typ = node.find('Rdfi_Acct_Typ')
        
        df_xml = df_xml.append(
            pd.Series([getvalueofnode(Pay_Rqst_Nbr), getvalueofnode(Pay_Admin_Sys),getvalueofnode(Cmpny_Cd),getvalueofnode(Src_Cd),
                       getvalueofnode(Lgcy_Src_Cd), getvalueofnode(Pay_Typ),getvalueofnode(Pay_Amt),getvalueofnode(Payee_Name),
                       getvalueofnode(Chk_Addr_Line_1), getvalueofnode(Chk_Addr_Line_2),getvalueofnode(Chk_Addr_Line_3),getvalueofnode(Chk_Addr_City),getvalueofnode(Chk_Addr_St),
                       getvalueofnode(Chk_Addr_Zip_Cd), getvalueofnode(Mail_Sort_Cd),getvalueofnode(Ent_Oper_Usr_Id),getvalueofnode(Rdfi_Rout_Nbr),
                       getvalueofnode(Rdfi_Acct_Nbr),getvalueofnode(Rdfi_Acct_Typ)], index=dfcols),
            ignore_index=True)
    #print(df_xml.columns)
    return df_xml
 
def getSQL(xmlDF): 
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10746,4242' +';'
                      'Database=' + 'ENTPRS_CLAIMS_ODS' + ';'
                      'Trusted_Connection=yes;')
    
    sqlQuery = pd.read_sql_query("select Pay_Rqst_Nbr,Pay_Admin_Sys,Cmpny_Cd,Src_Cd,Lgcy_Src_Cd,Pay_Typ,Pay_Amt,Payee_Name,Chk_Addr_Line_1,Chk_Addr_Line_2,Chk_Addr_Line_3,Chk_Addr_City,Chk_Addr_St,Chk_Addr_Zip_Cd,Mail_Sort_Cd,Ent_Oper_Usr_Id,Rdfi_Rout_Nbr,Rdfi_Acct_Nbr,Rdfi_Acct_Typ from FAI.PAY_RQST_INFO",conn)
    #df = pd.DataFrame(sqlQuery,columns = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ'])  
    
    #print(df_xml)
    df_sql = sqlQuery[sqlQuery['Pay_Rqst_Nbr'].isin(xmlDF['Pay_Rqst_Nbr'])]
    #print(df_sql.columns)
    return df_sql

def XMLtoSQL():
    html_logger.info("Start of Comparing XML with ODS")
    xmlDF = getXML()
    sqldf = getSQL(xmlDF)
    
    tmpsqldf = sqldf.sort_values(by=['Pay_Rqst_Nbr']).reset_index(drop=True)
    tmpXMLDF = xmlDF.sort_values(by=['Pay_Rqst_Nbr']).reset_index(drop=True)
    
    tmpsqldf2 = sqldf
    tmpXMLDF2 = xmlDF
    tmpsqldf2['Pay_Amt'] = sqldf['Pay_Amt'].astype(float)
    tmpXMLDF2['Pay_Amt'] = xmlDF['Pay_Amt'].astype(float)
    tmpsqldf2 = tmpsqldf2.sort_values(by=['Pay_Amt']).reset_index(drop=True)
    tmpXMLDF2 = tmpXMLDF2.sort_values(by=['Pay_Amt']).reset_index(drop=True)
    
    
    diffDataFrame(tmpsqldf['Pay_Rqst_Nbr'],tmpXMLDF['Pay_Rqst_Nbr'],"Pay_Rqst_Nbr")
    diffDataFrame(tmpsqldf['Pay_Admin_Sys'],tmpXMLDF['Pay_Admin_Sys'],"Pay_Admin_Sys")
    diffDataFrame(tmpsqldf['Cmpny_Cd'],tmpXMLDF['Cmpny_Cd'],"Cmpny_Cd")
    diffDataFrame(tmpsqldf['Src_Cd'],tmpXMLDF['Src_Cd'],"SRC_CD")
    diffDataFrame(tmpsqldf['Lgcy_Src_Cd'],tmpXMLDF['Lgcy_Src_Cd'],"Lgcy_Src_Cd")
    diffDataFrame(tmpsqldf['Pay_Typ'],tmpXMLDF['Pay_Typ'],"Pay_Typ")
    diffDataFrame(tmpsqldf2['Pay_Amt'],tmpXMLDF2['Pay_Amt'],"Pay_Amt")
    diffDataFrame(tmpsqldf['Payee_Name'],tmpXMLDF['Payee_Name'],"Payee_Name")
    diffDataFrame(tmpsqldf['Chk_Addr_Line_1'],tmpXMLDF['Chk_Addr_Line_1'],"Chk_Addr_Line_1")
    diffDataFrame(tmpsqldf['Chk_Addr_Line_2'],tmpXMLDF['Chk_Addr_Line_2'],"Chk_Addr_Line_2")
    diffDataFrame(tmpsqldf['Chk_Addr_Line_3'],tmpXMLDF['Chk_Addr_Line_3'],"Chk_Addr_Line_3")
    diffDataFrame(tmpsqldf['Chk_Addr_City'],tmpXMLDF['Chk_Addr_City'],"Chk_Addr_City")
    diffDataFrame(tmpsqldf['Chk_Addr_St'],tmpXMLDF['Chk_Addr_St'],"Chk_Addr_St")
    diffDataFrame(tmpsqldf['Chk_Addr_Zip_Cd'],tmpXMLDF['Chk_Addr_Zip_Cd'],"Chk_Addr_Zip_Cd")
    diffDataFrame(tmpsqldf['Mail_Sort_Cd'],tmpXMLDF['Mail_Sort_Cd'],"Mail_Sort_Cd")
    diffDataFrame(tmpsqldf['Ent_Oper_Usr_Id'],tmpXMLDF['Ent_Oper_Usr_Id'],"Ent_Oper_Usr_Id")
    diffDataFrame(tmpsqldf['Rdfi_Rout_Nbr'],tmpXMLDF['Rdfi_Rout_Nbr'],"Rdfi_Rout_Nbr")
    diffDataFrame(tmpsqldf['Rdfi_Acct_Nbr'],tmpXMLDF['Rdfi_Acct_Nbr'],"Rdfi_Acct_Nbr")
    diffDataFrame(tmpsqldf['Rdfi_Acct_Typ'],tmpXMLDF['Rdfi_Acct_Typ'],"Rdfi_Acct_Typ")
    html_logger.info("End of Comparing XML with ODS")
    



'''------------------------------------- 
 ---------------XML to SQL Validation--------------------------'''
 
#-------------------------------------------XML to SQL Validation------------------------------------------------- 



#------------------------------------------CSV to XML Validation--------------------------------------------------------------------
'''------------------------------------- 
 ---------------CSV to XML Validation--------------------------'''
    
def CSVtoXML():
    xmlDF = getXML()
    df1 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv")
    
    
    listpayname = xmlDF['Payee_Name'].tolist()
    listpayamt = xmlDF['Pay_Amt'].tolist()
    i = 0
    salutations = ['Mr', 'Ms', 'Miss','Mrs']

    for payname in listpayname:     
        print("--------" + payname + "-------------")
        if any(ext in df1.PAYEEFULLNAME.str.split()[0] for ext in salutations):
            df1['PAYEEFULLNAME'] = df1.PAYEEFULLNAME.replace(df1['PAYEEFULLNAME'].str.split()[0],'',1)        
        df2 = df1[df1['PAYEEFULLNAME'].str.contains(payname)]   # df1[df1['CLAIMNUMBER']==claimNo] 
        df3 = df2[df2['AMOUNT_MONAMT'] == float(listpayamt[i])]

        if(xmlDF['Pay_Admin_Sys'][i]=='FINEOS'):
            print("Pay Admin Sys value Matches")
        else:
            print("Fail. Expected FINEOS but actual is " + df3['Pay_Admin_Sys'])
        
        peival = df3['PAYMENTADD1'].to_string(index=False).strip() 
        xmlval = xmlDF['Chk_Addr_Line_1'][i]
           
        if(df3['PAYMENTADD1'].to_string(index=False).strip() == xmlDF['Chk_Addr_Line_1'][i]):
            print("Payment Address 1 matches as expected")
        else:
            print("Payment Address 1 doesn't match as expected.")  
         
        i = i + 1          
#------------------------------------------CSV to XML Validation--------------------------------------------------------------------
'''------------------------------------- 
 ---------------CSV to XML Validation--------------------------'''
def HandleHierarchyId(v):
      return str(v)        
        
def peiCSVtoLZ():
    html_logger.info("Start of Comparing CSV with LandingZone")
    dfPEICSV = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv")        
    
    sql1 = '''SELECT * FROM LZ.PEI_RQST_INFO'''
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10746,4242' +';'
                      'Database=' + 'ENTPRS_CLAIMS_ODS' + ';'
                      'Trusted_Connection=yes;')
    conn.add_output_converter(-150, HandleHierarchyId)
    sqldf = pd.read_sql_query(sql1,conn)   
    sqldf = sqldf.drop(['PEI_RQST_INFO_GEN_ID','LAST_UPDT_ID','LAST_UPDT_TS'], axis=1)

    dfPEICSV = dfPEICSV.sort_values(by=['I']).reset_index(drop=True)
    dfPEICSV = dfPEICSV[dfPEICSV.PAYMENTMETHOD.isin(['Check','Elec Funds Transfer'])]
    sqldf = sqldf.sort_values(by=['PEI_I']).reset_index(drop=True)  
    '''
    print(dfPEICSV)
    print("--------------")
    print(sqldf)
    print("--------------")
    
    print(dfPEICSV.isin(sqldf))
    '''
    
    diffDataFrame(dfPEICSV['C'],sqldf['PEI_C'],"C")
    diffDataFrame(sqldf['PEI_I'],dfPEICSV['I'],"I")
    
    diffDataFrame(pd.to_datetime(sqldf['LASTUPDATEDATE'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['LASTUPDATEDATE'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"LASTUPDATEDATE")
                                                                                                                                      
    #diffDataFrame_isin(dfPEICSV['LASTUPDATEDATE'],sqldf['LASTUPDATEDATE'],"LASTUPDATEDATE")
    
    diffDataFrame(dfPEICSV['C_OSUSER_UPDATEDBY'],sqldf['C_OSUSER_UPDATEDBY'],"C_OSUSER_UPDATEDBY")
    diffDataFrame(dfPEICSV['I_OSUSER_UPDATEDBY'],sqldf['I_OSUSER_UPDATEDBY'],"I_OSUSER_UPDATEDBY")
    diffDataFrame(sqldf['ADDRESSLINE1'],dfPEICSV['ADDRESSLINE1'],"ADDRESSLINE1")
    diffDataFrame(sqldf['ADDRESSLINE2'],dfPEICSV['ADDRESSLINE2'],"ADDRESSLINE2")
    diffDataFrame(sqldf['ADDRESSLINE3'],dfPEICSV['ADDRESSLINE3'],"ADDRESSLINE3")
    diffDataFrame(sqldf['ADDRESSLINE4'],dfPEICSV['ADDRESSLINE4'],"ADDRESSLINE4")
    diffDataFrame(sqldf['ADDRESSLINE5'],dfPEICSV['ADDRESSLINE5'],"ADDRESSLINE5")
    diffDataFrame(sqldf['ADDRESSLINE6'],dfPEICSV['ADDRESSLINE6'],"ADDRESSLINE6")
    diffDataFrame(sqldf['ADDRESSLINE7'],dfPEICSV['ADDRESSLINE7'],"ADDRESSLINE7")
    diffDataFrame(sqldf['ADVICETOPAY'].apply(lambda x: str(x).upper()),dfPEICSV['ADVICETOPAY'].apply(lambda x: str(x).upper()),"ADVICETOPAY")
    diffDataFrame(sqldf['ADVICETOPAYOV'],dfPEICSV['ADVICETOPAYOV'],"ADVICETOPAYOV")
    diffDataFrame(sqldf['AMALGAMATIONC'],dfPEICSV['AMALGAMATIONC'],"AMALGAMATIONC")
    diffDataFrame(sqldf['AMOUNT_MONAMT'],dfPEICSV['AMOUNT_MONAMT'],"AMOUNT_MONAMT")
    diffDataFrame(sqldf['AMOUNT_MONCUR'],dfPEICSV['AMOUNT_MONCUR'],"AMOUNT_MONCUR")
    diffDataFrame(sqldf['CHECKCUTTING'],dfPEICSV['CHECKCUTTING'],"CHECKCUTTING")
    diffDataFrame(sqldf['CONFIRMEDBYUS'],dfPEICSV['CONFIRMEDBYUS'],"CONFIRMEDBYUS")
    diffDataFrame(sqldf['CONFIRMEDUID'],dfPEICSV['CONFIRMEDUID'],"CONFIRMEDUID")
    
    diffDataFrame(sqldf['CONTRACTREF'],dfPEICSV['CONTRACTREF'],"CONTRACTREF")
    diffDataFrame(sqldf['CORRESPCOUNTR'],dfPEICSV['CORRESPCOUNTR'],"CORRESPCOUNTR")
    diffDataFrame(sqldf['CURRENCY'],dfPEICSV['CURRENCY'],"CURRENCY")
    
    diffDataFrame(pd.to_datetime(sqldf['DATEINTERFACE'],format='%y-%m-%d').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['DATEINTERFACE'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"DATEINTERFACE")
    #diffDataFrame(sqldf['DATEINTERFACE'],dfPEICSV['DATEINTERFACE'],"DATEINTERFACE")
    
    diffDataFrame(sqldf['DESCRIPTION'],dfPEICSV['DESCRIPTION'],"DESCRIPTION")
    diffDataFrame(sqldf['EMPLOYEECONTR'],dfPEICSV['EMPLOYEECONTR'],"EMPLOYEECONTR")
    
    #diffDataFrame(sqldf['EVENTEFFECTIV'],dfPEICSV['EVENTEFFECTIV'],"EVENTEFFECTIV")
    diffDataFrame(pd.to_datetime(sqldf['EVENTEFFECTIV'],format='%y-%m-%d').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['EVENTEFFECTIV'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"EVENTEFFECTIV")
    
    diffDataFrame(sqldf['EVENTREASON'],dfPEICSV['EVENTREASON'],"EVENTREASON")
    diffDataFrame(sqldf['EVENTTYPE'],dfPEICSV['EVENTTYPE'],"EVENTTYPE")
    diffDataFrame(sqldf['EXTRACTIONDAT'],dfPEICSV['EXTRACTIONDAT'],"EXTRACTIONDAT")
    diffDataFrame(sqldf['GROSSPAYMENTA_MONAMT'],dfPEICSV['GROSSPAYMENTA_MONAMT'],"GROSSPAYMENTA_MONAMT")
    diffDataFrame(sqldf['GROSSPAYMENTA_MONCUR'],dfPEICSV['GROSSPAYMENTA_MONCUR'],"GROSSPAYMENTA_MONCUR")
    diffDataFrame(sqldf['INSUREDRESIDE'],dfPEICSV['INSUREDRESIDE'],"INSUREDRESIDE")
    diffDataFrame(sqldf['NAMETOPRINTON'],dfPEICSV['NAMETOPRINTON'],"NAMETOPRINTON")
    diffDataFrame(sqldf['NOMINATEDPAYE'],dfPEICSV['NOMINATEDPAYE'],"NOMINATEDPAYE")
    diffDataFrame(sqldf['NOMPAYEECUSTO'],dfPEICSV['NOMPAYEECUSTO'],"NOMPAYEECUSTO")
    
    #diffDataFrame(sqldf['NOMPAYEEDOB'],dfPEICSV['NOMPAYEEDOB'],"NOMPAYEEDOB")
    diffDataFrame(pd.to_datetime(sqldf['NOMPAYEEDOB'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['NOMPAYEEDOB'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"NOMPAYEEDOB")
    
    diffDataFrame(sqldf['NOMPAYEEFULLN'],dfPEICSV['NOMPAYEEFULLN'],"NOMPAYEEFULLN")
    diffDataFrame(sqldf['NOMPAYEESOCNU'],dfPEICSV['NOMPAYEESOCNU'],"NOMPAYEESOCNU")
    diffDataFrame(sqldf['NOTES'],dfPEICSV['NOTES'],"NOTES")
    diffDataFrame(sqldf['PAYEEACCOUNTN'].astype(float),dfPEICSV['PAYEEACCOUNTN'].astype(float),"PAYEEACCOUNTN")
    
    diffDataFrame(sqldf['PAYEEACCOUNTT'],dfPEICSV['PAYEEACCOUNTT'],"PAYEEACCOUNTT")
    
    dfPEICSV['PAYEEADDRESS'] = dfPEICSV['PAYEEADDRESS'].str.replace('\\n',' ')
    diffDataFrame(sqldf['PAYEEADDRESS'],dfPEICSV['PAYEEADDRESS'],"PAYEEADDRESS")
    
    
    diffDataFrame(sqldf['PAYEEBANKBRAN'],dfPEICSV['PAYEEBANKBRAN'],"PAYEEBANKBRAN")
    diffDataFrame(sqldf['PAYEEBANKCODE'],dfPEICSV['PAYEEBANKCODE'],"PAYEEBANKCODE")
    diffDataFrame(sqldf['PAYEEBANKINST'],dfPEICSV['PAYEEBANKINST'],"PAYEEBANKINST")
    diffDataFrame(sqldf['PAYEEBANKSORT'].astype(float),dfPEICSV['PAYEEBANKSORT'].astype(float),"PAYEEBANKSORT")
    diffDataFrame(sqldf['PAYEECORRESPO'],dfPEICSV['PAYEECORRESPO'],"PAYEECORRESPO")
    diffDataFrame(sqldf['PAYEECUSTOMER'],dfPEICSV['PAYEECUSTOMER'],"PAYEECUSTOMER")
    
    #diffDataFrame(sqldf['PAYEEDOB'],dfPEICSV['PAYEEDOB'],"PAYEEDOB")
    diffDataFrame(pd.to_datetime(sqldf['PAYEEDOB'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['PAYEEDOB'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"PAYEEDOB")
    
    diffDataFrame(sqldf['PAYEEFULLNAME'],dfPEICSV['PAYEEFULLNAME'],"PAYEEFULLNAME")
    diffDataFrame(sqldf['PAYEEIDENTIFI'],dfPEICSV['PAYEEIDENTIFI'],"PAYEEIDENTIFI")
    diffDataFrame(sqldf['PAYEESOCNUMBE'],dfPEICSV['PAYEESOCNUMBE'],"PAYEESOCNUMBE")
    
    dfPEICSV['PAYMENTADD'] = dfPEICSV['PAYMENTADD'].str.replace('\\n',' ')
    diffDataFrame(sqldf['PAYMENTADD'],dfPEICSV['PAYMENTADD'],"PAYMENTADD")
        
    diffDataFrame(sqldf['PAYMENTADD1'],dfPEICSV['PAYMENTADD1'],"PAYMENTADD1")
    diffDataFrame(sqldf['PAYMENTADD2'],dfPEICSV['PAYMENTADD2'],"PAYMENTADD2")
    diffDataFrame(sqldf['PAYMENTADD3'],dfPEICSV['PAYMENTADD3'],"PAYMENTADD3")
    diffDataFrame(sqldf['PAYMENTADD4'],dfPEICSV['PAYMENTADD4'],"PAYMENTADD4")
    diffDataFrame(sqldf['PAYMENTADD5'],dfPEICSV['PAYMENTADD5'],"PAYMENTADD5")
    diffDataFrame(sqldf['PAYMENTADD6'],dfPEICSV['PAYMENTADD6'],"PAYMENTADD6")
    diffDataFrame(sqldf['PAYMENTADD7'],dfPEICSV['PAYMENTADD7'],"PAYMENTADD7")
    diffDataFrame(sqldf['PAYMENTADDCOU'],dfPEICSV['PAYMENTADDCOU'],"PAYMENTADDCOU")
    diffDataFrame(sqldf['PAYMENTCORRST'],dfPEICSV['PAYMENTCORRST'],"PAYMENTCORRST")
    
    diffDataFrame(pd.to_datetime(sqldf['PAYMENTDATE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['PAYMENTDATE'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"PAYMENTDATE")
    #diffDataFrame(sqldf['PAYMENTDATE'],dfPEICSV['PAYMENTDATE'],"PAYMENTDATE")
    
    diffDataFrame(sqldf['PAYMENTFREQUE'],dfPEICSV['PAYMENTFREQUE'],"PAYMENTFREQUE")
    diffDataFrame(sqldf['PAYMENTMETHOD'],dfPEICSV['PAYMENTMETHOD'],"PAYMENTMETHOD")
    diffDataFrame(sqldf['PAYMENTPOSTCO'],dfPEICSV['PAYMENTPOSTCO'],"PAYMENTPOSTCO")
    diffDataFrame(sqldf['PAYMENTPREMIS'],dfPEICSV['PAYMENTPREMIS'],"PAYMENTPREMIS")
    
    #diffDataFrame(sqldf['PAYMENTTRIGGE'],dfPEICSV['PAYMENTTRIGGE'],"PAYMENTTRIGGE")
    diffDataFrame(pd.to_datetime(sqldf['PAYMENTTRIGGE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['PAYMENTTRIGGE'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"PAYMENTTRIGGE")
    
    diffDataFrame(sqldf['PAYMENTTYPE'],dfPEICSV['PAYMENTTYPE'],"PAYMENTTYPE")
    diffDataFrame(sqldf['PAYMETHCURREN'],dfPEICSV['PAYMETHCURREN'],"PAYMETHCURREN")
    diffDataFrame(sqldf['POSTCODE'],dfPEICSV['POSTCODE'],"POSTCODE")
    diffDataFrame(sqldf['PREMISESNO'],dfPEICSV['PREMISESNO'],"PREMISESNO")
    diffDataFrame(sqldf['SETUPBYUSERID'],dfPEICSV['SETUPBYUSERID'],"SETUPBYUSERID")
    diffDataFrame(sqldf['SETUPBYUSERNA'],dfPEICSV['SETUPBYUSERNA'],"SETUPBYUSERNA")
    diffDataFrame(sqldf['STATUS'],dfPEICSV['STATUS'],"STATUS")
    
    diffDataFrame(pd.to_datetime(sqldf['STATUSEFFECTI'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['STATUSEFFECTI'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"STATUSEFFECTI")
    #diffDataFrame(sqldf['STATUSEFFECTI'],dfPEICSV['STATUSEFFECTI'],"STATUSEFFECTI")
    
    diffDataFrame(sqldf['STATUSREASON'],dfPEICSV['STATUSREASON'],"STATUSREASON")
    diffDataFrame(sqldf['STOCKNO'],dfPEICSV['STOCKNO'],"STOCKNO")
    
    diffDataFrame(pd.to_datetime(sqldf['SUMMARYEFFECT'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['SUMMARYEFFECT'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"SUMMARYEFFECT")
    #diffDataFrame(sqldf['SUMMARYEFFECT'],dfPEICSV['SUMMARYEFFECT'],"SUMMARYEFFECT")
    
    diffDataFrame(sqldf['SUMMARYSTATUS'],dfPEICSV['SUMMARYSTATUS'],"SUMMARYSTATUS")
    
    #diffDataFrame(sqldf['TRANSSTATUSDA'],dfPEICSV['TRANSSTATUSDA'],"TRANSSTATUSDA")
    diffDataFrame(pd.to_datetime(sqldf['TRANSSTATUSDA'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d/%m/%Y'),pd.to_datetime(dfPEICSV['TRANSSTATUSDA'],format='%d-%b-%Y').dt.strftime('%d/%m/%Y'),"TRANSSTATUSDA")
    html_logger.info("End of Comparing CSV with LandingZone")
    
def LZtoXML(date1):
    html_logger.info("Start of Comparing XML with Landing Zone")
    
    XMLDF = getXML()
    #date1 = str('2020-01-13')
    sql2 = '''SELECT
            A.PAYMENTMETHOD,A.GROSSPAYMENTA_MONAMT,A.payeefullname,A.PAYMENTADD1,A.PAYMENTADD2,A.PAYMENTADD3,A.PAYMENTPOSTCO,A.PAYMENTADD4,A.PAYMENTADD6,A.PAYEEBANKSORT,A.PAYEEACCOUNTN,A.PAYEEACCOUNTT,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID
            FROM LZ.PEI_RQST_INFO A
            INNER JOIN FAI.PAY_BATCH_DTL B ON ( Convert(Date,B.LAST_UPDT_TS) =\'''' + date1 + '''\')
            INNER JOIN FAI.PAY_RQST_INFO C ON (B.PAY_RQST_INFO_GEN_ID = C.PAY_RQST_INFO_GEN_ID AND Convert(Date,C.LAST_UPDT_TS)= (\'''' + date1 + '''\') AND C.PAY_AMT = A.GROSSPAYMENTA_MONAMT)'''         #WHERE A.payeefullname LIKE CONCAT('%', C.PAYEE_NAME)'''
    
    sql11 = '''select A.PAYMENTMETHOD,A.GROSSPAYMENTA_MONAMT,A.payeefullname,A.PAYMENTADD1,A.PAYMENTADD2,A.PAYMENTADD3,A.PAYMENTPOSTCO,A.PAYMENTADD4,A.PAYMENTADD6,A.PAYEEBANKSORT,A.PAYEEACCOUNTN,A.PAYEEACCOUNTT,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID
            from FINEOS.PEI_RQST_INFO_VW  A where PEI_I in (select Index_id from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))
            and PEI_C in (select class_ID from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))'''        
    
    sql1 = '''select A.PAYMENTMETHOD,A.GROSSPAYMENTA_MONAMT,A.payeefullname,A.PAYMENTADD1,A.PAYMENTADD2,A.PAYMENTADD3,A.PAYMENTPOSTCO,A.PAYMENTADD4,
            A.PAYMENTADD6,A.PAYEEBANKSORT,A.PAYEEACCOUNTN,A.PAYEEACCOUNTT,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID
            from FINEOS.PEI_RQST_INFO_VW  A where Convert(Date,SOURCE_LASTUPDATEDATE) = \'''' + date1 + '''\''''
    
            
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10667,4242' +';'
                      'Database=' + 'ENTPRS_LANDINGZONE' + ';'
                      'Trusted_Connection=yes;')
    
    sqldf = pd.read_sql_query(sql1,conn)
    #df_sql = sqlQuery[sqlQuery['Pay_Rqst_Nbr'].isin(xmlDF['Pay_Rqst_Nbr'])]

    #html_logger.dbg("Pay_Admin_Sys result is " + str((XMLDF['Pay_Admin_Sys'] == 'FINEOS').all()))
    #html_logger.dbg("Cmpny_Cd result is " + str((XMLDF['Cmpny_Cd'] == 'AUL').all()))
    #html_logger.dbg("Mail sort code result is " + str((XMLDF['Mail_Sort_Cd'] == 'MA').all()))
    #print("ENT Operator User id result is " + str((XMLDF['Ent_Oper_Usr_Id'] == 'Karen').all()))
    
    diffDataFrameString(XMLDF['Pay_Admin_Sys'],"FINEOS","Pay Admin Sys")
    diffDataFrameString(XMLDF['Cmpny_Cd'],"AUL","Cmpny_Cd")
    diffDataFrameString(XMLDF['Mail_Sort_Cd'],"MA","Mail_Sort_Cd")
    
    sqldf['srcCD'] = sqldf['PAYMENTMETHOD'].apply(lambda x: '8DCLC' if x == 'Check' else '8DCLE') 
    sqldf['payType'] = sqldf['PAYMENTMETHOD'].apply(lambda x: 'Check' if x == 'Check' else 'EFT') 
    
    tmpsqldf = sqldf.sort_values(by=['srcCD'])
    tmpsqldf1 = sqldf.sort_values(by=['payType'])
    tmpsqldf2 = sqldf
    tmpsqldf2['GROSSPAYMENTA_MONAMT'] = sqldf['GROSSPAYMENTA_MONAMT'].astype(float)
    tmpsqldf2 = tmpsqldf2.sort_values(by=['GROSSPAYMENTA_MONAMT'],ascending=True)
    #----------- To Change 
    tmpsqldf4 = tmpsqldf2
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Check', 'PAYEEBANKSORT'] = None   #A.PAYEEBANKSORT,A.PAYEEACCOUNTN
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Check', 'PAYEEACCOUNTN'] = None
    
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Elec Funds Transfer', 'PAYMENTADD1'] = None
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Elec Funds Transfer', 'PAYMENTADD2'] = None
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Elec Funds Transfer', 'PAYMENTADD3'] = None
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Elec Funds Transfer', 'PAYMENTPOSTCO'] = None
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Elec Funds Transfer', 'PAYMENTADD4'] = None
    tmpsqldf4.loc[tmpsqldf4.PAYMENTMETHOD == 'Elec Funds Transfer', 'PAYMENTADD6'] = None
    #A.PAYMENTADD1,A.PAYMENTADD2,A.PAYMENTADD3,A.PAYMENTPOSTCO,A.PAYMENTADD4,A.PAYMENTADD6
   # df.loc[df.PAYMENTMETHOD == 'Check', 'PAYEEBANKSORT'] = "" 
    
    tmpsqldf3 = sqldf.sort_values(by=['ENT_OP_ID']).reset_index(drop=True)
       
    tmpXMLDF = XMLDF.sort_values(by=['Src_Cd'])
    tmpXMLDF2 = XMLDF
    tmpXMLDF2['Pay_Amt'] = tmpXMLDF2['Pay_Amt'].astype(float)
    tmpXMLDF2 = tmpXMLDF2.sort_values(by=['Pay_Amt'],ascending=True)
    tmpXMLDF3 = XMLDF.sort_values(by=['Ent_Oper_Usr_Id']).reset_index(drop=True)
    
    diffDataFrame(tmpsqldf['srcCD'],tmpXMLDF['Src_Cd'],"SRC_CD")
    diffDataFrame(tmpsqldf['payType'],tmpXMLDF['Pay_Typ'],"Payment Type")
    diffDataFrame(XMLDF['Lgcy_Src_Cd'],XMLDF['Src_Cd'],"Legacy source code")
    diffDataFrame(tmpXMLDF2['Pay_Amt'],tmpsqldf2['GROSSPAYMENTA_MONAMT'],"Payment amount")
    diffDataFrame(tmpXMLDF2['Chk_Addr_Line_1'],tmpsqldf4['PAYMENTADD1'],"Address Line 1")
    diffDataFrame(tmpXMLDF2['Chk_Addr_Line_2'],tmpsqldf4['PAYMENTADD2'],"Address Line 2")
    diffDataFrame(tmpXMLDF2['Chk_Addr_Zip_Cd'],tmpsqldf4['PAYMENTPOSTCO'],"Address ZipCd")
    diffDataFrame(tmpXMLDF2['Chk_Addr_Line_3'],tmpsqldf4['PAYMENTADD3'],"Address Line 3")
    diffDataFrame(tmpXMLDF2['Chk_Addr_City'],tmpsqldf4['PAYMENTADD4'],"Address City")
    diffDataFrame(tmpXMLDF2['Chk_Addr_St'],tmpsqldf4['PAYMENTADD6'],"Address State")
    diffDataFrame(tmpXMLDF2['Rdfi_Rout_Nbr'],tmpsqldf4['PAYEEBANKSORT'],"PAYEEBANK SORT")
    diffDataFrame(tmpXMLDF2['Rdfi_Acct_Nbr'],tmpsqldf4['PAYEEACCOUNTN'],"PAYEE ACCOUNT Number")
    diffDataFrame(tmpXMLDF2['Rdfi_Acct_Typ'],tmpsqldf4['PAYEEACCOUNTT'],"PAYEE ACCOUNT Type")
    diffDataFrame(tmpXMLDF3['Ent_Oper_Usr_Id'],tmpsqldf3['ENT_OP_ID'],"ENT Operator ID")
    
    html_logger.info("End of Comparing XML with Landing Zone")


def ODStoXML(date1):
    html_logger.info("Start of Comparing XML with ODS")
    XMLDF = getXML()
    
    sql1 = '''select PAY_RQST_NBR,PAY_ADMIN_SYS,CMPNY_CD,SRC_CD,LGCY_SRC_CD,PAY_TYP_CD,PAY_AMT,PAYEE_NAME,CHK_ADDR_LINE_1,CHK_ADDR_LINE_2,CHK_ADDR_CITY,CHK_ADDR_ST,CHK_ADDR_ZIP_CD,MAIL_SORT_CD,ENT_OPER_USR_ID
            from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\')'''

    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10746,4242' +';'
                      'Database=' + 'ENTPRS_CLAIMS_ODS' + ';'
                      'Trusted_Connection=yes;')
    
    sqldf = pd.read_sql_query(sql1,conn)    
    tmpsqldf = sqldf.sort_values(by=['PAY_RQST_NBR'])
    tmpXMLDF = XMLDF.sort_values(by=['Pay_Rqst_Nbr'])
    
    tmpsqldf2 = tmpsqldf
    tmpXMLDF2 = tmpXMLDF
    tmpsqldf2['PAY_AMT'] = sqldf['PAY_AMT'].astype(float)
    tmpXMLDF2['Pay_Amt'] = XMLDF['Pay_Amt'].astype(float)
    
    diffDataFrame(tmpsqldf['PAY_RQST_NBR'],tmpXMLDF['Pay_Rqst_Nbr'],"PAY_RQST_NBR")
    diffDataFrame(tmpsqldf['PAY_ADMIN_SYS'],tmpXMLDF['Pay_Admin_Sys'],"PAY_ADMIN_SYS")
    diffDataFrame(tmpsqldf['CMPNY_CD'],tmpXMLDF['Cmpny_Cd'],"CMPNY_CD")
    diffDataFrame(tmpsqldf['SRC_CD'],tmpXMLDF['Src_Cd'],"SRC_CD")
    diffDataFrame(tmpsqldf['LGCY_SRC_CD'],tmpXMLDF['Lgcy_Src_Cd'],"LGCY_SRC_CD")
    diffDataFrame(tmpsqldf['PAY_TYP_CD'],tmpXMLDF['Pay_Typ'],"PAY_TYP")
    
    diffDataFrame(tmpsqldf2['PAY_AMT'],tmpXMLDF2['Pay_Amt'],"PAY_AMT")
    diffDataFrame(tmpsqldf['PAYEE_NAME'],tmpXMLDF['Payee_Name'],"PAYEE_NAME")
    diffDataFrame(tmpsqldf['CHK_ADDR_LINE_1'],tmpXMLDF['Chk_Addr_Line_1'],"CHK_ADDR_LINE_1")
    diffDataFrame(tmpsqldf['CHK_ADDR_LINE_2'],tmpXMLDF['Chk_Addr_Line_2'],"CHK_ADDR_LINE_2")
    diffDataFrame(tmpsqldf['CHK_ADDR_CITY'],tmpXMLDF['Chk_Addr_City'],"CHK_ADDR_CITY")
    diffDataFrame(tmpsqldf['CHK_ADDR_ST'],tmpXMLDF['Chk_Addr_St'],"CHK_ADDR_ST")
    diffDataFrame(tmpsqldf['CHK_ADDR_ZIP_CD'],tmpXMLDF['Chk_Addr_Zip_Cd'],"CHK_ADDR_ZIP_CD")
    diffDataFrame(tmpsqldf['MAIL_SORT_CD'],tmpXMLDF['Mail_Sort_Cd'],"MAIL_SORT_CD")
    diffDataFrame(tmpsqldf['ENT_OPER_USR_ID'],tmpXMLDF['Ent_Oper_Usr_Id'],"ENT_OPER_USR_ID")
    
    html_logger.info("End of Comparing XML with ODS")
    
    
                
def Sync(xpth):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))

def Click(xpth):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).click()
    #syncwait(3)

def Set(xpth,val1):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).send_keys(str(val1))

def syncwait(sec):
    time.sleep(sec)


def CSVtoLZ_ClaimDetail():
    html_logger.info("Start of Comparing Claim Detail CSV with LandingZone")
    XMLDF = getXML()
    
    sql1 = '''select PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,ABSENCECASENU,BENEFITRIGHTT,CLAIMANTAGE,CLAIMANTCUSTO,
             CLAIMANTDOB,CLAIMANTGENDE,CLAIMANTNAME,CLAIMANTRELTO,CLAIMNUMBER,DIAGCODE2,DIAGCODE3,DIAGCODE4,DIAGCODE5,EMPLOYEEID,
             EVENTCAUSE,INCURREDDATE,INSUREDADDRES,INSUREDADDRL1,INSUREDADDRL2,INSUREDADDRL3,INSUREDADDRL4,INSUREDADDRL5,
             INSUREDADDRL6,INSUREDADDRL7,INSUREDAGE,INSUREDCORCOU,INSUREDCORRES,INSUREDCUSTOM,INSUREDDOB,INSUREDEMPLOY,INSUREDFULLNA,
             INSUREDGENDER,INSUREDPOSTCO,INSUREDPREMIS,INSUREDRETIRE,INSUREDSOCNUM,LEAVEPLANID,LEAVEREQUESTI,NOTIFIEDDATE,PAYEEAGEATINC,
             PAYEECASEROLE,PAYEERELTOINS,PRIMARYDIAGNO,PRIMARYMEDICA,DIAG2MEDICALC,DIAG3MEDICALC,DIAG4MEDICALC,DIAG5MEDICALC,PECLASSID,PEINDEXID,DATEINTERFACE
             from LZ.PEI_CLAIM_DTL'''

    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10746,4242' +';'
                      'Database=' + 'ENTPRS_CLAIMS_ODS' + ';'
                      'Trusted_Connection=yes;')
    
    sqldf = pd.read_sql_query(sql1,conn)    
    
    dfPEICSV = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIClaimDetails.csv") 
    
    tmpsqldf = sqldf.sort_values(by=['PEI_I']).reset_index(drop=True)
    tmpdfPEICSV = dfPEICSV.sort_values(by=['I']).reset_index(drop=True)
    
    tmpsqldf['LASTUPDATEDATE'] = tmpsqldf['LASTUPDATEDATE'].astype('datetime64[ns]')    #pd.to_datetime(tmpsqldf['LASTUPDATEDATE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['LASTUPDATEDATE'] = tmpdfPEICSV['LASTUPDATEDATE'].astype('datetime64[ns]') #pd.to_datetime(tmpdfPEICSV['LASTUPDATEDATE'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    tmpsqldf['INCURREDDATE'] = pd.to_datetime(tmpsqldf['INCURREDDATE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['INCURREDDATE'] = pd.to_datetime(tmpdfPEICSV['INCURREDDATE'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    tmpsqldf['INSUREDDOB'] = pd.to_datetime(tmpsqldf['INSUREDDOB'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['INSUREDDOB'] = pd.to_datetime(tmpdfPEICSV['INSUREDDOB'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    tmpsqldf['NOTIFIEDDATE'] = pd.to_datetime(tmpsqldf['NOTIFIEDDATE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['NOTIFIEDDATE'] = pd.to_datetime(tmpdfPEICSV['NOTIFIEDDATE'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    tmpsqldf['DATEINTERFACE'] = pd.to_datetime(tmpsqldf['DATEINTERFACE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['DATEINTERFACE'] = pd.to_datetime(tmpdfPEICSV['DATEINTERFACE'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    diffDataFrame(tmpsqldf['PEI_C'],dfPEICSV['C'],"PEI_C")
    diffDataFrame(tmpsqldf['PEI_I'],dfPEICSV['I'],"PEI_I")
    diffDataFrame(tmpsqldf['LASTUPDATEDATE'],tmpdfPEICSV['LASTUPDATEDATE'],"LASTUPDATEDATE")
    diffDataFrame(tmpsqldf['ABSENCECASENU'],tmpdfPEICSV['ABSENCECASENU'],"ABSENCECASENU")
    diffDataFrame(tmpsqldf['C_OSUSER_UPDATEDBY'],tmpdfPEICSV['C_OSUSER_UPDATEDBY'],"C_OSUSER_UPDATEDBY")
    diffDataFrame(tmpsqldf['I_OSUSER_UPDATEDBY'],tmpdfPEICSV['I_OSUSER_UPDATEDBY'],"I_OSUSER_UPDATEDBY")
    diffDataFrame(tmpsqldf['BENEFITRIGHTT'],tmpdfPEICSV['BENEFITRIGHTT'],"BENEFITRIGHTT")
    diffDataFrame(tmpsqldf['CLAIMANTAGE'],tmpdfPEICSV['CLAIMANTAGE'],"CLAIMANTAGE")
    diffDataFrame(tmpsqldf['CLAIMANTAGE'],tmpdfPEICSV['CLAIMANTAGE'],'CLAIMANTAGE')
    diffDataFrame(tmpsqldf['CLAIMANTCUSTO'],tmpdfPEICSV['CLAIMANTCUSTO'],'CLAIMANTCUSTO')
    diffDataFrame(tmpsqldf['CLAIMANTDOB'],tmpdfPEICSV['CLAIMANTDOB'],'CLAIMANTDOB')
    diffDataFrame(tmpsqldf['CLAIMANTGENDE'],tmpdfPEICSV['CLAIMANTGENDE'],'CLAIMANTGENDE')
    diffDataFrame(tmpsqldf['CLAIMANTNAME'],tmpdfPEICSV['CLAIMANTNAME'],'CLAIMANTNAME')
    diffDataFrame(tmpsqldf['CLAIMANTRELTO'],tmpdfPEICSV['CLAIMANTRELTO'],'CLAIMANTRELTO')
    diffDataFrame(tmpsqldf['CLAIMNUMBER'],tmpdfPEICSV['CLAIMNUMBER'],'CLAIMNUMBER')
    diffDataFrame(tmpsqldf['DIAGCODE2'],tmpdfPEICSV['DIAGCODE2'],'DIAGCODE2')
    diffDataFrame(tmpsqldf['DIAGCODE3'],tmpdfPEICSV['DIAGCODE3'],'DIAGCODE3')
    diffDataFrame(tmpsqldf['DIAGCODE4'],tmpdfPEICSV['DIAGCODE4'],'DIAGCODE4')
    diffDataFrame(tmpsqldf['DIAGCODE5'],tmpdfPEICSV['DIAGCODE5'],'DIAGCODE5')
    diffDataFrame(tmpsqldf['EMPLOYEEID'],tmpdfPEICSV['EMPLOYEEID'],'EMPLOYEEID')
    diffDataFrame(tmpsqldf['EVENTCAUSE'],tmpdfPEICSV['EVENTCAUSE'],'EVENTCAUSE')
    diffDataFrame(tmpsqldf['INCURREDDATE'],tmpdfPEICSV['INCURREDDATE'],'INCURREDDATE')
    diffDataFrame(tmpsqldf['INSUREDADDRES'],tmpdfPEICSV['INSUREDADDRES'].str.replace('\n',' ').replace('\r',' '),'INSUREDADDRES')  #
    diffDataFrame(tmpsqldf['INSUREDADDRL1'],tmpdfPEICSV['INSUREDADDRL1'],'INSUREDADDRL1')
    diffDataFrame(tmpsqldf['INSUREDADDRL2'],tmpdfPEICSV['INSUREDADDRL2'],'INSUREDADDRL2')
    diffDataFrame(tmpsqldf['INSUREDADDRL3'],tmpdfPEICSV['INSUREDADDRL3'],'INSUREDADDRL3')
    diffDataFrame(tmpsqldf['INSUREDADDRL4'],tmpdfPEICSV['INSUREDADDRL4'],'INSUREDADDRL4')
    diffDataFrame(tmpsqldf['INSUREDADDRL5'],tmpdfPEICSV['INSUREDADDRL5'],'INSUREDADDRL5')
    diffDataFrame(tmpsqldf['INSUREDADDRL6'],tmpdfPEICSV['INSUREDADDRL6'],'INSUREDADDRL6')
    diffDataFrame(tmpsqldf['INSUREDADDRL7'],tmpdfPEICSV['INSUREDADDRL7'],'INSUREDADDRL7')
    diffDataFrame(tmpsqldf['INSUREDAGE'],tmpdfPEICSV['INSUREDAGE'],'INSUREDAGE')
    diffDataFrame(tmpsqldf['INSUREDCORCOU'],tmpdfPEICSV['INSUREDCORCOU'],'INSUREDCORCOU')
    diffDataFrame(tmpsqldf['INSUREDCORRES'],tmpdfPEICSV['INSUREDCORRES'],'INSUREDCORRES')
    diffDataFrame(tmpsqldf['INSUREDCUSTOM'].astype(str),tmpdfPEICSV['INSUREDCUSTOM'].astype(str),'INSUREDCUSTOM')  #
    diffDataFrame(tmpsqldf['INSUREDDOB'],tmpdfPEICSV['INSUREDDOB'],'INSUREDDOB')
    diffDataFrame(tmpsqldf['INSUREDEMPLOY'],tmpdfPEICSV['INSUREDEMPLOY'],'INSUREDEMPLOY')
    diffDataFrame(tmpsqldf['INSUREDFULLNA'],tmpdfPEICSV['INSUREDFULLNA'],'INSUREDFULLNA')
    diffDataFrame(tmpsqldf['INSUREDGENDER'],tmpdfPEICSV['INSUREDGENDER'],'INSUREDGENDER')
    diffDataFrame(tmpsqldf['INSUREDPOSTCO'],tmpdfPEICSV['INSUREDPOSTCO'],'INSUREDPOSTCO')
    diffDataFrame(tmpsqldf['INSUREDPREMIS'],tmpdfPEICSV['INSUREDPREMIS'],'INSUREDPREMIS')
    diffDataFrame(tmpsqldf['INSUREDRETIRE'],tmpdfPEICSV['INSUREDRETIRE'],'INSUREDRETIRE')
    
    tmpsqldf['INSUREDSOCNUM'] = tmpsqldf['INSUREDSOCNUM'].astype(str).str.zfill(9)
    tmpdfPEICSV['INSUREDSOCNUM'] = tmpdfPEICSV['INSUREDSOCNUM'].astype(str).str.zfill(9)   
    diffDataFrame(tmpsqldf['INSUREDSOCNUM'].astype(str),tmpdfPEICSV['INSUREDSOCNUM'].astype(str),'INSUREDSOCNUM')  #
    
    diffDataFrame(tmpsqldf['LEAVEPLANID'],tmpdfPEICSV['LEAVEPLANID'],'LEAVEPLANID')
    diffDataFrame(tmpsqldf['LEAVEREQUESTI'],tmpdfPEICSV['LEAVEREQUESTI'],'LEAVEREQUESTI')
    diffDataFrame(tmpsqldf['NOTIFIEDDATE'],tmpdfPEICSV['NOTIFIEDDATE'],'NOTIFIEDDATE')
    diffDataFrame(tmpsqldf['PAYEEAGEATINC'],tmpdfPEICSV['PAYEEAGEATINC'],'PAYEEAGEATINC')
    diffDataFrame(tmpsqldf['PAYEECASEROLE'],tmpdfPEICSV['PAYEECASEROLE'],'PAYEECASEROLE')
    diffDataFrame(tmpsqldf['PAYEERELTOINS'],tmpdfPEICSV['PAYEERELTOINS'],'PAYEERELTOINS')
    diffDataFrame(tmpsqldf['PRIMARYDIAGNO'],tmpdfPEICSV['PRIMARYDIAGNO'],'PRIMARYDIAGNO')
    diffDataFrame(tmpsqldf['PRIMARYMEDICA'],tmpdfPEICSV['PRIMARYMEDICA'],'PRIMARYMEDICA')
    diffDataFrame(tmpsqldf['DIAG2MEDICALC'],tmpdfPEICSV['DIAG2MEDICALC'],'DIAG2MEDICALC')
    diffDataFrame(tmpsqldf['DIAG3MEDICALC'],tmpdfPEICSV['DIAG3MEDICALC'],'DIAG3MEDICALC')
    diffDataFrame(tmpsqldf['DIAG4MEDICALC'],tmpdfPEICSV['DIAG4MEDICALC'],'DIAG4MEDICALC')
    diffDataFrame(tmpsqldf['DIAG5MEDICALC'],tmpdfPEICSV['DIAG5MEDICALC'],'DIAG5MEDICALC')
    diffDataFrame(tmpsqldf['PECLASSID'],tmpdfPEICSV['PECLASSID'],'PECLASSID')
    diffDataFrame(tmpsqldf['PEINDEXID'],tmpdfPEICSV['PEINDEXID'],'PEINDEXID')
    diffDataFrame(tmpsqldf['DATEINTERFACE'],tmpdfPEICSV['DATEINTERFACE'],'DATEINTERFACE')

    
#extractDetails()
##ValidatePaymentPEI()

#---------------------Start of execution-----------------------------------

def CSVtoLZ_PaymentDetail():
    html_logger.info("Start of Comparing Payment Detail CSV with LandingZone")
    XMLDF = getXML()
    
    sql1 = '''select PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,BENEFITEFFECT,BENEFITFINALP,DESCRIPTION_PAYMENTDTLS,PAYMENTENDPER,PAYMENTSTARTP,
            BALANCINGAMOU_MONAMT,BALANCINGAMOU_MONCUR,BUSINESSNETBE_MONAMT,BUSINESSNETBE_MONCUR,DUETYPE,GROUPID,PECLASSID,PEINDEXID,CLAIMDETAILSCLASSID,
            CLAIMDETAILSINDEXID,DATEINTERFACE,LAST_UPDT_ID,LAST_UPDT_TS from LZ.PEI_PAY_DTL'''

    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10746,4242' +';'
                      'Database=' + 'ENTPRS_CLAIMS_ODS' + ';'
                      'Trusted_Connection=yes;')
    
    sqldf = pd.read_sql_query(sql1,conn)    
    
    dfPEICSV = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentDetails.csv") 
    
    tmpsqldf = sqldf.sort_values(by=['PEI_I']).reset_index(drop=True)
    tmpdfPEICSV = dfPEICSV.sort_values(by=['I']).reset_index(drop=True)
    
    tmpsqldf['LASTUPDATEDATE'] = pd.to_datetime(tmpsqldf['LASTUPDATEDATE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['LASTUPDATEDATE'] = pd.to_datetime(tmpdfPEICSV['LASTUPDATEDATE'],format='%Y-%m-%d').dt.strftime('%d-%m-%y')
    
    tmpsqldf['BENEFITEFFECT'] = pd.to_datetime(tmpsqldf['BENEFITEFFECT'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['BENEFITEFFECT'] = pd.to_datetime(tmpdfPEICSV['BENEFITEFFECT'],format='%Y-%m-%d').dt.strftime('%d-%m-%y')
    
    tmpsqldf['BENEFITFINALP'] = pd.to_datetime(tmpsqldf['BENEFITFINALP'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['BENEFITFINALP'] = pd.to_datetime(tmpdfPEICSV['BENEFITFINALP'],format='%Y-%m-%d').dt.strftime('%d-%m-%y')
    
    tmpsqldf['PAYMENTENDPER'] = pd.to_datetime(tmpsqldf['PAYMENTENDPER'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['PAYMENTENDPER'] = pd.to_datetime(tmpdfPEICSV['PAYMENTENDPER'],format='%Y-%m-%d').dt.strftime('%d-%m-%y')
    
    tmpsqldf['PAYMENTSTARTP'] = pd.to_datetime(tmpsqldf['PAYMENTSTARTP'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['PAYMENTSTARTP'] = pd.to_datetime(tmpdfPEICSV['PAYMENTSTARTP'],format='%Y-%m-%d').dt.strftime('%d-%m-%y')
    
    tmpsqldf['DATEINTERFACE'] = pd.to_datetime(tmpsqldf['DATEINTERFACE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['DATEINTERFACE'] = pd.to_datetime(tmpdfPEICSV['DATEINTERFACE'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    diffDataFrame(tmpsqldf['PEI_C'],dfPEICSV['C'],"PEI_C")
    diffDataFrame(tmpsqldf['PEI_I'],dfPEICSV['I'],"PEI_I")
    diffDataFrame(tmpsqldf['LASTUPDATEDATE'],tmpdfPEICSV['LASTUPDATEDATE'],"LASTUPDATEDATE")
    diffDataFrame(tmpsqldf['C_OSUSER_UPDATEDBY'],tmpdfPEICSV['C_OSUSER_UPDATEDBY'],"C_OSUSER_UPDATEDBY")
    diffDataFrame(tmpsqldf['I_OSUSER_UPDATEDBY'],tmpdfPEICSV['I_OSUSER_UPDATEDBY'],"I_OSUSER_UPDATEDBY")
    diffDataFrame(tmpsqldf['BENEFITEFFECT'],tmpdfPEICSV['BENEFITEFFECT'],"BENEFITEFFECT")
    diffDataFrame(tmpsqldf['BENEFITFINALP'],tmpdfPEICSV['BENEFITFINALP'],"BENEFITFINALP")
    diffDataFrame(tmpsqldf['DESCRIPTION_PAYMENTDTLS'],tmpdfPEICSV['DESCRIPTION_PAYMENTDTLS'],'DESCRIPTION_PAYMENTDTLS')
    diffDataFrame(tmpsqldf['PAYMENTENDPER'],tmpdfPEICSV['PAYMENTENDPER'],'PAYMENTENDPER')
    diffDataFrame(tmpsqldf['PAYMENTSTARTP'],tmpdfPEICSV['PAYMENTSTARTP'],'PAYMENTSTARTP')
    diffDataFrame(tmpsqldf['BALANCINGAMOU_MONAMT'],tmpdfPEICSV['BALANCINGAMOU_MONAMT'],'BALANCINGAMOU_MONAMT')
    
    diffDataFrame(tmpsqldf['BALANCINGAMOU_MONCUR'],tmpdfPEICSV['BALANCINGAMOU_MONCUR'],'BALANCINGAMOU_MONCUR')
    diffDataFrame(tmpsqldf['BUSINESSNETBE_MONAMT'],tmpdfPEICSV['BUSINESSNETBE_MONAMT'],'BUSINESSNETBE_MONAMT')
    diffDataFrame(tmpsqldf['BUSINESSNETBE_MONCUR'],tmpdfPEICSV['BUSINESSNETBE_MONCUR'],'BUSINESSNETBE_MONCUR')
    diffDataFrame(tmpsqldf['DUETYPE'],tmpdfPEICSV['DUETYPE'],'DUETYPE')
    diffDataFrame(tmpsqldf['GROUPID'],tmpdfPEICSV['GROUPID'],'GROUPID')
    diffDataFrame(tmpsqldf['PECLASSID'],tmpdfPEICSV['PECLASSID'],'PECLASSID')
    diffDataFrame(tmpsqldf['PEINDEXID'],tmpdfPEICSV['PEINDEXID'],'PEINDEXID')
    diffDataFrame(tmpsqldf['CLAIMDETAILSCLASSID'],tmpdfPEICSV['CLAIMDETAILSCLASSID'],'CLAIMDETAILSCLASSID')
    diffDataFrame(tmpsqldf['CLAIMDETAILSINDEXID'],tmpdfPEICSV['CLAIMDETAILSINDEXID'],'CLAIMDETAILSINDEXID')
    diffDataFrame(tmpsqldf['DATEINTERFACE'],tmpdfPEICSV['DATEINTERFACE'],'DATEINTERFACE')
    

    
#extractDetails()
##ValidatePaymentPEI()

#---------------------Start of execution-----------------------------------

def CSVtoLZ_PaymentLine():
    html_logger.info("Start of Comparing Payment Line CSV with LandingZone")
    XMLDF = getXML()
    
    sql1 = '''select PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,AMOUNT_MONAMT,AMOUNT_MONCUR,INTEGRTYPE,LINETYPE,REFERENCE,RESERVECATEGO,RESERVETYPE,
            SEQUENCENUMBE,SUBTOTALS,TAXABLEINCOME,USETOCALCRULE,C_PYMNTEIF_PAYMENTLINES,I_PYMNTEIF_PAYMENTLINES,PAYMENTDETAILCLASSID,PAYMENTDETAILINDEXID,
            PURCHASEDETAILCLASSID,PURCHASEDETAILINDEXID,DATEINTERFACE
            from LZ.PEI_PAY_LINE p'''

    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + 'SQLD10746,4242' +';'
                      'Database=' + 'ENTPRS_CLAIMS_ODS' + ';'
                      'Trusted_Connection=yes;')
    
    sqldf = pd.read_sql_query(sql1,conn)    
    
    dfPEICSV = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentLine.csv") 
    
    tmpsqldf = sqldf.sort_values(by=['PEI_I']).reset_index(drop=True)
    tmpdfPEICSV = dfPEICSV.sort_values(by=['I']).reset_index(drop=True)
    
    tmpsqldf['LASTUPDATEDATE'] = pd.to_datetime(tmpsqldf['LASTUPDATEDATE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['LASTUPDATEDATE'] = pd.to_datetime(tmpdfPEICSV['LASTUPDATEDATE'],format='%Y-%m-%d').dt.strftime('%d-%m-%y')

    tmpsqldf['DATEINTERFACE'] = pd.to_datetime(tmpsqldf['DATEINTERFACE'],format='%y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%y')
    tmpdfPEICSV['DATEINTERFACE'] = pd.to_datetime(tmpdfPEICSV['DATEINTERFACE'],format='%d-%b-%Y').dt.strftime('%d-%m-%y')
    
    diffDataFrame(tmpsqldf['PEI_C'],dfPEICSV['C'],"PEI_C")
    diffDataFrame(tmpsqldf['PEI_I'],dfPEICSV['I'],"PEI_I")
    diffDataFrame(tmpsqldf['LASTUPDATEDATE'],tmpdfPEICSV['LASTUPDATEDATE'],"LASTUPDATEDATE")
    diffDataFrame(tmpsqldf['C_OSUSER_UPDATEDBY'],tmpdfPEICSV['C_OSUSER_UPDATEDBY'],"C_OSUSER_UPDATEDBY")
    diffDataFrame(tmpsqldf['I_OSUSER_UPDATEDBY'],tmpdfPEICSV['I_OSUSER_UPDATEDBY'],"I_OSUSER_UPDATEDBY")
    diffDataFrame(tmpsqldf['AMOUNT_MONAMT'],tmpdfPEICSV['AMOUNT_MONAMT'],"AMOUNT_MONAMT")
    diffDataFrame(tmpsqldf['AMOUNT_MONCUR'],tmpdfPEICSV['AMOUNT_MONCUR'],"AMOUNT_MONCUR")
    diffDataFrame(tmpsqldf['INTEGRTYPE'],tmpdfPEICSV['INTEGRTYPE'],'INTEGRTYPE')
    diffDataFrame(tmpsqldf['LINETYPE'],tmpdfPEICSV['LINETYPE'],'LINETYPE')
    diffDataFrame(tmpsqldf['REFERENCE'],tmpdfPEICSV['REFERENCE'],'REFERENCE')
    diffDataFrame(tmpsqldf['RESERVECATEGO'],tmpdfPEICSV['RESERVECATEGO'],'RESERVECATEGO')
    
    diffDataFrame(tmpsqldf['RESERVETYPE'],tmpdfPEICSV['RESERVETYPE'],'RESERVETYPE')
    diffDataFrame(tmpsqldf['SEQUENCENUMBE'],tmpdfPEICSV['SEQUENCENUMBE'],'SEQUENCENUMBE')
    diffDataFrame(tmpsqldf['SUBTOTALS'],tmpdfPEICSV['SUBTOTALS'],'SUBTOTALS')
    diffDataFrame(tmpsqldf['TAXABLEINCOME'],tmpdfPEICSV['TAXABLEINCOME'],'TAXABLEINCOME')
    diffDataFrame(tmpsqldf['USETOCALCRULE'],tmpdfPEICSV['USETOCALCRULE'],'USETOCALCRULE')
    diffDataFrame(tmpsqldf['C_PYMNTEIF_PAYMENTLINES'],tmpdfPEICSV['C_PYMNTEIF_PAYMENTLINES'],'C_PYMNTEIF_PAYMENTLINES')
    diffDataFrame(tmpsqldf['I_PYMNTEIF_PAYMENTLINES'],tmpdfPEICSV['I_PYMNTEIF_PAYMENTLINES'],'I_PYMNTEIF_PAYMENTLINES')
    diffDataFrame(tmpsqldf['PAYMENTDETAILCLASSID'],tmpdfPEICSV['PAYMENTDETAILCLASSID'],'PAYMENTDETAILCLASSID')
    diffDataFrame(tmpsqldf['PAYMENTDETAILINDEXID'],tmpdfPEICSV['PAYMENTDETAILINDEXID'],'PAYMENTDETAILINDEXID')
    diffDataFrame(tmpsqldf['PURCHASEDETAILCLASSID'],tmpdfPEICSV['PURCHASEDETAILCLASSID'],'PURCHASEDETAILCLASSID')
    diffDataFrame(tmpsqldf['PURCHASEDETAILINDEXID'],tmpdfPEICSV['PURCHASEDETAILINDEXID'],'PURCHASEDETAILINDEXID')
    diffDataFrame(tmpsqldf['DATEINTERFACE'],tmpdfPEICSV['DATEINTERFACE'],'DATEINTERFACE')
    

    
#extractDetails()
##ValidatePaymentPEI()

#---------------------Start of execution-----------------------------------



'''
Extract the payment details from Fineos 
Application for the following Case numbers

'''
#extractDetails(['DI-482'])  #'DI-327','DI-383' 'DI-476','DI-477',



'''
These are set of Test cases to Validate Payment Process
    * PEI extracts(CSV's) will be sent by FIneos
    * CSV will be Loaded in LandingZone
    * Landinzone is processed and moved to ODS
    * Disbursement XMl is created from ODS

These scripts validate 
1) PEICSV with LandingZone
2) LandingZone with XML  - Date of Extract has to be Passed 
'''


print("Start of Execution")
StrDate = '2020-02-04'
LZDate = '2020-02-03'

ODStoXML(StrDate)
LZtoXML(LZDate)     # XML to LZ
peiCSVtoLZ()
CSVtoLZ_ClaimDetail()
CSVtoLZ_PaymentDetail()
CSVtoLZ_PaymentLine()

try:
    driver.close()
except:
    pass    
print("End of Execution")













