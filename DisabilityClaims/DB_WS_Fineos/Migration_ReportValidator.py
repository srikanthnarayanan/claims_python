# coding=utf8
import csv
import datetime
import logging
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
import sys
import time
import unittest
import re

from Payment_ReusableArtifacts import Comparator, Parser, DFCompare
import Payment_ReusableArtifacts
from html_logger import setup, info
import html_logger
import numpy as np 
import pandas as pd
import xml.etree.ElementTree as ET
from jinja2.ext import do
from datetime import datetime
from datetime import timedelta as td
import _struct
from collections import Counter
from bleach._vendor.html5lib._ihatexml import name
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


#from datetime import date


#from test import support
now = datetime.now()
parentFolder = "G:\\Eclipise_workspace_10142020\\Python_Projects\\" #"C:\\Users\\t003320\\eclipse-workspace\\Python_Projects\\"
#sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

#driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  
timenow = str(now.isoformat().replace(":","_"))
LOG_FILENAME = parentFolder + "DisabilityClaims\\Claims_ReportValidation\\Results\\ReportValidation_"+ timenow + ".html"
pass_FileName =  parentFolder + "DisabilityClaims\\Claims_ReportValidation\\Results\\Pass_"+ timenow + ".csv"
fail_FileName =  parentFolder + "DisabilityClaims\\Claims_ReportValidation\\Results\\Fail_"+ timenow + ".csv"
setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
caseno = ''
Pass_df = pd.DataFrame(data = {'CaseNo':[],'Status':[]})
Fail_df = pd.DataFrame(data = {'CaseNo':[],'ColumnName':[],'FinVal':[],'ArenaVal':[]})

#Fail_df.append({'CaseNo':caseno, 'ColumnName':fieldval,'FinVal':finval, 'ArenaVal':xlval}, ignore_index=True)
caseStatus = 1

    
def report1():
    dbdata = pd.read_excel('C:\\Srikanth\\GL_ACCounting_detail_database_report.xls', sheet_name = 0)  #,encoding='cp437'  ,errors = 'ignore'
    print(dbdata.head(5))
    
    rptdata = pd.read_excel('C:\\Srikanth\\GL_ACCounting_detail_database_report.xls', sheet_name = 1)  #,encoding='cp437'  ,errors = 'ignore'
    print(rptdata.head(5))
    
    if(dbdata.equals(rptdata)):
        html_logger.dbg("ROw and column count matches")
    else:
        df_1notin2 = dbdata[~(dbdata['Group Main#'].isin(rptdata['Group Main#']) & dbdata['Account Number'].isin(rptdata['Account Number']))].reset_index(drop=True)
        html_logger.err("ROw and column count doesn't match. row count of dbdata is " + str(dbdata.shape[0]) + " but the row count of Report is " + str(rptdata.shape[0]))
        html_logger.err(df_1notin2.to_string())
        
    cols = [['Group Main#','Group Main#','String'],['Account Number','Account Number','String'],['JR#','JR#','String'],['GL Amonut','GL Amonut','float'],
            ['D/C','D/C','String'],['Cycle Date','Cycle Date','String'],['Member Name','Member Name','String'],['Branch','Branch','String'],['Dept','Dept','String'],
            ['Cert Number','Cert Number','String'],['State Code','State Code','String'],['Claim Office','Claim Office','String']]
    
    DFCompare.compare(dbdata,rptdata,cols)


def report2():
    sql1 = """SELECT distinct GRP_NBR AS 'Group Main',ACCT_NBR AS 'Account Number',JRNL_NBR AS 'JR_Num',PAY_AMT AS 'GL Amonut',DRCR_IND AS 'D_C',LAST_UPDT_TS AS 'Cycle Date'
        ,MBR_NAME AS 'Member Name',SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch',RIGHT([POLICY_NUMBER], 3) AS 'Dept',CERT_NBR AS 'Cert Number'
        ,ST_CD AS 'State Code',CLAIM_OFF_CD AS 'Claim Office' FROM [ENTPRS_CLAIMS_ODS].[FAI].[GL_ELMNT_DTL]"""
        
    sql11 = """SELECT GRP_NBR AS 'Group Main',ACCT_NBR AS 'Account Number',JRNL_NBR AS 'JR_Num',PAY_AMT AS 'GL Amount',DRCR_IND AS 'D_C'
        ,LAST_UPDT_TS AS 'Cycle Date',MBR_NAME AS 'Member Name',SUBSTRING([POLICY_NUMBER], 10, 4) AS 'Branch',RIGHT([POLICY_NUMBER], 3) AS 'Dept'
        ,CERT_NBR AS 'Cert Number' ,ST_CD AS 'State Code',CLAIM_OFF_CD AS 'Claim Office' FROM [ENTPRS_CLAIMS_ODS].[FAI].[GL_ELMNT_DTL] order by GRP_NBR"""
        
    dbdata = Parser.parseSQL(sql1,"SQLD10746,4242","ENTPRS_CLAIMS_ODS")

    #dbdata = dbdata.sort_values(by=['Group Main', 'Account Number', 'State Code'])
    dbdata['combined'] = dbdata['Group Main'].astype(str) + ";" + dbdata['Account Number'].astype(str) + ";" + dbdata['JR_Num'].astype(str) + ";" +  dbdata['GL Amount'].astype(str) + ";" + dbdata['Member Name'].astype(str) + ";" + dbdata['State Code'].astype(str)
    dbdata = dbdata.reset_index(drop=True) 
    #print(dbdata.head(25))
    
    
    rptdata = pd.read_excel('C:\\Users\\T003320\\eclipse-workspace\\Python_Projects\\DisabilityClaims\\Claims_ReportValidation\\Fineos Daily Accounting Details Report_0902.xls', sheet_name = 0, dtype=str)  #,encoding='cp437'  ,errors = 'ignore'  
    rptdata['Group Main#'] = rptdata['Group Main#'].fillna(method = 'ffill')
    rptdata = rptdata.dropna(axis=0, subset=['Account Number']).reset_index(drop=True)
    rptdata = rptdata.loc[:, ~rptdata.columns.str.contains('^Unnamed')]
    
    #rptdata = rptdata.sort_values(by=['Group Main#', 'Account Number', 'State Code'])
    rptdata = rptdata.reset_index(drop=True)
    rptdata1 = rptdata
    rptdata1['combined'] = rptdata1['Group Main#'].astype(str) + ";" + rptdata1['Account Number'].astype(str) + ";" + rptdata1['JR#'].astype(str) + ";" +  dbdata['GL Amount'].astype(str) + ";" + rptdata1['Member Name'].astype(str) + ";" + rptdata1['State Code'].astype(str) 
    #print(rptdata1.head(25))
    
    
    #------------------
    com_df = pd.concat([dbdata, rptdata1], ignore_index=True)
    
    
    #---------------------
    
    
    if(dbdata.reset_index(drop=True).equals(rptdata1.reset_index(drop=True))):
        html_logger.dbg("ROw and column count matches")
    else:
        dfdiff = rptdata1[~(rptdata1['combined'].isin(dbdata['combined']))]
        html_logger.err("ROw and column count doesn't match. row count of dbdata is " + str(dbdata.shape[0]) + "/"  + str(dbdata.shape[1]) + " but the row count of Report is " + str(rptdata1.shape[0]) + "/"  + str(rptdata1.shape[1]))
        print(dfdiff.head(25))
        
    cols = [['Group Main','Group Main#','float'],['Account Number','Account Number','float'],['JR_Num','JR#','string'],['GL Amount','GL Amount','float'],
            ['D_C','Debit(D) or Credit(C) Indicator','String'],['Cycle Date','Cycle Date','String'],['Member Name','Member Name','String'],['Branch','Branch','float'],['Dept','Dept','float'],
            ['Cert Number','Cert Number','float'],['State Code','State Code','float'],['Claim Office','Claim Office','float']]
    
    DFCompare.compare(dbdata,rptdata1,cols)
 
def  partyDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_DIM"  
    EBEN_SQL = "Select * from base.Party_Dim"
    
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyDim_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
  
    cols = [['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],['PARTY_TYP_CD','PARTY_TYP_CD','String'],['PARTY_TYP_DESCR','PARTY_TYP_DESCR','String'],
            ['PARTY_ROLE_TYP_TXT','PARTY_ROLE_TYP_TXT','String'],['PLCYHLDR_MSTR_NBR','PLCYHLDR_MSTR_NBR','String'],['PLCYHLDR_BR_NBR','PLCYHLDR_BR_NBR','String'],['PLCYHLDR_DEPT_NBR','PLCYHLDR_DEPT_NBR','String'],
            ['CMPNY_PREF_NAME','CMPNY_PREF_NAME','String'],['BILL_CNTCT_NAME','BILL_CNTCT_NAME','String'],['PLCYHLDR_BILL_SUBSYS_CD','PLCYHLDR_BILL_SUBSYS_CD','String'],['PLCYHLDR_BILL_SUBSYS_DESCR','PLCYHLDR_BILL_SUBSYS_DESCR','String'],
            ['RGNL_GRP_OFF_NAME','RGNL_GRP_OFF_NAME','String'],['RGNL_GRP_OFF_NBR','RGNL_GRP_OFF_NBR','String'],['PLCYHLDR_EFF_DT','PLCYHLDR_EFF_DT','String'],['PLCYHLDR_TERM_DT','PLCYHLDR_TERM_DT','String'],
            ['PLCYHLDR_REINSTMT_DT','PLCYHLDR_REINSTMT_DT','String'],['PLCYHLDR_STAT_CD','PLCYHLDR_STAT_CD','String'],['PLCYHLDR_STAT_DESCR','PLCYHLDR_STAT_DESCR','String'],['PREM_PAY_GRACE_PER_CD','PREM_PAY_GRACE_PER_CD','String'],
            ['PREM_PAY_GRACE_PER_DESCR','PREM_PAY_GRACE_PER_DESCR','String'],['PD_THRU_DT','PD_THRU_DT','String'],['PLCYHLDR_FRST_SUBSD_PREF_NAME','PLCYHLDR_FRST_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_SEC_SUBSD_PREF_NAME','PLCYHLDR_SEC_SUBSD_PREF_NAME','String'],['PLCYHLDR_THIRD_SUBSD_PREF_NAME','PLCYHLDR_THIRD_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_FOURTH_SUBSD_PREF_NAME','PLCYHLDR_FOURTH_SUBSD_PREF_NAME','String'],['PLCYHLDR_LEGAL_NAME','PLCYHLDR_LEGAL_NAME','String'],['PLCYHLDR_STD_INDY_CLSFCTN_CD','PLCYHLDR_STD_INDY_CLSFCTN_CD','String'],
            ['COV_TYP_CD','COV_TYP_CD','String'],['ALLNC_CD','ALLNC_CD','String'],['ALLNC_DESCR','ALLNC_DESCR','String'],['EOB_MAIL_RCPNT_TYP_CD','EOB_MAIL_RCPNT_TYP_CD','String'],['EOB_MAIL_RCPNT_TYP_DESCR','EOB_MAIL_RCPNT_TYP_DESCR','String'],
            ['PM_CUR_IND','PM_CUR_IND','String']
]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 
    
def benPlanDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):    
    ETPRS_SQL = "Select * from Ben_Plan_dim"  
    EBEN_SQL = "Select * from base.Ben_Plan_dim"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("benPlanDim_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],
            ['BEN_PLAN_NBR','BEN_PLAN_NBR','String'],['BEN_PLAN_TYP_CD','BEN_PLAN_TYP_CD','String'],['BEN_PLAN_TYP_DESCR','BEN_PLAN_TYP_DESCR','String'],['PLAN_EFF_DT','PLAN_EFF_DT','String'],
            ['BEN_MAX_PER_NBR','BEN_MAX_PER_NBR','String'],['BEN_MAX_PER_TYP_CD','BEN_MAX_PER_TYP_CD','String'],['BEN_MAX_PER_TYP_DESCR','BEN_MAX_PER_TYP_DESCR','String'],
            ['OTH_INCM_INTGRTN_MTHD_CD','OTH_INCM_INTGRTN_MTHD_CD','String'],['OTH_INCM_INTGRTN_MTHD_DESCR','OTH_INCM_INTGRTN_MTHD_DESCR','String'],['DISBLTY_BEN_CONT_PER_CD','DISBLTY_BEN_CONT_PER_CD','String'],
            ['DISBLTY_BEN_CONT_PER_DESCR','DISBLTY_BEN_CONT_PER_DESCR','String'],['IMMED_HOSP_BEN_IND','IMMED_HOSP_BEN_IND','String'],['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','String'],
            ['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','String'],['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','String'],
            ['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','String'],['RTRN_TO_WRK_PER_NBR','RTRN_TO_WRK_PER_NBR','String'],['RTRN_TO_WRK_PER_TYP_CD','RTRN_TO_WRK_PER_TYP_CD','String']
            ,['RTRN_TO_WRK_PER_DESCR','RTRN_TO_WRK_PER_DESCR','String'],['POST_EFF_DT_TRTMT_FREE_PER_NBR','POST_EFF_DT_TRTMT_FREE_PER_NBR','String'],['POST_EFF_DT_TRTMT_FREE_TYP_CD','POST_EFF_DT_TRTMT_FREE_TYP_CD','String'],
            ['POST_EFF_DT_TRTMT_FREE_DESCR','POST_EFF_DT_TRTMT_FREE_DESCR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','String'],
            ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','String'],
            ['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PRE_EXIT_COND_NOT_CVRD_PER_NBR','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','String'],
            ['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','String'],['CVRD_EARN_TYP_CD','CVRD_EARN_TYP_CD','String'],['CVRD_EARN_TYP_DESCR','CVRD_EARN_TYP_DESCR','String'],
            ['FULL_TM_STAT_QLFCTN_THRSHLD_NBR','FULL_TM_STAT_QLFCTN_THRSHLD_NBR','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','String'],
            ['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','String'],['INDV_COV_EFF_TYP_CD','INDV_COV_EFF_TYP_CD','String'],
            ['INDV_COV_EFF_TYP_DESCR','INDV_COV_EFF_TYP_DESCR','String'],['SURV_BEN_PER_NBR','SURV_BEN_PER_NBR','String'],['SURV_BEN_PER_TYP_CD','SURV_BEN_PER_TYP_CD','String'],
            ['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String'],
            ['PM_BATCH_ID','PM_BATCH_ID','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def partyPlanFact_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_PLAN_FACT"  
    EBEN_SQL = "Select * from base.PARTY_PLAN_FACT"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyPlanFact_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PARTY_PLAN_FACT_GEN_ID','PARTY_PLAN_FACT_GEN_ID','String'],['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],
            ['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['ERISA_APPL_CD','ERISA_APPL_CD','String'],
            ['ERISA_APPL_DESCR','ERISA_APPL_DESCR','String'],['PLCYHLDR_PLAN_CLS_CD','PLCYHLDR_PLAN_CLS_CD','String'],['PLCYHLDR_PLAN_CLS_DESCR','PLCYHLDR_PLAN_CLS_DESCR','String'],
            ['NEW_EMP_WAIT_PER_TYP_CD','NEW_EMP_WAIT_PER_TYP_CD','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEW_EMP_WAIT_PER_TYP_DESCR','String'],['NEW_EMP_COV_QLFCTN_TYP_CD','NEW_EMP_COV_QLFCTN_TYP_CD','String'],
            ['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEW_EMP_COV_QLFCTN_TYP_DESCR','String'],['INIT_EMP_WAIT_PER_TYP_CD','INIT_EMP_WAIT_PER_TYP_CD','String'],['INIT_EMP_WAIT_PER_TYP_DESCR','INIT_EMP_WAIT_PER_TYP_DESCR','String'],
            ['MNTL_ILL_LIMIT_IND','MNTL_ILL_LIMIT_IND','String'],['SPL_COND_BEN_DUR_TYP_CD','SPL_COND_BEN_DUR_TYP_CD','String'],['SPL_COND_BEN_DUR_TYP_DESCR','SPL_COND_BEN_DUR_TYP_DESCR','String'],
            ['SPL_DISBLTY_COND_LMT_IND','SPL_DISBLTY_COND_LMT_IND','String'],['SPS_DISBLTY_BEN_ELIM_PER_CD','SPS_DISBLTY_BEN_ELIM_PER_CD','String'],['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','String'],
            ['SPS_DISBLTY_MAX_MTHLY_BEN_AMT','SPS_DISBLTY_MAX_MTHLY_BEN_AMT','String'],['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','String'],['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','String'],
            ['DISBLTY_BEN_FREQ_CD','DISBLTY_BEN_FREQ_CD','String'],['DISBLTY_BEN_FREQ_TYP_CD','DISBLTY_BEN_FREQ_TYP_CD','String'],['DISBLTY_BEN_FREQ_TYP_DESCR','DISBLTY_BEN_FREQ_TYP_DESCR','String'],
            ['COST_OF_LVNG_BEN_ADJ_PER_CD','COST_OF_LVNG_BEN_ADJ_PER_CD','String'],['COST_OF_LVNG_BEN_ADJ_PER_DESCR','COST_OF_LVNG_BEN_ADJ_PER_DESCR','String'],['COST_OF_LVNG_BEN_ADJ_TYP_CD','COST_OF_LVNG_BEN_ADJ_TYP_CD','String'],
            ['DBL_ELIM_PER_IND','DBL_ELIM_PER_IND','String'],['DISBLTY_MAX_BEN_AMT','DISBLTY_MAX_BEN_AMT','String'],['DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','String'],
            ['MAX_COV_BEN_AMT','MAX_COV_BEN_AMT','String'],['DISBLTY_BEN_SLRY_PCT','DISBLTY_BEN_SLRY_PCT','String'],['DISBLTY_MIN_NET_BEN_PCT','DISBLTY_MIN_NET_BEN_PCT','String'],
            ['FRST_DAY_HOSP_BEN_IND','FRST_DAY_HOSP_BEN_IND','String'],['FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','String'],['EMPLR_CONTRIB_PCT','EMPLR_CONTRIB_PCT','String'],
            ['INIT_EMP_COV_QLFCTN_TYP_CD','INIT_EMP_COV_QLFCTN_TYP_CD','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INIT_EMP_COV_QLFCTN_TYP_DESCR','String'],['MIN_COV_BEN_AMT','MIN_COV_BEN_AMT','String'],
            ['MIN_NET_BEN_AMT','MIN_NET_BEN_AMT','String'],['FIX_COV_BEN_AMT','FIX_COV_BEN_AMT','String'],['INSUR_INCR_UNIT_AMT','INSUR_INCR_UNIT_AMT','String'],['DISBLTY_ELIM_PER_QLFCTN_IND','DISBLTY_ELIM_PER_QLFCTN_IND','String'],
            ['COLA_MAX_OCCUR_NBR','COLA_MAX_OCCUR_NBR','String'],['COLA_OCCUR_TYP','COLA_OCCUR_TYP','String'],['COLA_OCCUR_TYP_DESCR','COLA_OCCUR_TYP_DESCR','String'],['COLA_CD','COLA_CD','String'],
            ['COLA_DESCR','COLA_DESCR','String'],['CVRD_EARN_TYP_CD','CVRD_EARN_TYP_CD','String'],['CVRD_EARN_TYP_DESCR','CVRD_EARN_TYP_DESCR','String'],['PREM_WVR_CD','PREM_WVR_CD','String'],
            ['PREM_WVR_DESCR','PREM_WVR_DESCR','String'],['CMPNY_TAX_AGNT_CD','CMPNY_TAX_AGNT_CD','String'],['CMPNY_TAX_AGNT_DESCR','CMPNY_TAX_AGNT_DESCR','String'],['DISBLTY_FLAT_BEN_AMT','DISBLTY_FLAT_BEN_AMT','String'],
            ['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String'],['PM_BATCH_ID','PM_BATCH_ID','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def partyDtlExtDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_DTL_EXT_DIM"  
    EBEN_SQL = "Select * from base.PARTY_DTL_EXT_DIM"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyDtlExtDim_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PARTY_DTL_EXT_DIM_GEN_ID','PARTY_DTL_EXT_DIM_GEN_ID','String'],['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['PARTY_DTL_TYP_CD','PARTY_DTL_TYP_CD','String'],
            ['ADDR_FRST_LINE_TXT','ADDR_FRST_LINE_TXT','String'],['ADDR_SEC_LINE_TXT','ADDR_SEC_LINE_TXT','String'],['ADDR_THIRD_LINE_TXT','ADDR_THIRD_LINE_TXT','String'],
            ['ADDR_CITY_NAME','ADDR_CITY_NAME','String'],['ADDR_ZIP_CD','ADDR_ZIP_CD','String'],['ADDR_ST_CD','ADDR_ST_CD','String'],['ADDR_CNTRY_NAME','ADDR_CNTRY_NAME','String'],
            ['PHONE_NBR','PHONE_NBR','String'],['EMAIL_ADDR_TXT','EMAIL_ADDR_TXT','String'],['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],
            ['PM_EFF_END_TS','PM_EFF_END_TS','String'],['PM_BATCH_ID','PM_BATCH_ID','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 
    
def benPlanDimVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from Ben_Plan_dim_vw"  
    EBEN_SQL = "Select * from base.Ben_Plan_dim_vw"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("benPlanDimVW_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],['BEN_PLAN_NBR','BEN_PLAN_NBR','String']
            ,['BEN_PLAN_TYP_CD','BEN_PLAN_TYP_CD','String'],['BEN_PLAN_TYP_DESCR','BEN_PLAN_TYP_DESCR','String'],['PLAN_EFF_DT','PLAN_EFF_DT','String'],['BEN_MAX_PER_NBR','BEN_MAX_PER_NBR','String'],
            ['BEN_MAX_PER_TYP_DESCR','BEN_MAX_PER_TYP_DESCR','String'],['OTH_INCM_INTGRTN_MTHD_CD','OTH_INCM_INTGRTN_MTHD_CD','String'],['OTH_INCM_INTGRTN_MTHD_DESCR','OTH_INCM_INTGRTN_MTHD_DESCR','String'],
            ['DISBLTY_BEN_CONT_PER_CD','DISBLTY_BEN_CONT_PER_CD','String'],['DISBLTY_BEN_CONT_PER_DESCR','DISBLTY_BEN_CONT_PER_DESCR','String'],['IMMED_HOSP_BEN_IND','IMMED_HOSP_BEN_IND','String'],
            ['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','String'],['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','String'],
            ['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','String'],['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','String'],
            ['RTRN_TO_WRK_PER_NBR','RTRN_TO_WRK_PER_NBR','String'],['RTRN_TO_WRK_PER_TYP_CD','RTRN_TO_WRK_PER_TYP_CD','String'],['RTRN_TO_WRK_PER_DESCR','RTRN_TO_WRK_PER_DESCR','String'],
            ['POST_EFF_DT_TRTMT_FREE_PER_NBR','POST_EFF_DT_TRTMT_FREE_PER_NBR','String'],['POST_EFF_DT_TRTMT_FREE_TYP_CD','POST_EFF_DT_TRTMT_FREE_TYP_CD','String'],['POST_EFF_DT_TRTMT_FREE_DESCR','POST_EFF_DT_TRTMT_FREE_DESCR','String'],
            ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],
            ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','String'],['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PRE_EXIT_COND_NOT_CVRD_PER_NBR','String'],
            ['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','String'],
            ['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def partyPlanFactVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PARTY_PLAN_FACT_VW"  
    EBEN_SQL = "Select * from base.PARTY_PLAN_FACT_VW"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("partyPlanFactVW_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PARTY_PLAN_FACT_GEN_ID','PARTY_PLAN_FACT_GEN_ID','String'],['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['BEN_PLAN_DIM_GEN_ID','BEN_PLAN_DIM_GEN_ID','String'],['PARTY_ADMIN_SYS_NAME','PARTY_ADMIN_SYS_NAME','String'],
            ['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],['PLAN_ADMIN_SYS_NAME','PLAN_ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],
            ['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['ERISA_APPL_CD','ERISA_APPL_CD','String'],['ERISA_APPL_DESCR','ERISA_APPL_DESCR','String'],['PLCYHLDR_PLAN_CLS_CD','PLCYHLDR_PLAN_CLS_CD','String'],
            ['PLCYHLDR_PLAN_CLS_DESCR','PLCYHLDR_PLAN_CLS_DESCR','String'],['NEW_EMP_WAIT_PER_TYP_CD','NEW_EMP_WAIT_PER_TYP_CD','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEW_EMP_WAIT_PER_TYP_DESCR','String'],
            ['NEW_EMP_COV_QLFCTN_TYP_CD','NEW_EMP_COV_QLFCTN_TYP_CD','String'],['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEW_EMP_COV_QLFCTN_TYP_DESCR','String'],['INIT_EMP_WAIT_PER_TYP_CD','INIT_EMP_WAIT_PER_TYP_CD','String'],
            ['INIT_EMP_WAIT_PER_TYP_DESCR','INIT_EMP_WAIT_PER_TYP_DESCR','String'],['MNTL_ILL_LIMIT_IND','MNTL_ILL_LIMIT_IND','String'],['SPL_COND_BEN_DUR_TYP_CD','SPL_COND_BEN_DUR_TYP_CD','String'],
            ['SPL_COND_BEN_DUR_TYP_DESCR','SPL_COND_BEN_DUR_TYP_DESCR','String'],['SPL_DISBLTY_COND_LMT_IND','SPL_DISBLTY_COND_LMT_IND','String'],['SPS_DISBLTY_BEN_ELIM_PER_CD','SPS_DISBLTY_BEN_ELIM_PER_CD','String'],
            ['DISBLTY_BEN_FREQ_CD','DISBLTY_BEN_FREQ_CD','String'],['DISBLTY_BEN_FREQ_TYP_CD','DISBLTY_BEN_FREQ_TYP_CD','String'],['DISBLTY_BEN_FREQ_TYP_DESCR','DISBLTY_BEN_FREQ_TYP_DESCR','String'],
            ['COST_OF_LVNG_BEN_ADJ_PER_CD','COST_OF_LVNG_BEN_ADJ_PER_CD','String'],['COST_OF_LVNG_BEN_ADJ_PER_DESCR','COST_OF_LVNG_BEN_ADJ_PER_DESCR','String'],['COST_OF_LVNG_BEN_ADJ_TYP_CD','COST_OF_LVNG_BEN_ADJ_TYP_CD','String'],
            ['DBL_ELIM_PER_IND','DBL_ELIM_PER_IND','String'],['DISBLTY_MAX_BEN_AMT','DISBLTY_MAX_BEN_AMT','String'],['DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','String'],['MAX_COV_BEN_AMT','MAX_COV_BEN_AMT','String'],
            ['DISBLTY_BEN_SLRY_PCT','DISBLTY_BEN_SLRY_PCT','String'],['DISBLTY_MIN_NET_BEN_PCT','DISBLTY_MIN_NET_BEN_PCT','String'],['FRST_DAY_HOSP_BEN_IND','FRST_DAY_HOSP_BEN_IND','String'],['FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','String'],
            ['EMPLR_CONTRIB_PCT','EMPLR_CONTRIB_PCT','String'],['INIT_EMP_COV_QLFCTN_TYP_CD','INIT_EMP_COV_QLFCTN_TYP_CD','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INIT_EMP_COV_QLFCTN_TYP_DESCR','String'],
            ['MIN_COV_BEN_AMT','MIN_COV_BEN_AMT','String'],['MIN_NET_BEN_AMT','MIN_NET_BEN_AMT','String'],['FIX_COV_BEN_AMT','FIX_COV_BEN_AMT','String'],['INSUR_INCR_UNIT_AMT','INSUR_INCR_UNIT_AMT','String'],
            ['DISBLTY_ELIM_PER_QLFCTN_IND','DISBLTY_ELIM_PER_QLFCTN_IND','String'],['COLA_MAX_OCCUR_NBR','COLA_MAX_OCCUR_NBR','String'],['COLA_OCCUR_TYP','COLA_OCCUR_TYP','String'],['COLA_CD','COLA_CD','String'],
            ['PM_CUR_IND','PM_CUR_IND','String'],['PM_EFF_STRT_TS','PM_EFF_STRT_TS','String'],['PM_EFF_END_TS','PM_EFF_END_TS','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 
    
def pmBatch_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from PM_BATCH"  
    EBEN_SQL = "Select * from base.PM_BATCH"
    
    #-------------------------------------------------------------
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("pmBatch_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
 
    cols = [['PM_BATCH_ID','PM_BATCH_ID','String'],['PM_BATCH_STRT_TS','PM_BATCH_STRT_TS','String'],['PM_BATCH_END_TS','PM_BATCH_END_TS','String'],['PM_BATCH_STAT','PM_BATCH_STAT','String'],
            ['PM_BATCH_SCHEMA_NAME','PM_BATCH_SCHEMA_NAME','String'],['PM_BATCH_MODEL_TYP','PM_BATCH_MODEL_TYP','String'],['INS_TS','INS_TS','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols) 

def entprsPartyVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB):
    ETPRS_SQL = "Select * from ENTPRS_PARTY_VW"  
    EBEN_SQL = "Select * from base.ENTPRS_PARTY_VW"
    
    ETPRS_DF = Parser.parseSQL(ETPRS_SQL,ETPRS_Server,ETPRS_DB)
    EBEN_DF = Parser.parseSQL(EBEN_SQL,EBEN_Server,EBEN_DB)
    
    html_logger.info("entprsPartyVW_Comparison - Row count of ETPRS_DF is " + str(ETPRS_DF.shape[0]) + " but the row count of EBEN_DF is " + str(EBEN_DF.shape[0]))
    
  
    cols = [['PARTY_DIM_GEN_ID','PARTY_DIM_GEN_ID','String'],['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],
            ['PARTY_TYP_CD','PARTY_TYP_CD','String'],['PARTY_TYP_DESCR','PARTY_TYP_DESCR','String'],['PARTY_ROLE_TYP_TXT','PARTY_ROLE_TYP_TXT','String'],
            ['PLCYHLDR_MSTR_NBR','PLCYHLDR_MSTR_NBR','String'],['PLCYHLDR_BR_NBR','PLCYHLDR_BR_NBR','String'],['PLCYHLDR_DEPT_NBR','PLCYHLDR_DEPT_NBR','String'],
            ['CMPNY_PREF_NAME','CMPNY_PREF_NAME','String'],['BILL_CNTCT_NAME','BILL_CNTCT_NAME','String'],['PLCYHLDR_BILL_SUBSYS_CD','PLCYHLDR_BILL_SUBSYS_CD','String'],
            ['PLCYHLDR_BILL_SUBSYS_DESCR','PLCYHLDR_BILL_SUBSYS_DESCR','String'],['RGNL_GRP_OFF_NAME','RGNL_GRP_OFF_NAME','String'],['RGNL_GRP_OFF_NBR','RGNL_GRP_OFF_NBR','String'],
            ['PLCYHLDR_EFF_DT','PLCYHLDR_EFF_DT','String'],['PLCYHLDR_TERM_DT','PLCYHLDR_TERM_DT','String'],['PLCYHLDR_REINSTMT_DT','PLCYHLDR_REINSTMT_DT','String'],
            ['PLCYHLDR_STAT_CD','PLCYHLDR_STAT_CD','String'],['PLCYHLDR_STAT_DESCR','PLCYHLDR_STAT_DESCR','String'],['PREM_PAY_GRACE_PER_CD','PREM_PAY_GRACE_PER_CD','String'],
            ['PREM_PAY_GRACE_PER_DESCR','PREM_PAY_GRACE_PER_DESCR','String'],['PD_THRU_DT','PD_THRU_DT','String'],['PLCYHLDR_FRST_SUBSD_PREF_NAME','PLCYHLDR_FRST_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_SEC_SUBSD_PREF_NAME','PLCYHLDR_SEC_SUBSD_PREF_NAME','String'],['PLCYHLDR_THIRD_SUBSD_PREF_NAME','PLCYHLDR_THIRD_SUBSD_PREF_NAME','String'],
            ['PLCYHLDR_FOURTH_SUBSD_PREF_NAME','PLCYHLDR_FOURTH_SUBSD_PREF_NAME','String'],['PLCYHLDR_LEGAL_NAME','PLCYHLDR_LEGAL_NAME','String'],
            ['COV_TYP_CD','COV_TYP_CD','String'],['PM_CUR_IND','PM_CUR_IND','String']]

    DFCompare.compare(ETPRS_DF,EBEN_DF,cols)
    
def Eben_3717_1():
    print("Start of test")
    """
    #dev
    ETPRS_Server = "SQLD10747,4242" #"SQLD10785,4242"
    ETPRS_DB = "ENTPRS_CLAIMS_DM" 
        
    EBEN_Server = "SQLD10203,4242"
    EBEN_DB = "eBEN"
    """
    """
    #stg
    ETPRS_Server = "SQLT10747,5353" #"SQLD10785,4242"
    ETPRS_DB = "ENTPRS_CLAIMS_DM" 
        
    EBEN_Server = "SQLT10203,5353"
    EBEN_DB = "eBEN"
    """
    ETPRS_Server = "SQLP10747,2626" #"SQLD10785,4242"
    ETPRS_DB = "ENTPRS_CLAIMS_DM" 
        
    EBEN_Server = "SQLP10203,2626"
    EBEN_DB = "eBEN"
    
    partyDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    benPlanDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    partyPlanFact_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    partyDtlExtDim_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    benPlanDimVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    partyPlanFactVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    pmBatch_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    entprsPartyVW_Comparison(ETPRS_Server,ETPRS_DB,EBEN_Server,EBEN_DB)
    
    
    print("End of test")   




def compare_String(xlval,finval,fieldval):
    global Fail_df,caseStatus
    if(str(xlval).strip() == 'nan'):
        xlval = ''
    
    if(str(xlval).strip() == '-'):
        xlval = ''
        
    if(str(finval).strip() == '-'):
        finval = ''
                
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
        #Pass_df.append({'CaseNo':caseno, 'ColumnName':fieldval,'FinVal':finval, 'ArenaVal':xlval}, ignore_index=True)
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))
        df2 = pd.DataFrame([[caseno,fieldval,finval,xlval]], columns=['CaseNo','ColumnName','FinVal','ArenaVal'])
        Fail_df = pd.concat([df2, Fail_df])
        caseStatus = 0
        
def compare_float(xlval,finval,fieldval):
    global Fail_df,caseStatus
    if(str(xlval).strip() == 'nan'):
        xlval = 0
        
    if(str(xlval).strip() == ''):
        xlval = 0 
         
    if(str(finval).strip() == ''):
        xlval = 0   
            
    xlval = str(xlval).replace(",","")
    finval = str(finval).replace(",","")
       
    if(float(xlval)== float(finval)):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(float(finval)))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(float(finval)) + " but the excel value is " + str(float(xlval)))
        df2 = pd.DataFrame([[caseno,fieldval,finval,xlval]], columns=['CaseNo','ColumnName','FinVal','ArenaVal'])
        Fail_df = pd.concat([df2, Fail_df])
        caseStatus = 0


def compare_ClassDesc(xlval,finval,waiverDescList,fieldval):
    global Fail_df,caseStatus
    if(str(xlval).strip() == 'nan'):
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        if(str(xlval) in waiverDescList):
            html_logger.dbg(fieldval + " validation is pass. The value is " + str(xlval) + " and is present in the List of Class Description")
        else:     
            html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))
            df2 = pd.DataFrame([[caseno,fieldval,finval,xlval]], columns=['CaseNo','ColumnName','FinVal','ArenaVal'])
            Fail_df = pd.concat([df2, Fail_df])

def compare_Date(xlval,finval,fieldval):
    try:
        if(str(xlval).strip() == ''):
            xlval = ''
        elif(str(xlval).strip() == 'NaT'):
            xlval = ''     
        else:    
            datetimeobject = datetime.strptime(str(xlval),'%Y-%m-%d %H:%M:%S')
            xlval = datetimeobject.strftime('%m/%d/%Y')
    except:
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))

def compare_Date1(xlval,finval,fieldval):
    try:
        if(str(xlval).strip() == ''):
            xlval = ''
        elif(xlval =='No Waiver'):
            xlval = 'No Waiver'
        elif(str(xlval).strip() == 'NaT'):
            xlval = ''        
        else:    
            datetimeobject = datetime.strptime(str(xlval),'%m/%d/%Y')
            xlval = datetimeobject.strftime('%m/%d/%Y')
    except:
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))

def compare_Date_format_minus1day(xlval,xlvalFormat,finval,fieldval):
    global Fail_df,caseStatus
    try:
        if str(finval).strip() == '-':
            finval = ''
            
        if(str(xlval).strip() == ''):
            xlval = ''
        elif(str(xlval).strip() == '-'):
            xlval = ''    
        elif(str(xlval).strip() == 'NaT'):
            xlval = ''     
        elif(str(xlval).strip() == 'NaN'):
            xlval = ''      
        else:    
            datetimeobject = datetime.strptime(str(xlval),xlvalFormat) #'%Y-%m-%d %H:%M:%S')
            xlval = datetimeobject.strftime('%m/%d/%Y')
            datetimeobject = datetimeobject-td(days=1)
            xlval1 = datetimeobject.strftime('%m/%d/%Y')
    except:
        xlval1 = ''
        xlval = ''
        
    if(str(xlval1).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(xlval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))
        #Fail_df.append({'CaseNo':[caseno], 'ColumnName':[fieldval],'FinVal':[finval], 'ArenaVal':[xlval]}, ignore_index=True)      
        df2 = pd.DataFrame([[caseno,fieldval,finval,xlval]], columns=['CaseNo','ColumnName','FinVal','ArenaVal'])
        Fail_df = pd.concat([df2, Fail_df])
        caseStatus = 0
        
def compare_Date_format(xlval,xlvalFormat,finval,fieldval):
    global Fail_df,caseStatus
    try:
        if str(finval).strip() == '-':
            finval = ''
            
        if(str(xlval).strip() == ''):
            xlval = ''
        elif(str(xlval).strip() == '-'):
            xlval = ''    
        elif(str(xlval).strip() == 'NaT'):
            xlval = ''     
        elif(str(xlval).strip() == 'NaN'):
            xlval = ''      
        else:    
            datetimeobject = datetime.strptime(str(xlval),xlvalFormat) #'%Y-%m-%d %H:%M:%S')
            xlval = datetimeobject.strftime('%m/%d/%Y')
    except:
        xlval = ''
        
    if(str(xlval).strip()== str(finval).strip()):
        html_logger.dbg(fieldval + " validation is pass. The value is " + str(finval))
    else:
        html_logger.err(fieldval + " validation is fail. Fineos value is " + str(finval) + " but the excel value is " + str(xlval))
        #Fail_df.append({'CaseNo':[caseno], 'ColumnName':[fieldval],'FinVal':[finval], 'ArenaVal':[xlval]}, ignore_index=True)      
        df2 = pd.DataFrame([[caseno,fieldval,finval,xlval]], columns=['CaseNo','ColumnName','FinVal','ArenaVal'])
        Fail_df = pd.concat([df2, Fail_df])
        caseStatus = 0

def minusDays(date, format, noofDays):
    try:
        datetimeobject = datetime.strptime(str(date),format) #'%Y-%m-%d %H:%M:%S')
        xlval = datetimeobject.strftime('%m/%d/%Y')
        datetimeobject = datetimeobject-td(days=noofDays)
        xlval1 = datetimeobject.strftime('%m/%d/%Y')
        return  xlval1
    except:
        return ''
      
def Click(driver,xpth):
    try:
        wait = WebDriverWait(driver,10)
        wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
        driver.find_element_by_xpath(xpth).click()
    except:
        Click(driver,xpth)    
    #html_logger.dbg("Clicked " + xpth + " element successfully")


def Set(driver, xpth,val1):
    wait = WebDriverWait(driver,10)
    wait.until(ec.visibility_of_element_located((By.XPATH,xpth)))
    driver.find_element_by_xpath(xpth).clear()
    driver.find_element_by_xpath(xpth).send_keys(str(val1))
    #html_logger.dbg("Set the value of " + val1 + " to the " + xpth + " element successfully")
  
def getEnum(sql1):
    import pyodbc 
    conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=' + "SQLT10747,5353" +';'
                      'Database=' + "ENTPRS_CLAIMS_DM" + ';'
                      'Trusted_Connection=yes;')
    #sql1 = "select FINEOS_ENUM_INSTNC_NAME from dbo.fineos_enum_xref where fineos_enum_id = " + str(enum)
    sqldf = pd.read_sql_query(sql1,conn)
    try:
        return sqldf.iat[0,0]
    except:
        return ""
#####################################################################                     
def waiverReportValidation_Fineos(caseno,xldrow):
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
        time.sleep(3)    
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        time.sleep(3)    
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
        if len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel DataLabel']"))>0:
            benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel DataLabel']").text
        elif len(driver.find_elements_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel']"))>0:
            benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][@class='DataLabel']").text
        else:
            benStartDate = ''
         
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        time.sleep(3)           
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        #for i on range(rowCnt):
        waiverDescription = driver.find_element_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr["+ str(rowCnt) + "]/td").text
        try:
            disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text
        except:
            disDate = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(3)   
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(3)
        try:
            waiverEndDate = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[1]/td[5]").text
        except:
            waiverEndDate= ''
            
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        if disDate == '-'  or len(disDate)<10:
            disDate = claimDisDate
        else:
            disDate =  disDate
               
        idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        a = idNumber.split(" ")[0]
        idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
       
        #disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
        
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
        time.sleep(9)
        
    
        classDesc = '' 
        classID = '' 
        classid = ''
        row = 1
        x1 = 0
        rowcnts = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
        #print(rowcnt)
        while x1 == 0:
            for i in range(1,rowcnt+1):
                classID = driver.find_element_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr[" + str(i) + "]/td[3]").text
                if type in shortTerm:
                    if classID.split("-")[1] in shortTerm:
                        classid = classID
                        x1 =1
                        break
                        
                    else:
                        try:
                            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(row+1) + "]/td[1]").click()
                        except:
                             x1=1   
                        time.sleep(3)
                        
                elif type in longTerm:
                    if classID.split("-")[1] in longTerm:
                        classid = classID
                        x1 =1
                        break
                    else:
                        try:
                            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(row+1) + "]/td[1]").click()
                        except:
                             x1=1   
                        time.sleep(3)
                        
        classDescList = []                                                  
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr"))
        for i in range(1,rowcnt+1):
            classID = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[1]").text
            classDescList += [driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text]
            if classID==classid:
                classDesc = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text
                #break
        
        if len(disDate) == 10 :#benStatus == 'Approved'or 1==1 :
            if waiverDescription == "Waiver of Premium: No Waiver":
                waiverbeginDate = 'No Waiver'
                waiverendDate = 'No Waiver'
                
            elif waiverDescription == 'Waiver of Premium: Beginning of Elimination Period':
                waiverbeginDate = disDate
                waiverendDate = '' #need to pick from recurring payment
                
            elif waiverDescription == 'Waiver of Premium: After Elimination Period':
                waiverbeginDate = benStartDate
                waiverendDate = waiverEndDate #need to pick from recurring payment  
                
            elif waiverDescription == 'Waiver of Premium: First of Month After 30 Days':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=30)
                waiverbeginDate = (waiverbeginDate.replace(day=1) + td(days=32)).replace(day=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment     
                
            elif waiverDescription == 'Waiver of Premium: First of Month After 52wks Coverage':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(weeks=52)
                waiverbeginDate = (waiverbeginDate.replace(day=1) + td(days=32)).replace(day=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment   
             
            elif waiverDescription == 'Waiver of Premium: Begins Immed following 12W of Dis':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(weeks=12) + td(days=1)
                #waiverbeginDate = datetime.strptime(waiverbeginDate, "%m/%d/%Y") + td(days=1)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment   
                
            elif waiverDescription == 'Waiver of Premium: Begins First Day of Disability':
                waiverbeginDate = disDate
                waiverendDate = waiverEndDate #need to pick from recurring payment 
            
            elif waiverDescription == 'Waiver of Premium: Begins 91st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=91)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment    
            
            elif waiverDescription == 'Waiver of Premium: Begins 31st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=31)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 46th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=46)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 61st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=61)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 76th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=76)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
            
            elif waiverDescription == 'Waiver of Premium: Begins 101st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=101)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 121st Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=121)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
             
            elif waiverDescription == 'Waiver of Premium: Begins 366th Day of Disability':
                waiverbeginDate = datetime.strptime(disDate, "%m/%d/%Y") + td(days=366)
                waiverbeginDate = waiverbeginDate.strftime('%m/%d/%Y')
                waiverendDate = waiverEndDate #need to pick from recurring payment  
                                     
            else:        
                waiverbeginDate = disDate
                waiverendDate = ''
        else:
            waiverbeginDate = ''
            waiverendDate = ''    
                
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        
        xldisDate = xldrow[1]
        xlDOB = xldrow[2]
        xlidNumber = xldrow[3]
        xllName = xldrow[4]
        xlfname = xldrow[5]
        xlwaiverbeginDate = str(xldrow[6]).replace('nan','')
        xlwaiverendDate = str(xldrow[7]).replace('nan','')
        xlwaiverDescription = xldrow[8]
        xlbenStatus = xldrow[9]
        xlclassDesc = xldrow[10]
        
        compare_Date(xldisDate,disDate,'Disability Date')
        compare_Date(xlDOB,DOB,'Date of Birth')
        compare_String(xlidNumber,idNumber,'IDNumber')
        compare_String(xllName,lname,'Last Name')
        compare_String(xlfname,fname,'First Name')
        
        #compare_Date1(xlwaiverbeginDate,waiverbeginDate,'WaiverBeginDate')
        
        if(xlwaiverbeginDate==''):
            compare_Date1(xlwaiverbeginDate,'','WaiverBeginDate')
        elif(xlwaiverbeginDate=='No Waiver'):
            compare_Date1(xlwaiverbeginDate,'No Waiver','WaiverBeginDate')
        else:    
            compare_Date1(xlwaiverbeginDate,waiverbeginDate,'WaiverBeginDate')
            
        if(xlwaiverendDate==''):
            compare_Date1(xlwaiverendDate,'','WaiverEndDate')
        elif(xlwaiverendDate=='No Waiver'):
            compare_Date1(xlwaiverendDate,'No Waiver','WaiverEndDate')
        else:    
            compare_Date1(xlwaiverendDate,waiverendDate,'WaiverEndDate')
            
        compare_String(xlwaiverDescription,waiverDescription,'waiverDescription')
        compare_String(xlbenStatus,benStatus,'benStatus')
        compare_ClassDesc(xlclassDesc,classDesc,classDescList,'classDesc')
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        disDate = claimDisDate
               
        idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        a = idNumber.split(" ")[0]
        idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
       
        #disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
       
        
        xldisDate = xldrow[1]
        xlDOB = xldrow[2]
        xlidNumber = xldrow[3]
        xllName = xldrow[4]
        xlfname = xldrow[5]
        xlwaiverbeginDate = str(xldrow[6]).replace('nan','')
        xlwaiverendDate = str(xldrow[7]).replace('nan','')
        xlwaiverDescription = xldrow[8]
        xlbenStatus = xldrow[9]
        xlclassDesc = xldrow[10]
        
        compare_Date(xldisDate,disDate,'Disability Date')
        compare_Date(xlDOB,DOB,'Date of Birth')
        compare_String(xlidNumber,idNumber,'IDNumber')
        compare_String(xllName,lname,'Last Name')
        compare_String(xlfname,fname,'First Name')
        compare_String(xlwaiverbeginDate,'','Waiver begin Date')
        compare_String(xlwaiverendDate,'','Waiver End Date')
        compare_String(xlwaiverDescription,'','Description')        
        compare_String(xlbenStatus,benStatus,'Description')   
       
        #html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------

def waiverValidation():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of Disability Waiver Report Validation with Fineos")    
    
    """
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "STD Claims" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    waiverReportValidation_Fineos(caseno,xldf.iloc[i])
            #print(xldf.iloc[i,j])
    """
    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "Disability Waiver Report" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    waiverReportValidation_Fineos(caseno,xldf.iloc[i])
 
##################################################################### 
 
def disClaimStatusReport_Validation_Fineos(caseno,xldrow, sheetType):
      
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) >= 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5)    
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    
    if type in shortTerm:
        driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
    elif type in longTerm:
        driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
    else:
        driver.find_element_by_xpath("//div[text() = 'Group Disability Claim - ']").click()
        
    if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
        driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
    
    cnt = 0
    
    if type != '':     #don't perfirm these actions if there is no benefits
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')]").text
        try:
            benEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')]").text
        except:
            benEndDate = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)        
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        
        driver.find_element_by_xpath("//div[contains(text(),'Case History')][@class='TabOff']").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break    
       
       
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied':
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
            #closedReason = driver.find_element_by_xpath("//span[contains(@id,'_reason')][@class='DataLabel']").text
            cnt1 = len(driver.find_elements_by_xpath("//span[contains(text(),'Managing Process Stage changed to " + benStatus + "')]"))
            closedReason = driver.find_element_by_xpath("(//span[contains(text(),'Managing Process Stage changed to " + benStatus + "')])["+ str(cnt1) +"]/following::span[@title='The reason for changing the stage of the activity, as input by the user']").text
            
        else:
            closedDate = ''
            closedReason = ''
        
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(5)
        dateAuthThru = ''
        
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr"))
        if(rowcnt>0):
            for i in range(1,rowcnt+1):
                if(driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[" + str(i) + "]/td[2]").text == 'Approved'):
                    dateAuthThru = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[" + str(i) + "]/td[5]").text
                    break
        else:
            dateAuthThru = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
                
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        try:
            driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
            time.sleep(3)
            #driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
            #time.sleep(3)
            driver.find_element_by_xpath("//a[@title='Sort by: Period End Date']").click()
            time.sleep(3)
            driver.find_element_by_xpath("//a[@title='Sort by: Period End Date']").click()
            time.sleep(3)
            
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            if(rowcnt>0):
                datePaidThru = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[1]/td[3]").text
            else:
                datePaidThru = ''
        except:
                datePaidThru = ''       
         
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
    else:
         benStatus = ''
         benStartDate = ''
         benEndDate = notDate = datePaidThru = dateAuthThru = closedDate = closedReason =''
         
         #startDate
            
    if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
    
    if(benStatus == ''):
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
    name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
    name = name.replace("Mr ","")
    length = len(name.split(" "))  
    lname = name.split(" ")[-1]
    fname = name.split(" ")[0]
    
    DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
    
    idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
    a = idNumber.split(" ")[0]
    idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
   
    disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
    disDate = disDate.strip()
    
    notDate = driver.find_element_by_xpath("//span[contains(@id,'_notificationDate')][@class='DataLabel']").text
    
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'General Claim')]").click()
    time.sleep(3)
    if type in shortTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - STD']/ancestor::td[1]/following::td/span/span").text
    elif type in longTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - LTD']/ancestor::td[1]/following::td/span/span").text
    else:
         startDate = '-'   
         
    if len(startDate) < 10:
         startDate = ''
    '''
    #claim start date and CLaim end date
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Case History')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
    driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
    
    time.sleep(5)
    while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break    
   
    cnt = len(driver.find_elements_by_xpath("//div[contains(@class,'dateDivider')]/span"))
    if(cnt>0):
        startDate = driver.find_element_by_xpath("(//*[contains(text() ,'Intake Document - GDI')]|//span[contains(text(),'New Claim Submitted')])/preceding::div[@class='dateDivider'][1]/span").text   
        startDate = startDate.split(", ")[1]
        datetimeobject = datetime.strptime(str(startDate),'%b %d %Y')
        startDate = datetimeobject.strftime('%m/%d/%Y')  
    else:
        startDate = '' 
    
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  
    '''
          
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
    time.sleep(3)
    
    driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
    time.sleep(5)
           
    xllName = xldrow[1]
    xlfname = xldrow[2] 
    xlidNumber = xldrow[3]
    xlbenStatus = xldrow[4]
    xldisDate = xldrow[5]
    xlbenStartDate = xldrow[6]
    xlnotDate = xldrow[7]
    xlstartDate = xldrow[8]
    xldatePaidThru = xldrow[9]
    xldateAuthThru = xldrow[10]
    if sheetType != 'Open':
        xlclosedDate = xldrow[11] 
        xlclosedReason = xldrow[12]
        xlbenEndDate = xldrow[13]
    else:
        xlbenEndDate = xldrow[11]
        
                               
    compare_String(xllName, lname, "LastName") 
    compare_String(xlfname, fname, "FirstName") 
    compare_String(xlidNumber.lower(), idNumber.lower(), "idNumber") 
    compare_String(xlbenStatus, benStatus, "benStatus") 
    compare_Date(xldisDate, disDate, "disDate") 
    compare_Date(xlbenStartDate, benStartDate, "benStartDate") 
    
    compare_Date(xlnotDate, notDate, "notification Date")
    compare_Date(xlstartDate, startDate, "Claim Complete Date")
    
    if str(xldatePaidThru).strip() == '' or str(xldatePaidThru) == 'nan' :
        compare_Date(xldatePaidThru, '', "Date Claim Paid Thru")
    else:
        compare_Date(xldatePaidThru, datePaidThru, "Date Claim Paid Thru")
    
    if str(xldateAuthThru).strip() == '' or str(xldateAuthThru) == 'nan' :
        compare_Date(xldateAuthThru, '', "Date Claim Authorized Thru")
    else:
        compare_Date(xldateAuthThru, dateAuthThru, "Date Claim Authorized Thru")

    if str(xlbenEndDate).strip() == '' or str(xlbenEndDate) == 'nan' :
        compare_Date(xlbenEndDate, '', "Benefit End Date")
    else:
        compare_Date(xlbenEndDate, benEndDate, "Benefit End Date")    
    
    if sheetType != 'Open':
        compare_String(xlclosedReason, closedReason, "Closed Reason")
        
        if str(xlclosedDate).strip() == '' or str(xlclosedDate) == 'nan' :
            compare_Date(xlclosedDate, '', "Date Claim Closed")
        else:
            compare_Date(xlclosedDate, closedDate, "Date Claim Closed")


def disClaimStatusReport():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
    html_logger.info("Start of Disability Claim Status Report Validation with Fineos")  
      
    #xlpath = "C:\\Users\\T003320\\Downloads\\Disability Claim Status Report - All Claims.xlsx" #"C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    #sheetname = "ALL STD OPEN CLAIMS"#"STD Claims" #"LTD Claims" #"DisabilityPremiumWaiver"   
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "disClaimStatusReport"
    
    sheetType = 'All' #  'Open' #
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
    
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    disClaimStatusReport_Validation_Fineos(caseno,xldf.iloc[i],sheetType)
            
#####################################################################             

def disClaimDetailReport_Validation_Fineos(caseno,xldrow, sheetType):
      
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) >= 3:
        type = caseno.split("-")[2]
    else:
        type = ''
    classDesc = ''       
    time.sleep(5)    
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    
    if type in shortTerm:
        driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
    elif type in longTerm:
        driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
    else:
        driver.find_element_by_xpath("//div[text() = 'Group Disability Claim - ']").click()
        
    if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
        driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
    
    cnt = 0
    
  
    
    
    if type != '':     #don't perfirm these actions if there is no benefits
        if type in shortTerm:
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
        elif type in longTerm:   
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
            time.sleep(3)
        
        if type in shortTerm:
            polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
        elif type in longTerm:   
            polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
                       
        time.sleep(5)
        ClaimExaminer = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
        ClaimExaminer = ClaimExaminer.split("(")[0]
        ClaimExaminer = ClaimExaminer.strip()
    
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        benStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')]").text
        if len(benStartDate)<10:
            benStartDate = ''
            
        try:
            benEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')]").text
        except:
            benEndDate = ''
        
        ActionChains(driver).move_to_element(driver.find_element_by_xpath("//div[contains(text(),'Documents')][@class='TabOff']")).perform()    
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)        
        rowCnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))
        #for i on range(rowCnt):
        #waiverDescription = driver.find_element_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr["+ str(rowCnt) + "]/td").text
        claimWorkState = ''
        
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        try:
            driver.find_element_by_xpath("//div[contains(text(),'Occupation')][@class='TabOff']").click()
            time.sleep(3)
            driver.find_element_by_xpath("//input[contains(@id,'_OccupationList_cmdView')]").click()
            time.sleep(3)
            claimWorkState = driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
            
            driver.find_element_by_xpath("//input[contains(@Id,'_cmdPageBack')]").click()
        except:
            pass
       
        driver.find_element_by_xpath("//div[contains(text(),'Case History')][@class='TabOff']").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
                try:
                    driver.find_element_by_xpath("//a[text()='View Older']").click()
                    time.sleep(5)
                except:
                    break 
            
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied' or benStatus == 'Approved':
            if benStatus == 'Closed - Approved':
                benStatus1 = 'Approved'
            else:
                benStatus1 = benStatus
                        
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
            closedReason = driver.find_element_by_xpath("//span[contains(@id,'_reason')][@class='DataLabel']").text
            time.sleep(3)
            
            #cnt = len(driver.find_elements_by_xpath("//span[text()='Managing Process Stage changed to " + benStatus1 + "']/ancestor::div[@class='WidgetListWidget'][1]/preceding-sibling::div[@class='dateDivider']/span"))
            cnt = len(driver.find_elements_by_xpath("//span[text()='Managing Process Stage changed to " + benStatus1 + "']"))
            if(cnt>0):
                InitialDecisionDt = driver.find_element_by_xpath("(//span[text()='Managing Process Stage changed to " + benStatus1 + "'])["+ str(cnt) + "]/ancestor::div[@class='WidgetListWidget'][1]/preceding-sibling::div[@class='dateDivider'][1]/span").text
                #InitialDecisionDt = driver.find_element_by_xpath("(//span[text()='Managing Process Stage changed to " + benStatus1 + "']/ancestor::div[@class='WidgetListWidget'][1]/preceding-sibling::div[@class='dateDivider'][1]/span)[" + str(cnt) + "]").text
                InitialDecisionDt = InitialDecisionDt.split(", ")[1]
                datetimeobject = datetime.strptime(str(InitialDecisionDt),'%b %d %Y')
                InitialDecisionDt = datetimeobject.strftime('%m/%d/%Y')
            else:
                InitialDecisionDt = ''   

        else:
            closedDate = ''
            closedReason = InitialDecisionDt = ''

        time.sleep(5)
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Periods')][@class='TabOff']").click()
        time.sleep(5)
        
        if(len(driver.find_elements_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr"))>0):
            dateAuthThru = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr[1]/td[5]").text
        else:
            dateAuthThru = ''
            
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        
        #-------last payment date-------------------------
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        try:
            #driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
            time.sleep(3)
            driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
            time.sleep(3)
            
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            if rowcnt > 0:
                for i in range(1,rowcnt+1):
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    LastPaidDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')]").text
                    checkno = driver.find_element_by_xpath("//span[contains(@id,'_TransactionNumber')]").text
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    if checkno != '-' or checkno != '':
                        break
            else:
                 LastPaidDate = ''       
        except:
            LastPaidDate = ''
        #-------last payment date-------------------------
        
        #-------------Total benefits paid to date-----------------------------------
        try:
            NetAmount = 0
            if len(driver.find_elements_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']"))==0:
                NetAmount = 0
            else:   
                while(1==1):
                    driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
                    time.sleep(3)
                    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
                    if rowcnt > 0:
                        for i in range(1,rowcnt+1):
                            driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                            time.sleep(3)
                            driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                            time.sleep(3)
                            NetAmount = NetAmount + float(driver.find_element_by_xpath("//span[contains(@id,'_netBenefitAmountBean')]").text.replace(',',''))
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                        if(i==rowcnt):
                            if(len(driver.find_elements_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]"))>0):
                                driver.find_element_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]").click() 
                                time.sleep(3)
                            else:
                                break
                    else:
                            NetAmount = 0
                            break               
                print(NetAmount)
        except:
            NetAmount = 0    
        #---------------Total benefits paid to date-----------------------------------
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        try:
            #driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            if(rowcnt>0):
                datePaidThru = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[1]/td[3]").text
            else:
                datePaidThru = ''
        except:
                datePaidThru = ''       
        
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
    else:
         benStatus = ''
         benStartDate = ''
         benEndDate = notDate = datePaidThru = dateAuthThru = closedDate = closedReason = claimWorkState = InitialDecisionDt = dateAuthThru = LastPaidDate = datePaidThru = ''
         NetAmount = 0
         time.sleep(5)
         ClaimExaminer = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
         ClaimExaminer = ClaimExaminer.split("(")[0]
         ClaimExaminer = ClaimExaminer.strip()
         #startDate
            
    if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
    
    name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
    name = name.replace("Mr ","")
    name = name.replace("Ms ","")
    
    length = len(name.split(" "))  
    lname = name.split(" ")[-1]
    fname = name.split(" ")[0]
    
    if benStatus == '':
        benStatus = driver.find_element_by_xpath("//dt[text()='Status']/following-sibling::dd").text
        
    DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
    
    idNumber = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
    a = idNumber.split(" ")[0]
    idNumber = a[:3] + "-" + a[3:5]+ "-" + a[5:]
    try:
        disDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following-sibling::dd").text 
        disDate = disDate.strip()
    except:
        disDate = ''
        
    notDate = driver.find_element_by_xpath("//span[contains(@id,'_notificationDate')][@class='DataLabel']").text
    
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'General Claim')]").click()
    time.sleep(3)
    if type in shortTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - STD']/ancestor::td[1]/following::td/span/span").text
    elif type in longTerm:
        startDate = driver.find_element_by_xpath("//label[text()='Complete Claim Date - LTD']/ancestor::td[1]/following::td/span/span").text
    else:
         startDate = '-'   
         
    if len(startDate) < 10:
         startDate = ''
         
    if claimWorkState == '':
        #driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        #time.sleep(3)
        driver.find_element_by_xpath("//div[contains(text(),'Occupation')][@class='TabOff']").click()
        time.sleep(3)
        try:
            driver.find_element_by_xpath("//input[contains(@id,'_OccupationList_cmdView')]").click()
            time.sleep(3)
            claimWorkState = driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
            driver.find_element_by_xpath("//input[contains(@name,'_cmdPageBack')]").click()
        except:
            claimWorkState = ''
            
    '''
    #claim start date and CLaim end date
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Case History')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
    driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
    driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
    
    time.sleep(5)
    while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
            try:
                driver.find_element_by_xpath("//a[text()='View Older']").click()
                time.sleep(5)
            except:
                break    
   
    cnt = len(driver.find_elements_by_xpath("//div[contains(@class,'dateDivider')]/span"))
    if(cnt>0):
        startDate = driver.find_element_by_xpath("(//div[contains(@class,'dateDivider')]/span)[" + str(cnt) + "]").text   
        startDate = startDate.split(", ")[1]
        datetimeobject = datetime.strptime(str(startDate),'%b %d %Y')
        startDate = datetimeobject.strftime('%m/%d/%Y')  
    else:
        startDate = ''  
        
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  
    '''
    classid = ''          
    driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
    time.sleep(3)

    driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
    time.sleep(9)
    
    rwct1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr"))
    for s in range(1,rwct1+1):
        if driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(s) + "]/td[1]").text == polNo :
            driver.find_element_by_xpath("//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[" + str(s) + "]/td[1]").click()
            break
    
    time.sleep(3)
    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr"))
    #print(rowcnt)
    for i in range(1,rowcnt+1):
        classID = driver.find_element_by_xpath("//table[contains(@id,'DivisionClassLink')]/tbody/tr[" + str(i) + "]/td[3]").text
        if type in shortTerm:
            if classID.split("-")[1] in shortTerm:
                classid = classID
                break
        elif type in longTerm:
            if classID.split("-")[1] in longTerm:
                classid = classID
                break
        else:
                classid = ''    
    if(classid != ''):            
        classDescList = []                                                  
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr"))
        for i in range(1,rowcnt+1):
            classID = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[1]").text
            classDescList += [driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text]
            if classID==classid:
                classDesc = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyClassesListview')]/tbody/tr[" + str(i) + "]/td[3]").text
    else: 
        classDesc = ''
        classDescList = []
        
    driver.find_element_by_xpath("//input[contains(@name,'_editPageCancel')]").click()
    time.sleep(3)
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END)
    
    
    #----------Start of claimant deails------------
    time.sleep(5)
    driver.find_element_by_xpath("//dt[contains(text(),'Claimant')]/following::dd/a").click()
    time.sleep(5)
    try:
        driver.find_element_by_xpath("//dt[contains(text(),'Claimant')]/following::dd/a").click()
    except:
        pass   
    
    if(len(driver.find_elements_by_xpath("//span[contains(@id,'gender')]"))>0):
        Gender = driver.find_element_by_xpath("//span[contains(@id,'gender')]").text
    else:
        Gender = ''
    if(len(driver.find_elements_by_xpath("//span[text()='HOME']/ancestor::div[1]/following::div[1]/descendant::span[contains(@name,'_address')]")) > 0):
        addr = driver.find_element_by_xpath("//span[text()='HOME']/ancestor::div[1]/following::div[1]/descendant::span[contains(@name,'_address')]").text   #    //span[text()='HOME']/ancestor::div[1]/following::div[1]/div/span
        claimResState = addr.split('\n')[1].split(",")[1].strip()
        print(claimResState)
    else:    
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
            claimResState = addr.split('\n')[1].split(",")[1].strip()
            print(claimResState)
        else:
            claimResState = ''
    
    xllName = xldrow[1]
    xlfname = xldrow[2] 
    xlidNumber = xldrow[3]
    xlbenStatus = xldrow[4]
    xldisDate = xldrow[5]
    xlbenStartDate = xldrow[6]
    xlGender = xldrow[7]
    xlDOB = xldrow[8]
    xlnotDate = xldrow[9]
    xlstartDate = xldrow[10]
    xlInitialDecisionDt = xldrow[11]
    xldatePaidThru = xldrow[12]
    xlLastPaidDate = xldrow[13]
    xldateAuthThru = xldrow[14]
    if sheetType != 'Open':
        xlclosedDate = xldrow[15]
        xlclosedReason = xldrow[16]
        xlbenEndDate = xldrow[17]
        xltotBenPaidDate = xldrow[18]
        xlclassDesc = xldrow[19]
        xlclaimResState = xldrow[20]
        xlclaimWorkState = xldrow[21]
        xlClaimExaminer = xldrow[22] 
    else:
        xlbenEndDate = xldrow[15]
        xltotBenPaidDate = xldrow[16]
        xlclassDesc = xldrow[17]
        xlclaimResState = xldrow[18]
        xlclaimWorkState = xldrow[19]
        xlClaimExaminer = xldrow[20]    
    #------------------------------------------------------------                
    if xlclaimWorkState == 'Unknown':
            xlclaimWorkState =''
    if claimWorkState == 'Unknown':
         claimWorkState =''
                
    compare_String(xllName, lname, "LastName") 
    compare_String(xlfname, fname, "FirstName") 
    compare_String(xlidNumber.lower(), idNumber.lower(), "idNumber") 
    compare_String(xlbenStatus, benStatus, "benStatus") 
    compare_Date(xldisDate, disDate, "disDate") 
    compare_Date(xlbenStartDate, benStartDate, "benStartDate") 
    compare_Date(xlDOB, DOB, "Date Of Birth")
    compare_String(xlGender, Gender, "Gender")
    compare_Date(xlnotDate, notDate, "notification Date")
    compare_Date(xlstartDate, startDate, "Date Claim Opened")
    compare_Date(xlInitialDecisionDt, InitialDecisionDt, "Initial Decision Date")
    #compare_Date(xlInitialDecisionDt, InitialDecisionDt1, "Initial Decision Date")
    
    if str(xldatePaidThru).strip() == '' or str(xldatePaidThru) == 'nan' :
        compare_Date(xldatePaidThru, '', "Date Claim Paid Thru")
    else:
        compare_Date(xldatePaidThru, datePaidThru, "Date Claim Paid Thru")
    
    
    compare_Date(xlLastPaidDate, LastPaidDate, "Last Paid Date") 
    
    if str(xldateAuthThru).strip() == '' or str(xldateAuthThru) == 'nan' :
        compare_Date(xldateAuthThru, '', "Date Claim Authorized Thru")
    else:
        compare_Date(xldateAuthThru, dateAuthThru, "Date Claim Authorized Thru")

    if str(xlbenEndDate).strip() == '' or str(xlbenEndDate) == 'nan' :
        compare_Date(xlbenEndDate, '', "Benefit End Date")
    else:
        compare_Date(xlbenEndDate, benEndDate, "Benefit End Date")
    
    
    compare_String(str(xltotBenPaidDate).replace('.0',''), str(NetAmount).replace('.0',''), "Total Benefits Paid To Date")
    compare_ClassDesc(xlclassDesc,classDesc,classDescList,'classDesc') 
    compare_String(xlclaimResState, claimResState, "Claimant Resident State")
    compare_String(xlclaimWorkState, claimWorkState, "Claimant Work State")
    compare_String(xlClaimExaminer, ClaimExaminer, "Claim Examiner")
        
    if sheetType != 'Open':
        if len(closedReason.strip()) ==1:
            closedReason = closedReason.replace('-','')
            compare_String(xlclosedReason, closedReason, "Closed Reason")
            
        if str(xlclosedDate).strip() == '' or str(xlclosedDate) == 'nan' :
            compare_Date(xlclosedDate, '', "Date Claim Closed")
        else:
            compare_Date(xlclosedDate, closedDate, "Date Claim Closed")
            
    

def disClaimDetailReport():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
    html_logger.info("Start of Disability Claim Detail Report Validation with Fineos")  
      
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\\Users\\T003320\\Downloads\\Disability Claim Status Report - All Claims.xlsx" ##"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "DisClaimDtlRpt" #"ALL STD OPEN CLAIMS"#"STD Claims" #"LTD Claims" #"DisabilityPremiumWaiver"   
    sheetType =    'All' #'Open'  # 
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
    
        
    xldf = pd.read_excel(xlpath,sheetname)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iloc[i,0])[:3] == 'DI-'):
            for j in range(0,xldf.shape[1]):
                if(j==0):
                    caseno = xldf.iloc[i,j]
                    html_logger.info("Start of Case Number" + str(caseno)) 
                    disClaimDetailReport_Validation_Fineos(caseno,xldf.iloc[i],sheetType)
  
##################################################################### 
def FICAWithholdingMatch_Fineos(caseno,xldrow):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listTotamt = []
            listmedwithld = []
            listsswithld = []
            listmedTotamt = []
            listssTotamt = []
            listmedmatch = []
            listssmatch=[]
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listmedwithld.append(str('0')) 
                    listsswithld.append(str('0')) 
                    listmedTotamt.append(str('0')) 
                    listssTotamt.append(str('0')) 
                    listmedmatch.append(str('0'))
                    listssmatch.append(str('0'))
                    
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        if adjName == 'FICA Medicare':
                            medwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            medwithld  = '%.2f' %(float(medwithld))
                            #print(str(medwithld) + " as medwithld amount")
                            listmedwithld.append(str('%.2f' %(float(medwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            medTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            medTotamt = medTotamt.replace(",","")
                            
                            if '.00' in medTotamt:
                                listmedTotamt.append(str(medTotamt.replace('.00','')))
                            else:
                                listmedTotamt.append(str('%.2f' %(float(medTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            if xldrow['FICA Match Exists'].iloc[0] == 'Y':
                                medmatch = float(medwithld)
                            else:
                                medmatch = 0
                            listmedmatch.append(str('%.2f' %(float(medmatch))))
                                    
                        elif adjName == 'FICA Social Security':
                            sswithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            sswithld  = '%.2f' %(float(sswithld))
                            #print(str(sswithld) + " as sswithld amount")
                            listsswithld.append(str('%.2f' %(float(sswithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            ssTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            ssTotamt = ssTotamt.replace(",","")
                            
                            if '.00' in ssTotamt:
                                listssTotamt.append(str(ssTotamt.replace('.00','')))
                            else:
                                listssTotamt.append(str('%.2f' %(float(ssTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            if xldrow['FICA Match Exists'].iloc[0] == 'Y':
                                ssmatch = float(sswithld)
                            else:
                                ssmatch = 0
                            listssmatch.append(str('%.2f' %(float(ssmatch))))
                            
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listempMatch.append("0")
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Claimant Name': listName, 'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD Medicare Wages': listmedTotamt,'QTD Medicare Withheld': listmedwithld, 'QTD Employer Medicare Match': listmedmatch,'QTD Soc Sec Wages': listssTotamt, 'QTD Soc Sec Withheld': listsswithld,'QTD Employer Match Soc Sec': listssmatch} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,2:]
        
        fin_df['QTD Medicare Wages']  =  fin_df['QTD Medicare Wages'].astype(float)  
        xldrow['QTD Medicare Wages']  =  xldrow['QTD Medicare Wages'].astype(float)
        
        fin_df['QTD Soc Sec Wages']  =  fin_df['QTD Soc Sec Wages'].astype(float)  
        xldrow['QTD Soc Sec Wages']  =  xldrow['QTD Soc Sec Wages'].astype(float)
        
        fin_df['QTD Medicare Withheld']  =  fin_df['QTD Medicare Withheld'].astype(float)  
        xldrow['QTD Medicare Withheld']  =  xldrow['QTD Medicare Withheld'].astype(float)
        
        fin_df['QTD Soc Sec Withheld']  =  fin_df['QTD Soc Sec Withheld'].astype(float)  
        xldrow['QTD Soc Sec Withheld']  =  xldrow['QTD Soc Sec Withheld'].astype(float)
        
        fin_df['QTD Employer Medicare Match']  =  fin_df['QTD Employer Medicare Match'].astype(float)  
        xldrow['QTD Employer Medicare Match']  =  xldrow['QTD Employer Medicare Match'].astype(float)
        
        fin_df['QTD Employer Match Soc Sec']  =  fin_df['QTD Employer Match Soc Sec'].astype(float)  
        xldrow['QTD Employer Match Soc Sec']  =  xldrow['QTD Employer Match Soc Sec'].astype(float)
        
        fin_df['QTD Medicare Wages'].iloc[0] = str('%.2f' %(float(fin_df['QTD Medicare Wages'].sum())))
        fin_df['QTD Soc Sec Wages'].iloc[0] = str('%.2f' %(float(fin_df['QTD Soc Sec Wages'].sum())))
        fin_df['QTD Medicare Withheld'].iloc[0] = str(abs('%.2f' %(float( fin_df['QTD Medicare Withheld'].sum()))))
        fin_df['QTD Soc Sec Withheld'].iloc[0] =str(abs('%.2f' %(float(fin_df['QTD Soc Sec Withheld'].sum()))))
        fin_df['QTD Employer Medicare Match'].iloc[0] = str('%.2f' %(float( fin_df['QTD Employer Medicare Match'].sum())))
        fin_df['QTD Employer Match Soc Sec'].iloc[0] = str('%.2f' %(float( fin_df['QTD Employer Match Soc Sec'].sum())))
          
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[0:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def FICAWithholdingMatch():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 941 FICA Withhold and Match Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "FICAWithHolding_Match" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,5].unique()
    print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            FICAWithholdingMatch_Fineos(caseno,xldf)
            
            
            
##################################################################### 
def FedWageTax_Fineos(caseno,xldrow):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listFitWithhld = []
            listFitWages = []
           
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listFitWithhld.append(str('0')) 
                    listFitWages.append(str('0')) 
                  
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    fitPresent = 0
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        
                        if adjName == 'FIT Amount':
                            fitPresent = 1
                            fitwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            fitwithld = fitwithld.replace(",","")
                            fitwithld  = '%.2f' %(float(fitwithld))
                            #print(str(medwithld) + " as medwithld amount")
                            listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            if '.00' in fitwages:
                                listFitWages.append(str(fitwages.replace('.00','')))
                            else:
                                listFitWages.append(str('%.2f' %(float(fitwages)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                    if fitPresent == 0:
                        #driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        if adjName != 'Alimony' and adjName != 'Child Support':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(1) + "]/td[1]").click()
                            time.sleep(3)
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)   
                        
                        else:
                            fitwages = '0'
                            
                        if '.00' in fitwages:
                            listFitWages.append(str(fitwages.replace('.00','')))
                        else:
                            listFitWages.append(str('%.2f' %(float(fitwages)))) 
                        
                        fitwithld = '0'
                        listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                        
                         
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listempMatch.append("0")
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Claimant Name': listName, 'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD FIT Withheld': listFitWithhld,'QTD FIT Wages': listFitWages} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,1:]
        
        fin_df['QTD FIT Withheld']  =  fin_df['QTD FIT Withheld'].astype(float)  
        xldrow['QTD FIT Withheld']  =  xldrow['QTD FIT Withheld'].astype(float)
        
        fin_df['QTD FIT Wages']  =  fin_df['QTD FIT Wages'].astype(float)  
        xldrow['QTD FIT Wages']  =  xldrow['QTD FIT Wages'].astype(float)
        
        fin_df['QTD FIT Withheld'].iloc[0] =   fin_df['QTD FIT Withheld'].sum()
        fin_df['QTD FIT Wages'].iloc[0] =   fin_df['QTD FIT Wages'].sum()
           
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def FedWageTax():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 941 Federal wages and Tax Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "FedWageTax" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,4].unique()
    print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            FedWageTax_Fineos(caseno,xldf)
            

##################################################################### 
def StateWageTax_Fineos(caseno,xldrow):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    xldrow['State'] = xldrow['State'].apply(lambda x: '{0:0>2}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listFitWithhld = []
            listFitWages = []
           
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listFitWithhld.append(str('0')) 
                    listFitWages.append(str('0')) 
                  
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    fitPresent = 0
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        
                        if adjName == 'State Income Tax':
                            fitPresent = 1
                            fitwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            fitwithld = fitwithld.replace(",","")
                            fitwithld  = '%.2f' %(float(fitwithld))
                            #print(str(medwithld) + " as medwithld amount")
                            listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            if '.00' in fitwages:
                                listFitWages.append(str(fitwages.replace('.00','')))
                            else:
                                listFitWages.append(str('%.2f' %(float(fitwages)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                    if fitPresent == 0:
                        #driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        if adjName != 'Alimony' and adjName != 'Child Support':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(1) + "]/td[1]").click()
                            time.sleep(3)
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            fitwages = fitwages.replace(",","")
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)   
                        
                        else:
                            fitwages = '0'
                            
                        if '.00' in fitwages:
                            listFitWages.append(str(fitwages.replace('.00','')))
                        else:
                            listFitWages.append(str('%.2f' %(float(fitwages)))) 
                        
                        fitwithld = '0'
                        listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                        
                         
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()

        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
            claimResCountry = (addr.replace('\n','')).split(",")[-1].strip()
            if('USA' in claimResCountry):
                 claimResState = (addr.replace('\n','')).split(",")[-3].strip()   
            #print(claimResState)
            else:
                claimResState = '70'
        
        
        state = claimResState #driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
        #state = strip(str(state.split("-"))[0])
        
        sql11 = "select ST_GID_CD from [ref].[ST_CD_REF] where st_cd= '" + state + "'"
    
        dbdata = Parser.parseSQL(sql11,"SQLD10747,4242","ENTPRS_CLAIMS_DM")
        st_cd = dbdata['ST_GID_CD'][0]
            
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        liststcd = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listempMatch.append("0")
            liststcd.append(st_cd.strip())
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Claimant Name': listName, 'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD SIT Withheld': listFitWithhld,'QTD SIT Wages': listFitWages, 'State' : liststcd} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,2:]
        
        fin_df['QTD SIT Withheld']  =  fin_df['QTD SIT Withheld'].astype(float)  
        xldrow['QTD SIT Withheld']  =  xldrow['QTD SIT Withheld'].astype(float)
        
        fin_df['QTD SIT Wages']  =  fin_df['QTD SIT Wages'].astype(float)  
        xldrow['QTD SIT Wages']  =  xldrow['QTD SIT Wages'].astype(float)
        
        fin_df['QTD SIT Withheld'].iloc[0] = fin_df['QTD SIT Withheld'].sum()
        fin_df['QTD SIT Wages'].iloc[0] = fin_df['QTD SIT Wages'].sum()
        fin_df['QTD SIT Wages'].iloc[0]  = '%.2f' %(float( fin_df['QTD SIT Wages'].iloc[0]))
         
          
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[0:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def StateWageTax():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of State wages and Tax Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "StateWageTax" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            StateWageTax_Fineos(caseno,xldf)
            
##################################################################### 


##################################################################### 
def MiscReport1099_Fineos(caseno,name,xldrow):
    
    xldrow = xldrow[(xldrow['Claim Number']==caseno) & (xldrow['Name'] == name)]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    #xldrow['SSN'] = xldrow['SSN'].apply(lambda x: '{0:0>9}'.format(x))
    xldrow['Zip Code'] = xldrow['Zip Code'].apply(lambda x: '{0:0>5}'.format(x))
    xldrow['Box 3'] = xldrow['Box 3'].sum()
    xldrow['Box 4 FIT'] = xldrow['Box 4 FIT'].sum()
    xldrow['Box 10'] = xldrow['Box 10'].sum()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
    ssnein = "0.0"
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = xldrow['Name'].iloc[0] #driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listFitWithhld = []
            listFitWages = []
            liststmltamt = []
            listsurBenamt = []
            listssnein = []
            totalfitwithld = 0
            totalstmltamt = 0
            totalsurBenamt = 0
            
                
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listFitWithhld.append(str('0')) 
                    listFitWages.append(str('0')) 
                    liststmltamt.append(str('0'))
                    listsurBenamt.append(str('0'))
                    
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    fitPresent = 0
                    
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        if adjName == 'Settlement':
                            stlmtamt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            stlmtamt = float(stlmtamt.replace(',',''))
                            totalstmltamt = totalstmltamt + stlmtamt
                            ssnein = 'Tax ID'
                        
                        elif adjName == 'Survivor Benefit':
                            surBenamt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            surBenamt = float(surBenamt.replace(',',''))
                            totalsurBenamt = totalsurBenamt + surBenamt
                            ssnein = 'SSN'
                    
                                                    
                    
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                for j in range(1,rowcnt2+1):
                    adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                    if adjName == 'FIT Amount':
                        fitPresent = 1
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        fitwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                        fitwithld = fitwithld.replace(",","")
                        fitwithld  = '%.2f' %(float(fitwithld))
                        print(fitwithld)
                        totalfitwithld = float(fitwithld) + totalfitwithld
                        #print(str(medwithld) + " as medwithld amount")
                        #listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                        """
                        driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                        time.sleep(3)
                        fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                        fitwages = fitwages.replace(",","")
                        
                        if '.00' in fitwages:
                            listFitWages.append(str(fitwages.replace('.00','')))
                        else:
                            listFitWages.append(str('%.2f' %(float(fitwages)))) 
                        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                        """
                        
                """if fitPresent == 0:
                    #driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                    adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                    if adjName != 'Alimony' and adjName != 'Child Support':
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(1) + "]/td[1]").click()
                        time.sleep(3)
                        driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                        time.sleep(3)
                        fitwages = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                        fitwages = fitwages.replace(",","")
                        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)   
                    
                    else:
                        fitwages = '0'
                        
                    if '.00' in fitwages:
                        listFitWages.append(str(fitwages.replace('.00','')))
                    else:
                        listFitWages.append(str('%.2f' %(float(fitwages)))) 
                    
                    fitwithld = '0'
                    listFitWithhld.append(str('%.2f' %(float(fitwithld))))
                    
                     
                    #else:
                    #     listmedwithld.append(str('0')) 
                    #     listsswithld.append(str('0'))  
                    #     listmedTotamt.append(str('0')) 
                    #     listssTotamt.append(str('0')) 
      
                """
                time.sleep(3)        
                driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                time.sleep(3)
                    
                    
            listFitWithhld.append(str('%.2f' %(float(totalfitwithld)))) 
            listsurBenamt.append(str('%.2f' %(float(totalsurBenamt))))
            liststmltamt.append(str('%.2f' %(float(totalstmltamt)))) 
            listssnein.append(str(ssnein))      
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()

        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        
        name1 = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        if name1==name:
            driver.find_element_by_xpath("//a[contains(@name,'_KeyInfoBarLink_0')]").click()
        else:    
            driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(name)
            time.sleep(5)
            driver.find_element_by_xpath("//i[@class='icon-person' or @class='icon-organisation']/following::strong[text()='" + name + "']").click() 
            time.sleep(5)
        
        
        #cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        #cusid = cusid.strip()
        
        #ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        #ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        #ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        #ssn = ssn1+ssn2+ssn3
        
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
        elif(len(driver.find_elements_by_xpath("//span[contains(@id,'_Address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_Address')]").text  
              
        claimResCountry = (addr.replace('\n','')).split(",")[-1].strip()
        if('USA' in claimResCountry):
             #claimResState = (addr.replace('\n','')).split(",")[-3].strip()
             claimAdd1 =  (addr.replace('\n','')).split(",")[0].strip()
             
             #print((str2.replace('\n','')).split(",")[1].strip())
             claimCity =  (addr.replace('\n','')).split(",")[-4].strip()
             claimState =  (addr.replace('\n','')).split(",")[-3].strip()
             claimZip =  (addr.replace('\n','')).split(",")[-2].strip()
             #print((addr.replace('\n','')).split(",")[-1].strip())

             #claimAdd2 =   
        #print(claimResState)
        else:
            claimResState = '70'
        
        
        #state = claimResState #driver.find_element_by_xpath("//span[contains(@id,'_EmploymentLocationCode')]").text
        #state = strip(str(state.split("-"))[0])
                
        listName = []
        listclaimAdd1 = []
        listclaimAdd2 = []
        listclaimCity = []
        listclaimState = []
        listclaimZip = []
        
        
        #for i in range(1,rowcnt+1):
        listName.append(name.strip())
        listclaimAdd1.append(claimAdd1)
        listclaimAdd2.append('0.0')
        listclaimCity.append(claimCity)
        listclaimState.append(claimState)
        listclaimZip.append(claimZip)
            
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'EIN/SSN Type' :listssnein ,'Claimant Name': listName, 'Address Line One': listclaimAdd1,'Address Line Two': listclaimAdd2, 'City': listclaimCity, 'State': listclaimState, 'Zip Code': listclaimZip,'Box 3': listsurBenamt, 'Box 4 FIT' : listFitWithhld, 'Box 10' : liststmltamt } 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,1:-1]
        
        fin_df['Box 3']  =  fin_df['Box 3'].astype(float)  
        xldrow['Box 3']  =  xldrow['Box 3'].astype(float)
        
        fin_df['Box 4 FIT']  =  fin_df['Box 4 FIT'].astype(float)  
        xldrow['Box 4 FIT']  =  xldrow['Box 4 FIT'].astype(float)
        
        fin_df['Box 10']  =  fin_df['Box 10'].astype(float)  
        xldrow['Box 10']  =  xldrow['Box 10'].astype(float)
           
        fin_df = fin_df.applymap(str)
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def MiscReport1099():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 1099 Misc Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "1099Misc" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)a
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        #print(str(xldf.iloc[i,-1])[:3])
        if(str(xldf.iloc[i,-1])[:3] == 'DI-'):
            caseno = xldf.iloc[i,-1]
            name = xldf.iloc[i,2]
            html_logger.info("Start of Case Number" + str(caseno) + " and the name " + str(name)) 
            MiscReport1099_Fineos(caseno,name,xldf)
            
##################################################################### 

##################################################################### 
def ASOSimpleReport1099_Fineos(caseno,PaymentBeginDate,PaymentEndDate,xldrow1):
    
    PaymentBeginDate = datetime.strptime(str(PaymentBeginDate),'%m/%d/%Y')
    PaymentEndDate = datetime.strptime(str(PaymentEndDate),'%m/%d/%Y')
    
    xldrow = xldrow1[(xldrow1['FINEOS Claim Nbr']==caseno) & (xldrow1['Payment Begin Date'] == PaymentBeginDate.strftime('%m/%d/%Y'))]
    xldrow = xldrow.reset_index(drop=True)
    #xldrow = xldrow.iloc[:,2:]
    #xldrow = xldrow.drop('Policyholder Master Name',1)
    print(xldrow['Employer FICA Amt'][0])
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0)
    
    xldrow['Payment Begin Date'] = pd.to_datetime(xldrow['Payment Begin Date'])
    xldrow['Payment Begin Date'] = xldrow['Payment Begin Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment Date'] = pd.to_datetime(xldrow['Payment Date'])
    xldrow['Payment Date'] = xldrow['Payment Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment End Date'] = pd.to_datetime(xldrow['Payment End Date'])
    xldrow['Payment End Date'] = xldrow['Payment End Date'].dt.strftime('%m/%d/%Y')
    
    #xldrow['Zip Code'] = xldrow['Zip Code'].apply(lambda x: '{0:0>5}'.format(x))
    #xldrow['Box 3'] = xldrow['Box 3'].sum()
    #xldrow['Box 4 FIT'] = xldrow['Box 4 FIT'].sum()
    #xldrow['Box 10'] = xldrow['Box 10'].sum()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
    ssnein = "0.0"
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        time.sleep(3)
        if type in shortTerm:
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
        elif type in longTerm:   
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
        time.sleep(3)
        
        if type in shortTerm:
            polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
        elif type in longTerm:   
            polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
            
        #polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit']/following::strong[text()='Policy Number']/span").text 
        polNo1 = polNo   
        polNo = str(polNo[:8]) + '-' + str(polNo[8:12]) + '-' + str(polNo[12:])
        listPolNo = []
        listPolNo.append(polNo)
        
        #planType = driver.find_element_by_xpath("//strong[text()='Coverage Code']/span").get_attribute('textContent')
        try:
            planType = planType.split(" ")[1]
        except:
            planType = ''
            
        listPlanType = []
        listPlanType.append(planType)
        
        driver.find_element_by_xpath("//button[contains(text(),'All')]").click()
        time.sleep(1)
        driver.find_element_by_xpath("//li[contains(text(),'Organization')]").click()
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(polNo1)
        time.sleep(3)
        PHMstrName = driver.find_element_by_xpath("//strong[text()='" + polNo1 + "']/preceding::div[1]").text
        listPHMstrName = []
        listPHMstrName.append(PHMstrName)
        
        if type in shortTerm:
            driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']").click()
        elif type in longTerm:   
            driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']").click()
        time.sleep(3)
        """ 
       #-----------------------------------
        driver.find_element_by_xpath("//div[contains(text(),'Recurring Payments')][@class='TabOff']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[contains(text(),'Payment Plan')][@class='TabOff']").click()
        time.sleep(3)
        
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
        for i in range(rowcnt):
            daterng = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + i + "]/td[2]").text
            if daterng ==PaymentBeginDate.strftime('%m/%d/%Y'):
                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + i + "]/td[1]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@id,'_ViewButton')]").click()
                time.sleep(3)
                payStartDt = driver.find_element_by_xpath("//span[contains(@id,'_periodStartDateBean')]").text
                payEndDt = driver.find_element_by_xpath("//span[contains(@id,'_periodEndDateBean')]").text
                chkEFT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                chkEFT =chkEFT.replace(',','')   
                
                driver.find_element_by_xpath("//div[contains(text(),'Alternate Payee Dues')][@class='TabOff']").click()
                time.sleep(3)
                rowcnt1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[1]/td[6]"))
                if(rowcnt1 > 0):
                    for j in range(rowcnt1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[" + j + "]/td[4]")
                        if adjName == 'Garnishment' or adjName == 'Alimony'  or adjName == 'Child Support' or adjName == 'Tax Levy' or adjName == 'Health & Welfare Premium Deduction' or adjName == 'Pension - 401K Contribution' :
                                driver.find_element_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[" + j + "]/td[4]").click()
                                chkGarnishment = driver.find_element_by_xpath("//table[contains(@id,'_SubDueEvents')]/tbody/tr[" + j + "]/td[4]").text
                                chkGarnishment = chkGarnishment.replace(",","")
                                chkGarnishment  = '%.2f' %(float(chkGarnishment))
                                #print(chkGarnishment)
                                totalchkGarnishment = float(chkGarnishment) + totalchkGarnishment
                                #print(str(medwithld) + " as medwithld amount")
                        elif adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld' or adjName == 'FIT Amount' or adjName == 'FIT Refund' or adjName == 'Mandatory FIT' or adjName == 'State Income Tax' or adjName == 'SIT Refund':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                            EmployeeTaxAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            EmployeeTaxAmt = EmployeeTaxAmt.replace(",","")
                            EmployeeTaxAmt  = '%.2f' %(abs(float(EmployeeTaxAmt)))
                            #print(chkGarnishment)
                            totalEmployeeTaxAmt = float(EmployeeTaxAmt) + totalEmployeeTaxAmt 
                           
                            if adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld': 
                                FICAamt = FICAamt + float(EmployeeTaxAmt)
                        
                        
                        elif adjName == 'Employer FICA Medicare' or adjName == 'Employer FICA Medicare Refund' or adjName == 'Employer FICA Medicare Underwithheld' or adjName == 'Employer FICA Social Security' or adjName == 'Employer FICA Social Security Refund' or adjName == 'Employer FICA Social Security Withheld':
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                            EmployeeFICAAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            EmployeeFICAAmt = EmployeeFICAAmt.replace(",","")
                            EmployeeFICAAmt  = '%.2f' %(float(EmployeeFICAAmt))
                            #print(chkGarnishment)
                            totalEmployeeFICAAmt = float(EmployeeFICAAmt) + totalEmployeeFICAAmt   
                        
                        if totalEmployeeFICAAmt == 0:
                            totalEmployeeFICAAmt = totalEmployeeFICAAmt + FICAamt
                            
                        totalLiabAmt = float(totalchkEFT) +  float(totalchkGarnishment) + float(totalEmployeeTaxAmt) +  float(totalEmployeeFICAAmt)      
                        time.sleep(3)        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
        #------------------------------------
                        """
        
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text  #xldrow['Name'].iloc[0] #
        fname = name.split(' ')[0]
        lname = name.replace(fname,"").strip()#name.split(' ')[-1]
        listFName = []
        listLName = []
        listFName.append(str(fname))
        listLName.append(str(lname))
        

        listCaseNo = []
        listCaseNo.append(str(caseno))
        
        
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        
        totalchkGarnishment = 0   
        totalEmployeeTaxAmt = 0  
        FICAamt = 0
        totalEmployeeFICAAmt = 0 
        chkEFT = 0
        listchkEFT = []        
        listchkGarnishment = [] 
        listEmployeeTaxAmt = []
        listEmployeeFICAAmt = []
        listLiabAmt = []
        listpayStartDt = []
        listpayEndDt = []
        listtransDate = []
        
        totalchkGarnishment = 0   
        totalEmployeeTaxAmt = 0  
        totalEmployeeFICAAmt = 0
        totalLiabAmt = 0  
        totalchkEFT = 0              
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            rowAvlbl = 0
            
            for i in range(1,rowcnt+1):
                time.sleep(5)
                strtdate1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                #enddate1 = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                #d1 = date(date1)
                #d2 = date(PaymentBeginDate)
                #d3 = date(PaymentEndDate)
                
                date_format = "%Y-%m-%d %H:%M:%S"
                date_format1 = "%m/%d/%Y"
                """
                a = datetime.strptime(str(PaymentBeginDate), date_format)
                b = datetime.strptime(strtdate1, date_format1) # Date to be checked
                c = datetime.strptime(str(PaymentEndDate), date_format)
                d = datetime.strptime(str(PaymentBeginDate), date_format)  #Date entered here should always be the same as 'a'
                delta1 = b - a
                delta2 = c - b
                delta3 = d - a
                """
                strtDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                endDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                
                a = datetime.strptime(str(strtDt), date_format1)
                b = datetime.strptime(str(PaymentBeginDate), date_format) # Date to be checked
                c = datetime.strptime(str(endDt), date_format1)
                d = datetime.strptime(str(strtDt), date_format1)  #Date entered here should always be the same as 'a'
                delta1 = b - a
                delta2 = c - b
                delta3 = d - a
                
                
                if((driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text == PaymentBeginDate.strftime('%m/%d/%Y')) and (driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text == PaymentEndDate.strftime('%m/%d/%Y'))) :
                    rowAvlbl = 1
                    payStartDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                    payEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                    
                    time.sleep(3)
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    #--------------------
                    transDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')]").text
                    chkEFT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                    chkEFT =chkEFT.replace(',','')
                    #------------------
                    driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                    time.sleep(3) 
                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                    if(rowcnt2 == 0):
                        chkEFT = 0 
                        totalchkEFT = 0                       
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                    else:  
                         totalchkEFT = totalchkEFT + float(chkEFT)                    
                        #for j in range(1,rowcnt2+1):
                        #    adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        #   driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        #    #if(adjName == 'Auto Gross Entitlement' or adjName == 'Main Payment Line'):
                        #    chkEFT = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                        #   chkEFT = float(chkEFT.replace(',',''))
                        #   totalchkEFT = totalchkEFT + chkEFT
                                                   
                    totalchkGarnishment = 0
                    totalEmployeeTaxAmt = 0
                    totalEmployeeFICAAmt = 0
                    FICAamt = 0
                    
                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                    if rowcnt2 == 0:
                        #chkGarnishment = 0 
                        totalchkGarnishment = 0   
                        totalEmployeeTaxAmt = 0  
                        FICAamt = 0
                        totalEmployeeFICAAmt = 0                  
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3) 
                    else:    
                        for j in range(1,rowcnt2+1):
                            adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                            if adjName == 'Garnishment' or adjName == 'Alimony'  or adjName == 'Child Support' or adjName == 'Tax Levy' or adjName == 'Health & Welfare Premium Deduction' or adjName == 'Pension - 401K Contribution' :
                                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                chkGarnishment = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                chkGarnishment = chkGarnishment.replace(",","")
                                chkGarnishment  = '%.2f' %(float(chkGarnishment))
                                #print(chkGarnishment)
                                totalchkGarnishment = float(chkGarnishment) + totalchkGarnishment
                                #print(str(medwithld) + " as medwithld amount")
                            elif adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld' or adjName == 'FIT Amount' or adjName == 'FIT Refund' or adjName == 'Mandatory FIT' or adjName == 'State Income Tax' or adjName == 'SIT Refund':
                                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                EmployeeTaxAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                EmployeeTaxAmt = EmployeeTaxAmt.replace(",","")
                                EmployeeTaxAmt  = '%.2f' %(abs(float(EmployeeTaxAmt)))
                                #print(chkGarnishment)
                                totalEmployeeTaxAmt = float(EmployeeTaxAmt) + totalEmployeeTaxAmt 
                               
                                if adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld': 
                                    FICAamt = FICAamt + float(EmployeeTaxAmt)
                            
                            
                            elif adjName == 'Employer FICA Medicare' or adjName == 'Employer FICA Medicare Refund' or adjName == 'Employer FICA Medicare Underwithheld' or adjName == 'Employer FICA Social Security' or adjName == 'Employer FICA Social Security Refund' or adjName == 'Employer FICA Social Security Withheld':
                                driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                EmployeeFICAAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                EmployeeFICAAmt = EmployeeFICAAmt.replace(",","")
                                EmployeeFICAAmt  = '%.2f' %(float(EmployeeFICAAmt))
                                #print(chkGarnishment)
                                totalEmployeeFICAAmt = float(EmployeeFICAAmt) + totalEmployeeFICAAmt   
                        
                         
                        if totalEmployeeFICAAmt == 0:
                            if  xldrow['Employer FICA Amt'][0] == 0:
                                totalEmployeeFICAAmt = 0
                            else:    
                                totalEmployeeFICAAmt = totalEmployeeFICAAmt + FICAamt
                            
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)  
                    totalchkGarnishment = abs(totalchkGarnishment)      
                    
                    totalLiabAmt = float(totalchkEFT) +  float(totalchkGarnishment) + float(totalEmployeeTaxAmt) +  float(totalEmployeeFICAAmt)      
                    time.sleep(3)        
                    
                            
                    listchkEFT.append(str('%.2f' %(float(totalchkEFT))))         
                    listchkGarnishment.append(str('%.2f' %(float(totalchkGarnishment)))) 
                    listEmployeeTaxAmt.append(str('%.2f' %(float(totalEmployeeTaxAmt))))
                    listEmployeeFICAAmt.append(str('%.2f' %(float(totalEmployeeFICAAmt)))) 
                    listLiabAmt.append(str('%.2f' %(float(totalLiabAmt))))
                    
                    listpayStartDt.append(str(payStartDt))
                    listpayEndDt.append(str(payEndDt))
                    listtransDate.append(str(transDate))
                    
                    xldrow['Garnishment Amt'] = str('%.2f' %(float( xldrow['Garnishment Amt'])))
                    xldrow['Employee Tax Amt'] = str('%.2f' %(float( xldrow['Employee Tax Amt']))) 
                    xldrow['Employer FICA Amt'] = str('%.2f' %(float( xldrow['Employer FICA Amt']))) 
                    xldrow['Liability Amt'] = str('%.2f' %(float( xldrow['Liability Amt']))) 
                    xldrow['Check EFT Amt'] = str('%.2f' %(float( xldrow['Check EFT Amt'])))  
        #except:
        #   pass
        
                    dict = {'Policyholder Nbr':listPolNo, 'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo,'Last Name':listLName, 'First Name':listFName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Liability Amt':listLiabAmt,'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt} #'Policyholder Master Name':listPHMstrName,
                    fin_df = pd.DataFrame(dict)                  
                    fin_df = fin_df.applymap(str)
                    #xldrow = xldrow.iloc[:,:-1]
                    xldrow = xldrow.applymap(str)
                    
                    if xldrow.equals(fin_df):
                        html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
                    elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
                        html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                    elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
                        html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                    else:
                        html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                                               
                elif((delta1.days >= delta3.days) and (delta2.days >= delta3.days)):                   
                    rowAvlbl = 1
                    
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    
                    #payStartDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                   #payEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                    driver.find_element_by_xpath("//div[contains(text(),'Payment Allocations')][@class='TabOff']").click()
                    time.sleep(3)
                    itmfnd = 0
                    try:
                        rowcntpg = len(driver.find_elements_by_xpath("//span[contains(@id,'_AllocatedDuesListView_blockNumber_')]"))
                    except:
                        rowcntpg = 1
                    if rowcntpg == 0:
                        rowcntpg =1
                                
                    for xx in range(1,rowcntpg+1):
                        rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr"))
                        k = 1
                        for k in range(1,rowcnt2+1):
                            
                            if((driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[1]").text == PaymentBeginDate.strftime('%m/%d/%Y')) and (driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[2]").text == PaymentEndDate.strftime('%m/%d/%Y'))) :
                                payStartDt = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[1]").text
                                payEndDt = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[2]").text
                                 
                                driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(k) + "]/td[1]").click()
                                time.sleep(3)
                                driver.find_element_by_xpath("//input[contains(@name,'cmdView')]").click()
                                time.sleep(3)
                                chkEFT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                                chkEFT =chkEFT.replace(',','')
                                totalchkEFT = chkEFT
                                
                                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr"))
                                if rowcnt2 == 0:
                                    #chkGarnishment = 0 
                                    totalchkGarnishment = 0   
                                    totalEmployeeTaxAmt = 0  
                                    FICAamt = 0
                                    totalEmployeeFICAAmt = 0                  
                                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                                    time.sleep(3) 
                                else:    
                                    for j in range(1,rowcnt2+1):
                                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                                        if adjName == 'Garnishment' or adjName == 'Alimony'  or adjName == 'Child Support' or adjName == 'Tax Levy' or adjName == 'Health & Welfare Premium Deduction' or adjName == 'Pension - 401K Contribution' :
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            chkGarnishment = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            chkGarnishment = chkGarnishment.replace(",","")
                                            chkGarnishment  = '%.2f' %(float(chkGarnishment))
                                            totalchkGarnishment = float(chkGarnishment) + totalchkGarnishment
                                            #print(str(medwithld) + " as medwithld amount")
                                        elif adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld' or adjName == 'FIT Amount' or adjName == 'FIT Refund' or adjName == 'Mandatory FIT' or adjName == 'State Income Tax' or adjName == 'SIT Refund':
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            EmployeeTaxAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            EmployeeTaxAmt = EmployeeTaxAmt.replace(",","")
                                            EmployeeTaxAmt  = '%.2f' %(abs(float(EmployeeTaxAmt)))
                                            #print(chkGarnishment)
                                            totalEmployeeTaxAmt = float(EmployeeTaxAmt) + totalEmployeeTaxAmt 
                                           
                                            if adjName == 'FICA Medicare' or adjName == 'FICA Medicare Refund' or adjName == 'FICA Medicare Underwithheld' or adjName == 'FICA Social Security' or adjName =='FICA Social Security Refund' or adjName == 'FICA Social Security Withheld': 
                                                FICAamt = FICAamt + float(EmployeeTaxAmt)
                                        
                                        
                                        elif adjName == 'Employer FICA Medicare' or adjName == 'Employer FICA Medicare Refund' or adjName == 'Employer FICA Medicare Underwithheld' or adjName == 'Employer FICA Social Security' or adjName == 'Employer FICA Social Security Refund' or adjName == 'Employer FICA Social Security Withheld':
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            EmployeeFICAAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            EmployeeFICAAmt = EmployeeFICAAmt.replace(",","")
                                            EmployeeFICAAmt  = '%.2f' %(float(EmployeeFICAAmt))
                                            #print(chkGarnishment)
                                            totalEmployeeFICAAmt = float(EmployeeFICAAmt) + totalEmployeeFICAAmt   
                                    
                                    if totalEmployeeFICAAmt == 0:
                                        if  xldrow['Employer FICA Amt'][0] == 0:
                                            totalEmployeeFICAAmt = 0
                                        else:    
                                            totalEmployeeFICAAmt = totalEmployeeFICAAmt + FICAamt
                                    
                                    totalchkGarnishment1 = abs(totalchkGarnishment) 
                                    totalLiabAmt = float(totalchkEFT) +  float(totalchkGarnishment1) + float(totalEmployeeTaxAmt) +  float(totalEmployeeFICAAmt)      
                                    time.sleep(3)
                                    itmfnd  =1  
                                    break
                        if itmfnd == 0:
                             try:
                                 driver.find_element_by_xpath("//a[contains(@name,'AllocatedDuesListView_cmdNext')]").click()
                             except:
                                 html_logger.err("No dates found")           
                     #page for    
                    try:         
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                         
                        driver.find_element_by_xpath("//div[contains(text(),'Payment Details')][@class='TabOff']").click()
                        time.sleep(3) 
                        transDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')]").text
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                         
                        listchkEFT.append(str('%.2f' %(float(totalchkEFT))))         
                        listchkGarnishment.append(str('%.2f' %(float(totalchkGarnishment1)))) 
                        listEmployeeTaxAmt.append(str('%.2f' %(float(totalEmployeeTaxAmt))))
                        listEmployeeFICAAmt.append(str('%.2f' %(float(totalEmployeeFICAAmt)))) 
                        listLiabAmt.append(str('%.2f' %(float(totalLiabAmt))))
                        listpayStartDt.append(str(payStartDt))
                        listpayEndDt.append(str(payEndDt))
                        listtransDate.append(str(transDate))
                        
                        xldrow['Garnishment Amt'] = str('%.2f' %(float( xldrow['Garnishment Amt'])))
                        xldrow['Employee Tax Amt'] = str('%.2f' %(float( xldrow['Employee Tax Amt']))) 
                        xldrow['Employer FICA Amt'] = str('%.2f' %(float( xldrow['Employer FICA Amt']))) 
                        xldrow['Liability Amt'] = str('%.2f' %(float( xldrow['Liability Amt']))) 
                        xldrow['Check EFT Amt'] = str('%.2f' %(float( xldrow['Check EFT Amt'])))  
             #excep:
             #   pass
             
                        dict = {'Policyholder Nbr':listPolNo, 'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo,'Last Name':listLName, 'First Name':listFName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Liability Amt':listLiabAmt,'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt} #'Policyholder Master Name':listPHMstrName,
                        fin_df = pd.DataFrame(dict)                  
                        fin_df = fin_df.applymap(str)
                         #xldrow = xldrow.iloc[:,:-1]
                        xldrow = xldrow.applymap(str)
                         
                        if xldrow.equals(fin_df):
                            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
                        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
                            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
                            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                         
                        else:
                             html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())

                    except:
                         pass       
            """listchkEFT.append(str('%.2f' %(float(totalchkEFT))))         
                listchkGarnishment.append(str('%.2f' %(float(totalchkGarnishment)))) 
                listEmployeeTaxAmt.append(str('%.2f' %(float(totalEmployeeTaxAmt))))
                listEmployeeFICAAmt.append(str('%.2f' %(float(totalEmployeeFICAAmt)))) 
                listLiabAmt.append(str('%.2f' %(float(totalLiabAmt))))
                listpayStartDt.append(str(payStartDt))
                listpayEndDt.append(str(payEndDt))
                listtransDate.append(str(transDate))
                
                xldrow['Garnishment Amt'] = str('%.2f' %(float( xldrow['Garnishment Amt'])))
                xldrow['Employee Tax Amt'] = str('%.2f' %(float( xldrow['Employee Tax Amt']))) 
                xldrow['Employer FICA Amt'] = str('%.2f' %(float( xldrow['Employer FICA Amt']))) 
                xldrow['Liability Amt'] = str('%.2f' %(float( xldrow['Liability Amt']))) 
                xldrow['Check EFT Amt'] = str('%.2f' %(float( xldrow['Check EFT Amt'])))  
    #except:
    #   pass
    
                dict = {'Policyholder Nbr':listPolNo, 'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo,'Last Name':listLName, 'First Name':listFName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Liability Amt':listLiabAmt,'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt} #'Policyholder Master Name':listPHMstrName,
                fin_df = pd.DataFrame(dict)                  
                fin_df = fin_df.applymap(str)
                #xldrow = xldrow.iloc[:,:-1]
                xldrow = xldrow.applymap(str)
                
                if xldrow.equals(fin_df):
                    html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
                elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
                    html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
                    html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
                
                else:
                    html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
            """  
            if rowAvlbl == 0:
                html_logger.err("Payment record  with that date range is not available")
                    
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def ASOSimpleReport1099():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of ASO SimpleTable Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ASOSimple" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
   #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)a
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        #print(str(xldf.iloc[i,-1])[:3])
        if(str(xldf.iloc[i,2])[:3] == 'DI-'):
            caseno = xldf.iloc[i,2]
            PaymentBeginDate = xldf.iloc[i,6]
            PaymentEndDate = xldf.iloc[i,7]
            html_logger.info("Start of Case Number" + str(caseno) + " and the PaymentBeginDate " + str(PaymentBeginDate) + " and the PaymentEndDate " + str(PaymentEndDate)) 
            ASOSimpleReport1099_Fineos(caseno,PaymentBeginDate,PaymentEndDate,xldf)
            
##################################################################### 

##################################################################### 
def ReconcileEmpTax_NoW2_8922_Fineos(caseno,xldrow):
    
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listTotamt = []
            listmedwithld = []
            listsswithld = []
            listmedTotamt = []
            listssTotamt = []
            listmedmatch = []
            listssmatch=[]
            listRRT1Withheld = []
            for i in range(1,rowcnt+1):
                time.sleep(3)
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                time.sleep(3)
            
                driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                time.sleep(3) 
                rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                if(rowcnt2 == 0):
                    listmedwithld.append(str('0')) 
                    listsswithld.append(str('0')) 
                    listmedTotamt.append(str('0')) 
                    listssTotamt.append(str('0')) 
                    listmedmatch.append(str('0'))
                    listssmatch.append(str('0'))
                    listRRT1Withheld.append(str('0'))
                    
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                else:
                    for j in range(1,rowcnt2+1):
                        adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                        driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                        if adjName == 'FICA Medicare':
                            medwithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            medwithld  = '%.2f' %(float(medwithld))
                            medwithld = abs(float(medwithld))
                            #print(str(medwithld) + " as medwithld amount")
                            listmedwithld.append(str('%.2f' %(float(medwithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            medTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            medTotamt = medTotamt.replace(",","")
                            
                            if '.00' in medTotamt:
                                listmedTotamt.append(str(medTotamt.replace('.00','')))
                            else:
                                listmedTotamt.append(str('%.2f' %(float(medTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            """if xldrow['FICA Match Exists'].iloc[0] == 'Y':
                                medmatch = float(medwithld)
                            else:
                                medmatch = 0
                            listmedmatch.append(str('%.2f' %(float(medmatch))))
                            """
                                    
                        elif adjName == 'FICA Social Security':
                            sswithld = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            sswithld  = '%.2f' %(float(sswithld))
                            sswithld = abs(float(sswithld))
                            #print(str(sswithld) + " as sswithld amount")
                            listsswithld.append(str('%.2f' %(float(sswithld))))
                            
                            driver.find_element_by_xpath("//input[contains(@id,'ManPmntBalPayeeListView_')][contains(@id,'_OffsetsAndDeductions_cmdView')]").click()
                            time.sleep(3)
                            ssTotamt = driver.find_element_by_xpath("//span[contains(@id,'_TaxableIncome')]").text
                            ssTotamt = ssTotamt.replace(",","")
                            
                            if '.00' in ssTotamt:
                                listssTotamt.append(str(ssTotamt.replace('.00','')))
                            else:
                                listssTotamt.append(str('%.2f' %(float(ssTotamt)))) 
                            
                            driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                            time.sleep(3)
                            
                            '''if xldrow['FICA Match Exists'].iloc[0] == 'Y':
                                ssmatch = float(sswithld)
                            else:
                                ssmatch = 0
                            listssmatch.append(str('%.2f' %(float(ssmatch))))'''
                            
                        #else:
                        #     listmedwithld.append(str('0')) 
                        #     listsswithld.append(str('0'))  
                        #     listmedTotamt.append(str('0')) 
                        #     listssTotamt.append(str('0')) 
                               
                    time.sleep(3)        
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
        #except:
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listName = []
        listcustId = []
        listssn = []
        listcaseno = []
        listempMatch = []
        
        for i in range(1,rowcnt+1):
            listName.append(name.strip())
            listcustId.append(cusid)
            listssn.append(ssn)
            listcaseno.append(caseno)
            listRRT1Withheld.append("0")
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Customer ID': listcustId, 'SSN': listssn, 'Claim Number': listcaseno, 'QTD Medicare Wages': listmedTotamt,'QTD Medicare Withheld': listmedwithld, 'QTD Soc Sec Wages': listssTotamt, 'QTD Soc Sec Withheld': listsswithld,'FIT Wages': listssTotamt,'RRT1 Withheld':listRRT1Withheld} 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        xldrow = xldrow.iloc[:,2:]
        
        fin_df['QTD Medicare Wages']  =  fin_df['QTD Medicare Wages'].astype(float)  
        xldrow['QTD Medicare Wages']  =  xldrow['QTD Medicare Wages'].astype(float)
        
        fin_df['QTD Soc Sec Wages']  =  fin_df['QTD Soc Sec Wages'].astype(float)  
        xldrow['QTD Soc Sec Wages']  =  xldrow['QTD Soc Sec Wages'].astype(float)
        
        fin_df['QTD Medicare Withheld']  =  fin_df['QTD Medicare Withheld'].astype(float)  
        xldrow['QTD Medicare Withheld']  =  xldrow['QTD Medicare Withheld'].astype(float)
        
        fin_df['QTD Soc Sec Withheld']  =  fin_df['QTD Soc Sec Withheld'].astype(float)  
        xldrow['QTD Soc Sec Withheld']  =  xldrow['QTD Soc Sec Withheld'].astype(float)
        
        xldrow['FIT Wages']  =  xldrow['FIT Wages'].astype(float)
        fin_df['FIT Wages']  =  fin_df['FIT Wages'].astype(float)
        
        xldrow['RRT1 Withheld'] = xldrow['RRT1 Withheld'].astype(float)
        fin_df['RRT1 Withheld'] = fin_df['RRT1 Withheld'].astype(float)
        
        #fin_df = fin_df.iloc[:2,:]
             
        fin_df['QTD Medicare Wages'].iloc[0] = fin_df['QTD Medicare Wages'].sum()
        fin_df['QTD Soc Sec Wages'].iloc[0] = fin_df['QTD Soc Sec Wages'].sum()
        fin_df['QTD Medicare Withheld'].iloc[0] = fin_df['QTD Medicare Withheld'].sum()
        fin_df['QTD Soc Sec Withheld'].iloc[0] = fin_df['QTD Soc Sec Withheld'].sum()
        fin_df['FIT Wages'].iloc[0] = fin_df['FIT Wages'].sum()
        fin_df['RRT1 Withheld'].iloc[0] = fin_df['RRT1 Withheld'].sum()
        
       
        fin_df = fin_df.applymap(str)
        fin_df = fin_df.iloc[0:1,:]
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def ReconcileEmpTax_NoW2_8922():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of 8922 ReconcileEmpTax_NoW2 Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ReconcileEmpTax_NoW2" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    xldf1 = xldf.iloc[:,4].unique()
    print(xldf)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            ReconcileEmpTax_NoW2_8922_Fineos(caseno,xldf)
            
#############################################################
##################################################################### 
def ClaimantEOB_Fineos(caseno,xldrow, reportx):
    fullPayment = ''
    fullPayment1 = ''
    xldrow = xldrow[xldrow['Claim Number']==caseno]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow.reset_index(drop = True)
   
    #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
   
    date = xldrow.iat[0,9]
    name = xldrow.iat[0,3].split("\n")[0].strip()
    name = name.replace("  "," ")
    print(name)
    #xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
                
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        
        #name1 = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        #driver.find_element_by_xpath("//span[contains(@id,'_PaymentDateFrom')]").click()
        #time.sleep(3)
        #driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").clear()
        #driver.find_element_by_xpath("//input[contains(@id,'_PaymentDateFrom')]").send_keys("01/01/2021")
        try:
            driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[contains(text(),'" + name + "')]").click()
        except:
             pass   
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
            
            listTotamt = []
            listmedwithld = []
            listsswithld = []
            listmedTotamt = []
            listssTotamt = []
            listmedmatch = []
            listssmatch=[]
            listRRT1Withheld = []
            for i in range(1,rowcnt+1):
                date = xldrow.iloc[0][9]
                startdate = date.split("-")[0].strip()
                enddate = date.split("-")[1].strip()
                
                finStrtDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[2]").text
                finEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[3]").text
                
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                time.sleep(3)
                
                if(datetime.strptime(startdate,'%m/%d/%Y') == datetime.strptime(finStrtDt,'%m/%d/%Y') and datetime.strptime(enddate,'%m/%d/%Y') == datetime.strptime(finEndDt,'%m/%d/%Y') and reportx == 'EOB'):     
                    time.sleep(3)
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(5)
                    address = driver.find_element_by_xpath("//span[contains(@name,'_payeeAddress')]").text
                    
                    assignedTo1 = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
                    assignedTo =  assignedTo1.split("(")[0]
                    chkDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')][@class='DataLabel']").text
                    payMtd = driver.find_element_by_xpath("//span[contains(@id,'_paymentMethodDropDown')][@class='DataLabel']").text
                    name = driver.find_element_by_xpath("//span[contains(@id,'_payeeName')]").text
                    name = name.replace("Mr ","")
                    nomPayee = driver.find_element_by_xpath("//span[contains(@Id,'_nominatedPayee')]").text
                    if nomPayee == '-':
                        name = name
                    else:
                        name = nomPayee
                                        
                    TotPayment = driver.find_element_by_xpath("//span[contains(@id,'_NetPaymentAmount')]").text
                    TotPayment = TotPayment.replace(",","")
                                                            
                    driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                    time.sleep(3) 
                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                    if(rowcnt2 == 0):                       
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                    else:
                        fullPayment = ""
                        fullPayment1 = ""
                        for j in range(1,rowcnt2+1):
                            adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                            if adjName == 'Auto Gross Entitlement':
                                adjName = 'Gross Benefit'
                                adjName1 = adjName 
                            elif adjName == 'COLA Summary Auto Generated Adjustment Type':
                                adjName = 'COLA Summary Adjustment Type' 
                                adjName1 = 'COLA  Adjustment' 
                            #elif adjName == 'COLA Summary Adjustment Type':  DI-1131 fails if we have this condition
                                #adjName = 'COLA  Adjustment'      
                            elif adjName == 'FIT Amount':
                                adjName = 'Federal Income Tax'  
                                adjName1 = adjName 
                            else:
                                 adjName1 = adjName   
                                 
                            amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                            amount  = amount.replace(",","")
                            amount = str('%.2f' %(float(amount)))
                            
                            payment = adjName + "//" + str(amount)  
                            payment1 = adjName1 + "//" + str(amount)  
                            
                            if j ==1:
                                fullPayment = payment.replace(",","")
                                fullPayment1 = payment1.replace(",","")
                            else:
                                fullPayment = fullPayment + ";;" + payment.replace(",","")
                                fullPayment1 = fullPayment1 + ";;" + payment1.replace(",","")
                                    
                        rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
                        if rowcnt2 > 0:
                               for j in range(1,rowcnt2+1):
                                   adjName = driver.find_element_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + str(j) + "]/td[1]").text
                                   if adjName == 'FIT Amount':
                                       adjName = 'Federal Income Tax' 
                                       adjName1 = adjName
                                   elif adjName == 'FICA Medicare':
                                       adjName1 = adjName #'RRTM'
                                   elif adjName == 'FICA Social Security':
                                       adjName1 = adjName #'RRT1'      
                                   amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                   amount  = amount.replace(",","")
                                   amount = str('%.2f' %(float(amount)))       
                                   payment = adjName + "//" + str(amount)    
                                   payment1 = adjName1 + "//" + str(amount)  
                                   
                                   fullPayment = fullPayment + ";;" + payment.replace(",","")
                                   fullPayment1 = fullPayment1 + ";;" + payment1.replace(",","")
                        time.sleep(3)        
                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                        time.sleep(3)
                elif reportx != 'EOB':
                    #fullPayment = ''
                    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr"))
                    if rowcnt>0:
                        for x in range(1, rowcnt+1):
                            finStrtDt1 = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(x) + "]/td[1]").text
                            finendDt1 = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(x) + "]/td[2]").text
                            amount = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(x) + "]/td[4]").text
                            xlAmount = xldrow.iat[0,8].split("//")[1].strip()
        #except:        
                            amount  = amount.replace(",","")
                            amount = str('%.2f' %(float(amount)))
                            xlAmount  = xlAmount.replace(",","")
                            xlAmount = str('%.2f' %(float(xlAmount)))
                                            
                            if datetime.strptime(startdate,'%m/%d/%Y') == datetime.strptime(finStrtDt1,'%m/%d/%Y') and datetime.strptime(enddate,'%m/%d/%Y') == datetime.strptime(finendDt1,'%m/%d/%Y') and xlAmount == amount  :
                                    
                                    time.sleep(3)
                                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                                    time.sleep(3)
                                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                                    time.sleep(5)
                                    address = driver.find_element_by_xpath("//span[contains(@name,'_payeeAddress')]").text
                                    
                                    if len(driver.find_elements_by_xpath("//img[contains(@name,'CaseOwnershipSummary')][contains(@src,'plus.png')]"))>0:
                                        driver.find_element_by_xpath("//img[contains(@name,'CaseOwnershipSummary')][contains(@src,'plus.png')]").click()

                                    time.sleep(3)
                                    assignedTo1 = driver.find_element_by_xpath("//span[contains(@id,'_AssignedTo')]").text
                                    assignedTo =  assignedTo1.split("(")[0]
                                    chkDate = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')][@class='DataLabel']").text
                                    payMtd = driver.find_element_by_xpath("//span[contains(@id,'_paymentMethodDropDown')][@class='DataLabel']").text
                                    name = driver.find_element_by_xpath("//span[contains(@id,'_payeeName')]").text
                                    nomPayee = driver.find_element_by_xpath("//span[contains(@Id,'_nominatedPayee')]").text
                                    if nomPayee == '-':
                                        name = name
                                    else:
                                        name = nomPayee    
                                        
                                    TotPayment = driver.find_element_by_xpath("//span[contains(@id,'_NetPaymentAmount')]").text
                                    TotPayment = TotPayment.replace(",","")
                                    #address = driver.find_element_by_xpath("//span[contains(@id,'_payeeName')]").text
                                                                            
                                    driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                                    time.sleep(3) 
                                    rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                                    if(rowcnt2 == 0):                       
                                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                                        time.sleep(3)
                                    else:
                                        fullPayment = ""
                                        for j in range(1,rowcnt2+1):
                                            adjName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").text
                                            driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[1]").click()
                                            if adjName == 'Auto Gross Entitlement':
                                                adjName = 'Gross Benefit'
                                            elif adjName == 'COLA Summary Auto Generated Adjustment Type':
                                                adjName = 'COLA Summary Adjustment Type' 
                                            elif adjName == 'FIT Amount':
                                                adjName = 'Federal Income Tax' 
                                            #elif adjName == 'COLA Summary Adjustment Type':   DI-1131 fails if we have this condition
                                             #   adjName = 'COLA  Adjustment'     
                                            #amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                            
                                            
                                            payment = adjName + "//" + str(amount)    
                                            if j ==1:
                                                fullPayment = payment.replace(",","")
                                            else:
                                                fullPayment = fullPayment + ";;" + payment.replace(",","")
                                                    
                                        rowcnt2 = len(driver.find_elements_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
                                        if rowcnt2 > 0:
                                               for j in range(1,rowcnt2+1):
                                                   adjName = driver.find_element_by_xpath("//table[contains(@id,'ManPmntBalPayeeListView')][contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" + str(j) + "]/td[1]").text
                                                   if adjName == 'FIT Amount':
                                                       adjName = 'Federal Income Tax'
                                                
                                                   amount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(j) + "]/td[2]").text
                                                   amount  = amount.replace(",","")
                                                   amount = str('%.2f' %(float(amount)))       
                                                   payment = adjName + "//" + str(amount)    
                                                   fullPayment = fullPayment + ";;" + payment.replace(",","")
                                                
                                        time.sleep(3)        
                                        driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                                        time.sleep(3)
        #   pass
        
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Coverages')]").click()
        time.sleep(3)
        
        driver.find_element_by_xpath("//input[contains(@id,'_ChangeContracts')]").click()
        time.sleep(9)
        
        PolicyHolder = driver.find_element_by_xpath("//table[contains(@id,'_GroupPolicyDivisionsListview')]/tbody/tr[1]/td[2]").text
        driver.find_element_by_xpath("//input[contains(@name,'editPageCancel')]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        time.sleep(3)
        
        insname = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        insname = insname.replace("Mr ","")
        #insname = insname.split(" ")[0] + " " + insname.split(" ")[-1]
        insname = insname.strip()
        
        address = address.replace("\\n","\n")
        address = name + "\n" + address
        address = address.replace(" ","").replace("\n","").replace(" ","")
        driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").click()
        time.sleep(3)
        
        cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        cusid = cusid.strip()
        
        ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        ssn = ssn1+ssn2+ssn3
        
        listinsName = []
        listName = []
        listaddress = []
        listChkDt = []
        listcaseno = []
        listProcessor = []
        listChkDt = []
        listpayMtd = []
        listPolicyHolder = []
        listPayment = []
        listFinPayment = []
        
        for i in range(1,rowcnt+1):
            listinsName.append(insname.strip())
            listName.append(name.strip())
            listcaseno.append(caseno)
            listaddress.append(address.replace("\\n",""))
            listProcessor.append(assignedTo.strip())
            listChkDt.append(chkDate)
            listpayMtd.append(payMtd)
            listPolicyHolder.append(PolicyHolder)
            listPayment.append(fullPayment)
        
        
        #fin_df = pd.DataFrame(list(zip(listName, listcustId,listssn,listcaseno,listTotamt,listmedwithld,listempMatch,listTotamt,listsswithld,listempMatch)),columns =['Claimant Name', 'Customer ID','SSN','Claim Number','QTD Medicare Wages','QTD Medicare Withheld','QTD Employer Medicare Match','QTD Soc Sec Wages','QTD Soc Sec Withheld','QTD Employer Match Soc Sec'])
        dict = {'Insured Name': listinsName, 'Claim Number': caseno, 'Policy Holder': listPolicyHolder, 'address': listaddress,'Processor': listProcessor, 'Check Date': listChkDt, 'Source Code': listpayMtd}  #, 'Payment': listPayment 
        fin_df = pd.DataFrame(dict)         
        #print(benStatus + "\n" + waiverDescription + "\n" + disDate +"\n" +  fname + "\n" + lname + "\n" + idNumber + "\n" + DOB + "\n" + classDesc)
        #print(fin_df.head(5))
        #print(xldrow.head(5))
        
        #fullPayment = str('%.2f' %(float(fullPayment)))
        #xldrow['Payment'] = xldrow['Payment'].astype(float)
        try:
            xldrow['Check Date'] = xldrow['Check Date'].dt.strftime('%m/%d/%Y')
        except:
            xldrow['Check Date'] = ''
        listFinPayment = fullPayment.split(";;") 
        listFinPayment1 = fullPayment1.split(";;") 
        listXLPayment = xldrow['Payment'].str.split(";;")
        

        
        if (Counter(listXLPayment.tolist()[0]) == Counter(listFinPayment)) or (Counter(listXLPayment.tolist()[0]) == Counter(listFinPayment1)):
            html_logger.dbg("Payment match - The payment values are " + fullPayment)
        else:
            html_logger.err("Payment doesn't match - Extract has \n" + xldrow['Payment'].to_string() + "\n but Fin has \n" + fullPayment)    
        
        if float(TotPayment) == float(xldrow.iat[0,-1]):
            html_logger.dbg("Total Payment match - The payment values are " + TotPayment)
        else:
            html_logger.err("Total Payment doesn't match - Extract has \n" + str(xldrow.iat[0,-1]) + "\n but Fin has \n" + str(TotPayment))   
            
        xldrow = xldrow.drop(['Reference ID'],axis=1)
        xldrow = xldrow.drop(['Dates'],axis=1)
        xldrow = xldrow.drop(['Payment'],axis=1)
        xldrow = xldrow.drop(['PaymentTotal'],axis=1)
        
        xldrow['address'] = xldrow['address'].str.replace(" ","").str.replace("\\n","").str.replace(" ","")
        xldrow.reset_index(drop = True)

        
        #xldrow = xldrow.iloc[2:,:]
        
        #xldrow['Check Date'] = xldrow['Check Date'].str.split(" ").str[0]
        
        #fin_df = fin_df.iloc[:2,:]
        xldrow = xldrow.applymap(str)
        fin_df = fin_df.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
def ClaimantEOB():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of ClaimantEOB  Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ClaimEOB" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname, skiprows=[0])
    xldf1 = xldf.iloc[:,1].unique()
    print(xldf1)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i])[:3] == 'DI-'):
            caseno = xldf1[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            ClaimantEOB_Fineos(caseno,xldf,'AltEOB')     #allowed values =  AltEOB, EOB
###################################################################################
def VendorEOB_Fineos(name,xldrow):
    xldrow = xldrow[xldrow['Name']==name]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow.reset_index(drop = True)
   
    #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
   
    date = xldrow.iat[0,3]
    name = xldrow.iat[0,0].strip()
    name = name.replace("  "," ")
    #print(name)
    
    driver.find_element_by_xpath("//a[contains(@name,'_MENUITEM.SearchPartieslink')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//input[contains(@id,'Organisation_GROUP')]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//input[contains(@id,'_Name')]").send_keys(name)
    driver.find_element_by_xpath("//input[contains(@id,'_searchButton')]").click()
    time.sleep(10)
    
    rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PartySearchResultListviewWidget')]/tbody/tr"))
    if rowcnt > 0 :
        driver.find_element_by_xpath("//input[contains(@id,'_searchPageOk_cloned')]").click()
        time.sleep(5)
        try:
            address = driver.find_element_by_xpath("//span[contains(@name,'_Address')]").text
            address = address.rsplit("\n",1)[0]
            address = address.replace('\n',"").replace(",","").replace(" ","")
        except:
            address = ""
        #print(address)
        
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(3)
        allAmts = ''
        rowcntx = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        for i in range(1,rowcntx+1):
            if float(driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").text) == float(xldrow.iat[0,2]):
                totalamt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").text
                #print(totalamt)
                
                driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[7]").click()
                time.sleep(5)
                rowcnty = len(driver.find_elements_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr"))
                for j in range(1,rowcnty+1):
                    if j == 1:
                        casenos = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[1]" ).text
                        amounts = str(float(driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[5]" ).text))
                    else:    
                        casenos = casenos + ";;" + driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[1]" ).text
                        amounts = str(amounts) + ";;" +str(float(driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr[" + str(j) +"]/td[5]" ).text))
                        
                driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
                arrcasenos = casenos.split(";;")
                arramounts = amounts.split(";;")
                
                longTerm = ['LTD','VLD','CLC','WDL']
                shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
                
                
                cnt = 0
                for caseno in arrcasenos:
                    if len(caseno.split("-")) > 3:
                        type = caseno.split("-")[2]
                    else:
                        type = ''
                        
                    time.sleep(5) 
                    cnt  =cnt + 1
                    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
                    time.sleep(5)
                    if type!= '':
                        if type in shortTerm:
                            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
                        elif type in longTerm:
                            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
                        else:
                             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
                            
                            
                        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
                            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
                        time.sleep(5)
                        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
                            driver.find_element_by_xpath("//div[text()='Close']").click()
                        
                        driver.find_element_by_xpath("//div[contains(text(),'Expenses')][@class='TabOff']").click()
                        time.sleep(3) 
                        amt1 = arramounts[cnt-1]
                        rowcntz = len(driver.find_elements_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr"))
                        for x1 in range(1, rowcntz+1):
                            if float(driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(x1) + "]/td[6]").text) == float(amt1):
                                invval = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(x1) + "]/td[1]").text
                                date1 = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(x1) + "]/td[2]").text
                                if invval == '-':
                                    invval = ''
                                if date1 == '-':
                                    date1 = ''    
                                Chkamt = str('%.2f' %(float(amt1))) + ";;" + str(invval)+ ";;" + str(date1)
                                break
                        if allAmts == '':
                            allAmts = Chkamt
                        else:     
                            allAmts = allAmts + "//" + Chkamt 
            break           
        #print(allAmts)               
        compare_String(xldrow.iat[0,0].strip(), name, "Name")
        compare_String(xldrow.iat[0,1].strip(), address, "Address")   
        compare_String(str('%.2f' %(float(xldrow.iat[0,2]))), str('%.2f' %(float(totalamt))), "TotalAmt")  
         
        compare_String(xldrow.iat[0,3].strip(), allAmts, "All Amounts") 
        listFinPayment = allAmts.split("//")
        listXLPayment = xldrow.iat[0,3].split("//")
        
        if (Counter(listXLPayment) == Counter(listFinPayment)):
            html_logger.dbg("Payment match - The payment values are " + allAmts)
        else:
            html_logger.err("Payment doesn't match - Extract has \n" + allAmts + "\n but Fin has \n" + xldrow.iat[0,3])  
              
                       
    else:
        html_logger.err("Records not found, hence exiting")    
     
    #xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
##################################################
def VendorEOB():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of VendorEOB  Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "VendorEOB" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname, skiprows=[0])
    xldf1 = xldf.iloc[:,0].unique()
    print(xldf1)
    for i in range(0,xldf1.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf1[i]) != ''):
            name = xldf1[i]
            html_logger.info("Start of Vendor " + str(name)) 
            VendorEOB_Fineos(name,xldf)     #allowed values =  AltEOB, EOB


       
       

################################################################################
def DisClaimPayment_Fineos(caseno,BenfromDt,BenEndDt,xldrow):
    xldrow = xldrow[(xldrow['Claim Number']==caseno) & (xldrow['Benefit Paid From Date']==BenfromDt) & (xldrow['Benefit Paid Thru Date']==BenEndDt)]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    xldrow.reset_index(drop = True)
   
    #xlAmount = xldrow.iat[0,8].split("//")[1].strip()
   
    date = xldrow.iat[0,9]
    name = xldrow.iat[0,3].split("\n")[0].strip()
    print(name)
    #xldrow['SS Num'] = xldrow['SS Num'].apply(lambda x: '{0:0>9}'.format(x))
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
 
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
            
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        benStartDt = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][contains(@class,'DataLabel')]").text
        claimStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        
        driver.find_element_by_xpath("//div[contains(text(),'Benefit')][@class='TabOff']").click()
        time.sleep(3)
        driver.find_element_by_xpath("//div[contains(text(),'Calculation Parameters')][@class='TabOff']").click()
        time.sleep(3)
        earnings = driver.find_element_by_xpath("//label[text()='Earnings at Initial Payment Frequency']/ancestor::td[1]/following::td/span").text    
        benDisDate = driver.find_element_by_xpath("//dt[contains(text(),'Disability Date')]/following::dd").text   
        benDisDate = benDisDate.strip() 
        
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        wrkComp = 0
        fitAmt = 0
        ficaAmt = 0
        sitAmt = 0
        
        socSecOfst = 0
        GrpDisSTD = 0
        partialDis = 0
        OvrPymt = 0
        sickPymt = 0
        OthrAmt = 0
        stDisIncome = 0
        salCont = 0
        
        i = True
        while i == True:
            #len(driver.find_elements_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]")) > 0
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
            for i in range(1, rowcnt+1):
                 strdt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[2]").text
                 enddt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) + "]/td[3]").text
                 
                 if datetime.strptime(str(strdt),'%m/%d/%Y') == datetime.strptime(str(BenfromDt),'%Y-%m-%d %H:%M:%S') and datetime.strptime(str(enddt),'%m/%d/%Y') == datetime.strptime(str(BenEndDt),'%Y-%m-%d %H:%M:%S') :
                     driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" +str(i) + "]").click()
                     time.sleep(3)
                     driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                     time.sleep(5)
                     netbfrTax = driver.find_element_by_xpath("//span[contains(@id,'_netBenefitAmountBean')]").text
                     netbfrTax = netbfrTax.replace(",","")
                     netaftrTax = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')]").text
                     netaftrTax = netaftrTax.replace(",","")
                     grosBenAmt = driver.find_element_by_xpath("//span[contains(@id,'_basicAmount')]").text
                     grosBenAmt = grosBenAmt.replace(",","")
                     
                     
                     driver.find_element_by_xpath("//div[contains(text(),'Amounts')][@class='TabOff']").click()
                     time.sleep(3)
                     rowcnt1 = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr"))
                     for x in range(1,rowcnt1+1): 
                         benName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[1]").text
                         if benName == "FIT Amount":
                                fitAmt = float(fitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "FICA" in benName:
                                ficaAmt = float(ficaAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "Social Security" in benName:
                                socSecOfst = float(socSecOfst) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Group Disability Insurance":
                                GrpDisSTD = float(GrpDisSTD) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Partial Return To Work Offset":
                                partialDis = float(partialDis) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))              
                         elif benName == "State Income Tax":
                                sitAmt = float(sitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Offset Recovery":
                                OvrPymt = float(OvrPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Disability - State":
                                stDisIncome  = float(stDisIncome) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))     
                         elif benName =="Sick Leave":
                                sickPymt = float(sickPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))       
                         elif benName == "Workers' Compensation":
                                wrkComp = float(wrkComp) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Salary Continuance":
                                salCont = float(salCont) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         else:
                                OthrAmt = float(OthrAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                                        
                     rowcnt2 =  len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                     for x in range(1,rowcnt2+1): 
                         benName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[1]").text
                         if benName == "FIT Amount":
                                fitAmt = float(fitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "FICA" in benName:
                                ficaAmt = float(ficaAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif "Social Security" in benName:
                                socSecOfst = float(socSecOfst) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Group Disability Insurance":
                                GrpDisSTD = float(GrpDisSTD) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Partial Return To Work Offset":
                                partialDis = float(partialDis) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))              
                         elif benName == "State Income Tax":
                                sitAmt = float(sitAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Offset Recovery":
                                OvrPymt = float(OvrPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Disability - State":
                                stDisIncome  = float(stDisIncome) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))     
                         elif benName =="Sick Leave":
                                sickPymt = float(sickPymt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))       
                         elif benName == "Workers' Compensation":
                                wrkComp = float(wrkComp) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         elif benName == "Salary Continuance":
                                salCont = float(salCont) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                         else:
                                OthrAmt = float(OthrAmt) + float(str(driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(x) + "]/td[2]").text).replace(",",""))
                                           
                     driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                     time.sleep(3)
                     break       
                 else:
                     pass               
                 
                                                                        
            if len(driver.find_elements_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]")) > 0:
                 driver.find_element_by_xpath("//a[contains(@name,'_PaymentHistoryDetailsListview_cmdNext')]").click()
                 pass
            else: 
                 i = False
         #end of while loop# 
         
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()
        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        name = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        
        length = len(name.split(" "))  
        lname = name.split(" ")[length-1]
        fname = name[:-len(lname)]
        
        DOB = driver.find_element_by_xpath("//span[contains(@id,'dateOfBirth')][@class='DataLabel']").text
        SSN = driver.find_element_by_xpath("//div[contains(@id,'_identificationNumber')][@class='DataLabel']").text
        SSN = SSN.split("(")[0]
        SSN = str(SSN[0:3]) .upper()+ "-" + str(SSN[3:5]).upper() + "-" + str(SSN[5:9])
        
        claimDisDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text
        if benDisDate == '-'  or len(benDisDate)<10:
            benDisDate = claimDisDate
        else:
            benDisDate =  benDisDate
     
        
         
         #-------comparing stars----------------
        listClaimNo = []
        listLastName = []
        listFirstName = []
        listSSN = []
        listClaimStatus = []
        listDOD = []
        listBenStrtDt = []
        listBenPdfrm = []
        listBenPdto = []
        listEarnings = []
        listGrossBen = []
        
        listsocSecOfst = []
        listGrpDisSTD= []
        listpartialDis = []
        listOvrPymt = []
        listsickPymt = []
        liststDisIncome = []
        listsalCont = []
         
        listworkComp = []
        listNetBenbfrTax = []
        listFitAmt = []
        listSitAmt = []
        listFicaAmt = []
        listNetAftrbfrTax = []
        listothrAmt = []
         
         
        listClaimNo.append(str(caseno).strip())
        listLastName.append(str(lname).strip())
        listFirstName.append(str(fname).strip())
        listSSN.append(str(SSN).strip())
        listClaimStatus.append(str(claimStatus).strip())
        listDOD.append(str(datetime.strftime(datetime.strptime(benDisDate,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())    #datetime.date.strftime(benDisDate, "%Y-%m-%d %H:%M:%S")
        listBenStrtDt.append(str(datetime.strftime(datetime.strptime(benStartDt,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())
        listBenPdfrm.append(str(datetime.strftime(datetime.strptime(strdt,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())
        listBenPdto.append(str(datetime.strftime(datetime.strptime(enddt,'%m/%d/%Y'),"%Y-%m-%d %H:%M:%S")).strip())
        listEarnings.append(str('%.2f' %(float(earnings))).strip())  
        listGrossBen.append(str('%.2f' %(float(grosBenAmt))).strip())
        #---7 more required
        
        listsocSecOfst.append(str('%.2f' %(float(socSecOfst))).strip())
        listGrpDisSTD.append(str('%.2f' %(float(GrpDisSTD))).strip())
        listpartialDis.append(str('%.2f' %(float(partialDis))).strip())
        listOvrPymt.append(str('%.2f' %(float(OvrPymt))).strip())
        listsickPymt.append(str('%.2f' %(float(sickPymt))).strip())
        liststDisIncome.append(str('%.2f' %(float(stDisIncome))).strip())
        listsalCont.append(str('%.2f' %(float(salCont))).strip())         
        
        listworkComp.append(str('%.2f' %(float(wrkComp))).strip())
        if float(grosBenAmt) -float(netbfrTax) == 0:
            listothrAmt.append(str('0.00'))
        else:
            OthrAmt = '0.00' # remove this once the defect is fixed.    
            listothrAmt.append(str('%.2f' %(float(OthrAmt))).strip())
                               
        listNetBenbfrTax.append(str('%.2f' %(float(netbfrTax))).strip())
    
        listFitAmt.append(str('%.2f' %(float(fitAmt))).strip())
        listSitAmt.append(str('%.2f' %(float(sitAmt))).strip())
        listFicaAmt.append(str('%.2f' %(float(ficaAmt))).strip())
        listNetAftrbfrTax.append(str('%.2f' %(float(netaftrTax))).strip())
        
        
        dict = {'Claim Number' :listClaimNo ,'Claimant Last Name' :listLastName ,'Claimant First Name' :listFirstName ,'Claimant Social Security Number' :listSSN ,'Claim Status' :listClaimStatus ,'Date of Disability' :listDOD ,'Benefit Start Date' :listBenStrtDt ,'Benefit Paid From Date' :listBenPdfrm ,'Benefit Paid Thru Date' :listBenPdto ,'Earnings' :listEarnings ,'Gross Benefit ' :listGrossBen, 
                   'Social Security Offset': listsocSecOfst, 'Group Disability (STD)':listGrpDisSTD,'Overpayment':listOvrPymt, 'Partial\nDisability':listpartialDis, 'State Disability Income': liststDisIncome, 'Salary Continuance':listsalCont,'Sick Leave':listsickPymt                 
                   ,'Workers Compensation' :listworkComp ,'Other ' :listothrAmt ,'Net Benefit (before taxes)' :listNetBenbfrTax ,'FIT ' :listFitAmt ,'SIT' :listSitAmt ,'FICA ' :listFicaAmt ,'Net Payment (after taxes)' :listNetAftrbfrTax}

        fin_df = pd.DataFrame(dict)         
        
        xldrow['Net Benefit (before taxes)']  =  xldrow['Net Benefit (before taxes)'].astype(float)
        fin_df['Net Benefit (before taxes)']  =  fin_df['Net Benefit (before taxes)'].astype(float)
        
        xldrow['Gross Benefit ']  =  xldrow['Gross Benefit '].astype(float)
        fin_df['Gross Benefit ']  =  fin_df['Gross Benefit '].astype(float)
        
        xldrow['Net Payment (after taxes)']  =  xldrow['Net Payment (after taxes)'].astype(float)
        fin_df['Net Payment (after taxes)']  =  fin_df['Net Payment (after taxes)'].astype(float)
        
        xldrow['Earnings']  =  xldrow['Earnings'].astype(float)
        fin_df['Earnings']  =  fin_df['Earnings'].astype(float)
        
        xldrow['Social Security Offset ']  =  xldrow['Social Security Offset '].astype(float).map('{:,.2f}'.format)
        xldrow['Group Disability (STD)']  =  xldrow['Group Disability (STD)'].astype(float).map('{:,.2f}'.format)
        xldrow['Overpayment ']  =  xldrow['Overpayment '].astype(float).map('{:,.2f}'.format)
        xldrow['Partial\nDisability']  =  xldrow['Partial\nDisability'].astype(float).map('{:,.2f}'.format)
        xldrow['State Disability Income']  =  xldrow['State Disability Income'].astype(float).map('{:,.2f}'.format)
        xldrow['Salary Continuance']  =  xldrow['Salary Continuance'].astype(float).map('{:,.2f}'.format)
        xldrow['Sick Leave']  =  xldrow['Sick Leave'].astype(float).map('{:,.2f}'.format)
        xldrow['Workers Compensation']  =  xldrow['Workers Compensation'].astype(float).map('{:,.2f}'.format)
        xldrow['Other ']  =  xldrow['Other '].astype(float).map('{:,.2f}'.format)
        xldrow['FIT ']  =  xldrow['FIT '].astype(float).map('{:,.2f}'.format)
        xldrow['SIT']  =  xldrow['SIT'].astype(float).map('{:,.2f}'.format)
        xldrow['FICA ']  =  xldrow['FICA '].astype(float).map('{:,.2f}'.format)
         
        xldrow = xldrow.applymap(str)
        fin_df = fin_df.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
 
 
        
                
def DisClaimPayment():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of DisClaimPayment  Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "DisClaimPayment" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,1].unique()
    #print(xldf1)
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        if(str(xldf.iat[i,0])[:3] == 'DI-'):
            caseno = xldf.iat[i,0]
            BenFromDt = xldf.iat[i,7]
            BenToDt = xldf.iat[i,8]
            
            #xldf = xldf.iloc[i]
            html_logger.info("Start of Case Number" + str(caseno)) 
            DisClaimPayment_Fineos(caseno,BenFromDt,BenToDt,xldf)     #allowed values =  AltEOB, EOB
       

##################################################################### 
def ClaimsFeedReport_Fineos(caseno,PaymentBeginDate,PaymentAmount,xldrow):
    
    xldrow = xldrow[(xldrow['BEN_CASE_NBR']==caseno) & (xldrow['PAY_SRS_BEG_1_DT'] == PaymentBeginDate.strftime('%m/%d/%Y')) & (xldrow['GROS_BEN_AMT'] == PaymentAmount)]
    #xldrow = xldrow.iloc[:,2:]
    xldrow = xldrow.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    xldrow = xldrow.fillna(0) 
    
    """ 
    xldrow['Payment Begin Date'] = pd.to_datetime(xldrow['Payment Begin Date'])
    xldrow['Payment Begin Date'] = xldrow['Payment Begin Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment Date'] = pd.to_datetime(xldrow['Payment Date'])
    xldrow['Payment Date'] = xldrow['Payment Date'].dt.strftime('%m/%d/%Y')
    
    xldrow['Payment End Date'] = pd.to_datetime(xldrow['Payment End Date'])
    xldrow['Payment End Date'] = xldrow['Payment End Date'].dt.strftime('%m/%d/%Y')
    """
    
    #xldrow['Zip Code'] = xldrow['Zip Code'].apply(lambda x: '{0:0>5}'.format(x))
    #xldrow['Box 3'] = xldrow['Box 3'].sum()
    #xldrow['Box 4 FIT'] = xldrow['Box 4 FIT'].sum()
    #xldrow['Box 10'] = xldrow['Box 10'].sum()
    
    longTerm = ['LTD','VLD','CLC','WDL']
    shortTerm = ['STD','VSD','CSC','OLS','WDS','SDA','VMD']
    
    if len(caseno.split("-")) > 3:
        type = caseno.split("-")[2]
    else:
        type = ''
            
    time.sleep(5) 
    
    driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(caseno)
    time.sleep(5)
    if type!= '':
        if type in shortTerm:
            driver.find_element_by_xpath("//div[text() = 'STD Benefit - ']").click()
        elif type in longTerm:
            driver.find_element_by_xpath("//div[text() = 'LTD Benefit - ']").click()
        else:
             driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER)    
        
           
            
        if len(driver.find_elements_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']"))>0:
            driver.find_element_by_xpath("//input[contains(@name,'warningsLeavingPagePopup')][@value='Yes']").click()
        
        if len(driver.find_elements_by_xpath("//div[text()='Close']")) > 0:
            driver.find_element_by_xpath("//div[text()='Close']").click()
        
        time.sleep(3)
        ##################
        BenStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        arr_Claim_case = Split(caseno,"-")
        Claim_case = arr_Claim_case[0] + "-" + arr_Claim_case[1]
       ###################
       
        time.sleep(3)
        if type in shortTerm:
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']")).perform()
        elif type in longTerm:   
            ActionChains(driver).move_to_element(driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']")).perform() 
        time.sleep(3)
        
        if type in shortTerm:
            polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'STD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
        elif type in longTerm:   
            polNo = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Policy Number'][1]/span").get_attribute('textContent')
            planType = driver.find_element_by_xpath("//a[text() = 'LTD Benefit'][@class='mapEntry active']/following::strong[text()='Coverage Code']/span").get_attribute('textContent')
            
        #polNo = driver.find_element_by_xpath("//a[text() = 'STD Benefit']/following::strong[text()='Policy Number']/span").text 
        ##############
        polNo1 = polNo
        ######
        driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Case History')]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//span[text()='Advanced Filters']").click()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").send_keys("01/01/2020")
        driver.find_element_by_xpath("//input[contains(@id,'_applyFilterChanges')]").click()
        
        time.sleep(5)
        while len(driver.find_elements_by_xpath("//a[text()='View Older']"))>0:
                try:
                    driver.find_element_by_xpath("//a[text()='View Older']").click()
                    time.sleep(5)
                except:
                    break    
       
        cnt = len(driver.find_elements_by_xpath("//div[contains(@class,'dateDivider')]/span"))
        if(cnt>0):
            startDate = driver.find_element_by_xpath("(//*[contains(text() ,'Intake Document - GDI')]|//span[contains(text(),'New Claim Submitted')])/preceding::div[@class='dateDivider'][1]/span").text   
            startDate = startDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(startDate),'%b %d %Y')
            startDate = datetimeobject.strftime('%m/%d/%Y')  
        else:
            startDate = '' 
        
        if benStatus == 'Closed' or benStatus == 'Closed - Approved' or benStatus == 'Denied' or benStatus == 'Approved':
            if benStatus == 'Closed - Approved':
                benStatus1 = 'Approved'
            else:
                benStatus1 = benStatus
                        
            closedDate = driver.find_element_by_xpath("//span[text()='" + benStatus + "'][@class='DataLabel']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/parent::table/parent::span/parent::div/parent::div/preceding-sibling::div[@class='dateDivider'][1]").text
            #driver.find_element_by_xpath("//div[@class='dateDivider']/span").text
            closedDate = closedDate.split(", ")[1]
            datetimeobject = datetime.strptime(str(closedDate),'%b %d %Y')
            closedDate = datetimeobject.strftime('%m/%d/%Y')
            
        
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  
    
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        name = driver.find_element_by_xpath("//a[contains(@id,'_KeyInfoBarLink_0')]").text
        #try:
        driver.find_element_by_xpath("//select[contains(@id,'_Payee_List')]/option[text()='" + name + "']").click()
        time.sleep(3)   
        
        driver.find_element_by_xpath("//select[contains(@id,'_PaymentStatus')]/option[text() = 'Active']").click()
        time.sleep(3)
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr"))
        if rowcnt > 0:
             for i in range(1,rowcnt+1):
                time.sleep(3)
                fin_payBeginDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[1]").text
                fin_payamount = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[7]").text
                fin_payamount = replace(str(fin_payamount),"," )
                periodStrtDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[2]").text
                periodEndDt = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]/td[3]").text
                
                if(fin_payBeginDt == PaymentBeginDate and float(PaymentAmount) == float(fin_payamount)):
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(i) +"]").click()
                    time.sleep(3)
                    driver.find_element_by_xpath("//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]").click()
                    time.sleep(3)
                    claim_own = driver.find_element_by_xpath("//label[text()='Added By']/parent::span//parent::td/following::td/span").text
                    issueDate = driver.find_element_by_xpath("//label[text()='Issue Date']/parent::span//parent::td/following::td/span/span").text
                    transdate = driver.find_element_by_xpath("//label[text()='Transaction Status Date']/parent::span//parent::td/following::td/span").text 
                    fin_periodStrtDt = periodStrtDt
                    fin_periodEndDt = periodEndDt
                    driver.find_element_by_xpath("//input[contains(@id,'_cmdPageBack_cloned')]").click()
                    time.sleep(3)
                    
                    driver.find_element_by_xpath("//div[contains(text(),'Case Details')][@class='TabOff']").click()
                    time.sleep(5)
                    BenStDt = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][contains(@class,'DataLabel')]").text
                    BenEndDt = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')][contains(@class,'DataLabel')]").text
                    
        time.sleep(3)     
        driver.find_element_by_xpath("//a[text()='Group Disability Claim']").click()

        if(len(driver.find_elements_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]")) > 0):
            driver.find_element_by_xpath("//div[@class='TabOff'][contains(text(),'Claim Hub')]").click()
        
        ###########################
        disabilityDate = driver.find_element_by_xpath("//label[text()='Disability Date']/parent::div/following::span/span").text
        
        name1 = driver.find_element_by_xpath("//a[contains(@id,'_participantBeanForParty')]").text
        if name1==name:
            driver.find_element_by_xpath("//a[contains(@name,'_KeyInfoBarLink_0')]").click()
        else:    
            driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(name)
            time.sleep(5)
            driver.find_element_by_xpath("//i[@class='icon-person' or @class='icon-organisation']/following::strong[text()='" + name + "']").click() 
            time.sleep(5)
        
        if(len(driver.find_elements_by_xpath("//span[contains(@id,'_address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_address')]").text
        elif(len(driver.find_elements_by_xpath("//span[contains(@id,'_Address')]"))>0):
            addr = driver.find_element_by_xpath("//span[contains(@id,'_Address')]").text  
              
        claimResCountry = (addr.replace('\n','')).split(",")[-1].strip()
        if('USA' in claimResCountry):
             #claimResState = (addr.replace('\n','')).split(",")[-3].strip()
             claimState =  (addr.replace('\n','')).split(",")[-3].strip()
        else:
            claimState = ''
            
        #cusid = driver.find_element_by_xpath("//dt[text()='Customer Number']/following-sibling::dd").text
        #cusid = cusid.strip()
        
        #ssn1 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_0')]").text)   
        #ssn2 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_1')]").text)
        #ssn3 = str(driver.find_element_by_xpath("//span[contains(@id,'_obfuscatedIdNumber_obfuscatedIdNumber_2')]").text)
        #ssn = ssn1+ssn2+ssn3
        #except:
        #   pass
        
        dict = {'Policyholder Nbr':listPolNo, 'Policyholder Master Name':listPHMstrName,'Plan Type':listPlanType,'FINEOS Claim Nbr':listCaseNo, 'First Name':listFName,'Last Name':listLName, 'Payment Date':listtransDate, 'Payment Begin Date':listpayStartDt, 'Payment End Date':listpayEndDt, 'Check EFT Amt':listchkEFT, 'Garnishment Amt':listchkGarnishment, 'Employee Tax Amt':listEmployeeTaxAmt, 'Employer FICA Amt':listEmployeeFICAAmt, 'Liability Amt':listLiabAmt }
        fin_df = pd.DataFrame(dict)                  
        fin_df = fin_df.applymap(str)
        xldrow = xldrow.applymap(str)
        
        if xldrow.equals(fin_df):
            html_logger.dbg("All Values in extract is present in FIneos - Exact Match. Value is \n" +  xldrow.to_string())
        elif(len(xldrow.merge(fin_df).drop_duplicates()) == len(xldrow.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        elif(len(fin_df.merge(xldrow).drop_duplicates()) == len(fin_df.drop_duplicates())):
            html_logger.dbg("Values in extract is present in FIneos - Subset is extracted in Report.  Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())
        
        else:
            html_logger.err("Values in extract is not present in FIneos - Extract has \n" + xldrow.to_string() + "\n but Fin has \n" + fin_df.to_string())    
        
        if rowAvlbl == 0:
            html_logger.err("Payment record  with that date range is not available")
                    
    else:
        driver.find_element_by_xpath("//input[@id='universal-search']").send_keys(Keys.ENTER) 
        html_logger.err("No Claims and hence exiting") 
    #------------------------Excel read-------------------
 
 
def ClaimsFeedReport():
    #options = webdriver.ChromeOptions()
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
   
    html_logger.info("Start of Claims Feed Report with Fineos")    

    
    xlpath = "C:\\Users\\T003320\\Downloads\\DisabilityPremiumWaiver_11.xls" #"C:\Srikanth\Eclipise_workspace_10142020\Python_Projects\DisabilityClaims\Claims_ReportValidation\TestData.xlsx"
    sheetname = "ClaimFeedReport" #"Test" #"STD Claims" #"Test" #"LTD Claims" #"DisabilityPremiumWaiver"    
    #import xlwt
    
    #driver.get("https://uat-claims-webapp.oneamerica.fineos.com/")
    driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    xldf = pd.read_excel(xlpath,sheetname)
    #xldf1 = xldf.iloc[:,5].unique()
    #print(xldf)a
    for i in range(0,xldf.shape[0]):
        #print(str(xldf.iloc[i,0])[:3])
        #print(str(xldf.iloc[i,-1])[:3])
        if(str(xldf['BEN_CASE_NBR'].iloc[i])[:3] == 'DI-'):
            caseno = str(xldf['BEN_CASE_NBR'].iloc[i])
            PaymentBeginDate = xldf['PAY_SRS_BEG_1_DT'].iloc[i]
            PaymentAmount = xldf['GROS_BEN_AMT'].iloc[i]
            
            html_logger.info("Start of Case Number" + str(caseno) + " and the PaymentBeginDate " + str(PaymentBeginDate)) 
            ClaimsFeedReport_Fineos(caseno,PaymentBeginDate,PaymentAmount,xldf)
            
##################################################################### 


def Migration_Extract_CNV_CLAIM_1(driver):
    global caseStatus
    global Pass_df 
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
    driver.maximize_window()
    
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM.csv", delimiter = '|',encoding='cp1252', dtype = str)
    #CNV_CLAIM_BENEFITRIGHT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_BENEFITRIGHT.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    caseids = CNV_CLAIM_df['CASEID'].unique()
    for caseid in caseids:
        caseStatus = 1
        #flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_BENEFITRIGHT_df = CNV_CLAIM_BENEFITRIGHT_df[CNV_CLAIM_BENEFITRIGHT_df['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_CASENOTE_df  = CNV_CLAIM_CASENOTE_df [CNV_CLAIM_CASENOTE_df ['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_CODE_df = CNV_CLAIM_CODE_df[CNV_CLAIM_CODE_df['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_COVERAGE_df = CNV_CLAIM_COVERAGE_df[CNV_CLAIM_COVERAGE_df['CASEID'].isin(caseids)]
        #combinedDF = flt_CNV_CLAIM_df.merge(flt_CNV_CLAIM_BENEFITRIGHT_df,on = 'CASEID',how='outer')
        #combinedDF = combinedDF.merge(flt_CNV_CLAIM_CASENOTE_df,on = 'CASEID',how='outer')
        #combinedDF = combinedDF.merge(flt_CNV_CLAIM_CODE_df,on = 'CASEID',how='outer')
        #combinedDF = combinedDF.merge(flt_CNV_CLAIM_COVERAGE_df,on = 'CASEID',how='outer')   
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'] ==caseid]
        #flt_CNV_CLAIM_BENEFITRIGHT_df = CNV_CLAIM_BENEFITRIGHT_df[CNV_CLAIM_BENEFITRIGHT_df['CASEID'] ==caseid]
        #flt_CNV_CLAIM_CASENOTE_df  = CNV_CLAIM_CASENOTE_df [CNV_CLAIM_CASENOTE_df ['CASEID'] ==caseid]
        #flt_CNV_CLAIM_CODE_df = CNV_CLAIM_CODE_df[CNV_CLAIM_CODE_df['CASEID'] ==caseid]
        #flt_CNV_CLAIM_COVERAGE_df = CNV_CLAIM_COVERAGE_df[CNV_CLAIM_COVERAGE_df['CASEID'] ==caseid]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        global caseno
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        html_logger.info("Start of the CNV_CLAIM for Case number " + str(caseno))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        if len(driver.find_elements_by_xpath("//a[contains(text(),'Benefit')]"))> 0:
            BenType = driver.find_element_by_xpath("//a[contains(text(),'Benefit')]").text
            if BenType == 'STD Benefit':
                BenType = 'STD'
            elif BenType == 'LTD Benefit':
                BenType = 'LTD'   
            else:
                 BenType = 'OLS'    
        else:
            BenType = ''
            
        CaseStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd").text
        if CaseStatus == 'Closed':
                finCaseStatus = 'H3'
        elif CaseStatus == 'Open':
                finCaseStatus = 'H1'
        elif CaseStatus == 'Intake in Progress':
                finCaseStatus = 'H0'
        elif CaseStatus == 'Approved':
                finCaseStatus = 'H2'        
        xlCaseStatus= flt_CNV_CLAIM_df['CASESTATUS'].iat[0]
        compare_String(xlCaseStatus, finCaseStatus, "CASESTATUS")
                      
        if len(driver.find_elements_by_xpath("//h2[contains(@title,'Group Disability Claim')]")) > 0:
            finClaimType = 'Group Disability Claim'
        xlClaimType = flt_CNV_CLAIM_df['CASETYPE'].iat[0]
        compare_String(xlClaimType, finClaimType, "CASETYPE")
        
        
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        finClmStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd[1]").text
        #caseStatus - need to get mapping 
        
        benefit = driver.find_element_by_xpath("//a[contains(text(),'Group Disability Claim')]/parent::div/following-sibling::div/a") .text
        finBen = benefit.split(" ")[0]
        finBen1 = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finBen + "'  and fineos_dom_name = 'ClaimType'")
        xlBen = flt_CNV_CLAIM_df['CLAIMTYPE'].iat[0]
        compare_String(xlBen, finBen1, "CLAIMTYPE")

        
        finEventDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text   
        xlEventDate = flt_CNV_CLAIM_df['EVENTDATE'].iat[0]
        compare_Date_format(xlEventDate, "%Y-%m-%d %H:%M:%S.%f", finEventDate, "EVENTDATE")  #%Y-%m-%d %H:%M:%S')  %Y-%m-%d %H:%M:%S.%f
             
        finRTW = driver.find_element_by_xpath("//span[contains(@id,'_ExpectedRTWFullTimeDate')][@class='DataLabel']").text
        xlRTW = flt_CNV_CLAIM_df['EXPECTEDRETURNTOWORKDATE'].iat[0]
        compare_Date_format(xlRTW, "%Y-%m-%d %H:%M:%S.%f", finRTW, "EXPECTEDRETURNTOWORKDATE")  
        
        finNotDate = driver.find_element_by_xpath("//span[contains(@id,'_notificationDate')][@class='DataLabel']").text
        xlNotDate = flt_CNV_CLAIM_df['NOTIFICATIONDATE'].iat[0]
        compare_Date_format(xlNotDate, "%Y-%m-%d %H:%M:%S.%f", finNotDate, "NOTIFICATIONDATE")  
        
        finDtofHire = driver.find_element_by_xpath("//span[contains(@id,'_dateOfHire')][contains(@class,'DataLabel')]").text
        xlDtorHire = flt_CNV_CLAIM_df['DATEOFHIRE'].iat[0]
        compare_Date_format(xlDtorHire, "%Y-%m-%d %H:%M:%S.%f", finDtofHire, "DATEOFHIRE")  
        
        finSickAcci = driver.find_element_by_xpath("//span[contains(@id,'_SicknessAccident')][contains(@class,'DataLabel')]").text
        xlSickAcci = flt_CNV_CLAIM_df['SICKNESS_ACCIDENT'].iat[0]
        sql1 = "select FINEOS_ENUM_INSTNC_NAME from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_ID = " +xlSickAcci
        xlSickAcci = getEnum(sql1)
        
        compare_String(xlSickAcci, finSickAcci, 'SICKNESS_ACCIDENT')
        
        finEXPECTEDRETURNTOWORKDATE = driver.find_element_by_xpath("//span[contains(@id,'_ExpectedRTWFullTimeDate')][contains(@class,'DataLabel')]").text
        xlEXPECTEDRETURNTOWORKDATE = flt_CNV_CLAIM_df['EXPECTEDRETURNTOWORKDATE'].iat[0]
        compare_Date_format(xlEXPECTEDRETURNTOWORKDATE, "%Y-%m-%d %H:%M:%S.%f", finEXPECTEDRETURNTOWORKDATE, "EXPECTEDRETURNTOWORKDATE")  
        #------------------------
        if  BenType == 'STD':
            driver.find_element_by_xpath("//td[contains(@id,'More_Tabs_cell')]").click()
            driver.find_element_by_xpath("//td[contains(@id,'Insured')]").click()
            time.sleep(3)
            FINRETURNTOWORKPARTTIMEDATE = driver.find_element_by_xpath("//span[contains(@id,'_DateReturnToWorkPartTime')]").text
            XLRETURNTOWORKPARTTIMEDATE = flt_CNV_CLAIM_df['RETURNTOWORKPARTTIMEDATE'].iat[0]
            compare_Date_format(XLRETURNTOWORKPARTTIMEDATE, "%Y-%m-%d %H:%M:%S.%f", FINRETURNTOWORKPARTTIMEDATE, "RETURNTOWORKPARTTIMEDATE")  
        else:
             FINRETURNTOWORKPARTTIMEDATE = ''
             XLRETURNTOWORKPARTTIMEDATE = flt_CNV_CLAIM_df['RETURNTOWORKPARTTIMEDATE'].iat[0]
             compare_String(XLRETURNTOWORKPARTTIMEDATE, FINRETURNTOWORKPARTTIMEDATE, "RETURNTOWORKPARTTIMEDATE")  
           
        #-----------------------
        Click(driver,"//div[contains(text(),'Occupation')][@class='TabOff']")
        time.sleep(3)
        Click(driver,"//input[contains(@id,'_OccupationList_cmdView')]")
        time.sleep(1)
        
        OccCode = driver.find_element_by_xpath("//span[contains(@id,'_occupationCodeDynamicDropDownBean')]").text
        finOccCode = OccCode.split(":")[0]
        finOccCode = finOccCode.strip()
        xlOccCode = flt_CNV_CLAIM_df['OCCUPATION'].iat[0]
        # commenting temporarily until the bug is fixed
        #compare_String(xlOccCode, finOccCode, 'OCCUPATION')
        
        #OccStrn = driver.find_element_by_xpath("//table[contains(@id,'_OccupationList')]/tbody/tr/td[4]").text
        #sql1 = "select fineos_enum_id from dbo.fineos_enum_xref where fineos_enum_instnc_name= '" + OccStrn + "' and Fineos_DOm_name = 'OCOccupation Job Strenuousness'"
        #finOccStrn = getEnum(sql1)
        #xlOccStrn = flt_CNV_CLAIM_df['OCCUPATIONSTRENGTH'].iat[0]
        #compare_String(xlOccStrn, finOccStrn, 'OCCUPATIONSTRENGTH')
        
        Click(driver, "//input[contains(@id,'cmdPageBack_cloned')]")
        
        finErngStrdDt = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[2]").text
        xlErngStrdDt = flt_CNV_CLAIM_df['PDI_EARNINGS_SDATE'].iat[0]
        compare_Date_format(xlErngStrdDt, "%Y-%m-%d %H:%M:%S.%f", finErngStrdDt, "PDI_EARNINGS_SDATE")  
        
        #finErngEndDt = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[3]").text
        #xlErngEndDt = flt_CNV_CLAIM_df['PDI_EARNINGS_EDATE'].iat[0]
        #compare_Date_format(xlErngEndDt, "%Y-%m-%d %H:%M:%S.%f", finErngEndDt, "PDI_EARNINGS_EDATE")  
        
        ErngFreq = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[8]").text 
        sql2 = "select fineos_enum_id from dbo.fineos_enum_xref where fineos_enum_instnc_name= '" + ErngFreq +"' and Fineos_DOm_name = 'OCOccupation Salary Frequency'"
        finErngFreq = getEnum(sql2)
        xlErngFreq = flt_CNV_CLAIM_df['PDI_SALARY_FREQUENCY'].iat[0]
        compare_String(xlErngFreq, finErngFreq, 'PDI_SALARY_FREQUENCY')
        
        finEarnings = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[7]").text
        finEarnings = finEarnings.replace(",","")
        try:
            finEarnings = str('%.2f' %(float(finEarnings)))
        except:
           finEarnings = ''
           
        xlEarnings = flt_CNV_CLAIM_df['CURRENTEARININGS'].iat[0]
        xlEarnings = str('%.2f' %(float(xlEarnings)))
        compare_String(xlEarnings, finEarnings, 'CURRENTEARININGS')
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Medical')][@class='TabOff']")) == 0:
            Click(driver, "//input[contains(@id,'cmdPageBack_cloned')]")
        else:
             pass   
        
        """Click(driver,"//input[contains(@id,'_OccupationList_cmdView')]")
        time.sleep(5)
        time.sleep(5)
        finRETURNTOWORKPARTTIMEDATE = ''
        try:
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr"))
            for xx in range(1,rowcnt+1):
                if driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[1]").text == 'Partial Earnings':
                    finRETURNTOWORKPARTTIMEDATE = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[2]").text 
            xlRETURNTOWORKPARTTIMEDATE = flt_CNV_CLAIM_df['RETURNTOWORKPARTTIMEDATE'].iat[0]
            #compare_Date_format(xlRETURNTOWORKPARTTIMEDATE, "%Y-%m-%d %H:%M:%S.%f", finRETURNTOWORKPARTTIMEDATE, "RETURNTOWORKPARTTIMEDATE")  
        except:
            pass
        
        Click(driver,"//input[contains(@id,'_cmdPageBack_cloned')]")"""
        
        Click(driver,"//div[contains(text(),'Medical')][@class='TabOff']")
        time.sleep(5)
        if len(driver.find_elements_by_xpath("//span[contains(@id,'_expectedDelivery')][@class='DataLabel']"))>0:
            finEXPECTEDDELIVERYDATE = driver.find_element_by_xpath("//span[contains(@id,'_expectedDelivery')][@class='DataLabel']").text
        else:
             finEXPECTEDDELIVERYDATE = ''
               
        xlEXPECTEDDELIVERYDATE = flt_CNV_CLAIM_df['EXPECTEDDELIVERYDATE'].iat[0]
        compare_Date_format(xlEXPECTEDDELIVERYDATE, "%Y-%m-%d %H:%M:%S.%f", finEXPECTEDDELIVERYDATE, "EXPECTEDDELIVERYDATE")  
        
                      
        Click(driver,"//div[contains(text(),'General Claim')][@class='TabOff']")
        time.sleep(3)
        if finBen.strip() == 'STD':
            finclmcompDate = driver.find_element_by_xpath("//span[contains(@id,'_employeeStatReceivedDateBean')][contains(@class,'DataLabel')]").text
        else:    
             finclmcompDate = driver.find_element_by_xpath("//span[contains(@id,'_employerStatReceivedDateBean')][contains(@class,'DataLabel')]").text
            
        if finclmcompDate == '-':
            finclmcompDate = ''
        xlclmcompDate =  flt_CNV_CLAIM_df['CLAIMCOMPLETIONDATE'].iat[0]
        compare_Date_format(xlclmcompDate, "%Y-%m-%d %H:%M:%S.%f", finclmcompDate, "CLAIMCOMPLETIONDATE") 
        #xlclmcompNDate = flt_CNV_CLAIM_df['CLAIMCOMPLETEDNDATE'].iat[0]
        #compare_Date_format(xlclmcompDate, "%Y-%m-%d %H:%M:%S.%f", finclmcompDate, "CLAIMCOMPLETEDNDATE") 
        
        finDateUnabletoWork = driver.find_element_by_xpath("//span[contains(@id,'_DateFirstUnableToWork')][contains(@class,'DataLabel')]").text
        if finDateUnabletoWork == '-' :
            finDateUnabletoWork = ''
        xlDateUnabletoWork = flt_CNV_CLAIM_df['DATELASTWORKED'].iat[0]
        compare_Date_format(xlDateUnabletoWork, "%Y-%m-%d %H:%M:%S.%f", finDateUnabletoWork, "DATELASTWORKED")
       
        
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)
 
def Migration_Extract_CNV_CLAIM_2(driver):
    global caseStatus
    global Pass_df 
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
    driver.maximize_window()
    
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_2.csv", delimiter = '|',encoding='cp1252', dtype = str)
    #CNV_CLAIM_BENEFITRIGHT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_BENEFITRIGHT.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    caseids = CNV_CLAIM_df['CASEID'].unique()
    for caseid in caseids:
        caseStatus = 1
        #flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_BENEFITRIGHT_df = CNV_CLAIM_BENEFITRIGHT_df[CNV_CLAIM_BENEFITRIGHT_df['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_CASENOTE_df  = CNV_CLAIM_CASENOTE_df [CNV_CLAIM_CASENOTE_df ['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_CODE_df = CNV_CLAIM_CODE_df[CNV_CLAIM_CODE_df['CASEID'].isin(caseids)]
        #flt_CNV_CLAIM_COVERAGE_df = CNV_CLAIM_COVERAGE_df[CNV_CLAIM_COVERAGE_df['CASEID'].isin(caseids)]
        #combinedDF = flt_CNV_CLAIM_df.merge(flt_CNV_CLAIM_BENEFITRIGHT_df,on = 'CASEID',how='outer')
        #combinedDF = combinedDF.merge(flt_CNV_CLAIM_CASENOTE_df,on = 'CASEID',how='outer')
        #combinedDF = combinedDF.merge(flt_CNV_CLAIM_CODE_df,on = 'CASEID',how='outer')
        #combinedDF = combinedDF.merge(flt_CNV_CLAIM_COVERAGE_df,on = 'CASEID',how='outer')   
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'] ==caseid]
        #flt_CNV_CLAIM_BENEFITRIGHT_df = CNV_CLAIM_BENEFITRIGHT_df[CNV_CLAIM_BENEFITRIGHT_df['CASEID'] ==caseid]
        #flt_CNV_CLAIM_CASENOTE_df  = CNV_CLAIM_CASENOTE_df [CNV_CLAIM_CASENOTE_df ['CASEID'] ==caseid]
        #flt_CNV_CLAIM_CODE_df = CNV_CLAIM_CODE_df[CNV_CLAIM_CODE_df['CASEID'] ==caseid]
        #flt_CNV_CLAIM_COVERAGE_df = CNV_CLAIM_COVERAGE_df[CNV_CLAIM_COVERAGE_df['CASEID'] ==caseid]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        global caseno
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        html_logger.info("Start of the Case number " + str(caseno))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        if len(driver.find_elements_by_xpath("//h2[contains(@title,'Group Disability Claim')]")) > 0:
            finClaimType = 'Group Disability Claim'
        xlClaimType = flt_CNV_CLAIM_df['CASETYPE'].iat[0]
        compare_String(xlClaimType, finClaimType, "CASETYPE")
        
        
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        finClmStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd[1]").text
        #caseStatus - need to get mapping 
        
        benefit = driver.find_element_by_xpath("//a[contains(text(),'Group Disability Claim')]/parent::div/following-sibling::div/a") .text
        finBen = benefit.split(" ")[0]
        finBen1 = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finBen + "'  and fineos_dom_name = 'ClaimType'")
        xlBen = flt_CNV_CLAIM_df['CLAIMTYPE'].iat[0]
        compare_String(xlBen, finBen1, "CLAIMTYPE")

        
        finEventDate = driver.find_element_by_xpath("//span[contains(@id,'_IncurredDate')][contains(@class,'DataLabel')]").text   
        xlEventDate = flt_CNV_CLAIM_df['EVENTDATE'].iat[0]
        compare_Date_format(xlEventDate, "%Y-%m-%d %H:%M:%S.%f", finEventDate, "EVENTDATE")  #%Y-%m-%d %H:%M:%S')  %Y-%m-%d %H:%M:%S.%f
             
        finRTW = driver.find_element_by_xpath("//span[contains(@id,'_ExpectedRTWFullTimeDate')][@class='DataLabel']").text
        xlRTW = flt_CNV_CLAIM_df['EXPECTEDRETURNTOWORKDATE'].iat[0]
        compare_Date_format(xlRTW, "%Y-%m-%d %H:%M:%S.%f", finRTW, "EXPECTEDRETURNTOWORKDATE")  
        
        finNotDate = driver.find_element_by_xpath("//span[contains(@id,'_notificationDate')][@class='DataLabel']").text
        xlNotDate = flt_CNV_CLAIM_df['NOTIFICATIONDATE'].iat[0]
        compare_Date_format(xlNotDate, "%Y-%m-%d %H:%M:%S.%f", finNotDate, "NOTIFICATIONDATE")  
        
        finDtofHire = driver.find_element_by_xpath("//span[contains(@id,'_dateOfHire')][contains(@class,'DataLabel')]").text
        xlDtorHire = flt_CNV_CLAIM_df['DATEOFHIRE'].iat[0]
        compare_Date_format(xlDtorHire, "%Y-%m-%d %H:%M:%S.%f", finDtofHire, "DATEOFHIRE")  
        
        finSickAcci = driver.find_element_by_xpath("//span[contains(@id,'_SicknessAccident')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" +finSickAcci +"'  and fineos_dom_name = 'Event Type'"
        finSickAcci = getEnum(sql1)
        xlSickAcci = flt_CNV_CLAIM_df['SICKNESS_ACCIDENT'].iat[0]
        compare_String(xlSickAcci, finSickAcci, 'SICKNESS_ACCIDENT')
        
        Click(driver,"//div[contains(text(),'Occupation')][@class='TabOff']")
        time.sleep(3)
        OccCode = driver.find_element_by_xpath("//table[contains(@id,'_OccupationList')]/tbody/tr/td[5]").text
        finOccCode = OccCode.split(":")[0]
        finOccCode = finOccCode.strip()
        xlOccCode = flt_CNV_CLAIM_df['OCCUPATION'].iat[0]
        compare_String(xlOccCode, finOccCode, 'OCCUPATION')
        
        OccStrn = driver.find_element_by_xpath("//table[contains(@id,'_OccupationList')]/tbody/tr/td[4]").text
        sql1 = "select fineos_enum_id from dbo.fineos_enum_xref where fineos_enum_instnc_name= '" + OccStrn + "' and Fineos_DOm_name = 'OCOccupation Job Strenuousness'"
        finOccStrn = getEnum(sql1)
        xlOccStrn = flt_CNV_CLAIM_df['OCCUPATIONSTRENGTH'].iat[0]
        compare_String(xlOccStrn, finOccStrn, 'OCCUPATIONSTRENGTH')
        
        finErngStrdDt = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[2]").text
        xlErngStrdDt = flt_CNV_CLAIM_df['PDI_EARNINGS_SDATE'].iat[0]
        compare_Date_format(xlErngStrdDt, "%Y-%m-%d %H:%M:%S.%f", finErngStrdDt, "PDI_EARNINGS_SDATE")  
        
        #finErngEndDt = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[3]").text
        #xlErngEndDt = flt_CNV_CLAIM_df['PDI_EARNINGS_EDATE'].iat[0]
        #compare_Date_format(xlErngEndDt, "%Y-%m-%d %H:%M:%S.%f", finErngEndDt, "PDI_EARNINGS_EDATE")  
        
        ErngFreq = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[8]").text 
        sql2 = "select fineos_enum_id from dbo.fineos_enum_xref where fineos_enum_instnc_name= '" + ErngFreq +"' and Fineos_DOm_name = 'OCOccupation Salary Frequency'"
        finErngFreq = getEnum(sql2)
        xlErngFreq = flt_CNV_CLAIM_df['PDI_SALARY_FREQUENCY'].iat[0]
        compare_String(xlErngFreq, finErngFreq, 'PDI_SALARY_FREQUENCY')
        
        finEarnings = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr/td[7]").text
        finEarnings = finEarnings.replace(",","")
        try:
            finEarnings = str('%.2f' %(float(finEarnings)))
        except:
           finEarnings = ''
           
        xlEarnings = flt_CNV_CLAIM_df['CURRENTEARININGS'].iat[0]
        xlEarnings = str('%.2f' %(float(xlEarnings)))
        compare_String(xlEarnings, finEarnings, 'CURRENTEARININGS')
        
        Click(driver,"//input[contains(@id,'_OccupationList_cmdView')]")
        time.sleep(5)
        time.sleep(5)
        finRETURNTOWORKPARTTIMEDATE = ''
        try:
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr"))
            for xx in range(1,rowcnt+1):
                if driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[1]").text == 'Partial Earnings':
                    finRETURNTOWORKPARTTIMEDATE = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[2]").text 
            xlRETURNTOWORKPARTTIMEDATE = flt_CNV_CLAIM_df['RETURNTOWORKPARTTIMEDATE'].iat[0]
            compare_Date_format(xlRETURNTOWORKPARTTIMEDATE, "%Y-%m-%d %H:%M:%S.%f", finRETURNTOWORKPARTTIMEDATE, "RETURNTOWORKPARTTIMEDATE")  
        except:
            pass
        
        Click(driver,"//input[contains(@id,'_cmdPageBack_cloned')]")
        
        Click(driver,"//div[contains(text(),'Medical')][@class='TabOff']")
        time.sleep(5)
        if len(driver.find_elements_by_xpath("//span[contains(@id,'_expectedDelivery')][@class='DataLabel']"))>0:
            finEXPECTEDDELIVERYDATE = driver.find_element_by_xpath("//span[contains(@id,'_expectedDelivery')][@class='DataLabel']").text
        else:
             finEXPECTEDDELIVERYDATE = ''
               
        xlEXPECTEDDELIVERYDATE = flt_CNV_CLAIM_df['EXPECTEDDELIVERYDATE'].iat[0]
        compare_Date_format(xlEXPECTEDDELIVERYDATE, "%Y-%m-%d %H:%M:%S.%f", finEXPECTEDDELIVERYDATE, "EXPECTEDDELIVERYDATE")  
        
                      
        Click(driver,"//div[contains(text(),'General Claim')][@class='TabOff']")
        time.sleep(3)
        if finBen.strip() == 'STD':
            finclmcompDate = driver.find_element_by_xpath("//span[contains(@id,'_employeeStatReceivedDateBean')][contains(@class,'DataLabel')]").text
        else:    
             finclmcompDate = driver.find_element_by_xpath("//span[contains(@id,'_employerStatReceivedDateBean')][contains(@class,'DataLabel')]").text
            
        if finclmcompDate == '-':
            finclmcompDate = ''
        xlclmcompDate =  flt_CNV_CLAIM_df['CLAIMCOMPLETIONDATE'].iat[0]
        compare_Date_format(xlclmcompDate, "%Y-%m-%d %H:%M:%S.%f", finclmcompDate, "CLAIMCOMPLETIONDATE") 
        #xlclmcompNDate = flt_CNV_CLAIM_df['CLAIMCOMPLETEDNDATE'].iat[0]
        #compare_Date_format(xlclmcompDate, "%Y-%m-%d %H:%M:%S.%f", finclmcompDate, "CLAIMCOMPLETEDNDATE") 
        
        finDateUnabletoWork = driver.find_element_by_xpath("//span[contains(@id,'_DateFirstUnableToWork')][contains(@class,'DataLabel')]").text
        if finDateUnabletoWork == '-' :
            finDateUnabletoWork = ''
        xlDateUnabletoWork = flt_CNV_CLAIM_df['DATELASTWORKED'].iat[0]
        compare_Date_format(xlDateUnabletoWork, "%Y-%m-%d %H:%M:%S.%f", finDateUnabletoWork, "DATELASTWORKED")
       
        
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)
              
def Migration_Extract_CNV_BENEFIT(driver):
    global caseStatus
    global Pass_df 
    global caseno
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
        #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_All.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_CLAIM_BENEFIT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_BENEFIT.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    caseids = CNV_CLAIM_BENEFIT_df['CASEID'].unique()
    for caseid in caseids:
        caseStatus = 1 
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'] ==caseid]
        flt_CNV_CLAIM_BENEFIT_df = CNV_CLAIM_BENEFIT_df[CNV_CLAIM_BENEFIT_df['CASEID'] ==caseid]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        flt_CNV_CLAIM_BENEFIT_df.reset_index(drop=True)
        
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        sicInd = flt_CNV_CLAIM_df['SICKNESS_ACCIDENT'].to_string(index=False)
        
        html_logger.info("Start of CNV_BENEFIT for the Case number " + str(caseno) + " with case id " + str(caseid))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        xlBenCaseType = flt_CNV_CLAIM_BENEFIT_df['BENEFITCASETYPE'].iat[0]
        
        if len(driver.find_elements_by_xpath("//a[text()='" + xlBenCaseType +"']")) > 0:
            Click(driver,"//a[text()='" + xlBenCaseType +"']")
            time.sleep(3)
            finBenCaseType = driver.find_element_by_xpath("//a[text()='" + xlBenCaseType +"']").text
        else:
            finBenCaseType = '' 
        compare_String(xlBenCaseType, finBenCaseType, "BENEFITCASETYPE")
        
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        finBenStatus = driver.find_element_by_xpath("//dt[text()='Status']/following::dd[1]").text
        xlBenStatus = flt_CNV_CLAIM_BENEFIT_df['STATUS'].iat[0]
        compare_String(xlBenStatus, finBenStatus, "STATUS")
        #caseStatus - need to get mapping 
        
        finBenStartDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_Start_Date')][contains(@class,'DataLabel')]").text
        xlBenStartDate = flt_CNV_CLAIM_BENEFIT_df['STARTDATE'].iat[0]
        compare_Date_format(xlBenStartDate, "%Y-%m-%d %H:%M:%S.%f", finBenStartDate, "STARTDATE")  #%Y-%m-%d %H:%M:%S')  %Y-%m-%d %H:%M:%S.%f
        
        finBenEndDate = driver.find_element_by_xpath("//span[contains(@id,'_Approval_End_Date')][contains(@class,'DataLabel')]").text
        xlBenEndDate = flt_CNV_CLAIM_BENEFIT_df['BENEFITENDDATE'].iat[0]
        compare_Date_format_minus1day(xlBenEndDate, "%Y-%m-%d %H:%M:%S.%f", finBenEndDate, "BENEFITENDDATE") 
        
        xlEndDate = flt_CNV_CLAIM_BENEFIT_df['ENDDATE'].iat[0]
        compare_Date_format_minus1day(xlEndDate, "%Y-%m-%d %H:%M:%S.%f", finBenEndDate, "ENDDATE") 
        
        finDateAllCollDate = driver.find_element_by_xpath("//span[contains(@id,'_Date_All_Data_Collected')][contains(@class,'DataLabel')]").text
        xlDateAllCollDate = flt_CNV_CLAIM_BENEFIT_df['DATEALLDATACOLLECTED'].iat[0]
        compare_Date_format(xlDateAllCollDate, "%Y-%m-%d %H:%M:%S.%f", finDateAllCollDate, "DATEALLDATACOLLECTED")
        
        if finBenStatus == 'Closed':
            finFINISHDATE = driver.find_element_by_xpath("//span[contains(@id,'_Termination_Date')][contains(@class,'DataLabel')]").text
        else:
            finFINISHDATE = ''    
        xlFINISHDATE = flt_CNV_CLAIM_BENEFIT_df['FINISHDATE'].iat[0]
        compare_Date_format(xlFINISHDATE, "%Y-%m-%d %H:%M:%S.%f", finFINISHDATE, "FINISHDATE")
        
        Click(driver,"//div[contains(text(),'Case History')][@class='TabOff']")
        time.sleep(3)
        Click(driver,"//a[contains(@id,'_DROPDOWN.WidgetListContentSource.NewsFeedForCaseWidgetListlink')]")
        Click(driver,"//input[contains(@id,'NewsFeedForCaseWidgetList_All_Allcheck_CHECKBOX')]")
        time.sleep(3)
        Click(driver,"//input[contains(@id,'Process_Changes_Process_Changescheck_CHECKBOX')]")
        Click(driver,"//input[contains(@id,'NewsFeedForCaseWidgetList_applyChanges_applyChangesbutton')]")
        
        time.sleep(3)
        #Click(driver,"//a[@title='Advanced Filters']")
        Click(driver,"//span[contains(text(),'Advanced Filters')]")
        time.sleep(1)
        if len(driver.find_elements_by_xpath("//input[contains(@id,'_FromDate')]")) == 0:
            Click(driver,"//span[contains(text(),'Advanced Filters')]")
            time.sleep(1)
            
        driver.find_element_by_xpath("//input[contains(@id,'_FromDate')]").clear()
        time.sleep(1)
        Set(driver,"//input[contains(@id,'_FromDate')]","01/01/2000")
        Click(driver,"//input[contains(@id,'_applyFilterChanges')]")
        time.sleep(3)
        
        if len(driver.find_elements_by_xpath("//span[contains(text(),'Managing Process Stage changed to " +finBenStatus+ "')]")) > 0:
            finReason = driver.find_element_by_xpath("//span[contains(text(),'Managing Process Stage changed to " +finBenStatus+ "')]/following::span[contains(@id,'reason')][@class='DataLabel']").text
            finReason = finReason.strip()
            xlReason = flt_CNV_CLAIM_BENEFIT_df['STATUSREASON'].iat[0]
            compare_String(xlReason, finReason, "STATUSREASON")   
        #//span[contains(text(),'Managing Process Stage changed to ' +Closed+ ')]/following::span[contains(@id,'reason')][@class='DataLabel']
            
            finstatusDate = driver.find_element_by_xpath("//span[contains(text(),'Managing Process Stage changed to " +finBenStatus+ "')]//preceding::div[@class='dateDivider'][1]/span").text
            finstatusDate = finstatusDate.split(", ")[1].strip()
            datetimeobject = datetime.strptime(str(finstatusDate),'%b %d %Y')
            finSTATUSDATE = datetimeobject.strftime('%m/%d/%Y')
            xlSTATUSDATE = flt_CNV_CLAIM_BENEFIT_df['STATUSDATE'].iat[0]
            compare_Date_format(xlSTATUSDATE, "%Y-%m-%d %H:%M:%S.%f", finSTATUSDATE, "STATUSDATE") 
            
            if len(driver.find_elements_by_xpath("//span[contains(text(),'Managing Process Stage changed to Approved')]//preceding::div[@class='dateDivider'][1]/span"))>0:
                   finRECEIVEDDATE = driver.find_element_by_xpath("//span[contains(text(),'Managing Process Stage changed to Approved')]//preceding::div[@class='dateDivider'][1]/span").text
                   finRECEIVEDDATE = finRECEIVEDDATE.split(", ")[1].strip()
                   datetimeobject = datetime.strptime(str(finRECEIVEDDATE),'%b %d %Y')
                   finRECEIVEDDATE = datetimeobject.strftime('%m/%d/%Y')
            else:
                finRECEIVEDDATE = ''
            xlRECEIVEDDATE = flt_CNV_CLAIM_BENEFIT_df['RECEIVEDDATE'].iat[0]
            compare_Date_format(xlRECEIVEDDATE, "%Y-%m-%d %H:%M:%S.%f", finRECEIVEDDATE, "RECEIVEDDATE") 
            
        Click(driver,"//div[contains(text(),'Benefit')][@class='TabOff']")
        time.sleep(3)
        
        finEFFECTIVECOVERAGEDATE = driver.find_element_by_xpath("//span[contains(@id,'_earliestDateForClaim')][contains(@class,'DataLabel')]").text
        xlEFFECTIVECOVERAGEDATE = flt_CNV_CLAIM_BENEFIT_df['EFFECTIVECOVERAGEDATE'].iat[0]
        compare_Date_format(xlEFFECTIVECOVERAGEDATE, "%Y-%m-%d %H:%M:%S.%f", finEFFECTIVECOVERAGEDATE, "EFFECTIVECOVERAGEDATE") 
        
        if len(driver.find_elements_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr"))> 0:
            txtADJUSTMENTDESCRIPTION = driver.find_element_by_xpath("//table[contains(@id,'_EligibilityLimitsListviewWidget')]/tbody/tr/td[1]").text
            finADJUSTMENTDESCRIPTION = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + txtADJUSTMENTDESCRIPTION + "'  and fineos_dom_name = 'EligibilityLimitsType'")
        else:
            finADJUSTMENTDESCRIPTION = ''
        xlADJUSTMENTDESCRIPTION = flt_CNV_CLAIM_BENEFIT_df['ADJUSTMENTDESCRIPTION'].iat[0]
        compare_float(xlADJUSTMENTDESCRIPTION, finADJUSTMENTDESCRIPTION, "ADJUSTMENTDESCRIPTION")    
        
        finINLITIGATION = 'N'
        xlINLITIGATION =  flt_CNV_CLAIM_BENEFIT_df['INLITIGATION'].iat[0]
        compare_String(xlINLITIGATION, finINLITIGATION, "INLITIGATION")
        
        
        #----- c;lick COLA if LTD-------------------
        if xlBenCaseType == 'STD Benefit':
            finAPPLYCOLA = 0
        else:
            Click(driver,"//div[contains(text(),'COLA Info')][@class='TabOff']")
            time.sleep(3)
            if len(driver.find_elements_by_xpath("//input[contains(@id,'_toggleApplyIndexationButton')]"))>0:
                finAPPLYCOLA = 0
            else:    
                finAPPLYCOLA = 1 # add cola screens here
            
        xlAPPLYCOLA = flt_CNV_CLAIM_BENEFIT_df['APPLYCOLA'].iat[0]
        compare_float(xlAPPLYCOLA,finAPPLYCOLA, "APPLYCOLA") 
        
        
        Click(driver,"//div[contains(text(),'Benefit Parameters')][@class='TabOff']")
        time.sleep(3)
        if len(driver.find_elements_by_xpath("//img[contains(@id,'_hospitalizationClauseAppliesCheckBoxBean')][@checked='false']"))>0:
            finHOSPITALIZATIONAPPIES = 'N'
        elif len(driver.find_elements_by_xpath("//img[contains(@id,'_hospitalizationClauseAppliesCheckBoxBean')][@checked='true']"))>0:
            finHOSPITALIZATIONAPPIES = 'Y'   
        else:
            finHOSPITALIZATIONAPPIES = ''
        xlHOSPITALIZATIONAPPIES = flt_CNV_CLAIM_BENEFIT_df['HOSPITALIZATIONAPPIES'].iat[0]
        compare_String(xlHOSPITALIZATIONAPPIES,finHOSPITALIZATIONAPPIES, "HOSPITALIZATIONAPPIES") 
        
        finELIMINATIONPERIODBASIS = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriodBasis')][contains(@class,'DataLabel')]").text
        finELIMINATIONPERIOD = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriod')][contains(@class,'DataLabel')]").text

        finELIMINATIONPERIODBASIS1 = driver.find_element_by_xpath("//span[contains(@id,'_EliminationAccidentPeriodBasis')][contains(@class,'DataLabel')]").text
        finELIMINATIONPERIOD1 = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriodAccident')][contains(@class,'DataLabel')]").text
        
        finELIMINATIONPERIODBASIS2 = driver.find_element_by_xpath("//span[contains(@id,'_EliminationHospitalPeriodBasis')][contains(@class,'DataLabel')]").text
        finELIMINATIONPERIOD2 = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriodHospital')][contains(@class,'DataLabel')]").text
        
        if xlBenCaseType == 'LTD Benefit':
            """finDEFINITIONOFDISABILITYPERIODBASIS = driver.find_element_by_xpath("//span[contains(@id,'_benQualPeriodBasis')][contains(@class,'DataLabel')]").text
            finDEFINITIONOFDISABILITYPERIODBASIS = finDEFINITIONOFDISABILITYPERIODBASIS.replace("(","").replace(")","")
            xlDEFINITIONOFDISABILITYPERIODBASIS = flt_CNV_CLAIM_BENEFIT_df['DEFINITIONOFDISABILITYPERIODBASIS'].iat[0]
            compare_String(xlDEFINITIONOFDISABILITYPERIODBASIS, finDEFINITIONOFDISABILITYPERIODBASIS, "DEFINITIONOFDISABILITYPERIODBASIS")""" 
            
            finDEFINITIONOFDISABILITYTYPE = 'OWN'
            xlDEFINITIONOFDISABILITYTYPE = flt_CNV_CLAIM_BENEFIT_df['DEFINITIONOFDISABILITYTYPE'].iat[0]
            compare_String(xlDEFINITIONOFDISABILITYTYPE, finDEFINITIONOFDISABILITYTYPE, "DEFINITIONOFDISABILITYTYPE") 
            
            finDEFINITIONOFDISABILITYPERIOD = driver.find_element_by_xpath("//span[contains(@id,'_benQualPeriod')][contains(@class,'DataLabel')]").text
            finDEFINITIONOFDISABILITYPERIOD = finDEFINITIONOFDISABILITYPERIOD.replace("(","").replace(")","")
            xlDEFINITIONOFDISABILITYPERIOD = flt_CNV_CLAIM_BENEFIT_df['DEFINITIONOFDISABILITYPERIOD'].iat[0]
            compare_String(xlDEFINITIONOFDISABILITYPERIOD, finDEFINITIONOFDISABILITYPERIOD, "DEFINITIONOFDISABILITYPERIOD") 
            
            
        
        
        Click(driver,"//div[contains(text(),'Calculation Parameters')][@class='TabOff']")
        time.sleep(3)
        
        """finALLSOURCEMAXIMUMPERCENTAGE = driver.find_element_by_xpath("//span[contains(@id,'_overridePercentage')]").text
        xlALLSOURCEMAXIMUMPERCENTAGE = flt_CNV_CLAIM_BENEFIT_df['ALLSOURCEMAXIMUMPERCENTAGE'].iat[0]
        compare_float(xlALLSOURCEMAXIMUMPERCENTAGE, finALLSOURCEMAXIMUMPERCENTAGE, "ALLSOURCEMAXIMUMPERCENTAGE")
        
        finMAXPERCENTAGE = driver.find_element_by_xpath("//span[contains(@id,'_maxPercentage')]").text
        xlMAXPERCENTAGE = flt_CNV_CLAIM_BENEFIT_df['MAXPERCENTAGE'].iat[0]
        compare_float(xlMAXPERCENTAGE, finMAXPERCENTAGE, "MAXPERCENTAGE")"""
        
        finMAXBENEFIT = driver.find_element_by_xpath("//span[contains(@id,'_MaximumAmount')][contains(@class,'DataLabel')]").text
        xlMAXBENEFITD = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFIT'].iat[0]
        compare_float(xlMAXBENEFITD, finMAXBENEFIT, "MAXBENEFIT")
        
        finBENEFITTAXABLEPERCENT= driver.find_element_by_xpath("//span[contains(@id,'_employeeContributionPercentage')][contains(@class,'DataLabel')]").text
        #finBENEFITTAXABLEPERCENT = 100-float(finBENEFITTAXABLEPERCENT)
        xlBENEFITTAXABLEPERCENT = flt_CNV_CLAIM_BENEFIT_df['BENEFITTAXABLEPERCENT'].iat[0]
        compare_float(xlBENEFITTAXABLEPERCENT, finBENEFITTAXABLEPERCENT, "BENEFITTAXABLEPERCENT")
        
        finEMPLOYEECONTRIBUTION = driver.find_element_by_xpath("//span[contains(@id,'_contributoryStatus')][contains(@class,'DataLabel')]").text
        if finEMPLOYEECONTRIBUTION == 'Non-contributory':
            finEMPLOYEECONTRIBUTION = 0
        else:
            finEMPLOYEECONTRIBUTION = 1   
        xlEMPLOYEECONTRIBUTION = flt_CNV_CLAIM_BENEFIT_df['EMPLOYEECONTRIBUTION'].iat[0]
        compare_float(xlEMPLOYEECONTRIBUTION, finEMPLOYEECONTRIBUTION, "EMPLOYEECONTRIBUTION")
        
        finCHANGEOFDEFINITIONDATE = '0'
        xlCHANGEOFDEFINITIONDATE = flt_CNV_CLAIM_BENEFIT_df['CHANGEOFDEFINITIONDATE'].iat[0]
        compare_float(xlCHANGEOFDEFINITIONDATE, finCHANGEOFDEFINITIONDATE, "CHANGEOFDEFINITIONDATE")
        
        finPAYMENTFREQUENCY = driver.find_element_by_xpath("//span[contains(@id,'_Payment_Frequency')][contains(@class,'DataLabel')]").text
        xlPAYMENTFREQUENCY = flt_CNV_CLAIM_BENEFIT_df['PAYMENTFREQUENCY'].iat[0]
        finPAYMENTFREQUENCY = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finPAYMENTFREQUENCY +"'  and fineos_dom_name = 'Premium Payment Frequency'")
        compare_float(xlPAYMENTFREQUENCY, finPAYMENTFREQUENCY, "PAYMENTFREQUENCY")
        #----------------------------------------------
        driver.find_element_by_xpath("//div[contains(text(),'Payment History')][@class='TabOff']").click()
        time.sleep(5)
        finBENEFITTERMDATE = ""
        if len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr")) > 0:
            finBENEFITTERMDATE = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[1]/td[3]").text
        xlBENEFITTERMDATE = flt_CNV_CLAIM_BENEFIT_df['BENEFITTERMDATE'].iat[0]
        #compare_Date_format_minus1day(xlBENEFITTERMDATE, "%Y-%m-%d %H:%M:%S.%f", finBENEFITTERMDATE, "BENEFITTERMDATE")
        #-------------------------------------
        if 1 ==2: #xlBenCaseType == 'LTD Benefit' or xlBenCaseType == 'OLS Benefit':  #'STD Benefit'  COMMENTED AS IT'S NOT VALID ANYMORE
            datetimeobject = datetime.strptime(str(xlBenEndDate),'%Y-%m-%d %H:%M:%S.%f')
            finMEDDATE = datetimeobject.strftime('%m/%d/%Y')
        else:    
            Click(driver,"//div[contains(text(),'Recurring Payments')][@class='TabOff']")
            time.sleep(3)
            Click(driver,"//div[contains(text(),'Periods')][@class='TabOff']")
            time.sleep(3)
            if len(driver.find_elements_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr")) > 0:
                finMEDDATE = driver.find_element_by_xpath("//table[contains(@id,'_periodsListview')]/tbody/tr/td[5]").text
            else:
                finMEDDATE = finBENEFITTERMDATE
                
        xlMEDDATE = flt_CNV_CLAIM_BENEFIT_df['MEDDATE'].iat[0]  #'BENEFITTERMDATE'
        
        #compare_Date_format(xlMEDDATE, "%Y-%m-%d %H:%M:%S.%f", finMEDDATE, "MEDDATE")
        #compare_Date_format(xlMEDDATE, "%Y-%m-%d %H:%M:%S.%f", finMEDDATE, "MEDDATE")   #_minus1day
        compare_Date_format_minus1day(xlMEDDATE, "%Y-%m-%d %H:%M:%S.%f", finMEDDATE, "MEDDATE")
        
        
        
            
        Click(driver,"//a[contains(text(),'Group Disability Claim')]")
        time.sleep(3)
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        if sicInd != '68960004':

            finELIMINATIONPERIODTYPE = driver.find_element_by_xpath("//span[contains(@id,'_SicknessAccident')][contains(@class,'DataLabel')]").text
            xlELIMINATIONPERIODTYPE = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIODTYPE'].iat[0]
            finELIMINATIONPERIODTYPE = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finELIMINATIONPERIODTYPE + "'  and fineos_dom_name = 'Event Type'")
            compare_float(xlELIMINATIONPERIODTYPE, finELIMINATIONPERIODTYPE, "ELIMINATIONPERIODTYPE")
            
            if str(finELIMINATIONPERIODTYPE) == '68960002':
                finELIMINATIONPERIODBASIS = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finELIMINATIONPERIODBASIS + "'  and fineos_dom_name = 'General DurationUnits'")
                xlELIMINATIONPERIODBASIS = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIODBASIS'].iat[0] 
                compare_float(xlELIMINATIONPERIODBASIS, finELIMINATIONPERIODBASIS, "ELIMINATIONPERIODBASIS")        
                #finELIMINATIONPERIOD = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriod')][contains(@class,'DataLabel')]").text
                xlELIMINATIONPERIOD = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIOD'].iat[0]
                compare_float(xlELIMINATIONPERIOD, finELIMINATIONPERIOD, "ELIMINATIONPERIOD")
            elif str(finELIMINATIONPERIODTYPE) == '68960001':
                finELIMINATIONPERIODBASIS1 = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finELIMINATIONPERIODBASIS1 + "'  and fineos_dom_name = 'General DurationUnits'")
                xlELIMINATIONPERIODBASIS = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIODBASIS'].iat[0] 
                compare_float(xlELIMINATIONPERIODBASIS, finELIMINATIONPERIODBASIS1, "ELIMINATIONPERIODBASIS")        
                #finELIMINATIONPERIOD = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriodAccident')][contains(@class,'DataLabel')]").text
                xlELIMINATIONPERIOD = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIOD'].iat[0]
                compare_float(xlELIMINATIONPERIOD, finELIMINATIONPERIOD1, "ELIMINATIONPERIOD")
            else:
                finELIMINATIONPERIODBASIS2 = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finELIMINATIONPERIODBASIS2 + "'  and fineos_dom_name = 'General DurationUnits'")
                xlELIMINATIONPERIODBASIS = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIODBASIS'].iat[0] 
                compare_float(xlELIMINATIONPERIODBASIS, finELIMINATIONPERIODBASIS2, "ELIMINATIONPERIODBASIS")        
                #finELIMINATIONPERIOD = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriodAccident')][contains(@class,'DataLabel')]").text
                xlELIMINATIONPERIOD = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIOD'].iat[0]
                compare_float(xlELIMINATIONPERIOD, finELIMINATIONPERIOD2, "ELIMINATIONPERIOD")
        
        Click(driver,"//div[contains(text(),'Coverages')][@class='TabOff']")
        time.sleep(3)
        Click(driver,"//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr/td[1]")
        time.sleep(3)
        #Click(driver,"//input[contains(@id,'_CoveragesLVW_cmdEdit')]")
        finBENEFITTERMDATE = driver.find_element_by_xpath("//table[contains(@id,'_CoveragesLVW')]/tbody/tr/td[6]").text     
        xlBENEFITTERMDATE = flt_CNV_CLAIM_BENEFIT_df['BENEFITTERMDATE'].iat[0]
        #compare_Date_format_minus1day(xlBENEFITTERMDATE, "%Y-%m-%d %H:%M:%S.%f", finBENEFITTERMDATE, "BENEFITTERMDATE")
        # commented temp until fix
        #compare_Date_format(xlBENEFITTERMDATE, "%Y-%m-%d %H:%M:%S.%f", finBENEFITTERMDATE, "BENEFITTERMDATE") 
        
        
        Click(driver,"//div[contains(text(),'Occupation')][@class='TabOff']")
        time.sleep(3)
        Click(driver,"//input[contains(@id,'_OccupationList_cmdView')]")
        time.sleep(5)
        
        finEMPLOYEEID = driver.find_element_by_xpath("//span[contains(@id,'_EmployeeId')][contains(@class,'DataLabel')]").text
        if finEMPLOYEEID =='-':
            finEMPLOYEEID = ''
        xlEMPLOYEEID = flt_CNV_CLAIM_BENEFIT_df['EMPLOYEEID'].iat[0]
        compare_String(xlEMPLOYEEID, finEMPLOYEEID, "EMPLOYEEID")
        
        finPARTIALDISABILITYSTARTDATE = ''
        finPARTIALDISABILITYENDDATE = ''
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr"))
        pardisflg = 0
        for xx in range(1,rowcnt+1):
            if driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[1]").text == 'Partial Earnings':
                pardisflg = 1
                finPARTIALDISABILITYSTARTDATE = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[2]").text 
                finPARTIALDISABILITYENDDATE = driver.find_element_by_xpath("//table[contains(@id,'_DatedEarnings')]/tbody/tr[" + str(xx) + "]/td[3]").text
                
        if pardisflg==0:
            finPARTIALDISABILITYEMPLOYER = '70496000'
        else:
            finPARTIALDISABILITYEMPLOYER = '70496001'
        xlPARTIALDISABILITYEMPLOYER = flt_CNV_CLAIM_BENEFIT_df['PARTIALDISABILITYEMPLOYER'].iat[0]    
        compare_float(xlPARTIALDISABILITYEMPLOYER, finPARTIALDISABILITYEMPLOYER, "PARTIALDISABILITYEMPLOYER")
            
        
        #compare_String(xlELIMINATIONPERIOD, finELIMINATIONPERIOD, "ELIMINATIONPERIOD")
        xlPARTIALDISABILITYSTARTDATE = flt_CNV_CLAIM_BENEFIT_df['PARTIALDISABILITYSTARTDATE'].iat[0]
        xlPARTIALDISABILITYENDDATE = flt_CNV_CLAIM_BENEFIT_df['PARTIALDISABILITYENDDATE'].iat[0]
        compare_Date_format(xlPARTIALDISABILITYSTARTDATE, "%Y-%m-%d %H:%M:%S.%f", finPARTIALDISABILITYSTARTDATE, "PARTIALDISABILITYSTARTDATE")  
        compare_Date_format(xlPARTIALDISABILITYENDDATE, "%Y-%m-%d %H:%M:%S.%f", finPARTIALDISABILITYENDDATE, "PARTIALDISABILITYENDDATE")
        
        #finELIMINATIONPERIOD = driver.find_element_by_xpath("//span[contains(@id,'_EliminationPeriod')][contains(@class,'DataLabel')]").text
        #xlELIMINATIONPERIOD = flt_CNV_CLAIM_BENEFIT_df['ELIMINATIONPERIOD'].iat[0]
        #compare_String(xlELIMINATIONPERIOD, finELIMINATIONPERIOD, "ELIMINATIONPERIOD")
        
        Click(driver,"//input[contains(@id,'_cmdPageBack_cloned')]")
        
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus = 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)

def Migration_Extract_CNV_BENEFITRT(driver):
    global caseStatus
    global Pass_df 
    global caseno
    
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
    #driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
        #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_All.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_CLAIM_BENEFITRT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_BENEFITRIGHT.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    caseids = CNV_CLAIM_BENEFITRT_df['CASEID'].unique()
    for caseid in caseids:
        caseStatus == 1 
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'] ==caseid]
        flt_CNV_CLAIM_BENEFIT_df = CNV_CLAIM_BENEFITRT_df[CNV_CLAIM_BENEFITRT_df['CASEID'] ==caseid]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        flt_CNV_CLAIM_BENEFIT_df.reset_index(drop=True)
        
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        html_logger.info("Start of CNV_CLAIM_BENEFIT_Right for the Case number " + str(caseno) + " with Case id " + str(caseid))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        xlBENEFITTYPE = flt_CNV_CLAIM_BENEFIT_df['BENEFITTYPE'].iat[0]
        
        AccSick = driver.find_element_by_xpath("//span[contains(@id,'_SicknessAccident')][contains(@class,'DataLabel')]").text
        
        if len(driver.find_elements_by_xpath("//a[contains(text(),'Benefit')]")) > 0:
            Click(driver,"//a[contains(text(),'Benefit')]")
            time.sleep(3)
            finBENEFITTYPE = driver.find_element_by_xpath("//a[contains(text(),'Benefit')]").text
            finBENEFITTYPE = finBENEFITTYPE.split(" ")[0]
            sql1 = "select FINEOS_ENUM_ID from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_INSTNC_NAME  = '" + finBENEFITTYPE + "' and FINEOS_DOM_ID = 2170"
            finBENEFITTYPE = getEnum(sql1)
        else:
            finBENEFITTYPE = '' 
        compare_String(xlBENEFITTYPE, finBENEFITTYPE, "BENEFITTYPE")
        
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        Click(driver,"//div[contains(text(),'Benefit')][@class='TabOff']")
        time.sleep(3)
        finBENEFITRIGHTTYPE = driver.find_element_by_xpath("//span[contains(@id,'_type')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dbo.FINEOS_ENUM_XREF f where FINEOS_ENUM_INSTNC_NAME  = '" + finBENEFITRIGHTTYPE + "' and FINEOS_DOM_ID = 2170"
        finBENEFITRIGHTTYPE = getEnum(sql1)
        xlBENEFITRIGHTTYPE = flt_CNV_CLAIM_BENEFIT_df['BENEFITRIGHTTYPE'].iat[0]
        compare_String(xlBENEFITRIGHTTYPE, finBENEFITRIGHTTYPE,"BENEFITRIGHTTYPE")
        
        ''' these two to be left blank'''
        
        finWAITINGPERIODUNITS = driver.find_element_by_xpath("//span[contains(@id,'_LateEnrollmentPeriod')][contains(@class,'DataLabel')]").text
        xlWAITINGPERIODUNITS = flt_CNV_CLAIM_BENEFIT_df['WAITINGPERIODUNITS'].iat[0]
        compare_float(xlWAITINGPERIODUNITS, finWAITINGPERIODUNITS,"WAITINGPERIODUNITS")
        
        #finALTWAITINGPERIODUNITS = driver.find_element_by_xpath("//span[contains(@id,'_waitingPeriod')][contains(@class,'DataLabel')]").text
        xlALTERNATE_WAITINGPERIOD_UNITS = flt_CNV_CLAIM_BENEFIT_df['ALTERNATE_WAITINGPERIOD_UNITS'].iat[0]
        compare_float(xlALTERNATE_WAITINGPERIOD_UNITS, finWAITINGPERIODUNITS,"ALTERNATE_WAITINGPERIOD_UNITS")
        
        
        finWAITINGPERIODBASIS = driver.find_element_by_xpath("//span[contains(@id,'_BasisOfLateEnrollmentPeriod')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finWAITINGPERIODBASIS + "'  and fineos_dom_name = 'General DurationUnits'"
        finWAITINGPERIODBASIS = getEnum(sql1)  
        if str(finWAITINGPERIODBASIS) == '5568000':
            finWAITINGPERIODBASIS = '0'
        xlWAITINGPERIODBASIS = flt_CNV_CLAIM_BENEFIT_df['WAITINGPERIODBASIS'].iat[0]
        compare_float(xlWAITINGPERIODBASIS,finWAITINGPERIODBASIS,"WAITINGPERIODBASIS")
        
        #finALTWAITINGPERIODBASIS = driver.find_element_by_xpath("//span[contains(@id,'_waitingPeriodBasis')][contains(@class,'DataLabel')]").text
        #sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finALTWAITINGPERIODBASIS + "'  and fineos_dom_name = 'General DurationUnits'"
        #finALTWAITINGPERIODBASIS = getEnum(sql1)  
        #if str(finALTWAITINGPERIODBASIS) == '5568000':
        #   finALTWAITINGPERIODBASIS = '0'
        xlALTERNATE_WAITINGPERIOD_BASIS = flt_CNV_CLAIM_BENEFIT_df['ALTERNATE_WAITINGPERIOD_BASIS'].iat[0]
        compare_float(xlALTERNATE_WAITINGPERIOD_BASIS,finWAITINGPERIODBASIS,"ALTERNATE_WAITINGPERIOD_BASIS")
        
        
        finUSEGROSSSALARY = '1'
        xlUSEGROSSSALARY = flt_CNV_CLAIM_BENEFIT_df['USEGROSSSALARY'].iat[0]
        compare_String(xlUSEGROSSSALARY,finUSEGROSSSALARY,"USEGROSSSALARY")
        
        
        Click(driver,"//div[contains(text(),'Benefit Parameters')][@class='TabOff']")
        time.sleep(3)
        
        finSTARTDATECALCTYPE = driver.find_element_by_xpath("//span[contains(@id,'_benefitStartDateCalculationTypeDropDownBean')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finSTARTDATECALCTYPE +"'"
        finSTARTDATECALCTYPE = getEnum(sql1)  
        xlSTARTDATECALCTYPE = flt_CNV_CLAIM_BENEFIT_df['STARTDATECALCTYPE'].iat[0]
        compare_float(xlSTARTDATECALCTYPE, finSTARTDATECALCTYPE,"STARTDATECALCTYPE")
        
        finENDDATECALCTYPE = driver.find_element_by_xpath("//span[contains(@id,'_benefitEndDateCalculationTypeDropDownBean')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finENDDATECALCTYPE +"'"
        finENDDATECALCTYPE = getEnum(sql1)  
        xlENDDATECALCTYPE = flt_CNV_CLAIM_BENEFIT_df['ENDDATECALCTYPE'].iat[0]
        compare_float(xlENDDATECALCTYPE, finENDDATECALCTYPE,"ENDDATECALCTYPE")
        
        finDEFINITIONOFDISABILITYTYPE = driver.find_element_by_xpath("//span[contains(@id,'_benefitQualReqType')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dev.fineos_enum_xref where fineos_enum_instnc_name like '"+ finDEFINITIONOFDISABILITYTYPE +"' and fineos_dom_id = 2223"
        finDEFINITIONOFDISABILITYTYPE = getEnum(sql1)  
        xlDEFINITIONOFDISABILITYTYPE = flt_CNV_CLAIM_BENEFIT_df['DISABILITYDEFINITION'].iat[0]
        #--commented this as this field is blank for now
        #compare_float(xlENDDATECALCTYPE, finENDDATECALCTYPE,"ENDDATECALCTYPE")
        
        """ 
        finMAXBENEFITPERIOD_UNITS = driver.find_element_by_xpath("//span[contains(@id,'_maxBenefitPeriod')][contains(@class,'DataLabel')]").text
        xlMAXBENEFITPERIOD_UNITS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_UNITS'].iat[0]
        compare_float(xlMAXBENEFITPERIOD_UNITS, finMAXBENEFITPERIOD_UNITS,"MAXBENEFITPERIOD_UNITS")
        
        finMAXBENEFITPERIOD_BASIS = driver.find_element_by_xpath("//span[contains(@id,'_maxBenefitPeriodBasis')][contains(@class,'DataLabel')]").text
        finMAXBENEFITPERIOD_BASIS = finMAXBENEFITPERIOD_BASIS.replace("(","").replace(")","").upper()
        xlMAXBENEFITPERIOD_BASIS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_BASIS'].iat[0]
        xlMAXBENEFITPERIOD_BASIS = xlMAXBENEFITPERIOD_BASIS.upper()
        compare_String(xlMAXBENEFITPERIOD_BASIS, finMAXBENEFITPERIOD_BASIS,"MAXBENEFITPERIOD_BASIS")
        """
       
        xlINDEX_ADJT_NAME = flt_CNV_CLAIM_BENEFIT_df['INDEX_ADJT_NAME'].iat[0]
        finINDEX_ADJT_NAME = 'Unknown'
        compare_String(xlINDEX_ADJT_NAME, finINDEX_ADJT_NAME,"INDEX_ADJT_NAME")
        
        if AccSick=='Sickness':
            finMAXBENEFITPERIOD_UNITS = driver.find_element_by_xpath("//span[contains(@id,'_maxBenefitPeriod')][contains(@class,'DataLabel')]").text
            finMAXBENEFITPERIOD_BASIS = driver.find_element_by_xpath("//span[contains(@id,'_maxBenefitPeriodBasis')][contains(@class,'DataLabel')]").text
            finMAXBENEFITPERIOD_BASIS = finMAXBENEFITPERIOD_BASIS.replace("(","").replace(")","").upper()
            xlMAXBENEFITPERIOD_UNITS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_UNITS'].iat[0]
            xlMAXBENEFITPERIOD_BASIS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_BASIS'].iat[0]
            compare_float(xlMAXBENEFITPERIOD_UNITS, finMAXBENEFITPERIOD_UNITS,"MAXBENEFITPERIOD_UNITS")
            compare_String(xlMAXBENEFITPERIOD_BASIS, finMAXBENEFITPERIOD_BASIS,"MAXBENEFITPERIOD_BASIS")
        elif AccSick == 'Accident':
            finMAXBENEFITPERIOD_UNITS = driver.find_element_by_xpath("//span[contains(@id,'_accidentMaxBenefitPeriod')][contains(@class,'DataLabel')]").text
            finMAXBENEFITPERIOD_BASIS = driver.find_element_by_xpath("//span[contains(@id,'_accidentMaxBenefitPeriodBasis')][contains(@class,'DataLabel')]").text
            finMAXBENEFITPERIOD_BASIS = finMAXBENEFITPERIOD_BASIS.replace("(","").replace(")","").upper()
            xlMAXBENEFITPERIOD_UNITS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_UNITS'].iat[0]
            xlMAXBENEFITPERIOD_BASIS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_BASIS'].iat[0]
            compare_float(xlMAXBENEFITPERIOD_UNITS, finMAXBENEFITPERIOD_UNITS,"MAXBENEFITPERIOD_UNITS")
            compare_String(xlMAXBENEFITPERIOD_BASIS, finMAXBENEFITPERIOD_BASIS,"MAXBENEFITPERIOD_BASIS")
        else:
            finMAXBENEFITPERIOD_UNITS = driver.find_element_by_xpath("//span[contains(@id,'_hospitalMaxBenefitPeriod')][contains(@class,'DataLabel')]").text
            finMAXBENEFITPERIOD_BASIS = driver.find_element_by_xpath("//span[contains(@id,'_hospitalMaxBenefitPeriodBasis')][contains(@class,'DataLabel')]").text
            finMAXBENEFITPERIOD_BASIS = finMAXBENEFITPERIOD_BASIS.replace("(","").replace(")","").upper()
            xlMAXBENEFITPERIOD_UNITS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_UNITS'].iat[0]
            xlMAXBENEFITPERIOD_BASIS = flt_CNV_CLAIM_BENEFIT_df['MAXBENEFITPERIOD_BASIS'].iat[0]
            compare_float(xlMAXBENEFITPERIOD_UNITS, finMAXBENEFITPERIOD_UNITS,"MAXBENEFITPERIOD_UNITS")
            compare_String(xlMAXBENEFITPERIOD_BASIS, finMAXBENEFITPERIOD_BASIS,"MAXBENEFITPERIOD_BASIS")   
            
            
            
        Click(driver,"//div[contains(text(),'Calculation Parameters')][@class='TabOff']")
        time.sleep(3)
        finTAXAPPLIES = driver.find_element_by_xpath("//span[contains(@id,'_employeeContributionPercentage')][contains(@class,'DataLabel')]").text
        if str(finTAXAPPLIES) == '100.00':
            finTAXAPPLIES = 0
        else:
            finTAXAPPLIES = 1    
        xlTAXAPPLIES = flt_CNV_CLAIM_BENEFIT_df['TAXAPPLIES'].iat[0]
        compare_String(xlTAXAPPLIES, finTAXAPPLIES,"TAXAPPLIES")
    
        finFREQUENCYAMOUNT= driver.find_element_by_xpath("//span[contains(@id,'_frequencyAmount')][contains(@class,'DataLabel')]").text
        xlFREQUENCYAMOUNT = flt_CNV_CLAIM_BENEFIT_df['FREQUENCYAMOUNT'].iat[0]
        compare_float(xlFREQUENCYAMOUNT,finFREQUENCYAMOUNT,"FREQUENCYAMOUNT")
        
        #xlPROCESSED = flt_CNV_CLAIM_BENEFIT_df['PROCESSED'].iat[0]
        #finPROCESSED = 'S'
        #compare_String(xlPROCESSED, finPROCESSED,"PROCESSED")
        
        finPAYMENTFREQUENCY = driver.find_element_by_xpath("//span[contains(@id,'_Payment_Frequency')][contains(@class,'DataLabel')]").text
        sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finPAYMENTFREQUENCY +"' and fineos_dom_name = 'Premium Payment Frequency'"
        finPAYMENTFREQUENCY = getEnum(sql1)  
        xlPAYMENTFREQUENCY = flt_CNV_CLAIM_BENEFIT_df['PAYMENTFREQUENCY'].iat[0]
        compare_String(xlPAYMENTFREQUENCY, finPAYMENTFREQUENCY,"PAYMENTFREQUENCY")
        
        finMAXAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_MaximumAmount')][contains(@class,'DataLabel')]").text
        xlMAXAMOUNT = flt_CNV_CLAIM_BENEFIT_df['MAXAMOUNT'].iat[0]
        compare_float(xlMAXAMOUNT, finMAXAMOUNT,"MAXAMOUNT")
        #Click(driver,"//input[contains(@id,'_cmdPageBack_cloned')]")
        
        #Click(driver,"//div[contains(text(),'Recurring Payments')][@class='TabOff']")
        #time.sleep(3)
        #Click(driver,"//div[contains(text(),'Benefit Amount and Adjustments')][@class='TabOff']")
        #time.sleep(3)
        #if len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr")) > 0:
        #   driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr[1]/td[1]").click()
        #    Click(driver,"//input[contains(@id,'_cmdView')]")
        #    time.sleep(3)
        #   finIntegrationType = driver.find_element_by_xpath("//span[contains(@id,'_benefitCalculationType')][contains(@class,'DataLabel')]").text
        #    sql1 = "select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finIntegrationType +"' and fineos_dom_name = 'IncomeIntegrationType'"
        #   finIntegrationType = getEnum(sql1) 
         #   Click(driver,"//input[contains(@id,'_cmdPageBack_cloned')]")
        #else:
        #    finIntegrationType = '73660001'    
        finIntegrationType = driver.find_element_by_xpath("//span[contains(@id,'_benefitCalculationType')][contains(@class,'DataLabel')]").text 
        sql1 = "select FINEOS_ENUM_ID from dev.fineos_enum_xref where fineos_enum_instnc_name like '" + finIntegrationType + "' and fineos_dom_id = 2301"
        finIntegrationType = getEnum(sql1)
        xlIntegrationType = flt_CNV_CLAIM_BENEFIT_df['INTEGRATIONTYPE'].iat[0]
        compare_String(xlIntegrationType, finIntegrationType,"INTEGRATIONTYPE")  #COmmented until confirmation from Trina
        
               
        
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)
    
def Migration_Extract_CNV_PAYMENT(driver):
    global caseStatus
    global Pass_df 
    global caseno
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
        #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_All.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_PAYMENT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_PAYMENT.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_Benefit_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_BENEFIT_ALL.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    # claim get claim id , benefit & benefit ID & use the same in Payment
    
    
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    PAYMENTIDS = CNV_PAYMENT_df['PAYMENTID'].unique()
    for PAYMENTID in PAYMENTIDS:
        caseStatus == 1 
        flt_CNV_PAYMENT_df = CNV_PAYMENT_df[CNV_PAYMENT_df['PAYMENTID'] == PAYMENTID]
        BenID = flt_CNV_PAYMENT_df['BENEFITID'].iat[0]
        
        flt_CNV_Benefit_df = CNV_Benefit_df[CNV_Benefit_df['BENEFITID'].astype(str) ==BenID]
        caseID = flt_CNV_Benefit_df['CASEID'].iat[0]
        
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'].astype(str) ==str(caseID)]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        flt_CNV_Benefit_df.reset_index(drop=True)
        flt_CNV_PAYMENT_df.reset_index(drop=True)
        
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        html_logger.info("Start of CNV_Payment for the Case number " + str(caseno) + " with PAYMENTID " + str(PAYMENTID))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        xlClaimType = flt_CNV_CLAIM_df['CLAIMTYPE'].iat[0]
        xlClaimText = getEnum("select FINEOS_ENUM_INSTNC_NAME from dbo.fineos_enum_xref where FINEOS_ENUM_ID = " + str(xlClaimType))
                
        if len(driver.find_elements_by_xpath("//a[contains(text(),'" + xlClaimText +" Benefit')]")) > 0:
            Click(driver,"//a[contains(text(),'" + xlClaimText +" Benefit')]")
            time.sleep(3)

        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        if flt_CNV_PAYMENT_df['EXPENSECODE'].iat[0] == 'EXP':
            Click(driver,"//div[contains(text(),'Expenses')][@class='TabOff']")
            time.sleep(3)
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr"))
            expitmfnd = 0
            for zz in range(1,rowcnt+1):
                xlPAYEENAME = flt_CNV_PAYMENT_df['PAYEENAME'].iat[0]
                xlEXPENSEAMOUNT = flt_CNV_PAYMENT_df['EXPENSEAMOUNT'].iat[0]
                xlPPOSTEDDATE = str(flt_CNV_PAYMENT_df['PPOSTEDDATE'].iat[0])
                finPAYEENAME = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(zz) + "]/td[5]").text
                finEXPENSEAMOUNT = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(zz) + "]/td[6]").text
                finPPOSTEDDATE = driver.find_element_by_xpath("//table[contains(@id,'_InvoiceListviewWidget')]/tbody/tr[" + str(zz) + "]/td[3]").text
                
                xlPPOSTEDDATE1 = datetime.strptime(xlPPOSTEDDATE, '%Y-%m-%d %H:%M:%S.%f')
                finPPOSTEDDATE1 =datetime.strptime(finPPOSTEDDATE, '%m/%d/%Y')
                
                if finPAYEENAME.upper() == xlPAYEENAME.upper() and float(finEXPENSEAMOUNT.replace(",","")) == float(xlEXPENSEAMOUNT.replace(",","")) and xlPPOSTEDDATE1 == finPPOSTEDDATE1:
                    html_logger.dbg("PAYEENAME Validation is pass. The value is " + str(finPAYEENAME))
                    html_logger.dbg("EXPENSEAMOUNT Validation is pass. The value is " + str(finEXPENSEAMOUNT))
                    html_logger.dbg("PPOSTEDDATE  Validation is pass. The value is " + str(finPPOSTEDDATE))
                    
                    expitmfnd =1
                    break
                
            if expitmfnd ==0:
                html_logger.err("PAYEENAME Validation is fail. The value is " + str(xlPAYEENAME))
                html_logger.err("EXPENSEAMOUNT Validation is fail. The value is " + float(xlEXPENSEAMOUNT))
                html_logger.err("PPOSTEDDATE  Validation is fail. The value is " + str(xlPPOSTEDDATE))
        
        else:         
            Click(driver,"//div[contains(text(),'Payment History')][@class='TabOff']")
            time.sleep(3)
            
            effDate = flt_CNV_PAYMENT_df['REVIEWDATE'].iat[0]
            payStrtDt = flt_CNV_PAYMENT_df['PAYMENTSTARTDATE'].iat[0]
            payEndDt = flt_CNV_PAYMENT_df['PAYMENTENDDATE'].iat[0]
            payAmt = flt_CNV_PAYMENT_df['NETPAYMENTAMOUNT'].iat[0]
            
            tmpEffDate = effDate
            tmpEffDate = datetime.strptime(tmpEffDate,"%Y-%m-%d %H:%M:%S.%f").strftime('%m/%d/%y')
            effDate = minusDays(effDate,"%Y-%m-%d %H:%M:%S.%f",1)
            payEndDt = minusDays(payEndDt,"%Y-%m-%d %H:%M:%S.%f",1)
            payStrtDt = minusDays(payStrtDt,"%Y-%m-%d %H:%M:%S.%f",0)
            
            Click(driver,"//a[contains(text(),'Effective Date')]")
            time.sleep(10)
            
            """Click(driver,"//a[contains(@title,'Filter by:Period Start Date')]")
            time.sleep(2)
            driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodStartDa_startDateFilter')]").clear()
            Set(driver,"//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodStartDa_startDateFilter')]",payStrtDt)
            Click(driver,"//input[contains(@id,'_PaymentHistoryDetailsListview_PeriodStartDa_startDateFilter')]/following::input[contains(@id,'_PaymentHistoryDetailsListview_cmdFilter')][1]")
            time.sleep(5)"""
            
            Click(driver,"//a[contains(@title,'Filter by:Effective Date')]")
            time.sleep(2)
            driver.find_element_by_xpath("//input[contains(@id,'_PaymentHistoryDetailsListview_EventEffectiv_startDateFilter')]").clear()
            Set(driver,"//input[contains(@id,'_PaymentHistoryDetailsListview_EventEffectiv_startDateFilter')]",tmpEffDate)
            Click(driver,"//input[contains(@id,'_PaymentHistoryDetailsListview_EventEffectiv_startDateFilter')]/following::input[contains(@id,'_PaymentHistoryDetailsListview_cmdFilter')][1]")
            time.sleep(5)
           
            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr")) 
            if rowcnt > 0:
                for xx in range(1,rowcnt+1):
                    driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[1]").click()
                    time.sleep(2)
                    if abs(float(driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[7]").text.replace(",",""))) == abs(float(payAmt.replace(",",""))) and datetime.strptime(payStrtDt,'%m/%d/%Y') == datetime.strptime(driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[2]").text,'%m/%d/%Y'):
                        
                        xlREVIEWDATE = flt_CNV_PAYMENT_df['REVIEWDATE'].iat[0]
                        finREVIEWDATE = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[1]").text
                        #compare_Date_format(xlREVIEWDATE, "%Y-%m-%d %H:%M:%S.%f", finREVIEWDATE, "REVIEWDATE")   #commented as it's no longer reqd  Srini 6/2/2021
                        
                        xlPAYMENTREVIEWDATE = flt_CNV_PAYMENT_df['PAYMENTREVIEWDATE'].iat[0]
                        #compare_Date_format(xlPAYMENTREVIEWDATE, "%Y-%m-%d %H:%M:%S.%f", finREVIEWDATE, "PAYMENTREVIEWDATE") #commented as it's no longer reqd  Srini 6/2/2021
                        
                        finPAYMENTSTARTDATE = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[2]").text
                        compare_Date_format(payStrtDt, "%m/%d/%Y", finPAYMENTSTARTDATE, "PAYMENTSTARTDATE")
                        
                        finPAYMENTENDDATE = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[3]").text
                        compare_Date_format(payEndDt, "%m/%d/%Y", finPAYMENTENDDATE, "PAYMENTENDDATE")
                       
                        xlPAYEETYPE= flt_CNV_PAYMENT_df['PAYEETYPE'].iat[0]
                        finPAYEETYPE = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[4]").text
                        if 'Alt' in finPAYEETYPE:
                           finPAYEETYPE = 'AltPayee'
                        else:
                           finPAYEETYPE = 'Payee'   
                            
                        compare_String(xlPAYEETYPE, finPAYEETYPE, "PAYEETYPE")
                         
                        #finPAYMENTDESCRIPTOR = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[5]").text
                        #hardcoding to Online Main Payment
                        finPAYMENTDESCRIPTOR = 'Online Main Payment'
                        finPAYMENTDESCRIPTOR = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finPAYMENTDESCRIPTOR +"'  and fineos_dom_id = 2091")
                        xlPAYMENTDESCRIPTOR = flt_CNV_PAYMENT_df['PAYMENTDESCRIPTOR'].iat[0]
                        compare_String(xlPAYMENTDESCRIPTOR,finPAYMENTDESCRIPTOR, "PAYMENTDESCRIPTOR")
                        
                        finPAYEENAME = driver.find_element_by_xpath("//table[contains(@id,'_PaymentHistoryDetailsListview')]/tbody/tr[" + str(xx) + "]/td[8]").text
                        xlPAYEENAME = flt_CNV_PAYMENT_df['PAYEENAME'].iat[0]
                        compare_String(xlPAYEENAME, finPAYEENAME, "PAYEENAME")
                        
                        Click(driver,"//input[contains(@name,'_PaymentHistoryDetailsListview_cmdView')]")
                        #time.sleep(3)
                        finPAYMENTAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_netBenefitAmountBean')][contains(@class,'DataLabel')]").text
                        finPAYMENTAMOUNT = finPAYMENTAMOUNT.replace(",","")
                        xlPAYMENTAMOUNT =  flt_CNV_PAYMENT_df['PAYMENTAMOUNT'].iat[0]
                        #compare_float(xlPAYMENTAMOUNT, finPAYMENTAMOUNT,"PAYMENTAMOUNT")
                        
                        finNETPAYMENTAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_netAmountForPayment')][contains(@class,'DataLabel')]").text
                        finNETPAYMENTAMOUNT = finNETPAYMENTAMOUNT.replace(",","")
                        xlNETPAYMENTAMOUNT =  flt_CNV_PAYMENT_df['NETPAYMENTAMOUNT'].iat[0]
                        compare_float(xlNETPAYMENTAMOUNT, finNETPAYMENTAMOUNT,"NETPAYMENTAMOUNT")
                        
                        finGROSSBENEFITAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_basicAmount')][contains(@class,'DataLabel')]").text
                        finGROSSBENEFITAMOUNT= finGROSSBENEFITAMOUNT.replace(",","")
                        xlGROSSBENEFITAMOUNT =  flt_CNV_PAYMENT_df['GROSSBENEFITAMOUNT'].iat[0]
                        #compare_float(xlGROSSBENEFITAMOUNT, finGROSSBENEFITAMOUNT,"GROSSBENEFITAMOUNT")
                        
                        xlPOSTEDDATE = flt_CNV_PAYMENT_df['POSTEDDATE'].iat[0]
                        finPOSTEDDATE = driver.find_element_by_xpath("//span[contains(@id,'_DateIssue')][contains(@class,'DataLabel')]").text
                        compare_Date_format(xlPOSTEDDATE, "%Y-%m-%d %H:%M:%S.%f", finPOSTEDDATE, "POSTEDDATE")
                        
                        xlPAIDDATE = flt_CNV_PAYMENT_df['PAIDDATE'].iat[0]
                        finPAIDDATE = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')][contains(@class,'DataLabel')]").text
                        compare_Date_format(xlPAIDDATE, "%Y-%m-%d %H:%M:%S.%f", finPAIDDATE, "PAIDDATE")
                        
                        xlPAYMENTHISTORYSTATUS = flt_CNV_PAYMENT_df['STATUS'].iat[0]
                        finPAYMENTHISTORYSTATUS = driver.find_element_by_xpath("//span[contains(@id,'_Status')][contains(@class,'DataLabel')]").text
                        finPAYMENTHISTORYSTATUS = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finPAYMENTHISTORYSTATUS + "' and FINEOS_DOM_ID  = 2008")
                        compare_float(xlPAYMENTHISTORYSTATUS, finPAYMENTHISTORYSTATUS, "STATUS")
                        
                        finTRANSACTIONSTATUS =driver.find_element_by_xpath("//span[contains(@id,'Status')]").text 
                        #driver.find_element_by_xpath("//label[text()='Transaction Status']/following::span[contains(@id,'_TransactionStatus')][contains(@class,'DataLabel')]").text
                        finTRANSACTIONSTATUS = getEnum("select FINEOS_ENUM_ID from dbo.fineos_enum_xref where FINEOS_ENUM_INSTNC_NAME = '" + finTRANSACTIONSTATUS + "' and FINEOS_DOM_ID  = 2008")
                        xlTRANSACTIONSTATUS = flt_CNV_PAYMENT_df['TRANSACTIONSTATUS'].iat[0]
                        compare_float(xlTRANSACTIONSTATUS, finTRANSACTIONSTATUS, "TRANSACTIONSTATUS")
                        
                        finAddress = driver.find_element_by_xpath("//span[contains(@id,'_payeeAddress')]").text
                        arrAddress = finAddress.split(",")
                        finMASKED_UNITNO = arrAddress[-6]
                        finMASKED_STREET = arrAddress[-5]
                        finSUBURB = arrAddress[-4]
                        finPOSTCODE = arrAddress[-3]
                        finSTATE = arrAddress[-2]
                        finCOUNTRY = arrAddress[-1]
                        
                        xlMASKED_UNITNO = flt_CNV_PAYMENT_df['MASKED_UNITNO'].iat[0]
                        compare_String(xlMASKED_UNITNO, finMASKED_UNITNO, "MASKED_UNITNO")
                        xlMASKED_STREET = flt_CNV_PAYMENT_df['MASKED_STREET'].iat[0]
                        compare_String(xlMASKED_STREET, finMASKED_STREET, "MASKED_STREET")
                        xlSUBURB = flt_CNV_PAYMENT_df['SUBURB'].iat[0]
                        compare_String(xlSUBURB, finSUBURB, "SUBURB")
                        xlPOSTCODE = flt_CNV_PAYMENT_df['POSTCODE'].iat[0]
                        compare_String(xlPOSTCODE, finPOSTCODE, "POSTCODE")
                        xlSTATE = flt_CNV_PAYMENT_df['STATE'].iat[0]
                        compare_String(xlSTATE, finSTATE, "STATE")
                        xlCOUNTRY = flt_CNV_PAYMENT_df['COUNTRY'].iat[0]
                        compare_String(xlCOUNTRY, finCOUNTRY, "COUNTRY")
                        
                        xlPAYMENTIDENTIFIER = flt_CNV_PAYMENT_df['PAYMENTIDENTIFIER'].iat[0]
                        if driver.find_element_by_xpath("//span[contains(@id,'_paymentMethodDropDown')][contains(@class,'DataLabel')]").text == 'Check':
                            finPAYMENTIDENTIFIER = driver.find_element_by_xpath("//span[contains(@id,'_TransactionNumber')][contains(@class,'DataLabel')]").text
                            compare_float(xlPAYMENTIDENTIFIER, finPAYMENTIDENTIFIER, "PAYMENTIDENTIFIER")  
                            finPAYMENTMETHOD = 'CHK' 
                        else:    
                            finPAYMENTIDENTIFIER = ''
                            compare_String(xlPAYMENTIDENTIFIER, finPAYMENTIDENTIFIER, "PAYMENTIDENTIFIER")  
                            finPAYMENTMETHOD = 'ACH' 
                         
                        xlPAYMENTMETHOD = flt_CNV_PAYMENT_df['PAYMENTMETHOD'].iat[0]
                        compare_String(xlPAYMENTMETHOD, finPAYMENTMETHOD, "PAYMENTMETHOD") 
                        
                        finADDEDUSER = driver.find_element_by_xpath("//span[contains(@id,'_AddedBy')][contains(@class,'DataLabel')]").text
                        xlADDEDUSER = flt_CNV_PAYMENT_df['ADDEDUSER'].iat[0]
                        compare_String(xlADDEDUSER, finADDEDUSER, "ADDEDUSER")    #commented as not reqd now..... will need later  
                       
                        finAUTHUSER = driver.find_element_by_xpath("//span[contains(@id,'_IssuedBy')][contains(@class,'DataLabel')]").text
                        xlAUTHUSER = flt_CNV_PAYMENT_df['AUTHUSER'].iat[0]
                        compare_String(xlAUTHUSER, finAUTHUSER, "AUTHUSER") 
                         
                        
                        xlPAYMENTDATE = flt_CNV_PAYMENT_df['PAYMENTDATE'].iat[0]
                        #finPAYMENTDATE = driver.find_element_by_xpath("//span[contains(@id,'_ExtractionDate')][contains(@class,'DataLabel')]").text
                        #changing above line to get transaction ststua date as per srinis discussion
                        finPAYMENTDATE = driver.find_element_by_xpath("//span[contains(@id,'_TransactionStatusDate')][contains(@class,'DataLabel')]").text
                        compare_Date_format(xlPAYMENTDATE, "%Y-%m-%d %H:%M:%S.%f", finPAYMENTDATE, "PAYMENTDATE")
                            
                        Click(driver,"//div[contains(text(),'Amounts')][@class='TabOff']")
                        time.sleep(3)
                        rowcntx = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr"))
                        totamt = 0
                        for yy in range(1,rowcntx+1):
                            amt  = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr/td[2]").text
                            totamt = totamt + float(amt.replace(",",""))
                        finCURRBENEFITPAYMENTAMOUNT = totamt
                        xlCURRBENEFITPAYMENTAMOUNT = flt_CNV_PAYMENT_df['CURRBENEFITPAYMENTAMOUNT'].iat[0]  
                        #compare_float(xlCURRBENEFITPAYMENTAMOUNT, finCURRBENEFITPAYMENTAMOUNT,"CURRBENEFITPAYMENTAMOUNT")
                        
                        xlPaymentAmount = flt_CNV_PAYMENT_df['PAYMENTAMOUNT'].iat[0]
                        time.sleep(5)
                        
                        itm_fnd = 0
                        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr"))
                        for zz in range(1,rowcnt+1):
                            amt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntOffsetsListView_')]/tbody/tr[" + str(zz) + "]/td[2]").text
                            if float(amt.replace(",","")) ==float(xlPaymentAmount.replace(",","")):
                                compare_float(xlPaymentAmount, amt,"PAYMENTAMOUNT")
                                itm_fnd = 1
                         
                        if itm_fnd == 0:       
                            rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                            for zz in range(1,rowcnt+1):
                                amt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(zz) + "]/td[2]").text
                                if float(amt.replace(",","")) ==float(xlPaymentAmount.replace(",","")):
                                    compare_float(xlPaymentAmount, amt,"PAYMENTAMOUNT")
                                    itm_fnd = 1 
                        
                        if itm_fnd == 0:   
                             html_logger.err("Payment Amount  of " + str(xlPaymentAmount) + " is not displayed, hence failed")
                        #FITAMOUNT goes here
                        FITAmt = 0
                        SITAmt = 0
                        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr"))
                        for zz in range(1,rowcnt+1):
                            benName = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(zz) + "]/td[1]").text
                            if benName == 'FIT Amount':
                                FITAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(zz) + "]/td[2]").text
                            elif benName == 'SIT Amount':
                                SITAmt = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'ManPmntBalPayeeListView_')]/tbody/tr[" + str(zz) + "]/td[2]").text     
                    #SITAMOUNT goes here
                        xlFITAmt = flt_CNV_PAYMENT_df['FITAMOUNT'].iat[0]
                        xlSITAmt = flt_CNV_PAYMENT_df['SITAMOUNT'].iat[0]
                        
                        compare_float(xlFITAmt, FITAmt,"FITAMOUNT")
                        compare_float(xlSITAmt, SITAmt,"SITAMOUNT")   
                        
                        Click(driver,"//input[contains(@id,'_cmdPageBack_cloned')]")
                        
                        Click(driver,"//div[contains(text(),'Recurring Payments')][@class='TabOff']")
                        Click(driver,"//div[contains(text(),'Benefit Amount and Adjustments')][@class='TabOff']")
                        time.sleep(5)
                        
                        finGROSSBENEFITAMOUNT = 0
                        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr"))
                        for ii in range(1,rowcnt+1):
                            if driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr[" + str(ii) + "]/td[1]").text == 'Auto Gross Entitlement':
                                finGROSSBENEFITAMOUNT = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr[" + str(ii) + "]/td[2]").text
                                finGROSSBENEFITAMOUNT= finGROSSBENEFITAMOUNT.replace(",","")
                                
                        xlGROSSBENEFITAMOUNT =  flt_CNV_PAYMENT_df['GROSSBENEFITAMOUNT'].iat[0]
                        compare_float(xlGROSSBENEFITAMOUNT, finGROSSBENEFITAMOUNT,"GROSSBENEFITAMOUNT")
                        
                        Click(driver,"//div[contains(text(),'Benefit')][@class='TabOff']")
                        Click(driver,"//div[contains(text(),'Calculation Parameters')][@class='TabOff']")
                        time.sleep(3)
                        
                        finMINBENEFITAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_MinimumAmount')][contains(@class,'DataLabel')]").text
                        finMINBENEFITAMOUNT= finMINBENEFITAMOUNT.replace(",","")
                        xlMINBENEFITAMOUNT =  flt_CNV_PAYMENT_df['MINBENEFITAMOUNT'].iat[0]
                        compare_float(xlMINBENEFITAMOUNT, finMINBENEFITAMOUNT,"MINBENEFITAMOUNT")
                        
                        finMINBENEFITPERCENT = driver.find_element_by_xpath("//span[contains(@id,'_MinimumPercentage')][contains(@class,'DataLabel')]").text
                        finMINBENEFITPERCENT= finMINBENEFITPERCENT.replace(",","")
                        xlMINBENEFITPERCENT =  flt_CNV_PAYMENT_df['MINBENEFITPERCENT'].iat[0]
                        compare_float(xlMINBENEFITPERCENT, finMINBENEFITPERCENT,"MINBENEFITPERCENT")
                        
                        
                        break

        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)
   

def Migration_Extract_CNV_OverPayment(driver):  
    global caseStatus
    global Pass_df 
    global caseno
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
        #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_All.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_OverPAYMENT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_OVERPAYMENT.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_Benefit_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_BENEFIT_ALL.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    # claim get claim id , benefit & benefit ID & use the same in Payment
    OVERPAYMENTIDS = CNV_OverPAYMENT_df['OVERPAYMENTID'].unique()
    for OVERPAYMENTID in OVERPAYMENTIDS:
        caseStatus == 1 
        flt_CNV_OverPAYMENT_df = CNV_OverPAYMENT_df[CNV_OverPAYMENT_df['OVERPAYMENTID'] == OVERPAYMENTID]
        BenID = flt_CNV_OverPAYMENT_df['BENEFITID'].iat[0]
        
        flt_CNV_Benefit_df = CNV_Benefit_df[CNV_Benefit_df['BENEFITID'].astype(str) ==BenID]
        caseID = flt_CNV_Benefit_df['CASEID'].iat[0]
        
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'].astype(str) ==str(caseID)]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        flt_CNV_Benefit_df.reset_index(drop=True)
        flt_CNV_OverPAYMENT_df.reset_index(drop=True)
        
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        html_logger.info("Start of CNV_OverPayment for the Case number " + str(caseno) + " with OverPaymentID " + str(OVERPAYMENTID))

        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        if len(driver.find_elements_by_xpath("//a[text()='Overpayment']"))> 0:
            Click(driver,"//a[text()='Overpayment']")
            
            FinAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_BalancingOverpaymentAmount')]").text
            FinAS_OF_DATE = driver.find_element_by_xpath("//span[contains(@id,'_DateOfCalculation')]").text
            FinPAYMENTTYPE = driver.find_element_by_xpath("//span[contains(@id,'_OverpaymentType')]").text
            FinRECOVEREDAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_RecoveredToDateAmount')]").text
            FinOUTSTANDINGAMOUNT = driver.find_element_by_xpath("//span[contains(@id,'_OutstandingAmount')]").text
            
            Click(driver,"//div[@class='TabOff'][contains(text(),'Associated Dues')]")
            Click(driver,"//a[@id='Period Start Date_sortHeaderName']") # sorting ascending
            time.sleep(3)
            Click(driver,"//a[@id='Period Start Date_sortHeaderName']") # sorting descending
            time.sleep(3)
            FinINCURREDDATE = driver.find_element_by_xpath("//table[contains(@id,'_AllocatedDuesListView')]/tbody/tr/td[1]").text
            
            if(float(FinAMOUNT.replace(",","")) - float(FinRECOVEREDAMOUNT.replace(",","")) == 0):
                FinOVERPAYMENTSTATUS = 'PAID-IN-FULL'
            elif(float(FinAMOUNT.replace(",","")) - float(FinRECOVEREDAMOUNT.replace(",","")) > 0):
                FinOVERPAYMENTSTATUS = 'ACTIVE'
            elif(float(FinAMOUNT.replace(",","")) - float(FinRECOVEREDAMOUNT.replace(",","")) < 0):
                FinOVERPAYMENTSTATUS = 'OVERPAID'    
            
            XLAMOUNT = flt_CNV_OverPAYMENT_df['AMOUNT'].iat[0]      
            XLAS_OF_DATE = flt_CNV_OverPAYMENT_df['AS_OF_DATE'].iat[0]
            XLPAYMENTTYPE = flt_CNV_OverPAYMENT_df['PAYMENTTYPE'].iat[0]
            XLRECOVEREDAMOUNT = flt_CNV_OverPAYMENT_df['RECOVEREDAMOUNT'].iat[0]
            XLOUTSTANDINGAMOUNT = flt_CNV_OverPAYMENT_df['OUTSTANDINGAMOUNT'].iat[0]
            XLINCURREDDATE = flt_CNV_OverPAYMENT_df['INCURREDDATE'].iat[0]
            XLOVERPAYMENTSTATUS = flt_CNV_OverPAYMENT_df['OVERPAYMENTSTATUS'].iat[0]
            
            compare_float(XLAMOUNT,FinAMOUNT, "AMOUNT")
            compare_Date_format(XLAS_OF_DATE, "%Y-%m-%d %H:%M:%S.%f", FinAS_OF_DATE, "AS_OF_DATE")
            compare_String(XLPAYMENTTYPE,FinPAYMENTTYPE, "PAYMENTTYPE")
            compare_float(XLRECOVEREDAMOUNT,FinRECOVEREDAMOUNT, "RECOVEREDAMOUNT")
            compare_float(XLOUTSTANDINGAMOUNT,FinOUTSTANDINGAMOUNT, "OUTSTANDINGAMOUNT")
            compare_Date_format(XLINCURREDDATE, "%Y-%m-%d %H:%M:%S.%f", FinINCURREDDATE, "XLINCURREDDATE")
            compare_String(XLOVERPAYMENTSTATUS,FinOVERPAYMENTSTATUS, "OVERPAYMENTSTATUS")
        else:
            html_logger.err("OverPayment Link not available hence Failed")
            caseStatus = 0
            
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)

              
def Migration_Extract_CNV_BENEFITAdjustment(driver):
    global caseStatus
    global Pass_df 
    global caseno
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
        #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
    Arena_Fineos_df = pd.read_excel("K:\IT WORK\PROJ00014208 - Disability Claims Migration\Design\TDD\Arena_To_Fineos_Code_Mapping.xlsx",sheet_name = 'BenAdjCode')
    
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_All.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_CLAIM_BENEFIT_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_BENEFIT_All.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    CNV_CLAIM_BENEFITAdjustment_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_BENEFIT_ADJUSTMENT.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    ADJUSTMENTIDs = CNV_CLAIM_BENEFITAdjustment_df['ADJUSTMENTID'].unique()
    for ADJUSTMENTID in ADJUSTMENTIDs:
        caseStatus = 1 
        flt_CNV_CLAIM_BENEFITAdjustment_df = CNV_CLAIM_BENEFITAdjustment_df[CNV_CLAIM_BENEFITAdjustment_df['ADJUSTMENTID'] ==ADJUSTMENTID]
        BenId = flt_CNV_CLAIM_BENEFITAdjustment_df['BENEFITID'].iat[0]
        
        flt_CNV_CLAIM_BENEFIT_df = CNV_CLAIM_BENEFIT_df[CNV_CLAIM_BENEFIT_df['BENEFITID'] ==BenId]
        caseid = flt_CNV_CLAIM_BENEFIT_df['CASEID'].iat[0]
        
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['CASEID'] ==caseid]
        caseno = flt_CNV_CLAIM_df['CASEID'].iat[0]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        flt_CNV_CLAIM_BENEFIT_df.reset_index(drop=True)
        
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].to_string(index=False)
        sicInd = flt_CNV_CLAIM_df['SICKNESS_ACCIDENT'].to_string(index=False)
        
        html_logger.info("Start of CNV_BENEFITADJUSTMENT for the Case number " + str(caseno) + " with Adjustment id " + str(ADJUSTMENTID))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        #xlBenCaseType = flt_CNV_CLAIM_BENEFIT_df['BENEFITCASETYPE'].iat[0]
        
        if len(driver.find_elements_by_xpath("//a[contains(text(),'Benefit')]")) > 0:
            Click(driver,"//a[contains(text(),'Benefit')]")
            time.sleep(3)
            finBenCaseType = driver.find_element_by_xpath("//a[contains(text(),'Benefit')]").text
            BenType = finBenCaseType.split()[0]
        else:
            finBenCaseType = '' 
            

        
        if len(driver.find_elements_by_xpath("//div[contains(text(),'Close')][@class='bottom-close']"))> 0:
            Click(driver, "//div[contains(text(),'Close')][@class='bottom-close']")
        
        
        Click(driver,"//div[@class='TabOff'][contains(text(),'Recurring Payments')]")
        Click(driver,"//div[@class='TabOff'][contains(text(),'Benefit Amount and Adjustments')]")
        time.sleep(3)
        
        itmfnd = 0
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr"))
        if rowcnt > 0:
            for i in range(1,rowcnt+1):
                FinType = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr[" +str(i) + "]/td[1]").text
                FinAmount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BenefitAmountOffsetsAndDeductionsListView')]/tbody/tr[" +str(i) + "]/td[2]").text
                
                xlType = flt_CNV_CLAIM_BENEFITAdjustment_df['TYPE'].iat[0]
                xlAmount = flt_CNV_CLAIM_BENEFITAdjustment_df['AMOUNT'].iat[0]
                
                if float(str(xlAmount).replace(',','')) ==float(str(FinAmount).replace(',','')) and FinType == xlType:
                    itmfnd = 1
                    
        rowcnt = len(driver.find_elements_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr"))
        if rowcnt > 0:
            for i in range(1,rowcnt+1):
                FinType = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" +str(i) + "]/td[1]").text
                FinAmount = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')][contains(@id,'BalancedPayeeOffsetsAndDeductionsListView_')]/tbody/tr[" +str(i) + "]/td[2]").text
                
                xlType = flt_CNV_CLAIM_BENEFITAdjustment_df['TYPE'].iat[0]
                xlAmount = flt_CNV_CLAIM_BENEFITAdjustment_df['AMOUNT'].iat[0]
                
                if float(str(xlAmount).replace(',','')) ==float(str(FinAmount).replace(',','')) and FinType == xlType:
                    itmfnd = 1
                            
        if itmfnd == 1:    
            finStartDate = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" +str(i) + "]/td[4]").text
            xlStartDate = flt_CNV_CLAIM_BENEFITAdjustment_df['STARTDATE'].iat[0]
            compare_Date_format(xlStartDate, "%Y-%m-%d %H:%M:%S.%f", finStartDate, "STARTDATE")
            
            finEndDate = driver.find_element_by_xpath("//table[contains(@id,'_OffsetsAndDeductions')]/tbody/tr[" +str(i) + "]/td[5]").text
            xlEndDate = flt_CNV_CLAIM_BENEFITAdjustment_df['ENDDATE'].iat[0]
            compare_Date_format(xlEndDate, "%Y-%m-%d %H:%M:%S.%f", finEndDate, "ENDDATE")
            
            compare_String(xlType, FinType, 'TYPE')
            try:
                fltArena_Fineos_df = Arena_Fineos_df[Arena_Fineos_df['Fineos Value'] ==FinType]
                FinName = fltArena_Fineos_df['Description'].iat[0]
            except:
                FinName = ''
                
            xlName = flt_CNV_CLAIM_BENEFITAdjustment_df['NAME'].iat[0]
            compare_String(xlName, FinName, 'NAME')
            
            compare_float(xlAmount, FinAmount, 'AMOUNT')
            
            if BenType == 'STD':
                FINADJUSTMENTFREQUENCY = 'Weeks'
            elif BenType == 'LTD':    
                FINADJUSTMENTFREQUENCY = 'Months'
            else:
                FINADJUSTMENTFREQUENCY = ''
            xlADJUSTMENTFREQUENCY = flt_CNV_CLAIM_BENEFITAdjustment_df['ADJUSTMENTFREQUENCY'].iat[0]
            compare_String(xlADJUSTMENTFREQUENCY, FINADJUSTMENTFREQUENCY, 'ADJUSTMENTFREQUENCY')
            
            FinADJTERMINATIONREASON = 'Unknown'
            xlADJTERMINATIONREASON = flt_CNV_CLAIM_BENEFITAdjustment_df['ADJTERMINATIONREASON'].iat[0]
            compare_String(xlADJTERMINATIONREASON, FinADJTERMINATIONREASON, 'ADJTERMINATIONREASON')
            
            FinADJINTEGRATIONTYPE = 'Direct'
            xlADJINTEGRATIONTYPE = flt_CNV_CLAIM_BENEFITAdjustment_df['ADJINTEGRATIONTYPE'].iat[0]
            compare_String(xlADJINTEGRATIONTYPE, FinADJINTEGRATIONTYPE, 'ADJINTEGRATIONTYPE')
                    
        else:
            html_logger.err("Amount of " + str(xlAmount) + " and description of " + xlType + " doesn't match hence Failed")
            caseStatus = 0
            #caseStatus - need to get mapping 
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)
                   
def Migration_Extract_Policy(driver):
    global caseStatus
    global Pass_df 
    global caseno
    
    #driver.get("https:CONTENT:password1@pde-claims-webapp.oneamerica.fineos.com")
    driver.get("https:CONTENT:password1@mig1-claims-webapp.oneamerica.fineos.com")
        #driver.get("https:KKELLY:password1@idt-claims-webapp.oneamerica.fineos.com/")
    driver.maximize_window()
        
    CNV_CLAIM_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_All.csv", delimiter = '|',encoding='cp1252', dtype = str)
    CNV_POLICY_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_POLICY.csv", delimiter = '|',encoding='cp1252',dtype = str)
    
    #CNV_CLAIM_CASENOTE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CASENOTE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_CODE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_CODE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    #CNV_CLAIM_COVERAGE_df = pd.read_csv("C:\Srikanth\Srikanth\Claims_ReportValidation\ARENA_Migration_Extracts_01252021\CNV_CLAIM_COVERAGE.csv", delimiter = '|',encoding='cp1252', converters={'CASEID': lambda x: str(x)})
    
    POLICYIDS = CNV_POLICY_df['POLICYID'].unique()
    for POLICYID in POLICYIDS:
        caseStatus = 1 
        flt_CNV_POLICY_df = CNV_POLICY_df[CNV_POLICY_df['POLICYID'] ==POLICYID]
        PARTYID =  flt_CNV_POLICY_df['PARTYID'].iat[0]
        
        flt_CNV_CLAIM_df = CNV_CLAIM_df[CNV_CLAIM_df['PARTYID'] == str(PARTYID)]
        caseno = flt_CNV_CLAIM_df['CLAIMNUMBER'].iat[0]
        
        flt_CNV_CLAIM_df.reset_index(drop=True)
        flt_CNV_POLICY_df.reset_index(drop=True)
        
        html_logger.info("Start of CNV_Policy for the Case number " + str(caseno) + " with Policy id " + str(POLICYID))
       
    
        #driver.find_element_by_xpath("//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]").click()
        #driver.find_element_by_xpath("//div[contains(text(),'Case')][@class='TabOff']").click()
        Click(driver, "//a[contains(@name,'.SearchCases_MENUITEM.SearchCaseslink')]")
        Click(driver,"//div[contains(text(),'Case')][@class='TabOff']")
        Set(driver, "//input[contains(@id,'_caseNumber')]",caseno)
        Click(driver,"//input[contains(@id,'_searchButton')]")
        
        time.sleep(5)
        
        WebDriverWait(driver,10).until(ec.visibility_of_element_located((By.XPATH,"//h2[contains(@title,'Group Disability Claim')]/span")))
        claimNo = driver.find_element_by_xpath("//h2[contains(@title,'Group Disability Claim')]/span").text
        
        Click(driver,"//div[contains(text(),'Coverages')][@class='TabOff']")
        time.sleep(3)
        Click(driver,"//table[contains(@id,'_ClaimsContractStubListviewControlBean')]/tbody/tr[1]/td[1]")
        Click(driver,"//input[contains(@id,'_ChangeContracts')]")
        time.sleep(3)
        
        finStartDate = driver.find_element_by_xpath("//span[contains(@id,'_StartDate')][@class='DataLabel']").text
        xlStartDate = flt_CNV_POLICY_df['PERIODCOMMENCEMENTDATE'].iat[0]
        compare_Date_format(xlStartDate, "%Y-%m-%d %H:%M:%S.%f", finStartDate, "PERIODCOMMENCEMENTDATE")
        
        finEndDate = driver.find_element_by_xpath("//span[contains(@id,'_EndDate')][@class='DataLabel']").text
        if finEndDate == '-':
            finPOLICYSTATUSCODE = 'Live'
        else:
            finPOLICYSTATUSCODE = 'Terminated' 
        xlPOLICYSTATUSCODE =  flt_CNV_POLICY_df['POLICYSTATUSCODE'].iat[0]
        compare_String(xlPOLICYSTATUSCODE, finPOLICYSTATUSCODE, "POLICYSTATUSCODE")
        
        xlEndDate = flt_CNV_POLICY_df['PERIODEXPIRYDATE'].iat[0]
        compare_Date_format(xlEndDate, "%Y-%m-%d %H:%M:%S.%f", finEndDate, "PERIODEXPIRYDATE")
        
        finADMINSYSTEM = '382001'
        xlADMINSYSTEM = flt_CNV_POLICY_df['ADMINSYSTEM'].iat[0]
        compare_String(xlADMINSYSTEM, finADMINSYSTEM, "ADMINSYSTEM")
        
        finPRODUCTTYPE = '1248012'
        xlPRODUCTTYPE = flt_CNV_POLICY_df['PRODUCTTYPE'].iat[0]
        compare_String(xlPRODUCTTYPE, finPRODUCTTYPE, "PRODUCTTYPE")
        
        finDIVISION = '7552002'
        xlDIVISION = flt_CNV_POLICY_df['DIVISION'].iat[0]
        compare_String(xlDIVISION, finDIVISION, "DIVISION")
        
        finPOLICYHOLDERNUMBER = driver.find_element_by_xpath("//span[contains(@id,'_fullRefNum')][@class='DataLabel']").text
        xlPOLICYHOLDERNUMBER =  flt_CNV_POLICY_df['POLICYHOLDERNUMBER'].iat[0]
        compare_String(xlPOLICYHOLDERNUMBER, finPOLICYHOLDERNUMBER, "POLICYHOLDERNUMBER")
        
        Click(driver,"//input[contains(@id,'_editPageCancel_cloned')]")
        time.sleep(3)
        
        if caseStatus == 1:
             #Pass_df.append([{'CaseNo':caseno, 'Status':'Pass'}], ignore_index=True)
             df2 = pd.DataFrame([[caseno,'Pass']], columns=['CaseNo','Status'])
             Pass_df = pd.concat([df2, Pass_df])
             #caseStatus == 1
        else:
             caseStatus == 1    
         
        Pass_df.to_csv(pass_FileName, mode='w', header=True, index = False)    
        Fail_df.to_csv(fail_FileName, mode='w', header=True, index = False)
""" commentred temporarily and must be removed while actual executon"""
from multiprocessing import Process
import random
import time

if __name__ == '__main__':  
    global driver
    options = webdriver.ChromeOptions()
    capabilities = DesiredCapabilities.CHROME.copy()
    capabilities['platform'] = 'WINDOWS'
    capabilities['version'] = "ANY"
    #driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe',options = options)
    driver = webdriver.Remote(command_executor=f"http://10.32.232.129:4444/wd/hub",desired_capabilities=capabilities,options = options)
    '''
    #driver = webdriver.Remote(command_executor=f"http://10.32.232.129:4444/wd/hub",desired_capabilities=capabilities,options = options)
    jobs=[]
    #jobs.append(Process(target=Migration_Extract_CNV_CLAIM_1(driver)))
    #jobs.append(Process(target=Migration_Extract_CNV_CLAIM_2(driver)))
    jobs.append(Process(target=Migration_Extract_CNV_BENEFIT(driver)))
    #jobs.append(Process(target=Migration_Extract_CNV_CLAIM_BENEFITRT(driver)))

    for job in jobs:
        job.start()
        job.join()
    '''
    #Migration_Extract_CNV_CLAIM_1(driver)   #looks OK - partial returntoworkdate 
    #Migration_Extract_CNV_PAYMENT(driver)
    Migration_Extract_CNV_BENEFIT(driver)   # Looks Ok
    #Migration_Extract_CNV_PAYMENT(driver)
    #Migration_Extract_CNV_BENEFITRT(driver)  #MAXBENEFITPERIOD_UNITS  & MAXBENEFITPERIOD_BASIS  Issue... rest OK
    #Migration_Extract_CNV_OverPayment(driver)  #XLINCURREDDATE issue rest OK
    #Migration_Extract_CNV_BENEFITAdjustment(driver)   #mostly OK..but some rows missing - rerun with new data
    #Migration_Extract_Policy(driver)
    
    #disClaimDetailReport()  #PHReport Disability Claim Detail Report fin vs Excel Report
    #waiverValidation()  # Report comparison for fineos vs Excel report - Swetha
    #disClaimStatusReport()# Report comparison for fineos vs Excel report - Swetha
    
    
    #FICAWithholdingMatch() # Report comparison for fineos vs Excel report - Swetha  # Fixed Sum
    #FedWageTax() # fixed sum
    #StateWageTax() # Fixed Sum
    #MiscReport1099()
    #ASOSimpleReport1099()
    #ReconcileEmpTax_NoW2_8922()  # fixed sum
    #ClaimantEOB()
    #VendorEOB()
    
    #Migration_Extract_CNV_CLAIM()
#Migration_Extract_CNV_BENEFIT()

#DisClaimPayment()   #same script fro DIsclaimStatusReport

#ASODB validation is in C:\Srikanth\Srikanth\Claims_ReportValidation\ASODB_Report.xlsm

#ClaimsFeedReport()


'''
str1 = """123 QA Avenue,
        Vancouver,
        BC,
        V5J3Z7,
        Canada."""
        
str2 = """street 1,
washington, WA, 98052,
USA."""
claimResState = (str2.replace('\n','')).split(",")[-3].strip()
print(claimResState)
print((str2.replace('\n','')).split(",")[0].strip())
#print((str2.replace('\n','')).split(",")[1].strip())
print((str2.replace('\n','')).split(",")[-4].strip())
print((str2.replace('\n','')).split(",")[-3].strip())
print((str2.replace('\n','')).split(",")[-2].strip())
print((str2.replace('\n','')).split(",")[-1].strip())
claimResState = (str1.replace('\n','')).split(",")[-3].strip()
print(claimResState)
'''


#DisabilityClaimsDetailReport_AllClaims("006067360000000","STD") #-- need to work on this  after data changes
#DisabilityClaimsStatusReport_AllClaims("006067360000000","STD")


#EC3802_PremiumWaiver("'000005550003000','006170980000000','006999970000000','006191570000000','006171190000000','000020765001000','000075440002000','006018800000000','000020765001000','116000010002000'","LTD")
#EC3802_PremiumWaiver("'000005550003000','006170980000000','006999970000000','006191570000000','006171190000000','000020765001000','000075440002000','006018800000000','000020765001000','116000010002000'","STD")

#Misc1099Reports()
#FineosDailyAccountingDetailsReport()