#import unittest
#from test import support

import Payment_ReusableArtifacts
from Payment_ReusableArtifacts import Comparator,Parser,DFCompare

import html_logger
from html_logger import setup, info

import datetime
import logging
import sys
import time
import csv

import pandas as pd
import xml.etree.ElementTree as ET
       
now = datetime.datetime.now()
parentFolder = "C:\\Srikanth\\Eclipise_workspace_10142020\\Python_Projects\\"
sys.path.append(parentFolder + "DisabilityClaims\\DB_WS_Fineos\\")

#driver= webdriver.Chrome(executable_path= parentFolder + 'Driver\\chromedriver.exe')  
#driver= webdriver.Firefox(executable_path= parentFolder + 'Driver\\geckodriver.exe')  


#######   Start of  UAT vs Dev Comparison  ############################################ 
#################################################################################################


LOG_FILENAME = parentFolder + "DisabilityClaims\\DB_WS_Fineos\\Results\\DBValidation_"+ str(now.isoformat().replace(":","_")) + ".html"
setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
dbserver1 = "SQLD10747,4242"
dbname1 = "ENTPRS_CLAIMS_DM"

dbserver2 = "SQLT10747,5353"
dbname2 = "ENTPRS_CLAIMS_DM"               


#########################################PARTY_PLAN_FACT#################################
##########################################################################################
html_logger.info("Start of Comparing Party_plan_fact " + dbserver1 + "--" + dbname1 + " vs " + dbserver2 + " -- " + dbname2)
sql1 = '''select * from dev.PARTY_PLAN_FACT p where PM_CUR_IND = 'Y' order by plcyhldr_ben_plan_eff_dt'''
#dbserver1 = "SQLD10747,4242"
#dbname1 = "ENTPRS_CLAIMS_DM"

sql2 = '''select * from dev.PARTY_PLAN_FACT p where PM_CUR_IND = 'Y' order by plcyhldr_ben_plan_eff_dt'''
#dbserver2 = "SQLD10785,4242"
#dbname2 = "ENTPRS_CLAIMS_DM_UAT"


sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)

cols = [['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['ERISA_APPL_CD','ERISA_APPL_CD','String'],
        ['ERISA_APPL_DESCR','ERISA_APPL_DESCR','String'],['PLCYHLDR_PLAN_CLS_CD','PLCYHLDR_PLAN_CLS_CD','String'],['PLCYHLDR_PLAN_CLS_DESCR','PLCYHLDR_PLAN_CLS_DESCR','String'],
        ['NEW_EMP_WAIT_PER_TYP_CD','NEW_EMP_WAIT_PER_TYP_CD','String'],['NEW_EMP_WAIT_PER_TYP_DESCR','NEW_EMP_WAIT_PER_TYP_DESCR','String'],['NEW_EMP_COV_QLFCTN_TYP_CD','NEW_EMP_COV_QLFCTN_TYP_CD','String'],
        ['NEW_EMP_COV_QLFCTN_TYP_DESCR','NEW_EMP_COV_QLFCTN_TYP_DESCR','String'],['INIT_EMP_WAIT_PER_TYP_CD','INIT_EMP_WAIT_PER_TYP_CD','String'],
        ['INIT_EMP_WAIT_PER_TYP_DESCR','INIT_EMP_WAIT_PER_TYP_DESCR','String'],['MNTL_ILL_LIMIT_IND','MNTL_ILL_LIMIT_IND','String'],['SPL_COND_BEN_DUR_TYP_CD','SPL_COND_BEN_DUR_TYP_CD','String'],
        ['SPL_COND_BEN_DUR_TYP_DESCR','SPL_COND_BEN_DUR_TYP_DESCR','String'],['SPL_DISBLTY_COND_LMT_IND','SPL_DISBLTY_COND_LMT_IND','String'],['SPS_DISBLTY_BEN_ELIM_PER_CD','SPS_DISBLTY_BEN_ELIM_PER_CD','String'],
        ['SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','SPS_DISBLTY_BEN_ELIM_PER_TYP_DESCR','String'],['SPS_DISBLTY_MAX_MTHLY_BEN_AMT','SPS_DISBLTY_MAX_MTHLY_BEN_AMT','String'],
        ['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','SPS_DISBLTY_BEN_LIFETIME_MAX_PER_CD','String'],['SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','SPS_DISBLTY_BEN_LIFETIME_MAX_PER_DESCR','String'],
        ['DISBLTY_BEN_FREQ_CD','DISBLTY_BEN_FREQ_CD','String'],['DISBLTY_BEN_FREQ_TYP_CD','DISBLTY_BEN_FREQ_TYP_CD','String'],['DISBLTY_BEN_FREQ_TYP_DESCR','DISBLTY_BEN_FREQ_TYP_DESCR','String'],
        ['COST_OF_LVNG_BEN_ADJ_PER_CD','COST_OF_LVNG_BEN_ADJ_PER_CD','String'],['COST_OF_LVNG_BEN_ADJ_PER_DESCR','COST_OF_LVNG_BEN_ADJ_PER_DESCR','String'],
        ['COST_OF_LVNG_BEN_ADJ_TYP_CD','COST_OF_LVNG_BEN_ADJ_TYP_CD','String'],['DBL_ELIM_PER_IND','DBL_ELIM_PER_IND','String'],['DISBLTY_MAX_BEN_AMT','DISBLTY_MAX_BEN_AMT','String'],
        ['DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','DRUG_ALCOL_ABUSE_BEN_LIMIT_IND','String'],['MAX_COV_BEN_AMT','MAX_COV_BEN_AMT','String'],['DISBLTY_BEN_SLRY_PCT','DISBLTY_BEN_SLRY_PCT','String'],
        ['DISBLTY_MIN_NET_BEN_PCT','DISBLTY_MIN_NET_BEN_PCT','String'],['FRST_DAY_HOSP_BEN_IND','FRST_DAY_HOSP_BEN_IND','String'],['FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','FRST_DAY_HOSP_OTPNT_SRGRY_BEN_IND','String'],
        ['EMPLR_CONTRIB_PCT','EMPLR_CONTRIB_PCT','String'],['INIT_EMP_COV_QLFCTN_TYP_CD','INIT_EMP_COV_QLFCTN_TYP_CD','String'],['INIT_EMP_COV_QLFCTN_TYP_DESCR','INIT_EMP_COV_QLFCTN_TYP_DESCR','String'],
        ['MIN_COV_BEN_AMT','MIN_COV_BEN_AMT','String'],['MIN_NET_BEN_AMT','MIN_NET_BEN_AMT','String'],['FIX_COV_BEN_AMT','FIX_COV_BEN_AMT','String'],['INSUR_INCR_UNIT_AMT','INSUR_INCR_UNIT_AMT','String'],
        ['DISBLTY_ELIM_PER_QLFCTN_IND','DISBLTY_ELIM_PER_QLFCTN_IND','String'],['COLA_MAX_OCCUR_NBR','COLA_MAX_OCCUR_NBR','String'],['COLA_OCCUR_TYP','COLA_OCCUR_TYP','String'],
        ['COLA_OCCUR_TYP_DESCR','COLA_OCCUR_TYP_DESCR','String'],['COLA_CD','COLA_CD','String'],['COLA_DESCR','COLA_DESCR','String'],['CVRD_EARN_TYP_CD','CVRD_EARN_TYP_CD','String'],
        ['CVRD_EARN_TYP_DESCR','CVRD_EARN_TYP_DESCR','String'],['PREM_WVR_CD','PREM_WVR_CD','String'],['PREM_WVR_DESCR','PREM_WVR_DESCR','String'],['CMPNY_TAX_AGNT_CD','CMPNY_TAX_AGNT_CD','String'],
        ['CMPNY_TAX_AGNT_DESCR','CMPNY_TAX_AGNT_DESCR','String'],['DISBLTY_FLAT_BEN_AMT','DISBLTY_FLAT_BEN_AMT','String'],['PM_CUR_IND','PM_CUR_IND','String']
]

DFCompare.compare(sqlqadf,sqluatdf,cols)
html_logger.info("End of Comparing Party_plan_fact")
#########################################PARTY_PLAN_FACT#################################
##########################################################################################


"""

#########################################ben_plan_dim#################################
##########################################################################################
html_logger.info("Start of Comparing ben_plan_dim " + dbserver1 + "--" + dbname1 + " vs " + dbserver2 + " -- " + dbname2)
sql1 = "select * from qa.ben_plan_dim where PM_CUR_IND = 'Y' order by ben_plan_dim_gen_id"
sql2 = "select * from dbo.ben_plan_dim where PM_CUR_IND = 'Y' order by ben_plan_dim_gen_id"

sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)
sqluatdf = sqluatdf.iloc[1:]

cols = [['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PLAN_KEY_TXT','ADMIN_SYS_PLAN_KEY_TXT','String'],['BEN_PLAN_NBR','BEN_PLAN_NBR','String'],['BEN_PLAN_TYP_CD','BEN_PLAN_TYP_CD','String'],
        ['BEN_PLAN_TYP_DESCR','BEN_PLAN_TYP_DESCR','String'],['PLAN_EFF_DT','PLAN_EFF_DT','String'],['BEN_MAX_PER_NBR','BEN_MAX_PER_NBR','String'],['BEN_MAX_PER_TYP_CD','BEN_MAX_PER_TYP_CD','String'],
        ['BEN_MAX_PER_TYP_DESCR','BEN_MAX_PER_TYP_DESCR','String'],['OTH_INCM_INTGRTN_MTHD_CD','OTH_INCM_INTGRTN_MTHD_CD','String'],['OTH_INCM_INTGRTN_MTHD_DESCR','OTH_INCM_INTGRTN_MTHD_DESCR','String'],
        ['DISBLTY_BEN_CONT_PER_CD','DISBLTY_BEN_CONT_PER_CD','String'],['DISBLTY_BEN_CONT_PER_DESCR','DISBLTY_BEN_CONT_PER_DESCR','String'],['IMMED_HOSP_BEN_IND','IMMED_HOSP_BEN_IND','String'],
        ['OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','OWN_OCPTN_DISBLTY_BEN_ELIG_TYP_CD','String'],['OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','OWN_OCPTN_DISBLTY_BEN_ELIG_DESCR','String'],
        ['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_CD','String'],['SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','SHRT_TERM_DISBLTY_BEN_INTGRTN_TYP_DESCR','String'],
        ['RTRN_TO_WRK_PER_NBR','RTRN_TO_WRK_PER_NBR','String'],['RTRN_TO_WRK_PER_TYP_CD','RTRN_TO_WRK_PER_TYP_CD','String'],['RTRN_TO_WRK_PER_DESCR','RTRN_TO_WRK_PER_DESCR','String'],
        ['POST_EFF_DT_TRTMT_FREE_PER_NBR','POST_EFF_DT_TRTMT_FREE_PER_NBR','String'],['POST_EFF_DT_TRTMT_FREE_TYP_CD','POST_EFF_DT_TRTMT_FREE_TYP_CD','String'],
        ['POST_EFF_DT_TRTMT_FREE_DESCR','POST_EFF_DT_TRTMT_FREE_DESCR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','String'],
        ['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','String'],
        ['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PRE_EXIT_COND_NOT_CVRD_PER_NBR','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','String'],
        ['PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','PRE_EXIT_COND_NOT_CVRD_PER_TYP_DESCR','String'],['CVRD_EARN_TYP_CD','CVRD_EARN_TYP_CD','String'],['CVRD_EARN_TYP_DESCR','CVRD_EARN_TYP_DESCR','String'],
        ['FULL_TM_STAT_QLFCTN_THRSHLD_NBR','FULL_TM_STAT_QLFCTN_THRSHLD_NBR','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','String'],
        ['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','String'],['INDV_COV_EFF_TYP_CD','INDV_COV_EFF_TYP_CD','String'],['INDV_COV_EFF_TYP_DESCR','INDV_COV_EFF_TYP_DESCR','String'],
        ['SURV_BEN_PER_NBR','SURV_BEN_PER_NBR','String'],['SURV_BEN_PER_TYP_CD','SURV_BEN_PER_TYP_CD','String'],['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['PM_CUR_IND','PM_CUR_IND','String']]

DFCompare.compare(sqlqadf,sqluatdf,cols)
#########################################ben_plan_dim#################################
##########################################################################################
"""
"""
#########################################party_dim#################################
##########################################################################################
html_logger.info("Start of Comparing party_dim " + dbserver1 + "--" + dbname1 + " vs " + dbserver2 + " -- " + dbname2)
sql1 = "select * from qa.party_dim where pm_cur_ind = 'Y' order by party_dim_gen_id"
sql2 = "select * from dbo.party_dim where pm_cur_ind = 'Y' order by party_dim_gen_id"

sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)
sqluatdf = sqluatdf.iloc[1:]

cols = [['ADMIN_SYS_NAME','ADMIN_SYS_NAME','String'],['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],['PARTY_TYP_CD','PARTY_TYP_CD','String'],['PARTY_TYP_DESCR','PARTY_TYP_DESCR','String'],
        ['PARTY_ROLE_TYP_TXT','PARTY_ROLE_TYP_TXT','String'],['PLCYHLDR_MSTR_NBR','PLCYHLDR_MSTR_NBR','String'],['PLCYHLDR_BR_NBR','PLCYHLDR_BR_NBR','String'],['PLCYHLDR_DEPT_NBR','PLCYHLDR_DEPT_NBR','String'],
        ['CMPNY_PREF_NAME','CMPNY_PREF_NAME','String'],['BILL_CNTCT_NAME','BILL_CNTCT_NAME','String'],['PLCYHLDR_BILL_SUBSYS_CD','PLCYHLDR_BILL_SUBSYS_CD','String'],
        ['PLCYHLDR_BILL_SUBSYS_DESCR','PLCYHLDR_BILL_SUBSYS_DESCR','String'],['RGNL_GRP_OFF_NAME','RGNL_GRP_OFF_NAME','String'],['RGNL_GRP_OFF_NBR','RGNL_GRP_OFF_NBR','String'],
        ['PLCYHLDR_EFF_DT','PLCYHLDR_EFF_DT','String'],['PLCYHLDR_TERM_DT','PLCYHLDR_TERM_DT','String'],['PLCYHLDR_REINSTMT_DT','PLCYHLDR_REINSTMT_DT','String'],['PLCYHLDR_STAT_CD','PLCYHLDR_STAT_CD','String'],
        ['PLCYHLDR_STAT_DESCR','PLCYHLDR_STAT_DESCR','String'],['PREM_PAY_GRACE_PER_CD','PREM_PAY_GRACE_PER_CD','String'],['PREM_PAY_GRACE_PER_DESCR','PREM_PAY_GRACE_PER_DESCR','String'],
        ['PD_THRU_DT','PD_THRU_DT','String'],['PLCYHLDR_FRST_SUBSD_PREF_NAME','PLCYHLDR_FRST_SUBSD_PREF_NAME','String'],['PLCYHLDR_SEC_SUBSD_PREF_NAME','PLCYHLDR_SEC_SUBSD_PREF_NAME','String'],
        ['PLCYHLDR_THIRD_SUBSD_PREF_NAME','PLCYHLDR_THIRD_SUBSD_PREF_NAME','String'],['PLCYHLDR_FOURTH_SUBSD_PREF_NAME','PLCYHLDR_FOURTH_SUBSD_PREF_NAME','String'],
        ['PLCYHLDR_LEGAL_NAME','PLCYHLDR_LEGAL_NAME','String'],['PLCYHLDR_STD_INDY_CLSFCTN_CD','PLCYHLDR_STD_INDY_CLSFCTN_CD','String'],['COV_TYP_CD','COV_TYP_CD','String'],
        ['ALLNC_CD','ALLNC_CD','String'],['ALLNC_DESCR','ALLNC_DESCR','String'],['EOB_MAIL_RCPNT_TYP_CD','EOB_MAIL_RCPNT_TYP_CD','String'],
        ['EOB_MAIL_RCPNT_TYP_DESCR','EOB_MAIL_RCPNT_TYP_DESCR','String'],['PM_CUR_IND','PM_CUR_IND','String']]

DFCompare.compare(sqlqadf,sqluatdf,cols)
#########################################party_dim#################################
##########################################################################################
"""

'''
#########################################Query#################################
##########################################################################################
sql1 = """select [ADMIN_SYS_PARTY_KEY_TXT], concat(b.BEN_PLAN_NBR,'-',b.[BEN_PLAN_TYP_CD],'-',
concat(substring(cast(a.[PLCYHLDR_BEN_PLAN_EFF_DT] as varchar(50)),6,2),'/',substring(cast(a.[PLCYHLDR_BEN_PLAN_EFF_DT] as varchar(50)),9,2),'/',substring(cast(a.[PLCYHLDR_BEN_PLAN_EFF_DT] as varchar(50)),1,4))) as classes
from qa.PARTY_PLAN_FACT a
join qa.BEN_PLAN_DIM b on  a.[BEN_PLAN_DIM_GEN_ID]=b.[BEN_PLAN_DIM_GEN_ID]
join qa.PARTY_DIM c on c.[PARTY_DIM_GEN_ID]=a.[PARTY_DIM_GEN_ID]
--left join [ENTPRS_CLAIMS_DM].qa.WRKSITE_OLS_COV_FACT d on d.[PARTY_DIM_GEN_ID]=a.[PARTY_DIM_GEN_ID] and a.[BEN_PLAN_DIM_GEN_ID]=d.[BEN_PLAN_DIM_GEN_ID]
--left join [ENTPRS_CLAIMS_DM].qa.TRAD_VOL_COV_FACT  e on e.[PARTY_DIM_GEN_ID]=a.[PARTY_DIM_GEN_ID] and a.[BEN_PLAN_DIM_GEN_ID]=e.[BEN_PLAN_DIM_GEN_ID]
where a.[PM_CUR_IND]='Y' and b.[PM_CUR_IND]='Y' and c.[PM_CUR_IND]='Y' and [PARTY_ROLE_TYP_TXT]= 'Policyholder'
and ben_plan_typ_cd in('CLC','CLP','LDA','LTD','VLD','VMD','CSC','CSP','SDA','STD','VSD')
and a.[PLCYHLDR_BEN_PLAN_TERM_DT]is null"""

sql2 = """
select [ADMIN_SYS_PARTY_KEY_TXT],
 concat(b.BEN_PLAN_NBR,'-',
        b.[BEN_PLAN_TYP_CD],'-',
        concat(substring(cast(a.[PLCYHLDR_BEN_PLAN_EFF_DT] as varchar(50)),6,2),'/',substring(cast(a.[PLCYHLDR_BEN_PLAN_EFF_DT] as varchar(50))
        ,9,2),'/',substring(cast(a.[PLCYHLDR_BEN_PLAN_EFF_DT] as varchar(50)),1,4))) as classes
  from dbo.PARTY_PLAN_FACT  a
join dbo.BEN_PLAN_DIM b on  a.BEN_PLAN_DIM_GEN_ID=b.BEN_PLAN_DIM_GEN_ID
join dbo.PARTY_DIM c on c.PARTY_DIM_GEN_ID =a.PARTY_DIM_GEN_ID
--left join [ENTPRS_CLAIMS_DM].qa.WRKSITE_OLS_COV_FACT d on d.[PARTY_DIM_GEN_ID]=a.[PARTY_DIM_GEN_ID] and a.[BEN_PLAN_DIM_GEN_ID]=d.[BEN_PLAN_DIM_GEN_ID]
--left join [ENTPRS_CLAIMS_DM].qa.TRAD_VOL_COV_FACT  e on e.[PARTY_DIM_GEN_ID]=a.[PARTY_DIM_GEN_ID] and a.[BEN_PLAN_DIM_GEN_ID]=e.[BEN_PLAN_DIM_GEN_ID]
where
ben_plan_typ_cd in('CLC','CLP','LDA','LTD','VLD','VMD','CSC','CSP','SDA','STD','VSD')
and a.PLCYHLDR_BEN_PLAN_TERM_DT is null
and a.PM_CUR_IND='Y' and b.PM_CUR_IND='Y' and c.PM_CUR_IND='Y' and PARTY_ROLE_TYP_TXT= 'Policyholder'"""

sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)

cols = [['ADMIN_SYS_PARTY_KEY_TXT','ADMIN_SYS_PARTY_KEY_TXT','String'],['classes','classes','String']]
DFCompare.compare(sqlqadf,sqluatdf,cols)
#########################################Query#################################
##########################################################################################

'''

"""
#########################################fineos_enum_xref#################################
##########################################################################################
html_logger.info("Start of Comparing fineos_enum_xref " + dbserver1 + "--" + dbname1 + " vs " + dbserver2 + " -- " + dbname2)
sql1 = "select * from qa.fineos_enum_xref where pm_cur_ind = 'Y' order by fineos_enum_xref_gen_id"
sql2 = "select * from dbo.fineos_enum_xref where pm_cur_ind = 'Y' order by fineos_enum_xref_gen_id"

sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)
 
cols = [['FINEOS_ENUM_ID','FINEOS_ENUM_ID','String'],['FINEOS_DOM_ID','FINEOS_DOM_ID','String'],['FINEOS_DOM_NAME','FINEOS_DOM_NAME','String'],['FINEOS_ENUM_INSTNC_NAME','FINEOS_ENUM_INSTNC_NAME','String'],
        ['XREF_TBL_NAME','XREF_TBL_NAME','String'],['XREF_COL_NAME','XREF_COL_NAME','String'],['XREF_VAL_TXT','XREF_VAL_TXT','String'],['PM_CUR_IND','PM_CUR_IND','String']]

DFCompare.compare(sqlqadf,sqluatdf,cols)
#########################################fineos_enum_xref#################################
##########################################################################################
"""

"""
#########################################Trad_vol_cov_fact#################################
##########################################################################################
html_logger.info("Start of Comparing Trad_vol_cov_fact " + dbserver1 + "--" + dbname1 + " vs " + dbserver2 + " -- " + dbname2)
sql1 = "select * from qa.Trad_vol_cov_fact where pm_cur_ind = 'Y' order by party_dim_gen_id"
sql2 = "select * from dbo.Trad_vol_cov_fact where pm_cur_ind = 'Y' order by party_dim_gen_id"

sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)
 
cols = [['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['CNTRCT_OPT_NBR','CNTRCT_OPT_NBR','String'],
        ['BEN_LMT_TYP_CD','BEN_LMT_TYP_CD','String'],['BEN_LMT_TYP_DESCR','BEN_LMT_TYP_DESCR','String'],['SICK_LEAVE_ELIM_PER_CNTRCT_CD','SICK_LEAVE_ELIM_PER_CNTRCT_CD','String'],
        ['SICK_LEAVE_ELIM_PER_CNTRCT_DESCR','SICK_LEAVE_ELIM_PER_CNTRCT_DESCR','String'],['PM_CUR_IND','PM_CUR_IND','String']]

DFCompare.compare(sqlqadf,sqluatdf,cols)
#########################################Trad_vol_cov_fact#################################
##########################################################################################
"""


#########################################wrksite_ols_cov_fact#################################
##########################################################################################
html_logger.info("Start of Comparing wrksite_ols_cov_fact " + dbserver1 + "--" + dbname1 + " vs " + dbserver2 + " -- " + dbname2)
sql1 = "select * from dev.wrksite_ols_cov_fact where pm_cur_ind = 'Y' order by party_dim_gen_id"
sql2 = "select * from dev.wrksite_ols_cov_fact where pm_cur_ind = 'Y' order by party_dim_gen_id"

sqlqadf = Parser.parseSQL(sql1,dbserver1,dbname1)
sqluatdf = Parser.parseSQL(sql2,dbserver2,dbname2)
 
cols = [['PLCYHLDR_BEN_PLAN_EFF_DT','PLCYHLDR_BEN_PLAN_EFF_DT','String'],['PLCYHLDR_BEN_PLAN_TERM_DT','PLCYHLDR_BEN_PLAN_TERM_DT','String'],['WRKSITE_BEN_INJRY_ELIM_PER_CD','WRKSITE_BEN_INJRY_ELIM_PER_CD','String'],
        ['WRKSITE_BEN_INJRY_ELIM_PER_DESCR','WRKSITE_BEN_INJRY_ELIM_PER_DESCR','String'],['WRKSITE_BEN_SICK_ELIM_PER_CD','WRKSITE_BEN_SICK_ELIM_PER_CD','String'],['WRKSITE_BEN_SICK_ELIM_PER_DESCR','WRKSITE_BEN_SICK_ELIM_PER_DESCR','String'],
        ['WRKSITE_DISBLTY_OPT_NBR','WRKSITE_DISBLTY_OPT_NBR','String'],['WRKSITE_DRUG_ALCOL_LIMIT_PER_CD','WRKSITE_DRUG_ALCOL_LIMIT_PER_CD','String'],['WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR','WRKSITE_DRUG_ALCOL_LIMIT_PER_DESCR','String'],
        ['WRKSITE_RTRN_TO_WRK_BEN_PER_CD','WRKSITE_RTRN_TO_WRK_BEN_PER_CD','String'],['WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR','WRKSITE_RTRN_TO_WRK_BEN_PER_DESCR','String'],['FULL_TM_STAT_AVG_PER_NBR','FULL_TM_STAT_AVG_PER_NBR','String'],
        ['FULL_TM_STAT_AVG_PER_TYP_CD','FULL_TM_STAT_AVG_PER_TYP_CD','String'],['FULL_TM_STAT_AVG_PER_TYP_DESCR','FULL_TM_STAT_AVG_PER_TYP_DESCR','String'],['OLS_ELIM_PER_TYP_DESCR','OLS_ELIM_PER_TYP_DESCR','String'],['OLS_QLFCTN_DISBLTY_TYP_CD','OLS_QLFCTN_DISBLTY_TYP_CD','String'],['OLS_QLFCTN_DISBLTY_TYP_DESCR','OLS_QLFCTN_DISBLTY_TYP_DESCR','String'],['MNTL_ILL_BEN_DUR_TYP_CD','MNTL_ILL_BEN_DUR_TYP_CD','String'],['MNTL_ILL_BEN_DUR_TYP_DESCR','MNTL_ILL_BEN_DUR_TYP_DESCR','String'],['MAX_INJRY_BEN_DUR_CD','MAX_INJRY_BEN_DUR_CD','String'],['MAX_SICK_BEN_DUR_CD','MAX_SICK_BEN_DUR_CD','String'],['OTH_INCM_INTGRTN_CD','OTH_INCM_INTGRTN_CD','String'],['OTH_INCM_INTGRTN_DESCR','OTH_INCM_INTGRTN_DESCR','String'],['TOT_DISBLTY_DUR_TYP_CD','TOT_DISBLTY_DUR_TYP_CD','String'],['TOT_DISBLTY_DUR_TYP_DESCR','TOT_DISBLTY_DUR_TYP_DESCR','String'],['LUMP_SUM_ELIM_PER_NBR','LUMP_SUM_ELIM_PER_NBR','String'],['INDV_COV_EFF_TYP_CD','INDV_COV_EFF_TYP_CD','String'],['INDV_COV_EFF_TYP_DESCR','INDV_COV_EFF_TYP_DESCR','String'],['BEN_RDCTN_CD','BEN_RDCTN_CD','String'],['BEN_RDCTN_DESCR','BEN_RDCTN_DESCR','String'],['SLRY_CONT_TYP_CD','SLRY_CONT_TYP_CD','String'],['SLRY_CONT_TYP_DESCR','SLRY_CONT_TYP_DESCR','String'],['FULL_TM_CNTRCT_IND','FULL_TM_CNTRCT_IND','String'],['PRE_EXIT_COND_NOT_CVRD_PER_NBR','PRE_EXIT_COND_NOT_CVRD_PER_NBR','String'],['PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','PRE_EXIT_COND_NOT_CVRD_PER_TYP_CD','String'],['PRE_EXIT_COND_NOT_CVRD_PER_DESCR','PRE_EXIT_COND_NOT_CVRD_PER_DESCR','String'],['POST_EFF_DT_TRTMT_FREE_PER_NBR','POST_EFF_DT_TRTMT_FREE_PER_NBR','String'],['POST_EFF_DT_TRTMT_FREE_PER_TYP_CD','POST_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],['POST_EFF_DT_TRTMT_FREE_PER_DESCR','POST_EFF_DT_TRTMT_FREE_PER_DESCR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_NBR','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_TYP_CD','String'],['PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','PRIOR_TO_EFF_DT_TRTMT_FREE_PER_DESCR','String'],['BSC_EARN_INCM_TYP_CD','BSC_EARN_INCM_TYP_CD','String'],['BSC_EARN_INCM_TYP_DESCR','BSC_EARN_INCM_TYP_DESCR','String'],['WRKSITE_DISBLTY_BSC_EARN_TYP_CD','WRKSITE_DISBLTY_BSC_EARN_TYP_CD','String'],['WRKSITE_DISBLTY_BSC_EARN_TYP_DESCR','WRKSITE_DISBLTY_BSC_EARN_TYP_DESCR','String'],['OCPTN_DISBLTY_CD','OCPTN_DISBLTY_CD','String'],['OCPTN_DISBLTY_DESCR','OCPTN_DISBLTY_DESCR','String'],['OLS_DEATH_BEN_PCT','OLS_DEATH_BEN_PCT','String'],['OLS_SUBS_ABUSE_BEN_LMT_PCT','OLS_SUBS_ABUSE_BEN_LMT_PCT','String'],['OLS_MNTL_ILL_BEN_LMT_PCT','OLS_MNTL_ILL_BEN_LMT_PCT','String'],['OLS_SPEC_COND_BEN_LMT_PCT','OLS_SPEC_COND_BEN_LMT_PCT','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_NBR','FULL_TM_STAT_QLFCTN_THRSHLD_NBR','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_CD','String'],['FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','FULL_TM_STAT_QLFCTN_THRSHLD_TYP_DESCR','String'],['PARTNERSHIP_INCM_TYP_CD','PARTNERSHIP_INCM_TYP_CD','String'],['PARTNERSHIP_INCM_TYP_DESCR','PARTNERSHIP_INCM_TYP_DESCR','String'],['S_CORP_INCM_TYP_CD','S_CORP_INCM_TYP_CD','String'],['S_CORP_INCM_TYP_DESCR','S_CORP_INCM_TYP_DESCR','String'],['SOLE_PROP_INCM_TYP_CD','SOLE_PROP_INCM_TYP_CD','String'],['SOLE_PROP_INCM_TYP_DESCR','SOLE_PROP_INCM_TYP_DESCR','String'],['RTRN_TO_WRK_PER_NBR','RTRN_TO_WRK_PER_NBR','String'],['RTRN_TO_WRK_PER_TYP_CD','RTRN_TO_WRK_PER_TYP_CD','String'],['RTRN_TO_WRK_PER_TYP_DESCR','RTRN_TO_WRK_PER_TYP_DESCR','String'],['SURV_BEN_PER_NBR','SURV_BEN_PER_NBR','String'],['SURV_BEN_PER_TYP_CD','SURV_BEN_PER_TYP_CD','String'],['SURV_BEN_PER_TYP_DESCR','SURV_BEN_PER_TYP_DESCR','String'],['BSC_EARN_AVG_PER_NBR','BSC_EARN_AVG_PER_NBR','String'],['BSC_EARN_AVG_PER_TYP_CD','BSC_EARN_AVG_PER_TYP_CD','String'],['BSC_EARN_AVG_PER_TYP_DESCR','BSC_EARN_AVG_PER_TYP_DESCR','String'],['TOTAL_DISBLTY_JOB_CLSFCTN_TYP_CD','TOTAL_DISBLTY_JOB_CLSFCTN_TYP_CD','String'],['TOTAL_DISBLTY_JOB_CLSFCTN_TYP_DESCR','TOTAL_DISBLTY_JOB_CLSFCTN_TYP_DESCR','String'],['PM_CUR_IND','PM_CUR_IND','String']
]

DFCompare.compare(sqlqadf,sqluatdf,cols)
#########################################Trad_vol_cov_fact#################################
##########################################################################################




#######   End of  UAT vs Dev Comparison  ############################################ 
#################################################################################################



"""
setup("Payment Validation","Executed on " + str(now),LOG_FILENAME)
dbserver = 'SQLD10747,4242'
dbname = 'ENTPRS_CLAIMS_DM'
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.PARTY_DIM vs dev.PARTY_DIM")   

sql1 = '''select * from dbo.PARTY_DIM p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.PARTY_DIM p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.PARTY_DTL_EXT_DIM vs dev.PARTY_DTL_EXT_DIM")   

sql1 = '''select * from dbo.PARTY_DTL_EXT_DIM p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.PARTY_DTL_EXT_DIM p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.PARTY_PLAN_FACT vs dev.PARTY_PLAN_FACT")   

sql1 = '''select * from dbo.PARTY_PLAN_FACT p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.PARTY_PLAN_FACT p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.BEN_PLAN_DIM vs dev.BEN_PLAN_DIM")   

sql1 = '''select * from dbo.BEN_PLAN_DIM p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.BEN_PLAN_DIM p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.CAUSE_LMT_DIM vs dev.CAUSE_LMT_DIM")   

sql1 = '''select * from dbo.CAUSE_LMT_DIM p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.CAUSE_LMT_DIM p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.CAUSE_REL_BEN_LMT_DIM vs dev.CAUSE_REL_BEN_LMT_DIM")   

sql1 = '''select * from dbo.CAUSE_REL_BEN_LMT_DIM p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.CAUSE_REL_BEN_LMT_DIM p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.FINEOS_ENUM_XREF vs dev.FINEOS_ENUM_XREF")   

sql1 = '''select * from dbo.FINEOS_ENUM_XREF p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.FINEOS_ENUM_XREF p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.TRAD_VOL_COV_FACT vs dev.TRAD_VOL_COV_FACT")   

sql1 = '''select * from dbo.TRAD_VOL_COV_FACT p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.TRAD_VOL_COV_FACT p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing dbo.WRKSITE_OLS_COV_FACT vs dev.WRKSITE_OLS_COV_FACT")   

sql1 = '''select * from dbo.WRKSITE_OLS_COV_FACT p'''
sql1df = Parser.parseSQL(sql1,dbserver,dbname)

sql2 = '''select * from dev.WRKSITE_OLS_COV_FACT p'''
sql2df = Parser.parseSQL(sql2,dbserver,dbname)


DFCompare.diffDataFrame_allCols(sql1df,sql2df)
#-------------------------------------------------------------------------------------------------------------
"""


"""
xmlpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\OneAmerica_Disbursement.xml"
xmldfcols = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']

date1 = '2020-03-04'
LZDate = '2020-03-04'
dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'
#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing XML with ODS")   

xmldf = Parser.parseXML(xmlpath,xmldfcols)

sql1 = '''select PAY_RQST_NBR,PAY_ADMIN_SYS,CMPNY_CD,SRC_CD,LGCY_SRC_CD,PAY_TYP_CD,PAY_AMT,PAYEE_NAME,CHK_ADDR_LINE_1,CHK_ADDR_LINE_2,CHK_ADDR_CITY_NAME,CHK_ADDR_ST_CD,CHK_ADDR_ZIP_CD,MAIL_SORT_CD,ENT_OPER_USR_ID,RDFI_ROUT_NBR,RDFI_ACCT_NBR,RDFI_ACCT_TYP_DESC
            from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\')'''

sqldf = Parser.parseSQL(sql1,dbserver,dbname)

cols = [['Pay_Rqst_Nbr','PAY_RQST_NBR','String'],['Pay_Admin_Sys','PAY_ADMIN_SYS','String'],['Cmpny_Cd','CMPNY_CD','String'],['Src_Cd','SRC_CD','String'],['Lgcy_Src_Cd','LGCY_SRC_CD','String'],
        ['Pay_Typ','PAY_TYP_CD','String'],['Pay_Amt','PAY_AMT','float'],['Payee_Name','PAYEE_NAME','String'],['Chk_Addr_Line_1','CHK_ADDR_LINE_1','String'],['Chk_Addr_Line_2','CHK_ADDR_LINE_2','String'],
        ['Chk_Addr_Zip_Cd','CHK_ADDR_ZIP_CD','String'],['Chk_Addr_City','CHK_ADDR_CITY_NAME','String'],['Chk_Addr_St','CHK_ADDR_ST_CD','String'],['Mail_Sort_Cd','MAIL_SORT_CD','String'],
        ['Ent_Oper_Usr_Id','ENT_OPER_USR_ID','String'],['Rdfi_Rout_Nbr','RDFI_ROUT_NBR','float'],['Rdfi_Acct_Nbr','RDFI_ACCT_NBR','float'],['Rdfi_Acct_Typ','RDFI_ACCT_TYP_DESC','String']]

DFCompare.compare(xmldf,sqldf,cols)

#--------------------------------------------------------------------------------------------------------------------

html_logger.info("Start of Comparing XML with LandingZone")   

sql2 = '''select index_id from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\')'''
index_id = Parser.parseSQL(sql2,dbserver,dbname)


sql1 = '''select 'FINEOS' as Pay_Admin_Sys, 'AUL' as Cmpny_cd,

        case when A.AMALGAMATIONC <> 'Expense' and A.PAYMENTMETHOD = 'Check' then '8DCLC'
             when A.AMALGAMATIONC <> 'Expense' and A.PAYMENTMETHOD = 'Elec Funds Transfer' then '8DCLE'
             WHEN A.AMALGAMATIONC = 'Expense' and A.PAYMENTMETHOD = 'Check' THEN '8DAPC'
             WHEN A.AMALGAMATIONC = 'Expense' and A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '8DAPE' End as srcCD,

        case when A.AMALGAMATIONC <> 'Expense' and A.PAYMENTMETHOD = 'Check' then '8DCLC'
             when A.AMALGAMATIONC <> 'Expense' and A.PAYMENTMETHOD = 'Elec Funds Transfer' then '8DCLE'
             WHEN A.AMALGAMATIONC = 'Expense' and A.PAYMENTMETHOD = 'Check' THEN '8DAPC'
             WHEN A.AMALGAMATIONC = 'Expense' and A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '8DAPE' END AS Lgcy_Src_Cd,

CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN 'EFT' Else 'Check' END AS PAYMENTMETHOD,
A.GROSSPAYMENTA_MONAMT, case when Nametoprinton is null then CASE WHEN SUBSTRING(A.payeefullname,1,CHARINDEX(' ', A.payeefullname)) in ('Mr','Mrs','Miss','Ms','Dr','Sir') Then SUBSTRING(A.payeefullname,CHARINDEX(' ', A.payeefullname)+1,len(A.payeefullname) - CHARINDEX(' ', A.payeefullname)+1)
        Else A.payeefullname End else Nametoprinton  End as payeename1, CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD1 END AS PAYMENTADD1_mod,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD2 END AS PAYMENTADD2_mod,
CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD3 END AS PAYMENTADD3_mod,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD4 END AS PAYMENTADD4_mod,
CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD6 END AS PAYMENTADD6_mod,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTPOSTCO END AS PAYMENTPOSTCO_mod,
'MA' as Mail_Sort_Cd,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID,CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '' Else A.PAYEEBANKSORT END AS PAYEEBANKSORT_mod,
CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '' Else A.PAYEEACCOUNTN END AS PAYEEACCOUNTN_mod,CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '' Else A.PAYEEACCOUNTT END AS PAYEEACCOUNTT_mod
from FINEOS.PEI_RQST_INFO_VW  A  where PEI_I in (''' + ','.join(str(s) for s in index_id['index_id'].astype(int).values) + ')'

#where Convert(Date,SOURCE_LASTUPDATEDATE) = \''''+ LZDate + '''\''''

'''
from LZ.PEI_RQST_INFO A where PEI_I in (select Index_id from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + LZDate + '''\'))
and PEI_C in (select class_ID from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + LZDate + '''\'))
'''    
dbserver = 'SQLD10667,4242'
dbname = 'ENTPRS_LANDINGZONE'            
sqldf = Parser.parseSQL(sql1,dbserver,dbname)

cols = [['Pay_Admin_Sys','Pay_Admin_Sys','String'],['Cmpny_Cd','Cmpny_cd','String'],['Src_Cd','srcCD','String'],['Lgcy_Src_Cd','Lgcy_Src_Cd','String'],
        ['Pay_Typ','PAYMENTMETHOD','String'],['Pay_Amt','GROSSPAYMENTA_MONAMT','float'],['Payee_Name','payeename1','String'],['Chk_Addr_Line_1','PAYMENTADD1_mod','String'],
        ['Chk_Addr_Line_2','PAYMENTADD2_mod','String'],['Chk_Addr_Line_3','PAYMENTADD3_mod','String'],['Chk_Addr_City','PAYMENTADD4_mod','String'],
        ['Chk_Addr_Zip_Cd','PAYMENTPOSTCO_mod','String'],['Chk_Addr_St','PAYMENTADD6_mod','String'],['Mail_Sort_Cd','Mail_Sort_Cd','String'],
        ['Ent_Oper_Usr_Id','ENT_OP_ID','String'],['Rdfi_Rout_Nbr','PAYEEBANKSORT_mod','String'],['Rdfi_Acct_Nbr','PAYEEACCOUNTN_mod','String'],['Rdfi_Acct_Typ','PAYEEACCOUNTT_mod','String']]

DFCompare.compare(xmldf,sqldf,cols)


#--------------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing PEI CSV with LandingZone")

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv"
csvdf = Parser.parseCSV(csvpath)
csvdf = csvdf[csvdf.PAYMENTMETHOD.isin(['Check','Elec Funds Transfer'])]
csvdf = csvdf[~csvdf.EVENTTYPE.isin(['Overpayment Actual Recovery'])]


sql1 = '''SELECT PEI_C,PEI_I,SOURCE_LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,ADDRESSLINE1,ADDRESSLINE2,ADDRESSLINE3,ADDRESSLINE4,ADDRESSLINE5,
        ADDRESSLINE6,ADDRESSLINE7,ADVICETOPAY,ADVICETOPAYOV,AMALGAMATIONC,AMOUNT_MONAMT,AMOUNT_MONCUR,CHECKCUTTING,CONFIRMEDBYUS,CONFIRMEDUID,CONTRACTREF,
        CORRESPCOUNTR,CURRENCY,DATEINTERFACE,DESCRIPTION,EMPLOYEECONTR,EVENTEFFECTIV,EVENTREASON,EVENTTYPE,EXTRACTIONDAT,GROSSPAYMENTA_MONAMT,GROSSPAYMENTA_MONCUR,
        INSUREDRESIDE,NAMETOPRINTON,NOMINATEDPAYE,NOMPAYEECUSTO,NOMPAYEEDOB,NOMPAYEEFULLN,NOMPAYEESOCNU,NOTES,PAYEEACCOUNTN,PAYEEACCOUNTT,PAYEEADDRESS,PAYEEBANKBRAN,
        PAYEEBANKCODE,PAYEEBANKINST,PAYEEBANKSORT,PAYEECORRESPO,PAYEECUSTOMER,PAYEEDOB,PAYEEFULLNAME,PAYEEIDENTIFI,REPLICATE('0', 9-LEN(PAYEESOCNUMBE)) + PAYEESOCNUMBE as PAYEESOCNUMBE,
        PAYMENTADD,PAYMENTADD1,PAYMENTADD2,PAYMENTADD3,PAYMENTADD4,PAYMENTADD5,PAYMENTADD6,PAYMENTADD7,PAYMENTADDCOU,PAYMENTCORRST,PAYMENTDATE,PAYMENTFREQUE,PAYMENTMETHOD,PAYMENTPOSTCO,
        PAYMENTPREMIS,PAYMENTTRIGGE,PAYMENTTYPE,PAYMETHCURREN,POSTCODE,PREMISESNO,SETUPBYUSERID,SETUPBYUSERNA,STATUS,STATUSEFFECTI,STATUSREASON,STOCKNO,SUMMARYEFFECT,SUMMARYSTATUS,
        TRANSSTATUSDA FROM FINEOS.PEI_RQST_INFO_VW'''
sqldf = Parser.parseSQL(sql1,dbserver,dbname)

cols = [['C','PEI_C','float'],['I','PEI_I','float'],['LASTUPDATEDATE','SOURCE_LASTUPDATEDATE','Date'],['C_OSUSER_UPDATEDBY','C_OSUSER_UPDATEDBY','float'],
        ['I_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','float'],['ADDRESSLINE1','ADDRESSLINE1','String'],['ADDRESSLINE2','ADDRESSLINE2','String'],['ADDRESSLINE3','ADDRESSLINE3','String'],
        ['ADDRESSLINE4','ADDRESSLINE4','String'],['ADDRESSLINE5','ADDRESSLINE5','String'],['ADDRESSLINE6','ADDRESSLINE6','String'],
        ['ADDRESSLINE7','ADDRESSLINE7','String'],['ADVICETOPAY','ADVICETOPAY','String'],['ADVICETOPAYOV','ADVICETOPAYOV','String'],
        ['AMALGAMATIONC','AMALGAMATIONC','String'],['AMOUNT_MONAMT','AMOUNT_MONAMT','float'],['AMOUNT_MONCUR','AMOUNT_MONCUR','float'],['CHECKCUTTING','CHECKCUTTING','String'],
        ['CONFIRMEDBYUS','CONFIRMEDBYUS','String'],['CONFIRMEDUID','CONFIRMEDUID','String'],['CONTRACTREF','CONTRACTREF','String'],['CORRESPCOUNTR','CORRESPCOUNTR','String'],
        ['CURRENCY','CURRENCY','String'],['DATEINTERFACE','DATEINTERFACE','Date'],['DESCRIPTION','DESCRIPTION','String'],['EMPLOYEECONTR','EMPLOYEECONTR','float'],
        ['EVENTEFFECTIV','EVENTEFFECTIV','date'],['EVENTREASON','EVENTREASON','String'],['EVENTTYPE','EVENTTYPE','String'],['EXTRACTIONDAT','EXTRACTIONDAT','String'],
        ['GROSSPAYMENTA_MONAMT','GROSSPAYMENTA_MONAMT','float'],['GROSSPAYMENTA_MONCUR','GROSSPAYMENTA_MONCUR','float'],['INSUREDRESIDE','INSUREDRESIDE','String'],
        ['NAMETOPRINTON','NAMETOPRINTON','String'],['NOMINATEDPAYE','NOMINATEDPAYE','String'],['NOMPAYEECUSTO','NOMPAYEECUSTO','String'],['NOMPAYEEDOB','NOMPAYEEDOB','Date'],
        ['NOMPAYEEFULLN','NOMPAYEEFULLN','String'],['NOMPAYEESOCNU','NOMPAYEESOCNU','String'],['NOTES','NOTES','String'],['PAYEEACCOUNTN','PAYEEACCOUNTN','float'],
        ['PAYEEACCOUNTT','PAYEEACCOUNTT','String'],['PAYEEADDRESS','PAYEEADDRESS','String'],['PAYEEBANKBRAN','PAYEEBANKBRAN','String'],['PAYEEBANKCODE','PAYEEBANKCODE','String'],
        ['PAYEEBANKINST','PAYEEBANKINST','String'],['PAYEEBANKSORT','PAYEEBANKSORT','float'],['PAYEECORRESPO','PAYEECORRESPO','String'],['PAYEECUSTOMER','PAYEECUSTOMER','String'],
        ['PAYEEDOB','PAYEEDOB','Date'],['PAYEEFULLNAME','PAYEEFULLNAME','String'],['PAYEEIDENTIFI','PAYEEIDENTIFI','String'],['PAYEESOCNUMBE','PAYEESOCNUMBE','floatLen-9-0-left'],
        ['PAYMENTADD','PAYMENTADD','String'],['PAYMENTADD1','PAYMENTADD1','String'],['PAYMENTADD2','PAYMENTADD2','String'],['PAYMENTADD3','PAYMENTADD3','String'],
        ['PAYMENTADD4','PAYMENTADD4','String'],['PAYMENTADD5','PAYMENTADD5','String'],['PAYMENTADD6','PAYMENTADD6','String'],['PAYMENTADD7','PAYMENTADD7','String'],
        ['PAYMENTADDCOU','PAYMENTADDCOU','String'],['PAYMENTCORRST','PAYMENTCORRST','String'],['PAYMENTDATE','PAYMENTDATE','Date'],['PAYMENTFREQUE','PAYMENTFREQUE','String'],
        ['PAYMENTMETHOD','PAYMENTMETHOD','String'],['PAYMENTPOSTCO','PAYMENTPOSTCO','float'],['PAYMENTPREMIS','PAYMENTPREMIS','String'],['PAYMENTTRIGGE','PAYMENTTRIGGE','Date'],
        ['PAYMENTTYPE','PAYMENTTYPE','String'],['PAYMETHCURREN','PAYMETHCURREN','String'],['POSTCODE','POSTCODE','float'],['PREMISESNO','PREMISESNO','String'],
        ['SETUPBYUSERID','SETUPBYUSERID','String'],['SETUPBYUSERNA','SETUPBYUSERNA','String'],['STATUS','STATUS','String'],['STATUSEFFECTI','STATUSEFFECTI','Date'],
        ['STATUSREASON','STATUSREASON','String'],['STOCKNO','STOCKNO','String'],['SUMMARYEFFECT','SUMMARYEFFECT','Date'],['SUMMARYSTATUS','SUMMARYSTATUS','String'],
        ['TRANSSTATUSDA','TRANSSTATUSDA','Date']]

DFCompare.compare(csvdf,sqldf,cols)        

#--------------------------------------------------------------------------------------------------------------------

html_logger.info("Start of Comparing PEIPaymentDetails CSV with LandingZone")

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentDetails.csv"
csvdf = Parser.parseCSV(csvpath)

sql1 = '''select PEI_C,PEI_I,SOURCE_LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,BENEFITEFFECT,BENEFITFINALP,DESCRIPTION_PAYMENTDTLS,PAYMENTENDPER,PAYMENTSTARTP,
            BALANCINGAMOU_MONAMT,BALANCINGAMOU_MONCUR,BUSINESSNETBE_MONAMT,BUSINESSNETBE_MONCUR,DUETYPE,GROUPID,PECLASSID,PEINDEXID,CLAIMDETAILSCLASSID,
            CLAIMDETAILSINDEXID,DATEINTERFACE,LAST_UPDT_ID,LAST_UPDT_TS from FINEOS.PEI_PAY_DTL_VW'''

sqldf = Parser.parseSQL(sql1,dbserver,dbname)

cols = [['PEI_C','C','float'],['PEI_I','I','float'],['SOURCE_LASTUPDATEDATE','LASTUPDATEDATE','Date'],['C_OSUSER_UPDATEDBY','C_OSUSER_UPDATEDBY','float'],
        ['I_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','float'],['BENEFITEFFECT','BENEFITEFFECT','Date'],['BENEFITFINALP','BENEFITFINALP','Date'],['DESCRIPTION_PAYMENTDTLS','DESCRIPTION_PAYMENTDTLS','String'],
        ['PAYMENTENDPER','PAYMENTENDPER','Date'],['PAYMENTSTARTP','PAYMENTSTARTP','Date'],['BALANCINGAMOU_MONAMT','BALANCINGAMOU_MONAMT','float'],
        ['BALANCINGAMOU_MONCUR','BALANCINGAMOU_MONCUR','float'],['BUSINESSNETBE_MONAMT','BUSINESSNETBE_MONAMT','float'],['BUSINESSNETBE_MONCUR','BUSINESSNETBE_MONCUR','float'],
        ['DUETYPE','DUETYPE','String'],['GROUPID','GROUPID','String'],['PECLASSID','PECLASSID','float'],['PEINDEXID','PEINDEXID','float'],
        ['CLAIMDETAILSCLASSID','CLAIMDETAILSCLASSID','float'],['CLAIMDETAILSINDEXID','CLAIMDETAILSINDEXID','float'],['DATEINTERFACE','DATEINTERFACE','Date']]

DFCompare.compare(sqldf,csvdf,cols)


#--------------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing PEIPaymentLine CSV with LandingZone")

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentLine.csv"
csvdf = Parser.parseCSV(csvpath)

sql1 = '''select PEI_C,PEI_I,SOURCE_LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,AMOUNT_MONAMT,AMOUNT_MONCUR,INTEGRTYPE,LINETYPE,REFERENCE,RESERVECATEGO,RESERVETYPE,
            SEQUENCENUMBE,SUBTOTALS,TAXABLEINCOME,USETOCALCRULE,C_PYMNTEIF_PAYMENTLINES,I_PYMNTEIF_PAYMENTLINES,PAYMENTDETAILCLASSID,PAYMENTDETAILINDEXID,
            PURCHASEDETAILCLASSID,PURCHASEDETAILINDEXID,DATEINTERFACE from FINEOS.PEI_PAY_LINE_VW'''            
sqldf = Parser.parseSQL(sql1,dbserver,dbname)

cols = [['PEI_C','C','float'],['PEI_I','I','float'],['SOURCE_LASTUPDATEDATE','LASTUPDATEDATE','Date'],['C_OSUSER_UPDATEDBY','C_OSUSER_UPDATEDBY','float'],
        ['I_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','float'],['AMOUNT_MONAMT','AMOUNT_MONAMT','String'],['AMOUNT_MONCUR','AMOUNT_MONCUR','float'],['INTEGRTYPE','INTEGRTYPE','String'],
        ['LINETYPE','LINETYPE','String'],['REFERENCE','REFERENCE','String'],['RESERVECATEGO','RESERVECATEGO','float'],
        ['RESERVETYPE','RESERVETYPE','float'],['SEQUENCENUMBE','SEQUENCENUMBE','float'],['SUBTOTALS','SUBTOTALS','string'],
        ['TAXABLEINCOME','TAXABLEINCOME','float'],['USETOCALCRULE','USETOCALCRULE','String'],['C_PYMNTEIF_PAYMENTLINES','C_PYMNTEIF_PAYMENTLINES','float'],['I_PYMNTEIF_PAYMENTLINES','I_PYMNTEIF_PAYMENTLINES','float'],
        ['PAYMENTDETAILCLASSID','PAYMENTDETAILCLASSID','float'],['PAYMENTDETAILINDEXID','PAYMENTDETAILINDEXID','float'],['PURCHASEDETAILCLASSID','PURCHASEDETAILCLASSID','String'],
        ['PURCHASEDETAILINDEXID','PURCHASEDETAILINDEXID','String'],['DATEINTERFACE','DATEINTERFACE','Date']]

DFCompare.compare(sqldf,csvdf,cols)


#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing Claim Detail CSV with LandingZone") 
csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIClaimDetails.csv"
csvdf = Parser.parseCSV(csvpath)

sql1 = '''select PEI_C,PEI_I,SOURCE_LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,ABSENCECASENU,BENEFITRIGHTT,CLAIMANTAGE,CLAIMANTCUSTO,
             CLAIMANTDOB,CLAIMANTGENDE,CLAIMANTNAME,CLAIMANTRELTO,CLAIMNUMBER,DIAGCODE2,DIAGCODE3,DIAGCODE4,DIAGCODE5,EMPLOYEEID,
             EVENTCAUSE,INCURREDDATE,INSUREDADDRES,INSUREDADDRL1,INSUREDADDRL2,INSUREDADDRL3,INSUREDADDRL4,INSUREDADDRL5,
             INSUREDADDRL6,INSUREDADDRL7,INSUREDAGE,INSUREDCORCOU,INSUREDCORRES,INSUREDCUSTOM,INSUREDDOB,INSUREDEMPLOY,INSUREDFULLNA,
             INSUREDGENDER,INSUREDPOSTCO,INSUREDPREMIS,INSUREDRETIRE,INSUREDSOCNUM,LEAVEPLANID,LEAVEREQUESTI,NOTIFIEDDATE,PAYEEAGEATINC,
             PAYEECASEROLE,PAYEERELTOINS,PRIMARYDIAGNO,PRIMARYMEDICA,DIAG2MEDICALC,DIAG3MEDICALC,DIAG4MEDICALC,DIAG5MEDICALC,PECLASSID,PEINDEXID,DATEINTERFACE
             from FINEOS.PEI_CLAIM_DTL_VW'''
sqldf = Parser.parseSQL(sql1,dbserver,dbname)

cols = [['PEI_C','C','float'],['PEI_I','I','float'],['SOURCE_LASTUPDATEDATE','LASTUPDATEDATE','date'],['C_OSUSER_UPDATEDBY','C_OSUSER_UPDATEDBY','float'],
        ['I_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','float'],['ABSENCECASENU','ABSENCECASENU','string'],['BENEFITRIGHTT','BENEFITRIGHTT','string'],
        ['CLAIMANTAGE','CLAIMANTAGE','float'],['CLAIMANTCUSTO','CLAIMANTCUSTO','string'],['CLAIMANTDOB','CLAIMANTDOB','date'],['CLAIMANTGENDE','CLAIMANTGENDE','string'],
        ['CLAIMANTNAME','CLAIMANTNAME','string'],['CLAIMANTRELTO','CLAIMANTRELTO','string'],['CLAIMNUMBER','CLAIMNUMBER','string'],['DIAGCODE2','DIAGCODE2','string'],
        ['DIAGCODE3','DIAGCODE3','string'],['DIAGCODE4','DIAGCODE4','string'],['DIAGCODE5','DIAGCODE5','string'],['EMPLOYEEID','EMPLOYEEID','string'],['EVENTCAUSE','EVENTCAUSE','string'],
        ['INCURREDDATE','INCURREDDATE','date'],['INSUREDADDRES','INSUREDADDRES','string'],['INSUREDADDRL1','INSUREDADDRL1','string'],['INSUREDADDRL2','INSUREDADDRL2','string'],
        ['INSUREDADDRL3','INSUREDADDRL3','string'],['INSUREDADDRL4','INSUREDADDRL4','string'],['INSUREDADDRL5','INSUREDADDRL5','string'],['INSUREDADDRL6','INSUREDADDRL6','string'],
        ['INSUREDADDRL7','INSUREDADDRL7','string'],['INSUREDAGE','INSUREDAGE','float'],['INSUREDCORCOU','INSUREDCORCOU','string'],['INSUREDCORRES','INSUREDCORRES','string'],
        ['INSUREDCUSTOM','INSUREDCUSTOM','float'],['INSUREDDOB','INSUREDDOB','date'],['INSUREDEMPLOY','INSUREDEMPLOY','string'],['INSUREDFULLNA','INSUREDFULLNA','string'],
        ['INSUREDGENDER','INSUREDGENDER','string'],['INSUREDPOSTCO','INSUREDPOSTCO','float'],['INSUREDPREMIS','INSUREDPREMIS','string'],['INSUREDRETIRE','INSUREDRETIRE','float'],
        ['INSUREDSOCNUM','INSUREDSOCNUM','floatLen-9-0'],['LEAVEPLANID','LEAVEPLANID','string'],['LEAVEREQUESTI','LEAVEREQUESTI','string'],['NOTIFIEDDATE','NOTIFIEDDATE','Date'],
        ['PAYEEAGEATINC','PAYEEAGEATINC','float'],['PAYEECASEROLE','PAYEECASEROLE','string'],['PAYEERELTOINS','PAYEERELTOINS','string'],['PRIMARYDIAGNO','PRIMARYDIAGNO','string'],
        ['PRIMARYMEDICA','PRIMARYMEDICA','float'],['DIAG2MEDICALC','DIAG2MEDICALC','float'],['DIAG3MEDICALC','DIAG3MEDICALC','float'],['DIAG4MEDICALC','DIAG4MEDICALC','float'],
        ['DIAG5MEDICALC','DIAG5MEDICALC','float'],['PECLASSID','PECLASSID','float'],['PEINDEXID','PEINDEXID','float'],['DATEINTERFACE','DATEINTERFACE','Date']]

DFCompare.compare(sqldf,csvdf,cols)

  
#-------------------------------------------------------------------------------------------------------------
"""

try:
    driver.close()
except:
    pass  

print("Done")








"""
#-------------------------------------------------------------------------------------------------------------

html_logger.info("Start of Comparing XML with ODS")   
xmlpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\OneAmerica_Disbursement.xml"
xmldfcols = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']

sql1 = '''select PAY_RQST_NBR,PAY_ADMIN_SYS,CMPNY_CD,SRC_CD,LGCY_SRC_CD,PAY_TYP,PAY_AMT,PAYEE_NAME,CHK_ADDR_LINE_1,CHK_ADDR_LINE_2,CHK_ADDR_CITY,CHK_ADDR_ST,CHK_ADDR_ZIP_CD,MAIL_SORT_CD,ENT_OPER_USR_ID,RDFI_ROUT_NBR,RDFI_ACCT_NBR,RDFI_ACCT_TYP
            from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\')'''

dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'

xmlcols = ['Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ'] 
sqlcols = ['PAY_ADMIN_SYS','CMPNY_CD','SRC_CD','LGCY_SRC_CD','PAY_TYP','PAY_AMT','PAYEE_NAME','CHK_ADDR_LINE_1','CHK_ADDR_LINE_2','CHK_ADDR_CITY','CHK_ADDR_ST','CHK_ADDR_ZIP_CD','MAIL_SORT_CD','ENT_OPER_USR_ID','RDFI_ROUT_NBR','RDFI_ACCT_NBR','RDFI_ACCT_TYP']

Comparator.Compare_XML_DB(xmlpath, xmldfcols, sql1, dbserver, dbname, xmlcols, sqlcols)

#-------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing Claim Detail CSV with LandingZone") 
csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIClaimDetails.csv"
sql1 = '''select PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,ABSENCECASENU,BENEFITRIGHTT,CLAIMANTAGE,CLAIMANTCUSTO,
             CLAIMANTDOB,CLAIMANTGENDE,CLAIMANTNAME,CLAIMANTRELTO,CLAIMNUMBER,DIAGCODE2,DIAGCODE3,DIAGCODE4,DIAGCODE5,EMPLOYEEID,
             EVENTCAUSE,INCURREDDATE,INSUREDADDRES,INSUREDADDRL1,INSUREDADDRL2,INSUREDADDRL3,INSUREDADDRL4,INSUREDADDRL5,
             INSUREDADDRL6,INSUREDADDRL7,INSUREDAGE,INSUREDCORCOU,INSUREDCORRES,INSUREDCUSTOM,INSUREDDOB,INSUREDEMPLOY,INSUREDFULLNA,
             INSUREDGENDER,INSUREDPOSTCO,INSUREDPREMIS,INSUREDRETIRE,INSUREDSOCNUM,LEAVEPLANID,LEAVEREQUESTI,NOTIFIEDDATE,PAYEEAGEATINC,
             PAYEECASEROLE,PAYEERELTOINS,PRIMARYDIAGNO,PRIMARYMEDICA,DIAG2MEDICALC,DIAG3MEDICALC,DIAG4MEDICALC,DIAG5MEDICALC,PECLASSID,PEINDEXID,DATEINTERFACE
             from LZ.PEI_CLAIM_DTL'''
dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'


sqlcols = ['PEI_C','PEI_I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','ABSENCECASENU','BENEFITRIGHTT','CLAIMANTAGE','CLAIMANTCUSTO','CLAIMANTDOB','CLAIMANTGENDE','CLAIMANTNAME','CLAIMANTRELTO','CLAIMNUMBER','DIAGCODE2','DIAGCODE3','DIAGCODE4','DIAGCODE5','EMPLOYEEID','EVENTCAUSE','INCURREDDATE','INSUREDADDRES','INSUREDADDRL1','INSUREDADDRL2','INSUREDADDRL3','INSUREDADDRL4','INSUREDADDRL5','INSUREDADDRL6','INSUREDADDRL7','INSUREDAGE','INSUREDCORCOU','INSUREDCORRES','INSUREDCUSTOM','INSUREDDOB','INSUREDEMPLOY','INSUREDFULLNA','INSUREDGENDER','INSUREDPOSTCO','INSUREDPREMIS','INSUREDRETIRE','INSUREDSOCNUM','LEAVEPLANID','LEAVEREQUESTI','NOTIFIEDDATE','PAYEEAGEATINC','PAYEECASEROLE','PAYEERELTOINS','PRIMARYDIAGNO','PRIMARYMEDICA','DIAG2MEDICALC','DIAG3MEDICALC','DIAG4MEDICALC','DIAG5MEDICALC','PECLASSID','PEINDEXID','DATEINTERFACE']
csvcols = ['C','I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','ABSENCECASENU','BENEFITRIGHTT','CLAIMANTAGE','CLAIMANTCUSTO','CLAIMANTDOB','CLAIMANTGENDE','CLAIMANTNAME','CLAIMANTRELTO','CLAIMNUMBER','DIAGCODE2','DIAGCODE3','DIAGCODE4','DIAGCODE5','EMPLOYEEID','EVENTCAUSE','INCURREDDATE','INSUREDADDRES','INSUREDADDRL1','INSUREDADDRL2','INSUREDADDRL3','INSUREDADDRL4','INSUREDADDRL5','INSUREDADDRL6','INSUREDADDRL7','INSUREDAGE','INSUREDCORCOU','INSUREDCORRES','INSUREDCUSTOM','INSUREDDOB','INSUREDEMPLOY','INSUREDFULLNA','INSUREDGENDER','INSUREDPOSTCO','INSUREDPREMIS','INSUREDRETIRE','INSUREDSOCNUM','LEAVEPLANID','LEAVEREQUESTI','NOTIFIEDDATE','PAYEEAGEATINC','PAYEECASEROLE','PAYEERELTOINS','PRIMARYDIAGNO','PRIMARYMEDICA','DIAG2MEDICALC','DIAG3MEDICALC','DIAG4MEDICALC','DIAG5MEDICALC','PECLASSID','PEINDEXID','DATEINTERFACE']
            
Comparator.Compare_CSV_DB(csvpath,sql1,dbserver,dbname,csvcols,sqlcols)

#--------------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing XML with LandingZone")   
xmlpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\OneAmerica_Disbursement.xml"
xmldfcols = ['Pay_Rqst_Nbr','Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']

date1 = '2020-01-02'
sql1 = '''select 'FINEOS' as Pay_Admin_Sys, 'AUL' as Cmpny_cd,CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '8DCLC' Else '8DCLE' END AS srcCD,
CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '8DCLC' Else '8DCLE' END AS Lgcy_Src_Cd, CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN 'EFT' Else 'Check' END AS PAYMENTMETHOD,
A.GROSSPAYMENTA_MONAMT,CASE WHEN SUBSTRING(A.payeefullname,1,CHARINDEX(' ', A.payeefullname)) in ('Mr','Mrs','Miss','Ms') Then SUBSTRING(A.payeefullname,CHARINDEX(' ', A.payeefullname)+1,len(A.payeefullname) - CHARINDEX(' ', A.payeefullname)+1)
Else A.payeefullname End as payeename1,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD1 END AS PAYMENTADD1_mod,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD2 END AS PAYMENTADD2_mod,
CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD3 END AS PAYMENTADD3_mod,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD4 END AS PAYMENTADD4_mod,
CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTADD6 END AS PAYMENTADD6_mod,CASE WHEN A.PAYMENTMETHOD = 'Elec Funds Transfer' THEN '' Else A.PAYMENTPOSTCO END AS PAYMENTPOSTCO_mod,
'MA' as Mail_Sort_Cd,SUBSTRING(A.confirmedbyus, 1, CHARINDEX(' ', A.confirmedbyus)-1) as ENT_OP_ID,CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '' Else A.PAYEEBANKSORT END AS PAYEEBANKSORT_mod,
CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '' Else A.PAYEEACCOUNTN END AS PAYEEACCOUNTN_mod,CASE WHEN A.PAYMENTMETHOD = 'Check' THEN '' Else A.PAYEEACCOUNTT END AS PAYEEACCOUNTT_mod
from LZ.PEI_RQST_INFO A where PEI_I in (select Index_id from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))
and PEI_C in (select class_ID from FAI.PAY_RQST_INFO where PAY_RQST_INFO_GEN_ID in (select PAY_RQST_INFO_GEN_ID from FAI.PAY_BATCH_DTL where Convert(Date,LAST_UPDT_TS) = \'''' + date1 + '''\'))
'''                
dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'

xmlcols = ['Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ'] 
sqlcols = ['Pay_Admin_Sys','Cmpny_cd','srcCD','Lgcy_Src_Cd','PAYMENTMETHOD','GROSSPAYMENTA_MONAMT','payeename1','PAYMENTADD1_mod','PAYMENTADD2_mod','PAYMENTADD3_mod','PAYMENTADD4_mod','PAYMENTADD6_mod','PAYMENTPOSTCO_mod','Mail_Sort_Cd','ENT_OP_ID','PAYEEBANKSORT_mod','PAYEEACCOUNTN_mod','PAYEEACCOUNTT_mod']

Comparator.Compare_XML_DB(xmlpath, xmldfcols, sql1, dbserver, dbname, xmlcols, sqlcols)
#html_logger.info("End of Comparing XML with LandingZone") 

#--------------------------------------------------------------------------------------------------------------------
html_logger.info("Start of Comparing PEI CSV with LandingZone")

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv"
csvdf = CSVParser.getCSV(csvpath)
csvdf = csvdf[csvdf.PAYMENTMETHOD.isin(['Check','Elec Funds Transfer'])]



sql1 = '''SELECT PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,ADDRESSLINE1,ADDRESSLINE2,ADDRESSLINE3,ADDRESSLINE4,ADDRESSLINE5,
        ADDRESSLINE6,ADDRESSLINE7,ADVICETOPAY,ADVICETOPAYOV,AMALGAMATIONC,AMOUNT_MONAMT,AMOUNT_MONCUR,CHECKCUTTING,CONFIRMEDBYUS,CONFIRMEDUID,CONTRACTREF,
        CORRESPCOUNTR,CURRENCY,DATEINTERFACE,DESCRIPTION,EMPLOYEECONTR,EVENTEFFECTIV,EVENTREASON,EVENTTYPE,EXTRACTIONDAT,GROSSPAYMENTA_MONAMT,GROSSPAYMENTA_MONCUR,
        INSUREDRESIDE,NAMETOPRINTON,NOMINATEDPAYE,NOMPAYEECUSTO,NOMPAYEEDOB,NOMPAYEEFULLN,NOMPAYEESOCNU,NOTES,PAYEEACCOUNTN,PAYEEACCOUNTT,PAYEEADDRESS,PAYEEBANKBRAN,
        PAYEEBANKCODE,PAYEEBANKINST,PAYEEBANKSORT,PAYEECORRESPO,PAYEECUSTOMER,PAYEEDOB,PAYEEFULLNAME,PAYEEIDENTIFI,REPLICATE('0', 9-LEN(PAYEESOCNUMBE)) + PAYEESOCNUMBE as PAYEESOCNUMBE,
        PAYMENTADD,PAYMENTADD1,PAYMENTADD2,PAYMENTADD3,PAYMENTADD4,PAYMENTADD5,PAYMENTADD6,PAYMENTADD7,PAYMENTADDCOU,PAYMENTCORRST,PAYMENTDATE,PAYMENTFREQUE,PAYMENTMETHOD,PAYMENTPOSTCO,
        PAYMENTPREMIS,PAYMENTTRIGGE,PAYMENTTYPE,PAYMETHCURREN,POSTCODE,PREMISESNO,SETUPBYUSERID,SETUPBYUSERNA,STATUS,STATUSEFFECTI,STATUSREASON,STOCKNO,SUMMARYEFFECT,SUMMARYSTATUS,
        TRANSSTATUSDA FROM LZ.PEI_RQST_INFO'''

dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'
sqldf = SQLParser.getSQL(sql1,dbserver,dbname)

sqlcols = ['PEI_C','PEI_I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','ADDRESSLINE1','ADDRESSLINE2','ADDRESSLINE3','ADDRESSLINE4','ADDRESSLINE5','ADDRESSLINE6','ADDRESSLINE7','ADVICETOPAY','ADVICETOPAYOV','AMALGAMATIONC','AMOUNT_MONAMT','AMOUNT_MONCUR','CHECKCUTTING','CONFIRMEDBYUS','CONFIRMEDUID','CONTRACTREF','CORRESPCOUNTR','CURRENCY','DATEINTERFACE','DESCRIPTION','EMPLOYEECONTR','EVENTEFFECTIV','EVENTREASON','EVENTTYPE','EXTRACTIONDAT','GROSSPAYMENTA_MONAMT','GROSSPAYMENTA_MONCUR','INSUREDRESIDE','NAMETOPRINTON','NOMINATEDPAYE','NOMPAYEECUSTO','NOMPAYEEDOB','NOMPAYEEFULLN','NOMPAYEESOCNU','NOTES','PAYEEACCOUNTN','PAYEEACCOUNTT','PAYEEADDRESS','PAYEEBANKBRAN','PAYEEBANKCODE','PAYEEBANKINST','PAYEEBANKSORT','PAYEECORRESPO','PAYEECUSTOMER','PAYEEDOB','PAYEEFULLNAME','PAYEEIDENTIFI','PAYEESOCNUMBE','PAYMENTADD','PAYMENTADD1','PAYMENTADD2','PAYMENTADD3','PAYMENTADD4','PAYMENTADD5','PAYMENTADD6','PAYMENTADD7','PAYMENTADDCOU','PAYMENTCORRST','PAYMENTDATE','PAYMENTFREQUE','PAYMENTMETHOD','PAYMENTPOSTCO','PAYMENTPREMIS','PAYMENTTRIGGE','PAYMENTTYPE','PAYMETHCURREN','POSTCODE','PREMISESNO','SETUPBYUSERID','SETUPBYUSERNA','STATUS','STATUSEFFECTI','STATUSREASON','STOCKNO','SUMMARYEFFECT','SUMMARYSTATUS','TRANSSTATUSDA']
csvcols = ['C','I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','ADDRESSLINE1','ADDRESSLINE2','ADDRESSLINE3','ADDRESSLINE4','ADDRESSLINE5','ADDRESSLINE6','ADDRESSLINE7','ADVICETOPAY','ADVICETOPAYOV','AMALGAMATIONC','AMOUNT_MONAMT','AMOUNT_MONCUR','CHECKCUTTING','CONFIRMEDBYUS','CONFIRMEDUID','CONTRACTREF','CORRESPCOUNTR','CURRENCY','DATEINTERFACE','DESCRIPTION','EMPLOYEECONTR','EVENTEFFECTIV','EVENTREASON','EVENTTYPE','EXTRACTIONDAT','GROSSPAYMENTA_MONAMT','GROSSPAYMENTA_MONCUR','INSUREDRESIDE','NAMETOPRINTON','NOMINATEDPAYE','NOMPAYEECUSTO','NOMPAYEEDOB','NOMPAYEEFULLN','NOMPAYEESOCNU','NOTES','PAYEEACCOUNTN','PAYEEACCOUNTT','PAYEEADDRESS','PAYEEBANKBRAN','PAYEEBANKCODE','PAYEEBANKINST','PAYEEBANKSORT','PAYEECORRESPO','PAYEECUSTOMER','PAYEEDOB','PAYEEFULLNAME','PAYEEIDENTIFI','PAYEESOCNUMBE','PAYMENTADD','PAYMENTADD1','PAYMENTADD2','PAYMENTADD3','PAYMENTADD4','PAYMENTADD5','PAYMENTADD6','PAYMENTADD7','PAYMENTADDCOU','PAYMENTCORRST','PAYMENTDATE','PAYMENTFREQUE','PAYMENTMETHOD','PAYMENTPOSTCO','PAYMENTPREMIS','PAYMENTTRIGGE','PAYMENTTYPE','PAYMETHCURREN','POSTCODE','PREMISESNO','SETUPBYUSERID','SETUPBYUSERNA','STATUS','STATUSEFFECTI','STATUSREASON','STOCKNO','SUMMARYEFFECT','SUMMARYSTATUS','TRANSSTATUSDA']
comtype = ['String','String','Date','String','String','String','String','String','String','String','String','String','Boolean','String',]

cols = [['PEI_C','C','String'],['PEI_I','I','String'],['LASTUPDATEDATE','LASTUPDATEDATE','Date']]

##Comparator.Compare_CSV_DB(csvpath,sql1,dbserver,dbname,csvcols,sqlcols)
#DFCompare.diffDataFrame(csvdf,sqldf,csvcols,sqlcols)

DFCompare.compare(csvdf,sqldf,cols)

#--------------------------------------------------------------------------------------------------------------------

html_logger.info("Start of Comparing PEIPaymentDetails CSV with LandingZone")

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentDetails.csv"
sql1 = '''select PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,BENEFITEFFECT,BENEFITFINALP,DESCRIPTION_PAYMENTDTLS,PAYMENTENDPER,PAYMENTSTARTP,
            BALANCINGAMOU_MONAMT,BALANCINGAMOU_MONCUR,BUSINESSNETBE_MONAMT,BUSINESSNETBE_MONCUR,DUETYPE,GROUPID,PECLASSID,PEINDEXID,CLAIMDETAILSCLASSID,
            CLAIMDETAILSINDEXID,DATEINTERFACE,LAST_UPDT_ID,LAST_UPDT_TS from LZ.PEI_PAY_DTL'''

dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'

sqlcols = ['PEI_C','PEI_I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','BENEFITEFFECT','BENEFITFINALP','DESCRIPTION_PAYMENTDTLS','PAYMENTENDPER','PAYMENTSTARTP','BALANCINGAMOU_MONAMT','BALANCINGAMOU_MONCUR','BUSINESSNETBE_MONAMT','BUSINESSNETBE_MONCUR','DUETYPE','GROUPID','PECLASSID','PEINDEXID','CLAIMDETAILSCLASSID','CLAIMDETAILSINDEXID','DATEINTERFACE']
csvcols = ['C','I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','BENEFITEFFECT','BENEFITFINALP','DESCRIPTION_PAYMENTDTLS','PAYMENTENDPER','PAYMENTSTARTP','BALANCINGAMOU_MONAMT','BALANCINGAMOU_MONCUR','BUSINESSNETBE_MONAMT','BUSINESSNETBE_MONCUR','DUETYPE','GROUPID','PECLASSID','PEINDEXID','CLAIMDETAILSCLASSID','CLAIMDETAILSINDEXID','DATEINTERFACE']

Comparator.Compare_CSV_DB(csvpath,sql1,dbserver,dbname,csvcols,sqlcols)

#--------------------------------------------------------------------------------------------------------------------

html_logger.info("Start of Comparing PEIPaymentLine CSV with LandingZone")

csvpath = "C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentLine.csv"
sql1 = '''select PEI_C,PEI_I,LASTUPDATEDATE,C_OSUSER_UPDATEDBY,I_OSUSER_UPDATEDBY,AMOUNT_MONAMT,AMOUNT_MONCUR,INTEGRTYPE,LINETYPE,REFERENCE,RESERVECATEGO,RESERVETYPE,
            SEQUENCENUMBE,SUBTOTALS,TAXABLEINCOME,USETOCALCRULE,C_PYMNTEIF_PAYMENTLINES,I_PYMNTEIF_PAYMENTLINES,PAYMENTDETAILCLASSID,PAYMENTDETAILINDEXID,
            PURCHASEDETAILCLASSID,PURCHASEDETAILINDEXID,DATEINTERFACE
            from LZ.PEI_PAY_LINE p'''
            
dbserver = 'SQLD10746,4242'
dbname = 'ENTPRS_CLAIMS_ODS'

sqlcols = ['PEI_C','PEI_I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','AMOUNT_MONAMT','AMOUNT_MONCUR','INTEGRTYPE','LINETYPE','REFERENCE','RESERVECATEGO','RESERVETYPE','SEQUENCENUMBE','SUBTOTALS','TAXABLEINCOME','USETOCALCRULE','C_PYMNTEIF_PAYMENTLINES','I_PYMNTEIF_PAYMENTLINES','PAYMENTDETAILCLASSID','PAYMENTDETAILINDEXID','PURCHASEDETAILCLASSID','PURCHASEDETAILINDEXID','DATEINTERFACE']
csvcols = ['C','I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','AMOUNT_MONAMT','AMOUNT_MONCUR','INTEGRTYPE','LINETYPE','REFERENCE','RESERVECATEGO','RESERVETYPE','SEQUENCENUMBE','SUBTOTALS','TAXABLEINCOME','USETOCALCRULE','C_PYMNTEIF_PAYMENTLINES','I_PYMNTEIF_PAYMENTLINES','PAYMENTDETAILCLASSID','PAYMENTDETAILINDEXID','PURCHASEDETAILCLASSID','PURCHASEDETAILINDEXID','DATEINTERFACE']

Comparator.Compare_CSV_DB(csvpath,sql1,dbserver,dbname,csvcols,sqlcols)

#--------------------------------------------------------------------------------------------------------------------
'''
from Payment_ReusableArtifacts import CSVParser,SQLParser,XMlParser,DFCompare

csvdf = CSVParser.getCSV(csvpath)
xmlcols = ['Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_Line_3','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']
xmldf = XMLParser.getXML(xmlpath,xmlcols)

csvcols = ['C','I','LASTUPDATEDATE','C_OSUSER_UPDATEDBY','I_OSUSER_UPDATEDBY','AMOUNT_MONAMT','AMOUNT_MONCUR','INTEGRTYPE','LINETYPE','REFERENCE','RESERVECATEGO','RESERVETYPE','SEQUENCENUMBE','SUBTOTALS','TAXABLEINCOME','USETOCALCRULE','C_PYMNTEIF_PAYMENTLINES','I_PYMNTEIF_PAYMENTLINES','PAYMENTDETAILCLASSID','PAYMENTDETAILINDEXID','PURCHASEDETAILCLASSID','PURCHASEDETAILINDEXID','DATEINTERFACE']
xmlcols = ['Pay_Admin_Sys','Cmpny_Cd','Src_Cd','Lgcy_Src_Cd','Pay_Typ','Pay_Amt','Payee_Name','Chk_Addr_Line_1','Chk_Addr_Line_2','Chk_Addr_City','Chk_Addr_St','Chk_Addr_Zip_Cd','Mail_Sort_Cd','Ent_Oper_Usr_Id','Rdfi_Rout_Nbr','Rdfi_Acct_Nbr','Rdfi_Acct_Typ']

DFCompare.diffDataFrame(csvdf,xmldf,)
'''
"""


