'''
import matplotlib.pyplot as plt
from numpy import math
import numpy as np

i = np.arange(0,3*np.pi,0.1)
j = np.sin(i)  
plt.plot(i,j)
plt.show()
#-----------------------------------------
'''
'''
s = [1,2,3]
print("%s ~ %s" %(s,s[0]))
print("%s %s" %(s[-1], s[-2]))
s1 = "hello"
print(s1.capitalize())
print(s1.upper())
print(s1.rjust(7,"*"))
print(s1.ljust(15,"*"))
print(s1.replace('l', "(llo)"))

'''
'''
s = list(range(4))
print(s)
print(s[-1])
print(s[2:])
print(s[:-1])

for s1 in s:
    print(s1)
    
for i,s1 in enumerate(s):
    print("#%d : %s" %(i,s1))
'''
'''
nums = [0,1,2,3,12,15,25,50]
squ_num = []
for num in nums:
    squ_num.append(num**2) 
print(squ_num)
'''
    
# Create the following rank 2 array with shape (3, 4)
# [[ 1  2  3  4]
#  [ 5  6  7  8]
#  [ 9 10 11 12]]   
'''
import numpy as np 
a = np.array([[1,2,3,4], [5,6,7,8], [9,10,11,12]])

row=a[1,:]
print(row)

rows = a[1:3,:]
print(rows)

col = a[:,1:2]
print(col)

cols = a[:,1]
print(cols)

rows = [0,1,2]
cols = [0,3,1]
print(a[rows,cols])

im1 = im.resize((100,100),Image.ANTIALIAS)
im2 = im.thumbnail((150,150))


from PIL import Image
im = Image.open("C:\\Users\\T003320\\Pictures\\Sanju.jpg").convert("L")
im.show()
'''
'''
claimNos = ['DI-327','DI-416']               #['DI-416'] #['DI-383','DI-382', 'DI-384'] #
import pandas as pd
for claimNo in claimNos:
    print("Working on Claim Number " + str(claimNo))
    PEclass = ""
    PEindex = ""
    df1 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIClaimDetails.csv")
    df2 = df1[df1['CLAIMNUMBER']==claimNo]
    PEclass = df2['PECLASSID'].to_string(index = False)
    PEindex = df2['PEINDEXID'].to_string(index = False)
    
    if("\n' in PEclass"):
        arrPEclass = PEclass.split("\n")
        arrPEindex = PEindex.split("\n")
    else:
        arrPEclass = PEclass
        arrPEindex = PEindex
    i = 0 
    PElistAmount = [] #list1 = []
    list3 = []
    PEIlistLineType = []
    listLineType = []
    
    for PEclass1 in arrPEclass:     
        PEindex1 = arrPEindex[i]
        i = i + 1 
        df3 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentLine.csv")
        try:
            df4 = df3[df3['C_PYMNTEIF_PAYMENTLINES'] == int(PEclass1)]
            df4 = df4[df4['I_PYMNTEIF_PAYMENTLINES'] == int(PEindex1)]
            
            df5 = df4['AMOUNT_MONAMT']
            #df6 = df4['PAYEEFULLNAME']
            df7 = df4['LINETYPE']
            
            PElistAmount = PElistAmount + (df5.values).tolist()
            #list3 = list3 + (df6.values).tolist()
            PEIlistLineType = PEIlistLineType + (df7.values).tolist()
        except:
            print("no records found for " + str(claimNo) + " in the PEI Extract")
    
    exp_df = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\extract.csv")            
    exp_df = exp_df[exp_df['CLAIMNUMBER']==claimNo]
    exp_df1 = exp_df['AMOUNT_MONAMT']
    exp_name = exp_df['PAYEEFULLNAME'].dropna()
    exp_paymethod = exp_df['PAYMENTMETHOD'].dropna()
    exp_lineType = exp_df['LINETYPE'].dropna()
    
    ExpAMOUNT = exp_df1.values.tolist()
    listName = exp_name.values.tolist()
    listpaymentMethod = exp_paymethod.values.tolist()
    listLineType = exp_lineType.values.tolist()
    
    totamt = 0
    for amnt in ExpAMOUNT:
        totamt = totamt + amnt
    print(totamt)
    
    try:
        if(sorted(PElistAmount)==sorted(ExpAMOUNT)):
            print("Breakdown of the amount matches")
        else:
            print("Breakdown of the amount doesn't match")    
    except:
        print(False)
        
    print("--------------------------------------------------------------------")
    
    peinamelist = []
    peipaymentmethod = []
    
    acttotamt = 0    
    df31 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv")
    i = 0
    for PEclass1 in arrPEclass:     
        PEindex1 = arrPEindex[i]
        i = i + 1 
        #try:
        df41 = df31[df31['C'] == int(PEclass1)]
        df41 = df41[df41['I'] == int(PEindex1)]
        df51 = df41['AMOUNT_MONAMT']
        df52 = df41['PAYMENTMETHOD']
        df53 = df41['PAYEEFULLNAME']
        df54 = df41['PAYMENTMETHOD']
        
        print(str(df53.to_string(index = False)))
        peinamelist.append(df53.to_string(index = False).strip())
        peipaymentmethod.append(df54.to_string(index = False).strip())
        
        print("Individual amount of the payment is " + str(round(float(df51),2)))
        print("Payment method is " + df52.to_string(index = False))
        #print("Payee FullName is " + df53.to_string(index = False))
        acttotamt = acttotamt + round(float(df51),2)
            
    if(round(float(acttotamt),2)==round(float(totamt),2)):
        print("amount matches. Values are " + str(round(float(totamt),2)))
    else:
        print("amount doesn't match. Expected value is " + str(round(float(totamt),2)) + " but the actual is " + str(round(float(acttotamt),2)) + " and the payment method is " + df52.to_string(index = False))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------")

    if(sorted(listName) == sorted(peinamelist)):
        print("Name matches. Values are " + str(listName))
    else:
        print("Name doesn't match.PEI extract has " + str(peinamelist) + " but the payment made for " + str(listName))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------")   
    
    if(sorted(listpaymentMethod) == sorted(peipaymentmethod)):
        print("Payment Method matches. Values are " + str(listpaymentMethod))
    else:
        print("Payment Method doesn't match.PEI extract has " + str(peipaymentmethod) + " but the payment made for " + str(listpaymentMethod))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------") 
       
    if(sorted(listLineType) == sorted(PEIlistLineType)):
        print("Payment LineType matches. Values are " + str(listLineType))
    else:
        print("Payment LineType doesn't match.PEI extract has " + str(PEIlistLineType) + " but the payment made for " + str(listLineType))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------") 
     
        
    print("*************************************************************")
    
'''       
'''
   claimNos = ['DI-327','DI-416']               #['DI-416'] #['DI-383','DI-382', 'DI-384'] #
import pandas as pd
for claimNo in claimNos:
    print("Working on Claim Number " + str(claimNo))
    PEclass = ""
    PEindex = ""
    df1 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIClaimDetails.csv")
    df2 = df1[df1['CLAIMNUMBER']==claimNo]
    PEclass = df2['PECLASSID'].to_string(index = False)
    PEindex = df2['PEINDEXID'].to_string(index = False)
    
    if("\n' in PEclass"):
        arrPEclass = PEclass.split("\n")
        arrPEindex = PEindex.split("\n")
    else:
        arrPEclass = PEclass
        arrPEindex = PEindex
    i = 0 
    list1 = []
    list3 = []
    PEIlistLineType = []
    listLineType = []
    
    for PEclass1 in arrPEclass:     
        PEindex1 = arrPEindex[i]
        i = i + 1 
        df3 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEIPaymentLine.csv")
        try:
            df4 = df3[df3['C_PYMNTEIF_PAYMENTLINES'] == int(PEclass1)]
            df4 = df4[df4['I_PYMNTEIF_PAYMENTLINES'] == int(PEindex1)]
            
            df5 = df4['AMOUNT_MONAMT']
            #df6 = df4['PAYEEFULLNAME']
            df7 = df4['LINETYPE']
            
            list1 = list1 + (df5.values).tolist()
            #list3 = list3 + (df6.values).tolist()
            PEIlistLineType = PEIlistLineType + (df7.values).tolist()
        except:
            print("no records found for " + str(claimNo) + " in the PEI Extract")
    
    exp_df = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\extract.csv")            
    exp_df = exp_df[exp_df['CLAIMNUMBER']==claimNo]
    exp_df1 = exp_df['AMOUNT_MONAMT']
    exp_name = exp_df['PAYEEFULLNAME'].dropna()
    exp_paymethod = exp_df['PAYMENTMETHOD'].dropna()
    exp_lineType = exp_df['LINETYPE'].dropna()
    
    list2 = exp_df1.values.tolist()
    listName = exp_name.values.tolist()
    listpaymentMethod = exp_paymethod.values.tolist()
    listLineType = exp_lineType.values.tolist()
    
    totamt = 0
    for amnt in list2:
        totamt = totamt + amnt
    print(totamt)
    
    try:
        if(sorted(list1)==sorted(list2)):
            print("Breakdown of the amount matches")
        else:
            print("Breakdown of the amount doesn't match")    
    except:
        print(False)
        
    print("--------------------------------------------------------------------")
    
    peinamelist = []
    peipaymentmethod = []
    
    acttotamt = 0    
    df31 = pd.read_csv("C:\\Users\\T003320\\Desktop\\New_PEI\\CurrentPEI\\PEI.csv")
    i = 0
    for PEclass1 in arrPEclass:     
        PEindex1 = arrPEindex[i]
        i = i + 1 
        #try:
        df41 = df31[df31['C'] == int(PEclass1)]
        df41 = df41[df41['I'] == int(PEindex1)]
        df51 = df41['AMOUNT_MONAMT']
        df52 = df41['PAYMENTMETHOD']
        df53 = df41['PAYEEFULLNAME']
        df54 = df41['PAYMENTMETHOD']
        
        print(str(df53.to_string(index = False)))
        peinamelist.append(df53.to_string(index = False).strip())
        peipaymentmethod.append(df54.to_string(index = False).strip())
        
        print("Individual amount of the payment is " + str(round(float(df51),2)))
        print("Payment method is " + df52.to_string(index = False))
        #print("Payee FullName is " + df53.to_string(index = False))
        acttotamt = acttotamt + round(float(df51),2)
            
    if(round(float(acttotamt),2)==round(float(totamt),2)):
        print("amount matches. Values are " + str(round(float(totamt),2)))
    else:
        print("amount doesn't match. Expected value is " + str(round(float(totamt),2)) + " but the actual is " + str(round(float(acttotamt),2)) + " and the payment method is " + df52.to_string(index = False))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------")

    if(sorted(listName) == sorted(peinamelist)):
        print("Name matches. Values are " + str(listName))
    else:
        print("Name doesn't match.PEI extract has " + str(peinamelist) + " but the payment made for " + str(listName))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------")   
    
    if(sorted(listpaymentMethod) == sorted(peipaymentmethod)):
        print("Payment Method matches. Values are " + str(listpaymentMethod))
    else:
        print("Payment Method doesn't match.PEI extract has " + str(peipaymentmethod) + " but the payment made for " + str(listpaymentMethod))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------") 
       
    if(sorted(listLineType) == sorted(PEIlistLineType)):
        print("Payment LineType matches. Values are " + str(listLineType))
    else:
        print("Payment LineType doesn't match.PEI extract has " + str(PEIlistLineType) + " but the payment made for " + str(listLineType))   
    #except:
        #print("False")
    print("--------------------------------------------------------------------") 
     
        
    print("*************************************************************")
    ''' 
    
    
import numpy as np    
import pandas as pd

df = pd.DataFrame(
    [['Srikanth','38','M','Indian'],
     ['Sanjana','6','F',"Indian"],
     ['Shyam','9','M','Indian']],
     index= ('1','2','3'),
     columns=('Name','Age','Sex','Nationality')   
    )

print(df.head(2))
print(df.tail(2))
print(df.describe())
df['Age'] = df['Age'].astype(float)
print(df.sort_values('Age',ascending=True))
print(df.sort_index(ascending=False))

print(df['Nationality'])
print(df[0:3])
print(df.loc[:,['Name','Age']])
print(df.loc[:,['Name','Sex']])
print(df.iloc[1:3,[1,3]])

print(df[df['Age']<10])
print(df[df['Sex'] == 'M'])

df.loc[:,'temp_col'] = np.array(5)
print(df) 




    